import extensions from './vinDrExtensions';
import loading from './loading';
import preferences from './vinDrPreferences';
import servers from './servers';
import studies from './studies';
import timepointManager from './timepointManager';
import viewports from './viewports';

const reducers = {
  extensions,
  loading,
  preferences,
  servers,
  studies,
  timepointManager,
  viewports,
};

export default reducers;
