import get from 'lodash/get';

const settings = localStorage.getItem('settings');
const settingsParsed = JSON.parse(settings);
const defaultPanelTabName =
  get(settingsParsed, 'settings.defaultPanelTabName') || 'ai-tab';

const defaultSettings = {
  isVisibleAnnotation: true,
  panelTabName: 'ai-tab', // the current of tab name
  defaultPanelTabName, // force user has default tab name
  isVisibleLocalTag: true,
};

const getSettings = () => {
  let result = defaultSettings;

  // override values that we do not want to store
  if (settings && settingsParsed && defaultPanelTabName) {
    return {
      ...settingsParsed.settings,
      panelTabName: defaultPanelTabName,
      isVisibleAllTag: true,
    };
  }

  return result;
};

export const defaultState = {
  settings: getSettings(),
  user: {},
  studylist: {
    NEW: [],
    APPROVED: [],
    ALL: [],
  },
  metadata: {
    isChestBody: false,
    isBreastBody: false,
    modality: '',
    bodyPart: '',
    workgroupsOfStudy: [],
  },
  predictionPanel: {
    tabName: 'doctor-tab',
    workgroupsOfStudy: [],
    aiTabContent: {
      localData: [],
      globalData: [],
      measurementsData: {},
    },
    doctorTabContent: {
      editorSourceHTML: null,
      measurementData: {},
    },
    editorTemplates: [],
    activeEditorTemplate: '',
  },
};

const persists = ['settings'];

const extensions = (state = defaultState, action) => {
  switch (action.type) {
    case 'SET_EXTENSION_DATA': {
      const extensionName = action.extension;
      const currentData = state[extensionName] || {};
      const incomingData = action.data;

      const extension = {
        [extensionName]: {
          ...currentData,
          ...incomingData,
        },
      };

      if (persists.includes(extensionName)) {
        localStorage.setItem(extensionName, JSON.stringify(extension));
      }

      return { ...state, ...extension };
    }

    default:
      return state;
  }
};

export default extensions;
