import { addInstancesToStudy } from './studyInstanceHelpers';

import apiService from '@tuvm/viewer/src/services/api';

const MEDIATYPES = {
  DICOM: 'application/dicom',
  DICOM_JSON: 'application/dicom+json',
  OCTET_STREAM: 'application/octet-stream',
  PDF: 'application/pdf',
  JPEG: 'image/jpeg',
  PNG: 'image/png',
};

export async function retrieveSeriesAsyncLoader(
  server,
  study,
  studyInstanceUID,
  seriesInstanceUID
) {
  console.info(`Retry fetch metadata of series ${seriesInstanceUID}`);
  let url = `${server.wadoRoot}/studies/${studyInstanceUID}/series/${seriesInstanceUID}/metadata`;

  const { data: sopInstances } = await apiService({
    url,
    method: 'get',
    headers: { Accept: MEDIATYPES.DICOM_JSON },
  });

  await addInstancesToStudy(server, study, sopInstances);

  return study.seriesMap[seriesInstanceUID];
}
