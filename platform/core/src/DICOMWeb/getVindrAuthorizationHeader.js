// import user from '../user';
import keycloak from '@tuvm/viewer/src/services/auth';
import { ACCESS_KEY, X_ACCESS_KEY } from '@tuvm/viewer/src/utils/constants';
// import cookie from 'js-cookie';
/**
 * Returns the Authorization header as part of an Object.
 *
 * @export
 * @param {Object} [server={}]
 * @param {Object} [server.requestOptions]
 * @param {string|function} [server.requestOptions.auth]
 * @returns {Object} { Authorization }
 */
export default function getAuthorizationHeader({ requestOptions } = {}) {
  const headers = {};

  // Check for OHIF.user since this can also be run on the server
  const accessToken = keycloak.getPermissionToken(); //cookie.get(PERMISSION_TOKEN);

  // Auth for a specific server
  if (requestOptions && requestOptions.auth) {
    if (typeof requestOptions.auth === 'function') {
      // Custom Auth Header
      headers.Authorization = requestOptions.auth(requestOptions);
    } else {
      // HTTP Basic Auth (user:password)
      headers.Authorization = `Basic ${btoa(requestOptions.auth)}`;
    }
  }
  // Auth for the user's default
  else if (accessToken) {
    headers.Authorization = `Bearer ${accessToken}`;
  }

  const url = new URL(window.location.href);
  const tempToken = url.searchParams.get(ACCESS_KEY);
  if (tempToken) {
    headers[X_ACCESS_KEY] = tempToken;
  }

  return headers;
}
