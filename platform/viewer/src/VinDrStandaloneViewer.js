import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { withRouter, matchPath } from 'react-router';
import { Route } from 'react-router-dom';
import { NProgress } from '@tanem/react-nprogress';
import { CSSTransition } from 'react-transition-group';
import { connect } from 'react-redux';
import { ViewerbaseDragDropContext } from '@tuvm/ui';
// import { SignoutCallbackComponent } from 'redux-oidc';
import get from 'lodash/get';
import OHIF from '@tuvm/core';
// import asyncComponent from './components/AsyncComponent.js';
import * as RoutesUtil from './routes/routesUtil';
import intersection from 'lodash/intersection';
import { withTranslation } from 'react-i18next';
import NotFound from './pages/NotFound/index.js';
import NoPermission from './pages/NoPermission.js';
import { Bar, Container } from './components/LoadingBar/';
import { compose } from 'redux';
import './VinDrStandaloneViewer.css';
import './variables.css';
import './theme-tide.css';

import AppContext from './context/AppContext';
import { CONFIG_SERVER } from './utils/constants';
// import { REFRESH_TOKEN, TOKEN } from './utils/constants';
// import cookie from 'js-cookie';
// import { getViewportApiKey } from './utils/helper';

const { redux } = OHIF;
const { setExtensionData } = redux.actions;
let unlisten;

// Contexts
// const CallbackPage = asyncComponent(() =>
//   import(/* webpackChunkName: "CallbackPage" */ './routes/CallbackPage.js')
// );

const OHIFStandaloneViewer = props => {
  // const contextType = AppContext;
  window.t = props.t;
  const [isLoading, setIsLoading] = useState(false);
  // const { keycloak, initialized } = useKeycloak();

  useEffect(() => {
    unlisten = props.history.listen((location, action) => {
      if (props.setContext) {
        props.setContext(window.location.pathname);
      }
    });

    return () => {
      unlisten();
    };
  }, []);

  // useEffect(() => {
  //   console.log(keycloak);
  //   console.log(keycloak.authenticated);
  //   if (!keycloak.authenticated) {
  //     console.log('user is not authenticated..!');
  //     // keycloak.login();
  //   } else {
  //     getToken();
  //   }
  // }, [keycloak]);

  // const getToken = async () => {
  //   console.log('get token');
  //   try {
  //     console.log(keycloak.token);
  //     const user = await getAccountInfo(keycloak.token);
  //     if (user && user.scopes) {
  //       requestPermissionToken(keycloak, user.scopes, () => {
  //         onPermissionTokenFetched(AuthService.getPermissionToken());
  //       });
  //     } else {
  //       console.log('No permission');
  //     }
  //     // await getAccountInfo(keycloak.token);
  //   } catch (err) {
  //     console.log('Can not get Account Info');
  //   }
  // };

  // componentDidUpdate() {
  //   const refreshToken = cookie.get(REFRESH_TOKEN);
  //   if (!refreshToken) {
  //     requestLogin(true);
  //   }
  // }

  // const accessToken = cookie.get(TOKEN);
  // const refreshToken = cookie.get(REFRESH_TOKEN);

  // const { userInfo } = props;
  // console.log(userManager);
  // if (!userInfo.username || (!accessToken && !refreshToken)) {
  //   // TODO: should redirect to login page.
  //   return <FullScreenLoading />;
  // }

  const { appConfig = {} } = useContext(AppContext);
  // const userNotLoggedIn = userManager && (!user || user.expired);
  // if (userNotLoggedIn) {
  //   const pathname = props.location.pathname;

  //   if (pathname !== '/callback') {
  //     sessionStorage.setItem('ohif-redirect-to', pathname);
  //   }

  //   return (
  //     <Switch>
  //       <Route
  //         exact
  //         path="/silent-refresh.html"
  //         onEnter={RoutesUtil.reload}
  //       />
  //       <Route
  //         exact
  //         path="/logout-redirect"
  //         render={() => (
  //           <SignoutCallbackComponent
  //             userManager={userManager}
  //             successCallback={() => console.log('Signout successful')}
  //             errorCallback={error => {
  //               console.warn(error);
  //               console.warn('Signout failed');
  //             }}
  //           />
  //         )}
  //       />
  //       <Route
  //         path="/callback"
  //         render={() => <CallbackPage userManager={userManager} />}
  //       />
  //       <Route
  //         path="/login"
  //         component={() => {
  //           const queryParams = new URLSearchParams(
  //             props.location.search
  //           );
  //           const iss = queryParams.get('iss');
  //           const loginHint = queryParams.get('login_hint');
  //           const targetLinkUri = queryParams.get('target_link_uri');
  //           const oidcAuthority =
  //             appConfig.oidc !== null && appConfig.oidc[0].authority;
  //           if (iss !== oidcAuthority) {
  //             console.error(
  //               'iss of /login does not match the oidc authority'
  //             );
  //             return null;
  //           }

  //           userManager.removeUser().then(() => {
  //             if (targetLinkUri !== null) {
  //               sessionStorage.setItem(
  //                 'ohif-redirect-to',
  //                 new URL(targetLinkUri).pathname
  //               );
  //             } else {
  //               sessionStorage.setItem('ohif-redirect-to', '/');
  //             }

  //             if (loginHint !== null) {
  //               userManager.signinRedirect({ login_hint: loginHint });
  //             } else {
  //               userManager.signinRedirect();
  //             }
  //           });

  //           return null;
  //         }}
  //       />
  //       <Route
  //         component={() => {
  //           userManager.getUser().then(user => {
  //             if (user) {
  //               userManager.signinSilent();
  //             } else {
  //               userManager.signinRedirect();
  //             }
  //           });

  //           return null;
  //         }}
  //       />
  //     </Switch>
  //   );
  // }

  /**
   * Note: this approach for routing is caused by the conflict between
   * react-transition-group and react-router's <Switch> component.
   *
   * See http://reactcommunity.org/react-transition-group/with-react-router/
   */
  const routes = RoutesUtil.getRoutes(appConfig);

  const currentPath = props.location.pathname;
  const matchingRoutes = routes.find(r =>
    matchPath(currentPath, {
      path: r.path,
      exact: true,
    })
  );

  // const { userInfo } = props;

  // const tempToken = getViewportApiKey();
  // if (!tempToken) {
  //   if (!userInfo.username) {
  //     // check authorization only if having token or route is private
  //     if (accessToken) {
  //       getAccountInfo();
  //     } else {
  //       checkAuthorizationFlow();
  //     }
  //   }
  // }

  const scopes = get(props.userInfo, 'scopes');

  return (
    <>
      <NProgress isAnimating={isLoading}>
        {({ isFinished, progress, animationDuration }) => (
          <Container
            isFinished={isFinished}
            animationDuration={animationDuration}
          >
            <Bar progress={progress} animationDuration={animationDuration} />
          </Container>
        )}
      </NProgress>
      <Route exact path="/silent-refresh.html" onEnter={RoutesUtil.reload} />
      <Route exact path="/logout-redirect.html" onEnter={RoutesUtil.reload} />
      {matchingRoutes &&
        routes.map(({ path, Component, permissions }) => (
          <Route key={path} exact path={path}>
            {({ match }) => {
              const hasPermission =
                CONFIG_SERVER.DISABLE_AUTH ||
                intersection(permissions, scopes).length;
              const getCom = () => {
                if (match === null) {
                  return <></>;
                } else if (permissions && !hasPermission) {
                  return <NoPermission />;
                }
                return <Component match={match} location={props.location} />;
              };

              return (
                <CSSTransition
                  in={match !== null}
                  timeout={300}
                  classNames="fade"
                  unmountOnExit
                  onEnter={() => {
                    setIsLoading(true);
                  }}
                  onEntered={() => {
                    setIsLoading(false);
                  }}
                >
                  {getCom()}
                </CSSTransition>
              );
            }}
          </Route>
        ))}
      {!matchingRoutes && <NotFound />}
    </>
  );
};

OHIFStandaloneViewer.propTypes = {
  history: PropTypes.object.isRequired,
  user: PropTypes.object,
  setContext: PropTypes.func,
  userManager: PropTypes.object,
  location: PropTypes.object,
  setExtensions: PropTypes.func,
  userInfo: PropTypes.object,
  t: PropTypes.func,
};

const mapStateToProps = state => {
  return {
    user: state.oidc.user,
    userInfo: state.extensions.user,
    extensions: state.extensions,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setExtensions: (key, data) => dispatch(setExtensionData(key, data)),
  };
};

const ConnectedOHIFStandaloneViewer = compose(
  connect(mapStateToProps, mapDispatchToProps),
  withTranslation('Vindoc')
)(OHIFStandaloneViewer);

export default ViewerbaseDragDropContext(
  withRouter(ConnectedOHIFStandaloneViewer)
);
