import React, { Component } from 'react';
// import { OidcProvider } from 'redux-oidc';
import { I18nextProvider } from 'react-i18next';
import { ConfigProvider } from 'antd';
import vi_VN from 'antd/es/locale/vi_VN';
import en_US from 'antd/es/locale/en_US';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
// import { hot } from 'react-hot-loader/root';
import { DraggableModalProvider } from 'ant-design-draggable-modal';
import OHIFCornerstoneExtension from '@tuvm/extension-cornerstone';

import {
  SnackbarProvider,
  ModalProvider,
  DialogProvider,
  OHIFModal,
  ErrorBoundary,
} from '@tuvm/ui';

import {
  CommandsManager,
  ExtensionManager,
  ServicesManager,
  HotkeysManager,
  UINotificationService,
  UIModalService,
  UIDialogService,
  MeasurementService,
  utils,
} from '@tuvm/core';

import i18n from '@tuvm/i18n';

// TODO: This should not be here
//import './config';
import { setConfiguration } from './config';

/** Utils */
import {
  // getUserManagerForOpenIdConnectClient,
  initWebWorkers,
} from './utils/index.js';

/** Extensions */
import { GenericViewerCommands } from './appExtensions';

/** Viewer */
import OHIFStandaloneViewer from './VinDrStandaloneViewer';

/** Store */
import { getActiveContexts } from './store/layout/selectors.js';
import store from './store';

/** Contexts */
// import WhiteLabelingContext from './context/WhiteLabelingContext';
// import UserManagerContext from './context/UserManagerContext';
import { AppProvider, useAppContext, CONTEXTS } from './context/AppContext';
import PredictionPanel from '@tuvm/viewer/src/components/Report';
import { LANGUAGES } from './utils/constants';

import './App.css';
import './App.less';
import { getViewportApiKey } from './utils/helper';

/** ~~~~~~~~~~~~~ Application Setup */
const commandsManagerConfig = {
  getAppState: () => store.getState(),
  getActiveContexts: () => getActiveContexts(store.getState()),
};

/** Managers */
const commandsManager = new CommandsManager(commandsManagerConfig);
const servicesManager = new ServicesManager();
const hotkeysManager = new HotkeysManager(commandsManager, servicesManager);
let extensionManager;
/** ~~~~~~~~~~~~~ End Application Setup */

// TODO[react] Use a provider when the whole tree is React
window.store = store;

window.ohif = window.ohif || {};
window.ohif.app = {
  commandsManager,
  hotkeysManager,
  servicesManager,
  extensionManager,
};

class App extends Component {
  static propTypes = {
    config: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.shape({
        routerBasename: PropTypes.string.isRequired,
        oidc: PropTypes.array,
        extensions: PropTypes.array,
      }),
    ]).isRequired,
    defaultExtensions: PropTypes.array,
  };

  static defaultProps = {
    config: {
      showStudyList: true,
      oidc: [],
      extensions: [],
    },
    defaultExtensions: [],
  };

  _appConfig;

  constructor(props) {
    super(props);

    const { config, defaultExtensions } = props;

    const appDefaultConfig = {
      showStudyList: true,
      cornerstoneExtensionConfig: {},
      extensions: [],
      routerBasename: '/',
    };

    this._appConfig = {
      ...appDefaultConfig,
      ...(typeof config === 'function' ? config({ servicesManager }) : config),
    };

    const tempToken = getViewportApiKey();
    if (tempToken) {
      this._appConfig = {
        ...this._appConfig,
        servers: {
          ...this._appConfig.servers,
          dicomWeb: [
            {
              name: 'ORTHANC',
              wadoUriRoot:
                (process.env.BACKEND_URL || window.origin) +
                '/dicomweb/viewport/wado',
              qidoRoot:
                (process.env.BACKEND_URL || window.origin) +
                '/dicomweb/viewport/dicom-web',
              wadoRoot:
                (process.env.BACKEND_URL || window.origin) +
                '/dicomweb/viewport/dicom-web',
              aiRoot: process.env.BACKEND_URL + '/viewport',
              qidoSupportsIncludeField: true,
              imageRendering: 'wadors',
              thumbnailRendering: 'wadors',
              enableStudyLazyLoad: true,
            },
          ],
        },
      };
    }

    const {
      servers,
      hotkeys: appConfigHotkeys,
      cornerstoneExtensionConfig,
      extensions,
      // oidc,
    } = this._appConfig;

    setConfiguration(this._appConfig);

    // this.initUserManager(oidc);
    _initServices([
      UINotificationService,
      UIModalService,
      UIDialogService,
      MeasurementService,
    ]);
    _initExtensions(
      [...defaultExtensions, ...extensions],
      cornerstoneExtensionConfig,
      this._appConfig
    );

    /*
     * Must run after extension commands are registered
     * if there is no hotkeys from localStorage set up from config.
     */
    _initHotkeys(appConfigHotkeys);
    _initServers(servers);
    initWebWorkers();
  }

  render() {
    const { routerBasename } = this._appConfig;
    const {
      UINotificationService,
      UIDialogService,
      UIModalService,
    } = servicesManager.services;

    return (
      <ErrorBoundary context="App">
        <Provider store={store}>
          <AppProvider config={this._appConfig}>
            <I18nextProvider i18n={i18n}>
              <ConfigProvider
                locale={i18n?.language === LANGUAGES.US ? en_US : vi_VN}
              >
                <Router basename={routerBasename}>
                  <SnackbarProvider service={UINotificationService}>
                    <DialogProvider service={UIDialogService}>
                      <ModalProvider modal={OHIFModal} service={UIModalService}>
                        <DraggableModalProvider>
                          <OHIFStandaloneViewer />
                        </DraggableModalProvider>
                      </ModalProvider>
                    </DialogProvider>
                  </SnackbarProvider>
                </Router>
              </ConfigProvider>
            </I18nextProvider>
          </AppProvider>
        </Provider>
      </ErrorBoundary>
    );
  }
}

function _initServices(services) {
  servicesManager.registerServices(services);
}

/**
 * @param
 */
function _initExtensions(extensions, cornerstoneExtensionConfig, appConfig) {
  extensionManager = new ExtensionManager({
    commandsManager,
    servicesManager,
    appConfig,
    api: {
      contexts: CONTEXTS,
      hooks: {
        useAppContext,
      },
    },
  });

  const requiredExtensions = [
    GenericViewerCommands,
    [OHIFCornerstoneExtension, cornerstoneExtensionConfig],
  ];

  if (appConfig.disableMeasurementPanel !== true) {
    /* WARNING: MUST BE REGISTERED _AFTER_ OHIFCornerstoneExtension */
    requiredExtensions.push(PredictionPanel);
  }

  const mergedExtensions = requiredExtensions.concat(extensions);
  extensionManager.registerExtensions(mergedExtensions);
}

/**
 *
 * @param {Object} appConfigHotkeys - Default hotkeys, as defined by app config
 */
function _initHotkeys(appConfigHotkeys) {
  // TODO: Use something more resilient
  // TODO: Mozilla has a special library for this
  const userPreferredHotkeys = JSON.parse(
    localStorage.getItem('hotkey-definitions') || '{}'
  );

  // TODO: hotkeysManager.isValidDefinitionObject(/* */)
  const hasUserPreferences =
    userPreferredHotkeys && Object.keys(userPreferredHotkeys).length > 0;
  if (hasUserPreferences) {
    hotkeysManager.setHotkeys(userPreferredHotkeys);
  } else {
    hotkeysManager.setHotkeys(appConfigHotkeys);
  }

  hotkeysManager.setDefaultHotKeys(appConfigHotkeys);
}

function _initServers(servers) {
  if (servers) {
    utils.addServers(servers, store);
  }
}

export default App;
export { commandsManager, extensionManager, hotkeysManager, servicesManager };
