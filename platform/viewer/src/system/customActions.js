import { isEmpty } from 'lodash';
import api from '../services/api';
import { getExtensionData } from '../utils/helper';
import { actionSetExtensionData } from './systemActions';
import get from 'lodash/get';

export const actionGetAIResult = (params = {}) => {
  let { studyInstanceUIDs } = params;
  if (typeof studyInstanceUIDs == 'string') {
    studyInstanceUIDs = studyInstanceUIDs.split(';');
  }
  const newAIResults = {};
  const promises = [];
  const predictionPanel = getExtensionData('predictionPanel', {});
  const aiResult = getExtensionData('predictionPanel.aiResult', {});

  studyInstanceUIDs.forEach(async studyUid => {
    promises.push(
      api({
        url: `/backend/doctor/study?study_sop_id=${studyUid}`,
        method: 'GET',
      })
    );
  });

  Promise.allSettled(promises).then(responses => {
    if (responses) {
      responses.forEach((data, i) => {
        if (!isEmpty(data) && data.status == 'fulfilled') {
          data = get(data, 'value.data', {});
          newAIResults[studyInstanceUIDs[i]] = data;
        }
      });

      actionSetExtensionData('predictionPanel', {
        // ...predictionPanel,
        aiResult: {
          ...aiResult,
          ...newAIResults,
        },
      });
    }
  });
};
