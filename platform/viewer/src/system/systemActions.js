import OHIF from '@tuvm/core';
import get from 'lodash/get';
import cloneDeep from 'lodash.clonedeep';
import axios from 'axios';

import {
  CONFIG_SERVER,
  TOKEN,
  REFRESH_TOKEN,
  REALM_ID,
  PERMISSION_TOKEN,
} from '../utils/constants';
import cookie from 'js-cookie';
import { message } from 'antd';
import { getTenant } from '../utils/helper';
import keycloak from '../services/auth';

const {
  CLIENT_ID,
  AUTH_URL,
  RESPONSE_TYPE,
  STATE,
  AUDIENCE,
  BACKEND_URL,
} = CONFIG_SERVER;

export const actionSetExtensionData = (key, data) => {
  const { redux } = OHIF;
  const { setExtensionData } = redux.actions;

  window.store.dispatch(setExtensionData(key, data));
};

export const getAuthUrl = () => {
  const realmId = sessionStorage.getItem(REALM_ID);
  const url =
    AUTH_URL + `/auth/realms/${realmId}/protocol/openid-connect/token`;

  return url;
};

export const actionGetExtensionData = key => {
  const store = window.store.getState();
  const extensions = get(store, 'extensions');

  return cloneDeep(extensions[key]);
};

export const requestLogin = isRedirect => {
  const realmId = sessionStorage.getItem(REALM_ID);
  const pathAuth =
    AUTH_URL + `/auth/realms/${realmId}/protocol/openid-connect/auth`;

  let loginUrl =
    pathAuth +
    '?client_id=' +
    CLIENT_ID +
    '&response_type=' +
    RESPONSE_TYPE +
    '&state=' +
    STATE;

  if (isRedirect) {
    loginUrl += '&redirect_uri=' + window.location.origin;
    sessionStorage.setItem('redirect_uri', window.location.href);
  }

  window.location.href = encodeURI(loginUrl);
};

export const actionRefreshToken = (refreshToken = '') => {
  let requestBody = new URLSearchParams();
  requestBody.append('grant_type', 'refresh_token');
  requestBody.append('client_id', CLIENT_ID);
  requestBody.append('refresh_token', refreshToken);
  requestBody.append('redirect_uri', window.location.origin);
  const realmId = sessionStorage.getItem(REALM_ID);

  return axios({
    method: 'post',
    url: AUTH_URL + `/auth/realms/${realmId}/protocol/openid-connect/token`,
    data: requestBody,
  });
};

export const actionFetchPermissionToken = (token, scopes) => {
  let requestBody = new URLSearchParams();
  requestBody.append(
    'grant_type',
    'urn:ietf:params:oauth:grant-type:uma-ticket'
  );
  requestBody.append('audience', AUDIENCE);
  requestBody.append('permission', 'api');

  // scopes.map(it => {
  //   requestBody.append('permission', `api#${it}`);
  // });

  return axios({
    url: getAuthUrl(),
    method: 'post',
    data: requestBody,
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

// export const actionGetPermissionToken = async (token, scopes, isSlient) => {
//   try {
//     let requestBody = new URLSearchParams();
//     requestBody.append(
//       'grant_type',
//       'urn:ietf:params:oauth:grant-type:uma-ticket'
//     );
//     requestBody.append('audience', AUDIENCE);
//     // requestBody.append('permission', 'api#all');

//     scopes.map(it => {
//       requestBody.append('permission', `api#${it}`);
//     });

//     const res = await axios({
//       url: getAuthUrl(),
//       method: 'post',
//       data: requestBody,
//       headers: {
//         Authorization: `Bearer ${token}`,
//       },
//     });

//     if (res && res.data && res.data.access_token) {
//       const { data } = res;

//       if (!isSlient) {
//         window.location.href =
//           sessionStorage.getItem('redirect_uri') || location.origin;
//       }

//       cookie.set(TOKEN, data.access_token, {
//         expires: new Date((res.data.expires_in || 1800) * 1000 + Date.now()),
//       });
//       cookie.set(REFRESH_TOKEN, data.refresh_token, {
//         expires: new Date(
//           (res.data.refresh_expires_in || 1800) * 1000 + Date.now()
//         ),
//       });

//       return res;
//     }
//   } catch (error) {
//     console.log(error);
//   }
// };

export const actionFetchTokenFromCode = (code = '', sessionState = '') => {
  let requestBody = new URLSearchParams();
  requestBody.append('grant_type', 'authorization_code');
  requestBody.append('client_id', CLIENT_ID);
  requestBody.append('code', code);
  requestBody.append('session_state', sessionState);
  requestBody.append('redirect_uri', window.location.origin);

  return axios({
    method: 'post',
    url: getAuthUrl(),
    data: requestBody,
  });
};

export const actionGetTenantSetting = async () => {
  const res = await axios({
    method: 'GET',
    url: BACKEND_URL + '/backend/tenant/setting',
    // headers: {
    //   'X-TENANT-ID': 'local',
    // },
  });

  return res;
};

export const getAccountInfo = async (token = null) => {
  const tenant = getTenant();
  const r = await axios({
    baseURL: CONFIG_SERVER.BACKEND_URL,
    url: `/om/${tenant}/current_user`,
    method: 'GET',
    headers: { Authorization: `Bearer ${token || keycloak.getAccessToken()}` },
  });
  let data = get(r, 'data.data') || {};

  if (data && data.scopes) {
    actionSetExtensionData('user', data);
  }
  return data;
};

export const actionLogout = async () => {
  try {
    // cookie.remove(REFRESH_TOKEN);
    // cookie.remove(TOKEN);
    localStorage.removeItem(TOKEN);
    localStorage.removeItem(PERMISSION_TOKEN);
    localStorage.removeItem(REFRESH_TOKEN);

    let realmId = sessionStorage.getItem(REALM_ID);

    if (!realmId) {
      const res = await actionGetTenantSetting();
      if (res && res.data && res.data.realm_id) {
        realmId = res.data.realm_id;
      }
    }

    if (realmId) {
      const url =
        AUTH_URL +
        `/auth/realms/${realmId}/protocol/openid-connect/logout?redirect_uri=${window.origin}`;
      sessionStorage.removeItem(REALM_ID);
      window.location.href = url;
    } else {
      message.error('Can not get tenant info!');
    }
  } catch (error) {
    console.log(error);
  }
};
