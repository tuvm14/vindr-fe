import get from 'lodash/get';

function setup(instance) {
  instance.interceptors.request.use(
    config => {
      // const token = '';
      // if (token) {
      //   config.headers.Authorization = `Bearer ${token}`;
      // }
      // config.maxRedirects = 0;

      return config;
    },
    function(err) {
      return Promise.reject(err);
    }
  );
}

// define which cases will be redirected
const redirectCases = [301, 302, 308];
const logoutUrl = window.location.origin + '/logout';

function checkTokenExpired(instance) {
  instance.interceptors.response.use(
    response => {
      const responseUrl = get(response, 'request.responseURL');
      if (
        redirectCases.includes(response.status) ||
        responseUrl.includes('/auth/realms')
      ) {
        location.reload();
      }

      return response;
    },
    error => {
      const status = get(error, 'response.status');

      if (status && status === 401) {
        window.location.href = logoutUrl;
      }

      if (
        error.response &&
        error.response.data &&
        error.response.data.location
      ) {
        window.location = error.response.data.location;
      }

      return Promise.reject(error);
    }
  );
}

export default {
  setup,
  checkTokenExpired,
};
