import axios from 'axios';
import Qs from 'qs';
import get from 'lodash/get';
import {
  CONFIG_SERVER,
  TOKEN,
  REFRESH_TOKEN,
  X_ACCESS_KEY,
  PERMISSION_TOKEN,
} from '../utils/constants';
import { actionLogout } from '../system/systemActions';
// import cookie from 'js-cookie';
import { getViewportApiKey } from '../utils/helper';
import keycloak from './auth';

let pendingRequests = [];
let updatingToken = false;

export function onPermissionTokenFetched(access_token) {
  pendingRequests = pendingRequests.map(callback => callback(access_token));
  pendingRequests = [];
}

function addPendingRequest(callback) {
  pendingRequests.push(callback);
}

const request = axios.create();

const api = (options = {}, authAPI) => {
  let config = {
    baseURL: authAPI ? CONFIG_SERVER.AUTH_URL : CONFIG_SERVER.BACKEND_URL,
    ...options,
    paramsSerializer: params => Qs.stringify(params, { arrayFormat: 'repeat' }),
    headers: {
      Accept: '*/*',
      ...options.headers,
    },
  };
  if (keycloak.getPermissionToken()) {
    config.headers.Authorization = `Bearer ${keycloak.getPermissionToken()}`;
  }

  const tempToken = getViewportApiKey();
  if (tempToken) {
    config = {
      ...config,
      baseURL: config.baseURL.replace('api', 'viewport'),
      headers: {
        ...config.headers,
        [X_ACCESS_KEY]: tempToken,
        Authorization: '',
      },
    };
    return request(config);
  }

  if (
    !CONFIG_SERVER.DISABLE_AUTH &&
    !(keycloak && keycloak.getPermissionToken())
  ) {
    if (!updatingToken) {
      // if token is not fetching, fetch it! Then execute the pending queue.
      // console.log('Update token');
      updatingToken = true;
      keycloak.updateToken().then(() => {
        updatingToken = false;
        onPermissionTokenFetched(keycloak.getPermissionToken());
      });
    }
    // Add the request to pending queue because we don't have token!
    return new Promise(resolve => {
      addPendingRequest(access_token => {
        resolve(
          request({
            ...config,
            headers: {
              ...config.headers,
              Authorization: 'Bearer ' + access_token,
            },
          })
        );
      });
    });
  }
  return request({
    ...config,
    headers: {
      ...config.headers,
      Authorization: `Bearer ${keycloak.getPermissionToken()}`,
    },
  });
};

request.interceptors.request.use(
  config => {
    return config;
  },
  error => Promise.reject(error)
);

request.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    const originalRequest = error.config;
    const errorCode = get(error, 'response.status');
    if ((errorCode === 401 || errorCode === 403) && !originalRequest._retry) {
      // if call fail the first time, try to get another token
      // push to queue to retry request
      originalRequest._retry = true;

      if (!CONFIG_SERVER.DISABLE_AUTH && !updatingToken) {
        updatingToken = true;
        keycloak.updateToken().then(() => {
          updatingToken = false;
          onPermissionTokenFetched(keycloak.getPermissionToken());
        });
      }
      return new Promise(resolve => {
        addPendingRequest(access_token => {
          originalRequest.headers.Authorization = 'Bearer ' + access_token;
          resolve(request(originalRequest));
        });
      });
    } else {
      // if token still ok but fail to call api twice, go to no permission
      if (errorCode === 401) {
        localStorage.removeItem(TOKEN);
        localStorage.removeItem(PERMISSION_TOKEN);
        localStorage.removeItem(REFRESH_TOKEN);
        actionLogout();
      } else if (errorCode === 403) {
        // localStorage.removeItem(TOKEN);
        // localStorage.removeItem(PERMISSION_TOKEN);
        // localStorage.removeItem(REFRESH_TOKEN);
        // message.error('Permission required');
        window.location.href = '/no-permission';
      } else {
        return Promise.reject(error);
      }
    }
  }
);

export default api;
