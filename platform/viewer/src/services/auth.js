import {
  CONFIG_SERVER,
  FEATURES_KEY,
  PERMISSION_TOKEN,
  REALM_ID,
  REFRESH_TOKEN,
  TOKEN,
} from '../utils/constants';
// import cookie from 'js-cookie';
import {
  actionGetTenantSetting,
  getAccountInfo,
  actionRefreshToken,
  actionFetchTokenFromCode,
  actionFetchPermissionToken,
  actionSetExtensionData,
} from '../system/systemActions';
import { get } from 'lodash';
import {
  getViewportApiKey,
  localStorageGetWithExpiry,
  localStorageSetWithExpiry,
  parseJwt,
} from '../utils/helper';

function Keycloak(config) {
  let kc = this;
  kc._permissions = [];
  kc._features = [];
  kc._userInfo = null;
  kc._initiated = false;

  // getter

  kc.getRealmId = function() {
    return sessionStorage.getItem(REALM_ID);
  };

  kc.getAccessToken = function() {
    return localStorageGetWithExpiry(TOKEN);
  };

  kc.getRefreshToken = function() {
    return localStorageGetWithExpiry(REFRESH_TOKEN);
  };

  kc.getPermissionToken = function() {
    return localStorageGetWithExpiry(PERMISSION_TOKEN);
  };

  kc.getPermissions = function() {
    return kc._permissions;
  };

  kc.getFeatures = function() {
    return JSON.parse(sessionStorage.getItem(FEATURES_KEY));
  };

  kc.getUserInfo = function() {
    return kc._userInfo;
  };

  // setter

  kc.setRealmId = function(reslmId) {
    return sessionStorage.setItem(REALM_ID, reslmId);
  };

  kc.setAccessToken = function(token, expiresIn) {
    localStorageSetWithExpiry(
      TOKEN,
      token,
      new Date((expiresIn || 1800) * 1000 + Date.now()).getTime()
    );
  };

  kc.setRefreshToken = function(token, expiresIn) {
    localStorageSetWithExpiry(
      REFRESH_TOKEN,
      token,
      new Date((expiresIn || 1800) * 1000 + Date.now()).getTime()
    );
  };

  kc.setPermissionToken = function(token, expiresIn) {
    localStorageSetWithExpiry(
      PERMISSION_TOKEN,
      token,
      new Date((expiresIn || 1800) * 1000 + Date.now()).getTime()
    );
  };

  kc.setPermissions = function(permissions) {
    kc._permissions = permissions;
  };

  kc.setFeatures = function(features) {
    kc._features = features;
    return sessionStorage.setItem(FEATURES_KEY, JSON.stringify(features));
  };

  kc.setUserInfo = function(userInfo) {
    kc._userInfo = userInfo;
  };

  // helper
  function getCodeAndState() {
    const urlParams = new URLSearchParams(window.location.search);
    const code = urlParams.get('code');
    const sessionState = urlParams.get('session_state');
    return { code, sessionState };
  }

  kc.hasAllOfPerm = perm_list => {
    if (CONFIG_SERVER.DISABLE_AUTH) {
      return true;
    }
    const permissions = kc.getPermissions();
    if (!permissions || !permissions.length) return false;
    for (let perm of perm_list) {
      if (typeof perm === 'object') {
        // if perm is object, check all perm in object
        const list = Object.values(perm);
        const check = kc.hasAllOfPerm(list);
        if (!check) return false;
      } else if (!permissions.includes(perm)) {
        return false;
      }
    }

    console.log('NO_PERM ', perm_list);

    return true;
  };

  kc.hasOneOfPerm = perm_list => {
    if (CONFIG_SERVER.DISABLE_AUTH) {
      return true;
    }
    const permissions = kc.getPermissions();
    if (!perm_list.length) return true;
    if (!permissions || !permissions.length) return false;
    for (let perm of perm_list) {
      if (typeof perm === 'object') {
        // if perm is object, check all perm in object
        const list = Object.values(perm);
        const check = kc.hasAllOfPerm(list);
        if (check) return true;
      } else if (permissions.includes(perm)) {
        return true;
      }
    }

    console.log('NO_PERM ', perm_list);

    return false;
  };

  kc.hasFeature = feature => {
    if (!feature) return true;
    const features = kc.getFeatures();
    if (!features || !features.length) return false;
    return features.includes(feature);
  };

  kc.init = async () => {
    const apiKey = getViewportApiKey();
    if (apiKey) return;
    let reamId = sessionStorage.getItem(REALM_ID);

    if (!reamId) {
      const res = await actionGetTenantSetting();
      if (res && res.data && res.data.realm_id) {
        reamId = res.data.realm_id;
        kc.setRealmId(res.data.realm_id);
      }
      if (res && res.data && res.data.features) {
        kc.setFeatures(res.data.features);
      }
    }

    kc.realmId = reamId;

    kc.authUrl = CONFIG_SERVER.AUTH_URL + '/auth';
    kc.clientId = CONFIG_SERVER.CLIENT_ID;

    kc.updateToken()
      .then(() => {
        console.log('AUTH INITIATED');
        kc._initiated = true;
      })
      .catch(err => {
        console.log('AUTH INIT FAILED: ', err);
      });
  };

  kc.requestLogin = isRedirect => {
    console.log('REQUEST LOGIN');
    if (!kc.getRealmId()) {
      console.log('REALM NOT FOUND');
      return;
    }
    const pathAuth =
      CONFIG_SERVER.AUTH_URL +
      `/auth/realms/${kc.getRealmId()}/protocol/openid-connect/auth`;

    let loginUrl =
      pathAuth +
      '?client_id=' +
      kc.clientId +
      '&response_type=' +
      CONFIG_SERVER.RESPONSE_TYPE +
      '&state=' +
      CONFIG_SERVER.STATE;

    if (isRedirect) {
      loginUrl += '&redirect_uri=' + window.location.origin;
      sessionStorage.setItem('redirect_uri', window.location.href);
    }

    window.location.href = encodeURI(loginUrl);
  };

  kc.updateToken = () => {
    return new Promise(async (resolve, reject) => {
      try {
        if (!kc.getRefreshToken()) {
          const { code, sessionState } = getCodeAndState();
          if (code) {
            const res = await actionFetchTokenFromCode(code, sessionState);
            if (res && res.data && res.data.access_token) {
              kc.setAccessToken(res.data.access_token, res.data.expires_in);
              kc.setRefreshToken(
                res.data.refresh_token,
                res.data.refresh_expires_in
              );
            } else {
              return reject('AUTH FAILED: Fetch token from code failed');
            }
          } else {
            kc.requestLogin(true);
            return reject('AUTH FAILED: No code');
          }
        } else {
          const res = await actionRefreshToken(kc.getRefreshToken());
          if (res && res.data && res.data.access_token) {
            kc.setAccessToken(res.data.access_token, res.data.expires_in);
            kc.setRefreshToken(
              res.data.refresh_token,
              res.data.refresh_expires_in
            );
          } else {
            return reject('AUTH FAILED: Refresh token failed');
          }
        }

        const user = await getAccountInfo(kc.getAccessToken());
        if (user && user.scopes) {
          // kc.setPermissions(user.scopes);
          const res = await actionFetchPermissionToken(
            kc.getAccessToken(),
            user.scopes
          );
          if (res && res.data && res.data.access_token) {
            kc.setPermissionToken(res.data.access_token, res.data.expires_in);
            kc.setRefreshToken(
              res.data.refresh_token,
              res.data.refresh_expires_in
            );
            const parseInfo = parseJwt(res.data.access_token);
            const permissions =
              get(parseInfo, 'authorization.permissions[0].scopes') || [];
            kc.setPermissions(permissions);
            actionSetExtensionData('user', { ...user, scopes: permissions });
          } else {
            return reject('AUTH FAILED: Fetch permission token failed');
          }

          return resolve(kc.getPermissionToken());
        } else {
          return reject('AUTH FAILED: No user or user scope');
        }
      } catch (error) {
        // Not sure if need to logout here
        localStorage.removeItem(TOKEN);
        localStorage.removeItem(REFRESH_TOKEN);
        localStorage.removeItem(PERMISSION_TOKEN);
        return reject(error);
      }
    });
  };
}

const keycloak = new Keycloak();
if (!CONFIG_SERVER.DISABLE_AUTH) {
  keycloak.init();
}

export default keycloak;
