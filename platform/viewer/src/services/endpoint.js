export const API = process.env.AI_API_URL;

export const workspaceEndpoint = `${API}/workspace`;
export const workgroupEndpoint = `${workspaceEndpoint}/workgroups`;
export const assigneeEndpoint = `${workspaceEndpoint}/joblist/assignees`;
export const workgroupStudiesEndpoint = `${workspaceEndpoint}/joblist/assigned-workgroups`;

export const getWorklistAssigned = studyInstanceUIDs => {
  return `${assigneeEndpoint}?study_instance_uid=${studyInstanceUIDs}`;
};

export const workgroupsOfStudyEndpoint = studyInstanceUIDs => {
  return `${workgroupStudiesEndpoint}?study_instance_uid=${studyInstanceUIDs}`;
};

export const userInfoEndpoint = () => {
  return 'workspace/userinfo/current';
};

export const userAssignedWorkGroupEndpoint = workgroupId => {
  return `${workgroupEndpoint}/${workgroupId}/users`;
};
