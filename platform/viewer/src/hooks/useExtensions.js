import { redux } from '@tuvm/core';
import { useDispatch, useSelector } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import { useCallback } from 'react';

export const useExtensions = () => {
  const extensionsState = useSelector(state => state && state.extensions);
  const dispatch = useDispatch();

  const setExtensionData = useCallback(
    (key, data) => {
      dispatch(redux.actions.setExtensionData(key, data));
    },
    [dispatch]
  );

  const getExtensionsData = useCallback(
    key => {
      return cloneDeep(extensionsState[key]);
    },
    [extensionsState]
  );

  return {
    setExtensionData,
    getExtensionsData,
  };
};
