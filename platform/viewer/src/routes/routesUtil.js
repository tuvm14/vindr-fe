import asyncComponent from '../components/AsyncComponent.js';
import { NEW_SCOPES, SCOPES } from '../utils/constants';
import OHIF from '@tuvm/core';
const { urlUtil: UrlUtil } = OHIF.utils;

// Dynamic Import Routes (CodeSplitting)
const IHEInvokeImageDisplay = asyncComponent(() =>
  import(
    /* webpackChunkName: "IHEInvokeImageDisplay" */ './IHEInvokeImageDisplay.js'
  )
);
const ViewerRouting = asyncComponent(() =>
  import(/* webpackChunkName: "ViewerRouting" */ './ViewerRouting.js')
);
const ExtendViewerRouting = asyncComponent(() =>
  import(/* webpackChunkName: "ViewerRouting" */ './ExtendViewerRouting.js')
);

const StudyListRouting = asyncComponent(() =>
  import(/* webpackChunkName: "StudyListRouting" */ '../pages/studyList')
);
const StandaloneRouting = asyncComponent(() =>
  import(
    /* webpackChunkName: "ConnectedStandaloneRouting" */ './ConnectedStandaloneRouting.js'
  )
);
const ViewerLocalFileData = asyncComponent(() =>
  import(
    /* webpackChunkName: "ViewerLocalFileData" */ '../pages/ViewerLocalFileData'
  )
);

const ViewerDocument = asyncComponent(() =>
  import(/* webpackChunkName: "ViewerDocument" */ '../pages/ViewerDocument')
);

const HomeRouting = asyncComponent(() =>
  import(/* webpackChunkName: "HomeRouting" */ './HomeRouting.js')
);

const NoPermissionRouting = asyncComponent(() =>
  import(/* webpackChunkName: "NoPermission" */ '../pages/NoPermission')
);

const TechnicianRouting = asyncComponent(() =>
  import(/* webpackChunkName: "technician" */ '../pages/technician')
);

const NonDicomRouting = asyncComponent(() =>
  import(/* webpackChunkName: "nonDicom" */ '../pages/nonDicom')
);

const SystemManagementRouting = asyncComponent(() =>
  import(/* webpackChunkName: "technician" */ '../pages/systemManagement')
);

const Dashboard = asyncComponent(() =>
  import(/* webpackChunkName: "dashboard" */ '../pages/dashboard')
);

const reload = () => window.location.reload();

const ROUTES_DEF = {
  default: {
    root: {
      path: '/',
      component: HomeRouting,
    },
    viewer: {
      path: '/viewer/:studyInstanceUIDs',
      component: ViewerRouting,
    },
    extendViewer: {
      path: '/viewer',
      component: ExtendViewerRouting,
    },
    standaloneViewer: {
      path: '/standaloneviewer',
      component: StandaloneRouting,
    },
    list: {
      path: ['/studylist'],
      component: StudyListRouting,
      condition: appConfig => {
        return appConfig.showStudyList;
      },
      permissions: [
        SCOPES.studyView,
        SCOPES.studyUpload,
        SCOPES.apiAll,
        NEW_SCOPES.worklist.studylist.view,
      ],
    },
    local: {
      path: '/local',
      component: ViewerLocalFileData,
    },
    noPermission: {
      path: '/no-permission',
      component: NoPermissionRouting,
    },
    document: {
      path: '/document',
      component: ViewerDocument,
    },
    IHEInvokeImageDisplay: {
      path: '/IHEInvokeImageDisplay',
      component: IHEInvokeImageDisplay,
    },
    technician: {
      path: '/technician/:page',
      component: TechnicianRouting,
      permissions: [
        SCOPES.worklistAdmin,
        SCOPES.globalAdmin,
        NEW_SCOPES.order.view,
      ],
    },
    nonDicom: {
      path: '/nondicom/:page',
      component: NonDicomRouting,
      permissions: [
        SCOPES.worklistAdmin,
        SCOPES.globalAdmin,
        NEW_SCOPES.nondicom.create,
      ],
    },
    systemManagement: {
      path: '/system/:page',
      component: SystemManagementRouting,
      permissions: [
        SCOPES.organizationAdmin,
        SCOPES.globalAdmin,
        NEW_SCOPES.administration.institute.view,
        NEW_SCOPES.administration.role.view,
        NEW_SCOPES.administration.user.view,
        NEW_SCOPES.administration.report_template.view,
        NEW_SCOPES.administration.report_layout.view,
        NEW_SCOPES.administration.filter.view,
      ],
    },
    dashboard: {
      path: '/dashboard',
      component: Dashboard,
      permissions: [
        SCOPES.organizationAdmin,
        SCOPES.globalAdmin,
        NEW_SCOPES.dashboard.view,
      ],
    },
  },
};

const getRoutes = appConfig => {
  const routes = [];
  for (let keyConfig in ROUTES_DEF) {
    const routesConfig = ROUTES_DEF[keyConfig];

    for (let routeKey in routesConfig) {
      const route = routesConfig[routeKey];
      const validRoute =
        typeof route.condition === 'function'
          ? route.condition(appConfig)
          : true;

      if (validRoute) {
        routes.push({
          path: route.path,
          Component: route.component,
          permissions: route.permissions,
        });
      }
    }
  }

  return routes;
};

const parsePath = (path, server, params) => {
  let _path = path;
  const _paramsCopy = Object.assign({}, server, params);

  for (let key in _paramsCopy) {
    _path = UrlUtil.paramString.replaceParam(_path, key, _paramsCopy[key]);
  }

  return _path;
};

const parseViewerPath = (appConfig = {}, server = {}, params) => {
  let viewerPath = ROUTES_DEF.default.viewer.path;

  return parsePath(viewerPath, server, params);
};

const parseStudyListPath = (appConfig = {}, server = {}, params) => {
  let studyListPath = ROUTES_DEF.default.list.path;

  return parsePath(studyListPath, server, params);
};

export { getRoutes, parseViewerPath, parseStudyListPath, reload };
