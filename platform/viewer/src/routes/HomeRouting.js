import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { NEW_SCOPES, SCOPES } from '../utils/constants';
import { useSelector } from 'react-redux';
import { FullScreenLoading } from '../components/Customize/FullScreenLoading';
import keycloak from '../services/auth';

export default function HomeRouting() {
  const history = useHistory();
  const user = useSelector(state => state.extensions.user);

  useEffect(() => {
    if (
      keycloak.hasOneOfPerm([
        SCOPES.studyView,
        SCOPES.studyUpload,
        NEW_SCOPES.worklist.studylist.view,
      ])
    ) {
      history.push('/studylist');
      return;
    } else if (
      keycloak.hasOneOfPerm([SCOPES.worklistAdmin, NEW_SCOPES.order.match])
    ) {
      history.push('/technician/worklist');
      return;
    } else if (
      keycloak.hasOneOfPerm([
        SCOPES.organizationAdmin,
        NEW_SCOPES.administration.institute.view,
        NEW_SCOPES.administration.role.view,
        NEW_SCOPES.administration.user.view,
        NEW_SCOPES.administration.report_template.view,
        NEW_SCOPES.administration.report_layout.view,
        NEW_SCOPES.administration.filter.view,
      ])
    ) {
      if (
        keycloak.hasOneOfPerm([
          SCOPES.organizationAdmin,
          NEW_SCOPES.administration.institute.view,
        ])
      )
        history.push('/system/institution');
      else if (keycloak.hasOneOfPerm([NEW_SCOPES.administration.role.view]))
        history.push('/system/role');
      else if (keycloak.hasOneOfPerm([NEW_SCOPES.administration.user.view]))
        history.push('/system/account');
      else if (
        keycloak.hasOneOfPerm([NEW_SCOPES.administration.report_template.view])
      )
        history.push('/system/template');
      else if (
        keycloak.hasOneOfPerm([NEW_SCOPES.administration.report_layout.view])
      )
        history.push('/system/layout');
      else if (keycloak.hasOneOfPerm([NEW_SCOPES.administration.filter.view]))
        history.push('/system/filter');
      return;
    } else {
      if (keycloak._initiated) {
        history.push('/no-permission');
      }
    }
  }, [user]);

  return <FullScreenLoading />;
}
