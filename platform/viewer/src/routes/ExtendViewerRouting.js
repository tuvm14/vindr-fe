import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { utils } from '@tuvm/core';
//
import ConnectedViewerRetrieveStudyData from '../pages/viewer/ViewerRetrieveStudyData';
import useServer from '../customHooks/useServer';
import NotFound from '../pages/NotFound';
import { FullScreenLoading } from '../components/Customize/FullScreenLoading';
import { actionGetStudiesFromParams } from '../pages/studyList/actions';
const { urlUtil: UrlUtil } = utils;

/**
 * Get array of seriesUIDs from param or from queryString
 * @param {*} seriesInstanceUIDs
 * @param {*} location
 */
const getSeriesInstanceUIDs = (seriesInstanceUIDs, routeLocation) => {
  const queryFilters = UrlUtil.queryString.getQueryFilters(routeLocation);
  const querySeriesUIDs = queryFilters && queryFilters['seriesInstanceUID'];
  const _seriesInstanceUIDs = seriesInstanceUIDs || querySeriesUIDs;

  return UrlUtil.paramString.parseParam(_seriesInstanceUIDs);
};

function ExtendViewerRouting({ match: routeMatch, location: routeLocation }) {
  const {
    project,
    location,
    dataset,
    dicomStore,
    studyInstanceUIDs,
    seriesInstanceUIDs,
  } = routeMatch.params;

  const [studyUIDs, setStudyUIDs] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState('');

  const showError = () => {
    setIsLoading(false);
    setStudyUIDs(null);
    setError('Study not found');
  };

  const fetchStudies = async search => {
    try {
      const data = await actionGetStudiesFromParams(search);
      if (data && data.data && data.data.length > 0) {
        setStudyUIDs(data.data);
        setIsLoading(false);
      } else {
        showError();
      }
    } catch (err) {
      showError();
    }
  };

  useEffect(() => {
    if (routeLocation && routeLocation.search) {
      fetchStudies(routeLocation.search);
    } else {
      showError();
    }
  }, []);

  const server = useServer({ project, location, dataset, dicomStore });
  // const studyUIDs = UrlUtil.paramString.parseParam(studyInstanceUIDs);
  const seriesUIDs = getSeriesInstanceUIDs(seriesInstanceUIDs, routeLocation);

  if (isLoading) {
    return <FullScreenLoading />;
  } else if (server && studyUIDs) {
    return (
      <ConnectedViewerRetrieveStudyData
        studyInstanceUIDs={studyUIDs}
        seriesInstanceUIDs={seriesUIDs}
      />
    );
  } else {
    return <NotFound message={error} showGoBackButton={false} />;
  }
}

ExtendViewerRouting.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      studyInstanceUIDs: PropTypes.string,
      seriesInstanceUIDs: PropTypes.string,
      dataset: PropTypes.string,
      dicomStore: PropTypes.string,
      location: PropTypes.string,
      project: PropTypes.string,
    }),
  }),
  location: PropTypes.any,
};

export default ExtendViewerRouting;
