import React, { useEffect, useState, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { Menu, Dropdown, Checkbox } from 'antd';
import { Icon, Tooltip } from '@tuvm/ui';
import cornerstone from 'cornerstone-core';
import cornerstoneTools from '@vindr/vindr-tools';
import { get, isEmpty } from 'lodash';
import { useTranslation } from 'react-i18next';
import synchronizeManager from './synchronizeManager';
import './SeriesSynchronization.styl';

const SYNC_TYPE = {
  SYNC_REFERENCE_LINE: 'SYNC_REFERENCE_LINE',
  SYNC_SLICE_POSITION: 'SYNC_SLICE_POSITION',
  SYNC_ZOOM_PAN: 'SYNC_ZOOM_PAN',
  SYNC_WINDOW_SETTINGS: 'SYNC_WINDOW_SETTINGS',
};

function SeriesSynchronization() {
  const { t } = useTranslation('Vindoc');
  const [isDisableSync, setDisableSync] = useState(true);
  const viewports = useSelector(state => state.viewports);
  const { layout, activeViewportIndex } = viewports;
  const [isPopup, setIsPopup] = useState(false);

  const [syncOptions, setSyncOptions] = useState([
    {
      id: 0,
      label: t('Reference Line'),
      value: SYNC_TYPE.SYNC_REFERENCE_LINE,
      selected: false,
    },
    {
      id: 1,
      label: t('Synchronize Slice Position'),
      value: SYNC_TYPE.SYNC_SLICE_POSITION,
      selected: false,
    },
    {
      id: 2,
      label: t('Synchronize Zoom & Pan'),
      value: SYNC_TYPE.SYNC_ZOOM_PAN,
      selected: false,
    },
    {
      id: 3,
      label: t('Synchronize Window Settings'),
      value: SYNC_TYPE.SYNC_WINDOW_SETTINGS,
      selected: false,
    },
  ]);

  const disableReferenceLineTool = () => {
    cornerstoneTools.setToolDisabled('ReferenceLines');
  };

  const resetSync = () => {
    if (synchronizeManager.get(SYNC_TYPE.SYNC_REFERENCE_LINE)) {
      disableReferenceLineTool();
    }
    synchronizeManager.destroyAll();
    setDisableSync(true);
    const tempSyncOptions = syncOptions.map(it => ({
      ...it,
      selected: false,
    }));
    setSyncOptions([...tempSyncOptions]);
  };

  const enableNewElement = useCallback(evt => {
    try {
      const element = evt.detail.element;
      const isViewportElm = element.className === 'viewport-element';
      if (!isViewportElm) return;
      const syncData = synchronizeManager.getAll();
      if (syncData.size) {
        syncData.forEach((sync, key) => {
          if (key === SYNC_TYPE.SYNC_REFERENCE_LINE) {
            if (sync && sync.addTarget) {
              sync.addTarget(element);
              cornerstoneTools.setToolEnabledForElement(
                element,
                'ReferenceLines',
                { synchronizationContext: sync }
              );
            }
          } else {
            if (sync && sync.add) {
              sync.add(element);
            }
          }
        });
      }
    } catch (error) {
      console.warn(error);
    }
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    cornerstone.events.addEventListener(
      cornerstone.EVENTS.ELEMENT_ENABLED,
      enableNewElement
    );
    window.synchronizeManager = synchronizeManager;
    return () => {
      synchronizeManager.destroyAll();
      cornerstone.events.removeEventListener(
        cornerstone.EVENTS.ELEMENT_ENABLED,
        enableNewElement
      );
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    const layoutVp = get(layout, 'viewports') || [];
    const isMprMode = layoutVp.some(it => {
      return typeof it === 'object' && it.hasOwnProperty('vtk');
    });

    if (isMprMode) {
      if (!isDisableSync) {
        resetSync();
      }
    } else {
      if (layoutVp.length > 0 && !isEmpty(layoutVp[0])) {
        if (!isDisableSync && layoutVp.length === 1) {
          resetSync();
        } else if (isDisableSync && layoutVp.length > 1) {
          setDisableSync(false);
        }
      }
    }
    // eslint-disable-next-line
  }, [layout]);

  useEffect(() => {
    try {
      const syncLine = synchronizeManager.get(SYNC_TYPE.SYNC_REFERENCE_LINE);
      if (syncLine) {
        const enabledElements = cornerstone.getEnabledElements() || [];
        const currentActiveElm = enabledElements[activeViewportIndex];
        const { element } = currentActiveElm || {};
        const sourceElm = syncLine.getSourceElements() || [];
        if (sourceElm[0]) {
          syncLine.removeSource(sourceElm[0]);
        }
        syncLine.addSource(element);
      }
    } catch (error) {
      console.log(error);
    }
  }, [activeViewportIndex]);

  const actionSetSyncMode = (elms = [], syncType, eventType, synchronizer) => {
    const sync = new cornerstoneTools.Synchronizer(eventType, synchronizer);
    const currentActiveElm = elms[activeViewportIndex];
    const { element } = currentActiveElm || {};

    elms.forEach(it => {
      if (syncType === SYNC_TYPE.SYNC_REFERENCE_LINE) {
        if (element === it.element) {
          sync.addSource(it.element);
        }
        sync.addTarget(it.element);
        cornerstoneTools.setToolEnabledForElement(
          it.element,
          'ReferenceLines',
          {
            synchronizationContext: sync,
          }
        );
      } else {
        sync.add(it.element);
      }
    });

    synchronizeManager.add(syncType, sync);
  };

  const handleSelectSyncItem = item => {
    if (isDisableSync) return;
    try {
      const elms = cornerstone.getEnabledElements() || [];
      if (elms.length < 2) return;

      const tempSyncOptions = syncOptions;
      const isSelected = !tempSyncOptions[item.id].selected;
      tempSyncOptions[item.id].selected = isSelected;
      setSyncOptions([...tempSyncOptions]);

      if (!isSelected) {
        synchronizeManager.destroy(item.value);
        if (item.value === SYNC_TYPE.SYNC_REFERENCE_LINE) {
          disableReferenceLineTool();
        }
      } else {
        switch (item.value) {
          case SYNC_TYPE.SYNC_REFERENCE_LINE:
            actionSetSyncMode(
              elms,
              item.value,
              cornerstone.EVENTS.NEW_IMAGE,
              cornerstoneTools.updateImageSynchronizer
            );
            break;
          case SYNC_TYPE.SYNC_SLICE_POSITION:
            actionSetSyncMode(
              elms,
              item.value,
              cornerstone.EVENTS.NEW_IMAGE,
              cornerstoneTools.stackImagePositionSynchronizer
            );
            break;
          case SYNC_TYPE.SYNC_ZOOM_PAN:
            actionSetSyncMode(
              elms,
              item.value,
              cornerstone.EVENTS.IMAGE_RENDERED,
              cornerstoneTools.panZoomSynchronizer
            );
            break;
          case SYNC_TYPE.SYNC_WINDOW_SETTINGS:
            actionSetSyncMode(
              elms,
              item.value,
              cornerstone.EVENTS.IMAGE_RENDERED,
              cornerstoneTools.wwwcSynchronizer
            );
            break;
          default:
            break;
        }
      }
    } catch (error) {
      console.warn(error);
    }
  };

  const syncMenu = (
    <Menu className="sync-options">
      {syncOptions.map(item => (
        <Menu.Item key={item.id} disabled={isDisableSync} className="sync-item">
          <Checkbox
            disabled={isDisableSync}
            onClick={() => handleSelectSyncItem(item)}
            checked={item.selected}
            className="sync-checkbox-item"
          >
            {item.label || ''}
          </Checkbox>
        </Menu.Item>
      ))}
    </Menu>
  );

  const handleChangeVisible = flag => {
    setIsPopup(flag);
  };

  return (
    <Dropdown
      overlay={syncMenu}
      trigger={['click']}
      visible={isPopup}
      onVisibleChange={handleChangeVisible}
      overlayClassName="dropdown-sync"
    >
      <Tooltip
        title={t('Series synchronization')}
        overlayClassName="toolbar-tooltip"
      >
        <div className="toolbar-button">
          <Icon name="insertLink" style={{ height: 24, width: 24 }} />
          <Icon name="caret-down" className="expand-caret" />
          <div className="toolbar-label">{t('Series sync')}</div>
        </div>
      </Tooltip>
    </Dropdown>
  );
}

export default SeriesSynchronization;
