const syncMap = new Map();

function add(key, value) {
  syncMap.set(key, value);
}

function get(key) {
  return syncMap.get(key);
}
function getAll() {
  return syncMap;
}

function destroy(key) {
  const sync = get(key);
  if (!sync) return;
  if (sync && sync.destroy) {
    sync.destroy();
  }
  syncMap.delete(key);
}

function destroyAll() {
  if (!syncMap.size) return;
  syncMap.forEach(sync => {
    if (sync && sync.destroy) {
      sync.destroy();
    }
  });
  syncMap.clear();
}

export default {
  add,
  get,
  getAll,
  destroy,
  destroyAll,
};
