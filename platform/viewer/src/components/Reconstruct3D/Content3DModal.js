import React, { useEffect, useState, useRef } from 'react';
import { connect, useSelector } from 'react-redux';
import { Modal, Select, Slider, Row, Col, Checkbox, Spin } from 'antd';
import { getImageData, loadImageData } from '@vindr/vindr-vtkjs-viewport';
import OHIF from '@tuvm/core';
import cornerstone from 'cornerstone-core';
import vtkVolume from 'vtk.js/Sources/Rendering/Core/Volume';
import vtkVolumeMapper from 'vtk.js/Sources/Rendering/Core/VolumeMapper';

// import 'vtk.js/Sources/favicon';
import vtkColorTransferFunction from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction';
import GenericRenderWindow from 'vtk.js/Sources/Rendering/Misc/GenericRenderWindow';
import vtkPiecewiseFunction from 'vtk.js/Sources/Common/DataModel/PiecewiseFunction';
// import vtkPiecewiseGaussianWidget from 'vtk.js/Sources/Interaction/Widgets/PiecewiseGaussianWidget';

import vtkInteractorStyleManipulator from 'vtk.js/Sources/Interaction/Style/InteractorStyleManipulator';
import vtkMouseCameraTrackballRotateManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballRotateManipulator';
import vtkMouseCameraTrackballPanManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballPanManipulator';
import vtkMouseCameraTrackballZoomManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballZoomManipulator';

import presets from './presets.js';

const { StackManager } = OHIF.utils;

const PROPERTY_TYPE = {
  QUALITY: 1,
  SHADE: 2,
  SAMPLE_DISTANCE: 3,
  RANGE: 4,
  AMBIENT: 5,
  DIFFUSE: 6,
  SPECULAR: 7,
  SPECULAR_POWER: 8,
  OPACITY_DISTANCE: 9,
};

let renderWindow;
let oglrw;
let volumeActor;
let volumeMapper;
let ofun;
let cfun;
let widget;

let timeoutRender;
const defaultPreset = 10;

function vtkDefaultCameraInteractor() {
  const interactorStyle = vtkInteractorStyleManipulator.newInstance();

  const rotateManipulator = vtkMouseCameraTrackballRotateManipulator.newInstance();
  rotateManipulator.setButton(1); // Left Button
  interactorStyle.addMouseManipulator(rotateManipulator);

  const zoomManipulatorScroll = vtkMouseCameraTrackballZoomManipulator.newInstance();
  zoomManipulatorScroll.setDragEnabled(false);
  zoomManipulatorScroll.setScrollEnabled(true);
  interactorStyle.addMouseManipulator(zoomManipulatorScroll);

  const zoomManipulatorRightMouse = vtkMouseCameraTrackballZoomManipulator.newInstance();
  zoomManipulatorRightMouse.setButton(3); // Right button
  interactorStyle.addMouseManipulator(zoomManipulatorRightMouse);

  const panManipulatorMiddleButton = vtkMouseCameraTrackballPanManipulator.newInstance();
  panManipulatorMiddleButton.setButton(2); // Middle Button
  interactorStyle.addMouseManipulator(panManipulatorMiddleButton);

  const panManipulatorLeftMouse = vtkMouseCameraTrackballPanManipulator.newInstance();
  panManipulatorLeftMouse.setButton(1); // Left button
  panManipulatorLeftMouse.setShift(true);
  interactorStyle.addMouseManipulator(panManipulatorLeftMouse);

  return interactorStyle;
}

const Content3DModal = props => {
  const { studies = [], isModalVisible, onCancel } = props;
  const viewports = useSelector(state => state.viewports);
  const { viewportSpecificData, activeViewportIndex = 0 } = viewports || {};
  const activeViewport = viewportSpecificData[activeViewportIndex] || {};

  const volumeContainer = useRef();
  const volumeControl = useRef();

  const [isLoaded, setIsLoaded] = useState(false);
  const [percentComplete, setPercentComplete] = useState(0);
  const [sampleDistance, setSampleDistance] = useState(1);
  const [opacityDistance, setOpacityDistance] = useState(2.5);
  const [newLower, setLower] = useState(0);
  const [newUpper, setUpper] = useState(1);
  const [shade, setShade] = useState(true);
  const [ambient, setAmbient] = useState(0);
  const [diffuse, setDiffuse] = useState(0);
  const [specular, setSpecular] = useState(0);
  const [specularPower, setSpecularPower] = useState(0);

  useEffect(() => {
    if (
      isModalVisible &&
      viewportSpecificData &&
      studies.length > 0 &&
      !isLoaded
    ) {
      setStateFromProps(studies);
    }
  }, [viewportSpecificData, studies, isLoaded, isModalVisible]);

  useEffect(() => {
    if (isLoaded) {
      renderWindow && renderWindow.render();
    }
  }, [isLoaded]);

  useEffect(() => {
    return () => {
      renderWindow = null;
      oglrw = null;
      volumeActor = null;
      volumeMapper = null;
      ofun = null;
      cfun = null;
      widget = null;
    };
  }, []);

  const getCornerstoneStack = (
    studies,
    StudyInstanceUID,
    displaySetInstanceUID,
    SOPInstanceUID,
    frameIndex
  ) => {
    // Create shortcut to displaySet
    const study = studies.find(
      study => study.StudyInstanceUID === StudyInstanceUID
    );

    const displaySet = study.displaySets.find(set => {
      return set.displaySetInstanceUID === displaySetInstanceUID;
    });

    // Get stack from Stack Manager
    const storedStack = StackManager.findOrCreateStack(study, displaySet);

    // Clone the stack here so we don't mutate it
    const stack = Object.assign({}, storedStack);

    if (frameIndex !== undefined) {
      stack.currentImageIdIndex = frameIndex;
    } else if (SOPInstanceUID) {
      const index = stack.imageIds.findIndex(imageId => {
        const imageIdSOPInstanceUID = cornerstone.metaData.get(
          'SOPInstanceUID',
          imageId
        );

        return imageIdSOPInstanceUID === SOPInstanceUID;
      });

      if (index > -1) {
        stack.currentImageIdIndex = index;
      }
    } else {
      stack.currentImageIdIndex = 0;
    }

    return stack;
  };

  const getViewportData = (
    studies,
    StudyInstanceUID,
    displaySetInstanceUID,
    SOPClassUID,
    SOPInstanceUID,
    frameIndex
  ) => {
    const stack = getCornerstoneStack(
      studies,
      StudyInstanceUID,
      displaySetInstanceUID,
      SOPClassUID,
      SOPInstanceUID,
      frameIndex
    );

    const imageDataObject = getImageData(stack.imageIds, displaySetInstanceUID);

    return {
      imageDataObject,
    };
  };

  const getOrCreateVolume = (imageDataObject, displaySetInstanceUID) => {
    const { vtkImageData, imageMetaData0 } = imageDataObject;
    const {
      windowWidth: WindowWidth,
      windowCenter: WindowCenter,
      modality: Modality,
    } = imageMetaData0;

    const { lower, upper } = _getRangeFromWindowLevels(
      WindowWidth,
      WindowCenter,
      Modality
    );
    volumeActor = vtkVolume.newInstance();
    volumeMapper = vtkVolumeMapper.newInstance();

    volumeActor.setMapper(volumeMapper);
    volumeMapper.setInputData(vtkImageData);

    volumeActor
      .getProperty()
      .getRGBTransferFunction(0)
      .setRange(lower, upper);

    const spacing = vtkImageData.getSpacing();
    const sampleDistance = (spacing[0] + spacing[1] + spacing[2]) / 6;

    setSampleDistance(sampleDistance);

    volumeMapper.setSampleDistance(sampleDistance);

    // Be generous to surpress warnings, as the logging really hurts performance.
    // TODO: maybe we should auto adjust samples to 1000.
    // volumeMapper.setMaximumSamplesPerRay(4000);

    return volumeActor;
  };

  const setStateFromProps = studies => {
    const {
      StudyInstanceUID,
      displaySetInstanceUID,
      sopClassUIDs,
      SOPInstanceUID,
      frameIndex,
    } = activeViewport;

    if (sopClassUIDs.length > 1) {
      console.warn(
        'More than one SOPClassUID in the same series is not yet supported.'
      );
    }

    const { imageDataObject } = getViewportData(
      studies,
      StudyInstanceUID,
      displaySetInstanceUID,
      SOPInstanceUID,
      frameIndex
    );

    const volumeActor = getOrCreateVolume(
      imageDataObject,
      displaySetInstanceUID
    );

    loadProgressively(imageDataObject);

    render3D(imageDataObject, volumeActor);
  };

  const loadProgressively = imageDataObject => {
    loadImageData(imageDataObject);

    const { isLoading, imageIds } = imageDataObject;

    if (!isLoading) {
      setIsLoaded(true);
      return;
    }

    const NumberOfFrames = imageIds.length;

    const onPixelDataInsertedCallback = numberProcessed => {
      const percentCompleteTemp = Math.floor(
        (numberProcessed * 100) / NumberOfFrames
      );

      if (percentCompleteTemp !== percentComplete) {
        setPercentComplete(percentComplete);
      }
    };

    const onPixelDataInsertedErrorCallback = error => {
      console.log('vanht-error', error);
    };

    const onAllPixelDataInsertedCallback = () => {
      setIsLoaded(true);
    };

    imageDataObject.onPixelDataInserted(onPixelDataInsertedCallback);
    imageDataObject.onAllPixelDataInserted(onAllPixelDataInsertedCallback);
    imageDataObject.onPixelDataInsertedError(onPixelDataInsertedErrorCallback);
  };

  const handleCancel = () => {
    setIsLoaded(false);
    if (onCancel) onCancel();
  };

  // const handleAddGaussianWidget = (renderWindow, vtkImageData) => {
  //   const container = volumeControl.current || document.querySelector('body');
  //   // Create Widget container
  //   const widgetContainer = document.createElement('div');
  //   widgetContainer.style.position = 'absolute';
  //   widgetContainer.style.top = '-25px';
  //   widgetContainer.style.right = '-20px';
  //   widgetContainer.style.zIndex = 1;
  //   widgetContainer.style.background = 'rgba(255, 255, 255, 0.7)';
  //   container.appendChild(widgetContainer);

  //   widget.updateStyle({
  //     backgroundColor: 'rgba(255, 255, 255, 0.6)',
  //     histogramColor: 'rgba(100, 100, 100, 0.5)',
  //     strokeColor: 'rgb(0, 0, 0)',
  //     activeColor: 'rgb(255, 255, 255)',
  //     handleColor: 'rgb(50, 150, 50)',
  //     buttonDisableFillColor: 'rgba(255, 255, 255, 0.5)',
  //     buttonDisableStrokeColor: 'rgba(0, 0, 0, 0.5)',
  //     buttonStrokeColor: 'rgba(0, 0, 0, 1)',
  //     buttonFillColor: 'rgba(255, 255, 255, 1)',
  //     strokeWidth: 2,
  //     activeStrokeWidth: 3,
  //     buttonStrokeWidth: 1.5,
  //     handleWidth: 3,
  //     iconSize: 20, // Can be 0 if you want to remove buttons (dblClick for (+) / rightClick for (-))
  //     padding: 10,
  //   });
  //   widget.addGaussian(0.48, 0.5, 0.2, 0.3, 0.2);
  //   widget.addGaussian(0.75, 1, 0.3, 0, 0);
  //   widget.setContainer(widgetContainer);
  //   widget.bindMouseListeners();

  //   widget.onAnimation(start => {
  //     if (start) {
  //       renderWindow.getInteractor().requestAnimation(widget);
  //     } else {
  //       renderWindow.getInteractor().cancelAnimation(widget);
  //     }
  //   });
  //   widget.onOpacityChange(() => {
  //     widget.applyOpacity(ofun);
  //     if (!renderWindow.getInteractor().isAnimating()) {
  //       renderWindow.render();
  //     }
  //   });

  //   const dataArray = vtkImageData.getPointData().getScalars();
  //   widget.setDataArray(dataArray.getData());
  //   widget.applyOpacity(ofun);
  //   widget.setColorTransferFunction(cfun);
  //   cfun.onModified(() => {
  //     widget.render();
  //     renderWindow.render();
  //   });
  // };

  const render3D = (imageDataObject, volumeActor) => {
    // const { vtkImageData } = imageDataObject;
    // widget = vtkPiecewiseGaussianWidget.newInstance({
    //   numberOfBins: 256,
    //   size: [250, 100],
    // });

    applyPreset(
      volumeActor,
      presets[defaultPreset],
      setShade,
      setAmbient,
      setDiffuse,
      setSpecular,
      setSpecularPower,
      setLower,
      setUpper
    );

    const genericRender = GenericRenderWindow.newInstance({
      background: [0, 0, 0],
      listenWindowResize: false,
    });
    genericRender.setContainer(volumeContainer.current);
    const renderer = genericRender.getRenderer();
    renderWindow = genericRender.getRenderWindow();
    // renderWindow.getInteractor().setDesiredUpdateRate(15.0);

    setTimeout(() => {
      volumeActor
        .getProperty()
        .setScalarOpacityUnitDistance(0, opacityDistance);
      oglrw = genericRender.getOpenGLRenderWindow();
      oglrw.setSize(600, 600);

      renderer.addVolume(volumeActor);
      renderer.resetCamera();
      const camera = renderer.getActiveCamera();
      camera.elevation(-70);
      renderer.updateLightsGeometryToFollowCamera();

      let center = camera.getFocalPoint();
      const bounds = volumeActor.getMapper().getBounds();
      center = [
        (bounds[0] + bounds[1]) / 2.0,
        (bounds[2] + bounds[3]) / 2.0,
        (bounds[4] + bounds[5]) / 2.0,
      ];
      const interactorStyle = vtkDefaultCameraInteractor();
      interactorStyle.setCenterOfRotation(center);
      renderWindow.getInteractor().setInteractorStyle(interactorStyle);

      // handleAddGaussianWidget(renderWindow, vtkImageData);

      // TODO: Not sure why this is necessary to force the initial draw
      // genericRender.resize();

      renderWindow.render();

      // Debug
      // window.cfun = cfun;
      // window.ofun = ofun;
      // window.widget = widget;
      // window.oglrw = oglrw;
      // window.volumeActor = volumeActor;
      // window.volumeMapper = volumeMapper;
      // window.genericRender = genericRender;
      // window.renderer = renderer;
      // window.renderWindow = renderWindow;
    }, 200);
  };

  function handleChangePreset(value) {
    const preset = presets.find(preset => preset.id === value);

    applyPreset(
      volumeActor,
      preset,
      setShade,
      setAmbient,
      setDiffuse,
      setSpecular,
      setSpecularPower,
      setLower,
      setUpper
    );
    renderWindow.render();
  }

  const handleChangeProperty = (type, values) => {
    switch (type) {
      case PROPERTY_TYPE.QUALITY:
        if (values == 1) {
          oglrw.setSize(300, 300);
        } else if (values == 2) {
          oglrw.setSize(600, 600);
        } else {
          oglrw.setSize(1000, 1000);
        }
        break;
      case PROPERTY_TYPE.SHADE:
        setShade(values);
        volumeActor.getProperty().setShade(values);
        break;
      case PROPERTY_TYPE.OPACITY_DISTANCE:
        setOpacityDistance(values);
        volumeActor.getProperty().setScalarOpacityUnitDistance(0, values);
        break;
      case PROPERTY_TYPE.SAMPLE_DISTANCE:
        setSampleDistance(values);
        // mapper
        volumeMapper.setSampleDistance(values);
        break;
      case PROPERTY_TYPE.RANGE:
        setLower(values[0]);
        setUpper(values[1]);
        volumeActor
          .getProperty()
          .getRGBTransferFunction(0)
          .setRange(values[0], values[1]);
        break;
      case PROPERTY_TYPE.AMBIENT:
        setAmbient(values);
        volumeActor.getProperty().setAmbient(values);
        break;
      case PROPERTY_TYPE.DIFFUSE:
        setDiffuse(values);
        volumeActor.getProperty().setDiffuse(values);
        break;
      case PROPERTY_TYPE.SPECULAR:
        setSpecular(values);
        volumeActor.getProperty().setSpecular(values);
        break;
      case PROPERTY_TYPE.SPECULAR_POWER:
        setSpecularPower(values);
        volumeActor.getProperty().setSpecularPower(values);
        break;
      default:
        break;
    }

    clearTimeout(timeoutRender);
    timeoutRender = setTimeout(() => {
      renderWindow.render();
    }, 500);
  };

  return (
    <Modal
      title="3D Reconstruction"
      centered
      visible={isModalVisible}
      onCancel={handleCancel}
      maskClosable={false}
      width={1000}
      footer={null}
      className="3d-modal"
    >
      <Spin spinning={!isLoaded}>
        <div className="reconstruction-3d">
          <Row>
            <div ref={volumeControl}></div>
          </Row>
          <Row>
            <Col sm={18}>
              <div
                ref={volumeContainer}
                style={{
                  height: '600px',
                  width: '600px',
                  margin: '0 auto',
                }}
              ></div>
            </Col>
            <Col sm={6}>
              <div className="form-item">
                <div className="title">Presets:</div>
                <Select
                  style={{ width: '100%' }}
                  defaultValue={presets[defaultPreset].id}
                  onChange={handleChangePreset}
                >
                  {presets.map(preset => (
                    <Select.Option key={preset.id} value={preset.id}>
                      {preset.name}
                    </Select.Option>
                  ))}
                </Select>
              </div>
              <div className="form-item">
                <div className="title">Quality:</div>
                <Select
                  style={{ width: '100%' }}
                  defaultValue="2"
                  onChange={value =>
                    handleChangeProperty(PROPERTY_TYPE.QUALITY, value)
                  }
                >
                  <Select.Option value="1">Normal</Select.Option>
                  <Select.Option value="2">High</Select.Option>
                  {/* <Select.Option value="3">Highest</Select.Option> */}
                </Select>
              </div>
              <div className="form-item">
                <div className="title">Opacity distance:</div>
                <Slider
                  min={0.1}
                  max={10}
                  step={0.1}
                  value={opacityDistance}
                  onChange={value =>
                    handleChangeProperty(PROPERTY_TYPE.OPACITY_DISTANCE, value)
                  }
                />
              </div>
              <div className="form-item">
                <div className="title">Sample distance:</div>
                <Slider
                  min={0.1}
                  max={10}
                  step={0.1}
                  value={sampleDistance}
                  onChange={value =>
                    handleChangeProperty(PROPERTY_TYPE.SAMPLE_DISTANCE, value)
                  }
                />
              </div>
              <div className="form-item">
                <div className="title">Range:</div>
                <Slider
                  range
                  min={-4000}
                  max={4000}
                  value={[newLower, newUpper]}
                  onChange={values =>
                    handleChangeProperty(PROPERTY_TYPE.RANGE, values)
                  }
                />
              </div>
              <div className="form-item">
                <Checkbox
                  checked={shade}
                  onChange={e =>
                    handleChangeProperty(PROPERTY_TYPE.SHADE, e.target.checked)
                  }
                >
                  Shade
                </Checkbox>
              </div>
              <div className="form-item">
                <div className="title">Ambient:</div>
                <Slider
                  min={0}
                  max={1}
                  step={0.01}
                  value={ambient}
                  onChange={value =>
                    handleChangeProperty(PROPERTY_TYPE.AMBIENT, value)
                  }
                />
              </div>
              <div className="form-item">
                <div className="title">Diffuse:</div>
                <Slider
                  min={0}
                  max={1}
                  step={0.01}
                  value={diffuse}
                  onChange={value =>
                    handleChangeProperty(PROPERTY_TYPE.DIFFUSE, value)
                  }
                />
              </div>
              <div className="form-item">
                <div className="title">Specular:</div>
                <Slider
                  min={0}
                  max={1}
                  step={0.01}
                  value={specular}
                  onChange={value =>
                    handleChangeProperty(PROPERTY_TYPE.SPECULAR, value)
                  }
                />
              </div>
              <div className="form-item">
                <div className="title">Specular power:</div>
                <Slider
                  min={0}
                  max={100}
                  step={0.1}
                  value={specularPower}
                  onChange={value =>
                    handleChangeProperty(PROPERTY_TYPE.SPECULAR_POWER, value)
                  }
                />
              </div>
            </Col>
          </Row>
        </div>
      </Spin>
    </Modal>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Content3DModal);

function _getRangeFromWindowLevels(width, center, Modality = undefined) {
  // For PET just set the range to 0-5 SUV
  if (Modality === 'PT') {
    return { lower: 0, upper: 5 };
  }

  const levelsAreNotNumbers = isNaN(center) || isNaN(width);

  if (levelsAreNotNumbers) {
    return { lower: 0, upper: 512 };
  }

  return {
    lower: center - width / 2.0,
    upper: center + width / 2.0,
  };
}

function getShiftRange(colorTransferArray) {
  let lower = Infinity;
  let upper = -Infinity;
  for (let i = 0; i < colorTransferArray.length; i += 4) {
    lower = Math.min(lower, colorTransferArray[i]);
    upper = Math.max(upper, colorTransferArray[i]);
  }

  const center = (upper - lower) / 2;

  return {
    shiftRange: [-center, center],
    lower,
    upper,
  };
}

function applyPointsToPiecewiseFunction(points, range, pwf) {
  const width = range[1] - range[0];
  const rescaled = points.map(([x, y]) => [x * width + range[0], y]);

  pwf.removeAllPoints();
  rescaled.forEach(([x, y]) => pwf.addPoint(x, y));

  return rescaled;
}

function applyPointsToRGBFunction(points, range, cfun) {
  const width = range[1] - range[0];
  const rescaled = points.map(([x, r, g, b]) => [
    x * width + range[0],
    r,
    g,
    b,
  ]);

  cfun.removeAllPoints();
  rescaled.forEach(([x, r, g, b]) => cfun.addRGBPoint(x, r, g, b));

  return rescaled;
}

function applyPreset(
  actor,
  preset,
  setShade,
  setAmbient,
  setDiffuse,
  setSpecular,
  setSpecularPower,
  setLower,
  setUpper
) {
  // Create color transfer function
  const colorTransferArray = preset.colorTransfer
    .split(' ')
    .splice(1)
    .map(parseFloat);

  const { shiftRange, lower, upper } = getShiftRange(colorTransferArray);
  let min = shiftRange[0];
  const width = shiftRange[1] - shiftRange[0];

  cfun = vtkColorTransferFunction.newInstance();
  const normColorTransferValuePoints = [];
  for (let i = 0; i < colorTransferArray.length; i += 4) {
    let value = colorTransferArray[i];
    const r = colorTransferArray[i + 1];
    const g = colorTransferArray[i + 2];
    const b = colorTransferArray[i + 3];

    value = (value - min) / width;
    normColorTransferValuePoints.push([value, r, g, b]);
  }

  applyPointsToRGBFunction(normColorTransferValuePoints, shiftRange, cfun);

  actor.getProperty().setRGBTransferFunction(0, cfun);

  //Add widget
  // widget.setColorTransferFunction(cfun);

  // Create scalar opacity function
  const scalarOpacityArray = preset.scalarOpacity
    .split(' ')
    .splice(1)
    .map(parseFloat);

  ofun = vtkPiecewiseFunction.newInstance();
  const normPoints = [];
  for (let i = 0; i < scalarOpacityArray.length; i += 2) {
    let value = scalarOpacityArray[i];
    const opacity = scalarOpacityArray[i + 1];

    value = (value - min) / width;

    normPoints.push([value, opacity]);
  }

  applyPointsToPiecewiseFunction(normPoints, shiftRange, ofun);

  actor.getProperty().setScalarOpacity(0, ofun);

  //Add widget
  // widget.applyOpacity(ofun);

  const [
    gradientMinValue,
    gradientMinOpacity,
    gradientMaxValue,
    gradientMaxOpacity,
  ] = preset.gradientOpacity
    .split(' ')
    .splice(1)
    .map(parseFloat);

  actor.getProperty().setUseGradientOpacity(0, true);
  actor.getProperty().setGradientOpacityMinimumValue(0, gradientMinValue);
  actor.getProperty().setGradientOpacityMinimumOpacity(0, gradientMinOpacity);

  actor.getProperty().setGradientOpacityMaximumValue(0, gradientMaxValue);
  actor.getProperty().setGradientOpacityMaximumOpacity(0, gradientMaxOpacity);

  if (preset.interpolation === '1') {
    actor.getProperty().setInterpolationTypeToFastLinear();
  }

  const ambient = parseFloat(preset.ambient);
  const shade = preset.shade === '1';
  const diffuse = parseFloat(preset.diffuse);
  const specular = parseFloat(preset.specular);
  const specularPower = parseFloat(preset.specularPower);

  actor.getProperty().setShade(shade);
  actor.getProperty().setAmbient(ambient);
  actor.getProperty().setDiffuse(diffuse);
  actor.getProperty().setSpecular(specular);
  actor.getProperty().setSpecularPower(specularPower);

  // set state
  setLower(lower);
  setUpper(upper);

  setShade(shade);
  setAmbient(ambient);
  setDiffuse(diffuse);
  setSpecular(specular);
  setSpecularPower(specularPower);
}
