import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Icon, Tooltip } from '@tuvm/ui';
import Content3DModal from './Content3DModal';
import './Reconstruct3D.styl';
import { useTranslation } from 'react-i18next';

function Reconstruct3D(props) {
  const { studies } = props;
  const { t } = useTranslation('Vindoc');
  const viewports = useSelector(state => state.viewports);
  const { viewportSpecificData, activeViewportIndex = 0 } = viewports || {};
  const activeViewport = viewportSpecificData[activeViewportIndex] || {};

  const [isModalVisible, setIsModalVisible] = useState(false);

  const onShowModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <React.Fragment>
      {activeViewport.isReconstructable && (
        <React.Fragment>
          <Tooltip
            title={t('3D Reconstruction')}
            placement="bottom"
            overlayClassName="toolbar-tooltip"
          >
            <div className="toolbar-button" onClick={onShowModal}>
              <Icon name="cube3D1" />
            </div>
          </Tooltip>
          {isModalVisible && (
            <Content3DModal
              isModalVisible={isModalVisible}
              studies={studies}
              onCancel={handleCancel}
            />
          )}
        </React.Fragment>
      )}
    </React.Fragment>
  );
}

export default Reconstruct3D;
