import React from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import moment from 'moment';

export default function VindocFooter(props) {
  const { ...rest } = props;

  const { t } = useTranslation('Vindoc');

  return (
    <div
      {...rest}
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        marginTop: 14,
      }}
    >
      <div>
        &copy;{' '}
        <a href="https://vinbigdata.org/" target="_blank">
          {t('Vingroup Big Data Institute')}
        </a>
        . {moment().format('YYYY')}
      </div>
      <div>
        Email:{' '}
        <a href="mailto:vindr.contact@vinbigdata.org" target="_top">
          vindr.contact@vinbigdata.org
        </a>
      </div>
    </div>
  );
}

VindocFooter.propTypes = {
  className: PropTypes.string,
};
