export const chestHighlight = [
  'No finding',
  'Cardiomegaly',
  'Nodule/Mass',
  'Pleural Effusion',
  'Pneumothorax',
];

export const isHighlight = name => {
  const finder = chestHighlight.find(
    n => n.toLowerCase() === name.toLowerCase()
  );

  if (finder) return true;
  return false;
};

export const getHighlightStyle = label => {
  if (isHighlight(label)) {
    return {
      color: 'var(--active)',
      fontWeight: 'bold',
    };
  }
  return null;
};
