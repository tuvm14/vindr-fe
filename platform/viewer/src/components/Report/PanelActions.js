// import OHIF from '@tuvm/core';
import get from 'lodash/get';
import { actionGetStudyList } from '../../pages/studyList/actions';
import api from '../../services/api';
import { actionSetExtensionData } from '../../system/systemActions';
import {
  CONFIG_SERVER,
  FEEDBACK_ACTIONS,
  STUDY_OPENING,
} from '../../utils/constants';
import { getLastDoctorActionResult, getTenant } from '../../utils/helper';
import { formatMeasurementsData } from './ProcessData';

const tenant = getTenant();

// const { redux } = OHIF;
// const { setExtensionData } = redux.actions;

export const getRelatedStudies = patientId => {
  const relatedParams = {
    offset: 0,
    limit: 30,
    sort: '-StudyDate',
    query_string: `PatientID.keyword:"${patientId}"`,
  };
  return actionGetStudyList(relatedParams);
};

export const getListReportV2 = async studyId => {
  try {
    const studyInfo = await actionGetStudyInfo(studyId);
    const patientId = get(studyInfo, 'data.records[0].PatientID');
    const relatedStudies = await getRelatedStudies(patientId);
    const studies = get(relatedStudies, 'data.records');
    const promises = studies.map(async study => {
      const reports = await getReportOfStudy(study.StudyInstanceUID);
      return {
        study: study,
        reports: reports,
      };
    });
    const reportList = await Promise.all(promises);
    return reportList;
  } catch (err) {
    console.log(err);
    return [];
  }
};

export const actionGetStudyInfo = studyId => {
  const params = {
    offset: 0,
    limit: 1,
    sort: '-StudyDate',
    query_string: `StudyInstanceUID.keyword:"${studyId}"`,
  };
  return actionGetStudyList(params);
};

export const getReportOfStudy = async studyId => {
  const url = `/medreport/report/${tenant}/study-reports/${studyId}`;
  try {
    const { data } = await api({ url, method: 'GET' });
    return data.reports;
  } catch (error) {
    return [];
  }
};

export const getListReport = async studyId => {
  const url = `/backend/report/${studyId}/reports`;
  try {
    const { data } = await api({ url, method: 'GET' });
    return data.data;
  } catch (error) {
    // todo
  }
};

export const updateOrderReport = async (reportId, data) => {
  const url = `/backend/report/${reportId}`;
  try {
    return api({ url, method: 'PUT', data });
  } catch (error) {
    // todo
  }
};

export const updateOrderReportV2 = (reportId, data) => {
  return api({
    url: `/medreport/report/${tenant}/report/${reportId}`,
    method: 'PUT',
    data,
  });
};

export const callDoctorActionResult = async (params = {}) => {
  const url = `/backend/doctor/action`;
  try {
    const { data } = await api({ url, method: 'GET', params });
    return data;
  } catch (error) {
    // todo
  }
};

export const actionGetFeedbackSummary = async (params = {}) => {
  return api({
    url: `/backend/doctor/feedback/summary`,
    method: 'GET',
    params,
  });
};

export const actionSubmitReport = async (params = {}) => {
  const url = `/medreport/report/${tenant}/submit`;

  const { data } = await api({
    url: url,
    method: 'POST',
    data: params,
  });

  return Promise.resolve(data);
};

export const actionGetPreview = async (params = {}) => {
  const url = `/medreport/report/${tenant}/preview`;

  const { data } = await api({
    url: url,
    method: 'POST',
    data: params,
  });

  return Promise.resolve(data);
};

export const actionGetApprovedPreview = async (params = {}) => {
  const url = `/medreport/report/${tenant}/viewhtml/${params.reportId}`;

  const { data } = await api({
    url: url,
    method: 'GET',
    data: params,
  });

  return Promise.resolve(data);
};

export const actionFeedback = async (params = {}) => {
  const url = '/backend/doctor/action';

  const { data } = await api({
    url: url,
    method: 'POST',
    data: params,
  });

  return data;
};

export const actionViewStudy = async params => {
  const studyOpening = sessionStorage.getItem(STUDY_OPENING);

  if (studyOpening !== params.object_instance_uid) {
    const url = '/backend/doctor/action';

    const { data } = await api({
      url: url,
      method: 'POST',
      data: params,
    });

    if (data && data.inserted_id) {
      sessionStorage.setItem(STUDY_OPENING, params.object_instance_uid);
    }
  }
};

export const actionViewMeasurement = async params => {
  const { measurementData } = params;
  const { StudyInstanceUID } = measurementData;
  const store = window.store.getState();
  const username = get(store, 'extensions.user.username');

  const payload = {
    action: FEEDBACK_ACTIONS.VIEW_MORE,
    object_type: 'study',
    object_instance_uid: StudyInstanceUID,
    global: {},
    local: {},
    metadata: {},
    sr_data: {},
    form_data: {
      username,
      StudyInstanceUID,
      timestamp: new Date().toISOString(),
      labels: measurementData.location,
      handles: {
        start: measurementData.handles.start,
        end: measurementData.handles.end,
      },
    },
  };

  const url = '/backend/doctor/action';

  return api({
    url: url,
    method: 'POST',
    data: payload,
  });
};

export const actionGetSummary = async (params = {}) => {
  const { StudyInstanceUID, owner } = params;
  try {
    const baseUrl = `/backend/doctor/action/summary/${StudyInstanceUID}`;
    const url = owner ? baseUrl + `?owner=${owner}` : baseUrl;
    const res = await api({ url, method: 'GET' });
    const data = get(res, 'data.data');
    if (data) {
      actionSetExtensionData('predictionPanel', {
        doctorResult: data,
      });

      let measurementsData;

      const { SUBMIT, APPROVE, DONE, REVOKE } = data;
      const lastAction = getLastDoctorActionResult([
        SUBMIT,
        APPROVE,
        DONE,
        REVOKE,
      ]);
      const formData = get(lastAction, 'form_data');
      if (formData) {
        const measurements = get(formData, 'measurements') || {};
        if (measurements) {
          const RectangleRoi =
            formatMeasurementsData(measurements.RectangleRoi) || [];
          measurementsData = {
            RectangleRoi,
          };
        }

        actionSetExtensionData('predictionPanel', {
          activeEditorTemplate: formData.activeEditorTemplate,
          // reportId: formData.report_id,
          doctorTabContent: {
            editorSourceHTML: formData.editorSourceHTML,
            conclusion: formData?.result,
            measurementsData: measurementsData,
          },
        });
      }
    }
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const actionGetSummaryV2 = async (params = {}) => {
  const { StudyInstanceUID, owner } = params;
  try {
    const baseUrl = `/medreport/report/${tenant}/doctor/action/summary/${StudyInstanceUID}`;
    const url = owner ? baseUrl + `?owner=${owner}` : baseUrl;
    const res = await api({ url, method: 'GET' });
    const data = get(res, 'data.data');
    if (data) {
      actionSetExtensionData('predictionPanel', {
        doctorResult: data,
      });

      let measurementsData;

      const { SUBMIT, APPROVE, DONE, REVOKE } = data;
      const lastAction = getLastDoctorActionResult([
        SUBMIT,
        APPROVE,
        DONE,
        REVOKE,
      ]);
      const formData = get(lastAction, 'form_data');
      if (formData) {
        const measurements = get(formData, 'measurements') || {};
        if (measurements) {
          const RectangleRoi =
            formatMeasurementsData(measurements.RectangleRoi) || [];
          measurementsData = {
            RectangleRoi,
          };
        }

        actionSetExtensionData('predictionPanel', {
          activeEditorTemplate: formData.activeEditorTemplate,
          // reportId: formData.report_id,
          doctorTabContent: {
            editorSourceHTML: formData.editorSourceHTML,
            conclusion: formData?.result,
            measurementsData: measurementsData,
          },
        });
      }
    }
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const actionGetTemplate = async (params = { offset: 0, limit: 999 }) => {
  // const url = `/backend/tenant/assets/templates?display=tree&type=case_template`;
  const tenant = getTenant();
  const url = `/om/${tenant}/user_templates?type=report-content`;

  const res = await api({ url, method: 'GET', params });
  // let templates = get(res, 'data.data.case_template') || [];

  // templates.forEach(function(item, i) {
  //   if (item.name.toLowerCase() === 'default') {
  //     templates.splice(i, 1);
  //     templates.unshift(item);
  //   }
  // });

  // if (!isEmpty(templates)) {
  //   templates = templates.map((item, index) => ({
  //     ...item,
  //     id: index,
  //   }));
  // }
  let templates = get(res, 'data.data') || [];
  let groups = [];
  for (const it of templates) {
    if (groups.indexOf(it.meta.group) < 0) {
      groups.push(it.meta.group);
    }
  }

  let groupTemplates = groups.map(it => ({
    name: it,
    type: 'category',
    children: [],
  }));
  let formatTemplates = [
    {
      name: 'Default',
      type: 'category',
      children: [],
    },
  ];
  for (const it of groupTemplates) {
    if (it && it.name !== 'Default') {
      formatTemplates.push(it);
    }
  }
  for (const it of templates) {
    const idx = formatTemplates.findIndex(item => item.name == it.meta.group);
    formatTemplates[idx].children.push({
      category: it.meta.group,
      content: it.value,
      name: it.name,
      type: 'case_template',
      _id: it.id,
      ...it,
    });
  }

  const activeEditorTemplate = get(formatTemplates, '[0].children[0]._id');

  actionSetExtensionData('predictionPanel', {
    editorTemplates: formatTemplates,
    activeEditorTemplate,
  });

  return templates;
};

export const actionGetLayout = async (params = { offset: 0, limit: 999 }) => {
  // const url = `/backend/tenant/assets/templates?display=tree&type=case_template`;
  const tenant = getTenant();
  const url = `/om/${tenant}/user_templates?type=report-layout`;

  const res = await api({ url, method: 'GET', params });

  let layout = get(res, 'data.data') || [];

  actionSetExtensionData('predictionPanel', {
    reportLayouts: layout,
  });

  return layout;
};

export const actionDoctorStorageV2 = (params = {}) => {
  const payload = {
    report_id: params.reportId,
    action: params.action,
    object_type: 'study',
    object_instance_uid: params.StudyInstanceUID,
    global: {},
    local: {},
    metadata: {
      AccessionNumber: params.AccessionNumber,
    },
    sr_data: {},
    form_data: {
      timestamp: new Date().toISOString(),
      ...params,
    },
  };

  return api({
    baseURL: CONFIG_SERVER.BACKEND_URL.replace('/api', ''),
    url: `/api/medreport/report/${tenant}/doctor/actions`,
    method: 'POST',
    data: payload,
  });
};

export const actionDoctorStorage = (params = {}) => {
  const payload = {
    report_id: params.reportId,
    action: params.action,
    object_type: 'study',
    object_instance_uid: params.StudyInstanceUID,
    global: {},
    local: {},
    metadata: {
      AccessionNumber: params.AccessionNumber,
    },
    sr_data: {},
    form_data: {
      timestamp: new Date().toISOString(),
      ...params,
    },
  };

  const url = '/backend/doctor/action';

  return api({
    url: url,
    method: 'POST',
    data: payload,
  });
};

export const actionGetTenantInfo = async (params = {}) => {
  const url = '/backend/tenant/assets/info';

  try {
    const { data } = await api({
      url,
      method: 'GET',
      params: {
        study_instance_uid: params.StudyInstanceUID,
      },
    });

    if (data) {
      actionSetExtensionData('predictionPanel', {
        tenant: data,
      });
      return data;
    }
  } catch (error) {
    console.log(error);
  }
};
