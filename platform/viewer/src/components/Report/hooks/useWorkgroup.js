import { useState, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import get from 'lodash/get';

import {
  getWorklistAssigned,
  workgroupsOfStudyEndpoint,
} from '../../../services/endpoint';
import api from '../../../services/api';

export default function useWorkgroup() {
  const [workgroups, setWorkgroups] = useState(null);
  const [workgroupsLoaded, setWorkgroupsLoaded] = useState(false);
  const [workgroupsOfStudy, setWorkgroupsOfStudy] = useState(null);
  const [workgroupsOfStudyLoaded, setWorkgroupsOfStudyLoaded] = useState(false);

  const { studyInstanceUIDs } = useParams();

  const callGetWorkgroups = useCallback(async () => {
    const url = getWorklistAssigned(studyInstanceUIDs);
    setWorkgroupsLoaded(false);

    const res = await api({ url, method: 'GET' }).then(res => {
      if (res.status === 200) {
        const data = get(res, 'data.data') || [];
        setWorkgroups(data);
        setWorkgroupsLoaded(true);

        return data;
      }
    });

    return res;
  }, [studyInstanceUIDs]);

  const callGetWorkgroupsOfStudy = useCallback(async () => {
    const url = workgroupsOfStudyEndpoint(studyInstanceUIDs);
    setWorkgroupsOfStudyLoaded(false);

    const res = await api({ url, method: 'GET' }).then(res => {
      if (res.status === 200) {
        const data = get(res, 'data.data') || [];
        setWorkgroupsOfStudy(data);
        setWorkgroupsOfStudyLoaded(true);
        return data;
      }
    });

    return res;
  }, [studyInstanceUIDs]);

  return {
    workgroups,
    callGetWorkgroups,
    workgroupsOfStudy,
    callGetWorkgroupsOfStudy,
    workgroupsLoaded,
    workgroupsOfStudyLoaded,
  };
}
