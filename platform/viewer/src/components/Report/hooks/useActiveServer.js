import { useSelector } from 'react-redux';
import find from 'lodash/find';

export const useActiveServer = () => {
  return useSelector(state => find(state.servers.servers, { active: true }));
};
