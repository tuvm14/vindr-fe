import { redux } from '@tuvm/core';

export const useExtensions = () => {
  const setExtensionData = data => {
    return redux.actions.setExtensionData('predictionPanel', data);
  };

  return {
    setExtensionData,
  };
};
