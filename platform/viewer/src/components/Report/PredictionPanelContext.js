import React from 'react';

const PredictionPanelContext = React.createContext();

export const PredictionPanelProvider = PredictionPanelContext.Provider;

export default PredictionPanelContext;
