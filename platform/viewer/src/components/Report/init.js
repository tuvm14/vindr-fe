import OHIF from '@tuvm/core';
import cornerstone from 'cornerstone-core';
import csTools from '@vindr/vindr-tools';
import throttle from 'lodash.throttle';
import get from 'lodash/get';

// import VindocLabellingFlow from '../../components/VindocLabelling';
import ToolContextMenu from './components/ToolContextMenu';
import { HOVER_BOX_TIMER, PANEL_TAB_NAMES } from '../../utils/constants';
import { actionViewMeasurement } from './PanelActions';
import { currentPanelTabName } from './components/PanelTabs';

const {
  onAdded,
  onRemoved,
  onModified,
} = OHIF.measurements.MeasurementHandlers;

const MEASUREMENT_ACTION_MAP = {
  added: onAdded,
  removed: onRemoved,
  modified: throttle(event => {
    return onModified(event);
  }, 2000),
  labelmapModified: event => {},
};

/**
 *
 *
 * @export
 * @param {Object} servicesManager
 * @param {Object} configuration
 */
export default function init({
  servicesManager,
  commandsManager,
  configuration,
}) {
  const { UIDialogService } = servicesManager.services;

  // TODO: MEASUREMENT_COMPLETED (not present in initial implementation)
  const onMeasurementsChanged = (action, event) => {
    return MEASUREMENT_ACTION_MAP[action](event);
  };
  const onMeasurementAdded = onMeasurementsChanged.bind(this, 'added');
  const onMeasurementRemoved = onMeasurementsChanged.bind(this, 'removed');
  const onMeasurementModified = onMeasurementsChanged.bind(this, 'modified');
  const onLabelmapModified = onMeasurementsChanged.bind(
    this,
    'labelmapModified'
  );

  let mouseUpX = 0,
    mouseUpY = 0,
    mouseDownY = 0,
    mouseDownX = 0;

  const _getDefaultPosition = event => ({
    x: (event && event.currentPoints.client.x) || 0,
    y: (event && event.currentPoints.client.y) || 0,
  });

  // const _updateLabellingHandler = (labellingData, measurementData) => {
  //   const { location, description, response, boxNumber } = labellingData;
  //   if (location) {
  //     measurementData.location = location;
  //   }

  //   measurementData.description = description || '';

  //   if (response) {
  //     measurementData.response = response;
  //   }

  //   if (boxNumber) {
  //     measurementData.boxNumber = boxNumber;
  //   }

  //   const { MeasurementHandlers } = OHIF.measurements;

  //   MeasurementHandlers.onModified({
  //     detail: {
  //       toolType: measurementData.toolType,
  //       measurementData: {
  //         _id: measurementData._id,
  //         lesionNamingNumber: measurementData.lesionNamingNumber,
  //         measurementNumber: measurementData.measurementNumber,
  //         boxNumber: measurementData.boxNumber,
  //         active: true,
  //         showBoxLabel: true,
  //       },
  //       element: document.querySelector('.viewport-element'),
  //     },
  //   });
  //   const measurementApi = OHIF.measurements.MeasurementApi.Instance;

  //   Object.keys(measurementApi.tools).forEach(toolType => {
  //     const measurements = measurementApi.tools[toolType];

  //     measurements.forEach(measurement => {
  //       measurement.active = false;
  //     });
  //   });

  //   commandsManager.runCommand(
  //     'updateTableWithNewMeasurementData',
  //     measurementData
  //   );
  // };

  // const showLabellingDialog = (props, contentProps, measurementData) => {
  //   if (!UIDialogService) {
  //     console.warn('Unable to show dialog; no UI Dialog Service available.');
  //     return;
  //   }

  //   UIDialogService.create({
  //     id: 'labelling',
  //     isDraggable: true,
  //     showOverlay: false,
  //     centralize: false,
  //     // content: VindocLabellingFlow,
  //     contentProps: {
  //       measurementData,
  //       labellingClose: () => {
  //         UIDialogService.dismiss({ id: 'labelling' });
  //         UIDialogService.dismissAll();
  //         onRemoved({
  //           detail: {
  //             toolType: measurementData.toolType,
  //             measurementData: {
  //               _id: measurementData._id,
  //               lesionNamingNumber: measurementData.lesionNamingNumber,
  //               measurementNumber: measurementData.measurementNumber,
  //             },
  //           },
  //         });

  //         commandsManager.runCommand(
  //           'updateTableWithNewMeasurementData',
  //           measurementData
  //         );
  //       },
  //       labellingDoneCallback: () => {
  //         UIDialogService.dismiss({ id: 'labelling' });
  //         UIDialogService.doneCallback();
  //       },
  //       updateLabelling: labellingData =>
  //         _updateLabellingHandler(labellingData, measurementData),
  //       ...contentProps,
  //     },
  //     ...props,
  //   });
  // };

  const onRightClick = event => {
    if (!UIDialogService) {
      console.warn('Unable to show dialog; no UI Dialog Service available.');
      return;
    }

    UIDialogService.dismiss({ id: 'context-menu' });
    UIDialogService.create({
      id: 'context-menu',
      isDraggable: true,
      preservePosition: true,
      defaultPosition: _getDefaultPosition(event.detail),
      content: ToolContextMenu,
      contentProps: {
        eventData: event.detail,
        onDeleteAll: () => {
          commandsManager.runCommand('clearAnnotations');
          commandsManager.runCommand('clearSegmentations');
        },
        onDelete: (nearbyToolData, eventData) => {
          const element = eventData.element;
          commandsManager.runCommand('removeToolState', {
            element,
            toolType: nearbyToolData.toolType,
            tool: nearbyToolData.tool,
          });
        },
        onClose: () => UIDialogService.dismiss({ id: 'context-menu' }),
        onSetLabel: (eventData, measurementData) => {
          // showLabellingDialog(
          //   { centralize: true, isDraggable: true },
          //   { skipAddLabelButton: true, editLocation: true },
          //   measurementData
          // );
        },
        onSetDescription: (eventData, measurementData) => {
          // showLabellingDialog(
          //   { defaultPosition: _getDefaultPosition(eventData) },
          //   { editDescriptionOnDialog: true },
          //   measurementData
          // );
        },
      },
    });
  };

  const onTouchPress = event => {
    if (!UIDialogService) {
      console.warn('Unable to show dialog; no UI Dialog Service available.');
      return;
    }

    UIDialogService.create({
      eventData: event.detail,
      content: ToolContextMenu,
      contentProps: {
        isTouchEvent: true,
      },
    });
  };

  const _getLabelPopupPosition = measurementData => {
    const windowHeight = window.innerHeight;
    let y = 250;
    if (mouseDownY < windowHeight / 3) {
      y = mouseDownY;
    }
    return { x: mouseUpX + 5, y };
  };

  const labelToolList = ['RectangleRoi'];

  // const onComplete = event => {
  //   UIDialogService.dismiss({ id: 'labelling' });
  //   const toolType = get(event, 'detail.toolType');
  //   const measurementData = get(event, 'detail.measurementData') || {};

  //   setTimeout(() => {
  //     const labellingElement = document.getElementById(
  //       'draggableItem-labelling'
  //     );

  //     let y = 250;
  //     let x = mouseUpX + 5;

  //     const labelListHeight = labellingElement.offsetHeight;
  //     const windowHeight = window.innerHeight;
  //     const startToBottom = windowHeight - mouseDownY;

  //     if (startToBottom >= labelListHeight) {
  //       y = mouseDownY;
  //     } else if (mouseUpY <= labelListHeight) {
  //       y = 50;
  //     } else if (mouseUpY > labelListHeight) {
  //       y = mouseUpY - labelListHeight;
  //     }

  //     const screenHeight = window.screen.height;

  //     if (screenHeight <= 768 && screenHeight < labelListHeight + 50) {
  //       y = 0;
  //     }

  //     const transform = `translate(${x}px,${y}px)`;
  //     labellingElement.style.transform = transform;
  //     labellingElement.style.visibility = 'visible';
  //   }, 300);

  //   if (toolType && labelToolList.includes(toolType)) {
  //     showLabellingDialog(
  //       {
  //         centralize: false,
  //         isDraggable: true,
  //         showOverlay: false,
  //         preservePosition: true,
  //         defaultPosition: _getLabelPopupPosition(measurementData),
  //       },
  //       { skipAddLabelButton: true, editLocation: true },
  //       measurementData
  //     );
  //   }
  // };

  let lastTimeHoverBox = null;

  const onHoverOnMeasurement = eventData => {
    if (currentPanelTabName === PANEL_TAB_NAMES.AI_TAB) {
      setTimeout(() => {
        const timeNow = Date.now();

        if (
          !lastTimeHoverBox ||
          timeNow - lastTimeHoverBox >= HOVER_BOX_TIMER
        ) {
          const measurementData = get(eventData, 'detail.measurementData');
          if (measurementData && measurementData.toolType) {
            actionViewMeasurement({ measurementData });
          }
        }
      }, HOVER_BOX_TIMER);
      lastTimeHoverBox = Date.now();
    }
  };

  const onTouchStart = () => resetLabelligAndContextMenu();

  const onMouseClick = () => resetLabelligAndContextMenu();

  const resetLabelligAndContextMenu = () => {
    if (!UIDialogService) {
      console.warn('Unable to show dialog; no UI Dialog Service available.');
      return;
    }

    UIDialogService.dismiss({ id: 'context-menu' });
    UIDialogService.dismiss({ id: 'labelling' });
  };

  // TODO: This makes scrolling painfully slow
  const onNewImage = event => {
    const { image, viewport = {}, element } = get(event, 'detail');
    if (!image || !viewport || !element) return;
    try {
      const { columns, rows, windowCenter, windowWidth } = image || {};

      // displayedArea.brhc => This attribute has been removed from the release (v2.4.0) of cornerstone.
      // https://github.com/cornerstonejs/cornerstone/pull/462/files
      const { x, y } = get(viewport, 'displayedArea.brhc', {});
      const { windowCenter: currentWc, windowWidth: currentWw } = get(
        viewport,
        'voi'
      );

      if (columns && rows && x && y && (columns !== x || rows !== y)) {
        cornerstone.reset(element);
      } else if (
        windowCenter &&
        windowWidth &&
        (windowCenter !== currentWc || windowWidth !== currentWw)
      ) {
        const { modality } =
          cornerstone.metaData.get('generalSeriesModule', image.imageId) || {};
        if (modality === 'MG') {
          cornerstone.setViewport(element, {
            ...viewport,
            voi: { windowCenter, windowWidth },
          });
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  /*
   * Because click gives us the native "mouse up", buttons will always be `0`
   * Need to fallback to event.which;
   *
   */
  const handleClick = cornerstoneMouseClickEvent => {
    const mouseUpEvent = cornerstoneMouseClickEvent.detail.event;
    const isRightClick = mouseUpEvent.which === 3;

    if (isRightClick) {
      onRightClick(cornerstoneMouseClickEvent);
    } else {
      onMouseClick(cornerstoneMouseClickEvent);
    }
  };

  function elementEnabledHandler(evt) {
    const element = evt.detail.element;

    element.addEventListener(
      csTools.EVENTS.MEASUREMENT_ADDED,
      onMeasurementAdded
    );
    element.addEventListener(
      csTools.EVENTS.MEASUREMENT_REMOVED,
      onMeasurementRemoved
    );
    element.addEventListener(
      csTools.EVENTS.MEASUREMENT_MODIFIED,
      onMeasurementModified
    );
    element.addEventListener(
      csTools.EVENTS.LABELMAP_MODIFIED,
      onLabelmapModified
    );

    const rootTarget = document.getElementById('root');

    rootTarget.addEventListener('mouseup', e => {
      mouseUpX = e.clientX;
      mouseUpY = e.clientY;
    });

    rootTarget.addEventListener('mousedown', e => {
      mouseDownX = e.clientX;
      mouseDownY = e.clientY;
    });

    element.addEventListener(csTools.EVENTS.TOUCH_PRESS, onTouchPress);
    element.addEventListener(csTools.EVENTS.MOUSE_CLICK, handleClick);
    element.addEventListener(csTools.EVENTS.TOUCH_START, onTouchStart);
    // element.addEventListener(csTools.EVENTS.MEASUREMENT_COMPLETED, onComplete);
    element.addEventListener(csTools.EVENTS.MOUSE_HOVER, onHoverOnMeasurement);

    // TODO: This makes scrolling painfully slow
    element.addEventListener(cornerstone.EVENTS.NEW_IMAGE, onNewImage);
  }

  function elementDisabledHandler(evt) {
    const element = evt.detail.element;

    element.removeEventListener(
      csTools.EVENTS.MEASUREMENT_ADDED,
      onMeasurementAdded
    );
    element.removeEventListener(
      csTools.EVENTS.MEASUREMENT_REMOVED,
      onMeasurementRemoved
    );
    element.removeEventListener(
      csTools.EVENTS.MEASUREMENT_MODIFIED,
      onMeasurementModified
    );
    element.removeEventListener(
      csTools.EVENTS.LABELMAP_MODIFIED,
      onLabelmapModified
    );

    element.removeEventListener(csTools.EVENTS.TOUCH_PRESS, onTouchPress);
    element.removeEventListener(csTools.EVENTS.MOUSE_CLICK, handleClick);
    element.removeEventListener(csTools.EVENTS.TOUCH_START, onTouchStart);
    element.removeEventListener(
      csTools.EVENTS.MOUSE_HOVER,
      onHoverOnMeasurement
    );
    // element.removeEventListener(
    //   csTools.EVENTS.MEASUREMENT_COMPLETED,
    //   onComplete
    // );

    // TODO: This makes scrolling painfully slow
    element.removeEventListener(cornerstone.EVENTS.NEW_IMAGE, onNewImage);
  }

  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_ENABLED,
    elementEnabledHandler
  );
  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_DISABLED,
    elementDisabledHandler
  );
}
