import React from 'react';
import ConnectedMeasurementTable from './components/VindocMeasurementTable/ConnectedMeasurementTable.js';
import PredictionPanel from './PredictionPanel';
import init from './init.js';
import OHIF from '@tuvm/core';
// import VindocLabellingFlow from '../../components/VindocLabelling';

export default {
  /**
   * Only required property. Should be a unique value across all extensions.
   */
  id: 'prediction-panel',

  preRegistration({ servicesManager, commandsManager, configuration = {} }) {
    init({ servicesManager, commandsManager, configuration });
  },

  getPanelModule({ servicesManager, commandsManager }) {
    // const { UINotificationService, UIDialogService } = servicesManager.services;

    // const showLabellingDialog = (props, measurementData) => {
    //   if (!UIDialogService) {
    //     console.warn('Unable to show dialog; no UI Dialog Service available.');
    //     return;
    //   }

    //   UIDialogService.dismiss({ id: 'labelling' });
    //   UIDialogService.create({
    //     id: 'labelling',
    //     centralize: false,
    //     isDraggable: false,
    //     showOverlay: false,
    //     defaultPosition: {
    //       x: window.innerWidth - 370,
    //       y: 70,
    //     },
    //     // content: VindocLabellingFlow,
    //     contentProps: {
    //       measurementData,
    //       labellingDoneCallback: () =>
    //         UIDialogService.dismiss({ id: 'labelling' }),
    //       updateLabelling: ({
    //         location,
    //         description,
    //         response,
    //         boxNumber,
    //         boxDescription,
    //       }) => {
    //         measurementData.location = location || measurementData.location;
    //         measurementData.description = description || '';
    //         measurementData.response = response || measurementData.response;
    //         measurementData.boxNumber = boxNumber;
    //         measurementData.boxDescription =
    //           boxDescription || measurementData.boxDescription;
    //         const { MeasurementHandlers } = OHIF.measurements;

    //         MeasurementHandlers.onModified({
    //           detail: {
    //             toolType: measurementData.toolType,
    //             measurementData: {
    //               _id: measurementData._id,
    //               lesionNamingNumber: measurementData.lesionNamingNumber,
    //               measurementNumber: measurementData.measurementNumber,
    //               boxNumber: measurementData.boxNumber,
    //             },
    //             element: document.querySelector('.viewport-element'),
    //           },
    //         });

    //         commandsManager.runCommand(
    //           'updateTableWithNewMeasurementData',
    //           measurementData
    //         );
    //       },
    //       ...props,
    //     },
    //   });
    // };

    // const onDeleteBoxFromLabel = measurementData => {
    //   const { MeasurementHandlers } = OHIF.measurements;

    //   MeasurementHandlers.onModified({
    //     detail: {
    //       toolType: measurementData.toolType,
    //       measurementData: {
    //         _id: measurementData._id,
    //         lesionNamingNumber: measurementData.lesionNamingNumber,
    //         measurementNumber: measurementData.measurementNumber,
    //         boxNumber: measurementData.boxNumber,
    //       },
    //       element: document.querySelector('.viewport-element'),
    //     },
    //   });

    //   commandsManager.runCommand(
    //     'updateTableWithNewMeasurementData',
    //     measurementData
    //   );
    // };

    // const ExtendedConnectedMeasurementTable = () => (
    //   <ConnectedMeasurementTable
    //     onRelabel={tool =>
    //       showLabellingDialog(
    //         { editLocation: true, skipAddLabelButton: false },
    //         tool
    //       )
    //     }
    //     onEditDescription={tool =>
    //       showLabellingDialog({ editDescriptionOnDialog: true }, tool)
    //     }
    //     onDeleteBoxFromLabel={measurementData =>
    //       onDeleteBoxFromLabel(measurementData)
    //     }
    //     onSaveComplete={message => {
    //       if (UINotificationService) {
    //         UINotificationService.show(message);
    //       }
    //     }}
    //   />
    // );

    return {
      menuOptions: [
        {
          icon: 'th-large',
          label: 'Panel',
          target: 'prediction-panel',
          from: 'right',
        },
      ],
      components: [
        {
          id: 'prediction-panel',
          component: PredictionPanel,
        },
        // {
        //   id: 'vindoc-measurement-table',
        //   component: ExtendedConnectedMeasurementTable,
        // },
      ],
      defaultContext: ['VIEWER'],
    };
  },
};
