import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { notification } from 'antd';
import { Icon } from '@tuvm/ui';
import { DraggableModal } from 'ant-design-draggable-modal';
import DoctorTab from './components/DoctorTab';
// import { useParams } from 'react-router';
import {
  actionGetTemplate,
  actionGetSummary,
  actionGetTenantInfo,
  actionGetLayout,
} from './PanelActions';
import './DoctorModal.css';
import 'ant-design-draggable-modal/dist/index.css';
import { MinusOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

function DoctorModal(props) {
  // const dispatch = useDispatch();
  // const { studyInstanceUIDs } = useParams();
  // const viewports = useSelector(state => state.viewports);
  // const activeViewportIndex = get(viewports, 'activeViewportIndex');
  // const studyInstanceUID = get(
  //   viewports,
  //   `viewportSpecificData[${activeViewportIndex}].StudyInstanceUID`,
  //   ''
  // );

  // const [isModalVisible, setIsModalVisible] = useState(false);
  // const extensions = useSelector(state => state.extensions);
  // const tenant = get(extensions, 'predictionPanel.tenant');
  // const Patient = get(tenant, 'Patient');
  // const patientName = get(Patient, 'Name');
  // const studyDescription = get(Patient, 'StudyDescription');

  const { study, onCancel, isModalVisible } = props;
  const { StudyInstanceUID, PatientName, StudyDescription } = study;
  const { t } = useTranslation('Vindoc');

  useEffect(() => {
    document.title = 'VinDr - Viewer';
    actionGetSummary({ StudyInstanceUID: StudyInstanceUID });
    actionGetTemplate();
    actionGetLayout();
    actionGetTenantInfo({ StudyInstanceUID: StudyInstanceUID });
  }, [StudyInstanceUID]);

  // useEffect(() => {
  //   const modality = get(props.studies, '[0].series[0].Modality') || '';
  //   const bodyPartExamined =
  //     get(
  //       props.studies,
  //       '[0].series[0].instances[0].metadata.BodyPartExamined'
  //     ) || '';

  //   let bodyPart = bodyPartExamined;

  //   dispatch(
  //     redux.actions.setExtensionData('metadata', {
  //       bodyPart,
  //       modality: modality,
  //       isChestBody: isChestBody(bodyPart),
  //       isBreastBody: isBreastBody(bodyPart),
  //     })
  //   );
  // }, [props.studies, dispatch]);

  useEffect(() => {
    if (isModalVisible) {
      notification.close('doctor-report');
    }
  }, [isModalVisible]);

  // const series = get(props.studies, '[0].series');

  // const onShowModal = () => {
  //   setIsModalVisible(true);
  // };

  const handleCancel = () => {
    // setIsModalVisible(false);
    onCancel();
    // const actionName = DOCTOR_TAB_ACTIONS.CANCEL;
    // const payload = {
    //   editorSourceHTML: '',
    //   activeEditorTemplate: '',
    //   measurements: {
    //     RectangleRoi: [],
    //   },
    //   StudyInstanceUID: studyInstanceUIDs,
    //   action: actionName,
    // };

    // actionDoctorStorage(payload)
    //   .then(res => {
    //     actionGetSummary({ StudyInstanceUID: studyInstanceUIDs });
    //   })
    //   .catch(error => {
    //     snackbar.show({
    //       message: t('Error Message'),
    //       type: 'error',
    //     });
    //   });
  };

  const handleMinimize = () => {
    // setIsModalVisible(false);
    notification.open({
      key: 'doctor-report',
      message: (
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Icon
            name="description"
            style={{ width: 24, height: 24, marginRight: 20 }}
          />
          <span>{`${PatientName}${
            StudyDescription ? ' | ' + StudyDescription : ''
          }`}</span>
        </div>
      ),
      placement: 'bottomRight',
      duration: 0,
      className: 'collapse-report',
      // onClick: () => {
      //   setIsModalVisible(true);
      // },
    });
  };

  const DoctorModalTitle = () => {
    return (
      <div className="modal-title">
        <div className="modal-title-text">{t('Report_Report')}</div>
        <MinusOutlined className="minimize-icon" onClick={handleMinimize} />
      </div>
    );
  };
  // if (series) {
  return (
    <React.Fragment>
      {/* <Tooltip
        title={'Report'}
        placement="bottom"
        overlayClassName="toolbar-tooltip"
      >
        <div className="toolbar-button" onClick={onShowModal}>
          <Icon name="description" />
        </div>
      </Tooltip> */}
      {isModalVisible && (
        <DraggableModal
          title={<DoctorModalTitle />}
          zIndex={101}
          visible={isModalVisible}
          onCancel={handleCancel}
          maskClosable={false}
          initialWidth={window.innerWidth < 700 ? window.innerWidth - 100 : 600}
          initialHeight={
            window.innerHeight < 800 ? window.innerHeight - 100 : 700
          }
          footer={null}
          className="report-modal"
        >
          <DoctorTab
            StudyInstanceUID={study.StudyInstanceUID}
            reportType="Modal"
          />
        </DraggableModal>
      )}
    </React.Fragment>
  );
}

DoctorModal.propTypes = {
  study: PropTypes.object,
  onCancel: PropTypes.func,
};

export default DoctorModal;
