import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { redux } from '@tuvm/core';
import { useDispatch, useSelector } from 'react-redux';
// import { useParams } from 'react-router-dom';

import { isChestBody, isBreastBody } from '../../utils/common';
// import { actionGetAIResult } from '../../system/customActions';
import {
  actionViewStudy,
  actionGetTemplate,
  actionGetSummary,
  actionGetTenantInfo,
  actionGetLayout,
} from './PanelActions';
import { FEEDBACK_ACTIONS } from '../../utils/constants';
import { FullScreenLoading } from '../../components/Customize/FullScreenLoading';
import PanelTabs from './components/PanelTabs';
import './style.css';

export default function PredictionPanel(props) {
  const user = useSelector(state => state.extensions.user);
  const viewports = useSelector(state => state.viewports);
  const activeViewportIndex = get(viewports, 'activeViewportIndex');
  const studyInstanceUID = get(
    viewports,
    `viewportSpecificData[${activeViewportIndex}].StudyInstanceUID`,
    ''
  );

  const dispatch = useDispatch();
  // const { studyInstanceUID } = useParams();

  useEffect(() => {
    document.title = 'VinDr - Viewer';
    // actionGetAIResult({ studyInstanceUID });
    actionGetSummary({ StudyInstanceUID: studyInstanceUID });
    actionGetTemplate();
    actionGetLayout();
    actionGetTenantInfo({ StudyInstanceUID: studyInstanceUID });
  }, [studyInstanceUID]);

  useEffect(() => {
    actionViewStudy({
      action: FEEDBACK_ACTIONS.VIEWER_OPEN,
      object_type: 'study',
      object_instance_uid: studyInstanceUID,
      global: {},
      local: {},
      metadata: {},
      sr_data: {},
      form_data: {},
    });
  }, [user, studyInstanceUID]);

  useEffect(() => {
    const modality = get(props.studies, '[0].series[0].Modality') || '';
    const bodyPartExamined =
      get(
        props.studies,
        '[0].series[0].instances[0].metadata.BodyPartExamined'
      ) || '';

    let bodyPart = bodyPartExamined;

    dispatch(
      redux.actions.setExtensionData('metadata', {
        bodyPart,
        modality: modality,
        isChestBody: isChestBody(bodyPart),
        isBreastBody: isBreastBody(bodyPart),
      })
    );
  }, [props.studies, dispatch]);

  const series = get(props.studies, '[0].series');

  if (series) {
    return <PanelTabs {...props} StudyInstanceUID={studyInstanceUID} />;
  }

  return <FullScreenLoading />;
}

PredictionPanel.propTypes = {
  studies: PropTypes.array,
};
