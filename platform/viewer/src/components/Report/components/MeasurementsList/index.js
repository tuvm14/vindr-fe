import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { useTranslation } from 'react-i18next';

import { AI_RESULT_SCOPES, AI_MODEL } from '../../../../utils/constants';

import './MeasurementsList.styl';

function MeasurementsList({ title, data, scope, onClickItem, itemIcon }) {
  const { t } = useTranslation('Vindoc');

  const getPrefixName = item => {
    let prefix = null;
    if (item.model === AI_MODEL.MAMMOGRAPHY) {
      prefix = t(`Mammo ${item.position}`) + ': ';
    }
    return prefix;
  };

  return (
    <div className={'measurements-list'}>
      <div className={'measurement-item title'}>{t(title)}</div>
      <div className={'measurement-item-wrapper'}>
        {!isEmpty(data) &&
          data.map((item, index) => (
            <div
              key={item.name + index}
              className={'measurement-item'}
              onClick={() => onClickItem && onClickItem(item)}
            >
              <span
                className={`${
                  scope === AI_RESULT_SCOPES.LOCAL
                    ? 'measurement-item-local'
                    : ''
                }`}
              >
                {scope === AI_RESULT_SCOPES.GLOBAL && getPrefixName(item)}
                {t(item.name)}
                {item.suffix}
                {itemIcon}
              </span>
              {scope === AI_RESULT_SCOPES.GLOBAL && item.value > 0 && (
                <span className={'measurement-item__value'}>
                  {`${item.value}%`}
                </span>
              )}
            </div>
          ))}
        {isEmpty(data) && <div className="measurement-item">-</div>}
      </div>
    </div>
  );
}

MeasurementsList.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array,
  scope: PropTypes.string,
  onClickItem: PropTypes.func,
};

export default React.memo(MeasurementsList);
