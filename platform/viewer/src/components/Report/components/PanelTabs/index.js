import React, { useEffect, useState } from 'react';
// import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Tabs } from 'antd';
// import { getViewportApiKey } from '../../../../utils/helper';
import {
  NEW_SCOPES,
  PANEL_TAB_NAMES,
  SCOPES,
} from '../../../../utils/constants';
import AITab from '../AITab';
import DoctorTab from '../DoctorTab';
// import ReadOnlyTab from '../ReadOnlyTab';
import AnnotationSettings from '../AnnotationSettings';
import { PanelContext } from './context';
import { actionSetExtensionData } from '../../../../system/systemActions';
import { useSelector } from 'react-redux';
import get from 'lodash/get';
import keycloak from '../../../../services/auth';

export let currentPanelTabName;

function PanelTabs(props) {
  const { t } = useTranslation('Vindoc');
  const extensions = useSelector(state => state.extensions);
  const selectedTab = get(extensions, 'predictionPanel.selectedTab');
  const [tabIndex, setTabIndex] = useState(selectedTab);

  useEffect(() => {
    if (selectedTab != tabIndex) {
      setTabIndex(selectedTab);
    }
  }, [selectedTab]);

  const handleChangeTab = index => {
    actionSetExtensionData('predictionPanel', {
      rightPanel: true,
      selectedTab: index,
    });
  };

  // const tempToken = getViewportApiKey();

  const contextValues = {
    tabIndex,
  };

  return (
    <>
      <PanelContext.Provider value={contextValues}>
        <Tabs
          activeKey={tabIndex}
          onChange={tabIndex => handleChangeTab(tabIndex)}
          className="vindr-tabs"
        >
          {/* {!keycloak.hasOneOfPerm([SCOPES.aiResult, SCOPES.reportView]) &&
            tempToken && (
              <Tabs.TabPane
                tab={t('Doctor Result')}
                key={PANEL_TAB_NAMES.DOCTOR_TAB}
              >
                <ReadOnlyTab />
              </Tabs.TabPane>
            )} */}
          {keycloak.hasOneOfPerm([
            SCOPES.aiResult,
            SCOPES.reportView,
            NEW_SCOPES.worklist.cad.view,
            NEW_SCOPES.worklist.report.view,
          ]) && (
            <>
              <Tabs.TabPane
                tab={t('Doctor Result')}
                key={PANEL_TAB_NAMES.DOCTOR_TAB}
                disabled={
                  !keycloak.hasOneOfPerm([
                    SCOPES.reportView,
                    NEW_SCOPES.worklist.report.view,
                  ])
                }
              >
                <DoctorTab StudyInstanceUID={props.StudyInstanceUID} />
              </Tabs.TabPane>

              <Tabs.TabPane
                tab={t('CAD')}
                key={PANEL_TAB_NAMES.AI_TAB}
                disabled={
                  !keycloak.hasOneOfPerm([
                    SCOPES.aiResult,
                    NEW_SCOPES.worklist.cad.view,
                  ])
                }
              >
                <AITab />
                <div
                  style={{
                    position: 'absolute',
                    bottom: 9,
                    width: '100%',
                    padding: '8px 0',
                    boxShadow: '0px -1px 14px rgba(3, 3, 3, 0.17)',
                  }}
                >
                  <AnnotationSettings />
                </div>
              </Tabs.TabPane>
            </>
          )}
        </Tabs>
      </PanelContext.Provider>
    </>
  );
}

export default React.memo(PanelTabs, () => true);
