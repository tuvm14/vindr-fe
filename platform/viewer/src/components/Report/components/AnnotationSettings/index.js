import React, { useState, useMemo, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Switch } from 'antd';
// import { useSnackbarContext } from '@tuvm/ui';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import OHIF from '@tuvm/core';
import cornerstone from 'cornerstone-core';

import { actionSetExtensionData } from '../../../../system/systemActions';

import './AnnotationSettings.styl';

/**
 * General Preferences tab
 * It renders the General Preferences content
 *
 * @param {object} props component props
 * @param {function} props.onClose
 */
function AnnotationSettings({ onClose }) {
  const { t } = useTranslation('Vindoc');
  // const snackbar = useSnackbarContext();
  const settings = useSelector(state => state && state.extensions.settings);

  const [state, setState] = useState({
    isVisibleAnnotation: settings.isVisibleAnnotation,
    isVisibleLocalTag: settings.isVisibleLocalTag,
    isVisibleAllTag: settings.isVisibleAllTag,
  });

  useEffect(() => {
    setState(settings);
  }, [setState, settings]);

  const handleChangeSwitch = useCallback(
    name => event => {
      const checked = event;
      onSave({ [name]: checked });
      setState({ ...state, [name]: checked });
      if (name === 'isVisibleAllTag') {
        handleToggleAllBox(checked);
      }
    },
    [onSave, setState, state, handleToggleAllBox]
  );

  const onSave = useCallback(
    object => {
      const newSettings = {
        ...state,
        ...object,
      };

      actionSetExtensionData('settings', newSettings);
    },
    [state]
  );

  const handleToggleAllBox = useCallback(toggle => {
    const measurementApi = OHIF.measurements.MeasurementApi.Instance;

    Object.keys(measurementApi.tools).forEach(toolType => {
      const measurements = measurementApi.tools[toolType];

      measurements.forEach(measurement => {
        measurement.visible = toggle;
      });
    });

    measurementApi.syncMeasurementsAndToolData();

    cornerstone.getEnabledElements().forEach(enabledElement => {
      cornerstone.updateImage(enabledElement.element);
    });
  }, []);

  // const SwitchShowingAnnotation = useMemo(() => {
  //   return (
  //     <Switch
  //       size="small"
  //       checked={state.isVisibleAnnotation}
  //       onChange={handleChangeSwitch('isVisibleAnnotation')}
  //     />
  //   );
  // }, [state.isVisibleAnnotation, handleChangeSwitch]);

  // const SwitchShowingTagList = useMemo(() => {
  //   return (
  //     <Switch
  //       size="small"
  //       checked={state.isVisibleLocalTag}
  //       onChange={handleChangeSwitch('isVisibleLocalTag')}
  //     />
  //   );
  // }, [state.isVisibleLocalTag, handleChangeSwitch]);

  const SwitchShowingAllTag = useMemo(() => {
    return (
      <Switch
        size="small"
        checked={state.isVisibleAllTag}
        onChange={handleChangeSwitch('isVisibleAllTag')}
      />
    );
  }, [state.isVisibleAllTag, handleChangeSwitch]);

  return (
    <React.Fragment>
      <div className="AnnotationSettings">
        <div className="language setting-item">
          <label htmlFor="all-tag-select" className="languageLabel">
            {t('Show Bounding Boxes')}
          </label>
          {SwitchShowingAllTag}
        </div>

        {/* <div className="language setting-item">
          <label htmlFor="tag-list-select" className="languageLabel">
            {t('Show Tag List')}
          </label>
          {SwitchShowingTagList}
        </div> */}

        {/* <div className="language setting-item">
          <label htmlFor="annotation-select" className="languageLabel">
            {t('Show DICOM info')}
          </label>
          {SwitchShowingAnnotation}
        </div> */}
      </div>
    </React.Fragment>
  );
}

export default React.memo(AnnotationSettings);

AnnotationSettings.propTypes = {
  onClose: PropTypes.func,
};
