import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useReactToPrint } from 'react-to-print';
import moment from 'moment';
import { useSelector } from 'react-redux';
import get from 'lodash/get';

import { reportHtml, reportContent } from '../../ContentSection/TextEditor';
import './Printing.styl';
import {
  actionGetApprovedPreview,
  actionGetPreview,
} from '../../../../PanelActions';
import { Upload, Modal, Space } from 'antd';
import {
  PrinterOutlined,
  CameraOutlined,
  DeleteOutlined,
  EditOutlined,
} from '@ant-design/icons';

export default function MaxWidthDialog(props) {
  const extensions = useSelector(state => state.extensions);
  const tenant = get(extensions, 'predictionPanel.tenant');
  const fullname = get(extensions, 'user.fullname');
  const userTitle = get(extensions, 'user.user_title');
  const signature = get(extensions, 'user.signature');
  const { t } = useTranslation('Vindoc');
  const componentRef = useRef();
  const { StudyInstanceUID, studyApproved } = props;
  const [preview, setPreview] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const reportId = get(extensions, 'predictionPanel.reportId');
  const reportLayoutId = get(extensions, 'predictionPanel.reportLayoutId');

  useEffect(() => {
    getPreview(StudyInstanceUID, reportContent);
    if (signature) {
      setImageUrl(signature);
    }

    return () => {
      document.title = 'VinDr - Viewer';
    };
  }, []);

  useEffect(() => {
    if (tenant) {
      const { Id } = tenant?.Patient || {};
      document.title = `vindr_${Id}_${moment().format('DDMMYYYY_HHmmss')}`;
    } else {
      document.title = `vindr_report_${moment().format('DDMMYYYY_HHmmss')}`;
    }
  }, [tenant]);

  const getPreview = async (StudyInstanceUID, content) => {
    try {
      if (studyApproved) {
        const data = await actionGetApprovedPreview({
          StudyInstanceUID: StudyInstanceUID,
          reportId: reportId,
          content: content,
        });
        setPreview(data);
      } else {
        const data = await actionGetPreview({
          StudyInstanceUID: StudyInstanceUID,
          content: content,
          layoutTemplateID: reportLayoutId,
        });
        setPreview(data);
      }
    } catch (error) {
      setPreview(reportHtml);
    }
  };

  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
    onBeforeGetContent: () => {
      try {
        for (let item of document.querySelectorAll('.tb-edit-text')) {
          item.style.border = 'none';
          item.style.padding = '0';
        }
        for (let item of document.querySelectorAll(
          '.ant-upload-select-picture-card'
        )) {
          item.style.borderWidth = 0;
        }
        document.getElementById('signature').style.right = '0px';
        for (let item of document.querySelectorAll('.signature-text')) {
          item.style.color = '#fff';
          item.style.opacity = 0;
        }
      } catch (error) {
        console.log('hide border failed:', error);
      }
    },
    onAfterPrint: () => {
      try {
        for (let item of document.querySelectorAll('.tb-edit-text')) {
          item.style.removeProperty('border');
          item.style.removeProperty('padding');
        }
        for (let item of document.querySelectorAll(
          '.ant-upload-select-picture-card'
        )) {
          item.style.borderWidth = '1px';
        }
        document.getElementById('signature').style.right = '40px';
        for (let item of document.querySelectorAll('.signature-text')) {
          item.style.color = '#000';
          item.style.opacity = 0.4;
        }
      } catch (error) {
        console.log('hide border failed:', error);
      }
    },
    pageStyle: `@media print {
      body {
        background: #fff;
        font-size: 14px;
        font-family: Roboto;
        color: #000 !important;
      }
      strong, em, p, span {
        font-size: 1em;
      }
      table:not([cellpadding]) td, table:not([cellpadding]) th {
        padding: 3px 0 !important;
      }
      .wrapper .ant-upload-picture-card-wrapper {
        width: auto;
      }
      .ant-upload.ant-upload-select-picture-card {
        background: transparent !important;
      }
   }`,
  });

  const handleClose = () => {
    props.handleClose();
  };

  return (
    <React.Fragment>
      <Modal
        fullWidth={false}
        visible={true}
        style={{
          backgroundColor: '#fff',
          color: '#000',
          paddingBottom: 0,
        }}
        width={800}
        closable={false}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
        className="printing-dialog"
        size={'lg'}
        okText={
          <>
            <PrinterOutlined /> {t('Print')}
          </>
        }
        onOk={handlePrint}
        onCancel={handleClose}
        cancelText={t('Cancel')}
      >
        <div
          className="preview-content"
          style={{ position: 'relative' }}
          ref={componentRef}
        >
          <div dangerouslySetInnerHTML={{ __html: preview }}></div>
          {preview && (
            <div
              className="wrapper"
              id="signature"
              style={{
                position: 'absolute',
                bottom: 0,
                right: 40,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Upload
                name="signature"
                listType="picture-card"
                className="signature-uploader"
                style={{ marginRight: 30 }}
                showUploadList={false}
                action={null}
                beforeUpload={file => {
                  let reader = new FileReader();
                  reader.onload = e => {
                    setImageUrl(e.target.result);
                  };
                  reader.readAsDataURL(file);
                }}
                onChange={data => console.log(data)}
              >
                {imageUrl ? (
                  <>
                    <img
                      className="image"
                      src={imageUrl}
                      alt="signature"
                      style={{ width: '100%' }}
                    />
                    <div
                      className="overlay"
                      style={{
                        width: '100%',
                        height: '100%',
                        position: 'absolute',
                        display: 'none',
                        background: 'rgba(0,0,0,0.5)',
                        justifyContent: 'center',
                        alignItems: 'center',
                        fontSize: 16,
                        transition: 'all 0.3s',
                      }}
                    >
                      <Space size="middle">
                        <EditOutlined className="image-action" />
                        <DeleteOutlined
                          className="image-action"
                          onClick={e => {
                            e.preventDefault();
                            e.stopPropagation();
                            setImageUrl('');
                          }}
                        />
                      </Space>
                    </div>
                  </>
                ) : (
                  <div
                    className="signature-text"
                    style={{ color: '#000', opacity: 0.4 }}
                  >
                    <div style={{ marginTop: 8 }}>Upload Signature</div>
                    <CameraOutlined style={{ fontSize: 20 }} />
                  </div>
                )}
              </Upload>
              <input
                className="tb-edit-text"
                style={{ width: 250, fontSize: 14, textAlign: 'center' }}
                type="text"
                defaultValue={`${userTitle || ''} ${fullname || ''}`}
              />
            </div>
          )}
        </div>
      </Modal>
    </React.Fragment>
  );
}

MaxWidthDialog.propTypes = {
  handleClose: PropTypes.func,
};
