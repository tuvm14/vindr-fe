import React from 'react';
import PropTypes from 'prop-types';
import { Button, message } from 'antd';
import { useTranslation } from 'react-i18next';
import Printing from './Printing';
import { editorSourceHTML, conclusion } from '../ContentSection/TextEditor';
import { useSelector } from 'react-redux';
// import { useParams } from 'react-router-dom';
import get from 'lodash/get';

import {
  actionDoctorStorageV2,
  actionGetSummaryV2,
} from '../../../PanelActions';
import {
  DOCTOR_TAB_ACTIONS,
  NEW_SCOPES,
  REPORT_STATUS,
  SCOPES,
} from '../../../../../utils/constants';
import { actionSetExtensionData } from '../../../../../system/systemActions';
// import { htmlToText } from '../../../../../utils/helper';
import keycloak from '../../../../../services/auth';
import {
  CheckCircleOutlined,
  EyeOutlined,
  SaveOutlined,
} from '@ant-design/icons';

const ActionsSection = props => {
  const { t } = useTranslation('Vindoc');
  const [openPrintDialog, setOpenPrintDialog] = React.useState(false);
  const timepointManager = useSelector(state => state.timepointManager);
  const extensions = useSelector(state => state.extensions);
  // const viewports = useSelector(state => state.viewports);
  // const activeViewportIndex = get(viewports, 'activeViewportIndex');
  const studyInstanceUID = props.StudyInstanceUID;
  // const doctorResult = get(extensions, 'predictionPanel.doctorResult');
  const studyApproved = get(extensions, 'predictionPanel.studyApproved');
  const reportStatus = get(extensions, 'predictionPanel.reportStatus');

  const AccessionNumber = get(
    extensions,
    'predictionPanel.tenant.Patient.AccessionNumber'
  );

  const handlePrint = () => {
    setOpenPrintDialog(!openPrintDialog);
  };

  const handleAction = actionName => {
    const RectangleMeasurements = timepointManager.measurements;
    const activeEditorTemplate = get(
      extensions,
      'predictionPanel.activeEditorTemplate'
    );
    const reportId = get(extensions, 'predictionPanel.reportId');

    const RectangleRoi = RectangleMeasurements.RectangleRoi;

    let measurements = [];

    if (RectangleRoi) {
      measurements = RectangleRoi.map(item => ({
        start: item.handles.start,
        end: item.handles.end,
        labels: item.location,
        imagePath: item.imagePath,
        SOPInstanceUID: item.SOPInstanceUID,
        StudyInstanceUID: item.StudyInstanceUID,
        SeriesInstanceUID: item.SeriesInstanceUID,
        toolType: item.toolType,
      }));
    }

    // const text = htmlToText(editorSourceHTML);
    // const splitText = text.split(/([Kk][Ếế][Tt]\s*[Ll][Uu][Ậậ][Nn]|[Kk][Ll])\s*:*\s*/);
    // const rResult = splitText[splitText.length - 1] || '';
    // const result = rResult.trim();
    // const result = htmlToText(conclusion);

    const payload = {
      reportId: reportId,
      editorSourceHTML,
      activeEditorTemplate: activeEditorTemplate,
      measurements: {
        RectangleRoi: measurements,
      },
      StudyInstanceUID: studyInstanceUID,
      action: actionName,
      AccessionNumber: AccessionNumber || '',
      result: conclusion,
    };

    const notifyMessage =
      actionName === DOCTOR_TAB_ACTIONS.SUBMIT
        ? 'Submit successfully'
        : actionName === DOCTOR_TAB_ACTIONS.APPROVE
        ? 'Approve successfully'
        : 'Revoke successfully';

    if (
      reportStatus == REPORT_STATUS.READING &&
      actionName === DOCTOR_TAB_ACTIONS.APPROVE
    ) {
      // submit then approve
      actionDoctorStorageV2({ ...payload, action: DOCTOR_TAB_ACTIONS.SUBMIT })
        .then(res => {
          actionDoctorStorageV2(payload)
            .then(res => {
              message.success(t(notifyMessage));

              actionSetExtensionData('predictionPanel', {
                lastAction: `${actionName}_${reportId}`,
              });

              actionGetSummaryV2({ StudyInstanceUID: studyInstanceUID });
              // actionSubmitReport({
              //   StudyInstanceUID: studyInstanceUID,
              //   ReportID: reportId,
              // });
            })
            .catch(error => {
              message.error(t('Error Message'));
            });
        })
        .catch(error => {
          message.error(t('Error Message'));
        });
    } else {
      actionDoctorStorageV2(payload)
        .then(res => {
          message.success(t(notifyMessage));
          actionSetExtensionData('predictionPanel', {
            lastAction: `${actionName}_${reportId}`,
          });

          actionGetSummaryV2({ StudyInstanceUID: studyInstanceUID });
          // if (actionName === DOCTOR_TAB_ACTIONS.APPROVE) {
          //   actionSubmitReport({
          //     StudyInstanceUID: studyInstanceUID,
          //     ReportID: reportId,
          //   });
          // }
        })
        .catch(error => {
          message.error(t('Error Message'));
        });
    }
  };

  // useEffect(() => {
  //   if (!isEmpty(doctorResult)) {
  //     const { SUBMIT } = doctorResult;

  //     // const lastAction = getLastDoctorActionResult([
  //     //   SUBMIT,
  //     //   APPROVE,
  //     //   DONE,
  //     //   REVOKE,
  //     // ]);
  //     if (SUBMIT) {
  //       setStudySubmited(true);
  //     } else {
  //       setStudySubmited(false);
  //     }
  //     // if (lastAction && lastAction.action === DOCTOR_TAB_ACTIONS.APPROVE) {
  //     //   actionSetExtensionData('predictionPanel', {
  //     //     studyApproved: true,
  //     //   });
  //     // } else if (
  //     //   lastAction &&
  //     //   lastAction.action === DOCTOR_TAB_ACTIONS.REVOKE
  //     // ) {
  //     //   actionSetExtensionData('predictionPanel', {
  //     //     studyApproved: false,
  //     //   });
  //     // }
  //   }
  // }, [doctorResult]);

  return (
    <div className="panel-doctor-margin">
      <div
        className="button-group"
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          margin: '10px 0',
        }}
      >
        <Button
          type="primary"
          size="small"
          className="report-btn"
          style={{ marginLeft: 15, minWidth: 80 }}
          onClick={() => handleAction(DOCTOR_TAB_ACTIONS.SUBMIT)}
          disabled={
            studyApproved ||
            !keycloak.hasOneOfPerm([
              SCOPES.reportSubmit,
              NEW_SCOPES.worklist.report.submit,
            ])
          }
        >
          <SaveOutlined />
          {t('Submit')}
        </Button>

        {/* {studyApproved ? (
          <Button
            variant="contained"
            type="primary"
            className="report-btn"
            style={{ marginLeft: 15 }}
            onClick={() => {
              handleAction(DOCTOR_TAB_ACTIONS.REVOKE);
              actionSetExtensionData('predictionPanel', {
                studyApproved: false,
              });
            }}
            disabled={!keycloak.hasOneOfPerm([SCOPES.reportRevoke])}
          >
            <CheckCircleOutlined />
            {t('Revoke approval')}
          </Button>
        ) : ( */}
        <Button
          type="primary"
          className="report-btn"
          size="small"
          style={{ marginLeft: 15, minWidth: 80 }}
          onClick={() => handleAction(DOCTOR_TAB_ACTIONS.APPROVE)}
          disabled={
            studyApproved ||
            !keycloak.hasOneOfPerm([
              SCOPES.reportApprove,
              NEW_SCOPES.worklist.report.approve,
            ])
          }
        >
          <CheckCircleOutlined />
          {t('Approve')}
        </Button>

        <Button
          ghost
          className="report-btn"
          size="small"
          style={{ marginLeft: 15, minWidth: 80 }}
          onClick={handlePrint}
        >
          <EyeOutlined />
          {t('Preview')}
        </Button>
      </div>
      {openPrintDialog && (
        <Printing
          handleClose={handlePrint}
          StudyInstanceUID={studyInstanceUID}
          studyApproved={studyApproved}
        />
      )}
    </div>
  );
};

ActionsSection.propTypes = {
  StudyInstanceUID: PropTypes.string.isRequired,
};

export default React.memo(ActionsSection);
