import get from 'lodash/get';
import moment from 'moment';
import {
  isThaiThuyHospital,
  isHungHaHospital,
} from '../../../../../utils/helper';

const getSex = sex => {
  let name = '';
  if (sex) {
    name =
      sex.toLowerCase() === 'm' || sex.toLowerCase() === 'male'
        ? window.t('Male')
        : window.t('Female');
  }

  return name;
};

export function patientInfor(patient = {}) {
  const {
    Name = '',
    Age = '',
    Id = '',
    Address = '',
    StudyDate = '',
    StudyTime = '',
    ReferringPhysicianName = '',
  } = patient;

  return `<div style="margin-bottom: 12px">
      <table style="width:100%; font-size: 14px;" class="tabel-patient-info">
        <tr style="height: 24px">
          <td width="100px" style="padding: 2px 4px ;line-height: normal;">Họ và tên:</td>
          <td style="min-width: 130px; padding: 2px 4px;line-height: normal">
            <input style="width: 100%" class="tb-edit-text" value="${Name}" />
          </td>
          <td width="100px" style="padding: 2px 4px ;line-height: normal">Tuổi:</td>
          <td style="min-width: 130px; padding: 2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="${Age}" /></td>
        </tr>
        <tr style="height: 24px">
          <td style="padding: 2px 4px;line-height: normal">Mã BN:</td>
          <td style="padding: 2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="${Id}" /></td>
          <td style="padding: 2px 4px;line-height: normal">Giới tính:</td>
          <td style="padding:2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="${getSex(
            get(patient, 'Sex')
          )}" /></td>
        </tr>
        <tr style="height: 24px">
          <td style="padding: 2px 4px;line-height: normal">Địa chỉ:</td>
          <td colspan="3" style="padding: 2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="${Address}" /></td>
        </tr>
        <tr style="height: 24px">
          <td style="padding: 2px 4px;line-height: normal">Chỉ định:</td>
          <td style="padding: 2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="" /></td>
          <td style="padding: 2px 4px;line-height: normal">BS chỉ định:</td>
          <td style="padding: 2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="${ReferringPhysicianName}" /></td>
        </tr>
        <tr style="height: 24px">
          <td style="padding: 2px 4px;line-height: normal">Ngày chụp:</td>
          <td colspan="3" style="padding: 2px 4px;line-height: normal">
          <input style="width: 100%" class="tb-edit-text" value="${
            StudyDate
              ? moment(StudyDate + StudyTime, 'YYYYMMDDHHmmss').format(
                  'DD/MM/YYYY HH:mm:ss'
                )
              : ''
          }" />

          </td>
        </tr>
      </table>
      <h2 style="text-align:center; color: #000; font-weight: 700; padding-top: 16px; font-size: 18px; line-height: normal;">KẾT QUẢ</h2>
    </div>`;
}

export function hospitalInfor(hospital = {}) {
  const { Name = '', Department = '' } = hospital;

  if (isThaiThuyHospital()) {
    return `
      <div style="margin-bottom: 12px">
        <div style="display: flex; justify-content: space-between; padding-bottom: 16px">
          <div style="text-align: center; min-width: 150px"><img style="height: 78px;" src="./assets/vindoc/logo_bvdkthaithuy.png" alt="" /></div>
          <div style="flex: 1; text-align: center; color: #000; display: flex; flex-direction: column; justify-content: center;">
            <div style="font-size: 18px; font-weight: 500; line-height: normal;">
              BỆNH VIỆN ĐA KHOA THÁI THỤY
            </div>
            <div style="font-size: 18px; font-weight: 500; line-height: normal;">
            KHOA CHẨN ĐOÁN HÌNH ẢNH
            </div>
            <div style="line-height: normal;">TT. Diêm Điền, Thái Thụy, Thái Bình</div>
          </div>
        </div>
        <h2 style="text-align:center; color: #000; font-weight: 700; font-size: 18px; line-height: normal;">THÔNG TIN CA CHỤP</h2>
      </div>
    `;
  } else if (isHungHaHospital()) {
    return `
      <div style="margin-bottom: 12px">
        <div style="display: flex; justify-content: space-between; padding-bottom: 16px">
          <div style="flex: 1; text-align: center; color: #000; display: flex; flex-direction: column; justify-content: center;">
            <div style="text-align: center; min-width: 150px; margin-bottom: 5px;"><img style="height: 78px;" src="./assets/vindoc/logo-hungha.png" alt="" /></div>
            <div style="font-size: 12px; font-weight: 700; line-height: normal;">
              SỞ Y TẾ THÁI BÌNH
            </div>
            <div style="font-size: 12px; font-weight: 700; line-height: normal;">
              BỆNH VIỆN ĐA KHOA HƯNG HÀ
            </div>
          </div>
          <div style="flex: 1; text-align: center; color: #000; display: flex; flex-direction: column; justify-content: center;">
            <div style="font-size: 18px; font-weight: 700; line-height: normal;">
            KHOA CHẨN ĐOÁN HÌNH ẢNH
            </div>
            <div style="line-height: normal; font-style: italic;">Địa chỉ: Xã Minh Khai, huyện Hưng Hà, tỉnh Thái Bình</div>
            <div style="line-height: normal; font-style: italic;">Điện thoại: 0227 3861658</div>
            <div style="line-height: normal; font-style: italic;">Email: bvdkhh2017@gmail.com</div>
          </div>
        </div>
        <h2 style="text-align:center; color: #000; font-weight: 700; font-size: 18px; line-height: normal;">PHIẾU CHỤP CẮT LỚP VI TÍNH</h2>
      </div>
    `;
  } else {
    return `
      <div style="margin-bottom: 12px">
        ${
          Name || Department
            ? `<div style="display: flex; justify-content: space-between; padding-bottom: 16px">
          <div style="flex: 1; text-align: center; color: #000;display: flex; flex-direction: column; justify-content: center;">
            <div style="font-size: 18px; font-weight: 500; line-height: normal;">
              ${Name}
            </div>
            <div style="font-size: 18px; font-weight: 500; line-height: normal;">
            ${Department}
            </div>
          </div>
        </div>`
            : ''
        }
        <h2 style="text-align:center; color: #000; font-weight: 700; font-size: 18px; line-height: normal;">THÔNG TIN CA CHỤP</h2>
      </div>
    `;
  }
}

export function doctorInfor(doctor = {}) {
  const today = new Date();
  const { Position = '', LastName = '', FirstName = '' } = doctor;
  return `
  <div style="display: flex; justify-content: space-between; margin-top: 16px; font-size: 14px;">
    <div>

    </div>
    <div style="text-align: center;"><input class="tb-edit-text" style="min-width: 250px; text-align: center;" type="text" value="${
      isThaiThuyHospital()
        ? 'Thái Thụy, '
        : isHungHaHospital()
        ? 'Hưng Hà, '
        : ''
    }Ngày ${today.getDate()} tháng ${today.getMonth() +
    1} năm ${today.getFullYear()}" />
      <div>Bác sĩ chuyên khoa</div>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <p style="font-weight: 500; line-height: normal;">${Position} ${LastName} ${FirstName}</p>
    </div>
  </div>
  `;
}

export const defaultContent = `
<p><strong>KỸ THUẬT:</strong></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><strong>M&Ocirc; TẢ:</strong></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><strong>KẾT LUẬN:</strong></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><strong>LỜI DẶN CỦA B&Aacute;C SĨ:</strong></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>
`;
