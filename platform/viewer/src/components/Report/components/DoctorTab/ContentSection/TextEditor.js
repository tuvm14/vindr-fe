import React, { useEffect, useState } from 'react';
import 'tinymce/tinymce';
import 'tinymce/icons/default';
import 'tinymce/themes/silver';
import 'tinymce/plugins/print';
import 'tinymce/plugins/preview';
import 'tinymce/plugins/paste';
import 'tinymce/plugins/importcss';
import 'tinymce/plugins/searchreplace';
import 'tinymce/plugins/autolink';
import 'tinymce/plugins/autosave';
import 'tinymce/plugins/save';
import 'tinymce/plugins/directionality';
import 'tinymce/plugins/code';
import 'tinymce/plugins/visualblocks';
import 'tinymce/plugins/visualchars';
import 'tinymce/plugins/fullscreen';
import 'tinymce/plugins/image';
import 'tinymce/plugins/link';
import 'tinymce/plugins/media';
import 'tinymce/plugins/template';
import 'tinymce/plugins/codesample';
import 'tinymce/plugins/table';
import 'tinymce/plugins/charmap';
import 'tinymce/plugins/hr';
import 'tinymce/plugins/pagebreak';
import 'tinymce/plugins/nonbreaking';
import 'tinymce/plugins/anchor';
import 'tinymce/plugins/toc';
import 'tinymce/plugins/insertdatetime';
import 'tinymce/plugins/advlist';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/wordcount';
import 'tinymce/plugins/imagetools';
import 'tinymce/plugins/textpattern';
import 'tinymce/plugins/noneditable';
import 'tinymce/plugins/help';
import 'tinymce/plugins/quickbars';
import 'tinymce/skins/ui/oxide/skin.min.css';
import 'tinymce/skins/ui/oxide/content.min.css';
import 'tinymce/skins/content/default/content.min.css';
import { Editor } from '@tinymce/tinymce-react';
import PropTypes from 'prop-types';
import { patientInfor, hospitalInfor, doctorInfor } from './Report';
import { useSelector } from 'react-redux';
import get from 'lodash/get';
import './TextEditor.css';
import { Collapse } from 'antd';
import { useTranslation } from 'react-i18next';

const { Panel } = Collapse;

let hospital;
let patient;
let doctor;

const setReportHtml = content => {
  reportContent = content;
  reportHtml =
    '<div style="font-family: TimesNewRoman,Times New Roman,Times,Arial,Baskerville,Georgia,serif; ">' +
    hospital +
    patient +
    content +
    doctor +
    '</div>';
};

const setContent = content => {
  editorSourceHTML = content || '';
  setReportHtml(content || '');
};

const setConclusion = text => {
  conclusion = text || '';
};

const txtContentStyle = `
  body {
    font-family: Times New Roman;
    margin: 8px 4px !important;
    font-size: 14pt;
    color: #000000d9;
  }
  p {
    margin: 5px 0;
  }

  ::-webkit-scrollbar {
    width: 6px;
    height: 10px;
    transition: 0.2s ease-in-out;
    cursor: pointer;
    background: transparent;
  }

  ::-webkit-scrollbar-track {
    border-radius: 6px;
    background: transparent;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 6px;
    background: #586472;
    cursor: pointer;
  }

  ::-webkit-scrollbar-thumb:hover {
    cursor: pointer;
  }
`;

const TextEditor = props => {
  const [state, setState] = useState(null);
  const [result, setResult] = useState(null);
  const [expandResult, setExpandResult] = useState(true);
  const { t } = useTranslation('Vindoc');
  const extensions = useSelector(state => state.extensions);
  const studyApproved = get(extensions, 'predictionPanel.studyApproved');

  useEffect(() => {
    if (props.tenant) {
      const { Hospital, Doctor, Patient } = props.tenant;
      hospital = hospitalInfor(Hospital, Patient);
      patient = patientInfor(Patient, Hospital);
      doctor = doctorInfor(Doctor);
    }
    setState(props.content);
    setContent(props.content);
    setResult(props?.conclusion);
    setConclusion(props?.conclusion);
  }, [props]);

  const onEditorStateChange = textContent => {
    setContent(textContent);
    setState(textContent);
  };

  const onConclusionChange = text => {
    setConclusion(text);
    setResult(text);
  };

  return (
    <>
      <div
        className="main-report"
        style={{
          height: expandResult ? 'calc(100% - 220px)' : 'calc(100% - 50px)',
          transition: 'height 0.3s ease-in-out',
        }}
      >
        <Editor
          value={state}
          disabled={studyApproved}
          className="report"
          init={{
            skin: false,
            content_css: false,
            height: '100%',
            menubar: false,
            toolbar_mode: 'sliding',
            fontsize_formats: '8pt 10pt 12pt 14pt 16pt 18pt 24pt 36pt 48pt',
            content_style: txtContentStyle,
            plugins: [
              'paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap',
            ],
            toolbar:
              'undo redo | bold italic underline strikethrough | fullscreen | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | insertfile image media template link anchor codesample | ltr rtl',
          }}
          onEditorChange={onEditorStateChange}
        />
      </div>
      <Collapse
        activeKey={expandResult ? ['1'] : []}
        expandIconPosition="right"
        className="report-result"
        onChange={key => setExpandResult(key && key.length > 0)}
      >
        <Panel
          header={<span className="patient-info">{t('Conclusion')}</span>}
          className="list-thumbnails"
          key={1}
        >
          <Editor
            value={result}
            disabled={studyApproved}
            init={{
              skin: false,
              content_css: false,
              height: '170px',
              menubar: false,
              toolbar_mode: 'sliding',
              fontsize_formats: '8pt 10pt 12pt 14pt 16pt 18pt 24pt 36pt 48pt',
              content_style: txtContentStyle,
              plugins: [
                'paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap',
              ],
              toolbar:
                'undo redo | bold italic underline strikethrough | fullscreen | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | insertfile image media template link anchor codesample | ltr rtl',
            }}
            onEditorChange={onConclusionChange}
          />
        </Panel>
      </Collapse>
    </>
  );
};

export default TextEditor;

TextEditor.propTypes = {
  content: PropTypes.string,
  conclusion: PropTypes.string,
  tenant: PropTypes.object,
};

export let editorSourceHTML;

export let conclusion;

export let reportHtml;

export let reportContent;
