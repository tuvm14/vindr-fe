import React from 'react';
import PropTypes from 'prop-types';
import DragModal from '../../DragModal';
import './ModalDialog.css';

export default function CustomizedDialogs(props) {
  return (
    <div>
      <DragModal
        left={props.left}
        top={props.top}
        width={props.width}
        zIndex={102}
        title={props.title}
        visible={props.open}
        onCancel={props.handleClose}
        maskClosable={false}
        footer={null}
        className="dialog-modal"
      >
        {props.content}
      </DragModal>
    </div>
  );
}

CustomizedDialogs.propTypes = {
  handleClose: PropTypes.func,
  content: PropTypes.any,
  title: PropTypes.string,
  contentStyles: PropTypes.object,
  open: PropTypes.bool,
};
