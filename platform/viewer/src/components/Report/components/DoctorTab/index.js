import React from 'react';
import PropTypes from 'prop-types';

import './DoctorTab.styl';

import HeaderSection from './HeaderSection';
import ContentSection from './ContentSection';
import ActionsSection from './ActionsSection';
import { useSelector } from 'react-redux';
import get from 'lodash/get';

const DoctorTab = props => {
  const extensions = useSelector(state => state.extensions);
  const reportId = get(extensions, 'predictionPanel.reportId');

  return (
    <div className="panel-tab-wrapper">
      <HeaderSection
        reportType={props.reportType || 'Sidebar'}
        StudyInstanceUID={props.StudyInstanceUID}
      />
      {reportId && (
        <>
          <ContentSection />
          <ActionsSection StudyInstanceUID={props.StudyInstanceUID} />
        </>
      )}
    </div>
  );
};

DoctorTab.propTypes = {
  StudyInstanceUID: PropTypes.string.isRequired,
};

export default DoctorTab;
