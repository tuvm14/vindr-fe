import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Input,
  Button,
  Descriptions,
  Modal,
  Select,
  Space,
  message,
} from 'antd';
import { Tooltip } from '@tuvm/ui';
import { useTranslation } from 'react-i18next';
import ModalDialog from '../ModalDialog';
import OrderInformation from './OrderInformation';
import TemplateTree from './TemplateTree';
import { useSelector } from 'react-redux';
import get from 'lodash/get';
import moment from 'moment';
import {
  CaretDownOutlined,
  CloseOutlined,
  CopyOutlined,
  EditOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import {
  actionDoctorStorageV2,
  getListReportV2,
  updateOrderReportV2,
} from '../../../PanelActions';
import { actionSetExtensionData } from '../../../../../system/systemActions';
import {
  DOCTOR_TAB_ACTIONS,
  REPORT_STATUS,
} from '../../../../../utils/constants';
import { utils } from '@tuvm/core';
import './HeaderSection.styl';

const { studyMetadataManager } = utils;

const HeaderSection = props => {
  const { t } = useTranslation('Vindoc');
  const extensions = useSelector(state => state.extensions);
  // const viewports = useSelector(state => state.viewports);
  // const activeViewportIndex = get(viewports, 'activeViewportIndex');
  const { StudyInstanceUID, reportType } = props;
  // const { StudyInstanceUID, PatientName } = study;
  // const studyInstanceUID = get(
  //   viewports,
  //   `viewportSpecificData[${activeViewportIndex}].StudyInstanceUID`,
  //   ''
  // );
  const tenant = get(extensions, 'predictionPanel.tenant');
  // const modality = get(extensions, 'metadata.modality');
  const Patient = get(tenant, 'Patient');
  const patientName = get(Patient, 'Name');
  // const studyDate = get(Patient, 'StudyDate');
  // const studyDescription = get(Patient, 'StudyDescription');
  const studyApproved = get(extensions, 'predictionPanel.studyApproved');
  const lastAction = get(extensions, 'predictionPanel.lastAction');
  const reportLayouts = get(extensions, 'predictionPanel.reportLayouts') || [];
  const [selectedLayout, setSelectedLayout] = useState(null);
  const [openTemplate, setOpenTempplate] = useState(false);
  const [openOrderInfo, setOpenOrderInfo] = useState(false);
  const [selectedReport, setSelectedReport] = useState(null);
  const [selectedStudy, setSelectedStudy] = useState(null);
  const [reportList, setReportList] = useState([]);
  const [isCreatingReport, setIsCreatingReport] = useState(false);
  const [editOrderOpen, setEditOrderOpen] = useState(false);
  const [tempOrder, setTempOrder] = useState('');
  const [
    AdmittingDiagnosesDescription,
    setAdmittingDiagnosesDescription,
  ] = useState('');

  useEffect(() => {
    getReportList();
  }, [lastAction, StudyInstanceUID]);

  useEffect(() => {
    if (!selectedLayout && reportLayouts && reportLayouts.length) {
      handleChangeLayout(reportLayouts[0]?.id);
    }
  }, [reportLayouts]);

  useEffect(() => {
    if (selectedStudy && selectedStudy.StudyInstanceUID) {
      const studyMetadata = studyMetadataManager.get(
        selectedStudy.StudyInstanceUID
      );
      const studyData = (studyMetadata && studyMetadata.getData()) || {};
      setAdmittingDiagnosesDescription(studyData.AdmittingDiagnosesDescription);
    }
  }, [selectedStudy]);

  useEffect(() => {
    if (selectedReport && selectedReport.id) {
      actionSetExtensionData('predictionPanel', {
        reportId: selectedReport.id,
        doctorTabContent: {
          editorSourceHTML: selectedReport.content,
          conclusion: selectedReport.result,
        },
        studyApproved: selectedReport.status == REPORT_STATUS.APPROVED,
        reportStatus: selectedReport.status,
      });
    } else {
      actionSetExtensionData('predictionPanel', {
        reportId: '',
        doctorTabContent: {},
        studyApproved: false,
      });
    }
  }, [selectedReport]);

  const getReportList = async () => {
    try {
      const data = await getListReportV2(StudyInstanceUID);
      setReportList(
        data.sort((a, b) => {
          if (a.study.StudyInstanceUID == StudyInstanceUID) return -1;
          if (b.study.StudyInstanceUID == StudyInstanceUID) return 1;
          return 0;
        })
      );
      const currentStudyReports = data.find(
        it => it.study.StudyInstanceUID == StudyInstanceUID
      );
      if (
        currentStudyReports &&
        currentStudyReports.reports &&
        currentStudyReports.reports.length
      ) {
        setSelectedStudy(currentStudyReports.study);
        const creatingReport = currentStudyReports.reports.find(
          it => it.status == 'READING'
        );
        if (creatingReport) {
          const studyMetadata = studyMetadataManager.get(StudyInstanceUID);
          const studyData = (studyMetadata && studyMetadata.getData()) || {};
          setIsCreatingReport(true);
          setSelectedReport(creatingReport);
          await updateOrderReportV2(creatingReport.id, {
            order: creatingReport?.order || tempOrder,
            final_content: studyData.AdmittingDiagnosesDescription,
            layout_template_id: selectedLayout?.id || '',
          });
        } else {
          setIsCreatingReport(false);
          if (selectedReport) {
            const report = currentStudyReports.reports.find(
              it => it.id == selectedReport.id
            );
            setSelectedReport(report);
          } else {
            setSelectedReport(currentStudyReports.reports[0]);
          }
        }
      } else {
        setSelectedReport(null);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const handleOpenTemplate = () => {
    setOpenTempplate(!openTemplate);
  };

  const handleOpenOrder = () => {
    setOpenOrderInfo(!openOrderInfo);
  };

  const handleChangeReport = value => {
    reportList.forEach(study => {
      const report = study.reports.find(r => r.id === value);
      if (report) {
        setSelectedStudy(study.study);
        setSelectedReport(report);
      }
    });
  };

  const handleChangeLayout = value => {
    const layout = reportLayouts.find(it => it.id === value);
    if (layout) {
      setSelectedLayout(layout);
      actionSetExtensionData('predictionPanel', {
        reportLayoutId: layout.id,
      });
    }
  };

  const handleCreateReport = () => {
    const payload = {
      StudyInstanceUID: StudyInstanceUID,
      action: DOCTOR_TAB_ACTIONS.CREATE_REPORT,
    };
    actionDoctorStorageV2({ ...payload })
      .then(res => {
        getReportList();
        setIsCreatingReport(true);
        handleOpenTemplate();
        // setEditOrderOpen(true);
      })
      .catch(error => {
        message.error(t('Error Message'));
      });
  };
  const handleCancelCreateReport = () => {
    const payload = {
      reportId: selectedReport.id,
      StudyInstanceUID: StudyInstanceUID,
      action: DOCTOR_TAB_ACTIONS.CANCEL_REPORT,
    };
    actionDoctorStorageV2({ ...payload })
      .then(res => {
        getReportList();
        setIsCreatingReport(false);
      })
      .catch(error => {
        message.error(t('Error Message'));
      });
  };

  return (
    <div className="panel-doctor-margin">
      <div className="header-info" style={{ marginBottom: 12 }}>
        <span>{selectedStudy?.PatientName || patientName}</span>
        <span
          style={{
            marginLeft: 8,
            color: 'var(--active-color)',
            cursor: 'pointer',
          }}
          onClick={handleOpenOrder}
        >
          {t('More info')}
        </span>
      </div>
      <div className="select-report">
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            marginBottom: 5,
          }}
        >
          <span style={{ fontSize: 14 }}>{t('Report List')}</span>
          {!isCreatingReport ? (
            <Button
              size="small"
              style={{ minWidth: 100 }}
              type="primary"
              onClick={handleCreateReport}
            >
              <PlusOutlined />
              {t('New Report')}
            </Button>
          ) : (
            <Button
              size="small"
              type="primary"
              style={{ width: 100 }}
              onClick={handleCancelCreateReport}
            >
              <CloseOutlined />
              {t('Cancel')}
            </Button>
          )}
        </div>
        <Select
          size="small"
          defaultValue={selectedReport?.id}
          value={selectedReport?.id}
          style={{ width: '100%', marginBottom: '10px' }}
          placeholder={t('Select Report')}
          optionLabelProp="label"
          onChange={handleChangeReport}
          dropdownClassName="dropdown-options-dark vindr-dropdown"
          suffixIcon={<CaretDownOutlined />}
        >
          {reportList.map(study => {
            const label = `${study.study.Modality[0]} ${moment(
              study.study.StudyDate.slice(0, 8)
            ).format('DD-MM-YYYY')}`;
            let reports = study.reports;
            if (study.study.StudyInstanceUID != StudyInstanceUID) {
              reports = study.reports.filter(
                it => it.status == REPORT_STATUS.APPROVED
              );
            }
            if (!(reports && reports.length)) return null;

            return (
              <Select.OptGroup key={study.study.StudyInstanceUID} label={label}>
                {reports.map((item, idx) => (
                  <Select.Option
                    value={item.id}
                    key={item.id}
                    label={`${label} / ${item.order || item.id}`}
                    className={
                      study.study.StudyInstanceUID == StudyInstanceUID &&
                      idx == reports.length - 1
                        ? 'main-item'
                        : ' '
                    }
                  >
                    {item.order || item.id}
                  </Select.Option>
                ))}
              </Select.OptGroup>
            );
          })}
        </Select>
      </div>

      {selectedReport && (
        <div
          className="button-group"
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: 7,
            background: 'var(--box-bg-400)',
            borderRadius: 2,
          }}
        >
          <Descriptions size="small" column={1}>
            <Descriptions.Item label={t('Order')}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  width: '100%',
                }}
              >
                <span>{selectedReport.order}</span>
                <Space style={{ fontSize: 16 }}>
                  <Tooltip title={t('Edit Order')} arrowPointAtCenter>
                    <EditOutlined onClick={() => setEditOrderOpen(true)} />
                  </Tooltip>
                  <Tooltip
                    title={t('Select Template')}
                    arrowPointAtCenter
                    overlayClassName="open-template-tooltip"
                  >
                    <CopyOutlined
                      onClick={handleOpenTemplate}
                      disabled={studyApproved}
                    />
                  </Tooltip>
                </Space>
              </div>
            </Descriptions.Item>
            <Descriptions.Item label={t('AdmittingDiagnosesDescription')}>
              {t(
                isCreatingReport
                  ? AdmittingDiagnosesDescription
                  : selectedReport.final_content ||
                      (selectedStudy.StudyInstanceUID == StudyInstanceUID
                        ? AdmittingDiagnosesDescription
                        : '')
              )}
            </Descriptions.Item>
            <Descriptions.Item label={t('Status')}>
              {t(selectedReport.status)}
            </Descriptions.Item>
            <Descriptions.Item label={t('Submitted by')}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  width: '100%',
                }}
              >
                <span>{selectedReport.submit_by}</span>{' '}
                {selectedReport.submit_by && selectedReport.submit_at && (
                  <span>
                    {moment(selectedReport.submit_at).format(
                      'DD-MM-YYYY HH:mm:ss'
                    )}
                  </span>
                )}
              </div>
            </Descriptions.Item>
            <Descriptions.Item label={t('Approved by')}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  width: '100%',
                }}
              >
                <span>{selectedReport.approved_by}</span>
                {selectedReport.approved_by && selectedReport.approved_at && (
                  <span>
                    {moment(selectedReport.approved_at).format(
                      'DD-MM-YYYY HH:mm:ss'
                    )}
                  </span>
                )}
              </div>
            </Descriptions.Item>
            <Descriptions.Item label={t('Layout')}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  width: '100%',
                }}
              >
                <Select
                  size="small"
                  defaultValue={
                    reportLayouts && reportLayouts.length
                      ? reportLayouts[0].id
                      : null
                  }
                  value={selectedLayout?.id}
                  style={{ width: '100%' }}
                  placeholder={t('Select layout')}
                  optionLabelProp="label"
                  onChange={handleChangeLayout}
                  dropdownClassName="dropdown-options-dark vindr-dropdown"
                  suffixIcon={<CaretDownOutlined />}
                >
                  {reportLayouts.map(it => (
                    <Select.Option value={it.id} key={it.id} label={it.name}>
                      {it.name}
                    </Select.Option>
                  ))}
                </Select>
              </div>
            </Descriptions.Item>
            {/* <Descriptions.Item label="Creation Time">2017-01-10</Descriptions.Item>
            <Descriptions.Item label="Effective Time">2017-10-10</Descriptions.Item> */}
          </Descriptions>
          {/* <Button
            type="primary"
            style={{ minWidth: '48%' }}
            className="report-header-btn"
            onClick={handleOpenTemplate}
            disabled={studyApproved}
          >
            <SnippetsOutlined />
            {t('Select Template')}
          </Button> */}
          {/* <Button
            ghost
            style={{ minWidth: '48%' }}
            className="report-header-btn"
            onClick={handleOpenOrder}
          >
            <InfoCircleOutlined />
            {t('Order Report')}
          </Button> */}
        </div>
      )}

      <Modal
        title={t('Edit Order')}
        visible={editOrderOpen}
        onCancel={() => setEditOrderOpen(false)}
        onOk={() => {
          setEditOrderOpen(false);
          if (selectedReport.id) {
            setSelectedReport({ ...selectedReport, order: tempOrder });
            updateOrderReportV2(selectedReport.id, { order: tempOrder })
              .then(() => {
                getReportList();
              })
              .catch(err => {});
          }
        }}
      >
        <Input.Group compact>
          <Input
            style={{ width: '100%', height: 32 }}
            value={tempOrder}
            placeholder={t('Enter Order')}
            onChange={e => setTempOrder(e.target.value)}
          ></Input>
          {/* <Select
            style={{ width: '30%' }}
            value={t('Select Order')}
            onChange={value => setTempOrder(value)}
            dropdownClassName="dropdown-options-dark vindr-dropdown"
          >
            <Select.Option value="CT">CT</Select.Option>
            <Select.Option value="Chest">Chest</Select.Option>
          </Select> */}
        </Input.Group>
      </Modal>

      {openTemplate && (
        <ModalDialog
          left={
            reportType == 'Modal'
              ? (window.innerWidth + 600) / 2
              : window.innerWidth - 772
          }
          top={(window.innerHeight - 700) / 2}
          width={400}
          open={openTemplate}
          handleClose={handleOpenTemplate}
          title={t('Select Template')}
          content={<TemplateTree />}
        />
      )}
      {openOrderInfo && (
        <ModalDialog
          left={
            reportType == 'Modal'
              ? (window.innerWidth + 600) / 2
              : window.innerWidth - 772
          }
          top={(window.innerHeight - 700) / 2}
          width={400}
          open={openOrderInfo}
          handleClose={handleOpenOrder}
          title={t('Patient Information')}
          content={<OrderInformation study={selectedStudy} />}
        />
      )}
    </div>
  );
};

HeaderSection.propTypes = {
  StudyInstanceUID: PropTypes.string.isRequired,
};

export default React.memo(HeaderSection, () => true);
