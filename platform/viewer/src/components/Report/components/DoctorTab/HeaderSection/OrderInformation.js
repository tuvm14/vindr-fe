import React from 'react';
import isEmpty from 'lodash/isEmpty';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import get from 'lodash/get';
// import { utils } from '@tuvm/core';

// const { studyMetadataManager } = utils;

export default function OrderInformation(props) {
  const extensions = useSelector(state => state.extensions);
  // const viewports = useSelector(state => state.viewports);
  const { t } = useTranslation('Vindoc');
  const tenant = get(extensions, 'predictionPanel.tenant');

  // const { activeViewportIndex, viewportSpecificData } = viewports;
  // const { StudyInstanceUID } = viewportSpecificData[activeViewportIndex] || {};

  let demoData = {};

  if (props.study && props.study.StudyInstanceUID) {
    // const studyMetadata = studyMetadataManager.get(
    //   props.study.StudyInstanceUID
    // );
    // const studyData = (studyMetadata && studyMetadata.getData()) || {};

    // const { Patient = {} } = tenant;
    // const { Age, Id, Name, Sex } = Patient;
    const { study } = props;
    const { PatientName, PatientID, PatientAge, PatientSex } = study || {};

    demoData = {
      Name: PatientName,
      Id: PatientID,
      Age: PatientAge,
      Sex: PatientSex,
      // AdmittingDiagnosesDescription: studyData.AdmittingDiagnosesDescription,
    };
  } else {
    // const { activeViewportIndex, viewportSpecificData } = viewports;
    // const { StudyInstanceUID } =
    //   viewportSpecificData[activeViewportIndex] || {};

    // const studyMetadata = studyMetadataManager.get(StudyInstanceUID);
    // const studyData = (studyMetadata && studyMetadata.getData()) || {};
    const { Patient = {} } = tenant;
    const { Age, Id, Name, Sex } = Patient;
    demoData = {
      Name: Name,
      Id: Id,
      Age: Age,
      Sex: Sex,
      // AdmittingDiagnosesDescription: studyData.AdmittingDiagnosesDescription,
    };
  }

  return (
    <table style={{ width: '100%', margin: 8 }}>
      <tbody>
        {!isEmpty(demoData) &&
          Object.keys(demoData).map((key, index) => (
            <tr
              key={index}
              style={{
                lineHeight: '24px',
                verticalAlign: 'top',
                height: 24,
                boxShadow: 'inset 0px -1px 0px rgba(93, 105, 121, 0.25)',
              }}
            >
              <td style={{ width: '27%' }}>
                <strong>{t(key)}</strong>
              </td>
              <td style={{ width: '3%' }}>:</td>
              <td style={{ width: '70%' }}>{demoData[key]}</td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
