import React, {
  useEffect,
  useState,
  useContext,
  useCallback,
  useRef,
} from 'react';
import { useSelector } from 'react-redux';
import { get, isEmpty } from 'lodash';
import cornerstone from 'cornerstone-core';
import Feedback from '../Feedback';
import {
  clearMeasurementData,
  drawMeasurements,
} from '../../../../utils/helper';
import MeasurementsList from '../MeasurementsList';
import cloneDeep from 'lodash/cloneDeep';
import {
  AI_RESULT_SCOPES,
  AI_MODEL,
  MAMMO_GLOBAL_KEY,
  PANEL_TAB_NAMES,
  SPECIAL_CONCLUSIONS,
} from '../../../../utils/constants';
import { VINDR_AI_SCOPES } from '../../../../utils/constants';
import { AIAnalyze } from '../../../AIAnalyze';
import { PanelContext } from '../PanelTabs/context';
import { formatMeasurementsData } from '../../ProcessData';
import { Icon } from '@tuvm/ui';
import { jumpToImage, isDiff } from '../../../../utils/helper';
import { useTranslation } from 'react-i18next';
import './AITab.css';

let interval;

const AI_ERROR = {
  NO_RESULT: 'No Result',
  INVALID: 'Invalid image',
};

function AITab() {
  const extensions = useSelector(state => state.extensions);
  const viewports = useSelector(state => state.viewports);
  const aiResult = get(extensions, 'predictionPanel.aiResult', {});
  const { t } = useTranslation('Vindoc');
  const settings = useSelector(state => state && state.extensions.settings);
  const { tabIndex } = useContext(PanelContext);

  const { activeViewportIndex, viewportSpecificData } = viewports;
  const { StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID } =
    viewportSpecificData[activeViewportIndex] || {};

  const [selectedModel, setSelectedModel] = useState(null);
  const [globalSummary, setGlobalSummary] = useState([]);
  const [localSummary, setLocalSummary] = useState([]);
  const [aiError, setAiError] = useState(null);

  const prevAIResult = usePreviousValue(aiResult);
  const prevStudyInstanceUID = usePreviousValue(StudyInstanceUID);
  const prevSeriesInstanceUID = usePreviousValue(SeriesInstanceUID);
  const prevSOPInstanceUID = usePreviousValue(SOPInstanceUID);
  const prevtabIndex = usePreviousValue(tabIndex);
  const prevtabSelectedModel = usePreviousValue(selectedModel);

  const handleUpdateSelectedModel = useCallback(value => {
    setSelectedModel(value);
  }, []);

  useEffect(() => {
    if (
      tabIndex === PANEL_TAB_NAMES.AI_TAB &&
      selectedModel &&
      !isEmpty(aiResult) &&
      !isEmpty(viewports)
    ) {
      const currentStudyAIResult = aiResult[StudyInstanceUID];
      const seriesAIData = get(currentStudyAIResult, 'series', {});
      const currentSeriesAIResult = seriesAIData[SeriesInstanceUID] || {};

      if (VINDR_AI_SCOPES.STUDY_SCOPE.includes(selectedModel)) {
        if (
          isDiff(prevtabIndex, tabIndex) ||
          isDiff(prevAIResult, aiResult) ||
          isDiff(prevtabSelectedModel, selectedModel) ||
          isDiff(prevStudyInstanceUID, StudyInstanceUID)
        ) {
          const studyResult = get(currentStudyAIResult, 'ai_result.result', {});
          if (isEmpty(studyResult)) {
            const message = get(currentStudyAIResult, 'ai_result.message');
            if (message) {
              setAiError(message);
            } else {
              setAiError(AI_ERROR.NO_RESULT);
            }
          } else {
            setAiError(null);
          }

          let globalList = getGlobalSummary(studyResult, selectedModel);

          const { localList, measurementList } = getLocalSummary(
            currentStudyAIResult,
            selectedModel,
            StudyInstanceUID
          );

          drawBox({ RectangleRoi: formatMeasurementsData(measurementList) });
          setLocalSummary(localList);
          setGlobalSummary(globalList);
        }
      } else if (VINDR_AI_SCOPES.SERIES_SCOPE.includes(selectedModel)) {
        if (
          isDiff(prevtabIndex, tabIndex) ||
          isDiff(prevAIResult, aiResult) ||
          isDiff(prevtabSelectedModel, selectedModel) ||
          isDiff(prevStudyInstanceUID, StudyInstanceUID) ||
          isDiff(prevSeriesInstanceUID, SeriesInstanceUID)
        ) {
          const seriesResult = get(
            currentSeriesAIResult,
            'ai_result.result',
            {}
          );
          if (isEmpty(seriesResult)) {
            const message = get(currentSeriesAIResult, 'ai_result.message');
            if (message) {
              setAiError(message);
            } else {
              setAiError(AI_ERROR.NO_RESULT);
            }
          } else {
            setAiError(null);
          }
          let globalList = getGlobalSummary(seriesResult, selectedModel);

          const { localList, measurementList } = getLocalSummary(
            {
              series: {
                [SeriesInstanceUID]: currentSeriesAIResult,
              },
            },
            selectedModel,
            StudyInstanceUID
          );

          drawBox({ RectangleRoi: formatMeasurementsData(measurementList) });
          setLocalSummary(localList);
          setGlobalSummary(globalList);
        }
      } else if (VINDR_AI_SCOPES.INSTANCE_SCOPE.includes(selectedModel)) {
        if (
          isDiff(prevtabIndex, tabIndex) ||
          isDiff(prevAIResult, aiResult) ||
          isDiff(prevtabSelectedModel, selectedModel) ||
          isDiff(prevStudyInstanceUID, StudyInstanceUID) ||
          isDiff(prevSeriesInstanceUID, SeriesInstanceUID) ||
          isDiff(prevSOPInstanceUID, SOPInstanceUID)
        ) {
          const instancesAIData = get(currentSeriesAIResult, 'instances', {});
          const currentInstanceAIResult = instancesAIData[SOPInstanceUID] || {};

          const instanceResult = get(
            currentInstanceAIResult,
            'ai_result.result',
            {}
          );
          if (isEmpty(instanceResult)) {
            const message = get(currentInstanceAIResult, 'ai_result.message');
            if (message) {
              setAiError(message);
            } else {
              setAiError(AI_ERROR.NO_RESULT);
            }
          } else {
            setAiError(null);
          }
          let globalList = getGlobalSummary(instanceResult, selectedModel);

          const { localList, measurementList } = getLocalSummary(
            {
              series: {
                [SeriesInstanceUID]: {
                  ...currentSeriesAIResult,
                  instances: {
                    [SOPInstanceUID]: currentInstanceAIResult,
                  },
                },
              },
            },
            selectedModel,
            StudyInstanceUID
          );
          drawBox({ RectangleRoi: formatMeasurementsData(measurementList) });
          setLocalSummary(localList);
          setGlobalSummary(globalList);
        }
      }
    } else {
      setAiError(AI_ERROR.NO_RESULT);
    }
  }, [selectedModel, aiResult, viewports, tabIndex]);

  const drawBox = measurementsData => {
    clearMeasurementData();
    if (isEmpty(measurementsData) || isEmpty(measurementsData.RectangleRoi))
      return;

    let newMeasurementData = {};
    newMeasurementData = cloneDeep(measurementsData);
    newMeasurementData.RectangleRoi.forEach(measurement => {
      measurement.visible = settings.isVisibleAllTag;
    });

    let counter = 0;
    clearInterval(interval);
    interval = setInterval(() => {
      counter++;
      const element = document.querySelector('.viewport-element');
      if (element) {
        const enabledElement = cornerstone.getEnabledElement(element);
        if (enabledElement && enabledElement.image) {
          clearInterval(interval);
          drawMeasurements(newMeasurementData);
        }
      }
      if (counter === 1000) clearInterval(interval);
    }, 100);
  };

  useEffect(() => {
    if (tabIndex && tabIndex !== PANEL_TAB_NAMES.AI_TAB) {
      clearMeasurementData();
    }
  }, [tabIndex]);

  useEffect(() => {
    return () => {
      clearMeasurementData();
    };
  }, []);

  return (
    <div className="panel-tab-wrapper">
      <div className="cad-tool-selection">
        <div>{t('CAD Tool')}</div>
        <AIAnalyze
          selectedModel={selectedModel}
          setSelectedModel={handleUpdateSelectedModel}
        />
      </div>

      {aiError && <div className="mesurement-no-result">{t(aiError)}</div>}

      {!aiError && (
        <div className="mesurement-content">
          <MeasurementsList
            title="Findings"
            data={localSummary}
            scope={AI_RESULT_SCOPES.LOCAL}
            onClickItem={jumpToImage}
            itemIcon={<Icon name="external" />}
          />
          <MeasurementsList
            title="Conclusion"
            data={globalSummary}
            scope={AI_RESULT_SCOPES.GLOBAL}
          />
        </div>
      )}

      {selectedModel && !aiError && <Feedback selectedModel={selectedModel} />}
    </div>
  );
}

export default React.memo(AITab);

function getLocalSummary(AIResult = {}, selectedModel, studyInstanceUID) {
  let measurementList = [];
  let localList = [];

  if (AIResult.series) {
    let localData = [];
    const seriesData = AIResult.series;
    const seriesKeys = Object.keys(seriesData);
    seriesKeys.forEach(seriesSOPId => {
      const currentSeries = seriesData[seriesSOPId] || {};
      const instances = currentSeries.instances;

      if (!isEmpty(instances)) {
        Object.keys(instances).forEach(instanceSOPId => {
          const currentInstance = instances[instanceSOPId];
          let localResult = get(currentInstance, 'ai_result.result.local', []);
          const aiModel = get(
            currentInstance,
            'ai_result.result._metadata.ai_model'
          );
          if (aiModel === selectedModel && !isEmpty(localResult)) {
            localResult = localResult.map(localItem => {
              const value = Math.round(parseFloat(localItem.prob[0]) * 100);
              measurementList.push({
                ...localItem,
                StudyInstanceUID: studyInstanceUID,
                SeriesInstanceUID: seriesSOPId,
                SOPInstanceUID: instanceSOPId,
                value: value > 99 ? 99 : value,
              });
              return {
                ...localItem,
                StudyInstanceUID: studyInstanceUID,
                SeriesInstanceUID: seriesSOPId,
                SOPInstanceUID: instanceSOPId,
                // model: aiModel,
                value: value > 99 ? 99 : value,
              };
            });

            localData = [...localData, ...localResult];
          }
        });
      }
    });

    const flattenLocalData = [];
    if (!isEmpty(localData)) {
      localData.forEach(item => {
        for (let j = 0; j < (item.name || []).length; j++) {
          flattenLocalData.push({
            ...item,
            name: item.name[j],
            prob: item.prob[j],
          });
        }
      });

      const seen = new Set();
      localList = flattenLocalData.filter(el => {
        const duplicate = seen.has(el.name);
        seen.add(el.name);
        return !duplicate;
      });
    }
  }
  return { measurementList, localList };
}

function getGlobalSummary(AIData, selectedModel) {
  let globalResult = cloneDeep(get(AIData, 'global', {}));
  let aiModel = get(AIData, '_metadata.ai_model', null);

  let globalList = [];
  let globalTemp = [];

  if (aiModel === selectedModel) {
    if (AI_MODEL.MAMMOGRAPHY === aiModel) {
      delete globalResult.Abnormal;
    } else {
      // show Abnormal only
      if (globalResult.Abnormal) {
        let value = Math.round(parseFloat(globalResult.Abnormal) * 100);
        if (value >= 50) {
          globalResult = { Abnormal: globalResult.Abnormal };
        } else {
          globalResult = { Normal: 0 };
        }
      } else {
        globalResult = {};
      }
    }

    globalTemp = Object.keys(globalResult)
      .filter(name => name !== SPECIAL_CONCLUSIONS.NOT_AVAILABLE)
      .map(name => {
        let value = Math.round(parseFloat(globalResult[name]) * 100);
        value = value > 99 ? 99 : value;
        let keyName = name,
          suffix = null,
          position = null;

        // handle for mammo model
        if (AI_MODEL.MAMMOGRAPHY === aiModel) {
          for (let i = 0; i < MAMMO_GLOBAL_KEY.length; i++) {
            if (name.indexOf(MAMMO_GLOBAL_KEY[i]) > -1) {
              const splitString = name.split(MAMMO_GLOBAL_KEY[i]);
              keyName = MAMMO_GLOBAL_KEY[i];
              suffix = splitString[1];
              position = `${splitString[0]}`.trim();
              break;
            }
          }
        }

        return {
          name: keyName,
          value: value,
          position: position,
          suffix,
          model: aiModel,
        };
      });

    // delete duplicate items
    globalTemp.forEach(item => {
      const existsIdx = globalList.findIndex(
        it =>
          item.name === it.name &&
          item.position === it.position &&
          item.model === it.model
      );
      if (existsIdx > -1) {
        const newValue = item.value;
        const oldValue = globalList[existsIdx].value;
        if (newValue > oldValue) {
          globalList[existsIdx] = item;
        }
      } else {
        globalList.push(item);
      }
    });

    sortGlobalData(globalList);
  }
  return globalList;
}

function sortGlobalData(globalList) {
  const firstGlobalPosition = get(globalList, '[0].position');

  if (firstGlobalPosition === 'R' || firstGlobalPosition === 'L') {
    globalList.sort(compareStrings('position', 'desc'));
  } else {
    globalList.sort(compareValues('value', 'desc'));
  }
}

function compareValues(key, order = 'desc') {
  return function innerSort(a, b) {
    const varA = parseFloat(get(a, key));
    const varB = parseFloat(get(b, key));

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return order === 'desc' ? comparison * -1 : comparison;
  };
}

function compareStrings(key, order = 'desc') {
  return function innerSort(a, b) {
    const varA = get(a, key);
    const varB = get(b, key);

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return order === 'desc' ? comparison * -1 : comparison;
  };
}

function usePreviousValue(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}
