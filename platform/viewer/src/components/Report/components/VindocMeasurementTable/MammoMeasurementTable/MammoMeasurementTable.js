import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { compose } from 'redux';
import OHIF from '@tuvm/core';
import PropTypes from 'prop-types';

import { MammoMeasurementTableItem } from './MammoMeasurementTableItem.js';
import {
  groupBy,
  checkMammoPosition,
  isDoctorTab,
} from '../../../../../utils/common';
import '../MeasurementTable.styl';
import { commandsManager } from '../../../../../App';
import get from 'lodash/get';

class MammoMeasurementTable extends Component {
  static propTypes = {
    measurementCollection: PropTypes.array.isRequired,
    timepoints: PropTypes.array.isRequired,
    overallWarnings: PropTypes.object.isRequired,
    readOnly: PropTypes.bool,
    onItemClick: PropTypes.func,
    onRelabelClick: PropTypes.func,
    onDeleteClick: PropTypes.func,
    onEditDescriptionClick: PropTypes.func,
    selectedMeasurementNumber: PropTypes.number,
    t: PropTypes.func,
    saveFunction: PropTypes.func,
    onSaveComplete: PropTypes.func,
    onDeleteBoxFromLabel: PropTypes.func,
    showBackground: PropTypes.bool,
    predictionPanel: PropTypes.shape({
      tabName: PropTypes.string,
    }),
    timepointManager: PropTypes.object,
    extensions: PropTypes.object,
  };

  static defaultProps = {
    overallWarnings: {
      warningList: [],
    },
    readOnly: false,
  };

  state = {
    selectedKey: null,
    showBackground: false,
    timepointManager: {},
    extensions: this.props.extensions,
    predictionPanel: this.props.predictionPanel,
  };

  render() {
    const { measurementCollection, t } = this.props;
    const { showBackground, extensions } = this.state;
    const background = showBackground ? 'var(--dark-blue)' : '';
    const panelTabName = get(extensions, 'settings.panelTabName');
    let defaultEmptyText = 'There is no data';

    if (isDoctorTab(panelTabName)) {
      defaultEmptyText = 'Annotate by the rectangle tool';
    }

    if (
      measurementCollection.length === 1 &&
      measurementCollection[0].measurements.length === 0
    ) {
      return (
        <div className="flex w-100">
          <div className="measurement-column">
            <div
              className="table-list-item white-text text-center flex"
              style={{ height: '100%' }}
            >
              <span className="m-auto no-data-message">
                <i>{t(defaultEmptyText)}</i>
              </span>
            </div>
          </div>

          <div className="measurement-column">
            <div
              className="table-list-item white-text text-center flex"
              style={{ height: '100%' }}
            >
              <span className="m-auto no-data-message">
                <i>{t(defaultEmptyText)}</i>
              </span>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="measurement-table" style={{ background }}>
        <div className="measurement-container">
          {this.getMeasurementsGroups()}
        </div>
      </div>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.extensions !== this.props.extensions) {
      this.setState({ extensions: this.props.extensions });
    }
    if (prevProps.predictionPanel !== this.props.predictionPanel) {
      this.setState({ predictionPanel: this.props.predictionPanel });
    }
  }

  getMeasurementsGroups = () => {
    const { measurementCollection } = this.props;

    return measurementCollection.map((measureGroup, index) => (
      <Fragment key={Math.random() + index}>
        {this.getMeasurements(measureGroup)}
      </Fragment>
    ));
  };

  onDeleteBoxFromLabel = (labelName, box, measureGroupByLabel) => {
    let labels = [];
    const keyGroup = Object.keys(measureGroupByLabel);
    for (let i = 0; i < keyGroup.length; i++) {
      const currentGroup = measureGroupByLabel[keyGroup[i]];
      currentGroup.map(item => {
        if (item.itemNumber === box.itemNumber) {
          labels.push(item.label);
        }
      });
    }

    // delete completely if there are no label includes this box
    if (labels.length <= 1) {
      this.props.onDeleteClick(null, box);
      const { measurementCollection } = this.props;
      const { MeasurementHandlers } = OHIF.measurements;

      const list = measurementCollection[0].measurements;
      const filterList = list.filter(
        item => item.measurementId !== box.measurementId
      );

      filterList.forEach((item, index) => {
        const boxNumber = `${index + 1} - ${item.label
          .map(l => this.props.t(l))
          .join(', ')}`;

        MeasurementHandlers.onModified({
          detail: {
            toolType: box.toolType,
            measurementData: {
              _id: item.measurementId,
              lesionNamingNumber: index + 1,
              measurementNumber: index + 1,
              boxNumber,
            },
            element: document.querySelector('.viewport-element'),
          },
        });
        item.location = item.label;
        item.description = item.description || '';
        item.boxNumber = boxNumber;
        item.boxDescription = item.boxDescription || '';
        commandsManager.runCommand('updateTableWithNewMeasurementData', item);
      });
    } else {
      labels = labels.filter(l => l !== labelName);

      box.label = labels;
      box.location = labels;
      const labelsTranslated = labels.map(l => this.props.t(l));
      box.boxNumber = `${box.measurementNumber} - ${labelsTranslated.join(
        ', '
      )}`;
      box._id = box.measurementId;
      this.props.onDeleteBoxFromLabel(box);
    }
  };

  getMeasurements = measureGroup => {
    const { predictionPanel } = this.state;

    const { timepointManager } = this.props;
    let rightGroup = [],
      leftGroup = [];

    // measurements list here;
    if (
      measureGroup &&
      predictionPanel &&
      predictionPanel.allInstances &&
      timepointManager.measurements &&
      timepointManager.measurements.RectangleRoi
    ) {
      const timepointMeasurements = timepointManager.measurements.RectangleRoi;

      const allInstances = predictionPanel.allInstances;

      const rightMesurementGroup = timepointMeasurements.filter(item =>
        allInstances.some(instance =>
          checkMammoPosition(item.SOPInstanceUID, instance, 'rightSide')
        )
      );

      const leftMeasurementGroup = timepointMeasurements.filter(item =>
        allInstances.some(instance =>
          checkMammoPosition(item.SOPInstanceUID, instance, 'leftSide')
        )
      );

      rightGroup = measureGroup.measurements.filter(item =>
        rightMesurementGroup.some(
          m => m.measurementNumber === item.measurementNumber
        )
      );

      leftGroup = measureGroup.measurements.filter(item =>
        leftMeasurementGroup.some(
          m => m.measurementNumber === item.measurementNumber
        )
      );

      const newRightGroup = [];
      const newLeftGroup = [];

      rightGroup.map(m => {
        if (m.label.length > 0) {
          m.label.map(label => {
            const newMesuremntBox = { ...m, label };
            newRightGroup.push(newMesuremntBox);
          });
        }
      });

      leftGroup.map(m => {
        if (m.label.length > 0) {
          m.label.map(label => {
            const newMesuremntBox = { ...m, label };
            newLeftGroup.push(newMesuremntBox);
          });
        }
      });

      const rightGroupByLabel = groupBy(newRightGroup, 'label');
      const rightLabelList = Object.keys(rightGroupByLabel);

      const leftGroupByLabel = groupBy(newLeftGroup, 'label');
      const leftLabelList = Object.keys(leftGroupByLabel);

      const max = Math.max(rightLabelList.length, leftLabelList.length);

      return (
        <div className="flex">
          {rightLabelList.length > 0 ? (
            <div className="measurement-column">
              {this.renderMeasurementItem(
                rightLabelList,
                rightGroupByLabel,
                max
              )}
            </div>
          ) : (
            <div className="measurement-column">
              {leftLabelList.length > 0 ? (
                [...Array(leftLabelList.length).keys()].map(index => (
                  <Fragment key={Math.random() + index}>
                    {this.renderBlankBox()}
                  </Fragment>
                ))
              ) : (
                <>{this.renderBlankBox()}</>
              )}
            </div>
          )}

          {leftLabelList.length > 0 ? (
            <div className="measurement-column">
              {this.renderMeasurementItem(leftLabelList, leftGroupByLabel, max)}
            </div>
          ) : (
            <div className="measurement-column">
              {rightLabelList.length > 0 ? (
                [...Array(rightLabelList.length).keys()].map(index => (
                  <Fragment key={Math.random() + index}>
                    {this.renderBlankBox()}
                  </Fragment>
                ))
              ) : (
                <>{this.renderBlankBox()}</>
              )}
            </div>
          )}
        </div>
      );
    }
  };

  renderMeasurementItem = (labelList, measureGroupByLabel, max) => {
    const { extensions } = this.state;
    const panelTabName = get(extensions, 'settings.panelTabName');

    return [...Array(max).keys()].map(index => {
      if (index < labelList.length) {
        const measurement = measureGroupByLabel[labelList[index]];

        return (
          <MammoMeasurementTableItem
            key={labelList[index] + index}
            measurementData={measurement}
            labelName={labelList[index]}
            onItemClick={this.onItemClick}
            onRelabel={this.props.onRelabelClick}
            onDelete={this.props.onDeleteClick}
            onEditDescription={this.props.onEditDescriptionClick}
            onDeleteBoxFromLabel={(labelName, box) =>
              this.onDeleteBoxFromLabel(labelName, box, measureGroupByLabel)
            }
            panelTabName={panelTabName}
          />
        );
      }

      return (
        <Fragment key={Math.random() + index}>{this.renderBlankBox()}</Fragment>
      );
    });
  };

  onItemClick = (event, measurementData) => {
    if (this.props.readOnly) return;

    if (this.props.onItemClick) {
      this.props.onItemClick(event, measurementData);
    }
  };

  renderBlankBox = () => (
    <div className={`measurementItem table-list-item`}>
      <div className="measurement-location non-visible">_</div>
      <div className="box-number-list non-visible">
        <span className="box-number-item">
          <span>_</span>
        </span>
      </div>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    predictionPanel: state.extensions.predictionPanel,
    timepointManager: state.timepointManager,
    extensions: state.extensions,
  };
};

const connectedComponent = compose(
  connect(mapStateToProps, null),
  withTranslation(['Vindoc', 'Common'])
)(MammoMeasurementTable);

export { connectedComponent as MammoMeasurementTable };
export default connectedComponent;
