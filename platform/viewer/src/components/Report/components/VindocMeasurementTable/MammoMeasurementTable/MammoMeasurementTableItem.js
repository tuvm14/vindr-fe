import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { Icon } from '@tuvm/ui';

import { isDoctorTab } from '../../../../../utils/common';
import '../MeasurementTableItem.styl';

class MammoMeasurementTableItem extends Component {
  static propTypes = {
    measurementData: PropTypes.array.isRequired,
    onItemClick: PropTypes.func.isRequired,
    onRelabel: PropTypes.func,
    onDelete: PropTypes.func,
    onEditDescription: PropTypes.func,
    itemClass: PropTypes.string,
    itemIndex: PropTypes.number,
    t: PropTypes.func,
    labelName: PropTypes.string,
    onDeleteBoxFromLabel: PropTypes.func,
    panelTabName: PropTypes.string,
  };

  render() {
    return <>{this.getTableListItem()}</>;
  }

  getActionButton = (btnLabel, onClickCallback, icon = 'edit') => {
    return (
      <div key={btnLabel} className="btn-action" onClick={onClickCallback}>
        <span style={{ marginRight: '4px' }}>
          <Icon name={icon} width="14px" height="14px" />
        </span>
        {this.props.t(btnLabel)}
      </div>
    );
  };

  getActions = (labelName, box) => {
    const actionButtons = [];
    const { t } = this.props;

    if (typeof this.props.onRelabel === 'function') {
      const relabelButton = this.getActionButton(
        t('Edit'),
        this.onRelabelClick(box),
        'edit'
      );
      actionButtons.push(relabelButton);
    }

    if (typeof this.props.onDeleteBoxFromLabel === 'function') {
      const deleteButton = this.getActionButton(
        t('Delete'),
        this.onDeleteBoxFromLabel(labelName, box),
        'bin'
      );
      actionButtons.push(deleteButton);
    }

    return actionButtons;
  };

  getTableListItem = () => {
    const { measurementData, labelName, panelTabName, key } = this.props;

    return (
      <div className={`measurementItem table-list-item`} key={labelName + key}>
        <div className="measurement-location">{this.props.t(labelName)}</div>
        <div className="box-number-list">
          {measurementData &&
            measurementData.map((box, index) => {
              return (
                <span
                  className="box-number-item"
                  key={box.measurementNumber + index}
                  onClick={event => this.onItemClick(event, box)}
                >
                  <span>{box.itemNumber}</span>
                  {isDoctorTab(panelTabName) && (
                    <div className="row-actions">
                      {this.getActions(labelName, box)}
                    </div>
                  )}
                </span>
              );
            })}
        </div>
      </div>
    );
  };

  onItemClick = (event, box) => {
    this.props.onItemClick(event, box);
  };

  onRelabelClick = box => event => {
    // Prevent onItemClick from firing
    event.stopPropagation();

    this.props.onRelabel(event, box);
  };

  onEditDescriptionClick = event => box => {
    // Prevent onItemClick from firing
    event.stopPropagation();

    this.props.onEditDescription(event, box);
  };

  onDeleteClick = box => event => {
    // Prevent onItemClick from firing
    event.stopPropagation();

    this.props.onDelete(event, box);
  };

  onDeleteBoxFromLabel = (labelName, box) => event => {
    event.stopPropagation();
    this.props.onDeleteBoxFromLabel(labelName, box);
  };

  getDataDisplayText = () => {
    return this.props.measurementData.data.map((data, index) => {
      return (
        <div key={`displayText_${index}`} className="measurement-displayText">
          {data.displayText ? data.displayText : '...'}
        </div>
      );
    });
  };
}

const connectedComponent = withTranslation('Vindoc')(MammoMeasurementTableItem);
export { connectedComponent as MammoMeasurementTableItem };
export default connectedComponent;
