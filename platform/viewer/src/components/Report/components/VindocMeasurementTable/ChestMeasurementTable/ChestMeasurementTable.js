import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import { ChestMeasurementTableItem } from './ChestMeasurementTableItem.js';
import PropTypes from 'prop-types';
import OHIF from '@tuvm/core';
import { connect } from 'react-redux';
import { compose } from 'redux';
import get from 'lodash/get';

import { commandsManager } from '../../../../../App';
import { groupBy, isDoctorTab } from '../../../../../utils/common';
import '../MeasurementTable.styl';

class ChestMeasurementTable extends Component {
  static propTypes = {
    measurementCollection: PropTypes.array.isRequired,
    timepoints: PropTypes.array.isRequired,
    overallWarnings: PropTypes.object.isRequired,
    readOnly: PropTypes.bool,
    onItemClick: PropTypes.func,
    onRelabelClick: PropTypes.func,
    onDeleteClick: PropTypes.func,
    onEditDescriptionClick: PropTypes.func,
    selectedMeasurementNumber: PropTypes.number,
    t: PropTypes.func,
    saveFunction: PropTypes.func,
    onSaveComplete: PropTypes.func,
    onDeleteBoxFromLabel: PropTypes.func,
    showBackground: PropTypes.bool,
    tabName: PropTypes.number,
    predictionPanel: PropTypes.shape({
      tabName: PropTypes.string,
    }),
    extensions: PropTypes.object,
  };

  static defaultProps = {
    overallWarnings: {
      warningList: [],
    },
    readOnly: false,
  };

  state = {
    selectedKey: null,
    predictionPanel: this.props.predictionPanel,
    extensions: this.props.extensions,
  };

  shouldShowBackground = () => {
    const { measurementCollection } = this.props;
    if (measurementCollection) {
      const measurements = get(measurementCollection, '[0].measurements');
      return measurements && measurements.length === 1;
    }
    return false;
  };

  render() {
    const { measurementCollection } = this.props;
    const { extensions } = this.state;

    const panelTabName = get(extensions, 'settings.panelTabName');

    let defaultEmptyText = 'There is no data';

    if (isDoctorTab(panelTabName)) {
      defaultEmptyText = 'Annotate by the rectangle tool';
    }

    if (
      measurementCollection.length === 1 &&
      measurementCollection[0].measurements.length === 0
    ) {
      return (
        <div className="measurement-column table-list-item flex">
          <span className="m-auto no-data-message">
            <i>{this.props.t(defaultEmptyText)}</i>
          </span>
        </div>
      );
    }

    return (
      <div
        className="measurement-table"
        style={{
          background: this.shouldShowBackground() ? 'var(--dark-blue)' : '',
        }}
      >
        <div className="measurement-container">
          {this.getMeasurementsGroups()}
        </div>
      </div>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.extensions !== this.props.extensions) {
      this.setState({ extensions: this.props.extensions });
    }
    if (prevProps.predictionPanel !== this.props.predictionPanel) {
      this.setState({ predictionPanel: this.props.predictionPanel });
    }
  }

  saveFunction = async event => {
    const { saveFunction, onSaveComplete } = this.props;
    if (saveFunction) {
      try {
        const result = await saveFunction();
        if (onSaveComplete) {
          onSaveComplete({
            title: 'STOW SR',
            message: result.message,
            type: 'success',
          });
        }
      } catch (error) {
        if (onSaveComplete) {
          onSaveComplete({
            title: 'STOW SR',
            message: error.message,
            type: 'error',
          });
        }
      }
    }
  };

  getMeasurementsGroups = () => {
    const { measurementCollection } = this.props;
    return measurementCollection.map((measureGroup, index) => {
      return (
        <Fragment key={index}>{this.getMeasurements(measureGroup)}</Fragment>
      );
    });
  };

  onDeleteBoxFromLabel = (labelName, box, measureGroupByLabel) => {
    let labels = [];
    const keyGroup = Object.keys(measureGroupByLabel);

    for (let i = 0; i < keyGroup.length; i++) {
      const currentGroup = measureGroupByLabel[keyGroup[i]];
      currentGroup.map(item => {
        if (item.itemNumber === box.itemNumber) {
          labels.push(item.label);
        }
      });
    }

    // delete completely if there are no label includes this box
    if (labels.length <= 1) {
      this.props.onDeleteClick(null, box);
      const { measurementCollection } = this.props;
      const { MeasurementHandlers } = OHIF.measurements;

      const list = measurementCollection[0].measurements;
      const filterList = list.filter(
        item => item.measurementId !== box.measurementId
      );

      filterList.forEach((item, index) => {
        const boxNumber = `${index + 1} - ${item.label
          .map(l => this.props.t(l))
          .join(', ')}`;

        MeasurementHandlers.onModified({
          detail: {
            toolType: box.toolType,
            measurementData: {
              _id: item.measurementId,
              lesionNamingNumber: index + 1,
              measurementNumber: index + 1,
              boxNumber,
            },
            element: document.querySelector('.viewport-element'),
          },
        });
        item.location = item.label;
        item.description = item.description || '';
        item.boxNumber = boxNumber;
        item.boxDescription = item.boxDescription || '';
        commandsManager.runCommand('updateTableWithNewMeasurementData', item);
      });
    } else {
      labels = labels.filter(l => l !== labelName);

      box.label = labels;
      box.location = labels;
      const labelsTranslated = labels.map(l => this.props.t(l));
      box.boxNumber = `${box.measurementNumber} - ${labelsTranslated.join(
        ', '
      )}`;
      box._id = box.measurementId;
      this.props.onDeleteBoxFromLabel(box);
    }
  };

  getMeasurements = measureGroup => {
    let newMeasureGroup = [];
    const { extensions } = this.state;
    const panelTabName = get(extensions, 'settings.panelTabName');

    measureGroup.measurements.map(m => {
      if (m.label.length > 0) {
        m.label.map(l => {
          const newMesuremntBox = { ...m, label: l };

          newMeasureGroup.push(newMesuremntBox);
        });
      }
    });

    const measureGroupByLabel = groupBy(newMeasureGroup, 'label');
    const labelList = Object.keys(measureGroupByLabel);

    return labelList.map((labelName, index) => {
      const measurement = measureGroupByLabel[labelName];

      return (
        <ChestMeasurementTableItem
          key={labelName + index}
          measurementData={measurement}
          labelName={labelName}
          onItemClick={this.onItemClick}
          onRelabel={this.props.onRelabelClick}
          onDelete={this.props.onDeleteClick}
          onEditDescription={this.props.onEditDescriptionClick}
          onDeleteBoxFromLabel={(labelName, box) =>
            this.onDeleteBoxFromLabel(labelName, box, measureGroupByLabel)
          }
          panelTabName={panelTabName}
        />
      );
    });
  };

  onItemClick = (event, measurementData) => {
    if (this.props.readOnly) return;

    if (this.props.onItemClick) {
      this.props.onItemClick(event, measurementData);
    }
  };
}

const mapStateToProps = state => {
  return {
    predictionPanel: state.extensions.predictionPanel,
    timepointManager: state.timepointManager,
    extensions: state.extensions,
  };
};

const connectedComponent = compose(
  connect(mapStateToProps, null),
  withTranslation(['Vindoc', 'Common'])
)(ChestMeasurementTable);

export { connectedComponent as ChestMeasurementTable };
export default connectedComponent;
