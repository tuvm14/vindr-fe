import React from 'react';
// import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import get from 'lodash/get';

import { ChestMeasurementTable } from './ChestMeasurementTable';
import { MammoMeasurementTable } from './MammoMeasurementTable';

export default function MeasurementTableRouter(props) {
  // const { t } = useTranslation('Vindoc');

  const extensions = useSelector(state => state.extensions);

  // const isChestBody = get(extensions, 'metadata.isChestBody');
  const isBreastBody = get(extensions, 'metadata.isBreastBody');

  if (isBreastBody) {
    return <MammoMeasurementTable {...props} />;
  }
  return <ChestMeasurementTable {...props} />;

  // return (
  //   <div className="box-rounded flex w-100">
  //     <span className="text-center m-auto">{t('Loading')}...</span>
  //   </div>
  // );
}
