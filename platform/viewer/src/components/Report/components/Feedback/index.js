import React, { useState, useRef, useEffect } from 'react';
import { Button, message } from 'antd';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
// import { useParams } from 'react-router-dom';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

import {
  FEEDBACK_STATUS,
  FEEDBACK_ACTIONS,
  VINDR_AI_SCOPES,
} from '../../../../utils/constants';
import { actionFeedback, actionGetFeedbackSummary } from '../../PanelActions';
import { useSelector } from 'react-redux';
import {
  DislikeOutlined,
  LikeOutlined,
  CommentOutlined,
  SaveOutlined,
  CloseOutlined,
} from '@ant-design/icons';
import './Feedback.styl';
import { isDiff } from '../../../../utils/helper';

const OBJECT_TYPE = {
  STUDY: 'study',
  SERIES: 'series',
  INSTANCE: 'instance',
};

function Feedback(props) {
  const { selectedModel } = props;
  const { t } = useTranslation('Vindoc');
  const [likeStatus, setLikeStatus] = useState(null);
  const [openNote, setOpenNote] = useState(false);
  const [note, setNote] = useState();
  const [feedbackSummary, setFeedbackSummary] = useState({});
  const viewports = useSelector(state => state.viewports);
  const { activeViewportIndex = 0, viewportSpecificData = {} } = viewports;
  const {
    StudyInstanceUID,
    SeriesInstanceUID,
    SOPInstanceUID,
  } = viewportSpecificData[activeViewportIndex];

  const inputEl = useRef(null);

  const prevfbSummary = usePreviousValue(feedbackSummary);
  const prevStudyInstanceUID = usePreviousValue(StudyInstanceUID);
  const prevSeriesInstanceUID = usePreviousValue(SeriesInstanceUID);
  const prevSOPInstanceUID = usePreviousValue(SOPInstanceUID);
  const prevtabSelectedModel = usePreviousValue(selectedModel);

  useEffect(() => {
    if (selectedModel && !isEmpty(feedbackSummary)) {
      if (VINDR_AI_SCOPES.STUDY_SCOPE.includes(selectedModel)) {
        if (
          isDiff(prevfbSummary, feedbackSummary) ||
          isDiff(prevtabSelectedModel, selectedModel) ||
          isDiff(prevStudyInstanceUID, StudyInstanceUID)
        ) {
          const ratingInfo = get(feedbackSummary, 'RATING', {});
          setLikeStatus(
            (ratingInfo.form_data && ratingInfo.form_data.status) || null
          );

          const commentInfo = get(feedbackSummary, 'COMMENT');
          if (!isEmpty(commentInfo) && commentInfo.form_data) {
            setNote(commentInfo.form_data.note);
          } else {
            setNote('');
          }
        }
      } else if (VINDR_AI_SCOPES.SERIES_SCOPE.includes(selectedModel)) {
        if (
          isDiff(prevfbSummary, feedbackSummary) ||
          isDiff(prevtabSelectedModel, selectedModel) ||
          isDiff(prevStudyInstanceUID, StudyInstanceUID) ||
          isDiff(prevSeriesInstanceUID, SeriesInstanceUID)
        ) {
          const seriesData = get(feedbackSummary, 'series', {});
          const ratingInfo = get(seriesData[SeriesInstanceUID], 'RATING', {});
          setLikeStatus(
            (ratingInfo.form_data && ratingInfo.form_data.status) || null
          );

          const commentInfo = get(seriesData[SeriesInstanceUID], 'COMMENT');
          if (!isEmpty(commentInfo) && commentInfo.form_data) {
            setNote(commentInfo.form_data.note);
          } else {
            setNote('');
          }
        }
      } else if (VINDR_AI_SCOPES.INSTANCE_SCOPE.includes(selectedModel)) {
        if (
          isDiff(prevfbSummary, feedbackSummary) ||
          isDiff(prevtabSelectedModel, selectedModel) ||
          isDiff(prevStudyInstanceUID, StudyInstanceUID) ||
          isDiff(prevSeriesInstanceUID, SeriesInstanceUID) ||
          isDiff(prevSOPInstanceUID, SOPInstanceUID)
        ) {
          const seriesData = get(feedbackSummary, 'series', {});
          const instanceData = get(
            seriesData[SeriesInstanceUID],
            'instances',
            {}
          );

          const ratingInfo = get(instanceData[SOPInstanceUID], 'RATING', {});
          setLikeStatus(
            (ratingInfo.form_data && ratingInfo.form_data.status) || null
          );

          const commentInfo = get(instanceData[SOPInstanceUID], 'COMMENT');
          if (!isEmpty(commentInfo) && commentInfo.form_data) {
            setNote(commentInfo.form_data.note);
          } else {
            setNote('');
          }
        }
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    selectedModel,
    feedbackSummary,
    StudyInstanceUID,
    SeriesInstanceUID,
    SOPInstanceUID,
  ]);

  useEffect(() => {
    if (selectedModel && StudyInstanceUID) {
      handleGetFeedbackSummary(StudyInstanceUID, selectedModel);
    }
  }, [StudyInstanceUID, selectedModel]);

  const handleGetFeedbackSummary = async (studyUID, model) => {
    try {
      const { data } = await actionGetFeedbackSummary({
        study_instance_uid: studyUID,
        model,
      });
      setFeedbackSummary(data || {});
    } catch (error) {
      console.info(error);
    }
  };

  const handleAction = async status => {
    if (status === likeStatus) return;

    setLikeStatus(status);
    const payload = {
      action: FEEDBACK_ACTIONS.RATING,
      object_type: OBJECT_TYPE.INSTANCE,
      object_instance_uid: SOPInstanceUID || '',
      global: {},
      local: {},
      metadata: {},
      sr_data: {},
      form_data: {
        status: status,
        model: selectedModel,
        study_instance_uid: StudyInstanceUID,
        series_instance_uid: SeriesInstanceUID,
        sop_instance_uid: SOPInstanceUID,
      },
    };

    if (VINDR_AI_SCOPES.STUDY_SCOPE.includes(selectedModel)) {
      payload.object_type = OBJECT_TYPE.STUDY;
      payload.object_instance_uid = StudyInstanceUID;
    } else if (VINDR_AI_SCOPES.SERIES_SCOPE.includes(selectedModel)) {
      payload.object_type = OBJECT_TYPE.SERIES;
      payload.object_instance_uid = SeriesInstanceUID;
    }

    const data = await actionFeedback(payload);
    if (data) {
      message.success(t('Feedback has been saved'));
      handleGetFeedbackSummary(StudyInstanceUID, selectedModel);
    }
  };

  const handleNoteAction = async () => {
    const note = get(inputEl, 'current.value') || '';
    const payload = {
      action: FEEDBACK_ACTIONS.COMMENT,
      object_type: OBJECT_TYPE.INSTANCE,
      object_instance_uid: SOPInstanceUID || '',
      global: {},
      local: {},
      metadata: {},
      sr_data: {},
      form_data: {
        note: note,
        model: selectedModel,
        study_instance_uid: StudyInstanceUID,
        series_instance_uid: SeriesInstanceUID,
        sop_instance_uid: SOPInstanceUID,
      },
    };

    if (VINDR_AI_SCOPES.STUDY_SCOPE.includes(selectedModel)) {
      payload.object_type = OBJECT_TYPE.STUDY;
      payload.object_instance_uid = StudyInstanceUID;
    } else if (VINDR_AI_SCOPES.SERIES_SCOPE.includes(selectedModel)) {
      payload.object_type = OBJECT_TYPE.SERIES;
      payload.object_instance_uid = SeriesInstanceUID;
    }

    const data = await actionFeedback(payload);
    if (data) {
      handleGetFeedbackSummary(StudyInstanceUID, selectedModel);
      message.success(t('Feedback has been saved'));
    }
  };

  return (
    <div className="feedback-container">
      <div className="feedback-title">{t('Rate AI')}</div>
      <div className="feedback-buttons">
        <FeedbackButton
          name="Like"
          icon={<LikeOutlined />}
          handleClick={() => handleAction(FEEDBACK_STATUS.LIKE)}
          isActive={likeStatus === FEEDBACK_STATUS.LIKE}
        />
        <FeedbackButton
          name="Dislike"
          icon={<DislikeOutlined />}
          handleClick={() => handleAction(FEEDBACK_STATUS.DISLIKE)}
          isActive={likeStatus === FEEDBACK_STATUS.DISLIKE}
        />

        <FeedbackButton
          name="Note"
          icon={<CommentOutlined />}
          handleClick={() => {
            setOpenNote(true);
          }}
          isActive={openNote}
        />
      </div>
      {openNote && (
        <div>
          <textarea
            id="summary-comment"
            rows="5"
            placeholder={t('Leave comment')}
            spellCheck="false"
            className="box-rounded"
            ref={inputEl}
            value={note}
            onChange={e => setNote(e?.target?.value || '')}
          />
          <div className="note-buttons">
            <Button
              ghost
              className="note-btn"
              onClick={handleNoteAction}
              size="small"
              type="primary"
            >
              <SaveOutlined />
              {t('Save')}
            </Button>
            <Button
              ghost
              className="note-btn"
              onClick={() => setOpenNote(false)}
              size="small"
            >
              <CloseOutlined />
              {t('Close')}
            </Button>
          </div>
        </div>
      )}
    </div>
  );
}

function FeedbackButton({ name, icon, isActive, handleClick }) {
  const { t } = useTranslation('Vindoc');
  return (
    <Button
      ghost
      className={`feedback-btn ${isActive ? 'active' : ''}`}
      onClick={handleClick}
      size="small"
      type={`${isActive ? 'primary' : 'default'}`}
    >
      {icon}
      {t(name)}
    </Button>
  );
}

FeedbackButton.propTypes = {
  name: PropTypes.string,
  icon: PropTypes.any,
  isActive: PropTypes.bool,
  handleClick: PropTypes.func,
};

export default React.memo(Feedback);

function usePreviousValue(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}
