import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import {
  SPECIAL_CONCLUSIONS,
  MAMMO_GLOBAL_KEY,
  AI_MODEL,
} from '../../utils/constants';
import { groupBy, getBodyPart } from '../../utils/common';
import { getPosition } from '../../utils/helper';

class ProcessData {
  constructor(StudyInstanceUID, AIResult, scope = '') {
    this.AIResult = AIResult;
    this.measurementInput = [];
    this.measurementData = {};
    this.globalData = {};
    this.localData = [];
    this.allInstances = [];
    this.StudyInstanceUID = StudyInstanceUID;
    this.scope = scope;
  }

  init = () => {
    return true;
  };

  getGlobalData = () => {
    return this.globalData;
  };

  getLocalData = () => {
    return this.localData;
  };

  getAllInstances = () => {
    return this.allInstances;
  };

  getMeasurementData = () => {
    return this.measurementData;
  };

  getPatientID = () => {
    return this.PatientID;
  };

  getMeasurementInput = () => {
    return this.measurementInput;
  };

  setStudyInstanceUID = StudyInstanceUID => {
    this.StudyInstanceUID = StudyInstanceUID;
  };

  setPatientID = PatientID => {
    this.PatientID = PatientID;
  };

  setLocalData = localData => {
    this.localData = localData;
  };

  setGlobalData = globalData => {
    this.globalData = globalData;
  };

  setMeasurementData = measurementInput => {
    const measurementData = setMeasurementData(measurementInput);

    this.measurementData = measurementData;
  };

  setMeasurementInput = measurementInput => {
    this.measurementInput = measurementInput;
  };

  processAIResult = AIResult => {
    if (AIResult) {
      let measurementInput = [];
      let globalData = [],
        globalList = [];
      let localData = [],
        localList = [];
      //-- for series and instances
      if (AIResult.series) {
        const seriesData = AIResult.series;
        const seriesKeys = Object.keys(seriesData);

        seriesKeys.forEach(seriesSOPId => {
          const currentSeries = seriesData[seriesSOPId];
          const instances = currentSeries.instances;
          if (instances) {
            // for instances
            Object.keys(instances).forEach(instanceSOPId => {
              const currentInstance = instances[instanceSOPId];
              const aiResult = get(currentInstance, 'ai_result');
              const metadata = get(currentInstance, 'metadata') || {};
              let localResult = get(aiResult, 'result.local');
              let globalResult = get(aiResult, 'result.global');
              const model = get(aiResult, 'result._metadata.ai_model');

              if (localResult) {
                localResult = localResult.map(localItem => {
                  measurementInput.push({
                    ...localItem,
                    StudyInstanceUID: this.StudyInstanceUID,
                    SeriesInstanceUID: seriesSOPId,
                    SOPInstanceUID: instanceSOPId,
                  });
                  return {
                    ...localItem,
                    StudyInstanceUID: this.StudyInstanceUID,
                    SeriesInstanceUID: seriesSOPId,
                    SOPInstanceUID: instanceSOPId,
                    model: model,
                  };
                });

                localData = [...localData, ...localResult];
              }

              if (globalResult) {
                const bodyPart = getBodyPart(
                  metadata.Modality,
                  metadata.BodyPartExamined
                );

                if (model === AI_MODEL.MAMMOGRAPHY) {
                  const position = getPosition(metadata);
                  if (
                    Object.keys(globalResult).some(
                      key =>
                        `${key}`
                          .toLocaleLowerCase()
                          .indexOf(MAMMO_GLOBAL_KEY[0].toLocaleLowerCase()) > -1
                    )
                  ) {
                    delete globalResult.Abnormal;
                  } else if (globalResult.Abnormal) {
                    globalResult = {
                      Abnormal: globalResult.Abnormal || 0,
                    };
                  }
                  globalData = concatGlobalData(
                    globalData,
                    globalResult,
                    position,
                    bodyPart,
                    model
                  );
                } else {
                  if (globalResult.Abnormal) {
                    globalResult = {
                      Abnormal: globalResult.Abnormal || 0,
                    };
                  } else {
                    globalResult = {};
                  }
                  globalData = concatGlobalData(
                    globalData,
                    globalResult,
                    null,
                    bodyPart,
                    model
                  );
                }
              }
            });

            //-- for series
            // series has global data
            let seriesGlobalResult = get(
              currentSeries,
              'ai_result.result.global'
            );

            let seriesGlobalModel = get(
              currentSeries,
              'ai_result.result._metadata.ai_model',
              null
            );

            if (seriesGlobalResult && seriesGlobalResult.Abnormal) {
              seriesGlobalResult = {
                Abnormal: seriesGlobalResult.Abnormal || 0,
              };
            } else {
              seriesGlobalResult = {};
            }

            if (seriesGlobalResult) {
              globalData = concatGlobalData(
                globalData,
                seriesGlobalResult,
                null,
                null,
                seriesGlobalModel
              );
            }
          }
        });
      }

      //-- for study
      if (get(AIResult, 'ai_result')) {
        let studyGlobalResult = get(AIResult, 'ai_result.result.global');
        let studyGlobalModel = get(
          AIResult,
          'ai_result.result._metadata.ai_model',
          null
        );
        if (studyGlobalResult) {
          delete studyGlobalResult.Abnormal;
          globalData = concatGlobalData(
            globalData,
            studyGlobalResult,
            null,
            null,
            studyGlobalModel
          );
        }
      }

      localData = localData.map(item => {
        const value = Math.round(parseFloat(item.prob[0]) * 100);
        return {
          ...item,
          value: value > 99 ? 99 : value,
        };
      });

      const flattenLocalData = [];

      if (!isEmpty(localData)) {
        for (let i = 0; i < localData.length; i++) {
          const currentLocal = localData[i];
          for (let j = 0; j < currentLocal.name.length; j++) {
            const item = {
              ...currentLocal,
              name: currentLocal.name[j],
              prob: currentLocal.prob[j],
            };
            flattenLocalData.push(item);
          }
        }

        const seen = new Set();

        localList = flattenLocalData.filter(el => {
          const duplicate = seen.has(el.name);
          seen.add(el.name);
          return !duplicate;
        });

        // localList.sort(compareValues('value', 'desc'));
      }

      if (!isEmpty(globalData)) {
        for (let i = 0; i < globalData.length; i++) {
          const findIndex = globalList.findIndex(
            item =>
              globalData[i].name === item.name &&
              globalData[i].position === item.position &&
              globalData[i].model === item.model
          );
          if (findIndex > -1) {
            const value1 = globalData[i].value;
            const value2 = globalList[findIndex].value;

            if (value1 > value2) {
              globalList[findIndex] = globalData[i];
            }
          } else {
            globalList.push(globalData[i]);
          }
        }
        const firstGlobalPosition = get(globalList, '[0].position');

        if (firstGlobalPosition === 'R' || firstGlobalPosition === 'L') {
          globalList.sort(compareStrings('position', 'desc'));
        } else {
          globalList.sort(compareValues('value', 'desc'));
        }
      }

      this.setLocalData(localList);
      this.setGlobalData(globalList);
      this.setMeasurementInput(measurementInput);
    }
  };
}

function compareValues(key, order = 'desc') {
  return function innerSort(a, b) {
    const varA = parseFloat(get(a, key));
    const varB = parseFloat(get(b, key));

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return order === 'desc' ? comparison * -1 : comparison;
  };
}

function compareStrings(key, order = 'desc') {
  return function innerSort(a, b) {
    const varA = get(a, key);
    const varB = get(b, key);

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return order === 'desc' ? comparison * -1 : comparison;
  };
}

export const setMeasurementData = measurementInput => {
  if (measurementInput) {
    const data = formatMeasurementsData(measurementInput);

    let measurementData = {};

    const newData = groupBy(data, 'SOPInstanceUID');
    const newList = [];
    Object.keys(newData).map(key => {
      newData[key].forEach((item, index) => {
        newList.push(item);
      });
    });

    if (newList.length > 0) {
      measurementData = {
        RectangleRoi: newList,
      };
    }

    return measurementData;
  }
};

export const formatMeasurementsData = measurementInput => {
  if (measurementInput) {
    const data = measurementInput.map(item => {
      const labels = item.name ? item.name : item.labels || [];
      return {
        visible: true,
        active: false,
        invalidated: true,
        frameIndex: 0,
        toolType: item.toolType || 'RectangleRoi',
        location: labels,
        boxNumber: labels.map(n => window.t(n)).join(', '),
        boxDescription: '',
        showBoxLabel: true,
        groupName: '',
        handles: {
          start: {
            ...item.start,
            highlight: true,
            active: false,
          },
          end: {
            ...item.end,
            highlight: true,
            active: false,
          },
          initialRotation: 0,
          textBox: {
            active: false,
            hasMoved: false,
            movesIndependently: false,
            drawnIndependently: true,
            allowedOutsideImage: true,
            hasBoundingBox: true,
          },
        },

        StudyInstanceUID: item.StudyInstanceUID,
        SeriesInstanceUID: item.SeriesInstanceUID,
        SOPInstanceUID: item.SOPInstanceUID,
        imagePath:
          item.StudyInstanceUID +
          '_' +
          item.SeriesInstanceUID +
          '_' +
          item.SOPInstanceUID +
          `_0`,
      };
    });

    return data;
  }
};

const concatGlobalData = (
  globalData,
  globalResult,
  position,
  bodyPart,
  model
) => {
  const globalTemp = Object.keys(globalResult)
    .filter(key => key !== SPECIAL_CONCLUSIONS.NOT_AVAILABLE)
    .map(key => {
      let value = Math.round(parseFloat(globalResult[key]) * 100);
      value = value > 99 ? 99 : value;
      let keyName = key,
        suffix = null;
      let pos;
      for (let i = 0; i < MAMMO_GLOBAL_KEY.length; i++) {
        if (key.indexOf(MAMMO_GLOBAL_KEY[i]) > -1) {
          const splitString = key.split(MAMMO_GLOBAL_KEY[i]);
          keyName = MAMMO_GLOBAL_KEY[i];
          suffix = splitString[1];
          pos = `${splitString[0]}`.trim();
          break;
        }
      }
      return {
        name: keyName,
        value: value,
        position: pos || position,
        suffix,
        bodyPart,
        model,
      };
    });

  globalData = [...globalData, ...globalTemp];
  return globalData;
};

export { ProcessData };
