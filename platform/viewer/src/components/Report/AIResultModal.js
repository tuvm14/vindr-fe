import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import AITab from './components/AITab';
// import { useParams } from 'react-router';
import get from 'lodash/get';
import { actionViewStudy, callDoctorActionResult } from './PanelActions';
import { actionSetExtensionData } from '../../system/systemActions';
import { FEEDBACK_ACTIONS } from '../../utils/constants';
import { DraggableModal } from 'ant-design-draggable-modal';
import './DoctorModal.css';
import 'ant-design-draggable-modal/dist/index.css';
import { AnnotationSettings } from './components';

function AIResultModal(props) {
  const user = useSelector(state => state.extensions.user);
  // const { studyInstanceUIDs } = useParams();

  const viewports = useSelector(state => state.viewports);
  const activeViewportIndex = get(viewports, 'activeViewportIndex');
  const studyInstanceUID = get(
    viewports,
    `viewportSpecificData[${activeViewportIndex}].StudyInstanceUID`,
    ''
  );

  useEffect(() => {
    const actions = [
      FEEDBACK_ACTIONS.RATING,
      FEEDBACK_ACTIONS.COMMENT,
      FEEDBACK_ACTIONS.VIEW_MORE,
    ];

    actions.forEach(async action => {
      const data = await callDoctorActionResult({
        action: action,
        object_instance_uid: studyInstanceUID,
        owner: user.username,
      });

      if (data && data.data) {
        const store = window.store.getState();
        const userFeedback =
          get(store, 'extensions.predictionPanel.userFeedback') || {};
        actionSetExtensionData('predictionPanel', {
          userFeedback: {
            ...userFeedback,
            [action]: data.data,
          },
        });
      }
    });

    actionViewStudy({
      action: FEEDBACK_ACTIONS.VIEWER_OPEN,
      object_type: 'study',
      object_instance_uid: studyInstanceUID,
      global: {},
      local: {},
      metadata: {},
      sr_data: {},
      form_data: {},
    });
  }, [user, studyInstanceUID]);

  const handleCancel = () => {
    props.onClose();
  };

  return (
    <React.Fragment>
      <DraggableModal
        title={`Analysis of ${props.model}${props.analyzing ? ` (Analyzing)` : ''
          }`}
        zIndex={101}
        visible={props.visible}
        onCancel={handleCancel}
        maskClosable={false}
        initialWidth={window.innerWidth < 500 ? window.innerWidth - 100 : 400}
        initialHeight={
          window.innerHeight < 700 ? window.innerHeight - 100 : 600
        }
        footer={null}
        className="report-modal ai-modal"
      >
        <AITab />
        <AnnotationSettings />
      </DraggableModal>
    </React.Fragment>
  );
}

AIResultModal.propTypes = {
  model: PropTypes.string,
  analyzing: PropTypes.bool,
  visible: PropTypes.bool,
  onClose: PropTypes.func,
};

export default AIResultModal;
