import React from 'react';
import './style.css';

export function FullScreenLoading(props) {
  return (
    <div className="full-screen-loading ">
      <div className="uil-ring-css" style={{ transform: 'scale(0.79)' }}>
        <div></div>
      </div>
    </div>
  );
}
