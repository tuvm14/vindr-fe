import React, { useEffect, useState } from 'react';
import { Icon, Tooltip } from '@tuvm/ui';
import { Menu, Dropdown } from 'antd';
import { useSelector } from 'react-redux';
import { commandsManager } from '../../App';
import './WindowLevelSettings.styl';
import { useTranslation } from 'react-i18next';

function WindowLevelSettings(props) {
  const [windowLevelOptions, setWindowLevelOptions] = useState([]);
  const windowLevelData = useSelector(
    state => state.preferences.windowLevelData
  );
  const { t } = useTranslation('Vindoc');

  useEffect(() => {
    const customOptions = Object.keys(windowLevelData).map(key => {
      const item = windowLevelData[key];

      return {
        value: {
          window: item.window,
          level: item.level,
          preset: Number(key),
        },
        label: item.description,
      };
    });

    setWindowLevelOptions(customOptions);
  }, [setWindowLevelOptions, windowLevelData]);

  const handleChangeWindowLevel = preset => {
    if (Number(preset) === 0) {
      commandsManager.runCommand('resetViewport');
    } else {
      commandsManager.runCommand(`windowLevelPreset${preset}`);
    }
  };

  const windowSettingMenu = (
    <Menu className="window-setting-options">
      {windowLevelOptions.map(item => (
        <Menu.Item
          key={item.value.preset}
          onClick={() => handleChangeWindowLevel(item.value.preset)}
          className="window-setting-item"
        >
          {`${item.value.preset} - ${item.label}
              ${
                item.value.preset !== 0
                  ? `(ww: ${item.value.window}, wl: ${item.value.level})`
                  : ''
              }`}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Dropdown
      overlay={windowSettingMenu}
      destroyPopupOnHide={true}
      trigger={['click']}
      overlayClassName="dropdown-window-setting"
    >
      <Tooltip
        title={t('Preset Windows')}
        placement="bottom"
        overlayClassName="toolbar-tooltip"
      >
        <div className="toolbar-button">
          <Icon name="tune" />
          <div className="toolbar-label">{t('Preset Windows')}</div>
        </div>
      </Tooltip>
    </Dropdown>
  );
}

export default WindowLevelSettings;
