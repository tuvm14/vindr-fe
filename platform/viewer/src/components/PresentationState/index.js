import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Menu, Dropdown, Divider, Modal, Input, message } from 'antd';
import { Icon, Tooltip } from '@tuvm/ui';
import { get } from 'lodash';
import cornerstone from 'cornerstone-core';
import { useTranslation } from 'react-i18next';
import './PresentationState.styl';
import {
  createPresentationState,
  deletePresentationState,
  getListPresentationState,
} from './action';
import { CheckOutlined, DeleteOutlined, SaveOutlined } from '@ant-design/icons';
import {
  clearAllMeasurementData,
  drawMeasurements,
  jumpToImage,
  preFormatMeasurements,
} from '../../utils/helper';

const { confirm } = Modal;

function PresentationState() {
  const { t } = useTranslation('Vindoc');
  const viewports = useSelector(state => state.viewports);
  const annotations = useSelector(state => state.timepointManager);
  const { activeViewportIndex } = viewports;
  const [isPopup, setIsPopup] = useState(false);
  const [isDeleting, setDeleting] = useState(false);

  const [stateList, setStateList] = useState([]);
  const [selectedState, setSelectedState] = useState({});
  const studyInstanceUID = get(
    viewports,
    `viewportSpecificData[${activeViewportIndex}].StudyInstanceUID`,
    ''
  );

  const getListState = async StudyInstanceUID => {
    try {
      const res = await getListPresentationState(StudyInstanceUID);
      setStateList(res.data.result);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    if (studyInstanceUID) {
      getListState(studyInstanceUID);
    }
  }, [studyInstanceUID]);

  const handleApplyState = async state => {
    try {
      setSelectedState(state);
      const {
        viewport,
        measurements,
        viewportSpecificData,
      } = state.presentation_state;
      const { StudyInstanceUID, SOPInstanceUID } = viewportSpecificData;
      await jumpToImage({ StudyInstanceUID, SOPInstanceUID });

      setTimeout(() => {
        const enabledElements = cornerstone.getEnabledElements() || [];
        const currentActiveElm = enabledElements[activeViewportIndex];
        currentActiveElm.viewport = viewport;
        cornerstone.updateImage(currentActiveElm.element);
        const measurementsData = preFormatMeasurements(measurements);
        clearAllMeasurementData();
        drawMeasurements(measurementsData);
      }, 200);
    } catch (err) {
      console.log(err);
    }
  };

  const handleSaveState = () => {
    const enabledElements = cornerstone.getEnabledElements() || [];
    const currentActiveElm = enabledElements[activeViewportIndex];
    const { element } = currentActiveElm || {};
    const viewport = cornerstone.getViewport(element);
    const size = {
      clientWidth: element.clientWidth,
      clientHeight: element.clientHeight,
    };

    const state = {
      viewport: { ...viewport, size },
      viewportSpecificData: viewports.viewportSpecificData[activeViewportIndex],
      measurements: annotations.measurements,
    };

    let label;

    confirm({
      title: 'Label',
      icon: null,
      content: (
        <Input
          placeholder="Enter label"
          onChange={e => {
            label = e.target.value;
          }}
        />
      ),
      onOk() {
        if (label) {
          createPresentationState({
            label: label,
            data: state,
          })
            .then(() => {
              message.success(t('State created'));
              getListState(studyInstanceUID);
            })
            .catch(err => {
              message.error(t('Error Message'));
            });
        } else {
          message.error(t('Enter label'));
        }
      },
    });
  };

  const handleDeleteState = () => {
    setDeleting(!isDeleting);
  };

  const handleDelete = async state => {
    try {
      await deletePresentationState(state.id_);
      message.success('Success');
      await getListState(studyInstanceUID);
    } catch (err) {
      message.error('Error Message');
    }
  };

  const syncMenu = (
    <Menu className="state-options">
      {stateList &&
        stateList.map(item => (
          <Menu.Item
            key={item.id_}
            className="state-item"
            onClick={() =>
              isDeleting ? handleDelete(item) : handleApplyState(item)
            }
          >
            {isDeleting ? (
              <DeleteOutlined />
            ) : (
              <CheckOutlined
                className={`check ${
                  selectedState.id_ == item.id_ ? 'checked' : ''
                }`}
              />
            )}
            <div className="state-label-item">{item.label || ''}</div>
          </Menu.Item>
        ))}
      <Divider />
      <Menu.Item className="state-item" onClick={handleSaveState}>
        <SaveOutlined />
        <div className="state-label-item">Save Presentation State</div>
      </Menu.Item>
      <Menu.Item className="state-item" onClick={handleDeleteState}>
        <DeleteOutlined />
        <div className="state-label-item">Reset Presentation State</div>
      </Menu.Item>
    </Menu>
  );

  const handleChangeVisible = flag => {
    setIsPopup(flag);
    if (!flag) setDeleting(false);
  };

  return (
    <Dropdown
      overlay={syncMenu}
      trigger={['click']}
      visible={isPopup}
      onVisibleChange={handleChangeVisible}
      overlayClassName="dropdown-presentation-state"
    >
      <Tooltip
        title={t('Presentation State')}
        overlayClassName="toolbar-tooltip"
      >
        <div className="toolbar-button">
          <Icon name="turnedIn" />
          <Icon name="caret-down" className="expand-caret" />
          <div className="toolbar-label">{t('Presentation State')}</div>
        </div>
      </Tooltip>
    </Dropdown>
  );
}

export default PresentationState;
