import api from '../../services/api';

export const getListPresentationState = studyId => {
  const url = `/backend/presentation_state/all?study_instance_uid=${studyId}`;
  return api({ url, method: 'GET' });
};

export const createPresentationState = state => {
  const url = `/backend/presentation_state`;
  return api({ url, method: 'POST', data: state });
};

export const deletePresentationState = id => {
  const url = `/backend/presentation_state/single?ps_id=${id}`;
  return api({ url, method: 'DELETE' });
};
