import React from 'react';
import { Link } from 'react-router-dom';

import './VindocLogo.css';

const VindocLogo = ({ imgSrc = './assets/vindoc/Light-VinDr.svg' }) => {
  return (
    <Link className="header-brand" to="/">
      <img src={imgSrc} alt="vindoc logo" />
    </Link>
  );
};

export default React.memo(VindocLogo, () => true);
