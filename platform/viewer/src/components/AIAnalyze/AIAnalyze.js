import React, { useEffect, useState } from 'react';
import get from 'lodash/get';
import {
  AIAnalyzeList,
  AI_MODEL,
  VINDR_AI_SCOPES,
} from '../../utils/constants';
import { useTranslation } from 'react-i18next';
import { actionDiagnosis, actionCheckAIResult } from './AIAnalyzeActions';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { actionGetAIResult } from '../../system/customActions';
import { actionSetExtensionData } from '../../system/systemActions';
import { notification, message, Select, Button } from 'antd';
import './AIAnalyze.styl';
import { LoadingOutlined } from '@ant-design/icons';
import isEmpty from 'lodash/isEmpty';
import OHIF from '@tuvm/core';
import { jumpToImage } from '../../utils/helper';

const { studyMetadataManager } = OHIF.utils;
let interval;
const { Option } = Select;

const DIAGNOSE_STATUS = {
  PROCESSING: 'Processing',
  FINISHED: 'Finished',
  INVALID: 'Invalid',
};

function AIAnalyze(props) {
  const { t } = useTranslation('Vindoc');
  const { studyInstanceUIDs = '' } = useParams();
  const viewports = useSelector(state => state.viewports);
  const [loading, setLoading] = useState(false);
  const predictionPanel = useSelector(
    state => state.extensions.predictionPanel
  );
  const activeViewportIndex = get(viewports, 'activeViewportIndex');
  const studyInstanceUID = get(
    viewports,
    `viewportSpecificData[${activeViewportIndex}].StudyInstanceUID`,
    ''
  );
  const aiResult = get(predictionPanel, 'aiResult');

  useEffect(() => {
    document.title = 'VinDr - Viewer';
  }, [studyInstanceUIDs]);

  useEffect(() => {
    if (loading) {
      notification.open({
        key: 'ai-analyze',
        message: (
          <>
            <LoadingOutlined />
            <span style={{ paddingLeft: 20 }}>Analyzing...</span>
          </>
        ),
        placement: 'bottomRight',
        duration: 0,
        className: 'collapse-report',
        onClose: () => {
          clearInterval(interval);
          setLoading(false);
        },
      });
    } else {
      notification.close('ai-analyze');
    }
  }, [loading]);

  useEffect(() => {
    if (aiResult && studyInstanceUID && !props.selectedModel) {
      let currentStudyAIModels = []; // all AI models of current study

      let firstAbnormalInfo = {};
      const mappingModels = {};
      const studyMeta = studyMetadataManager.get(studyInstanceUID);

      mappingModels[studyInstanceUID] = {};
      Object.values(AI_MODEL).forEach(item => {
        mappingModels[studyInstanceUID][item] = [];
      });

      const studyRawData = aiResult[studyInstanceUID];
      const studyAIModel = get(
        studyRawData,
        'ai_result.result._metadata.ai_model'
      );
      if (studyAIModel) {
        currentStudyAIModels.push(studyAIModel);
        if (isEmpty(firstAbnormalInfo) && isHasAbnormal(studyRawData)) {
          firstAbnormalInfo = {
            aiModel: studyAIModel,
            studyInstanceUID: studyInstanceUID,
          };
        }
      }

      const seriesData = get(studyRawData, 'series', {});

      if (!isEmpty(seriesData)) {
        studyMeta._data.series.forEach(({ SeriesInstanceUID: seriesSOPId }) => {
          const currentSeries = seriesData[seriesSOPId];
          if (currentSeries) {
            const instances = get(currentSeries, 'instances', []);
            const seriesAIModel = get(
              currentSeries,
              'ai_result.result._metadata.ai_model'
            );

            if (seriesAIModel) {
              if (
                mappingModels[studyInstanceUID][seriesAIModel] &&
                !mappingModels[studyInstanceUID][seriesAIModel].includes(
                  seriesSOPId
                )
              ) {
                mappingModels[studyInstanceUID][seriesAIModel].push(
                  seriesSOPId
                );
              }
              currentStudyAIModels.push(seriesAIModel);
              if (isEmpty(firstAbnormalInfo) && isHasAbnormal(currentSeries)) {
                firstAbnormalInfo = {
                  aiModel: seriesAIModel,
                  studyInstanceUID: studyInstanceUID,
                  seriesInstanceUID: seriesSOPId,
                };
              }
            } else {
              Object.keys(instances).forEach(instanceSOPId => {
                const currentInstance = instances[instanceSOPId];
                const aiResult = get(currentInstance, 'ai_result');
                const instanceAIModel = get(
                  aiResult,
                  'result._metadata.ai_model'
                );
                if (instanceAIModel) {
                  if (
                    mappingModels[studyInstanceUID][instanceAIModel] &&
                    !mappingModels[studyInstanceUID][instanceAIModel].includes(
                      instanceSOPId
                    )
                  ) {
                    mappingModels[studyInstanceUID][instanceAIModel].push(
                      instanceSOPId
                    );
                  }
                  currentStudyAIModels.push(instanceAIModel);
                  if (isEmpty(firstAbnormalInfo) && isHasAbnormal(aiResult)) {
                    firstAbnormalInfo = {
                      aiModel: instanceAIModel,
                      studyInstanceUID: studyInstanceUID,
                      seriesInstanceUID: seriesSOPId,
                      sopInstanceUID: instanceSOPId,
                    };
                  }
                }
              });
            }
          }
        });
      }

      let firstAIModel = currentStudyAIModels[0]; // get the first ai model in current study.
      if (!isEmpty(firstAbnormalInfo)) {
        firstAIModel = firstAbnormalInfo.aiModel;
      }
      if (firstAIModel) {
        hanleChangeModel(firstAIModel);

        // active study
        if (VINDR_AI_SCOPES.STUDY_SCOPE.includes(firstAIModel)) {
          const studyMeta = studyMetadataManager.get(studyInstanceUID);
          const firstIns = studyMeta.getFirstInstance();
          if (firstIns) {
            const firstInstanceUID = firstIns.getSOPInstanceUID();
            handleJumpToImage(studyInstanceUID, firstInstanceUID, firstAIModel);
          }
        }
        // Active Series/Image
        else if (VINDR_AI_SCOPES.SERIES_SCOPE.includes(firstAIModel)) {
          const currentStudyMappingData = get(mappingModels, studyInstanceUID);
          let seriesUID = get(currentStudyMappingData, `${firstAIModel}[0]`);
          if (!isEmpty(firstAbnormalInfo)) {
            seriesUID = firstAbnormalInfo.seriesInstanceUID;
          }
          const studyRawData = aiResult[studyInstanceUID];
          const seriesData = get(studyRawData, 'series', {});

          const currentSeries = seriesData[seriesUID];
          if (currentSeries) {
            const instances = get(currentSeries, 'instances', {});
            let firstInstanceUID;
            Object.keys(instances).forEach(key => {
              if (instances[key]) {
                const currentInstance = instances[key];
                const localAIResult = get(
                  currentInstance,
                  'ai_result.result.local'
                );
                if (!isEmpty(localAIResult)) {
                  firstInstanceUID = key;
                  return;
                }
              }
            });

            if (!firstInstanceUID) {
              const studyMeta = studyMetadataManager.get(studyInstanceUID);
              const findIns = studyMeta.findInstance(instances => {
                return (
                  instances._series &&
                  instances._series.SeriesInstanceUID === seriesUID
                );
              });
              if (findIns) {
                firstInstanceUID = findIns.getSOPInstanceUID();
              }
            }

            if (firstInstanceUID) {
              handleJumpToImage(
                studyInstanceUID,
                firstInstanceUID,
                firstAIModel
              );
            }
          }
        } else if (VINDR_AI_SCOPES.INSTANCE_SCOPE.includes(firstAIModel)) {
          const currentStudyMappingData = get(mappingModels, studyInstanceUID);
          let firstInstanceUID = get(
            currentStudyMappingData,
            `${firstAIModel}[0]`
          );
          if (!isEmpty(firstAbnormalInfo)) {
            firstInstanceUID = firstAbnormalInfo.sopInstanceUID;
          }
          // jump to the image has AI result
          if (firstInstanceUID) {
            handleJumpToImage(studyInstanceUID, firstInstanceUID, firstAIModel);
          }
        }
      }
    }
  }, [aiResult, studyInstanceUID]);

  useEffect(() => {
    return () => clearInterval(interval);
  }, []);

  const handleJumpToImage = (studyInstanceUID, sopInstanceUID, aiModel) => {
    jumpToImage({
      StudyInstanceUID: studyInstanceUID,
      SOPInstanceUID: sopInstanceUID,
    });
    // set to redux
    actionSetExtensionData('cad', {
      StudyInstanceUID: studyInstanceUID,
      SOPInstanceUID: sopInstanceUID,
      model: aiModel,
    });
  };

  const sendRequest = async trackId => {
    let counter = 0;
    clearInterval(interval);
    interval = setInterval(async () => {
      counter += 1;
      if (counter === 600) {
        message.error(`${t('Sorry')}, ${t('AI Not Supported')}`);
        clearInterval(interval);
        setLoading(false);
        return;
      }
      const res = await actionCheckAIResult({
        studyInstanceUIDs: studyInstanceUIDs,
        trackId: trackId,
      });

      if (res && res.status && res.status != DIAGNOSE_STATUS.PROCESSING) {
        setLoading(false);
        clearInterval(interval);

        if (res.status == DIAGNOSE_STATUS.FINISHED) {
          message.success(t('Update AI result message'));
          actionSetExtensionData('predictionPanel', {
            aiTabContent: {
              localData: [],
              globalData: [],
              measurementsData: {},
            },
          });
          actionGetAIResult({ studyInstanceUIDs });
        } else if (res.status == DIAGNOSE_STATUS.INVALID) {
          message.error(t('Invalid image'));
        } else {
          message.error(t('Error Message'));
        }
      }
    }, 1000);
  };

  const handleDiagnose = async () => {
    setLoading(true);
    const activeViewportIndex = get(viewports, 'activeViewportIndex');
    const viewportSpecificData =
      viewports.viewportSpecificData[activeViewportIndex] || {};
    const StudyInstanceUID = get(viewportSpecificData, 'StudyInstanceUID', '');
    const SeriesInstanceUID = get(
      viewportSpecificData,
      'SeriesInstanceUID',
      ''
    );
    const SOPInstanceUID = get(viewportSpecificData, 'SOPInstanceUID', '');

    const body = {
      scope: 'series',
      uids: [SeriesInstanceUID],
    };

    switch (props.selectedModel) {
      case AI_MODEL.CHEST_XRAY:
        body.model = AI_MODEL.CHEST_XRAY;
        body.scope = 'instance';
        body.uids = [SOPInstanceUID];
        break;
      case AI_MODEL.MAMMOGRAPHY:
        body.model = AI_MODEL.MAMMOGRAPHY;
        body.scope = 'study';
        body.uids = [StudyInstanceUID];
        break;
      case AI_MODEL.LUNG_CT:
        body.model = AI_MODEL.LUNG_CT;
        body.scope = 'series';
        break;
      case AI_MODEL.LIVER_CT:
        body.model = AI_MODEL.LIVER_CT;
        body.scope = 'series';
        break;
      case AI_MODEL.BRAIN_CT:
        body.model = AI_MODEL.BRAIN_CT;
        body.scope = 'series';
        break;
      case AI_MODEL.SPINE_XR:
        body.model = AI_MODEL.SPINE_XR;
        body.scope = 'instance';
        body.uids = [SOPInstanceUID];
        break;
      case AI_MODEL.BRAIN_MRI:
        body.model = AI_MODEL.BRAIN_MRI;
        body.scope = 'series';
        break;
    }

    const res = await actionDiagnosis(body);

    if (res && res.track_id) {
      sendRequest(res.track_id);
    } else {
      message.error(`${t('Sorry')}, ${t('AI Not Supported')}`);
    }
  };

  const hanleChangeModel = item => {
    props.setSelectedModel(item);
  };

  return (
    <div className="ai-analyze">
      <Select
        value={props.selectedModel}
        placeholder={t('Select CAD Tool')}
        onChange={hanleChangeModel}
        className="select-model"
        dropdownClassName="dropdown-options-dark vindr-dropdown"
      >
        {AIAnalyzeList.map(item => (
          <Option value={item.key} key={item.key}>
            {t(item.name)}
          </Option>
        ))}
      </Select>
      <Button type="primary" className="btn-analyze" onClick={handleDiagnose}>
        {t('Analyze')}
      </Button>
    </div>
  );
}

export default React.memo(AIAnalyze);

function isHasAbnormal(data = {}) {
  try {
    const abnormalValue = get(data, 'ai_result.result.global.Abnormal');
    if (abnormalValue && parseFloat(abnormalValue) * 100 >= 50) {
      return true;
    }
  } catch (error) {
    console.error(error);
  }

  return false;
}
