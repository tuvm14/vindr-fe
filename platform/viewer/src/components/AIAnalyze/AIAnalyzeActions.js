import api from '../../services/api';

export const actionDiagnosis = async body => {
  const url = '/backend/diagnose_single/v2';
  const requestOptions = {
    method: 'POST',
    data: body,
    redirect: 'follow',
    url,
  };

  const { data } = await api(requestOptions);
  return data;
};

export const actionCheckAIResult = async (params = {}) => {
  const requestOptions = {
    method: 'GET',
    url: `/backend/track/${params.trackId}`,
  };

  const { data } = await api(requestOptions);
  return data;
};
