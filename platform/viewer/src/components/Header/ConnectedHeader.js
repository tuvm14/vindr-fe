import Header from './VindocHeader.js';
import { connect } from 'react-redux';

const mapStateToProps = () => {
  return {};
};

const ConnectedHeader = connect(mapStateToProps)(Header);

export default ConnectedHeader;
