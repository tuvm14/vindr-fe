import React from 'react';
import { Menu } from 'antd';
import { NAV_LIST } from '../../utils/constants';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import keycloak from '../../services/auth';

const useNavigationMenu = () => {
  const { t } = useTranslation(['Header', 'Vindoc']);
  const history = useHistory();
  return Object.keys(NAV_LIST).map(it =>
    keycloak.hasOneOfPerm(NAV_LIST[it].permission) &&
    keycloak.hasFeature(NAV_LIST[it].feature) ? (
      <Menu.Item
        key={NAV_LIST[it].key}
        onClick={() => history.push(NAV_LIST[it].url)}
      >
        {t(NAV_LIST[it].label)}
      </Menu.Item>
    ) : null
  );
};

export default useNavigationMenu;
