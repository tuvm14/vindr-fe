// import api from '../../services/api';
import axios from 'axios';

export const actionGetAboutInfo = async ({ tenant }) => {
  // const hostname = window.location.hostname;
  const { data } = await axios.get(
    `https://vindr-public.s3.ap-southeast-1.amazonaws.com/${tenant}_about.html`
  );

  return Promise.resolve({ template: data });
};

export const actionGetLogo = async ({ tenant }) => {
  const data = await axios
    .get(
      `https://vindr-public.s3.ap-southeast-1.amazonaws.com/${tenant}_logo.svg`,
      {
        responseType: 'arraybuffer',
      }
    )
    .then(response => Buffer.from(response.data, 'binary').toString('base64'));
  return Promise.resolve({ logo: `data:image/svg+xml;base64,${data}` });
};

export const actionGetLogoText = async ({ tenant }) => {
  let text = '';
  if (tenant == 'bvdkhaihau') text = ' - Bệnh Viện Hải Hậu';
  return Promise.resolve({ text: text });
};
