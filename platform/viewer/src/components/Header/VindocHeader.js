import React from 'react';
import { withRouter } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { withModal, Icon, Tooltip } from '@tuvm/ui';
import i18n from '@tuvm/i18n';
import { useSelector } from 'react-redux';
import { UserPreferences } from '../VindocUserPreferences';
import VindocLogo from '../VindocLogo/VindocLogo.js';
import { actionLogout, requestLogin } from '../../system/systemActions';
import { Menu, Dropdown } from 'antd';
import { useHistory } from 'react-router-dom';
import {
  CaretDownOutlined,
  LogoutOutlined,
  LoginOutlined,
  SettingOutlined,
  InfoCircleOutlined,
  MenuOutlined,
  // HomeOutlined,
  LineChartOutlined,
} from '@ant-design/icons';

import './VindocHeader.styl';
// import {
//   actionGetAboutInfo,
//   actionGetLogo,
//   actionGetLogoText,
// } from './actions';
import { getViewportApiKey, isMobile } from '../../utils/helper';
import keycloak from '../../services/auth';
import { NEW_SCOPES, SCOPES } from '../../utils/constants';
import { useWindowSize } from '@tuvm/ui/src/hooks/useWindowSize';

function Header(props) {
  const history = useHistory();
  const {
    modal: { show },
    children,
    t,
    navigationOptions,
    activeMenu,
  } = props;
  const userInfo = useSelector(state => state.extensions.user);
  const [width, height] = useWindowSize();
  // const [openAbout, setOpenAbout] = useState(false);
  // const [aboutInfo, setAbouInfo] = useState('');
  // const [logoSrc, setLogoSrc] = useState('');
  // const [logoText, setLogoText] = useState('');
  // const hostname = window.location.hostname;
  // const tenant = hostname.split('.')[0];

  // useEffect(() => {
  //   getAboutInfo();
  //   getLogo();
  // }, []);

  // const getAboutInfo = async () => {
  //   try {
  //     const data = await actionGetAboutInfo({ tenant: tenant });
  //     if (data && data.template) {
  //       setAbouInfo(data.template);
  //     }
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  // const getLogo = async () => {
  //   try {
  //     const logoData = await actionGetLogo({ tenant: tenant });
  //     if (logoData && logoData.logo) {
  //       setLogoSrc(logoData.logo);
  //     }
  //     const logoTextData = await actionGetLogoText({ tenant: tenant });
  //     if (logoTextData && logoTextData.text) {
  //       setLogoText(logoTextData.text);
  //     }
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  const handleChangeLanguage = value => {
    i18n.changeLanguage(value);
    // window.location.reload(true);
  };

  const langOptions = [
    <Menu.Item
      key="lang-vi"
      icon={
        <span role="img" className="anticon  ant-dropdown-menu-item-icon">
          <Icon name="vi" height="20" width="20" />
        </span>
      }
      onClick={() => handleChangeLanguage('vi')}
    >
      Tiếng việt
    </Menu.Item>,
    <Menu.Item
      key="lang-en-US"
      icon={
        <span role="img" className="anticon  ant-dropdown-menu-item-icon">
          <Icon name="en-US" height="20" width="20" />
        </span>
      }
      onClick={() => handleChangeLanguage('en-US')}
    >
      English
    </Menu.Item>,
  ];

  const getCurrentLang = currentLang => {
    if (currentLang && currentLang[0] && currentLang[0].value) {
      const value = currentLang[0].value;
      if (value === 'en-US') return 'English';
      if (value === 'vi') return 'Tiếng việt';

      return value;
    }

    return 'Tiếng việt';
  };

  const currentLang = i18n.availableLanguages.filter(
    lang => lang.value === i18n.language
  );

  const profileOptions = [
    // <Menu.Item
    //   key="dashboard"
    //   icon={<HomeOutlined />}
    //   onClick={() => history.push('/dashboard')}
    // >
    //   {t('Dashboard')}
    // </Menu.Item>,
    <Menu.Item
      key="preferences"
      icon={<SettingOutlined />}
      onClick={() =>
        show({
          content: UserPreferences,
          title: t('Preferences'),
        })
      }
    >
      {t('Preferences')}
    </Menu.Item>,
    <Menu.Item
      key="document"
      onClick={() => window.open('./document', '_blank')}
      icon={<InfoCircleOutlined />}
    >
      {t('Help')}
    </Menu.Item>,
    userInfo && userInfo.username ? (
      <Menu.Item key="logout" onClick={actionLogout} icon={<LogoutOutlined />}>
        {t('Logout')}
      </Menu.Item>
    ) : (
      <Menu.Item
        key="login"
        onClick={() => requestLogin(true)}
        icon={<LoginOutlined />}
      >
        {t('Login')}
      </Menu.Item>
    ),
  ];

  const mobileMenu = (
    <Dropdown
      overlay={
        <Menu selectedKeys={[activeMenu?.key, `lang-${currentLang[0]?.value}`]}>
          {navigationOptions && (
            <Menu.ItemGroup>{navigationOptions}</Menu.ItemGroup>
          )}
          <Menu.ItemGroup>{langOptions}</Menu.ItemGroup>
          {keycloak.hasOneOfPerm([
            SCOPES.reportApprove,
            NEW_SCOPES.worklist.report.approve,
          ]) && (
            <Menu.ItemGroup>
              <Menu.Item
                key="dashboard"
                onClick={() => history.push('/dashboard')}
                icon={<LineChartOutlined />}
              >
                {t('Dashboard')}
              </Menu.Item>
            </Menu.ItemGroup>
          )}
          <Menu.ItemGroup>{profileOptions}</Menu.ItemGroup>
        </Menu>
      }
      placement="bottomRight"
      className="header-dropdown-title"
      overlayClassName="vindr-dropdown header-dropdown"
      trigger={['click']}
    >
      <MenuOutlined style={{ fontSize: 22 }} />
    </Dropdown>
  );

  const tempToken = getViewportApiKey();

  return (
    <div className="entry-header">
      <div className="header-left-box">
        {/* {logoSrc && <VindocLogo imgSrc={logoSrc} />} */}
        {children || <VindocLogo />}
        {/* {logoText && (
          <span
            style={{
              display: 'flex',
              alignItems: 'flex-end',
              height: 30,
              fontSize: 16,
            }}
          >
            <span>{logoText}</span>
          </span>
        )} */}
        {!isMobile(width, height) && navigationOptions && (
          <Menu mode="horizontal" selectedKeys={[activeMenu?.key]}>
            {navigationOptions}
          </Menu>
        )}
      </div>
      {!isMobile(width, height) && (
        <div className="header-menu">
          {/* {aboutInfo && (
            <>
              <Button
                className="btn-about"
                onClick={() => setOpenAbout(true)}
                ghost
                size="small"
              >
                {t('About')}
              </Button>
              <Modal
                className="vindr-modal"
                title={t('About')}
                visible={openAbout}
                width="max-content"
                onCancel={() => setOpenAbout(false)}
                footer={[
                  <Button key="back" onClick={() => setOpenAbout(false)}>
                    {t('Close')}
                  </Button>,
                ]}
              >
                <div dangerouslySetInnerHTML={{ __html: aboutInfo }} />
              </Modal>
            </>
          )} */}
          {keycloak.hasOneOfPerm([
            SCOPES.reportApprove,
            NEW_SCOPES.worklist.report.approve,
          ]) && (
            <Tooltip
              title={t('Dashboard')}
              placement="bottom"
              overlayClassName="toolbar-tooltip"
            >
              <div
                className="toolbar-button"
                style={{ marginRight: 5 }}
                onClick={() => history.push('/dashboard')}
              >
                <LineChartOutlined />
              </div>
            </Tooltip>
          )}
          <Dropdown
            overlay={<Menu>{langOptions}</Menu>}
            placement="bottomRight"
            className="header-dropdown-title"
            overlayClassName="vindr-dropdown select-language header-dropdown"
          >
            <span className="language-info">
              <span className="current-lang">
                {t(getCurrentLang(currentLang))}
              </span>
              <CaretDownOutlined />
            </span>
          </Dropdown>
          {!tempToken && (
            <Dropdown
              overlay={<Menu>{profileOptions}</Menu>}
              placement="bottomRight"
              className="header-dropdown-title"
              overlayClassName="vindr-dropdown header-dropdown"
            >
              <div className="user-info">
                <Icon name="account" height="24" width="24" />
                <span className="user-name">
                  {userInfo.fullname || userInfo.username}
                </span>
              </div>
            </Dropdown>
          )}
        </div>
      )}
      {isMobile(width, height) && mobileMenu}
    </div>
  );
}

Header.propTypes = {
  children: PropTypes.node,
  modal: PropTypes.object,
  activeMenu: PropTypes.object,
  navigationOptions: PropTypes.array,
  t: PropTypes.func,
};

Header.defaultProps = {
  children: <VindocLogo />,
};

export default withTranslation(['Header', 'AboutModal', 'Vindoc'])(
  withRouter(withModal(React.memo(Header)))
);
