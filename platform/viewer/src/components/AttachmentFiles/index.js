import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Tooltip } from '@tuvm/ui';
import { get } from 'lodash';
import { useTranslation } from 'react-i18next';
import { FileZipOutlined } from '@ant-design/icons';
import AttachFile from '../../pages/studyList/components/AttachFile';
import './AttachmentFiles.styl';

function AttachmentFiles() {
  const { t } = useTranslation('Vindoc');
  const viewports = useSelector(state => state.viewports);
  const { activeViewportIndex } = viewports;
  const [visibleAttachFileModal, setVisibleAttachFileModal] = useState(false);

  const study = get(
    viewports,
    `viewportSpecificData[${activeViewportIndex}]`,
    ''
  );

  return (
    <>
      <Tooltip title={t('Attachment Files')} overlayClassName="toolbar-tooltip">
        <div
          className="toolbar-button"
          onClick={() => setVisibleAttachFileModal(true)}
        >
          <FileZipOutlined style={{ fontSize: 22 }} />
          <div className="toolbar-label">{t('Presentation State')}</div>
        </div>
      </Tooltip>
      {visibleAttachFileModal && (
        <AttachFile
          onCancel={() => setVisibleAttachFileModal(false)}
          study={study}
          isModalVisible={visibleAttachFileModal}
        />
      )}
    </>
  );
}

export default AttachmentFiles;
