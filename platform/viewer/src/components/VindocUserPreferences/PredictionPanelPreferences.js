import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import get from 'lodash/get';

import { TabFooter, LanguageSwitcher } from '@tuvm/ui';
import { useTranslation } from 'react-i18next';
import { panelTabNames } from '../../utils/common';
import { useExtensions } from '../../hooks';
import './PredictionPanelPreferences.styl';
import { message } from 'antd';

/**
 * General Preferences tab
 * It renders the General Preferences content
 *
 * @param {object} props component props
 * @param {function} props.onClose
 */
function PredictionPanelPreferences({ onClose }) {
  const { t } = useTranslation('Vindoc');
  const { setExtensionData } = useExtensions();
  const extensions = useSelector(state => state.extensions);
  const [currentOption, setOption] = useState('');

  const availableOptions = [
    {
      label: t('Showing doctor result first'),
      value: panelTabNames.doctorTab,
    },
    {
      label: t('Showing AI result first'),
      value: panelTabNames.aiTab,
    },
  ];

  React.useEffect(() => {
    const panelTabName = get(extensions, 'settings.panelTabName');

    setOption(panelTabName);
  }, [extensions]);

  const onResetPreferences = () => {
    const panelTabName = get(extensions, 'settings.panelTabName');

    setOption(panelTabName);
  };

  const onSave = () => {
    // save here
    setExtensionData('settings', { defaultPanelTabName: currentOption });

    onClose();

    message.success(t('SaveMessage'));

    setTimeout(() => location.reload(), 1000);
  };

  const hasErrors = false;

  return (
    <React.Fragment>
      <div className="PredictionPanelPreferences">
        <div className="language">
          <label htmlFor="language-select" className="languageLabel">
            {t('Showing result')}
          </label>
          <LanguageSwitcher
            language={currentOption}
            onLanguageChange={setOption}
            languages={availableOptions}
          />
        </div>
      </div>
      <TabFooter
        onResetPreferences={onResetPreferences}
        onSave={onSave}
        onCancel={onClose}
        hasErrors={hasErrors}
        t={t}
      />
    </React.Fragment>
  );
}

PredictionPanelPreferences.propTypes = {
  onClose: PropTypes.func,
};

export { PredictionPanelPreferences };
