import React from 'react';
import PropTypes from 'prop-types';

import { TabComponents } from '@tuvm/ui';

// Tabs
import { HotkeysPreferences } from './HotkeysPreferences';
import { WindowLevelPreferences } from './WindowLevelPreferences';
import { GeneralPreferences } from './GeneralPreferences';
// import { PredictionPanelPreferences } from './PredictionPanelPreferences';
import { useTranslation } from 'react-i18next';

import './UserPreferences.styl';

const tabs = [
  {
    name: 'General Settings',
    Component: GeneralPreferences,
    customProps: {},
  },
  // {
  //   name: 'Analysis Panel',
  //   Component: PredictionPanelPreferences,
  //   customProps: {},
  // },
  {
    name: 'Hotkeys',
    Component: HotkeysPreferences,
    customProps: {},
  },
  {
    name: 'Preset Windows',
    Component: WindowLevelPreferences,
    customProps: {},
  },
];

function UserPreferences({ hide }) {
  const { t } = useTranslation('Vindoc');

  const customProps = {
    onClose: hide,
  };

  const newTabs = tabs.map(tab => {
    return {
      ...tab,
      name: t(tab.name),
    };
  });
  return <TabComponents tabs={newTabs} customProps={customProps} />;
}

UserPreferences.propTypes = {
  hide: PropTypes.func,
};

export { UserPreferences };
