import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { message, Switch } from 'antd';
import i18n from '@tuvm/i18n';
import { TabFooter, LanguageSwitcher } from '@tuvm/ui';
import { useTranslation } from 'react-i18next';
import get from 'lodash/get';
import { redux } from '@tuvm/core';
import { useDispatch, useSelector } from 'react-redux';

import './GeneralPreferences.styl';

/**
 * General Preferences tab
 * It renders the General Preferences content
 *
 * @param {object} props component props
 * @param {function} props.onClose
 */
function GeneralPreferences({ onClose }) {
  const { t } = useTranslation('Vindoc');
  const dispatch = useDispatch();
  const extensions = useSelector(state => state && state.extensions);

  const currentLanguage = i18n.language;
  const { availableLanguages } = i18n;

  const newLanguagesOptions = availableLanguages.map(lang => {
    return {
      ...lang,
      label: t(lang.label),
    };
  });

  const settings = get(extensions, 'settings');

  const [state, setState] = useState({
    isVisibleAnnotation: settings.isVisibleAnnotation,
    isVisibleAllTag: settings.isVisibleAllTag,
    // isVisibleLocalTag: settings.isVisibleLocalTag
  });

  useEffect(() => {
    if (settings) {
      setState({
        isVisibleAnnotation: settings.isVisibleAnnotation,
        isVisibleAllTag: settings.isVisibleAllTag,
      });
    }
  }, [settings, setState]);

  const [language, setLanguage] = useState(currentLanguage);

  const onResetPreferences = () => {
    setLanguage(i18n.defaultLanguage);
  };

  const onSave = () => {
    i18n.changeLanguage(language);

    onClose();

    message.success(t('SaveMessage'));

    const newSettings = {
      ...settings,
      isVisibleAnnotation: state.isVisibleAnnotation,
      isVisibleAllTag: state.isVisibleAllTag,
      // isVisibleLocalTag: isVisibleLocalTag,
    };

    localStorage.setItem('settings', JSON.stringify(newSettings));

    dispatch(redux.actions.setExtensionData('settings', newSettings));

    // location.reload();
  };

  const setVisibleSetting = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const hasErrors = false;

  return (
    <React.Fragment>
      <div className="GeneralPreferences">
        <div className="language setting-item">
          <label htmlFor="language-select" className="languageLabel">
            {t('Language')}
          </label>
          <LanguageSwitcher
            language={language}
            onLanguageChange={setLanguage}
            languages={newLanguagesOptions}
          />
        </div>

        <div className="language setting-item">
          <label htmlFor="language-select" className="languageLabel">
            {t('Show Bounding Boxes')}
          </label>
          <Switch
            size="small"
            checked={state.isVisibleAllTag}
            onChange={value => setVisibleSetting('isVisibleAllTag', value)}
          />
        </div>

        <div className="language setting-item">
          <label htmlFor="language-select" className="languageLabel">
            {t('Show DICOM info')}
          </label>
          <Switch
            size="small"
            checked={state.isVisibleAnnotation}
            onChange={value => setVisibleSetting('isVisibleAnnotation', value)}
          />
        </div>
      </div>
      <TabFooter
        onResetPreferences={onResetPreferences}
        onSave={onSave}
        onCancel={onClose}
        hasErrors={hasErrors}
        t={t}
      />
    </React.Fragment>
  );
}

GeneralPreferences.propTypes = {
  onClose: PropTypes.func,
};

export { GeneralPreferences };
