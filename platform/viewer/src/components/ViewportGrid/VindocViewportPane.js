import React from 'react';
import { useDrop } from 'react-dnd';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useSelector } from 'react-redux';

import './ViewportPane.css';

const ViewportPane = function(props) {
  const { children, onDrop, viewportIndex, className: propClassName } = props;

  const { layout } = useSelector(state => {
    const { viewports = {} } = state;
    const { viewportSpecificData, layout } = viewports;

    return {
      viewportSpecificData,
      layout,
    };
  });

  const [{ hovered, highlighted }, drop] = useDrop({
    accept: 'thumbnail',
    drop: (droppedItem, monitor) => {
      const canDrop = monitor.canDrop();
      const isOver = monitor.isOver();

      if (canDrop && isOver && onDrop) {
        const { StudyInstanceUID, displaySetInstanceUID } = droppedItem;

        onDrop({ viewportIndex, StudyInstanceUID, displaySetInstanceUID });
      }
    },
    // Monitor, and collect props.
    // Returned as values by `useDrop`
    collect: monitor => ({
      highlighted: monitor.canDrop(),
      hovered: monitor.isOver(),
    }),
  });

  const handleDoubleClickViewport = event => {
    const { viewports = [] } = layout || {};
    const numOfViewport = viewports.length;

    if (numOfViewport < 2) {
      return;
    }

    try {
      const currentElm = event.currentTarget;
      const isExpandItem = currentElm.classList.contains('is-expand-item');

      if (isExpandItem) {
        currentElm.classList.remove('is-expand-item');
      } else {
        currentElm.classList.add('is-expand-item');
      }

      const allViewportElms =
        document.querySelectorAll('.js-viewport-grid-item') || [];

      allViewportElms.forEach(elm => {
        if (isExpandItem) {
          elm.classList.remove('is-absolute-item');
        } else {
          elm.classList.add('is-absolute-item');
        }
      });

      const viewportContainer = document.querySelector(
        '.js-viewport-gird-container'
      );

      if (isExpandItem) {
        viewportContainer.classList.remove('expand-container');
      } else {
        viewportContainer.classList.add('expand-container');
      }
    } catch (error) {
      console.log('Toggle viewport fail');
    }
  };

  return (
    <div
      className={classNames(
        'viewport-drop-target js-viewport-grid-item',
        { hovered: hovered },
        { highlighted: highlighted },
        propClassName
      )}
      ref={drop}
      data-cy={`viewport-container-${viewportIndex}`}
      onDoubleClick={handleDoubleClickViewport}
      style={props.style || {}}
    >
      {children}
    </div>
  );
};

ViewportPane.propTypes = {
  children: PropTypes.node.isRequired,
  viewportIndex: PropTypes.number.isRequired,
  onDrop: PropTypes.func.isRequired,
  className: PropTypes.string,
};

export default ViewportPane;
