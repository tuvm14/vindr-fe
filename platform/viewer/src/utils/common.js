import toNumber from 'lodash/toNumber';
import max from 'lodash/max';
import get from 'lodash/get';

export const orderListObject = (key, order = 'asc') => {
  return function(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      return 0;
    }

    const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
    const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return order == 'desc' ? comparison * -1 : comparison;
  };
};

export function groupBy(list, props) {
  return list.reduce((a, b) => {
    (a[b[props]] = a[b[props]] || []).push(b);
    return a;
  }, {});
}

export function getTextByLanguage(array) {
  const i18nextLng = localStorage.getItem('i18nextLng');
  const result = array.find(item => item.lang === i18nextLng);

  return get(result, 'value');
}

const chest = ['CR', 'DX'];

const mammo = ['MG'];

const isChest = modality => {
  return chest.includes(modality);
};

const isMammo = modality => {
  return mammo.includes(modality);
};

const isBreastBody = bodyPart => {
  return bodyPart.toLowerCase() === 'breast';
};

const isChestBody = bodyPart => {
  return bodyPart.toLowerCase() === 'chest';
};

export const isBrainBody = bodyPart => {
  return bodyPart.toLowerCase() === 'brain';
};

const isAITab = (panelTabName = '') => {
  return panelTabName.toLowerCase() === panelTabNames.aiTab.toLowerCase();
};

const isDoctorTab = (panelTabName = '') => {
  return panelTabName.toLowerCase() === panelTabNames.doctorTab.toLowerCase();
};

const panelTabNames = {
  aiTab: 'ai-tab',
  doctorTab: 'doctor-tab',
};

const isClickOnSameTab = (index, currentTabName, panelTabName) => {
  let docdorTabOrder = 0,
    aiTabOrder = 1,
    currentTabOrder = 0;

  if (isAITab(panelTabName)) {
    aiTabOrder = 0;
    docdorTabOrder = 1;
  }

  if (isDoctorTab(currentTabName)) {
    currentTabOrder = docdorTabOrder;
  } else {
    currentTabOrder = aiTabOrder;
  }

  return index === currentTabOrder;
};

export const uniqueListItems = arr => {
  var u = {},
    a = [];
  for (var i = 0, l = arr.length; i < l; ++i) {
    if (!u.hasOwnProperty(arr[i])) {
      a.push(arr[i]);
      u[arr[i]] = 1;
    }
  }
  return a;
};

export const getBestValueOfTwoObject = (obj1, obj2) => {
  const mergedList = { ...obj1, ...obj2 };
  const keys = Object.keys(mergedList);

  keys.forEach(key => {
    if (obj1[key] && obj2[key]) {
      const value1 = toNumber(obj1[key]);
      const value2 = toNumber(obj2[key]);

      const maxValue = max([value1, value2]);
      mergedList[key] = maxValue;
    }
  });

  return mergedList;
};

function formatDataSelect(list, labelKey, valueKey) {
  let result = [];
  if (Array.isArray(list)) {
    result = list.map(item => {
      return {
        ...item,
        label: item[labelKey],
        value: item[valueKey],
      };
    });
  }

  return result;
}

function getBodyPart(modality = '', bodyPart = '') {
  if (isMammo(modality) || (bodyPart && bodyPart.toLowerCase() === 'breast')) {
    return 'BREAST';
  }
  if (isChest(modality) || (bodyPart && bodyPart.toLowerCase() === 'chest')) {
    return 'CHEST';
  }
  return '';
}

function checkMammoPosition(SOPInstanceUID, instance, side) {
  if (side === 'rightSide') {
    return (
      (instance.SOPInstanceUID === SOPInstanceUID &&
        (instance.ViewPosition === 'MLO' || instance.ViewPosition === 'CC') &&
        (instance.ImageLaterality === 'R' || instance.Laterality === 'R')) ||
      (instance.SOPInstanceUID === SOPInstanceUID &&
        instance.AcquisitionDeviceProcessingDescription === 'RMLO') ||
      (instance.SOPInstanceUID === SOPInstanceUID &&
        instance.AcquisitionDeviceProcessingDescription === 'RCC')
    );
  }

  return (
    (instance.SOPInstanceUID === SOPInstanceUID &&
      (instance.ViewPosition === 'MLO' || instance.ViewPosition === 'CC') &&
      (instance.ImageLaterality === 'L' || instance.Laterality === 'L')) ||
    (instance.SOPInstanceUID === SOPInstanceUID &&
      instance.AcquisitionDeviceProcessingDescription === 'LMLO') ||
    (instance.SOPInstanceUID === SOPInstanceUID &&
      instance.AcquisitionDeviceProcessingDescription === 'LCC')
  );
}

export {
  isChest,
  isMammo,
  isAITab,
  isDoctorTab,
  isClickOnSameTab,
  panelTabNames,
  formatDataSelect,
  getBodyPart,
  isChestBody,
  isBreastBody,
  checkMammoPosition,
};
