import cornerstoneWADOImageLoader from 'cornerstone-wado-image-loader';

let initialized = false;

export default function initWebWorkers() {
  const config = {
    maxWebWorkers: Math.max(navigator.hardwareConcurrency - 1, 1),
    startWebWorkersOnDemand: true,
    taskConfiguration: {
      decodeTask: {
        initializeCodecsOnStartup: true,
        usePDFJS: false,
        strict: false,
      },
    },
  };

  if (!initialized) {
    cornerstoneWADOImageLoader.webWorkerManager.initialize(config);
    cornerstoneWADOImageLoader.configure({
      beforeSend: function(xhr, imageId, headers) {
        // delete previous accept
        delete headers.accept;

        // JPEG2000 Lossless Image Compression
        const accept = 'multipart/related; type="image/jp2"';

        //const accept = 'multipart/related; type="application/octet-stream"; transfer-syntax="1.2.840.10008.1.2.1"';
        xhr.setRequestHeader('Accept', accept);
      },
    });
    initialized = true;
  }
}
