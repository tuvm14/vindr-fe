export const documentS3Url =
  'https://vindoc-public.s3-ap-southeast-1.amazonaws.com/HDSD-VINDOC.pdf';

export const roles = {
  vindrAdmin: 'vindr-admin',
  vindrTechnicianAdmin: 'vindr-technician.admin',
  vindrTechnicianWorklist: 'vindr-technician.worklist',
  vindrStudyListView: 'vindr-study-list.view',
  vindrAIResult: 'vindr-ai.result',
  vindrViewerTabDoctorFirst: 'vindr-viewer-tab.doctor-first',
  vindrDoctorResult: 'vindr-doctor.result',
  vindrAIDiagnosis: 'vindr-ai.diagnosis',
  vindrStudyListUpload: 'vindr-study-list.upload',
  vindrStudyListAssign: 'vindr-study-list.assign',
  vindrOrderListAssign: 'vindr-order-list.assign',
  vindrOrderListView: 'vindr-order-list.view',
  vindrWorkgroupAssign: 'vindr-workgroup.assign',
  vindrWorkgroupView: 'vindr-workgroup.view',
  vindrViewerReview: 'vindr-viewer.review',
  vindrLabelling: 'vindr-labelling',
  vindrViewerTabDoctorOld: 'vindr-viewer-tab.doctor-old',
};

export const SCOPES = {
  apiAll: 'all',
  globalAdmin: 'admin',
  worklistAdmin: 'worklist_admin',
  reportSubmit: 'report_submit',
  reportApprove: 'report_approve',
  reportRevoke: 'report_revoke',
  reportView: 'report_view',
  studyView: 'study_view',
  studyUpload: 'study_upload',
  sysdirAdmin: 'sysdir_admin',
  sysdirView: 'sysdir_view',
  organizationAdmin: 'organization_admin',
  aiDiagnose: 'ai_diagnose',
  aiResult: 'ai_result',
};

export const FEATURES = {
  ris: 'ris_enabled',
  nondicom: 'nondicom_enabled',
  publicShare: 'public_share_enabled',
  presentationState: 'presentation_state_enabled',
};

export const NEW_SCOPES = {
  apiAll: 'all',
  globalAdmin: 'admin',
  worklist: {
    studylist: {
      view: 'worklist.studylist.view',
      admin: 'worklist.studylist.admin',
    },
    exam: {
      view: 'worklist.exam.view',
      bookmark: 'worklist.exam.bookmark',
      upload: 'worklist.exam.upload',
      share: 'worklist.exam.share',
      download: 'worklist.exam.download',
      admin: 'worklist.exam.admin',
    },
    bookmark: {
      create: 'worklist.bookmark.create',
      edit: 'worklist.bookmark.edit',
      view: 'worklist.bookmark.view',
      delete: 'worklist.bookmark.delete',
      admin: 'worklist.bookmark.admin',
    },
    report: {
      create: 'worklist.report.create',
      submit: 'worklist.report.submit',
      approve: 'worklist.report.approve',
      view: 'worklist.report.view',
      admin: 'worklist.report.admin',
    },
    cad: {
      view: 'worklist.cad.view',
      analyze: 'worklist.cad.analyze',
      admin: 'worklist.cad.admin',
    },
    attachmentfile: {
      view: 'worklist.attachmentfile.view',
      upload: 'worklist.attachmentfile.upload',
      download: 'worklist.attachmentfile.download',
      delete: 'worklist.attachmentfile.delete',
    },
    presentationstate: {
      view: 'worklist.presentationstate.view',
      create: 'worklist.presentationstate.create',
      delete: 'worklist.presentationstate.delete',
    }
  },
  nondicom: {
    create: 'nondicom.create',
    admin: 'nondicom.admin',
    report: {
      create: 'nondicom.report.create',
      submit: 'nondicom.report.submit',
      approve: 'nondicom.report.approve',
      view: 'nondicom.report.view',
      admin: 'nondicom.report.admin',
    },
    orderlist: {
      view: 'nondicom.orderlist.view',
      admin: 'nondicom.orderlist.admin',
    },
  },
  order: {
    studylist: {
      view: 'order.studylist.view',
      admin: 'order.studylist.admin',
    },
    match: 'order.match',
    unmatch: 'order.unmatch',
    view: 'order.view',
    admin: 'order.admin',
  },
  administration: {
    institute: {
      edit: 'administration.institute.edit',
      view: 'administration.institute.view',
      delete: 'administration.institute.delete',
      admin: 'administration.institute.admin',
    },
    role: {
      create: 'administration.role.create',
      edit: 'administration.role.edit',
      view: 'administration.role.view',
      admin: 'administration.role.admin',
    },
    user: {
      create: 'administration.user.create',
      edit: 'administration.user.edit',
      view: 'administration.user.view',
      delete: 'administration.user.delete',
      admin: 'administration.user.admin',
    },
    report_template: {
      create: 'administration.report_template.create',
      edit: 'administration.report_template.edit',
      view: 'administration.report_template.view',
      delete: 'administration.report_template.delete',
      admin: 'administration.report_template.admin',
    },
    report_layout: {
      create: 'administration.report_layout.create',
      edit: 'administration.report_layout.edit',
      view: 'administration.report_layout.view',
      delete: 'administration.report_layout.delete',
      admin: 'administration.report_layout.admin',
    },
    filter: {
      create: 'administration.filter.create',
      edit: 'administration.filter.edit',
      view: 'administration.filter.view',
      delete: 'administration.filter.delete',
      admin: 'administration.filter.admin',
    },
  },
  dashboard: {
    view: 'dashboard.view',
    admin: 'dashboard.admin',
  },
};

export const DEFAULT_ROLES = {
  superAdmin: 'vindr-admin',
  rad0: 'vindr-rad0',
  rad1: 'vindr-rad1',
  rad2: 'vindr-rad2',
  tech0: 'vindr-technician0',
  orgAdmin: 'vindr-org-admin',
};

export const ROWS_PER_PAGE = [25, 50, 100, 150, 200, 250, 500, 750, 1000];

export const NAV_LIST = {
  STUDYLIST: {
    key: 'studylist',
    url: '/studylist',
    label: 'Radiologist',
    permission: [
      SCOPES.studyView,
      SCOPES.globalAdmin,
      NEW_SCOPES.worklist.studylist.view,
    ],
  },
  NONDICOM: {
    key: 'nondicom',
    url: '/nondicom/worklist',
    label: 'Non Dicom',
    permission: [
      SCOPES.worklistAdmin,
      SCOPES.globalAdmin,
      NEW_SCOPES.nondicom.create,
    ],
    feature: FEATURES.nondicom,
  },
  WORKLIST: {
    key: 'worklist',
    url: '/technician/worklist',
    label: 'Technologist',
    permission: [
      SCOPES.worklistAdmin,
      SCOPES.globalAdmin,
      NEW_SCOPES.order.view,
    ],
  },
  SYSTEM: {
    key: 'system',
    url: '/system/institution',
    label: 'Administrator',
    permission: [
      SCOPES.organizationAdmin,
      SCOPES.globalAdmin,
      NEW_SCOPES.administration.institute.view,
      NEW_SCOPES.administration.role.view,
      NEW_SCOPES.administration.user.view,
      NEW_SCOPES.administration.report_template.view,
      NEW_SCOPES.administration.report_layout.view,
      NEW_SCOPES.administration.filter.view,
    ],
  },
};

export const windowLevelInModalities = ['CT'];

export const STUDY_LIST_ACTION_NAMES = {
  VIEW: 'VIEW',
  BATCH_AI_DIAGNOSIS: 'BATCH_AI_DIAGNOSIS',
  ASSIGN_DOCTOR: 'ASSIGN_DOCTOR',
  COPY_TO_FOLDER: 'COPY_TO_FOLDER',
  BOOKMARK: 'BOOKMARK',
  SHARE: 'SHARE',
  COMPARE: 'COMPARE',
  OPEN_REPORT: 'OPEN_REPORT',
  ATTACH_FILE: 'ATTACH_FILE',
};

export const STUDY_LIST_ACTIONS = [
  {
    name: 'Batch AI Analysis',
    action: STUDY_LIST_ACTION_NAMES.BATCH_AI_DIAGNOSIS,
    roles: [roles.vindrAIDiagnosis],
  },
  {
    name: 'View',
    action: STUDY_LIST_ACTION_NAMES.VIEW,
    roles: '*',
  },
  {
    name: 'Copy to my folder',
    action: STUDY_LIST_ACTION_NAMES.COPY_TO_FOLDER,
    roles: '*',
  },
  // {
  //   name: 'Assign doctor',
  //   action: STUDY_LIST_ACTION_NAMES.ASSIGN_DOCTOR,
  //   roles: [roles.vindrOrderListAssign, roles.vindrStudyListAssign],
  // },
];

export const AIAnalyzeActions = {
  CHEST_XRAY: 'CHEST_XRAY',
  MAMMOGRAPHY: 'MAMMOGRAPHY',
  BRAIN_CT: 'BRAIN_CT',
  LUNG_CT: 'LUNG_CT',
  LIVER_CT: 'LIVER_CT',
  BRAIN_MRI: 'BRAIN_MRI',
  SPINE_XR: 'SPINE_XR',
};

export const AI_MODEL = {
  CHEST_XRAY: 'chestxray',
  MAMMOGRAPHY: 'mammogram',
  LUNG_CT: 'lungct',
  LIVER_CT: 'liverct',
  BRAIN_MRI: 'brainmri',
  BRAIN_CT: 'brainct',
  SPINE_XR: 'spinexr',
};

export const AIAnalyzeList = [
  {
    name: 'Chest X-ray',
    action: AIAnalyzeActions.CHEST_XRAY,
    active: true,
    key: AI_MODEL.CHEST_XRAY,
  },
  {
    name: 'Spine XR',
    action: AIAnalyzeActions.SPINE_XR,
    active: true,
    key: AI_MODEL.SPINE_XR,
  },
  {
    name: 'Mammography',
    action: AIAnalyzeActions.MAMMOGRAPHY,
    active: true,
    key: AI_MODEL.MAMMOGRAPHY,
  },
  {
    name: 'Lung CT',
    action: AIAnalyzeActions.LUNG_CT,
    active: true,
    key: AI_MODEL.LUNG_CT,
  },
  {
    name: 'Liver CT',
    action: AIAnalyzeActions.LIVER_CT,
    active: true,
    key: AI_MODEL.LIVER_CT,
  },
  {
    name: 'Brain CT',
    action: AIAnalyzeActions.BRAIN_CT,
    active: true,
    key: AI_MODEL.BRAIN_CT,
  },
  {
    name: 'Brain MRI',
    action: AIAnalyzeActions.BRAIN_MRI,
    active: true,
    key: AI_MODEL.BRAIN_MRI,
  },
];

export const MODALITIES = [
  'Default',
  'CR',
  'DX',
  'DR',
  'CT',
  'MR',
  'MG',
  'US',
  'ES',
  'PT',
  'ST',
  'XA',
  'SR',
  'SM',
  'Other',
];

export const BODY_PARTS = {
  CHEST: 'CHEST',
  BREAST: 'BREAST',
  LUNG: 'LUNG',
};

export const SPECIAL_CONCLUSIONS = {
  NOT_AVAILABLE: 'N/A',
  NO_FINDING: 'NO FINDING',
  NO_CONCLUSION: 'N/C',
  NEED_FURTHER_INVESTIGATIONS: 'NFI',
};

export const PANEL_TAB_NAMES = {
  AI_TAB: 'ai-tab',
  DOCTOR_TAB: 'doctor-tab',
};

export const FEEDBACK_STATUS = {
  LIKE: 'LIKE',
  DISLIKE: 'DISLIKE',
  NOTE: 'NOTE',
};

export const FEEDBACK_ACTIONS = {
  RATING: 'RATING',
  COMMENT: 'COMMENT',
  VIEW_MORE: 'VIEW_MORE',
  VIEWER_OPEN: 'VIEWER_OPEN',
};

export const MAMMO_GLOBAL_KEY = ['BIRADS', 'DENSITY'];

export const SEGMENT_DEFAULT_INDEX = 10;

export const AI_RESULT_SCOPES = {
  GLOBAL: 'GLOBAL',
  LOCAL: 'LOCAL',
};

export const LANGUAGES = {
  VI: 'vi',
  US: 'en-US',
};

export const HOVER_BOX_TIMER = 500; // 500 mini seconds

// common
export const TOKEN = 'token';
export const PERMISSIONS = 'permissions';
export const PERMISSION_TOKEN = 'permission_token';
export const POLLING_TOKEN_TIMER = 4 * 60000;
export const STUDY_OPENING = 'study_opening';
export const REALM_ID = 'realm_id';
export const FEATURES_KEY = 'features';
export const ACCESS_KEY = 'x_access_key';
export const X_ACCESS_KEY = 'X-ACCESS-KEY';
export const REFRESH_TOKEN = 'refresh_token';
export const BASE_TIME = '19600101';
export const MOBILE_SIZE_LIMIT = 600;

export let CONFIG_SERVER = {
  BACKEND_URL: process.env.BACKEND_URL + '/api',
  AUTH_URL: process.env.AUTH_URL,
  DISABLE_AUTH: process.env.DISABLE_AUTH == 'true',
  WEBSOCKET_URL: process.env.WEBSOCKET_URL,
  REDIRECT_URI: process.env.REDIRECT_URI || window.origin,

  SCOPE: process.env.OIDC_SCOPE || 'profile',

  OIDC_ACCESS_TOKEN_URI: process.env.OIDC_ACCESS_TOKEN_URI,
  OIDC_LOGOUT_URI: process.env.OIDC_LOGOUT_URI,
  OIDC_USERINFO_ENDPOINT: process.env.OIDC_USERINFO_ENDPOINT,
  OIDC_REDIRECT_URI: process.env.OIDC_REDIRECT_URI || window.origin,
  STATE: Math.random()
    .toString(36)
    .substring(2),
  OIDC_AUTHORIZATION_URI: process.env.OIDC_AUTHORIZATION_URI,
  AUDIENCE: process.env.OIDC_AUDIENCE || 'vindr-api',
  RESPONSE_TYPE: 'code',
  CLIENT_ID: process.env.OIDC_CLIENT_ID || 'vindr-frontend',
};

export const REPORT_STATUS = {
  NEW: 'NEW',
  READING: 'READING',
  SUBMITTED: 'SUBMITTED',
  APPROVED: 'APPROVED',
};

export const DOCTOR_TAB_ACTIONS = {
  SUBMIT: 'SUBMIT',
  APPROVE: 'APPROVE',
  REVOKE: 'REVOKE',
  DONE: 'DONE',
  REJECT: 'REJECT',
  CANCEL: 'CANCEL',
  CREATE_REPORT: 'CREATE_REPORT',
  CANCEL_REPORT: 'CANCEL_REPORT',
};

export const FOLDER_TYPE = {
  ALL_STUDIES: 'ALL_STUDIES',
  DEVICES: 'DEVICES',
  PRIVATE: 'PRIVATE',
  USER_UPLOADS: 'USER_UPLOADS',
  SHARED: 'SHARED',
  BOOKMARK: 'BOOKMARK',
};

export const VINDR_AI_SCOPES = {
  SERIES_SCOPE: ['brainct', 'liverct', 'brainmri', 'lungct'],
  STUDY_SCOPE: ['mammogram'],
  INSTANCE_SCOPE: ['chestxray', 'spinexr'],
};

export const FILTER_TYPE = [
  { name: 'Conditional Filter', value: 'cond' },
  { name: 'Unconditional Filter', value: 'no_cond' },
];

export const FILTER_CONDITION_SET = [
  { value: 'StudyDate', name: 'StudyDate' },
  { value: 'StudyTime', name: 'StudyTime' },
  { value: 'AccessionNumber', name: 'AccessionNumber' },
  { value: 'ReferringPhysicianName', name: 'ReferringPhysicianName' },
  { value: 'StudyInstanceUID', name: 'StudyInstanceUID' },
  { value: 'StudyID', name: 'StudyID' },
  { value: 'Modality', name: 'Modality' },
  { value: 'ModalitiesInStudy', name: 'ModalitiesInStudy' },
  { value: 'SeriesInstanceUID', name: 'SeriesInstanceUID' },
  { value: 'SeriesNumber', name: 'SeriesNumber' },
  { value: 'SOPClassUID', name: 'SOPClassUID' },
  { value: 'SOPInstanceUID', name: 'SOPInstanceUID' },
  { value: 'InstanceNumber', name: 'InstanceNumber' },
  { value: 'PatientAge', name: 'PatientAge' },
  { value: 'PatientName', name: 'PatientName' },
  { value: 'PatientID', name: 'PatientID' },
  { value: 'PatientSex', name: 'PatientSex' },
  { value: 'BodyPartExamined', name: 'BodyPartExamined' },
  { value: 'Owner', name: 'Owner' },
  { value: 'Assignees', name: 'Assignees' },
  { value: 'VindocStatus', name: 'VindocStatus' },
  { value: 'StudyDescription', name: 'StudyDescription' },
  { value: 'vindr.AETitle', name: 'AE Title' },
  { value: 'vindr.Owner', name: 'VindrOwner' },
  { value: 'VindocAIResult', name: 'VindocAIResult' },
  { value: 'DoctorResult', name: 'DoctorResult' },
  { value: 'TimeInserted', name: 'TimeInserted' },
  { value: 'IsAbnormal', name: 'IsAbnormal' },
];

export const ALL_USER_USERNAME_VALUE = '__all_user__';
export const FILTER_ALL_NAME = '__all__';
