import cloneDeep from 'lodash.clonedeep';
import get from 'lodash/get';
import { redux } from '@tuvm/core';
import OHIF from '@tuvm/core';
import cornerstoneTools from '@vindr/vindr-tools';
import { isEmpty, map, join } from 'lodash';
import cornerstone from 'cornerstone-core';
// import moment from 'moment';

import { commandsManager } from '../App';
import {
  ACCESS_KEY,
  LANGUAGES,
  MOBILE_SIZE_LIMIT,
  REALM_ID,
  roles,
  SEGMENT_DEFAULT_INDEX,
} from './constants';
// import { isAITab } from './common';
import intersection from 'lodash/intersection';

const { setExtensionData: setExtensionDataStore } = redux.actions;

export const htmlToText = html => {
  var divContainer = document.createElement('div');
  divContainer.innerHTML = html;
  return divContainer.textContent || divContainer.innerText || '';
};

export const getViewportApiKey = () => {
  const viewerLink = '/viewer';
  const url = new URL(window.location.href);
  if (url && url.pathname && url.pathname.indexOf(viewerLink) == -1) {
    return null;
  }
  const tempToken = url.searchParams.get(ACCESS_KEY);
  return tempToken;
};

export const getExtensionData = keyName => {
  const store = window.store.getState();
  const extensions = get(store, 'extensions');

  if (extensions) {
    return cloneDeep(extensions[keyName]);
  }
};

export const setExtensionData = (keyName, data) => {
  window.store.dispatch(setExtensionDataStore(keyName, data));
};

export const drawMeasurements = measurementData => {
  if (measurementData) {
    try {
      const measurementApi = OHIF.measurements.MeasurementApi.Instance;
      if (measurementApi) {
        measurementApi.addMeasurements(measurementData);
      }
    } catch (error) {
      console.log(error);
    }
  }
};

export const clearMeasurementData = () => {
  try {
    const store = window.store.getState();
    const viewports = get(store, 'viewports');

    const RectangleRoi = get(
      store,
      'timepointManager.measurements.RectangleRoi'
    );
    if (viewports && RectangleRoi && RectangleRoi.length) {
      commandsManager.runCommand('clearAllAnnotations', {
        measurementsToRemove: RectangleRoi,
      });
    }
  } catch (error) {
    console.log(error);
  }
};

export const clearAllMeasurementData = () => {
  try {
    const store = window.store.getState();
    const viewports = get(store, 'viewports');

    const measurements = get(store, 'timepointManager.measurements');
    let flatMeasurements = [];
    for (let key of Object.keys(measurements)) {
      flatMeasurements = [...flatMeasurements, ...measurements[key]];
    }
    if (viewports && flatMeasurements && flatMeasurements.length) {
      commandsManager.runCommand('clearAllAnnotations', {
        measurementsToRemove: flatMeasurements,
      });
    }
  } catch (error) {
    console.log(error);
  }
};

export const isWorkflowAIFirst = () => {
  const store = window.store.getState();
  const userRoles = get(store, 'extensions.user.roles');

  return userRoles.includes(roles.vidrWorkflowAIFirst);
};

export const isViewerDoctorTabFirst = () => {
  const store = window.store.getState();
  const userRoles = get(store, 'extensions.user.roles');

  return userRoles.includes(roles.vindrViewerTabDoctorFirst);
};

export const isMammoRight = metadata => {
  return (
    metadata.ImageLaterality === 'R' ||
    metadata.Laterality === 'R' ||
    metadata.AcquisitionDeviceProcessingDescription === 'RMLO' ||
    metadata.AcquisitionDeviceProcessingDescription === 'RCC'
  );
};

export const isMammoLeft = metadata => {
  return (
    metadata.ImageLaterality === 'L' ||
    metadata.Laterality === 'L' ||
    metadata.AcquisitionDeviceProcessingDescription === 'LMLO' ||
    metadata.AcquisitionDeviceProcessingDescription === 'LCC'
  );
};

export const getPosition = metadata => {
  if (isMammoRight(metadata)) return 'R';
  if (isMammoLeft(metadata)) return 'L';

  return null;
};

export const isvindrStudyListView = userRoles => {
  return userRoles.includes(roles.vindrStudyListView);
};

export const isvindrStudyListAssign = userRoles => {
  return userRoles.includes(roles.vindrStudyListAssign);
};

export function hasRole(role) {
  const store = window.store.getState();
  const userRoles = get(store, 'extensions.user.roles');
  if (userRoles) {
    return userRoles.includes(role);
  }
  return false;
}

export const getFirstImageId = (StudyInstanceUID, displaySetInstanceUID) => {
  let firstImageId = '';
  const { utils } = OHIF;
  const { studyMetadataManager } = utils;
  const studyMetadata = studyMetadataManager.get(StudyInstanceUID);
  if (studyMetadata) {
    firstImageId = studyMetadata.getFirstImageId(displaySetInstanceUID);
    return firstImageId;
  }
};

let lastSeriesDrawSegment = '';

export const drawSegmentations = (AIResult, activeViewport) => {
  const { StackManager } = OHIF.utils;
  let interval,
    counter = 0;

  interval = setInterval(() => {
    if (counter === 100) clearInterval(interval);
    const enabledElements = cornerstone.getEnabledElements();
    counter += 1;
    const enabledElement = enabledElements[0] || {};
    const { rows, columns } = enabledElement.image || {};
    if (enabledElement && enabledElement.image) {
      clearInterval(interval);
      const { configuration, getters, setters } = cornerstoneTools.getModule(
        'segmentation'
      );

      configuration.fillAlpha = 0.2;
      configuration.fillAlphaInactive = 0.1;
      if (AIResult) {
        const seriesData = AIResult.series;

        Object.keys(seriesData).forEach(seriesInstanceUID => {
          const currentSeries = seriesData[seriesInstanceUID];
          const instances = currentSeries.instances;

          if (
            activeViewport.SeriesInstanceUID === seriesInstanceUID &&
            lastSeriesDrawSegment !== seriesInstanceUID
          ) {
            Object.keys(instances).forEach((instanceSOPId, index) => {
              const currentInstance = instances[instanceSOPId];
              const aiResult = get(currentInstance, 'ai_result');
              let localResult = get(aiResult, 'result.local');
              if (!isEmpty(localResult)) {
                let lb2dIdx = index;
                try {
                  const stack =
                    StackManager.findStack(
                      activeViewport.displaySetInstanceUID
                    ) || {};
                  const imageIdIndex =
                    stack.imageIds &&
                    stack.imageIds.findIndex(imageId => {
                      const imageIdSOPInstanceUID = cornerstone.metaData.get(
                        'SOPInstanceUID',
                        imageId
                      );
                      return imageIdSOPInstanceUID === instanceSOPId;
                    });

                  if (imageIdIndex > -1) {
                    lb2dIdx = imageIdIndex;
                  }
                } catch (error) {
                  console.log(error);
                }

                const segs = [];
                /*
                Segs is array of seg,
                seg: seg = [a, b,c,d]
                a: distance of box with top of image
                b: distance of box with left of image
                c: height of box
                d: width of box
                * */

                localResult.forEach(local => {
                  const { start, end } = local;
                  const distanceWithTop = start.y;
                  const distanceWithLeft = start.x;
                  const heightOfBox = end.y - start.y;
                  const widthOfBox = end.x - start.x;

                  const seg = [
                    distanceWithTop,
                    distanceWithLeft,
                    heightOfBox,
                    widthOfBox,
                  ];
                  segs.push(seg);
                });

                // let labelMapCount = 0;
                for (const seg of segs) {
                  // setters.activeLabelmapIndex(
                  //   enabledElement.element,
                  //   labelMapCount++
                  // );
                  const { labelmap3D } = getters.labelmap2D(
                    enabledElement.element
                  );
                  const l2dforImageIdIndex = getters.labelmap2DByImageIdIndex(
                    labelmap3D,
                    lb2dIdx,
                    rows,
                    columns
                  );
                  for (let r = 0; r < rows; r++) {
                    for (let c = 0; c < columns; c++) {
                      const k = r * columns + c;
                      if (l2dforImageIdIndex.pixelData[k] === 0) {
                        l2dforImageIdIndex.pixelData[k] =
                          r >= seg[0] &&
                          r < seg[0] + seg[2] &&
                          c >= seg[1] &&
                          c < seg[1] + seg[3]
                            ? SEGMENT_DEFAULT_INDEX
                            : 0;
                      }
                    }
                  }
                  lastSeriesDrawSegment = seriesInstanceUID;
                  setters.updateSegmentsOnLabelmap2D(l2dforImageIdIndex);
                }
              }
            });
            toggleAllSegments(SEGMENT_DEFAULT_INDEX);
          }
        });
      }
    }
  }, 200);
};

export const getSegmentsOnLabelmap = firstImageId => {
  const { state } = cornerstoneTools.getModule('segmentation');
  const brushStackState = state.series[firstImageId];
  if (brushStackState) {
    const { labelmaps2D } = brushStackState.labelmaps3D[0] || {};
    const { segmentsOnLabelmap = [] } = labelmaps2D[0] || {};
    return segmentsOnLabelmap;
  }
  return [];
};

export const toggleSegment = (tag = {}) => {
  const { setters } = cornerstoneTools.getModule('segmentation');
  const element = document.querySelector('.viewport-element');
  if (setters && element) {
    setters.toggleSegmentVisibility(
      element,
      tag.activeSegmentIndex,
      tag.labelMapIndex
    );
  }
};

export const toggleAllSegments = segmentIndex => {
  try {
    const module = cornerstoneTools.getModule('segmentation');
    const store = window.store.getState();
    const viewportSpecificData = get(store, 'viewports.viewportSpecificData');
    const activeViewportIndex = get(store, 'viewports.activeViewportIndex');
    const activeViewport = viewportSpecificData[activeViewportIndex];
    const { utils } = OHIF;
    const { studyMetadataManager } = utils;
    const studyMetadata = studyMetadataManager.get(
      activeViewport.StudyInstanceUID
    );
    const firstImageId = studyMetadata.getFirstImageId(
      activeViewport.displaySetInstanceUID
    );
    const brushStackState = module.state.series[firstImageId];
    if (brushStackState) {
      const labelmap3D =
        brushStackState.labelmaps3D[brushStackState.activeLabelmapIndex];
      const segmentsHidden = labelmap3D.segmentsHidden;
      segmentsHidden[segmentIndex] = !segmentsHidden[segmentIndex];
      !segmentsHidden[segmentIndex];
    }
  } catch (error) {
    console.log(error);
  }
};

export default function refreshViewports() {
  cornerstone.getEnabledElements().forEach(enabledElement => {
    cornerstone.updateImage(enabledElement.element);
  });
}

export const hasOneOfRoles = permissions => {
  const store = window.store.getState();
  const userRoles = get(store, 'extensions.user.roles');

  return intersection(permissions, userRoles).length > 0;
};

export const hasOneOfPermisstions = permissions => {
  const store = window.store.getState();
  const scopes = get(store, 'extensions.user.scopes');

  return intersection(permissions, scopes).length > 0;
};

export const getDateFormatted = () => {
  const i18n = require('@tuvm/i18n');
  const formatString =
    i18n.default.language === LANGUAGES.VI ? 'DD/MM/YYYY' : 'MM/DD/YYYY';

  return formatString;
};

export const setMeasurementDataToStore = (measurements, editorSourceHTML) => {
  setExtensionData('predictionPanel', {
    doctorTabContent: {
      measurementsData: measurements,
      editorSourceHTML,
    },
  });
};

export const setDoctorResultToStore = editorSourceHTML => {
  const store = window.store.getState();
  const timepointManager = get(store, 'timepointManager');
  const { measurements } = timepointManager;
  const newMeasurements = cloneDeep(measurements);
  setMeasurementDataToStore(newMeasurements, editorSourceHTML);
};

export const preFormatMeasurements = measurements => {
  const store = window.store.getState();
  const settings = get(store, 'extensions.settings') || {};
  const isVisibleAllTag = get(settings, 'isVisibleAllTag');

  Object.keys(measurements).map(key => {
    const toolData = measurements[key];
    let newToolData = toolData;

    if (toolData && toolData.length) {
      newToolData = toolData.map(measurement => {
        return {
          ...measurement,
          active: false,
          visible: true, //isVisibleAllTag,
          invalidated: false,
          isNearTool: false,
        };
      });
    }

    measurements[key] = newToolData;
  });

  return measurements;
};

export const getLastDoctorActionResult = actions => {
  let result = null;

  for (let i = 0; i < actions.length; i++) {
    const lastTime = get(result, 'last_update') || 0;
    if (actions[i] && actions[i].last_update > lastTime) {
      result = actions[i];
    }
  }

  return result;
};

export function groupBy(list, props) {
  return list.reduce((a, b) => {
    (a[b[props]] = a[b[props]] || []).push(b);
    return a;
  }, {});
}

export const toLuceneQueryString = (
  queryObject,
  separated = '&',
  mapKey = '='
) => {
  return join(
    map(queryObject, (value, field) => {
      if (value) {
        return `${field}${mapKey}${value}`;
      }
    }).filter(it => !isEmpty(it)),
    separated
  );
};
export function isThaiThuyHospital() {
  return `${window.origin}`.indexOf('dkthaithuy.vindr.ai') > -1;
}

export function isHungHaHospital() {
  return `${window.origin}`.indexOf('bvhungha.vindr.ai') > -1;
}

export const resetExpandLayout = () => {
  try {
    const viewportContainer = document.querySelector(
      '.js-viewport-gird-container'
    );

    if (viewportContainer.classList.contains('expand-container')) {
      viewportContainer.classList.remove('expand-container');

      const allViewportElms =
        document.querySelectorAll('.js-viewport-grid-item') || [];

      allViewportElms.forEach(elm => {
        elm.classList.remove('is-absolute-item');
        elm.classList.remove('is-expand-item');
      });
    }
  } catch (error) {
    console.log(error);
  }
};

export const getTenant = () => {
  const realmId = sessionStorage.getItem(REALM_ID);
  if (realmId) return realmId;
  const host = process.env.BACKEND_URL || window.location.hostname;
  if (host.indexOf('.vindr.ai') < 0) {
    return 'local';
  } else {
    return host
      .replace('.vindr.ai', '')
      .replace('https://', '')
      .replace('http://', '');
  }
};

export const jumpToImage = item => {
  commandsManager.runCommand('jumpToImage', {
    StudyInstanceUID: item.StudyInstanceUID,
    SOPInstanceUID: item.SOPInstanceUID,
    frameIndex: 0,
    activeViewportIndex: 0,
  });
};

export const isDiff = (A, B) => JSON.stringify(A) !== JSON.stringify(B);

export const uuidv4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    let r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;

    return v.toString(16);
  });
};

export const resizedataURL = (datas, x, y, scale) => {
  return new Promise(async function(resolve, reject) {
    // We create an image to receive the Data URI
    var img = document.createElement('img');

    // When the event "onload" is triggered we can resize the image.
    img.onload = function() {
      const inputWidth = img.naturalWidth;
      const inputHeight = img.naturalHeight;
      const wantedWidth = inputWidth / scale;
      const wantedHeight = inputHeight / scale;
      // We create a canvas and get its context.
      var canvas = document.createElement('canvas');
      var ctx = canvas.getContext('2d');

      // We set the dimensions at the wanted size.
      canvas.width = wantedWidth;
      canvas.height = wantedHeight;

      // We resize the image with the canvas method drawImage();
      ctx.drawImage(
        this,
        x,
        y,
        wantedWidth,
        wantedHeight,
        0,
        0,
        wantedWidth,
        wantedHeight
      );

      var dataURI = canvas.toDataURL();

      // This is the return of the Promise
      resolve(dataURI);
    };

    // We put the Data URI in the image's src attribute
    img.src = datas;
  });
}; // Use it like : var newDataURI = await resizedataURL('yourDataURIHere', 50, 50);

export const dataURLtoFile = (dataurl, filename) => {
  var arr = dataurl.split(','),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  const ext = mime.replace('image/', '');
  return new File([u8arr], `${filename}.${ext}`, { type: mime });
};

export const fileToDataURL = file => {
  return new Promise(resole => {
    let reader = new FileReader();
    reader.onload = e => {
      resole(e.target.result);
    };
    reader.readAsDataURL(file);
  });
};

export const dicomTagsAdapter = studyInfo => {
  let newStudy = {
    PatientName: studyInfo.patient_name || '',
    PatientID: studyInfo.patient_id || '',
    AccessionNumber: studyInfo.accession_number || '',
    Modality: studyInfo.modality || '',
  };
  if (studyInfo.patient_sex) {
    newStudy = {
      ...newStudy,
      BodyPartExamined: studyInfo.body_part || '',
      PatientSex: studyInfo.patient_sex || '',
      PatientAge: studyInfo.patient_age || '',
      StudyID: studyInfo.study_id || '',
      PatientBirthDate: studyInfo.patient_birth_date || '',
      StudyDescription: studyInfo.procedure_name || '',
      ReferringPhysicianName: studyInfo.referring_physician_name || '',
      PerformingPhysicianName: studyInfo.performing_physician_name || '',
      InstitutionName: studyInfo.institution_name || '',
    };
  }
  return newStudy;
};

export function parseJwt(token) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  var jsonPayload = decodeURIComponent(
    atob(base64)
      .split('')
      .map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join('')
  );

  return JSON.parse(jsonPayload);
}

export function localStorageSetWithExpiry(key, value, expireIn) {
  // `item` is an object which contains the original value
  // as well as the time when it's supposed to expire
  const item = {
    value: value,
    expiry: expireIn,
  };
  localStorage.setItem(key, JSON.stringify(item));
}

export function localStorageGetWithExpiry(key) {
  const itemStr = localStorage.getItem(key);
  // if the item doesn't exist, return null
  if (!itemStr) {
    return null;
  }
  const item = JSON.parse(itemStr);
  const now = new Date();
  // compare the expiry time of the item with the current time
  if (now.getTime() > item.expiry) {
    // If the item is expired, delete the item from storage
    // and return null
    localStorage.removeItem(key);
    return null;
  }
  return item.value;
}

export function isMobile(width, height) {
  return width < MOBILE_SIZE_LIMIT;
}
