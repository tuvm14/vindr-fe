import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { MODULE_TYPES } from '@tuvm/core';
import OHIF, { DICOMSR } from '@tuvm/core';
import { withDialog } from '@tuvm/ui';
import get from 'lodash/get';
import moment from 'moment';
import { Icon } from '@tuvm/ui';
import ConnectedHeader from '../../../components/Header/ConnectedHeader.js';
import ToolbarRow from './VinDrToolbarRow';
import ConnectedStudyBrowser from './ConnectedStudyBrowser.js';
import ConnectedViewerMain from './ViewerMain/index.js';
import SidePanel from '../../../components/SidePanel.js';
import { extensionManager } from '../../../App';
import { Container, Section, Bar } from 'react-simple-resizer';

import './VinDrViewer.scss';
import { isMobile, jumpToImage } from '../../../utils/helper.js';
import keycloak from '../../../services/auth.js';
import { actionGetAIResult } from '../../../system/customActions';
import { NEW_SCOPES, SCOPES } from '../../../utils/constants.js';
import { connect } from 'react-redux';
import { actionSetExtensionData } from '../../../system/systemActions.js';
import { LeftOutlined } from '@ant-design/icons';
import { withTranslation } from 'react-i18next';

const DEFAULT_SIZE = 370;

class Viewer extends Component {
  static propTypes = {
    studies: PropTypes.arrayOf(
      PropTypes.shape({
        StudyInstanceUID: PropTypes.string.isRequired,
        StudyDate: PropTypes.string,
        displaySets: PropTypes.arrayOf(
          PropTypes.shape({
            displaySetInstanceUID: PropTypes.string.isRequired,
            SeriesDescription: PropTypes.string,
            SeriesNumber: PropTypes.oneOfType([
              PropTypes.string,
              PropTypes.number,
            ]),
            InstanceNumber: PropTypes.oneOfType([
              PropTypes.string,
              PropTypes.number,
            ]),
            numImageFrames: PropTypes.number,
            Modality: PropTypes.string.isRequired,
            images: PropTypes.arrayOf(
              PropTypes.shape({
                getImageId: PropTypes.func.isRequired,
              })
            ),
          })
        ),
      })
    ),
    studyInstanceUIDs: PropTypes.array,
    activeServer: PropTypes.shape({
      type: PropTypes.string,
      wadoRoot: PropTypes.string,
    }),
    onTimepointsUpdated: PropTypes.func,
    onMeasurementsUpdated: PropTypes.func,
    // window.store.getState().viewports.viewportSpecificData
    viewports: PropTypes.object.isRequired,
    // window.store.getState().viewports.activeViewportIndex
    activeViewportIndex: PropTypes.number.isRequired,
    isStudyLoaded: PropTypes.bool,
    dialog: PropTypes.object,
    extensions: PropTypes.object,
  };

  constructor(props) {
    super(props);

    const { activeServer } = this.props;
    const server = Object.assign({}, activeServer);

    OHIF.measurements.MeasurementApi.setConfiguration({
      dataExchange: {
        retrieve: DICOMSR.retrieveMeasurements,
        store: DICOMSR.storeMeasurements,
      },
      server,
    });

    OHIF.measurements.TimepointApi.setConfiguration({
      dataExchange: {
        retrieve: this.retrieveTimepoints,
        store: this.storeTimepoints,
        remove: this.removeTimepoint,
        update: this.updateTimepoint,
        disassociate: this.disassociateStudy,
      },
    });
  }

  containerRef = React.createRef();

  state = {
    rightSidePanelSize: 0,
    isLeftSidePanelOpen: window.innerWidth > 600,
    isRightSidePanelOpen: false,
    selectedRightSidePanel: 'prediction-panel',
    selectedLeftSidePanel: 'studies', // TODO: Don't hardcode this
    thumbnails: [],
    barExpandInteractiveArea: { left: 5, right: 5 },
  };

  componentWillUnmount() {
    if (this.props.dialog) {
      this.props.dialog.dismissAll();
    }
  }

  retrieveTimepoints = filter => {
    OHIF.log.info('retrieveTimepoints');

    // Get the earliest and latest study date
    let earliestDate = new Date().toISOString();
    let latestDate = new Date().toISOString();
    if (this.props.studies) {
      latestDate = new Date('1000-01-01').toISOString();
      this.props.studies.forEach(study => {
        const StudyDate = moment(study.StudyDate, 'YYYYMMDD').toISOString();
        if (StudyDate < earliestDate) {
          earliestDate = StudyDate;
        }
        if (StudyDate > latestDate) {
          latestDate = StudyDate;
        }
      });
    }

    // Return a generic timepoint
    return Promise.resolve([
      {
        timepointType: 'baseline',
        timepointId: 'TimepointId',
        studyInstanceUIDs: this.props.studyInstanceUIDs,
        PatientID: filter.PatientID,
        earliestDate,
        latestDate,
        isLocked: false,
      },
    ]);
  };

  storeTimepoints = timepointData => {
    OHIF.log.info('storeTimepoints');
    return Promise.resolve();
  };

  updateTimepoint = (timepointData, query) => {
    OHIF.log.info('updateTimepoint');
    return Promise.resolve();
  };

  removeTimepoint = timepointId => {
    OHIF.log.info('removeTimepoint');
    return Promise.resolve();
  };

  disassociateStudy = (timepointIds, StudyInstanceUID) => {
    OHIF.log.info('disassociateStudy');
    return Promise.resolve();
  };

  onTimepointsUpdated = timepoints => {
    if (this.props.onTimepointsUpdated) {
      this.props.onTimepointsUpdated(timepoints);
    }
  };

  onMeasurementsUpdated = measurements => {
    if (this.props.onMeasurementsUpdated) {
      this.props.onMeasurementsUpdated(measurements);
    }
  };

  componentDidMount() {
    const { studies, isStudyLoaded } = this.props;
    const { TimepointApi, MeasurementApi } = OHIF.measurements;
    const currentTimepointId = 'TimepointId';

    const timepointApi = new TimepointApi(currentTimepointId, {
      onTimepointsUpdated: this.onTimepointsUpdated,
    });

    const measurementApi = new MeasurementApi(timepointApi, {
      onMeasurementsUpdated: this.onMeasurementsUpdated,
    });

    this.currentTimepointId = currentTimepointId;
    this.timepointApi = timepointApi;
    this.measurementApi = measurementApi;

    if (studies) {
      const PatientID = get(studies, '[0].PatientID');

      timepointApi.retrieveTimepoints({ PatientID });
      if (isStudyLoaded) {
        this.measurementApi.retrieveMeasurements(PatientID, [
          currentTimepointId,
        ]);
      }
      this.setState({
        thumbnails: _mapStudiesToThumbnails(studies),
      });
    }

    // call AI result and other stuff:
    const studyInstanceUIDs = this.props.studyInstanceUIDs;
    actionGetAIResult({ studyInstanceUIDs });
  }

  componentDidUpdate(prevProps) {
    const { studies, isStudyLoaded } = this.props;
    if (studies !== prevProps.studies) {
      this.setState({
        thumbnails: _mapStudiesToThumbnails(studies),
      });
    }

    if (isStudyLoaded && isStudyLoaded !== prevProps.isStudyLoaded) {
      const PatientID = get(studies, '[0].PatientID');
      const { currentTimepointId } = this;

      this.timepointApi.retrieveTimepoints({ PatientID });
      this.measurementApi.retrieveMeasurements(PatientID, [currentTimepointId]);
    }

    const isOpenRightSidePanel = get(
      this.props.extensions,
      'predictionPanel.rightPanel'
    );
    const selectedTab = get(
      this.props.extensions,
      'predictionPanel.selectedTab'
    );
    if (isOpenRightSidePanel && !this.state.isRightSidePanelOpen) {
      this.setSize(DEFAULT_SIZE);
      this.setState(state => ({
        isRightSidePanelOpen: true,
        selectedTab: selectedTab,
      }));
    }
  }

  setSize = size => {
    const container = this.containerRef.current;
    if (container) {
      const resizer = container.getResizer();
      resizer.resizeSection(1, { toSize: size });
      container.applyResizer(resizer);
    }
    this.setState({ ...this.state, rightSidePanelSize: size });
  };

  closeRightSidePanel = () => {
    this.setSize(0);
    actionSetExtensionData('predictionPanel', {
      rightPanel: false,
    });
    this.setState(state => ({
      isRightSidePanelOpen: false,
    }));
  };
  openRightSidePanel = () => {
    this.setSize(DEFAULT_SIZE);
    this.setState(state => ({
      isRightSidePanelOpen: true,
    }));
  };

  render() {
    let VisiblePanelLeft, VisiblePanelRight;
    const panelExtensions = extensionManager.modules[MODULE_TYPES.PANEL];

    panelExtensions.forEach(panelExt => {
      panelExt.module.components.forEach(comp => {
        if (comp.id === this.state.selectedRightSidePanel) {
          VisiblePanelRight = comp.component;
        } else if (comp.id === this.state.selectedLeftSidePanel) {
          VisiblePanelLeft = comp.component;
        }
      });
    });

    const { t } = this.props;

    return (
      <>
        {/* HEADER */}
        <ConnectedHeader>
          {isMobile(window.innerWidth, window.innerHeight) &&
            this.state.isRightSidePanelOpen && (
              <div
                onClick={() => this.closeRightSidePanel()}
                style={{ display: 'flex', alignItems: 'center' }}
              >
                <LeftOutlined style={{ fontSize: 24 }} />
                <span style={{ fontSize: 14 }}>
                  {this.state.selectedTab == 'doctor-tab'
                    ? t('Doctor Result')
                    : this.state.selectedTab == 'ai-tab'
                    ? t('CAD')
                    : ''}
                </span>
              </div>
            )}
        </ConnectedHeader>

        {/* TOOLBAR */}
        <ToolbarRow
          isLeftSidePanelOpen={this.state.isLeftSidePanelOpen}
          isRightSidePanelOpen={this.state.isRightSidePanelOpen}
          selectedLeftSidePanel={
            this.state.isLeftSidePanelOpen
              ? this.state.selectedLeftSidePanel
              : ''
          }
          selectedRightSidePanel={
            this.state.isRightSidePanelOpen
              ? this.state.selectedRightSidePanel
              : ''
          }
          handleSidePanelChange={(side, selectedTab, selectedPanel) => {
            const sideClicked = side && side[0].toUpperCase() + side.slice(1);
            const openKey = `is${sideClicked}SidePanelOpen`;
            const selectedKey = `selected${sideClicked}SidePanel`;
            const updatedState = Object.assign({}, this.state);

            const isOpen = updatedState[openKey];
            const prevSelectedPanel = updatedState[selectedKey];
            // RoundedButtonGroup returns `null` if selected button is clicked
            const isSameSelectedPanel =
              prevSelectedPanel === selectedPanel || selectedPanel === null;

            updatedState[selectedKey] = selectedPanel || prevSelectedPanel;

            const isClosedOrShouldClose = !isOpen || isSameSelectedPanel;
            if (isClosedOrShouldClose) {
              updatedState[openKey] = !updatedState[openKey];
            }
            this.setSize(DEFAULT_SIZE);
            if (side == 'right' && this.state.isRightSidePanelOpen) {
              const extensions = this.props.extensions;
              if (extensions && extensions.cad) {
                const StudyInstanceUID = get(
                  extensions,
                  'cad.StudyInstanceUID'
                );
                const SOPInstanceUID = get(extensions, 'cad.SOPInstanceUID');
                if (StudyInstanceUID && SOPInstanceUID) {
                  jumpToImage({
                    StudyInstanceUID: StudyInstanceUID,
                    SOPInstanceUID: SOPInstanceUID,
                  });
                }
              }
            }
            this.setState(updatedState);
          }}
          studies={this.props.studies}
        />

        {/*<ConnectedStudyLoadingMonitor studies={this.props.studies} />*/}
        {/*<StudyPrefetcher studies={this.props.studies} />*/}

        {/* VIEWPORTS + SIDEPANELS */}
        <div
          className={`FlexboxLayout viewer-section ${
            this.state.isRightSidePanelOpen ? 'right-side-panel-open' : ''
          }`}
        >
          {/* LEFT */}
          <Container
            ref={this.containerRef}
            style={{
              width: '100vw',
            }}
          >
            <Section
              style={{ display: 'flex' }}
              className={`container viewer-container ${
                this.state.isRightSidePanelOpen ? 'right-side-panel-open' : ''
              }`}
            >
              <SidePanel from="left" isOpen={this.state.isLeftSidePanelOpen}>
                {VisiblePanelLeft ? (
                  <VisiblePanelLeft
                    viewports={this.props.viewports}
                    studies={this.props.studies}
                    activeIndex={this.props.activeViewportIndex}
                  />
                ) : (
                  <>
                    <div
                      className="button-collapse-side left-side"
                      onClick={() => {
                        this.setState(state => ({
                          isLeftSidePanelOpen: !state.isLeftSidePanelOpen,
                        }));
                      }}
                    >
                      <Icon
                        name={
                          this.state.isLeftSidePanelOpen
                            ? 'left-arrow'
                            : 'right-arrow'
                        }
                      />
                    </div>
                    <ConnectedStudyBrowser
                      studies={this.state.thumbnails}
                      studyMetadata={this.props.studies}
                      onRetrySeriesMetadata={this.props.retrySeriesMetadata}
                    />
                  </>
                )}
              </SidePanel>

              {/* MAIN */}
              <div className={classNames('main-content')}>
                <ConnectedViewerMain
                  studies={this.props.studies}
                  isStudyLoaded={this.props.isStudyLoaded}
                />
              </div>
            </Section>
            <Bar
              size={3}
              className="drag-bar"
              style={{ zIndex: 1200 }}
              expandInteractiveArea={this.state.barExpandInteractiveArea}
              onMouseDown={() =>
                this.setState({
                  ...this.state,
                  barExpandInteractiveArea: { left: 200, right: 200 },
                })
              }
              onMouseUp={() =>
                this.setState({
                  ...this.state,
                  barExpandInteractiveArea: { left: 5, right: 5 },
                })
              }
            />
            {/* RIGHT */}
            {keycloak.hasOneOfPerm([
              SCOPES.reportView,
              SCOPES.aiResult,
              NEW_SCOPES.worklist.report.view,
              NEW_SCOPES.worklist.cad.view,
            ]) && (
              <Section
                defaultSize={this.state.rightSidePanelSize}
                onSizeChanged={e =>
                  this.setState({ ...this.state, rightSidePanelSize: e })
                }
                style={{ overflow: 'inherit' }}
              >
                <SidePanel
                  from="right"
                  isOpen={this.state.isRightSidePanelOpen}
                >
                  {VisiblePanelRight && (
                    <>
                      <div
                        className="button-collapse-side right-side"
                        onClick={() => {
                          if (this.state.rightSidePanelSize > 0) {
                            this.closeRightSidePanel();
                          } else {
                            this.openRightSidePanel();
                          }
                        }}
                      >
                        <Icon
                          name={
                            this.state.rightSidePanelSize > 0
                              ? 'right-arrow'
                              : 'left-arrow'
                          }
                        />
                      </div>
                      {this.state.isRightSidePanelOpen && (
                        <VisiblePanelRight
                          isOpen={this.state.isRightSidePanelOpen}
                          viewports={this.props.viewports}
                          studies={this.props.studies}
                          activeIndex={this.props.activeViewportIndex}
                          measurementApi={this.measurementApi}
                        />
                      )}
                    </>
                  )}
                </SidePanel>
              </Section>
            )}
          </Container>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  extensions: state.extensions,
});

export default withTranslation('Vindoc')(
  withDialog(connect(mapStateToProps)(Viewer))
);

/**
 * What types are these? Why do we have "mapping" dropped in here instead of in
 * a mapping layer?
 *
 * TODO[react]:
 * - Add sorting of display sets
 * - Add showStackLoadingProgressBar option
 *
 * @param {Study[]} studies
 * @param {DisplaySet[]} studies[].displaySets
 */
const _mapStudiesToThumbnails = function(studies) {
  return studies.map(study => {
    const { StudyInstanceUID, series = [] } = study;

    const thumbnails = study.displaySets.map(displaySet => {
      const {
        displaySetInstanceUID,
        SeriesDescription,
        SeriesNumber,
        InstanceNumber,
        numImageFrames,
        SeriesInstanceUID,
      } = displaySet;

      const seriesData = series.find(
        ser => ser.SeriesInstanceUID === SeriesInstanceUID
      );
      const { isFetching, success } = seriesData || {};

      let imageId;
      let altImageText;

      if (displaySet.Modality && displaySet.Modality === 'SEG') {
        // TODO: We want to replace this with a thumbnail showing
        // the segmentation map on the image, but this is easier
        // and better than what we have right now.
        altImageText = 'SEG';
      } else if (displaySet.images && displaySet.images.length) {
        const imageIndex = Math.floor(displaySet.images.length / 2);

        imageId = displaySet.images[imageIndex].getImageId();
      } else {
        altImageText = displaySet.Modality ? displaySet.Modality : 'UN';
      }

      return {
        imageId,
        altImageText,
        displaySetInstanceUID,
        SeriesDescription,
        SeriesNumber,
        InstanceNumber,
        numImageFrames,
        SeriesInstanceUID,
        isFetching,
        success,
      };
    });

    return {
      ...study,
      StudyInstanceUID,
      thumbnails,
    };
  });
};
