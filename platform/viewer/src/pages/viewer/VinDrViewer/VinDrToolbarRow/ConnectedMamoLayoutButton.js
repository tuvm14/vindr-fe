import { MamoLayoutButton } from '@tuvm/ui';
import OHIF from '@tuvm/core';
import { connect } from 'react-redux';
import get from 'lodash/get';
import { message } from 'antd';
import { resetExpandLayout } from '../../../../utils/helper';

const {
  setLayout,
  setViewportActive,
  setViewportSpecificData,
} = OHIF.redux.actions;

const mapStateToProps = state => {
  return {
    currentLayout: state.viewports.layout,
    activeViewportIndex: state.viewports.activeViewportIndex,
  };
};

const CC_OR_MLO_ORDER = {
  RCC: 0,
  LCC: 1,
  RMLO: 0,
  LMLO: 1,
};

const CC_AND_MLO_ORDER = {
  RCC: 0,
  LCC: 1,
  RMLO: 2,
  LMLO: 3,
};

const _getMammoViewPosition = (instance = {}) => {
  let position = '';

  if (instance.AcquisitionDeviceProcessingDescription === 'RCC') {
    position = 'RCC';
  } else if (instance.AcquisitionDeviceProcessingDescription === 'LCC') {
    position = 'LCC';
  } else if (instance.AcquisitionDeviceProcessingDescription === 'RMLO') {
    position = 'RMLO';
  } else if (instance.AcquisitionDeviceProcessingDescription === 'LMLO') {
    position = 'LMLO';
  } else if (
    instance.ViewPosition === 'MLO' &&
    instance.ImageLaterality == 'R'
  ) {
    position = 'RMLO';
  } else if (
    instance.ViewPosition === 'MLO' &&
    instance.ImageLaterality == 'L'
  ) {
    position = 'LMLO';
  } else if (
    instance.ViewPosition === 'CC' &&
    instance.ImageLaterality == 'R'
  ) {
    position = 'RCC';
  } else if (
    instance.ViewPosition === 'CC' &&
    instance.ImageLaterality == 'L'
  ) {
    position = 'LCC';
  }

  return {
    ...instance,
    position,
  };
};

const _filterMammoByViewPosition = (mammoView, instances) => {
  if (mammoView) {
    return instances.filter(item => item.ViewPosition == mammoView);
  }
  return instances;
};

const _filterUnique_CCMLO = instances => {
  const result = [];
  instances.forEach(instance => {
    const check = result.filter(item => item.position === instance.position);
    if (check.length == 0) {
      result.push(instance);
    }
  });

  return result;
};

const mapDispatchToProps = dispatch => {
  return {
    // TODO: Change if layout switched becomes more complex
    onChange: (selectedCell, currentLayout, activeViewportIndex) => {
      let viewports = [];
      let numRows = 0;
      let numColumns = 0;
      let numViewports = 0;

      const MAMMO_VIEW = selectedCell.position; // MLO, CC/MLO

      if (MAMMO_VIEW) {
        const store = window.store.getState();
        const currentViewportSpecificData = get(
          store,
          `viewports.viewportSpecificData[${activeViewportIndex}]`
        );

        const currentSeriesInstanceUID = get(
          currentViewportSpecificData,
          'SeriesInstanceUID'
        );

        const StudyInstanceUID = get(
          currentViewportSpecificData,
          'StudyInstanceUID'
        );

        const { studyMetadataManager } = OHIF.utils;
        const study = studyMetadataManager.get(StudyInstanceUID);

        const _all_series = get(study, '_series');
        const series_filter = _all_series.find(
          s => s.seriesInstanceUID == currentSeriesInstanceUID
        );

        const instances = get(series_filter, '_instances', []);
        let modalities = [];
        let viewpositions = [];

        let instances_tags = instances.map(instance => {
          const metadata = get(instance, '_data.metadata');
          modalities.push(get(metadata, 'Modality'));
          viewpositions.push(get(metadata, 'ViewPosition'));
          const current_instance = {
            SOPInstanceUID: get(metadata, 'SOPInstanceUID'),
            SeriesInstanceUID: get(metadata, 'SeriesInstanceUID'),
            StudyInstanceUID: get(metadata, 'StudyInstanceUID'),
            Modality: get(metadata, 'Modality'),
            ViewPosition: get(metadata, 'ViewPosition'),
            ImageLaterality: get(metadata, 'ImageLaterality'),
            AcquisitionDeviceProcessingDescription: get(
              metadata,
              'AcquisitionDeviceProcessingDescription'
            ),
          };

          return _getMammoViewPosition(current_instance);
        });

        modalities = Array.from(new Set(modalities));
        viewpositions = Array.from(new Set(viewpositions));
        if (!modalities.includes('MG')) {
          message.info(
            `Cannot activate hanging protocol. There are not any CC or MLO view position in this series.`
          );
          return;
        }
        let positionMapping = {};

        if (MAMMO_VIEW == 'CC' || MAMMO_VIEW == 'MLO') {
          positionMapping = CC_OR_MLO_ORDER;
          numRows = 1;
          instances_tags = _filterMammoByViewPosition(
            MAMMO_VIEW,
            instances_tags
          );

          numColumns = 2;
          if (instances_tags.length === 1) {
            numRows = 1;
            numColumns = 1;
          }
        } else if (MAMMO_VIEW == 'CC/MLO') {
          if (!viewpositions.includes('CC') || !viewpositions.includes('MLO')) {
            message.info(
              `Cannot activate hanging protocol. There are not enough CC and MLO view position in this series.`
            );
            return;
          }

          positionMapping = CC_AND_MLO_ORDER;
          numRows = 2;
          numColumns = 2;
          instances_tags = _filterUnique_CCMLO(instances_tags);
        }

        if (instances_tags.length == 0) {
          message.info(
            `Cannot activate hanging protocol. There are not any ${MAMMO_VIEW} view position in this series.`
          );
          return;
        }

        numViewports = numRows * numColumns;

        for (let i = 0; i < numViewports; i++) {
          viewports.push({
            plugin: 'cornerstone',
          });
        }

        const layout = {
          numRows,
          numColumns,
          viewports,
        };

        dispatch(setLayout(layout));
        resetExpandLayout();
        const allStudyDisplaySets = get(study, 'displaySets', []);
        const currentSeriesDisplaySet = allStudyDisplaySets.find(
          s => s.SeriesInstanceUID == currentSeriesInstanceUID
        );

        instances_tags.forEach((instance, index) => {
          const newDisplaySet = { ...currentSeriesDisplaySet };

          newDisplaySet.SOPInstanceUID = instance.SOPInstanceUID;
          newDisplaySet.frameIndex = 0;

          setTimeout(() => {
            dispatch(
              setViewportSpecificData(
                positionMapping[instance.position],
                newDisplaySet
              )
            );
          }, 0);
        });
      } else {
        numRows = selectedCell.row + 1;
        numColumns = selectedCell.col + 1;
        numViewports = numRows * numColumns;

        for (let i = 0; i < numViewports; i++) {
          const viewport = currentLayout.viewports[i];
          let plugin = viewport && viewport.plugin;
          if (viewport && viewport.vtk) {
            plugin = 'cornerstone';
          }

          viewports.push({
            plugin,
          });
        }
        const layout = {
          numRows,
          numColumns,
          viewports,
        };

        const maxActiveIndex = numViewports - 1;
        if (activeViewportIndex > maxActiveIndex) {
          dispatch(setViewportActive(0));
        }

        dispatch(setLayout(layout));
        resetExpandLayout();
      }
    },
  };
};

const mergeProps = (propsFromState, propsFromDispatch) => {
  const onChangeFromDispatch = propsFromDispatch.onChange;
  const { currentLayout, activeViewportIndex } = propsFromState;

  return {
    onChange: selectedCell =>
      onChangeFromDispatch(selectedCell, currentLayout, activeViewportIndex),
  };
};

const ConnectedMamoLayoutButton = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(MamoLayoutButton);

export default ConnectedMamoLayoutButton;
