import React, { Component } from 'react';
import { metadata, utils } from '@tuvm/core';

import ConnectedViewer from '../viewer/VinDrViewer';
import PropTypes from 'prop-types';
import { extensionManager } from '../../App.js';
import Dropzone from 'react-dropzone';
import filesToStudies from './filesToStudies';
import './ViewerLocalFileData.css';
import { withTranslation } from 'react-i18next';
import { FullScreenLoading } from '../../components/Customize/FullScreenLoading';
import ConnectedHeader from '../../components/Header/ConnectedHeader';
import { NAV_LIST } from '../../utils/constants';
import useNavigationMenu from '../../components/Header/useNavigationMenu';
import NotFound from '../NotFound';

const { OHIFStudyMetadata } = metadata;
const { studyMetadataManager } = utils;

const dropZoneLinkDialog = (onDrop, i18n, dir) => {
  return (
    <Dropzone onDrop={onDrop} noDrag>
      {({ getRootProps, getInputProps }) => (
        <span
          {...getRootProps()}
          className="link-dialog"
          style={{ textDecoration: 'underline', cursor: 'pointer' }}
        >
          {dir ? (
            <span>
              {i18n('Load folders')}
              <input
                {...getInputProps()}
                webkitdirectory="true"
                mozdirectory="true"
              />
            </span>
          ) : (
            <span>
              {i18n('Load files')}
              <input {...getInputProps()} />
            </span>
          )}
        </span>
      )}
    </Dropzone>
  );
};

const linksDialogMessage = (onDrop, i18n) => {
  return (
    <>
      {i18n('Or click to ')}
      {dropZoneLinkDialog(onDrop, i18n)}
      {i18n(' or ')}
      {dropZoneLinkDialog(onDrop, i18n, true)}
      {i18n(' from dialog')}
    </>
  );
};

class ViewerLocalFileData extends Component {
  static propTypes = {
    studies: PropTypes.array,
  };

  state = {
    studies: null,
    loading: false,
    error: null,
  };

  updateStudies = studies => {
    // Render the viewer when the data is ready
    studyMetadataManager.purge();

    // Map studies to new format, update metadata manager?
    const updatedStudies = studies.map(study => {
      const studyMetadata = new OHIFStudyMetadata(
        study,
        study.StudyInstanceUID
      );
      const sopClassHandlerModules =
        extensionManager.modules['sopClassHandlerModule'];

      study.displaySets =
        study.displaySets ||
        studyMetadata.createDisplaySets(sopClassHandlerModules);

      studyMetadata.forEachDisplaySet(displayset => {
        displayset.localFile = true;
      });

      studyMetadataManager.add(studyMetadata);

      return study;
    });

    this.setState({
      studies: updatedStudies,
    });
  };

  render() {
    const { navigationOptions, user, t } = this.props;
    const onDrop = async acceptedFiles => {
      this.setState({ loading: true });

      // Note: this following code is used to save dicom file to session storage

      // console.log(acceptedFiles);
      // var reader = new FileReader();
      // reader.readAsArrayBuffer(acceptedFiles[0]);
      // reader.onload = function() {
      //   console.log(reader.result);
      // };
      console.log(acceptedFiles);
      dicomFiles = acceptedFiles;
      // const dicomMeta = acceptedFiles.map(it => ({
      //   name: it.name,
      //   type: it.type,
      // }));

      // sessionStorage.setItem('dicomMeta', JSON.stringify(dicomMeta));
      // forEach(acceptedFiles, it => {
      //   const reader = new FileReader();
      //   const dicomName = it.name;
      //   reader.readAsDataURL(it);
      //   reader.onload = function() {
      //     sessionStorage.setItem(dicomName, reader.result);
      //   };
      // });

      // const dicomMetas = JSON.parse(sessionStorage.getItem('dicomMeta'));
      // let files = [];
      // forEach(dicomMetas, it => {
      //   const dataUrl = sessionStorage.getItem(it.name);
      //   const file = dataURLtoBlob(dataUrl);
      //   files.push(file);
      // });

      const studies = await filesToStudies(acceptedFiles);
      const updatedStudies = this.updateStudies(studies);

      if (!updatedStudies) {
        return;
      }

      this.setState({ studies: updatedStudies, loading: false });
    };

    if (this.state.error) {
      // return <div>Error: {JSON.stringify(this.state.error)}</div>;
      return (
        <NotFound
          message={t('Error Message')}
          showGoBackButton={this.state.error}
        />
      );
    }

    return (
      <>
        {!this.state.studies && (
          <ConnectedHeader
            useLargeLogo={true}
            user={user}
            navigationOptions={navigationOptions}
            activeMenu={NAV_LIST.LOCAL}
          />
        )}
        <Dropzone onDrop={onDrop} noClick>
          {({ getRootProps, getInputProps }) => (
            <div {...getRootProps()} style={{ width: '100%', height: '100%' }}>
              {this.state.studies ? (
                <ConnectedViewer
                  studies={this.state.studies}
                  studyInstanceUIDs={
                    this.state.studies &&
                    this.state.studies.map(a => a.StudyInstanceUID)
                  }
                />
              ) : (
                <div className={'drag-drop-instructions'}>
                  <div className={'drag-drop-contents'}>
                    {this.state.loading ? (
                      <FullScreenLoading />
                    ) : (
                      <>
                        <h2>
                          {this.props.t(
                            'Drag and Drop DICOM files here to load them in the Viewer'
                          )}
                        </h2>
                        <h3 style={{ padding: 10 }}>
                          {linksDialogMessage(onDrop, this.props.t)}
                        </h3>
                      </>
                    )}
                  </div>
                </div>
              )}
            </div>
          )}
        </Dropzone>
      </>
    );
  }
}

export let dicomFiles;

export default withNavbarOptions(
  withTranslation('Common')(ViewerLocalFileData)
);

function withNavbarOptions(Component) {
  return function WrappedComponent(props) {
    const navigationOptions = useNavigationMenu();
    return <Component {...props} navigationOptions={navigationOptions} />;
  };
}
