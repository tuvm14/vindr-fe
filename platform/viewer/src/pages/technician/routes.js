import React from 'react';
import { LinkOutlined } from '@ant-design/icons';
import { NEW_SCOPES, SCOPES } from '../../utils/constants';
import Worklist from './worklist';

const TechnicianRoutes = [
  {
    key: 'worklist',
    path: '/worklist',
    component: <Worklist />,
    permissions: [
      SCOPES.worklistAdmin,
      SCOPES.globalAdmin,
      NEW_SCOPES.order.match,
    ],
    menuLabel: 'Matching Worklist',
    icon: <LinkOutlined />,
  },
];

export default TechnicianRoutes;
