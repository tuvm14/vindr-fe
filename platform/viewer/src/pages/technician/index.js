import React from 'react';
import { Layout } from 'antd';
import { useRouteMatch } from 'react-router-dom';
import ConnectedHeader from '../../components/Header/ConnectedHeader';
// import LeftMenu from './leftMenu';
import TechnicianRoutes from './routes';
import keycloak from '../../services/auth';
import NoPermission from '../NoPermission';

import './Technician.styl';
import { NAV_LIST } from '../../utils/constants';
import useNavigationMenu from '../../components/Header/useNavigationMenu';

const { Content } = Layout;

function Technician() {
  const { params } = useRouteMatch();
  const navigationOptions = useNavigationMenu();
  let routes = [];
  TechnicianRoutes.forEach(it => {
    if (it.children) {
      routes = [...routes, ...it.children];
    } else {
      routes = [...routes, it];
    }
  });
  const content = routes.find(it => it.key == params.page);
  const hasPermission = keycloak.hasOneOfPerm(content.permissions);
  if (!hasPermission) {
    return <NoPermission />;
  }

  return (
    <Layout className="technician-container">
      <ConnectedHeader
        activeMenu={NAV_LIST.WORKLIST}
        navigationOptions={navigationOptions}
      />
      <Layout>
        {/* <LeftMenu /> */}
        <Content className="technician-content">{content.component}</Content>
      </Layout>
    </Layout>
  );
}

export default Technician;
