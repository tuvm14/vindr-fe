import React from 'react';

import { Checkbox, Dropdown, Button, Row, Col } from 'antd';
import './CheckboxMenu.styl';
import { withTranslation } from 'react-i18next';

class CheckboxMenu extends React.Component {
  state = {
    icon: {},
    selectedItems: [],
  };

  componentDidMount = () => {
    if (this.props.value && this.props.value.length) {
      this.setState(
        {
          selectedItems: [...this.props.value],
        },
        () => this.checkIconFilled()
      );
    }
  };

  onChange = selection => {
    this.setState({ selectedItems: [...selection] }, () => {
      this.checkIconFilled();
    });

    // return this.props.onChange(selection);
  };

  checkIconFilled = () => {
    if (this.state.selectedItems.length) {
      this.setState({ icon: { theme: 'filled' } });
    } else {
      this.setState({ icon: {} });
    }
  };

  checkboxRender = () => {
    const _this = this;
    const height = this.props.height || 3;
    const { t } = this.props;

    const groups = this.props.options
      .map(function(e, i) {
        return i % height === 0
          ? _this.props.options.slice(i, i + height)
          : null;
      })
      .filter(function(e) {
        return e;
      });

    return (
      <Checkbox.Group
        onChange={this.onChange}
        value={this.state.selectedItems}
        className="checkbox-menu"
      >
        <Row>
          {groups.map((group, i) => {
            return (
              <Col
                key={'checkbox-group-' + i}
                span={Math.floor(24 / groups.length)}
              >
                {group.map((label, i) => {
                  return (
                    <Checkbox
                      key={label}
                      value={label}
                      style={{ display: 'flex', margin: '0' }}
                    >
                      {t(label)}
                    </Checkbox>
                  );
                })}
              </Col>
            );
          })}
        </Row>
        <Row>
          <Col sm={12}>
            <Button
              size="small"
              onClick={() => {
                return this.onChange([]);
              }}
              style={{ marginTop: 10, width: '100%' }}
            >
              {t('Clear')}
            </Button>
          </Col>
          <Col sm={12}>
            <Button
              size="small"
              type="primary"
              onClick={() => {
                return this.props.onChange(this.state.selectedItems);
              }}
              style={{ marginTop: 10, width: '100%' }}
            >
              {t('Apply')}
            </Button>
          </Col>
        </Row>
      </Checkbox.Group>
    );
  };

  render() {
    const CheckboxRender = this.checkboxRender;
    const { t } = this.props;
    return (
      <Dropdown overlay={<CheckboxRender />} trigger={['click']}>
        <Button
          size={this.props.size}
          style={{
            color: 'rgba(255, 255, 255, 0.3)',
            width: '100%',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'flex-end',
          }}
        >
          <span style={{ overflow: 'hidden', textOverflow: 'ellipsis' }}>
            {this.state.selectedItems &&
            this.state.selectedItems.length &&
            !(
              this.state.selectedItems.length == 1 &&
              this.state.selectedItems[0] == '*'
            )
              ? this.state.selectedItems.join(', ')
              : t('Select')}
          </span>
          {this.props.suffixIcon}
        </Button>
      </Dropdown>
    );
  }
}

export default withTranslation('Vindoc')(CheckboxMenu);
