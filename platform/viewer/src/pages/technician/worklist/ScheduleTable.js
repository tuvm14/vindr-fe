import React, {
  useState,
  useEffect,
  useCallback,
  useImperativeHandle,
  forwardRef,
} from 'react';
import { connect } from 'react-redux';
import { Table } from 'antd';
import { Icon, Tooltip } from '@tuvm/ui';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { isEmpty } from 'lodash';
import { actionGetScheduledList } from './WorklistActions';
import { getDateFormatted, toLuceneQueryString } from '../../../utils/helper';
import PaginationTable from '../components/pagination';
import FilterData from './FilterData';
import TicketSubmit from './TicketSubmit';
import keycloak from '../../../services/auth';
import { FEATURES } from '../../../utils/constants';

const INITIAL_FILTERS = {
  startDate: moment()
    .subtract(30, 'days')
    .startOf('days')
    .format('x'),
  endDate: moment()
    .endOf('days')
    .format('x'),
};

const SEARCH_OPTIONS = [
  { text: 'Patient ID', value: 'PatientID' },
  { text: 'Accession #', value: 'AccessionNumber' },
];

const INITIAL_SEARCH_FIELDS = [
  {
    fieldId: 1,
    key: 'PatientID',
    label: 'Patient ID',
    value: '',
    options: SEARCH_OPTIONS,
  },
  {
    fieldId: 2,
    key: 'AccessionNumber',
    label: 'Accession #',
    value: '',
    options: SEARCH_OPTIONS,
  },
];

let params = {
  offset: 0,
  limit: 25,
  sort: '-order_date',
  query_string: `order_date_from=${INITIAL_FILTERS.startDate}&order_date_to=${INITIAL_FILTERS.endDate}`,
};

let intervalFetchList;

export const ScheduleTable = (props, ref) => {
  const { t } = useTranslation(['Vindoc']);
  const [scheduledData, setScheduledData] = useState({});
  const [isFetching, setIsFetching] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const { onSelectRow } = props;

  useEffect(() => {
    handleFetchData(params);
    clearInterval(intervalFetchList);

    intervalFetchList = setInterval(() => {
      handleFetchData(params, true);
    }, 10000);

    return () => {
      params = {
        offset: 0,
        limit: 25,
        sort: '-order_date',
        query_string: `order_date_from=${INITIAL_FILTERS.startDate}&order_date_to=${INITIAL_FILTERS.endDate}`,
      };
      clearInterval(intervalFetchList);
    };
  }, []);

  const handleFetchData = async (params, isSilent = false) => {
    try {
      if (!isSilent) {
        setIsFetching(true);
      }

      const { data } = await actionGetScheduledList({
        ...params,
        offset: params.offset * params.limit,
      });
      const tempPerformedData = {
        count: data?.count || 0,
        data: data?.data || [],
      };
      setScheduledData(tempPerformedData);
      if (!isSilent) {
        setIsFetching(false);
      }
    } catch (error) {
      if (!isSilent) {
        setIsFetching(false);
      }
    }
  };

  useImperativeHandle(ref, () => ({
    refreshData() {
      handleFetchData(params);
    },
  }));

  const scheduledColumns = [
    {
      title: '#',
      width: 40,
      fixed: 'left',
      align: 'center',
      dataIndex: 'PatientID',
      key: 'PatientID',
      render: (text, record, index) => index + 1,
    },
    {
      title: 'HIS',
      width: 42,
      dataIndex: 'matched',
      key: 'matched',
      fixed: 'left',
      align: 'center',
      render: matched => (
        <Tooltip
          title={matched ? t('Matched') : t('Unmatched')}
          placement="bottom"
          overlayClassName="toolbar-tooltip"
        >
          <span
            className="match-status"
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            {matched ? (
              <Icon
                style={{ color: 'rgb(0, 172, 90)' }}
                name="link2"
                width="16px"
                height="16px"
              />
            ) : (
              <Icon name="linkOff" width="16px" height="16px" />
            )}
          </span>
        </Tooltip>
      ),
    },
    {
      title: t('Patient Name'),
      width: 173,
      dataIndex: 'patient_name',
      key: 'patient_name',
      ellipsis: true,
      sorter: true,
      render: PatientName => (
        <span>
          {isEmpty(PatientName) ? '-' : `${PatientName}`.replace(/\^/gi, ' ')}
        </span>
      ),
    },
    {
      title: t('Status'),
      width: 100,
      dataIndex: 'status',
      key: 'status',
      ellipsis: true,
      sorter: true,
      render: status => t(status),
    },
    {
      title: t('Patient ID'),
      width: 110,
      dataIndex: 'patient_id',
      key: 'patient_id',
      ellipsis: true,
      sorter: true,
    },
    {
      title: t('Accession #'),
      dataIndex: 'accession_number',
      key: 'accession_number',
      width: 110,
      ellipsis: true,
      sorter: true,
    },
    {
      title: t('Ticket #'),
      dataIndex: 'ticket_number',
      key: 'ticket_number',
      width: 80,
      ellipsis: true,
      sorter: true,
    },
    {
      title: t('Sex'),
      dataIndex: 'patient_sex',
      key: 'patient_sex',
      width: 72,
      sorter: true,
      render: patient_sex => (
        <span>
          {isEmpty(patient_sex) ? '-' : t(`PatientSex_${patient_sex}`)}
        </span>
      ),
    },
    {
      title: t('Age'),
      dataIndex: 'patient_age',
      key: 'patient_age',
      width: 52,
      sorter: true,
      align: 'center',
    },
    {
      title: t('Procedure Code'),
      dataIndex: 'procedure_code',
      key: 'procedure_code',
      width: 80,
      ellipsis: true,
    },
    {
      title: t('Procedure Name'),
      dataIndex: 'procedure_name',
      key: 'procedure_name',
      width: 90,
      ellipsis: true,
    },
    {
      title: t('Modality'),
      dataIndex: 'modality',
      key: 'modality',
      width: 84,
      sorter: true,
      align: 'center',
    },
    {
      title: t('Scheduled Date'),
      dataIndex: 'order_date',
      key: 'order_date',
      sorter: true,
      width: 140,
      render: order_date => (
        <span>
          {order_date &&
            moment(order_date).format(`${getDateFormatted()} HH:mm:ss`)}
        </span>
      ),
    },
    {
      title: t('Referring Physician'),
      dataIndex: 'referring_physician_name',
      key: 'referring_physician_name',
      width: 140,
      ellipsis: true,
    },
    {
      title: t('Exam Description'),
      dataIndex: 'exam_description',
      key: 'exam_description',
      width: 150,
      ellipsis: true,
    },
  ];

  const resetSelectedItems = () => {
    setSelectedRowKeys([]);
    onSelectRow(null);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: (rowKeys, selectedRows) => {
      setSelectedRowKeys(rowKeys);
      if ((selectedRows || []).length === 1) {
        onSelectRow(selectedRows[0]);
      } else {
        onSelectRow(null);
      }
    },
  };

  const handleSearch = useCallback((values = {}) => {
    let transformQuery = {};

    // TODO
    if (values?.startDate && values?.endDate) {
      transformQuery.order_date_from = values?.startDate;
      transformQuery.order_date_to = values?.endDate;
    }
    if (values?.ModalitiesInStudy) {
      transformQuery.modality = values?.ModalitiesInStudy;
    }

    if (values?.matched && values?.matched !== '*') {
      transformQuery.matched = values?.matched;
    }
    if (values?.PatientID) {
      transformQuery.patient_id = values?.PatientID;
    }
    if (values?.AccessionNumber) {
      transformQuery.accession_number = values?.AccessionNumber;
    }

    let queryStr = toLuceneQueryString(transformQuery);
    params = { ...params, query_string: queryStr };
    resetSelectedItems();
    handleFetchData(params);
  }, []);

  const handleOnChangeTable = useCallback((_, __, sorter) => {
    let sort;
    if (sorter?.order === 'ascend') {
      sort = `${sorter.field}`;
    } else if (sorter?.order === 'descend') {
      sort = `-${sorter.field}`;
    }
    params = { ...params, sort };
    resetSelectedItems();
    handleFetchData(params);
  }, []);

  return (
    <div className="schedule-table-content">
      <div className="table-header">
        <div className="tb-actions">
          <div className="tb-title">{t('Scheduled Studies')}</div>
        </div>
        <div className="search-container">
          <FilterData
            initFilter={INITIAL_FILTERS}
            initSearchFields={INITIAL_SEARCH_FIELDS}
            formatDate="x"
            onSearch={handleSearch}
          />
          {keycloak.hasFeature(FEATURES.ris) && (
            <TicketSubmit onSubmitSuccess={() => handleFetchData(params)} />
          )}
        </div>
      </div>
      <div className="table-content">
        <Table
          size="small"
          rowSelection={{ ...rowSelection }}
          columns={scheduledColumns}
          dataSource={scheduledData?.data || []}
          scroll={{ y: 'calc(100% - 31px)' }}
          rowKey={record => record._id}
          pagination={false}
          loading={isFetching}
          onChange={handleOnChangeTable}
        />
        <PaginationTable
          page={params.offset}
          size={params.limit}
          onChange={(page, pageSize) => {
            params = { ...params, offset: page - 1, limit: pageSize };
            handleFetchData(params);
          }}
          totalElements={scheduledData?.count || 0}
        />
      </div>
    </div>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(forwardRef(ScheduleTable));
