import { Button, Input, message, Row } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { actionSubmitTicket } from './WorklistActions';

const TicketSubmit = props => {
  const { t } = useTranslation('Vindoc');
  const [ticket, setTicket] = useState('');
  const handleSubmit = async () => {
    try {
      const res = await actionSubmitTicket({ ticket_number: ticket });
      if (res && res.status === 200) {
        if (res && res.data && res.data.message) {
          message.success(t(res.data.message));
        } else {
          message.success(t('Update success'));
        }
        if (props && props.onSubmitSuccess) {
          props.onSubmitSuccess();
        }
      } else {
        if (res && res.data && res.data.message) {
          message.error(t(res.data.message));
        } else {
          message.error(t('Update failed'));
        }
      }
    } catch (err) {
      if (
        err &&
        err.response &&
        err.response.data &&
        err.response.data.message
      ) {
        message.error(t(err.response.data.message));
      } else {
        message.error(t('Update failed'));
      }
    }
  };

  return (
    <div className="ticket-submit filter-items">
      <Row>
        <div className="filter-item">
          <Row>
            <div style={{ marginRight: 8 }}>
              <Input
                size="small"
                value={ticket}
                placeholder={t('Enter Ticket')}
                onChange={e => setTicket(e?.target?.value)}
                onPressEnter={() => handleSubmit()}
              />
            </div>
          </Row>
        </div>
        <div className="filter-item">
          <Button
            className="filter-btn"
            size="small"
            type="primary"
            onClick={() => handleSubmit()}
          >
            {t('Submit Ticket')}
          </Button>
        </div>
      </Row>
    </div>
  );
};

export default TicketSubmit;
