import React, { useEffect, useState } from 'react';
import { Dropdown, Menu, DatePicker, Button } from 'antd';
import { getDateFormatted } from '../../../../utils/helper';
import moment from 'moment';
import { CaretDownOutlined, SwapRightOutlined } from '@ant-design/icons';
import './CustomDateRange.styl';
import { BASE_TIME } from '../../../../utils/constants';
import { useTranslation } from 'react-i18next';

const { RangePicker } = DatePicker;

const CustomDateRange = ({
  value,
  formatDate = 'DD/MM/YYYY',
  onChange,
  suffixIcon,
  ...rest
}) => {
  const [isCustom, setIsCustom] = useState(false);
  const [displayValue, setDisplayValue] = useState('');
  const { t } = useTranslation('Vindoc');

  const presetRanges = {
    Today: [moment(), moment()],
    'Last 30 days': [moment().subtract(30, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment()],
    'All time': [moment(BASE_TIME, 'YYYYMMDD'), moment()],
  };

  const getDisplayValue = (value = []) => {
    // if (v)
    const [start, end] = value;
    if (!start || !end) return '';
    const keys = Object.keys(presetRanges);
    for (let i = 0; i < keys.length; i++) {
      const [checkStart, checkEnd] = presetRanges[keys[i]];
      if (
        start.format(formatDate) == checkStart.format(formatDate) &&
        end.format(formatDate) == checkEnd.format(formatDate)
      ) {
        return keys[i];
      }
    }
    if (
      end >= presetRanges['All time'][1] &&
      start <= presetRanges['All time'][0]
    ) {
      return 'All time';
    }
    return (
      <>
        {start.format(formatDate)} <SwapRightOutlined />{' '}
        {end.format(formatDate)}
      </>
    );
  };

  useEffect(() => {
    const displayValue = getDisplayValue(value);
    setDisplayValue(displayValue);
  }, [value]);

  const menu = (
    <Menu>
      {Object.entries(presetRanges).map(([key, value]) => (
        <Menu.Item key={key} onClick={() => onChange(value)}>
          {t(key)}
        </Menu.Item>
      ))}
      {window.innerWidth > 600 && (
        <Menu.Item onClick={() => setIsCustom(true)}>{t('Custom')}</Menu.Item>
      )}
    </Menu>
  );

  return (
    <>
      {!isCustom && (
        <Dropdown
          overlay={menu}
          placement="bottomLeft"
          trigger={['click']}
          overlayClassName="vindr-dropdown dropdown-options-dark"
        >
          <Button
            size="small"
            style={{
              width: 200,
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'flex-end',
            }}
          >
            {t(displayValue)}
            {suffixIcon || (
              <CaretDownOutlined
                style={{ color: 'rgba(255, 255, 255, 0.3)' }}
              />
            )}
          </Button>
        </Dropdown>
      )}
      {isCustom && (
        <RangePicker
          open={isCustom}
          className="studylist-datepicker"
          size="small"
          allowClear={false}
          style={{ width: '100%' }}
          format={getDateFormatted()}
          dropdownClassName="date-picker-light"
          onChange={values => {
            onChange(values);
            setIsCustom(false);
          }}
          onOpenChange={flag => setIsCustom(flag)}
          {...rest}
        />
      )}
    </>
  );
};

export default CustomDateRange;
