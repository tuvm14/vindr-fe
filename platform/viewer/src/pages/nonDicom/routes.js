import React from 'react';
import { LinkOutlined } from '@ant-design/icons';
import { NEW_SCOPES, SCOPES } from '../../utils/constants';
import Worklist from './worklist';
// import CreateStudy from './createStudy';

const NonDicomRoutes = [
  {
    key: 'worklist',
    path: '/worklist',
    component: <Worklist />,
    permissions: [
      SCOPES.worklistAdmin,
      SCOPES.globalAdmin,
      NEW_SCOPES.nondicom.create,
    ],
    menuLabel: 'Ordering Worklist',
    icon: <LinkOutlined />,
  },
  // {
  //   key: 'createstudy',
  //   path: '/createstudy',
  //   component: <CreateStudy />,
  //   permissions: [SCOPES.worklistAdmin, SCOPES.globalAdmin],
  //   menuLabel: 'Create Study',
  //   icon: <LinkOutlined />,
  // },
];

export default NonDicomRoutes;
