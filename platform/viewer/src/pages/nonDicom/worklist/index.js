import React, { useState, useRef } from 'react';
import { ConfigProvider, Empty } from 'antd';
import Icon from '@ant-design/icons';
import MatchConfirmation from './MatchConfirmation';
import ScheduleTable from './ScheduleTable';
import EmptyIcon from '../../../../public/assets/empty-img-simple.svg';
import './Worklist.styl';
import { useTranslation } from 'react-i18next';

export const Worklist = () => {
  const performRef = useRef();
  const scheduleRef = useRef();
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [performedData, setPerformedData] = useState(null);
  const [scheduledData, setScheduledData] = useState(null);
  const { t } = useTranslation('Vindoc');

  const handleCloseModal = isRefresh => {
    if (isRefresh) {
      performRef.current.refreshData();
      scheduleRef.current.refreshData();
    }
    setConfirmOpen(false);
  };

  return (
    <div className="worklist-page">
      <ConfigProvider
        renderEmpty={() => (
          <Empty
            image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
            description={t('No Data')}
          />
        )}
      >
        <ScheduleTable
          ref={scheduleRef}
          onSelectRow={data => {
            setScheduledData(data);
          }}
        />
      </ConfigProvider>
      {confirmOpen && (
        <MatchConfirmation
          visible={confirmOpen}
          onCancel={isRefresh => handleCloseModal(isRefresh)}
          scheduledData={scheduledData}
          performedData={performedData}
        />
      )}
    </div>
  );
};

export default Worklist;
