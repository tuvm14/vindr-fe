import React, { useEffect, useState, useMemo } from 'react';
import { Modal, Table, Tag, message, Spin } from 'antd';
import PropTypes from 'prop-types';
import { ExclamationCircleFilled } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import './MatchConfirmation.styl';
import { actionMatchStudies } from './WorklistActions';
import { getDateFormatted } from '../../../utils/helper';

const MatchConfirmation = ({
  visible,
  performedData = {},
  scheduledData = {},
  onCancel,
}) => {
  const { t } = useTranslation(['Vindoc']);
  const [tableData, setTableData] = useState([]);
  const [processing, setProcessing] = useState(false);

  const columns = [
    { title: '', dataIndex: 'index', key: 'index', width: 186 },
    {
      title: 'SCHEDULED STUDY INFO',
      dataIndex: 'scheduledInfo',
      key: 'scheduledInfo',
      ellipsis: true,
      className: 'highlight-cell',
    },
    {
      title: 'PERFORMED STUDY INFO',
      dataIndex: 'performedInfo',
      key: 'performedInfo',
      ellipsis: true,
      className: 'highlight-cell',
    },
  ];

  const rows = useMemo(() => {
    return [
      {
        title: 'Study Instance UID',
        dataIndex: 'StudyInstanceUID',
        key: 'StudyInstanceUID',
      },
      {
        title: 'PID',
        dataIndex: 'PatientID',
        key: 'PatientID',
      },
      {
        title: 'Patient Name',
        dataIndex: 'PatientName',
        key: 'PatientName',
      },
      {
        title: 'Patient Sex / Age',
        dataIndex: 'PatientSexAge',
        key: 'PatientSexAge',
      },
      {
        title: 'Accession #',
        dataIndex: 'AccessionNumber',
        key: 'AccessionNumber',
      },
      {
        title: 'Modality',
        dataIndex: 'Modality',
        key: 'Modality',
      },
      {
        title: 'Body part',
        dataIndex: 'BodyPartExamined',
        key: 'BodyPartExamined',
      },
      {
        title: 'Referring Doctor',
        dataIndex: 'ReferringPhysicianName',
        key: 'ReferringPhysicianName',
      },
      {
        title: 'Exam Description',
        dataIndex: 'description',
        key: 'description',
      },
      {
        title: 'Study Date',
        dataIndex: 'StudyDate',
        key: 'StudyDate',
      },
    ];
  }, []);

  useEffect(() => {
    const data = [];
    scheduledData = {};
    rows.forEach(it => {
      switch (it.key) {
        case 'StudyInstanceUID':
          data.push({
            index: it.title,
            key: it.key,
            scheduledInfo: scheduledData['study_instance_uid'],
            performedInfo: performedData[it.dataIndex],
          });
          break;
        case 'PatientID':
          data.push({
            index: it.title,
            key: it.key,
            scheduledInfo: scheduledData['patient_id'],
            performedInfo: performedData[it.dataIndex],
          });
          break;
        case 'PatientName':
          data.push({
            index: it.title,
            key: it.key,
            scheduledInfo: scheduledData['patient_name'],
            performedInfo: performedData[it.dataIndex],
          });
          break;
        case 'PatientSexAge':
          data.push({
            index: it.title,
            key: it.key,
            scheduledInfo: (
              <>
                {scheduledData['patient_sex'] && (
                  <Tag>{t(`PatientSex_${scheduledData['patient_sex']}`)}</Tag>
                )}
                {scheduledData['patient_age'] && (
                  <Tag>{scheduledData['patient_age']}</Tag>
                )}
              </>
            ),
            performedInfo: (
              <>
                {performedData['PatientSex'] && (
                  <Tag>{t(`PatientSex_${performedData['PatientSex']}`)}</Tag>
                )}
                {performedData['PatientAge'] && (
                  <Tag>{performedData['PatientAge']}</Tag>
                )}
              </>
            ),
          });
          break;

        case 'AccessionNumber':
          data.push({
            index: it.title,
            key: it.key,
            scheduledInfo: scheduledData['accession_number'],
            performedInfo: performedData[it.dataIndex],
          });
          break;
        case 'Modality':
          data.push({
            index: it.title,
            key: it.key,
            scheduledInfo: scheduledData['modality'],
            performedInfo: performedData[it.dataIndex],
          });
          break;
        case 'ReferringPhysicianName':
          data.push({
            index: it.title,
            key: it.key,
            scheduledInfo: scheduledData['referring_physician_name'],
            performedInfo: (performedData[it.dataIndex] || []).join(', '),
          });
          break;
        case 'StudyDate':
          data.push({
            index: it.title,
            key: it.key,
            scheduledInfo:
              scheduledData['order_date'] &&
              moment(scheduledData['order_date']).format(
                `${getDateFormatted()} HH:mm:ss`
              ),
            performedInfo: performedData[it.dataIndex],
          });
          break;
        default:
          data.push({
            index: it.title,
            key: it.key,
            scheduledInfo: (scheduledData || {})[it.dataIndex],
            performedInfo: (performedData || {})[it.dataIndex],
          });
      }
    });
    setTableData(data);
  }, [scheduledData, performedData, rows, t]);

  const handleOk = async () => {
    if (processing) return;
    try {
      setProcessing(true);
      const data = {
        study_instance_uid: performedData?.StudyInstanceUID,
        accession_number: scheduledData?.accession_number,
      };
      await actionMatchStudies(data);
      setProcessing(false);
      onCancel(true);
    } catch (error) {
      message.error(
        <span style={{ color: 'var(--text-color)' }}>System error!</span>
      );
      setProcessing(false);
    }
  };

  return (
    <Modal
      title="Match Confirmation"
      centered
      visible={visible}
      onOk={handleOk}
      onCancel={() => onCancel()}
      width={800}
      className="match-confirm-modal vindr-modal"
      okText="Confirm"
      cancelText="Cancel"
      okType="primary"
      cancelButtonProps={{ type: 'ghost' }}
    >
      <Spin spinning={processing}>
        <div className="warning">
          <ExclamationCircleFilled className="warning-icon" />
          Scheduled and Performed study information are different. Are you sure
          you want to proceed anyway?
        </div>
        <Table
          columns={columns}
          dataSource={tableData}
          pagination={false}
          rowClassName={record => {
            return record?.performedInfo != record?.scheduledInfo
              ? 'highlight-row'
              : '';
          }}
        />
      </Spin>
    </Modal>
  );
};

MatchConfirmation.propTypes = {
  visible: PropTypes.bool,
  setVisible: PropTypes.func,
};

export default MatchConfirmation;
