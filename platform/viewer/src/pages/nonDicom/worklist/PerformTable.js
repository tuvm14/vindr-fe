import React, {
  useState,
  useEffect,
  useCallback,
  useImperativeHandle,
  forwardRef,
} from 'react';
import { connect } from 'react-redux';
import { Table, Button, Space, message } from 'antd';
import { Tooltip } from '@tuvm/ui';
import { LinkOutlined } from '@ant-design/icons';
import { Icon } from '@tuvm/ui/src/elements/Icon';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { isEmpty, toString } from 'lodash';
import { getDateFormatted, toLuceneQueryString } from '../../../utils/helper';
import AppContext from '../../../context/AppContext';
import {
  actionGetPerformedList,
  actionUnMatchStudies,
} from './WorklistActions';
import PaginationTable from '../components/pagination';
import FilterData from './FilterData';

const INITIAL_FILTERS = {
  startDate: moment()
    .subtract(30, 'days')
    .format('YYYYMMDD'),
  endDate: moment().format('YYYYMMDD'),
};

const SEARCH_OPTIONS = [
  { text: 'Patient Name', value: 'PatientName' },
  { text: 'Patient ID', value: 'PatientID' },
  { text: 'Accession #', value: 'AccessionNumber' },
  { text: 'Body Part', value: 'BodyPartExamined' },
];

const INITIAL_SEARCH_FIELDS = [
  {
    fieldId: 1,
    key: 'PatientID',
    label: 'Patient ID',
    value: '',
    options: SEARCH_OPTIONS,
  },
  {
    fieldId: 2,
    key: 'AccessionNumber',
    label: 'Accession #',
    value: '',
    options: SEARCH_OPTIONS,
  },
  {
    fieldId: 3,
    key: 'BodyPartExamined',
    label: 'Body Part',
    value: '',
    options: SEARCH_OPTIONS,
  },
];

let params = {
  offset: 0,
  limit: 25,
  sort: '-StudyDate',
  query_string: `StudyDate:[${INITIAL_FILTERS.startDate} TO ${INITIAL_FILTERS.endDate}]`,
};

let intervalFetchList;

const PerformTable = (props, ref) => {
  const { t } = useTranslation(['Vindoc']);
  const { appConfig = {} } = React.useContext(AppContext);
  const { onClickMatch, onSelectRow, selectedSchedule, scheduleRef } = props;

  const [performedData, setPerformedData] = useState({});
  const [isFetching, setIsFetching] = useState(false);
  const [isProcessing, setProcessing] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  useEffect(() => {
    handleFetchData(params);
    clearInterval(intervalFetchList);

    intervalFetchList = setInterval(() => {
      handleFetchData(params, true);
    }, 10000);

    return () => {
      params = {
        offset: 0,
        limit: 25,
        sort: '-StudyDate',
        query_string: `StudyDate:[${INITIAL_FILTERS.startDate} TO ${INITIAL_FILTERS.endDate}]`,
      };
      clearInterval(intervalFetchList);
    };
  }, []);

  const handleFetchData = async (params, isSilent = false) => {
    try {
      if (!isSilent) {
        setIsFetching(true);
      }
      const { data } = await actionGetPerformedList({
        ...params,
        offset: params.offset * params.limit,
      });
      const tempPerformedData = {
        count: data?.count || 0,
        data: data?.records || [],
      };
      setPerformedData(tempPerformedData);
      if (!isSilent) {
        setIsFetching(false);
      }
    } catch (error) {
      console.log(error);
      if (!isSilent) {
        setIsFetching(false);
      }
    }
  };

  useImperativeHandle(ref, () => ({
    refreshData() {
      handleFetchData(params);
    },
  }));

  const handleDoubleClickRow = record => {
    if (!record?.StudyInstanceUID) return;
    const urlViewer =
      appConfig.routerBasename + `viewer/${record.StudyInstanceUID}`;
    window.open(urlViewer, 'vindrViewerWindow');
  };

  const performedColumns = [
    {
      title: '#',
      width: 40,
      fixed: 'left',
      align: 'center',
      dataIndex: 'PatientID',
      key: 'PatientID',
      render: (text, record, index) => index + 1,
    },
    {
      title: 'HIS',
      width: 42,
      dataIndex: 'Matched',
      key: 'Matched',
      fixed: 'left',
      align: 'center',
      render: matched => (
        <Tooltip
          title={toString(matched) === 'true' ? t('Matched') : t('Unmatched')}
          placement="bottom"
          overlayClassName="toolbar-tooltip"
        >
          <span
            className="match-status"
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            {toString(matched) === 'true' ? (
              <Icon
                style={{ color: 'rgb(0, 172, 90)' }}
                name="link2"
                width="16px"
                height="16px"
              />
            ) : (
              <Icon name="linkOff" width="16px" height="16px" />
            )}
          </span>
        </Tooltip>
      ),
    },
    {
      title: t('Patient Name'),
      width: 173,
      dataIndex: 'PatientName',
      key: 'PatientName',
      ellipsis: true,
      sorter: true,
      render: PatientName => (
        <span>
          {isEmpty(PatientName) ? '-' : `${PatientName}`.replace(/\^/gi, ' ')}
        </span>
      ),
    },
    {
      title: t('Status'),
      width: 93,
      dataIndex: 'VindocStatus',
      key: 'VindocStatus',
      ellipsis: true,
      sorter: true,
    },
    {
      title: t('Patient ID'),
      width: 110,
      dataIndex: 'PatientID',
      key: 'PatientID',
      ellipsis: true,
      sorter: true,
    },
    {
      title: t('Accession #'),
      dataIndex: 'AccessionNumber',
      key: 'AccessionNumber',
      width: 140,
      ellipsis: true,
      sorter: true,
    },
    {
      title: t('Sex'),
      dataIndex: 'PatientSex',
      key: 'PatientSex',
      width: 72,
      sorter: true,
      render: patient_sex => (
        <span>
          {isEmpty(patient_sex) ? '-' : t(`PatientSex_${patient_sex}`)}
        </span>
      ),
    },
    {
      title: t('Age'),
      dataIndex: 'PatientAge',
      key: 'PatientAge',
      width: 52,
      align: 'center',
      sorter: true,
    },
    {
      title: '#S/#I',
      dataIndex: 'seriesInstances',
      key: 'seriesInstances',
      width: 80,
      render: (_, record) => (
        <span>{`${record?.NumberOfStudyRelatedSeries ||
          ''}/${record?.NumberOfStudyRelatedInstances || ''}`}</span>
      ),
    },
    {
      title: t('Body Part'),
      dataIndex: 'BodyPartExamined',
      key: 'BodyPartExamined',
      width: 90,
      sorter: true,
      ellipsis: true,
    },
    {
      title: t('Modality'),
      dataIndex: 'Modality',
      key: 'Modality',
      width: 84,
      sorter: true,
    },
    {
      title: t('Study Date'),
      dataIndex: 'StudyDate',
      key: 'StudyDate',
      width: 140,
      sorter: true,
      render: (StudyDate, { StudyTime = '' }) => {
        return (
          <span>
            {StudyDate &&
              moment(StudyDate + StudyTime, 'YYYYMMDDHHmmss').format(
                `${getDateFormatted()} HH:mm:ss`
              )}
          </span>
        );
      },
    },
    {
      title: t('Referring Physician'),
      dataIndex: 'ReferringPhysicianName',
      key: 'ReferringPhysicianName',
      width: 140,
      ellipsis: true,
    },
    {
      title: t('Exam Description'),
      dataIndex: 'StudyDescription',
      key: 'StudyDescription',
      width: 150,
      ellipsis: true,
    },
  ];

  const resetSelectedItems = () => {
    setSelectedRowKeys([]);
    setSelectedItems([]);
    onSelectRow(null);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: (rowKeys, selectedRows) => {
      setSelectedRowKeys(rowKeys);
      setSelectedItems(selectedRows || []);
      if ((selectedRows || []).length === 1) {
        onSelectRow(selectedRows[0]);
      } else {
        onSelectRow(null);
      }
    },
  };

  const handleSearch = useCallback(values => {
    let transformQuery = { ...values };
    delete transformQuery.startDate;
    delete transformQuery.endDate;
    delete transformQuery.matched;

    // TODO
    if (values?.startDate && values?.endDate) {
      transformQuery.StudyDate = `[${values?.startDate} TO ${values?.endDate}]`;
    }
    if (values?.matched && values?.matched !== '*') {
      transformQuery.Matched = values?.matched;
    }

    let queryStr = toLuceneQueryString(transformQuery, ' AND ', ':');
    params = { ...params, query_string: queryStr };
    resetSelectedItems();
    handleFetchData(params);
  }, []);

  const handleOnChangeTable = useCallback((_, __, sorter) => {
    let sort;
    if (sorter?.order === 'ascend') {
      sort = `${sorter.field}`;
    } else if (sorter?.order === 'descend') {
      sort = `-${sorter.field}`;
    }
    params = { ...params, sort };
    resetSelectedItems();
    handleFetchData(params);
  }, []);

  const disableBtnMatch = () => {
    return !!(
      isEmpty(selectedItems) ||
      selectedItems.length > 1 ||
      toString(selectedItems[0].Matched) === 'true' ||
      isEmpty(selectedSchedule) ||
      selectedSchedule?.matched
    );
  };

  const handleUnmatchStudies = async () => {
    if (isProcessing) return;
    try {
      setProcessing(true);
      const data = { study_instance_uid: selectedItems[0].StudyInstanceUID };
      await actionUnMatchStudies(data);
      setProcessing(false);
      handleFetchData(params);
      scheduleRef?.current?.refreshData();
    } catch (error) {
      setProcessing(false);
      message.error(
        <span style={{ color: 'var(--text-color)' }}>System error!</span>
      );
    }
  };

  return (
    <div className="perform-table-content">
      <div className="table-header">
        <div className="tb-actions">
          <div className="tb-title">{t('Performed Studies')}</div>
          <div className="btn-actions">
            <Space size="middle">
              <Button
                size="small"
                type="primary"
                icon={<LinkOutlined />}
                onClick={onClickMatch}
                disabled={disableBtnMatch()}
              >
                Match
              </Button>
              <Button
                size="small"
                type="primary"
                onClick={handleUnmatchStudies}
                loading={isProcessing}
                disabled={
                  !!(
                    isEmpty(selectedItems) ||
                    selectedItems.length > 1 ||
                    toString(selectedItems[0].Matched) !== 'true'
                  )
                }
              >
                Un-match
              </Button>
            </Space>
          </div>
        </div>
        <FilterData
          initFilter={INITIAL_FILTERS}
          initSearchFields={INITIAL_SEARCH_FIELDS}
          onSearch={handleSearch}
        />
      </div>

      <div className="table-content">
        <Table
          size="small"
          rowSelection={{ ...rowSelection }}
          columns={performedColumns}
          dataSource={performedData?.data || []}
          scroll={{ y: 'calc(100% - 31px)' }}
          rowKey={record => record.StudyInstanceUID}
          pagination={false}
          loading={isFetching}
          onChange={handleOnChangeTable}
          onRow={record => {
            return {
              onDoubleClick: event => {
                event.stopPropagation();
                event.preventDefault();
                if (event?._targetInst?.elementType === 'input') {
                  // handle action doubleClick to checkbox
                  return;
                }
                handleDoubleClickRow(record);
              },
            };
          }}
        />
        <PaginationTable
          page={params.offset}
          size={params.limit}
          onChange={(page, pageSize) => {
            params = { ...params, offset: page - 1, limit: pageSize };
            handleFetchData(params);
          }}
          totalElements={performedData?.count || 0}
        />
      </div>
    </div>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(forwardRef(PerformTable));
