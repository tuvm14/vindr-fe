import api from '../../../services/api';

export const actionGetPerformedList = (params = { limit: 25, offset: 0 }) => {
  return api({
    url: '/studylist/v2/search',
    method: 'GET',
    params,
  });
};

export const actionGetScheduledList = (params = { limit: 25, offset: 0 }) => {
  return api({
    url: '/ris/order',
    method: 'GET',
    params,
  });
};

export const actionMatchStudies = data => {
  return api({
    url: '/ris/match',
    method: 'POST',
    data,
  });
};

export const actionUnMatchStudies = data => {
  return api({
    url: '/ris/unmatch',
    method: 'POST',
    data,
  });
};
