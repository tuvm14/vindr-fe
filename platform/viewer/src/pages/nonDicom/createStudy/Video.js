import { PlayCircleOutlined } from '@ant-design/icons';
import React, { useState } from 'react';

const Video = ({ src, width, height }) => {
  const [isPreview, setIsPreview] = useState(false);
  const videoRef = React.useRef(null);

  return (
    <div className='video' style={{ position: 'relative' }}>

      <video
        // id="video"
        ref={videoRef}
        width="100%"
        height="100%"
        // controls
        src={src}
      />
      {videoRef && videoRef.current && videoRef.current.duration != Infinity && (
        <div
          className="duration"
          style={{
            position: 'absolute',
            right: 2,
            bottom: 2,
          }}
        >
          {`${videoRef.current.duration}s`}
        </div>
      )}
      <PlayCircleOutlined
        style={{
          fontSize: 24,
          position: 'absolute',
          left: '50%',
          top: '50%',
          cursor: 'pointer',
          transform: 'translate(-50%, -50%)',
        }}
        onClick={e => {
          e.preventDefault();
          e.stopPropagation();
          setIsPreview(true);
        }}
      />
      {isPreview && (
        <div
          className="preview-overlay"
          style={{
            width: '100vw',
            height: '100vh',
            position: 'fixed',
            left: 0,
            top: 0,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 1004,
            backgroundColor: 'rgba(0, 0, 0, 0.45)',
          }}
          onClick={() => setIsPreview(false)}
        >
          <video
            // id="video"
            style={{ maxWidth: '100%', maxHeight: '100%', height: 'auto' }}
            width="80%"
            height="auto"
            controls
            src={src}
            onClick={e => {
              e.preventDefault();
              e.stopPropagation();
              setIsPreview(true);
            }}
          />
        </div>
      )}
    </div>
  );
};

export default React.memo(Video);
