import { clone } from 'lodash';
import React, { useState } from 'react';
import DoctorTab from '../components/DoctorTab';
import {
  dataURLtoFile,
  dicomTagsAdapter,
  fileToDataURL,
  uuidv4,
} from '../../../utils/helper';
import CreateStudyContext from './CreateStudyContext';
import ImageList from './ImageList';
import StudyInfomation from './StudyInfomation';
import WebcamCapture from './WebcamCapture';
import './CreateStudy.styl';
import { actionUploadNonDICOM } from '../nonDicomAction';
import { Button, message, notification } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

const CreateStudy = props => {
  const { studyInfo, onStudyInfoChange, onOpenEdit, withReport, convertToDicom } = props;
  const [imageList, setImageList] = useState([]);
  const [StudyInstanceUID, setStudyInstanceUID] = useState(null);

  const addImage = (prevList, src) => {
    const images = clone(prevList);
    if (Array.isArray(src)) {
      src.map(it => {
        const uid = uuidv4();
        images.push({
          id: uid,
          src: it,
          type: 'image',
        });
      });
    } else {
      const uid = uuidv4();
      images.push({
        id: uid,
        src: src,
        type: 'image',
      });
    }

    setImageList(images);
  };

  const addFile = async (prevList, file) => {
    const images = clone(prevList);
    if (Array.isArray(file)) {
      Promise.all(
        file.map(it => {
          if (it.type.indexOf('image') >= 0) {
            return fileToDataURL(it);
          } else {
            return it;
          }
        })
      ).then(list => {
        list.map(it => {
          const uid = uuidv4();
          if (it && it.type) {
            const type =
              it.type.indexOf('mp4') >= 0 || it.type.indexOf('webm') >= 0
                ? 'video'
                : it.type.split('/')[1];
            images.push({
              id: uid,
              type: type,
              src: it,
            });
          } else {
            images.push({
              id: uid,
              type: 'image',
              src: it,
            });
          }
        });
        setImageList(images);
      });
    } else {
      const uid = uuidv4();
      if (file.type.indexOf('image') >= 0) {
        fileToDataURL(file).then(src => {
          images.push({
            id: uid,
            type: 'image',
            src: src,
          });
          setImageList(images);
        });
      } else {
        const type =
          file.type.indexOf('mp4') >= 0 || file.type.indexOf('webm') >= 0
            ? 'video'
            : file.type.split('/')[1];
        images.push({
          id: uid,
          type: type,
          src: file,
          name: type == 'video' ? `${uid}.${file.type.split('/')[1]}` : '',
        });
        setImageList(images);
      }
    }
  };

  const removeImage = (prevList, id) => {
    const images = clone(prevList);
    const newImages = images.filter(it => it.id !== id);
    setImageList(newImages);
  };

  // useImperativeHandle(ref, () => ({
  const uploadImage = async () => {
    notification.open({
      key: 'create-study',
      message: (
        <>
          <LoadingOutlined />
          <span style={{ paddingLeft: 20 }}>Processing...</span>
        </>
      ),
      placement: 'bottomRight',
      duration: 0,
      className: 'collapse-report',
      // onClose: () => {
      //   clearInterval(interval);
      //   setLoading(false);
      // },
    });
    const formData = new FormData();
    imageList.map((it, idx) => {
      if (it.type == 'image') {
        const file = dataURLtoFile(it.src, idx + 1);
        formData.append('file', file);
      } else {
        if (it.name) {
          formData.append('file', it.src, it.name);
        } else {
          formData.append('file', it.src);
        }
      }
    });

    formData.append(
      'non_dicom_tags',
      JSON.stringify(dicomTagsAdapter(studyInfo))
    );

    formData.append('convert_to_dicom', convertToDicom);

    try {
      const res = await actionUploadNonDICOM(formData);
      if (res && res.data && res.data.study_instance_uid) {
        setStudyInstanceUID(res.data.study_instance_uid);
        message.success('Created study');

        notification.close('create-study');
      } else {
        message.error('Upload image failed');
        notification.close('create-study');
      }
    } catch (err) {
      console.log(err);
      message.error('Upload image failed');
      notification.close('create-study');
    }
  };
  // }));

  const store = {
    imageList,
    addImage,
    removeImage,
    addFile,
    studyInfo,
  };
  return (
    <CreateStudyContext.Provider value={store}>
      <div
        className="create-study"
        style={{ display: 'flex', width: '100%', height: '100%' }}
      >
        <div
          className="image-container"
          style={{ display: 'flex', width: '100%' }}
        >
          <div
            className="left-side"
            style={{
              width: withReport ? '35vw' : '50vw',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
              paddingRight: 10,
              marginBottom: 54,
            }}
          >
            <WebcamCapture withReport={withReport} />
            <StudyInfomation
              studyInfo={studyInfo}
              onChange={value => onStudyInfoChange(value)}
              onOpenEdit={onOpenEdit}
            />
          </div>
          <div
            className="right-side"
            style={{
              display: 'flex',
              width: withReport ? 'calc(65vw - 476px)' : 'calc(50vw - 56px)',
              flexDirection: 'column',
              justifyContent: 'space-between',
            }}
          >
            <ImageList imageList={imageList} />
            <div
              className="bottom-btn"
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
                padding: '15px 0 15px',
              }}
            >
              <Button
                onClick={() => uploadImage()}
                size="small"
                type="primary"
                style={{ minWidth: 80 }}
              >
                Finish
              </Button>
            </div>
          </div>
        </div>
        {withReport && (
          <div
            style={{
              width: 400,
              minWidth: 400,
              marginLeft: 10,
              // padding: 10,
              // border: '1px solid #647a87',
              // borderRadius: 2,
            }}
          >
            <DoctorTab StudyInstanceUID={StudyInstanceUID} />
          </div>
        )}
      </div>
    </CreateStudyContext.Provider>
  );
};

export default CreateStudy;
