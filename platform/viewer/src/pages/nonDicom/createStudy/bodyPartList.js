export const BODY_PART_LIST = [
  'ABDOMEN',
  'ABDOMENPELVIS',
  'ADRENAL',
  'ANKLE',
  'AORTA',
  'AXILLA',
  'BACK',
  'BLADDER',
  'BRAIN',
  'BREAST',
  'BRONCHUS',
  'BUTTOCK',
  'CALCANEUS',
  'CALF',
  'CAROTID',
  'CEREBELLUM',
  'CSPINE',
  'CTSPINE',
  'CERVIX',
  'CHEEK',
  'CHEST',
  'CHESTABDOMEN',
  'CHESTABDPELVIS',
  'CIRCLEOFWILLIS',
  'CLAVICLE',
  'COCCYX',
  'COLON',
  'CORNEA',
  'CORONARYARTERY',
  'DUODENUM',
  'EAR',
  'ELBOW',
  'WHOLEBODY',
  'ESOPHAGUS',
  'EXTREMITY',
  'EYE',
  'EYELID',
  'FACE',
  'FEMUR',
  'FINGER',
  'FOOT',
  'GALLBLADDER',
  'HAND',
  'HEAD',
  'HEADNECK',
  'HEART',
  'HIP',
  'HUMERUS',
  'ILEUM',
  'ILIUM',
  'IAC',
  'JAW',
  'JEJUNUM',
  'KIDNEY',
  'KNEE',
  'LARYNX',
  'LIVER',
  'LEG',
  'LSPINE',
  'LSSPINE',
  'LUNG',
  'JAW',
  'MAXILLA',
  'MEDIASTINUM',
  'MOUTH',
  'NECK',
  'NECKCHEST',
  'NECKCHESTABDOMEN',
  'NECKCHESTABDPELV',
  'NOSE',
  'ORBIT',
  'OVARY',
  'PANCREAS',
  'PAROTID',
  'PATELLA',
  'PELVIS',
  'PENIS',
  'PHARYNX',
  'PROSTATE',
  'RECTUM',
  'RIB',
  'SSPINE',
  'SCALP',
  'SCAPULA',
  'SCLERA',
  'SCROTUM',
  'SHOULDER',
  'SKULL',
  'SPINE',
  'SPLEEN',
  'STERNUM',
  'STOMACH',
  'SUBMANDIBULAR',
  'TMJ',
  'TESTIS',
  'THIGH',
  'TSPINE',
  'TLSPINE',
  'THUMB',
  'THYMUS',
  'THYROID',
  'TOE',
  'TONGUE',
  'TRACHEA',
  'ARM',
  'URETER',
  'URETHRA',
  'UTERUS',
  'VAGINA',
  'VULVA',
  'WRIST',
  'ZYGOMA',
];
