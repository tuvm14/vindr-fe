import Icon, {
  CameraOutlined,
  LoadingOutlined,
  SettingOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons';
import { Button, message, Select, Row, Col } from 'antd';
import React, { useEffect, useState } from 'react';
import Webcam from 'react-webcam';
import { TransformWrapper, TransformComponent } from 'react-zoom-pan-pinch';
import { resizedataURL } from '../../../utils/helper';
import CreateStudyContext from './CreateStudyContext';
import WebcamOffIcon from './videocam_off.svg';
import './CreateStudy.styl';
import { useTranslation } from 'react-i18next';

const VIDEO_SIZES = [
  { width: 800, height: 600 },
  { width: 1024, height: 768 },
  { width: 1152, height: 864 },
  { width: 1280, height: 720 },
  { width: 1280, height: 800 },
  { width: 1280, height: 1024 },
  { width: 1600, height: 900 },
  { width: 1600, height: 1200 },
  { width: 1600, height: 1050 },
  { width: 1920, height: 1080 },
];

// const videoConstraints = {
//   width: 1280,
//   height: 720,
//   facingMode: 'user',
// };

const WebcamPlaceholder = ({ text, withReport }) => {
  return (
    <div
      style={{
        width: withReport ? 'calc(35vw - 10px)' : 'calc(50vw - 10px)',
        paddingTop: '56.25%',
        position: 'relative',
      }}
    >
      <div
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        {text}
      </div>
    </div>
  );
};

let interval;

const WebcamCapture = ({ withReport }) => {
  const { t } = useTranslation('Vindoc');
  const webcamRef = React.useRef(null);
  const wrapperRef = React.useRef(null);
  const [sizeOption, setSizeOption] = useState(3);
  const [deviceList, setDeviceList] = useState([]);
  const [selectedDevice, setSelectedDevice] = useState(null);
  const [resetCameraView, setResetCameraView] = useState(false);
  const [videoConstraints, setVideoConstraints] = useState({
    width: 1280,
    height: 720,
    deviceId: null,
  });
  const [zoomState, setZoomState] = useState({
    positionX: 0,
    positionY: 0,
    previousScale: 1,
    scale: 1,
  });
  const { imageList, addImage, addFile } = React.useContext(CreateStudyContext);
  const mediaRecorderRef = React.useRef(null);
  const [recording, setRecording] = useState(false);
  const [timer, setTimer] = useState(0);
  const [recordedChunks, setRecordedChunks] = useState([]);
  const startTimer = () => {
    setTimer(0);
    interval = setInterval(() => {
      setTimer(timer => timer + 1);
    }, 1000);
  };
  const stopTimer = () => {
    clearInterval(interval);
  };

  const handleStartRecord = React.useCallback(() => {
    setRecording(true);
    startTimer();

    mediaRecorderRef.current = new MediaRecorder(webcamRef.current.stream, {
      mimeType: 'video/webm',
    });
    mediaRecorderRef.current.addEventListener(
      'dataavailable',
      handleDataAvailable
    );
    mediaRecorderRef.current.start();
  }, [webcamRef, setRecording, mediaRecorderRef]);

  const handleDataAvailable = React.useCallback(
    ({ data }) => {
      if (data.size > 0) {
        setRecordedChunks(prev => prev.concat(data));
      }
    },
    [setRecordedChunks]
  );

  const hanldeStopRecord = React.useCallback(() => {
    mediaRecorderRef.current.stop();
    setRecording(false);
    stopTimer();
    // console.log(recordedChunks);
  }, [mediaRecorderRef, webcamRef, setRecording]);

  useEffect(() => {
    if (!recording && recordedChunks.length) {
      const blob = new Blob(recordedChunks, {
        type: 'video/webm',
      });
      addFile(imageList, blob);
      // window.URL.revokeObjectURL(url);
      setRecordedChunks([]);
    }
  }, [recording, recordedChunks, imageList]);

  useEffect(() => {
    navigator.mediaDevices
      .getUserMedia({ video: true })
      .then(() => {
        navigator.mediaDevices
          .enumerateDevices()
          .then(devices => {
            console.log(devices);
            const videoDevices = devices.filter(it => it.kind == 'videoinput');
            setDeviceList(videoDevices);
            if (videoDevices && videoDevices.length) {
              setResetCameraView(true);
              setSelectedDevice(videoDevices[0].deviceId);
              setVideoConstraints({
                ...videoConstraints,
                deviceId: { exact: videoDevices[0].deviceId },
              });
              setTimeout(() => {
                setResetCameraView(false);
              }, 1000);
            }
          })
          .catch(function(err) {
            console.log(err.name + ': ' + err.message);
          });
      })
      .catch(function(err) {
        console.log(err.name + ': ' + err.message);
        message.error(t('Can not start camera'));
      });
  }, []);

  useEffect(() => {
    window.addEventListener('keyup', handleKeyUp);

    return () => {
      window.removeEventListener('keyup', handleKeyUp);
    };
  }, [imageList]);

  const handleKeyUp = event => {
    if (event.code === 'Space') {
      capture();
    }
  };

  const capture = React.useCallback(async () => {
    const viewWidth = wrapperRef.current.clientWidth - 10;
    console.log(videoConstraints);
    const imageSrc = webcamRef.current.getScreenshot({
      width: videoConstraints.width,
      height: videoConstraints.height,
    });
    const cropSrc = await resizedataURL(
      imageSrc,
      (((-1 * zoomState.positionX) / zoomState.scale) *
        videoConstraints.width) /
        viewWidth,
      (((-1 * zoomState.positionY) / zoomState.scale) *
        videoConstraints.width) /
        viewWidth,
      zoomState.scale
    );
    addImage(imageList, cropSrc);
  }, [webcamRef, imageList, zoomState, videoConstraints]);

  return (
    <div style={{ marginBottom: 10 }}>
      <div
        className="header"
        style={{ display: 'flex', justifyContent: 'space-between' }}
      >
        <Select
          style={{ width: 200, marginBottom: 10 }}
          size="small"
          value={selectedDevice}
          onChange={value => {
            setSelectedDevice(value);
            setResetCameraView(true);
            setVideoConstraints({
              ...videoConstraints,
              deviceId: value,
            });
            setTimeout(() => {
              // loading the changing size
              setResetCameraView(false);
            }, 1000);
          }}
        >
          {deviceList.map(it => (
            <Select.Option value={it.deviceId} key={it.deviceId}>
              {it.label}
            </Select.Option>
          ))}
        </Select>

        <Select
          style={{ width: 100, marginBottom: 10 }}
          size="small"
          value={sizeOption}
          onChange={value => {
            setSizeOption(value);
            setResetCameraView(true);
            setVideoConstraints({
              ...videoConstraints,
              width: VIDEO_SIZES[value].width,
              height: VIDEO_SIZES[value].height,
            });
            setTimeout(() => {
              setResetCameraView(false);
            }, 1000);
          }}
          suffixIcon={<SettingOutlined />}
        >
          {VIDEO_SIZES.map((it, idx) => (
            <Select.Option
              key={idx}
              value={idx}
            >{`${it.width}x${it.height}`}</Select.Option>
          ))}
        </Select>
      </div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <div
          className="video-wrapper"
          style={{
            padding: 5,
            border: '1px solid #647a87',
            borderRadius: 2,
            width: '100%',
          }}
          ref={wrapperRef}
        >
          {selectedDevice && (
            <div style={{ display: resetCameraView ? 'none' : 'block' }}>
              <TransformWrapper
                onZoomStop={(ref, event) => {
                  setZoomState(ref.state);
                }}
                onPanningStop={(ref, event) => setZoomState(ref.state)}
              >
                <TransformComponent>
                  <Webcam
                    audio={false}
                    ref={webcamRef}
                    screenshotFormat="image/jpeg"
                    width="100%"
                    screenshotQuality={1}
                    videoConstraints={videoConstraints}
                  />
                </TransformComponent>
              </TransformWrapper>
            </div>
          )}
          {!resetCameraView && !selectedDevice && (
            <WebcamPlaceholder
              text={
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  <Icon
                    style={{ fontSize: 24, marginRight: 10 }}
                    component={WebcamOffIcon}
                  />
                  <div>No Video Source</div>
                </div>
              }
              withReport={withReport}
            />
          )}
          {resetCameraView && (
            <WebcamPlaceholder
              text={
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  <LoadingOutlined style={{ fontSize: 24, marginRight: 10 }} />
                  <div>Loading</div>
                </div>
              }
              withReport={withReport}
            />
          )}
        </div>
        <Row gutter={10} style={{ width: '100%' }}>
          <Col span={12}>
            <Button
              onClick={capture}
              style={{ textAlign: 'center', width: '100%', marginTop: 10 }}
              type="primary"
            >
              <CameraOutlined /> Capture Image
            </Button>
          </Col>
          <Col span={12}>
            {recording ? (
              <Button
                onClick={hanldeStopRecord}
                style={{ textAlign: 'center', width: '100%', marginTop: 10 }}
                type="primary"
                danger
              >
                Stop {`${parseInt(timer / 60)}:${timer}`}
              </Button>
            ) : (
              <Button
                onClick={handleStartRecord}
                style={{ textAlign: 'center', width: '100%', marginTop: 10 }}
                type="primary"
              >
                <VideoCameraOutlined /> Record Video
              </Button>
            )}
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default React.memo(WebcamCapture);
