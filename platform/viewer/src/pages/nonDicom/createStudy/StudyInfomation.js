import { EditOutlined } from '@ant-design/icons';
import { Descriptions, Space } from 'antd';
import { Tooltip } from '@tuvm/ui';
import React from 'react';
import { useTranslation } from 'react-i18next';

const StudyInfomation = props => {
  const { studyInfo, onOpenEdit } = props;
  const { patient_name, patient_id, accession_number } = studyInfo;
  const { t } = useTranslation('Vindoc');
  return (
    <div
      className="study-info"
      style={{
        padding: 5,
        border: '1px solid #647a87',
        borderRadius: 2,
      }}
    >
      <Descriptions size="small" column={1}>
        <Descriptions.Item label={t('Patient Name')}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              width: '100%',
            }}
          >
            <span>{patient_name}</span>
            <Space style={{ fontSize: 14 }}>
              <Tooltip title={t('Edit')} arrowPointAtCenter>
                <EditOutlined onClick={() => onOpenEdit()} />
              </Tooltip>
            </Space>
          </div>
        </Descriptions.Item>
        <Descriptions.Item label={t('Patient ID')}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              width: '100%',
            }}
          >
            <span>{patient_id}</span>
          </div>
        </Descriptions.Item>
        <Descriptions.Item label={t('Accession Number')}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              width: '100%',
            }}
          >
            <span>{accession_number}</span>
          </div>
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

export default React.memo(StudyInfomation);
