import React, { useEffect, useState } from 'react';
import {
  Form,
  Input,
  Select,
  Collapse,
  DatePicker,
  Button,
  Modal,
  Switch,
} from 'antd';
import { useTranslation } from 'react-i18next';
import { BODY_PART_LIST } from './bodyPartList';
import moment from 'moment';
import './StudyInfoForm.styl';

const MODALITIES = [
  'CR',
  'DX',
  // 'DR',
  'CT',
  'MR',
  'MG',
  'US',
  'ES',
  'PT',
  'ST',
  'XA',
  'SR',
  'SM',
];

const SEX = [
  { display: 'Male', value: 'M' },
  { display: 'Female', value: 'F' },
  { display: 'Other', value: '' },
];

const StudyInfoForm = props => {
  const { studyInfo, onSave, onCancel } = props;
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const [birthDate, setBirthDate] = useState(null);
  const [openWithReport, setOpenWithReport] = useState(true);
  const [convertToDicom, setConvertToDicom] = useState(false);
  // const [studyDate, setStudyDate] = useState(null);
  // const [studyTime, setStudyTime] = useState(null);
  const [collapse, setCollapse] = useState([]);

  useEffect(() => {
    form.setFieldsValue({
      patientName: studyInfo?.patient_name,
      patientId: studyInfo?.patient_id,
      accessionNumber: studyInfo?.accession_number,
      modality: studyInfo?.modality,
      bodyPart: null,
      sex: studyInfo?.patient_sex,
      studyId: null,
      patientAge: studyInfo?.patient_age,
      institutionName: studyInfo?.tenant_id,
      studyDescription: studyInfo?.procedure_name,
      referringPhysician: studyInfo?.referring_physician_name,
      performingPhysician: null,
    });
    setBirthDate(studyInfo?.patient_birth_date || null);
  }, [studyInfo]);

  const handleSave = () => {
    form
      .validateFields()
      .then(async values => {
        let newStudy = {
          ...studyInfo,
          patient_id: values.patientId,
          patient_name: values.patientName,
          accession_number: values.accessionNumber,
          modality: values.modality,
          patient_birth_date: birthDate,
        };
        // Trick if patientSex is having value so more info is init
        if (values.sex) {
          newStudy = {
            ...newStudy,
            body_part: values.bodyPart,
            patient_sex: values.sex,
            patient_age: values.patientAge,
            study_id: values.studyId,
            procedure_name: values.studyDescription,
            referring_physician_name: values.referringPhysician,
            performing_physician_name: values.performingPhysician,
            institution_name: values.institutionName,
          };
        }
        onSave(newStudy, openWithReport, convertToDicom);
      })
      .catch(info => {
        console.log('Validate Failed:', info);
        Modal.error({
          title: 'Something went wrong',
          content: 'Please try again or contact admin for supports.',
        });
      });
  };

  return (
    <div className="study-info-form">
      <Form
        layout="vertical"
        hideRequiredMark
        form={form}
        name="dynamic_form_nest_item"
        onFinish={handleSave}
        className="form-filter-condition"
      >
        <Form.Item name="patientName" label={t('Patient Name')}>
          <Input placeholder={t('Enter patient name')} />
        </Form.Item>
        <Form.Item name="patientId" label={t('Patient ID')}>
          <Input placeholder={t('Enter patient ID')} />
        </Form.Item>
        <Form.Item name="accessionNumber" label={t('Accession Number')}>
          <Input placeholder={t('Enter accession number')} />
        </Form.Item>

        <Form.Item name="modality" label={t('Modality')}>
          <Select
            placeholder={t('Select Modality')}
            dropdownClassName="vindr-dropdown dropdown-options-dark"
          >
            {MODALITIES.map((it, idx) => (
              <Select.Option key={idx} value={it}>
                {it}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <div
          style={{ display: 'flex', alignItems: 'center', marginBottom: 10 }}
        >
          <label style={{ marginRight: 20 }}>{t('Open With Report')}</label>
          <Switch
            checked={openWithReport}
            onChange={value => setOpenWithReport(value)}
          />
        </div>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <label style={{ marginRight: 20 }}>
            {t('Convert file to DICOM format')}
          </label>
          <Switch
            checked={convertToDicom}
            onChange={value => setConvertToDicom(value)}
          />
        </div>

        <Collapse
          activeKey={collapse}
          ghost
          onChange={value => setCollapse(value)}
        >
          <Collapse.Panel
            header={collapse.indexOf('1') >= 0 ? 'Show less' : 'Show more'}
            key="1"
          >
            <Form.Item name="bodyPart" label={t('Body Part')}>
              <Select
                placeholder={t('Select Body Part')}
                dropdownClassName="vindr-dropdown dropdown-options-dark"
              >
                {BODY_PART_LIST.map((it, idx) => (
                  <Select.Option key={idx} value={it}>
                    {it}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              label={t('Birth Date')}
              rules={[{ required: true, message: t('Required value') }]}
            >
              <DatePicker
                value={birthDate ? moment(birthDate) : birthDate}
                onChange={date => {
                  setBirthDate(date.format('YYYYMMDD'));
                }}
              />
            </Form.Item>
            <Form.Item name="sex" label={t('Sex')}>
              <Select
                placeholder={t('Select')}
                dropdownClassName="vindr-dropdown dropdown-options-dark"
              >
                {SEX.map((it, idx) => (
                  <Select.Option key={idx} value={it.value}>
                    {it.display}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item name="patientAge" label={t('Patient Age')}>
              <Input placeholder={t('Enter patient age')} />
            </Form.Item>
            <Form.Item name="studyId" label={t('Study ID')}>
              <Input placeholder={t('Enter study ID')} />
            </Form.Item>
            <Form.Item name="studyDescription" label={t('Study Description')}>
              <Input placeholder={t('Enter study description')} />
            </Form.Item>
            <Form.Item name="institutionName" label={t('Institution Name')}>
              <Input placeholder={t('Enter institution name')} />
            </Form.Item>
            <Form.Item
              name="referringPhysician"
              label={t('Referring Physician')}
            >
              <Input placeholder={t('Enter referring physician')} />
            </Form.Item>
            <Form.Item
              name="performingPhysician"
              label={t('Performing Physician')}
            >
              <Input placeholder={t('Enter performing physician')} />
            </Form.Item>
            {/* <Form.Item
              label={t('Study Date')}
              rules={[{ required: true, message: t('Required value') }]}
            >
              <DatePicker
                value={studyDate ? moment(studyDate) : studyDate}
                onChange={date => {
                  setStudyDate(date.valueOf());
                }}
              />
            </Form.Item>
            <Form.Item
              label={t('Study Time')}
              rules={[{ required: true, message: t('Required value') }]}
            >
              <DatePicker
                picker="time"
                value={studyTime ? moment(studyTime) : studyTime}
                onChange={date => {
                  setStudyTime(date.valueOf());
                }}
              />
            </Form.Item> */}
          </Collapse.Panel>
        </Collapse>

        {/* <Form.Item name="report_layout" label={t('Report Layout')}>
      <Select
        placeholder={t('Select report layout')}
        dropdownClassName="vindr-dropdown dropdown-options-dark"
      >
        {REPORT_LAYOUT.map((it, idx) => (
          <Option key={idx} value={it.value}>
            {it.value}
          </Option>
        ))}
      </Select>
    </Form.Item> */}
      </Form>
      <div
        className="footer"
        style={{ display: 'flex', justifyContent: 'flex-end' }}
      >
        <Button ghost onClick={() => onCancel()} style={{ marginRight: 10 }}>
          Cancel
        </Button>
        <Button onClick={handleSave} type="primary">
          Confirm
        </Button>
      </div>
    </div>
  );
};

export default StudyInfoForm;
