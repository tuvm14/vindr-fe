import React from 'react';
import { connect } from 'react-redux';
import { Menu, Layout } from 'antd';
import { CaretRightOutlined, CaretLeftOutlined } from '@ant-design/icons';
import { useHistory, useRouteMatch } from 'react-router';
import TechnicianRoutes from '../routes';
import keycloak from '../../../services/auth';

import './LeftMenu.styl';
import { useTranslation } from 'react-i18next';

const { Sider } = Layout;
const { SubMenu } = Menu;

export const LeftMenu = props => {
  const history = useHistory();
  const { params } = useRouteMatch();
  const { page } = params;
  const [collapsed, setCollapsed] = React.useState(window.innerWidth <= 600);
  const { t } = useTranslation('Vindoc');

  const handleSelectMenu = e => {
    history.push(`/nondicom/${e.key}`);
  };

  const listMenu = TechnicianRoutes.filter(it =>
    keycloak.hasOneOfPerm(it.permissions)
  );

  return (
    <Sider
      className="left-menu-container"
      collapsible
      collapsed={collapsed}
      onCollapse={() => setCollapsed(!collapsed)}
      trigger={collapsed ? <CaretRightOutlined /> : <CaretLeftOutlined />}
      collapsedWidth={60}
      width={200}
    >
      <Menu
        mode="inline"
        defaultSelectedKeys={[page]}
        style={{ height: '100%' }}
        onSelect={e => handleSelectMenu(e)}
      >
        {listMenu.map(it =>
          it.children ? (
            <SubMenu
              className="submenu-list"
              key={it.key}
              icon={it.icon}
              title={t(it.menuLabel)}
            >
              {it.children.map(child => (
                <Menu.Item key={child.key} icon={child.icon}>
                  {t(child.menuLabel)}
                </Menu.Item>
              ))}
            </SubMenu>
          ) : (
            <Menu.Item key={it.key} icon={it.icon}>
              {t(it.menuLabel)}
            </Menu.Item>
          )
        )}
      </Menu>
    </Sider>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(LeftMenu);
