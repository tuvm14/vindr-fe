import api from '../../services/api';

export const actionUploadNonDICOM = (data = {}) => {
  return api({
    method: 'POST',
    url: '/backend/upload/v2/nondicom',
    data,
    redirect: 'follow',
  });
};
