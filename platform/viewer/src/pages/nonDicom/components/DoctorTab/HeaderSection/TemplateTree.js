import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Modal, Tree } from 'antd';

import './TemplateTree.styl';
import { useSelector } from 'react-redux';
import { actionSetExtensionData } from '../../../../../system/systemActions';
import { ExclamationCircleOutlined } from '@ant-design/icons';
const { confirm } = Modal;

export default function TemplateTree(props) {
  const [activeChild, setActiveChild] = useState({});
  const extensions = useSelector(state => state.extensions);
  const activeEditorTemplate =
    get(extensions, 'predictionPanel.activeEditorTemplate') || '';
  const doctorTabContent = get(extensions, 'predictionPanel.doctorTabContent');

  const editorTemplates =
    get(extensions, 'predictionPanel.editorTemplates') || [];
  const [expandedKeys, setExpandedKeys] = useState([]);
  const [treeData, setTreeData] = useState([]);
  const { t } = useTranslation('Vindoc');

  useEffect(() => {
    if (!isEmpty(editorTemplates)) {
      let data = [];
      editorTemplates.forEach((element, eIdx) => {
        const children = get(element, 'children') || [];
        if (!isEmpty(children)) {
          const filter = children.find(
            item => item._id == activeEditorTemplate
          );
          if (filter) {
            if (expandedKeys.indexOf(`parent_${element.name}`) < 0) {
              setExpandedKeys([...expandedKeys, `parent_${element.name}`]);
            }
            setActiveChild(filter);
          }
        }
        data.push({
          key: `parent_${element.name}`,
          title: (
            <div onClick={() => setExpandedKeys([`parent_${element.name}`])}>
              {`${eIdx + 1}. ${element.name}`}
            </div>
          ),
          selectable: false,
          children: children.map((it, idx) => ({
            key: it._id,
            title: `${eIdx + 1}.${idx + 1}. ${it.name}`,
          })),
        });
      });
      setTreeData(data);
    }
  }, [activeEditorTemplate, editorTemplates]);

  const getItem = id => {
    for (let i = 0; i < editorTemplates.length; i++) {
      const children = get(editorTemplates[i], 'children') || [];
      const filter = children.find(item => item._id == id);
      if (filter) return filter;
    }
  };

  const handleClickChild = id => {
    if (id && id.length) {
      const item = getItem(id[0]);
      confirm({
        zIndex: 1002,
        title: t('This template will overwrite your current work.'),
        icon: <ExclamationCircleOutlined />,
        content: t('Do you want to continue?'),
        centered: true,
        onOk() {
          setActiveChild(item);
          actionSetExtensionData('predictionPanel', {
            activeEditorTemplate: item._id,
            doctorTabContent: {
              ...doctorTabContent,
              editorSourceHTML: item.content,
            },
          });
        },
      });
    }
  };

  const isTemplateEmpty = isEmpty(editorTemplates);

  if (isTemplateEmpty) {
    return <div className="no-template-found">{t('There is no data')}</div>;
  }

  return (
    <div className="template">
      <Tree
        expandedKeys={expandedKeys}
        selectedKeys={[activeChild._id]}
        onExpand={expandedKeys => setExpandedKeys(expandedKeys)}
        onSelect={handleClickChild}
        treeData={treeData}
      />
    </div>
  );
}
