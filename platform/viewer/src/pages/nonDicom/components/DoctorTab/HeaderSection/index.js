import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Descriptions, Space } from 'antd';
import { Tooltip } from '@tuvm/ui';
import { useTranslation } from 'react-i18next';
import ModalDialog from '../ModalDialog';

import TemplateTree from './TemplateTree';
import { useSelector } from 'react-redux';
import get from 'lodash/get';
import { CopyOutlined } from '@ant-design/icons';
import CreateStudyContext from '../../../createStudy/CreateStudyContext';

const HeaderSection = props => {
  const { t } = useTranslation('Vindoc');
  const extensions = useSelector(state => state.extensions);
  // const viewports = useSelector(state => state.viewports);
  // const activeViewportIndex = get(viewports, 'activeViewportIndex');
  const { reportType } = props;

  const studyApproved = get(extensions, 'predictionPanel.studyApproved');
  const [openTemplate, setOpenTempplate] = useState(false);

  const { studyInfo } = React.useContext(CreateStudyContext);

  const handleOpenTemplate = () => {
    setOpenTempplate(!openTemplate);
  };

  return (
    <div className="panel-doctor-margin">
      <div className="header-info" style={{ marginBottom: 12 }}>
        <span>{studyInfo?.patient_name}</span>
      </div>
      <div
        className="button-group"
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          padding: 7,
          background: 'var(--box-bg-400)',
          borderRadius: 2,
        }}
      >
        <Descriptions size="small" column={1}>
          <Descriptions.Item label={t('Order')}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                width: '100%',
              }}
            >
              <span>{studyInfo.procedure_name}</span>
              <Space style={{ fontSize: 16 }}>
                <Tooltip
                  title={t('Select Template')}
                  arrowPointAtCenter
                  overlayClassName="open-template-tooltip"
                >
                  <CopyOutlined
                    onClick={handleOpenTemplate}
                    disabled={studyApproved}
                  />
                </Tooltip>
              </Space>
            </div>
          </Descriptions.Item>
          <Descriptions.Item label={t('AdmittingDiagnosesDescription')}>
            {t(studyInfo?.data?.diagnose)}
          </Descriptions.Item>
        </Descriptions>
      </div>

      {openTemplate && (
        <ModalDialog
          left={
            reportType == 'Modal'
              ? (window.innerWidth + 600) / 2
              : window.innerWidth - 772
          }
          top={(window.innerHeight - 700) / 2}
          width={400}
          open={openTemplate}
          handleClose={handleOpenTemplate}
          title={t('Select Template')}
          content={<TemplateTree />}
        />
      )}
    </div>
  );
};

HeaderSection.propTypes = {
  StudyInstanceUID: PropTypes.string,
};

export default React.memo(HeaderSection, () => true);
