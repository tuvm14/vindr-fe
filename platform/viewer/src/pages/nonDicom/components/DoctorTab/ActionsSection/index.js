import React from 'react';
import PropTypes from 'prop-types';
import { Button, message } from 'antd';
import { useTranslation } from 'react-i18next';
import Printing from './Printing';
import { editorSourceHTML } from '../ContentSection/TextEditor';
import { useSelector } from 'react-redux';
// import { useParams } from 'react-router-dom';
import get from 'lodash/get';

// import {
//   actionDoctorStorage,
//   actionGetSummary,
//   actionSubmitReport,
// } from '../../../PanelActions';
import {
  DOCTOR_TAB_ACTIONS,
  NEW_SCOPES,
  // REPORT_STATUS,
  SCOPES,
} from '../../../../../utils/constants';
// import { actionSetExtensionData } from '../../../../../system/systemActions';
import { htmlToText } from '../../../../../utils/helper';
import keycloak from '../../../../../services/auth';
import {
  CheckCircleOutlined,
  EyeOutlined,
  SaveOutlined,
} from '@ant-design/icons';
import {
  actionDoctorStorageV2,
  actionGetSummary,
  actionSubmitReport,
  getListReport,
} from '../../../../../components/Report/PanelActions';

const ActionsSection = props => {
  const { t } = useTranslation('Vindoc');
  const [openPrintDialog, setOpenPrintDialog] = React.useState(false);
  // const timepointManager = useSelector(state => state.timepointManager);
  const extensions = useSelector(state => state.extensions);
  // const viewports = useSelector(state => state.viewports);
  // const activeViewportIndex = get(viewports, 'activeViewportIndex');
  const studyInstanceUID = props.StudyInstanceUID;
  // const doctorResult = get(extensions, 'predictionPanel.doctorResult');
  const [studyApproved, setStudyApproved] = React.useState(false);
  const [reportId, setReportId] = React.useState(null);
  // const reportStatus = get(extensions, 'predictionPanel.reportStatus');

  const AccessionNumber = get(
    extensions,
    'predictionPanel.tenant.Patient.AccessionNumber'
  );

  const handlePrint = () => {
    setOpenPrintDialog(!openPrintDialog);
  };

  const handleAction = async actionName => {
    try {
      // const RectangleRoi = RectangleMeasurements.RectangleRoi;

      let measurements = [];

      // if (RectangleRoi) {
      //   measurements = RectangleRoi.map(item => ({
      //     start: item.handles.start,
      //     end: item.handles.end,
      //     labels: item.location,
      //     imagePath: item.imagePath,
      //     SOPInstanceUID: item.SOPInstanceUID,
      //     StudyInstanceUID: item.StudyInstanceUID,
      //     SeriesInstanceUID: item.SeriesInstanceUID,
      //     toolType: item.toolType,
      //   }));
      // }

      const text = htmlToText(editorSourceHTML);
      const rResult = text.split(/[Kk]ết luận\s*:\s*/)[1] || '';
      const result = rResult.trim();

      const payload = {
        editorSourceHTML,
        activeEditorTemplate: null,
        measurements: {
          RectangleRoi: measurements,
        },
        StudyInstanceUID: studyInstanceUID,
        action: actionName,
        AccessionNumber: AccessionNumber || '',
        result: result,
      };

      await actionDoctorStorageV2({
        ...payload,
        action: DOCTOR_TAB_ACTIONS.CREATE_REPORT,
      });
      // const RectangleMeasurements = timepointManager.measurements;
      // const activeEditorTemplate = get(
      //   extensions,
      //   'predictionPanel.activeEditorTemplate'
      // );
      const data1 = await getListReport(studyInstanceUID);

      const item = data1.find(
        it => it.study.StudyInstanceUID == studyInstanceUID
      );

      const reportId = get(item, 'reports[0].id_'); // change

      if (!reportId) {
        message.error('Create Report failed');
        // notification.close('create-study');
        return;
      }

      setReportId(reportId);

      // const reportId = get(extensions, 'predictionPanel.reportId');

      const notifyMessage =
        actionName === DOCTOR_TAB_ACTIONS.SUBMIT
          ? 'Submit successfully'
          : actionName === DOCTOR_TAB_ACTIONS.APPROVE
          ? 'Approve successfully'
          : 'Revoke successfully';

      await actionDoctorStorageV2({
        ...payload,
        reportId: reportId,
        action: DOCTOR_TAB_ACTIONS.SUBMIT,
      });

      if (actionName === DOCTOR_TAB_ACTIONS.APPROVE) {
        await actionDoctorStorageV2({
          ...payload,
          reportId: reportId,
          action: DOCTOR_TAB_ACTIONS.APPROVE,
        });
        setStudyApproved(true);
        await actionSubmitReport({
          StudyInstanceUID: studyInstanceUID,
          ReportID: reportId,
        });
      }

      await actionGetSummary({ StudyInstanceUID: studyInstanceUID });

      message.success(t(notifyMessage));
    } catch (err) {
      message.error(t('Error Message'));
    }
  };

  // useEffect(() => {
  //   if (!isEmpty(doctorResult)) {
  //     const { SUBMIT } = doctorResult;

  //     // const lastAction = getLastDoctorActionResult([
  //     //   SUBMIT,
  //     //   APPROVE,
  //     //   DONE,
  //     //   REVOKE,
  //     // ]);
  //     if (SUBMIT) {
  //       setStudySubmited(true);
  //     } else {
  //       setStudySubmited(false);
  //     }
  //     // if (lastAction && lastAction.action === DOCTOR_TAB_ACTIONS.APPROVE) {
  //     //   actionSetExtensionData('predictionPanel', {
  //     //     studyApproved: true,
  //     //   });
  //     // } else if (
  //     //   lastAction &&
  //     //   lastAction.action === DOCTOR_TAB_ACTIONS.REVOKE
  //     // ) {
  //     //   actionSetExtensionData('predictionPanel', {
  //     //     studyApproved: false,
  //     //   });
  //     // }
  //   }
  // }, [doctorResult]);

  return (
    <div className="panel-doctor-margin">
      <div
        className="button-group"
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          margin: '10px 0',
        }}
      >
        <Button
          type="primary"
          size="small"
          className="report-btn"
          style={{ marginLeft: 15, minWidth: 80 }}
          onClick={() => handleAction(DOCTOR_TAB_ACTIONS.SUBMIT)}
          disabled={
            !studyInstanceUID ||
            studyApproved ||
            !keycloak.hasOneOfPerm([
              SCOPES.reportSubmit,
              NEW_SCOPES.nondicom.report.submit,
            ])
          }
        >
          <SaveOutlined />
          {t('Submit')}
        </Button>

        {/* {studyApproved ? (
          <Button
            variant="contained"
            type="primary"
            className="report-btn"
            style={{ marginLeft: 15 }}
            onClick={() => {
              handleAction(DOCTOR_TAB_ACTIONS.REVOKE);
              actionSetExtensionData('predictionPanel', {
                studyApproved: false,
              });
            }}
            disabled={!keycloak.hasOneOfPerm([SCOPES.reportRevoke])}
          >
            <CheckCircleOutlined />
            {t('Revoke approval')}
          </Button>
        ) : ( */}
        <Button
          type="primary"
          className="report-btn"
          size="small"
          style={{ marginLeft: 15, minWidth: 80 }}
          onClick={() => handleAction(DOCTOR_TAB_ACTIONS.APPROVE)}
          disabled={
            !studyInstanceUID ||
            studyApproved ||
            !keycloak.hasOneOfPerm([
              SCOPES.reportApprove,
              NEW_SCOPES.nondicom.report.approve,
            ])
          }
        >
          <CheckCircleOutlined />
          {t('Approve')}
        </Button>

        <Button
          ghost
          className="report-btn"
          size="small"
          style={{ marginLeft: 15, minWidth: 80 }}
          onClick={handlePrint}
          disabled={!studyInstanceUID}
        >
          <EyeOutlined />
          {t('Preview')}
        </Button>
      </div>
      {openPrintDialog && (
        <Printing
          handleClose={handlePrint}
          StudyInstanceUID={studyInstanceUID}
          studyApproved={studyApproved}
          reportId={reportId}
        />
      )}
    </div>
  );
};

ActionsSection.propTypes = {
  StudyInstanceUID: PropTypes.string,
};

export default React.memo(ActionsSection);
