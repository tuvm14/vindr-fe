import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import './DoctorTab.styl';

import HeaderSection from './HeaderSection';
import ContentSection from './ContentSection';
import ActionsSection from './ActionsSection';
import {
  actionGetLayout,
  actionGetTemplate,
} from '../../../../components/Report/PanelActions';
// import { useSelector } from 'react-redux';
// import get from 'lodash/get';

const DoctorTab = props => {
  // const extensions = useSelector(state => state.extensions);
  // const reportId = get(extensions, 'predictionPanel.reportId');
  useEffect(() => {
    actionGetTemplate();
    actionGetLayout();
  }, []);

  return (
    <div className="panel-tab-wrapper">
      <div
        className="wrapper"
        style={{
          border: '1px solid #647a87',
          padding: 10,
          borderRadius: 2,
          paddingBottom: 110,
          height: '100%',
        }}
      >
        <HeaderSection
          reportType={props.reportType || 'Sidebar'}
          StudyInstanceUID={props.StudyInstanceUID}
        />

        <ContentSection />
      </div>

      <ActionsSection StudyInstanceUID={props.StudyInstanceUID} />
    </div>
  );
};

DoctorTab.propTypes = {
  StudyInstanceUID: PropTypes.string,
};

export default DoctorTab;
