import React, { useEffect } from 'react';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

import TextEditor from './TextEditor';
import './ContentSection.css';
import { useSelector } from 'react-redux';
import {
  clearMeasurementData,
  drawMeasurements,
  preFormatMeasurements,
  setDoctorResultToStore,
} from '../../../../../utils/helper';

import { editorSourceHTML as textEditorContent } from './TextEditor';

const ContentSection = () => {
  const extensions = useSelector(state => state.extensions);

  const measurementsData = get(
    extensions,
    'predictionPanel.doctorTabContent.measurementsData'
  );

  const editorSourceHTML = get(
    extensions,
    'predictionPanel.doctorTabContent.editorSourceHTML'
  );

  const editorTemplates =
    get(extensions, 'predictionPanel.editorTemplates') || [];

  const defaultTemplate = get(editorTemplates, '[0].children[0].content') || '';

  const tenant = get(extensions, 'predictionPanel.tenant');

  useEffect(() => {
    if (!isEmpty(measurementsData)) {
      clearMeasurementData();
      drawMeasurements(preFormatMeasurements(measurementsData));
    }
  }, [measurementsData]);

  useEffect(() => {
    return () => {
      setDoctorResultToStore(textEditorContent);
    };
  }, []);

  return (
    <>
      <div
        className="panel-doctor-description-content panel-doctor-margin"
        id="panel-text-editor"
      >
        <TextEditor
          content={editorSourceHTML || defaultTemplate}
          tenant={tenant}
        />
      </div>
    </>
  );
};

export default React.memo(ContentSection);
