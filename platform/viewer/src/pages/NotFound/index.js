import React from 'react';
import './NotFound.css';
import { Link } from 'react-router-dom';
import { Typography } from 'antd';
import PropTypes from 'prop-types';

import Header from '../../components/Header';
import { WarningOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

export default function NotFound({
  message = 'Sorry, this page does not exist.',
  showGoBackButton = true,
}) {
  const { t } = useTranslation('Vindoc');

  return (
    <>
      <Header />
      <div className="not-found text-center">
        <div>
          {/* <WarningIcon fontSize="large" /> */}
          <WarningOutlined style={{ fontSize: 35 }} />
          <Typography style={{ fontSize: 24 }}>{t(message)}</Typography>
          {showGoBackButton && (
            <Typography style={{ fontSize: 14 }}>
              <Link to={'/'}>{t('Go back Home')}</Link>
            </Typography>
          )}
        </div>
      </div>
    </>
  );
}

NotFound.propTypes = {
  message: PropTypes.string,
  showGoBackButton: PropTypes.bool,
};
