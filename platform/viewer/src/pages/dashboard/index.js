import React from 'react';
import { Layout, Row, Col } from 'antd';
import ConnectedHeader from '../../components/Header/ConnectedHeader';
import useNavigationMenu from '../../components/Header/useNavigationMenu';
import StudyByDay from './studyByDay';
import StudyByStatus from './studyByStatus';
import StudyByDoctor from './studyByDoctor';
import StudyByModality from './studyByModality';

import './dashboard.styl';

const { Content } = Layout;

function Dashboard() {
  const navigationOptions = useNavigationMenu();

  return (
    <Layout>
      <ConnectedHeader navigationOptions={navigationOptions} />
      <Layout className="dashboard-layout">
        <Row>
          <Col xs={24} sm={24} lg={16}>
            <Content className="dashboard-block-container">
              <StudyByDay />
            </Content>
          </Col>
          <Col xs={24} sm={24} lg={8}>
            <Content className="dashboard-block-container">
              <StudyByStatus />
            </Content>
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={24} lg={16}>
            <Content className="dashboard-block-container">
              <StudyByDoctor />
            </Content>
          </Col>
          <Col xs={24} sm={24} lg={8}>
            <Content className="dashboard-block-container">
              <StudyByModality />
            </Content>
          </Col>
        </Row>
      </Layout>
    </Layout>
  );
}

export default Dashboard;
