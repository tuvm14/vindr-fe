import React, { useState } from 'react';
import { DatePicker, Space, ConfigProvider } from 'antd';
import { useTranslation } from 'react-i18next';
import vi_VN from 'antd/lib/locale-provider/vi_VN';
import en_GB from 'antd/lib/locale-provider/en_GB';
import { LANGUAGES } from '../../../../utils/constants';

const { RangePicker } = DatePicker;

function CustomWeekRange({ initialTime, onChange }) {
  const { i18n } = useTranslation();
  const [date, setDate] = useState([
    initialTime?.startDate,
    initialTime?.endDate.subtract(1, 'days'),
  ]);

  return (
    <ConfigProvider locale={i18n.language === LANGUAGES.US ? en_GB : vi_VN}>
      <Space direction="vertical" size={12}>
        <RangePicker
          defaultValue={date}
          style={{
            maxHeight: '100%',
            maxWidth: '100%',
          }}
          picker="week"
          allowClear={false}
          onChange={newDate =>
            setDate([
              newDate[0].startOf('week').add(1, 'days'),
              newDate[1].endOf('week'),
            ])
          }
          onOpenChange={open => {
            if (open === true) return;
            let lastDate = date[1];
            lastDate.day() !== 7 && (lastDate = lastDate.endOf('week').add(1, 'days'));
            onChange([date[0], lastDate]);
          }}
        />
      </Space>
    </ConfigProvider>
  );
}

export default CustomWeekRange;
