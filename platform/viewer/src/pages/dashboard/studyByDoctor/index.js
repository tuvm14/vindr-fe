import React, { useState, useEffect } from 'react';

import { Table, Empty, ConfigProvider } from 'antd';
import Icon from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { actionGetStudyByDoctor } from '../actions';
import CustomDateRange from '../components/CustomDateRange';
import EmptyIcon from '../../../../public/assets/empty-img-simple.svg';
import '../dashboard.styl';

const columns = [
  'Doctor',
  'CR/DX',
  'CT',
  'MR',
  'US',
  'ES',
  'MG',
  'ECG',
  'XA',
  'PT',
  'Other',
  'Total',
];

function StudyByDoctor() {
  const { t } = useTranslation('Vindoc');
  const [filterDate, setFilterDate] = useState({
    start: moment(),
    end: moment(),
  });
  const [dataTable, setDataTable] = useState([]);
  const [isFetching, setIsFetching] = useState(false);
  const tableColumns = columns.map(item => {
    return { title: t(item), dataIndex: item, width: 54 };
  });

  useEffect(() => {
    handleFetchData({
      query_string: `StudyDate:[${filterDate.start.format(
        'YYYYMMDD'
      )} TO ${filterDate.end.format('YYYYMMDD')}]`,
    });
  }, [filterDate]);

  const handleFetchData = async params => {
    try {
      setIsFetching(true);
      const { data } = await actionGetStudyByDoctor({
        ...params,
      });
      data?.aggs?.length > 0 ? handleData(data.aggs) : setDataTable([]);
      setIsFetching(false);
    } catch (error) {
      console.log(error);
      setIsFetching(false);
    }
  };

  const handleData = data => {
    setDataTable(
      data.map(item => {
        let total = 0;
        let other = 0;
        for (const modality in item.data) {
          total += Number(item.data[modality]);
          if (
            !columns.includes(modality) &&
            modality !== 'CR' &&
            modality !== 'DX'
          )
            other += Number(item.data[modality]);
        }
        return {
          Doctor: item.user,
          'CR/DX': Number(item.data?.CR || 0) + Number(item.data?.DX || 0),
          CT: item.data?.CT || 0,
          MR: item.data?.MR || 0,
          US: item.data?.US || 0,
          ES: item.data?.ES || 0,
          MG: item.data?.MG || 0,
          ECG: item.data?.ECG || 0,
          XA: item.data?.XA || 0,
          PT: item.data?.PT || 0,
          Other: other,
          Total: total,
        };
      })
    );
  };

  return (
    <div className="chart-container">
      <div className="chart-header">
        <div className="chart-title">{t('Study statistic via doctor')}</div>
        <div className="chart-filter">
          <CustomDateRange
            size="small"
            value={filterDate}
            style={{ width: '100%' }}
            onChange={values =>
              setFilterDate({
                start: values[0],
                end: values[1],
              })
            }
          />
        </div>
      </div>
      <div className="data-content">
        <div className="ant-table-block">
          <ConfigProvider
            renderEmpty={() => (
              <Empty
                image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
                description={t('No Data')}
              />
            )}
          >
            <Table
              size="small"
              columns={tableColumns}
              dataSource={dataTable || []}
              // scroll={{ y: 'calc(100% - 31px)' }}
              pagination={false}
              loading={isFetching}
            />
          </ConfigProvider>
        </div>
      </div>
    </div>
  );
}

export default StudyByDoctor;
