import api from '../../services/api';

export const actionGetStudyByDay = params => {
  return api({
    url: '/studylist/dashboard/day',
    method: 'GET',
    params,
  });
};

export const actionGetStudyByDoctor = params => {
  return api({
    url: '/backend/report/dashboard/user',
    method: 'GET',
    params,
  });
};

export const actionGetStudyByModality = params => {
  return api({
    url: '/studylist/dashboard/modality',
    method: 'GET',
    params,
  });
};

export const actionGetStudyByStatus = params => {
  return api({
    url: '/studylist/dashboard/status',
    method: 'GET',
    params,
  });
};
