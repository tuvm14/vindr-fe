import React, { useState, useEffect } from 'react';

import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { actionGetStudyByStatus } from '../actions';
import CustomDateRange from '../components/CustomDateRange';
import '../dashboard.styl';

function StudyByStatus() {
  const { t } = useTranslation('Vindoc');
  const [filterDate, setFilterDate] = useState({
    start: moment(),
    end: moment(),
  });
  const [dataChart, setDataChart] = useState([0, 0, 0]);

  useEffect(() => {
    handleFetchData({
      query_string: `StudyDate:[${filterDate.start.format(
        'YYYYMMDD'
      )} TO ${filterDate.end.format('YYYYMMDD')}]`,
    });
  }, [filterDate]);

  const handleFetchData = async params => {
    try {
      const { data } = await actionGetStudyByStatus({
        ...params,
      });
      setDataChart([
        data.count || 0,
        data.aggs['READING'] || 0 + data.aggs['SUBMITTED'] || 0,
        data.aggs['APPROVED'] || 0,
      ]);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="chart-container">
      <div className="chart-header">
        <div className="chart-title">{t('Study statistic via status')}</div>
        <div className="chart-filter">
          <CustomDateRange
            size="small"
            value={filterDate}
            style={{ width: '100%' }}
            onChange={values =>
              setFilterDate({
                start: values[0],
                end: values[1],
              })
            }
          />
        </div>
      </div>
      <div className="data-content">
        <div className="bar-content">
          <p>{dataChart[0]}</p>
          <p>{t('Received')}</p>
        </div>
        <div className="bar-content">
          <p>{dataChart[1]}</p>
          <p>
            {t('Reading')} & {t('Submitted')}
          </p>
        </div>
        <div className="bar-content">
          <p>{dataChart[2]}</p>
          <p>{t('Approved')}</p>
        </div>
      </div>
    </div>
  );
}

export default StudyByStatus;
