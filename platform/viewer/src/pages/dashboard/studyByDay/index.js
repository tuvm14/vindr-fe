import React, { useEffect, useState } from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { actionGetStudyByDay } from '../actions';
import CustomWeekRange from '../components/CustomWeekRange';
import '../dashboard.styl';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip
);

const options = {
  responsive: true,
  scales: {
    x: {
      ticks: {
        color: '#ffffff',
      },
      grid: {
        color: '#615a5a',
      },
    },
    y: {
      ticks: {
        color: '#ffffff',
      },
      grid: {
        color: '#615a5a',
      },
      beginAtZero: true,
    },
  },
  maintainAspectRatio: false,
  plugins: {
    legend: {
      display: false,
    },
  },
};

const INITIAL_TIME = {
  startDate: moment()
    .endOf('week')
    .subtract(26, 'days'),
  endDate: moment()
    .endOf('week')
    .add(1, 'days'),
};

let params = {
  query_string: `StudyDate:[${INITIAL_TIME.startDate.format(
    'YYYYMMDD'
  )} TO ${INITIAL_TIME.endDate.format('YYYYMMDD')}]`,
};

function StudyByDay() {
  const { t } = useTranslation(['Vindoc']);
  const { i18n } = useTranslation();
  const labels = [
    t('Monday'),
    t('Tuesday'),
    t('Wednesday'),
    t('Thursday'),
    t('Friday'),
    t('Saturday'),
    t('Sunday'),
  ];
  const [dataChart, setDataChart] = useState({
    labels,
    datasets: [],
  });

  useEffect(() => {
    handleFetchData(params);
  }, []);

  useEffect(() => {
    setDataChart({
      labels,
      datasets: [
        {
          ...dataChart.datasets[0],
          label: t('Amount of studies'),
        },
      ],
    });
  }, [i18n.language]);

  const handleFetchData = async params => {
    try {
      const { data } = await actionGetStudyByDay({
        ...params,
      });
      const dataChart = [];
      for (const day in data.aggs) {
        dataChart.push(data.aggs[day]);
      }
      setDataChart({
        labels,
        datasets: [
          {
            label: t('Amount of studies'),
            data: dataChart,
            borderColor: 'rgb(255, 159, 64)',
            backgroundColor: 'rgba(255, 159, 64, 0.5)',
            pointStyle: 'circle',
            pointRadius: 5,
            pointHoverRadius: 8,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };

  const changeWeekRange = date => {
    params = {
      query_string: `StudyDate:[${date[0].format(
        'YYYYMMDD'
      )} TO ${date[1].format('YYYYMMDD')}]`,
    };
    handleFetchData(params);
  };

  return (
    <div className="chart-container">
      <div className="chart-header">
        <div className="chart-title">{t('Average of studies')}</div>
        <div className="chart-filter">
          <CustomWeekRange
            initialTime={INITIAL_TIME}
            onChange={date => changeWeekRange(date)}
          />
        </div>
      </div>
      <div className="chart-content">
        <Line options={options} data={dataChart} />
      </div>
    </div>
  );
}

export default StudyByDay;
