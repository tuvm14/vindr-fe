import React, { useState, useEffect } from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Doughnut } from 'react-chartjs-2';

import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { actionGetStudyByModality } from '../actions';
import CustomDateRange from '../components/CustomDateRange';
import '../dashboard.styl';

ChartJS.register(ArcElement, Tooltip, Legend);

const listColor = [
  'rgb(255, 99, 132)',
  'rgb(75, 192, 192)',
  'rgb(255, 159, 64)',
  'rgb(54, 162, 235)',
  'rgb(255, 205, 86)',
  'rgb(153, 102, 255)',
  'rgb(201, 203, 207)',
  'rgba(255, 99, 132, 0.6)',
  'rgba(54, 162, 235, 0.6)',
  'rgba(201, 203, 207, 0.6)',
];

const listModality = [
  'CR/DX',
  'CT',
  'MR',
  'US',
  'ES',
  'MG',
  'ECG',
  'XA',
  'PT',
  'Other',
];

const dataInit = {
  labels: listModality,
  datasets: [
    {
      data: [],
      backgroundColor: listColor,
      borderColor: listColor.map(() => '#1f2c33'),
      borderWidth: 2,
    },
  ],
};

const options = {
  responsive: true,
  maintainAspectRatio: true,
  plugins: {
    legend: {
      position: 'bottom',
      labels: {
        color: '#ffffff',
      },
    },
  },
};

function StudyByModality() {
  const { t } = useTranslation('Vindoc');
  const { i18n } = useTranslation();
  const [filterDate, setFilterDate] = useState({
    start: moment(),
    end: moment(),
  });
  const [dataChart, setDataChart] = useState(dataInit);
  const [isNonData, setNonData] = useState(true);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    handleFetchData({
      query_string: `StudyDate:[${filterDate.start.format(
        'YYYYMMDD'
      )} TO ${filterDate.end.format('YYYYMMDD')}]`,
    });
  }, [filterDate]);

  useEffect(() => {
    window.addEventListener('resize', () => {
      !isNonData && dataChart?.datasets[0].data?.length > 0 && setCenterText();
    });
  }, []);

  useEffect(() => {
    !isNonData &&
      dataChart?.datasets[0].data?.length > 0 &&
      setDataChart({
        ...dataChart,
        labels: dataChart?.pureLabels?.map(item => t(item)),
      });
  }, [i18n.language]);

  const handleFetchData = async params => {
    try {
      const { data } = await actionGetStudyByModality({
        ...params,
      });
      Object.keys(data.aggs).length > 0 ? handleData(data) : setNonData(true);
    } catch (error) {
      console.log(error);
    }
  };

  const handleData = data => {
    const newData = listModality.map(() => 0);
    const indexOfOther = listModality.indexOf('Other');
    for (const modality in data.aggs) {
      if (listModality.includes(modality)) {
        const index = listModality.indexOf(modality);
        newData[index] = data.aggs[modality];
      } else if (modality === 'CR' || modality === 'DX') {
        const index = listModality.indexOf('CR/DX');
        newData[index] = newData[index] + Number(data.aggs[modality]);
      } else {
        newData[indexOfOther] =
          newData[indexOfOther] + Number(data.aggs[modality]);
      }
    }
    const labels = newData
      .map((item, index) => {
        return {
          label: listModality[index],
          data: item,
        };
      })
      .filter(item => item.data > 0)
      .map(item => item.label);
    const newDataChart = {
      labels: labels.map(item => t(item)),
      pureLabels: labels,
      datasets: [
        {
          ...dataInit.datasets[0],
          data: newData.filter(item => item > 0),
        },
      ],
    };
    setNonData(false);
    setDataChart(newDataChart);
    setTotal(data.count);
    setCenterText();
  };

  const setCenterText = () => {
    const donutCenterTextDom = document.querySelector('.donut-center-text');
    const donutChartDom = document.querySelector('#donut-chart');
    if (donutCenterTextDom && donutChartDom) {
      let percentHeight = 0.92;
      if (screen.availWidth < 500) percentHeight = 0.86;
      else if (screen.availWidth < 800) percentHeight = 0.88;
      else if (screen.availWidth < 1100) percentHeight = 0.72;
      else if (screen.availWidth < 1400) percentHeight = 0.84;
      else if (screen.availWidth < 1700) percentHeight = 0.88;
      donutCenterTextDom.style.top = `${(Number(donutChartDom.offsetHeight) * percentHeight - Number(donutCenterTextDom.offsetHeight)) / 2}px`;
      donutCenterTextDom.style.left =`${(Number(donutChartDom.offsetWidth) - Number(donutCenterTextDom.offsetWidth)) / 2}px`;
    }
  };

  return (
    <div className="chart-container">
      <div className="chart-header">
        <div className="chart-title">{t('Study statistic via modality')}</div>
        <div className="chart-filter">
          <CustomDateRange
            size="small"
            value={filterDate}
            style={{ width: '100%' }}
            onChange={values =>
              setFilterDate({
                start: values[0],
                end: values[1],
              })
            }
          />
        </div>
      </div>
      <div id="donut-chart" className="chart-content">
        {isNonData || dataChart?.datasets[0].data?.length === 0 ? (
          <div className="chart-content-no-data">{t('No Data')}</div>
        ) : (
          <>
            <Doughnut options={options} data={dataChart} />
            {dataChart.datasets[0].data[0] !== 0 && (
              <div className="donut-center-text">{total}</div>
            )}
          </>
        )}
      </div>
    </div>
  );
}

export default StudyByModality;
