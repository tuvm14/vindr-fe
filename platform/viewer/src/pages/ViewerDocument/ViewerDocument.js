import React, { useEffect } from 'react';
import { documentS3Url } from '../../utils/constants';

const ViewerDocument = () => {
  const contentStyle = {
    width: '100%',
    height: '100vh',
    margin: 'auto',
  };

  useEffect(() => {
    document.title = 'VinDR - Help';
  }, []);

  return (
    <div style={contentStyle}>
      <object
        data={documentS3Url}
        aria-label="HDSD"
        type="application/pdf"
        style={contentStyle}
      ></object>
    </div>
  );
};

export default ViewerDocument;
