import api from '../../../services/api';
import { getTenant } from '../../../utils/helper';
const tenant = getTenant();

export const actionGetRoleList = async (params = {}) => {
  const data = await api({
    url: `/om/${tenant}/roles`,
    method: 'GET',
    params: params,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve([data.data.data, data.data.count]);
};

export const actionGetRole = async id => {
  const data = await api({
    url: `/om/${tenant}/roles/${id}`,
    method: 'GET',
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};

export const actionCreateRole = async (payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/roles`,
    method: 'POST',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};

export const actionUpdateRole = async (id, payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/roles/${id}`,
    method: 'PUT',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};

export const actionGetScopeList = async (params = {}) => {
  const data = await api({
    url: `/om/${tenant}/scopes`,
    method: 'GET',
    params: params,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });

  const result = data.data.data;
  // result.WORKLIST = [...result.STUDY, ...result.WORKLIST];
  // delete result.STUDY;

  const list = Object.keys(result).map(it => ({
    name: it,
    children: result[it],
  }));
  return Promise.resolve(list);
};

export const actionDeleteRole = async (id, payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/roles/${id}`,
    method: 'DELETE',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};
