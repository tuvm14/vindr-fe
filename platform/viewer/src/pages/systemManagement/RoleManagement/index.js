import React, { useEffect, useState } from 'react';
import {
  ConfigProvider,
  Empty,
  Table,
  Typography,
  Button,
  Modal,
  Form,
  Input,
  Card,
  Checkbox,
  Space,
  message,
} from 'antd';
import { Tooltip } from '@tuvm/ui';
import Icon, {
  // DeleteOutlined,
  EditOutlined,
  LockOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import EmptyIcon from '../../../../public/assets/empty-img-simple.svg';
import {
  actionGetRoleList,
  actionGetScopeList,
  actionCreateRole,
  actionUpdateRole,
  // actionDeleteRole,
  actionGetRole,
} from './actions';
import './RoleManagement.styl';
import keycloak from '../../../services/auth';
import { useTranslation } from 'react-i18next';
import { NEW_SCOPES, ROWS_PER_PAGE, SCOPES } from '../../../utils/constants';
import moment from 'moment';

const { Title } = Typography;

function RoleManagement() {
  const [roleList, setRoleList] = useState([]);
  // const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [openAddRoleModal, toggleAddRoleModal] = useState(false);
  const [scopes, setScopes] = useState([]);
  const [selectedScopes, setSelectedScopes] = useState([]);
  const [editItem, setEditItem] = useState(null);
  const [pagination, setPagination] = useState({
    page: 1,
    size: ROWS_PER_PAGE[0],
    total: 0,
  });
  const { t } = useTranslation('Vindoc');

  const [form] = Form.useForm();

  const handleSave = () => {
    form
      .validateFields()
      .then(async values => {
        let result = null;
        if (editItem && editItem.id) {
          result = await actionUpdateRole(editItem.id, {
            display_name: values.display_name,
            name: values.name,
            scopes: selectedScopes,
          });
        } else {
          result = await actionCreateRole({
            display_name: values.display_name,
            name: values.name,
            scopes: selectedScopes,
          });
        }
        console.log(result);
        if (result) {
          toggleAddRoleModal(false);
          fetchRoleList();
        } else {
          toggleAddRoleModal(false);
          Modal.error({
            title: 'Something went wrong',
            content: 'Please try again or contact admin for supports.',
          });
        }
      })
      .catch(info => {
        console.log('Validate Failed:', info);
        Modal.error({
          title: 'Something went wrong',
          content: 'Please try again or contact admin for supports.',
        });
      });
  };

  const getEditItemData = async id => {
    try {
      const role = await actionGetRole(id);
      setEditItem(role);
      setSelectedScopes(role.scopes);
      toggleAddRoleModal(true);
    } catch (err) {
      message.error('Cannot edit this role');
    }
  };

  const columns = [
    {
      title: '#',
      width: 40,
      align: 'center',
      dataIndex: 'id',
      key: 'id',
      render: (text, record, index) => index + 1,
    },
    {
      title: t('Role Name'),
      dataIndex: 'display_name',
      key: 'display_name',
      width: '25%',
      sorter: true,
    },
    {
      title: t('Role ID'),
      dataIndex: 'name',
      key: 'name',
      width: '25%',
      sorter: true,
    },
    // {
    //   title: 'Default Account',
    //   dataIndex: 'defaultAccount',
    //   key: 'defaultAccount',
    //   width: '25%',
    //   sorter: true,
    // },
    // {
    //   title: 'No.Accounts',
    //   dataIndex: 'numberAccount',
    //   key: 'numberAccount',
    //   width: '25%',
    //   sorter: true,
    // },
    {
      title: t('Last Modified'),
      dataIndex: 'updated',
      key: 'updated',
      sorter: true,
      render: text => moment(text * 1000).format('DD MMM YYYY HH:mm:ss'),
    },
    {
      title: '',
      dataIndex: 'operation',
      key: 'operation',
      align: 'center',
      width: 80,
      render: function ConnectorOperation(text, record) {
        if (record && record.is_locked) {
          return (
            <Tooltip title="Default role">
              <LockOutlined style={{ color: 'var(--error-color)' }} />
            </Tooltip>
          );
        }
        return (
          <Space size="small" className="actions">
            <Button
              style={{ width: 20, height: 20 }}
              size="small"
              ghost
              icon={<EditOutlined />}
              onClick={() => {
                form.setFieldsValue({
                  name: record.name,
                  display_name: record.display_name,
                });
                getEditItemData(record.id);
              }}
            />
            {/* <Button
              style={{ width: 20, height: 20 }}
              size="small"
              ghost
              icon={<DeleteOutlined />}
              onClick={() => {
                Modal.confirm({
                  title: t('Do you delete this item?'),
                  content: t('Data will be lost.'),
                  onOk: async () => {
                    await actionDeleteRole(record.id);
                    fetchRoleList();
                  },
                });
              }}
            /> */}
          </Space>
        );
      },
    },
  ];

  // const rowSelection = {
  //   selectedRowKeys,
  //   onChange: (rowKeys, selectedRows) => {
  //     setSelectedRowKeys(rowKeys);
  //   },
  // };

  useEffect(() => {
    fetchRoleList();
    fetchScopeList();
  }, []);

  const fetchRoleList = async (page = pagination.page) => {
    const [data, count] = await actionGetRoleList({
      offset: (page - 1) * pagination.size,
      limit: pagination.size,
    });
    setRoleList(data);
    setPagination({ ...pagination, total: count, page: page });
  };
  const fetchScopeList = async () => {
    const data = await actionGetScopeList({});
    setScopes(data);
  };

  return (
    <div className="role-management">
      <div className="page-title">
        <Title level={4}>{t('Role Management')}</Title>
        {keycloak.hasOneOfPerm([
          SCOPES.organizationAdmin,
          SCOPES.globalAdmin,
          NEW_SCOPES.administration.role.create,
        ]) && (
          <Button
            size="small"
            onClick={() => {
              form.setFieldsValue({
                name: null,
                display_name: null,
              });
              setSelectedScopes([]);
              toggleAddRoleModal(true);
              setEditItem(null);
            }}
            icon={<PlusOutlined />}
            type="primary"
          >
            {t('Add Role')}
          </Button>
        )}
      </div>
      <div className="content">
        <ConfigProvider
          renderEmpty={() => (
            <Empty
              image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
              description={t('No Data')}
            />
          )}
        >
          <Table
            size="small"
            columns={columns}
            dataSource={roleList}
            rowKey={record => record.id}
            pagination={{
              showQuickJumper: true,
              showSizeChanger: true,
              size: 'small',
              current: pagination.page,
              pageSize: pagination.size,
              total: pagination.total,
              onChange: pageNumber => {
                fetchRoleList(pageNumber);
              },
              showTotal: total => `${t('Total')} ${total} ${t('items')}`,
              pageSizeOptions: ROWS_PER_PAGE,
            }}
            // rowSelection={rowSelection}
          />
        </ConfigProvider>
      </div>
      <Modal
        className="vindr-modal add-institution"
        title={editItem && editItem.id ? t('Edit Role') : t('Add Role')}
        visible={openAddRoleModal}
        centered
        okText={t('Confirm')}
        cancelText={t('Cancel')}
        onOk={handleSave}
        okButtonProps={{
          disabled:
            (editItem &&
              editItem.id &&
              !keycloak.hasOneOfPerm([
                SCOPES.organizationAdmin,
                SCOPES.globalAdmin,
                NEW_SCOPES.administration.role.edit,
              ])) ||
            (!(editItem && editItem.id) &&
              !keycloak.hasOneOfPerm([
                SCOPES.organizationAdmin,
                SCOPES.globalAdmin,
                NEW_SCOPES.administration.role.create,
              ])),
        }}
        onCancel={() => toggleAddRoleModal(false)}
      >
        <Form
          layout="vertical"
          hideRequiredMark
          form={form}
          name="dynamic_form_nest_item"
          onFinish={handleSave}
          className="form-filter-condition"
        >
          <Form.Item
            name="display_name"
            label={t('Role Name') + ' *'}
            rules={[{ required: true, message: t('Required value') }]}
          >
            <Input placeholder={t('Enter Role Name')} />
          </Form.Item>
          <Form.Item
            name="name"
            label={t('Role ID') + ' *'}
            rules={[{ required: true, message: t('Required value') }]}
          >
            <Input
              disabled={editItem && editItem.id}
              placeholder={t('Enter Role ID')}
            />
          </Form.Item>
          <div style={{ paddingBottom: 8 }}>{t('Permissions')}</div>
          <div className="permission-list">
            <Checkbox.Group
              className="group-permission"
              value={selectedScopes}
              onChange={values => setSelectedScopes(values)}
            >
              {scopes.map((it, idx) => (
                <Card title={t(it.name)} key={idx} className="permission">
                  {it.children.map(item => (
                    <Checkbox
                      value={item.name}
                      key={item.id}
                      checked={
                        selectedScopes && selectedScopes.indexOf(item.name) >= 0
                      }
                    >
                      {t(item.display_name)}
                    </Checkbox>
                  ))}
                </Card>
              ))}
            </Checkbox.Group>
          </div>
        </Form>
      </Modal>
    </div>
  );
}

export default RoleManagement;
