import React, { useState, useEffect } from 'react';

import {
  Button,
  ConfigProvider,
  Typography,
  Empty,
  Table,
  Modal,
  Space,
  Form,
  Input,
  Select,
  Checkbox,
  Spin,
  message,
} from 'antd';
import Icon, {
  PlusOutlined,
  EditOutlined,
  DeleteOutlined,
  UserOutlined,
} from '@ant-design/icons';
import moment from 'moment';
import { isEmpty } from 'lodash';
import EmptyIcon from '../../../../public/assets/empty-img-simple.svg';
import keycloak from '../../../services/auth';
import {
  ROWS_PER_PAGE,
  FILTER_TYPE,
  FILTER_CONDITION_SET,
  FILTER_ALL_NAME,
  SCOPES,
  NEW_SCOPES,
} from '../../../utils/constants';
import { useTranslation } from 'react-i18next';
import {
  actionGetFilters,
  actionGetFilterGroups,
  actionCreateFilter,
  actionUpdateFilter,
  actionDeleteFilter,
} from './actions';
import './FilterManagement.styl';
import AddAccount from './components/AddAccount';

const { Title } = Typography;
const { Option } = Select;
const DATA_NULL = 'N/A';

const FilterManagement = () => {
  const { t } = useTranslation('Vindoc');
  const [form] = Form.useForm();
  const [filters, setFilters] = useState([]);
  const [filterGroups, setFilterGroups] = useState([]);
  const [editItem, setEditItem] = useState(null);
  const [editItemConditions, setEditItemConditions] = useState([]);
  const [editItemIsCon, setEditItemIsCon] = useState(true);
  const [openFormModal, setOpenFormModal] = useState(false);
  const [isMissValue, setIsMissValue] = useState(false);
  const [isSubmit, setIsSubmit] = useState(false);
  const [isActive, setIsActive] = useState(true);
  const [isProcessing, setIsProcessing] = useState(false);
  const [openAddAccountModal, setOpenAddAccountModal] = useState(false);
  const [pagination, setPagination] = useState({
    page: 1,
    size: ROWS_PER_PAGE[0],
    total: 0,
  });

  const columns = [
    {
      title: '#',
      width: 40,
      align: 'center',
      dataIndex: 'id',
      key: 'id',
      render: (text, record, index) => index + 1,
    },
    {
      title: t('Filter Name'),
      dataIndex: 'title',
      key: 'name',
      width: '15%',
    },
    {
      title: t('Group'),
      dataIndex: 'group',
      key: 'group',
      width: '10%',
      render: text =>
        text
          ? filterGroups.find(item => item.id === text)?.title || DATA_NULL
          : DATA_NULL,
    },
    {
      title: t('Created At'),
      dataIndex: 'created_time',
      key: 'created_time',
      render: text =>
        text ? moment(text).format('hh:mm:ss DD/MM/YYYY') : DATA_NULL,
    },
    {
      title: t('Updated At'),
      dataIndex: 'updated_time',
      key: 'updated_time',
      render: text =>
        text ? moment(text).format('hh:mm:ss DD/MM/YYYY') : DATA_NULL,
    },
    {
      title: t('Created By'),
      dataIndex: 'creator',
      key: 'creator',
      render: text => text || DATA_NULL,
    },
    {
      title: t('Status'),
      dataIndex: 'active',
      key: 'status',
      render: text => {
        return text ? t('ACTIVE') : t('DEACTIVATED');
      },
    },
    {
      title: t('Description'),
      dataIndex: 'description',
      key: 'description',
      render: text => text || DATA_NULL,
    },
    {
      title: t('Action'),
      dataIndex: '',
      key: 'action',
      align: 'center',
      width: 80,
      render: (text, record) => {
        return (
          <Space size="small" className="actions">
            <Button
              style={{ width: 20, height: 20 }}
              size="small"
              ghost
              disabled={record.name === FILTER_ALL_NAME}
              icon={<EditOutlined />}
              onClick={() => {
                const {
                  title,
                  group,
                  description,
                  type,
                  active,
                  conditions,
                  id,
                } = record;
                form.setFieldsValue({
                  id,
                  title,
                  group: group || null,
                  description,
                  type,
                  active,
                  conditions: conditions
                    ? conditions.map((item, index) => {
                        return { ...item, index: index };
                      })
                    : [],
                });
                setEditItem(record);
                setIsSubmit(false);
                setEditItemConditions(
                  conditions
                    ? conditions.map((item, index) => {
                        return { ...item, index: index };
                      })
                    : []
                );
                setIsActive(active);
                setEditItemIsCon(type === FILTER_TYPE[0].value);
                setOpenFormModal(true);
              }}
            />
            <Button
              style={{ width: 20, height: 20 }}
              size="small"
              ghost
              disabled={
                record.type === FILTER_TYPE[1].value &&
                record.name !== FILTER_ALL_NAME
              }
              icon={<UserOutlined />}
              onClick={() => {
                setEditItem(record);
                setOpenAddAccountModal(true);
              }}
            />
            {keycloak.hasOneOfPerm([
              SCOPES.organizationAdmin,
              SCOPES.globalAdmin,
              NEW_SCOPES.administration.filter.delete,
            ]) && (
              <Button
                style={{ width: 20, height: 20 }}
                size="small"
                ghost
                icon={<DeleteOutlined />}
                onClick={() => {
                  Modal.confirm({
                    title: t('Are you sure to delete this data'),
                    onOk: async () => {
                      setIsProcessing(true);
                      const deleteFilter = await actionDeleteFilter(record?.id);
                      if (deleteFilter?.status === 200) {
                        message.success(t('Success'));
                        fetchFilterList();
                        fetchFilterGroups();
                      } else message.error(t('Failed'));
                      setIsProcessing(false);
                    },
                    okText: t('Confirm'),
                    cancelText: t('Cancel'),
                  });
                }}
              />
            )}
          </Space>
        );
      },
    },
  ];

  const formColumns = [
    {
      title: t('Condition'),
      dataIndex: 'field',
      key: 'condition',
      width: '48%',
      render: (text, record, index) => {
        return (
          <Select
            bordered={false}
            style={{ width: '100%' }}
            defaultValue={text}
            placeholder={t('Select field', { field: t('condition') })}
            onChange={value => {
              const { conditions } = form?.getFieldValue();
              conditions[index].field = value;
              form.setFieldsValue({ conditions });
              setEditItemConditions(conditions);
              validateFilterCondition();
            }}
          >
            {FILTER_CONDITION_SET.map((item, index) => (
              <Option key={index} value={item.value}>
                {item.name}
              </Option>
            ))}
          </Select>
        );
      },
    },
    {
      title: t('Value'),
      dataIndex: 'values',
      key: 'value',
      width: '48%',
      render: (text, record, index) => {
        return (
          <Input
            bordered={false}
            defaultValue={text}
            onChange={event => {
              const { conditions } = form?.getFieldValue();
              conditions[index].values = event.target.value;
              form.setFieldsValue({ conditions });
              setEditItemConditions(conditions);
              validateFilterCondition();
            }}
            placeholder={t('Enter field', { field: t('value') })}
          />
        );
      },
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      align: 'center',
      width: '4%',
      render: (text, record, index) => {
        return (
          <Space size="small" className="actions">
            <Button
              style={{ width: 20, height: 20 }}
              size="small"
              ghost
              disabled={
                !keycloak.hasOneOfPerm([
                  SCOPES.organizationAdmin,
                  SCOPES.globalAdmin,
                  NEW_SCOPES.administration.filter.edit,
                ])
              }
              icon={<DeleteOutlined />}
              onClick={() => {
                Modal.confirm({
                  title: t('Are you sure to delete this data'),
                  onOk: () => {
                    const conditions = [...editItemConditions];
                    conditions.splice(index, 1);
                    form.setFieldsValue({ conditions });
                    setEditItemConditions(conditions);
                    validateFilterCondition();
                  },
                  okText: t('Confirm'),
                  cancelText: t('Cancel'),
                });
              }}
            />
          </Space>
        );
      },
    },
  ];

  useEffect(() => {
    fetchFilterList();
    fetchFilterGroups();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchFilterGroups = async () => {
    const { status, data } = await actionGetFilterGroups();
    if (status === 200) setFilterGroups(data.data);
  };

  const fetchFilterList = async (
    page = pagination.page,
    pageSize = pagination.size
  ) => {
    const { status, data } = await actionGetFilters({
      offset: (page - 1) * pageSize,
      limit: pageSize,
    });
    if (status === 200) setFilters(data.rows);
    setPagination({
      ...pagination,
      total: data.total,
      page: page,
      size: pageSize,
    });
  };

  const handleSave = () => {
    setIsSubmit(true);
    const data = form.getFieldValue();
    form
      .validateFields()
      .then(async () => {
        if (
          validateFilterCondition() &&
          (editItemConditions.length > 0 || data.type === FILTER_TYPE[1].value)
        ) {
          if (data.type === FILTER_TYPE[1].value) data.conditions = [];
          Modal.confirm({
            title: editItem
              ? t('Are you sure to edit this data')
              : t('Are you sure to create this data'),
            onOk: async () => {
              setIsProcessing(true);
              const requestFilter = editItem
                ? await actionUpdateFilter(data)
                : await actionCreateFilter(data);
              if (requestFilter.status === 200) {
                message.success(t('Success'));
                setOpenFormModal(false);
                fetchFilterList();
                fetchFilterGroups();
              } else message.error(t('Failed'));
              setIsProcessing(false);
            },
            okText: t('Confirm'),
            centered: true,
            cancelText: t('Cancel'),
          });
        }
      })
      .catch(() => {});
  };

  const validateFilterCondition = () => {
    if (form.getFieldValue().type === FILTER_TYPE[1].value) {
      setIsMissValue(false);
      return true;
    }
    for (const condition of editItemConditions) {
      if (isEmpty(condition.field) || isEmpty(condition.values)) {
        setIsMissValue(true);
        return false;
      }
    }
    setIsMissValue(false);
    return true;
  };

  return (
    <div className="filter">
      <div className="page-title">
        <Title level={4}>{t('Filter')}</Title>
        {keycloak.hasOneOfPerm([
          SCOPES.organizationAdmin,
          SCOPES.globalAdmin,
          NEW_SCOPES.administration.filter.create,
        ]) && (
          <Button
            size="small"
            onClick={() => {
              form.setFieldsValue({
                title: null,
                group: null,
                description: null,
                type: FILTER_TYPE[0].value,
                active: true,
                conditions: [{ field: 'vindr.AETitle', values: '', index: 1 }],
              });
              setEditItemConditions([]);
              setEditItemIsCon(true);
              setEditItemConditions([
                { field: 'vindr.AETitle', values: '', index: 1 },
              ]);
              setIsActive(true);
              setIsSubmit(false);
              setEditItem(null);
              setOpenFormModal(true);
            }}
            icon={<PlusOutlined />}
            type="primary"
          >
            {t('Add Filter')}
          </Button>
        )}
      </div>
      <div className="content">
        <ConfigProvider
          renderEmpty={() => (
            <Empty
              image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
              description={t('No Data')}
            />
          )}
        >
          <Table
            size="small"
            columns={columns}
            dataSource={filters}
            pagination={{
              showQuickJumper: true,
              showSizeChanger: true,
              size: 'small',
              current: pagination.page,
              pageSize: pagination.size,
              total: pagination.total,
              onChange: (page, pageSize) => {
                fetchFilterList(page, pageSize);
              },
              showTotal: total => `${t('Total')} ${total} ${t('items')}`,
              pageSizeOptions: ROWS_PER_PAGE,
            }}
            rowKey="id"
          />
        </ConfigProvider>
      </div>
      <Modal
        className="vindr-modal add-filter"
        title={editItem && editItem.id ? t('Edit Filter') : t('Add Filter')}
        visible={openFormModal}
        okText={t('Confirm')}
        cancelText={t('Cancel')}
        onOk={handleSave}
        okButtonProps={{
          style: {
            display: !keycloak.hasOneOfPerm([
              SCOPES.organizationAdmin,
              SCOPES.globalAdmin,
              NEW_SCOPES.administration.filter.edit,
            ])
              ? 'none'
              : 'inline-block',
          },
        }}
        onCancel={() => setOpenFormModal(false)}
        centered={true}
        destroyOnClose={true}
      >
        <Spin spinning={isProcessing}>
          <Form
            layout="vertical"
            hideRequiredMark
            form={form}
            name="dynamic_form_nest_item"
            onFinish={handleSave}
            className="form-filter-condition"
          >
            <Form.Item
              name="title"
              label={t('Filter Name') + ' *'}
              rules={[
                {
                  required: true,
                  message: t('Please enter field', {
                    field: t('filter name'),
                  }),
                },
              ]}
            >
              <Input
                placeholder={t('Enter field', {
                  field: t('filter name'),
                })}
              />
            </Form.Item>
            <Form.Item name="group" label={t('Group')}>
              <Select
                placeholder={t('Select field', { field: t('group') })}
                onChange={value => {
                  form.setFieldsValue({ group: value });
                }}
                allowClear
              >
                {(editItem
                  ? filterGroups.filter(item => item.id !== editItem.id)
                  : filterGroups
                )?.map((item, index) => (
                  <Option key={index} value={item?.id}>
                    {item?.title}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="type"
              label={t('Filter Type') + ' *'}
              rules={[
                {
                  required: true,
                  message: t('Please select field', {
                    field: t('filter type'),
                  }),
                },
              ]}
            >
              <Select
                placeholder={t('Select field', {
                  field: t('filter type'),
                })}
                disabled={Boolean(editItem)}
                onChange={value => {
                  form.setFieldsValue({ type: value });
                  setEditItemIsCon(value === FILTER_TYPE[0].value);
                }}
              >
                {FILTER_TYPE.map((item, index) => (
                  <Option key={index} value={item?.value}>
                    {t(item?.name)}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item name="description" label={t('Description')}>
              <Input.TextArea
                placeholder={t('Enter field', {
                  field: t('description'),
                })}
                rows={4}
              />
            </Form.Item>
            <Checkbox
              checked={isActive}
              onChange={event => {
                form.setFieldsValue({ active: event.target.checked });
                setIsActive(event.target.checked);
              }}
            >
              {t('ACTIVE')}
            </Checkbox>
            {editItemIsCon && (
              <div className="filter-form-table">
                <p>{t('Filter condition details')}</p>
                <p>{t('Comma separated values')}</p>
                <ConfigProvider
                  renderEmpty={() => (
                    <Empty
                      image={
                        <Icon style={{ fontSize: 64 }} component={EmptyIcon} />
                      }
                      description={t('No Data')}
                    />
                  )}
                >
                  <Table
                    size="small"
                    columns={formColumns}
                    dataSource={editItemConditions}
                    pagination={false}
                    rowKey="index"
                  />
                  {isMissValue && editItemConditions.length > 0 && isSubmit && (
                    <p className="error-text">
                      {t('Please enter the full filter data')}
                    </p>
                  )}
                  {editItemConditions.length === 0 && isSubmit && (
                    <p className="error-text">
                      {t('Please enter at least one filter')}
                    </p>
                  )}
                </ConfigProvider>
                <Button
                  type="link"
                  onClick={() => {
                    const conditions = [...editItemConditions];
                    conditions.push({
                      field: null,
                      values: null,
                      index: conditions.length + 1,
                    });
                    form?.setFieldsValue({ conditions });
                    setEditItemConditions(conditions);
                  }}
                  style={{ paddingLeft: 0, paddingTop: 8 }}
                  disabled={
                    !keycloak.hasOneOfPerm([
                      SCOPES.organizationAdmin,
                      SCOPES.globalAdmin,
                      NEW_SCOPES.administration.filter.edit,
                    ])
                  }
                >
                  {t('Add filter condition')}
                </Button>
              </div>
            )}
          </Form>
        </Spin>
      </Modal>
      <Modal
        className="vindr-modal add-account"
        title={
          <span>
            {t('Add accounts to')}{' '}
            {editItem?.title && (
              <span className="account-filter-name">{editItem.title}</span>
            )}
          </span>
        }
        visible={openAddAccountModal}
        onCancel={() => setOpenAddAccountModal(false)}
        centered={true}
        footer={
          <Button onClick={() => setOpenAddAccountModal(false)}>
            {t('Close')}
          </Button>
        }
      >
        <AddAccount filter={editItem} fetchFilterList={fetchFilterList} />
      </Modal>
    </div>
  );
};

export default FilterManagement;
