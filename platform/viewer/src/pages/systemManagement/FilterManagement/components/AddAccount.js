import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Empty,
  Table,
  Modal,
  Select,
  Spin,
  message,
  ConfigProvider,
} from 'antd';
import Icon, { DeleteOutlined } from '@ant-design/icons';
import EmptyIcon from '../../../../../public/assets/empty-img-simple.svg';
import { useTranslation } from 'react-i18next';
import { actionUpdateFilter } from '../actions';
import { actionGetAccountList } from '../../AccountManagement/actions';
import {
  ALL_USER_USERNAME_VALUE,
  NEW_SCOPES,
  SCOPES,
} from '../../../../utils/constants';

import '../FilterManagement.styl';
import keycloak from '../../../../services/auth';

const AddAccount = ({ filter, fetchFilterList }) => {
  const { t } = useTranslation('Vindoc');
  let usersInFilter = filter?.users.map(item => {
    return { username: item };
  });
  const [isProcessing, setIsProcessing] = useState(false);
  const [accountList, setAccountList] = useState([]);
  const [isSubmit, setIsSubmit] = useState(false);
  const [selectedAccount, setSelectedAccount] = useState([]);

  const columns = [
    {
      title: t('Account'),
      dataIndex: 'username',
      key: 'account',
    },
    {
      title: '',
      dataIndex: '',
      key: 'actions',
      width: 20,
      render: (text, record) => {
        return (
          <Button
            disabled={
              !keycloak.hasOneOfPerm([
                SCOPES.organizationAdmin,
                SCOPES.globalAdmin,
                NEW_SCOPES.administration.filter.edit,
              ])
            }
            style={{ width: 20, height: 20 }}
            size="small"
            ghost
            icon={<DeleteOutlined />}
            onClick={() => deleteAccount(record)}
          />
        );
      },
    },
  ];

  useEffect(() => {
    fetchAccountList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  const fetchAccountList = async () => {
    const data = await actionGetAccountList();
    if (data[0]) {
      setAccountList(
        [{ username: ALL_USER_USERNAME_VALUE }]
          .concat(data[0])
          .filter(
            item => !usersInFilter?.some(it => it.username === item.username)
          )
      );
    }
  };

  const addAccounts = () => {
    setIsSubmit(true);
    if (selectedAccount.length === 0) return;
    Modal.confirm({
      title: t('Are you sure to add these accounts to filter', {
        filterName: filter.title,
      }),
      onOk: async () => {
        setIsProcessing(true);
        const { users } = filter;
        for (const user of selectedAccount) {
          if (!users.some(item => item === user)) users.push(user);
        }
        filter.users = users;
        const data = { id: filter.id, users: users };
        try {
          const requestAddAccounts = await actionUpdateFilter(data);
          if (requestAddAccounts.status === 200) {
            message.success(t('Success'));
            fetchFilterList();
            setSelectedAccount([]);
            setIsSubmit(false);
            usersInFilter = users.map(item => {
              return { username: item };
            });
            fetchAccountList();
          }
          setIsProcessing(false);
        } catch (error) {
          setIsProcessing(false);
          message.error(t('Failed'));
        }
        setIsProcessing(false);
      },
      okText: t('Confirm'),
      centered: true,
      cancelText: t('Cancel'),
    });
  };

  const deleteAccount = account => {
    Modal.confirm({
      title: t('Are you sure to remove these accounts from filter', {
        filterName: filter.title,
      }),
      onOk: async () => {
        setIsProcessing(true);
        let { users } = filter;
        users = users.filter(item => item !== account.username);
        filter.users = users;
        const data = { id: filter.id, users: users };
        try {
          const requestRemoveAccounts = await actionUpdateFilter(data);
          if (requestRemoveAccounts.status === 200) {
            message.success(t('Success'));
            fetchFilterList();
            usersInFilter = users.map(item => {
              return { username: item };
            });
            fetchAccountList();
          }
          setIsProcessing(false);
        } catch (error) {
          setIsProcessing(false);
          message.error(t('Failed'));
        }
        setIsProcessing(false);
      },
      okText: t('Confirm'),
      centered: true,
      cancelText: t('Cancel'),
    });
  };

  return (
    <Spin spinning={isProcessing}>
      <div style={{ display: 'flex' }}>
        <Select
          mode="multiple"
          allowClear
          style={{ width: '100%', marginRight: 12 }}
          placeholder={t('Select field', { field: t('account') })}
          onChange={value => {
            setIsSubmit(false);
            setSelectedAccount(value);
          }}
          value={selectedAccount}
          options={accountList.map(item => {
            return { value: item.username };
          })}
        />
        <Button
          type="primary"
          onClick={addAccounts}
          disabled={
            !keycloak.hasOneOfPerm([
              SCOPES.organizationAdmin,
              SCOPES.globalAdmin,
              NEW_SCOPES.administration.filter.edit,
            ])
          }
        >
          {t('Add')}
        </Button>
      </div>
      {selectedAccount.length === 0 && isSubmit && (
        <p className="error-text">
          {t('Please select field', { field: t('account') })}
        </p>
      )}
      <p style={{ marginTop: 20 }}>
        {t('List of accounts with filter access')}
      </p>
      <ConfigProvider
        renderEmpty={() => (
          <Empty
            image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
            description={t('No Data')}
          />
        )}
      >
        <Table
          size="small"
          columns={columns}
          dataSource={usersInFilter}
          pagination={false}
          rowKey="username"
        />
      </ConfigProvider>
    </Spin>
  );
};

AddAccount.propTypes = {
  filter: PropTypes.object,
  fetchFilterList: PropTypes.func,
};

export default AddAccount;
