import api from '../../../services/api';

export const actionGetFilters = (params = {}) => {
  return api({
    url: '/studylist/filter/manage',
    method: 'GET',
    params,
  });
};

export const actionGetFilterGroups = (params = {}) => {
  return api({
    url: '/studylist/filter/groups',
    method: 'GET',
    params,
  });
};

export const actionCreateFilter = (payload = {}) => {
  return api({
    url: '/studylist/filter/manage',
    method: 'POST',
    data: payload,
  });
};

export const actionUpdateFilter = (payload = {}) => {
  return api({
    url: `/studylist/filter/manage/${payload.id}`,
    method: 'PUT',
    data: payload,
  });
};

export const actionDeleteFilter = id => {
  return api({
    url: `/studylist/filter/manage/${id}`,
    method: 'DELETE',
  });
};
