import React from 'react';
import {
  // PartitionOutlined,
  PlusSquareOutlined,
  SnippetsOutlined,
  TeamOutlined,
  FilterOutlined,
} from '@ant-design/icons';
import { NEW_SCOPES, SCOPES } from '../../utils/constants';
// import ServerConnection from './ServerConnection';
import RoleManagement from './RoleManagement';
import AccountManagement from './AccountManagement';
import Institution from './Institution';
import FilterManagement from './FilterManagement';
import TemplateManagement from './TemplateManagement';
import LayoutManagement from './LayoutManagement';

const SystemManagementRoutes = [
  // {
  //   key: 'connection',
  //   path: '/connection',
  //   component: <ServerConnection />,
  //   permissions: [SCOPES.organizationAdmin, SCOPES.globalAdmin],
  //   menuLabel: 'Server Connection',
  //   icon: <PartitionOutlined />,
  // },
  {
    key: 'institution',
    path: '/institution',
    component: <Institution />,
    permissions: [
      SCOPES.organizationAdmin,
      SCOPES.globalAdmin,
      NEW_SCOPES.administration.institute.view,
    ],
    menuLabel: 'Institution',
    icon: <PlusSquareOutlined />,
  },
  {
    key: 'user',
    permissions: [
      SCOPES.organizationAdmin,
      SCOPES.globalAdmin,
      NEW_SCOPES.administration.user.view,
    ],
    menuLabel: 'User Management',
    icon: <TeamOutlined />,
    children: [
      {
        key: 'role',
        path: '/role',
        component: <RoleManagement />,
        permissions: [
          SCOPES.organizationAdmin,
          SCOPES.globalAdmin,
          NEW_SCOPES.administration.role.view,
        ],
        menuLabel: 'Role',
      },
      {
        key: 'account',
        path: '/account',
        component: <AccountManagement />,
        permissions: [
          SCOPES.organizationAdmin,
          SCOPES.globalAdmin,
          NEW_SCOPES.administration.user.view,
        ],
        menuLabel: 'Account',
      },
    ],
  },
  {
    key: 'report',
    permissions: [
      SCOPES.organizationAdmin,
      SCOPES.globalAdmin,
      NEW_SCOPES.administration.report_template.view,
      NEW_SCOPES.administration.report_layout.view,
    ],
    menuLabel: 'Sidebar_Report',
    icon: <SnippetsOutlined />,
    children: [
      {
        key: 'template',
        path: '/template',
        component: <TemplateManagement />,
        permissions: [
          SCOPES.organizationAdmin,
          SCOPES.globalAdmin,
          NEW_SCOPES.administration.report_template.view,
        ],
        menuLabel: 'Template',
      },
      {
        key: 'layout',
        path: '/layout',
        component: <LayoutManagement />,
        permissions: [
          SCOPES.organizationAdmin,
          SCOPES.globalAdmin,
          NEW_SCOPES.administration.report_layout.view,
        ],
        menuLabel: 'Sidebar_Layout',
      },
    ],
  },
  {
    key: 'filter',
    path: '/filter',
    component: <FilterManagement />,
    permissions: [
      SCOPES.organizationAdmin,
      SCOPES.globalAdmin,
      NEW_SCOPES.administration.filter.view,
    ],
    menuLabel: 'Filter',
    icon: <FilterOutlined />,
  },
];

export default SystemManagementRoutes;
