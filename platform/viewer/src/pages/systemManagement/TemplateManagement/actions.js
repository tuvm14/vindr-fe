import api from '../../../services/api';
import { getTenant } from '../../../utils/helper';
const tenant = getTenant();

export const actionGetTemplateList = async (params = {}) => {
  const data = await api({
    url: `/om/${tenant}/templates?type=report-content`,
    method: 'GET',
    params,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve([data.data.data, data.data.count]);
};

export const actionCreateTemplate = async (payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/templates`,
    method: 'POST',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};

export const actionUpdateTemplate = async (id, payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/templates/${id}`,
    method: 'PUT',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};

export const actionDeleteTemplate = async (id, payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/templates/${id}`,
    method: 'DELETE',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};
