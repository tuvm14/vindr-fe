import React, { useEffect, useState } from 'react';
import {
  Button,
  ConfigProvider,
  Empty,
  Table,
  Typography,
  Modal,
  Space,
  Form,
  Input,
  Select,
} from 'antd';
import Icon, {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  // SearchOutlined,
} from '@ant-design/icons';
import EmptyIcon from '../../../../public/assets/empty-img-simple.svg';
import {
  actionCreateTemplate,
  actionDeleteTemplate,
  actionGetTemplateList,
  actionUpdateTemplate,
} from './actions';
import './TemplateManagement.styl';
import keycloak from '../../../services/auth';
import {
  MODALITIES,
  NEW_SCOPES,
  ROWS_PER_PAGE,
  SCOPES,
} from '../../../utils/constants';
import { useTranslation } from 'react-i18next';
import TextEditor from './TextEditor';

const { Title } = Typography;
const { Option } = Select;
// const { Search } = Input;

function TemplateManagement() {
  const [templates, setTemplates] = useState([]);
  // const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [openAddTemplateModal, toggleAddTemplateModal] = useState(false);
  const [templateContent, setTemplateContent] = useState('');
  const [newTemplateContent, setNewTemplateContent] = useState('');
  const [editItem, setEditItem] = useState(null);
  const [pagination, setPagination] = useState({
    page: 1,
    size: ROWS_PER_PAGE[0],
    total: 0,
  });
  const { t } = useTranslation('Vindoc');

  const [form] = Form.useForm();

  const handleSave = () => {
    form
      .validateFields()
      .then(async values => {
        let result = null;
        if (editItem && editItem.id) {
          result = await actionUpdateTemplate(editItem.id, {
            name: values.name,
            template_type: 'report-content',
            value: newTemplateContent,
            meta: {
              order: values.order,
              group: values.group,
            },
          });
        } else {
          result = await actionCreateTemplate({
            name: values.name,
            template_type: 'report-content',
            value: newTemplateContent,
            meta: {
              order: values.order,
              group: values.group,
            },
          });
        }
        if (result) {
          toggleAddTemplateModal(false);
          fetchTemplateList();
        }
      })
      .catch(info => {
        console.log('Validate Failed:', info);
        Modal.error({
          title: 'Something went wrong',
          content: 'Please try again or contact admin for supports.',
        });
      });
  };

  const columns = [
    {
      title: '#',
      width: 40,
      align: 'center',
      dataIndex: 'id',
      key: 'id',
      render: (text, record, index) => index + 1,
    },
    {
      title: t('Name'),
      dataIndex: 'name',
      key: 'name',
      // width: '25%',
      sorter: true,
    },
    {
      title: t('Order'),
      dataIndex: ['meta', 'order'],
      key: 'order',
      width: '25%',
      sorter: true,
    },
    {
      title: t('Group'),
      dataIndex: ['meta', 'group'],
      key: 'group',
      width: '15%',
      sorter: true,
    },
    {
      title: '',
      dataIndex: 'operation',
      key: 'operation',
      align: 'center',
      width: 80,
      render: function ConnectorOperation(text, record) {
        return (
          <Space size="small" className="actions">
            <Button
              style={{ width: 20, height: 20 }}
              size="small"
              ghost
              icon={<EditOutlined />}
              onClick={() => {
                form.setFieldsValue({
                  name: record.name,
                  order: record.meta.order,
                  group: record.meta.group,
                });
                setTemplateContent(record.value);
                setNewTemplateContent(record.value);
                toggleAddTemplateModal(true);
                setEditItem(record);
              }}
            />
            {keycloak.hasOneOfPerm([
              SCOPES.organizationAdmin,
              SCOPES.globalAdmin,
              NEW_SCOPES.administration.report_template.delete,
            ]) && (
              <Button
                style={{ width: 20, height: 20 }}
                size="small"
                ghost
                icon={<DeleteOutlined />}
                onClick={() => {
                  Modal.confirm({
                    title: t('Do you delete this item?'),
                    content: t('Data will be lost.'),
                    onOk: async () => {
                      await actionDeleteTemplate(record.id);
                      fetchTemplateList();
                    },
                  });
                }}
              />
            )}
          </Space>
        );
      },
    },
  ];

  // const rowSelection = {
  //   selectedRowKeys,
  //   onChange: (rowKeys, selectedRows) => {
  //     setSelectedRowKeys(rowKeys);
  //   },
  // };

  useEffect(() => {
    fetchTemplateList();
  }, []);

  const fetchTemplateList = async (page = pagination.page) => {
    const [data, count] = await actionGetTemplateList({
      offset: (page - 1) * pagination.size,
      limit: pagination.size,
    });
    setTemplates(data);
    setPagination({ ...pagination, total: count, page: page });
  };

  return (
    <div className="institution">
      <div className="page-title">
        <Title level={4}>{t('Template Management')}</Title>
        {keycloak.hasOneOfPerm([
          SCOPES.organizationAdmin,
          SCOPES.globalAdmin,
          NEW_SCOPES.administration.report_template.create,
        ]) && (
          <Button
            size="small"
            onClick={() => {
              form.setFieldsValue({
                name: null,
                order: null,
                group: null,
              });
              setTemplateContent('');
              setNewTemplateContent('');
              toggleAddTemplateModal(true);
              setEditItem(null);
            }}
            icon={<PlusOutlined />}
            type="primary"
          >
            {t('Add Template')}
          </Button>
        )}
      </div>
      <div className="content">
        {/* <Search
          size="small"
          style={{ marginBottom: 16, width: 'auto' }}
          placeholder={t('Keyword')}
          onSearch={keyword => console.log(keyword)}
          enterButton={
            <>
              <SearchOutlined /> Search
            </>
          }
        /> */}
        <ConfigProvider
          renderEmpty={() => (
            <Empty
              image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
              description={t('No Data')}
            />
          )}
        >
          <Table
            size="small"
            columns={columns}
            dataSource={templates}
            pagination={{
              showQuickJumper: true,
              showSizeChanger: true,
              size: 'small',
              current: pagination.page,
              pageSize: pagination.size,
              total: pagination.total,
              onChange: pageNumber => {
                fetchTemplateList(pageNumber);
              },
              showTotal: total => `${t('Total')} ${total} ${t('items')}`,
              pageSizeOptions: ROWS_PER_PAGE,
            }}
            // rowSelection={rowSelection}
            rowKey="id"
          />
        </ConfigProvider>
      </div>
      <Modal
        className="vindr-modal add-institution"
        title={editItem && editItem.id ? t('Edit Template') : t('Add Template')}
        height={window.innerHeight < 700 ? window.innerHeight : 700}
        width={window.innerWidth < 800 ? window.innerWidth : 800}
        visible={openAddTemplateModal}
        okText={t('Confirm')}
        cancelText={t('Cancel')}
        onOk={handleSave}
        okButtonProps={{
          disabled:
            (editItem &&
              editItem.id &&
              !keycloak.hasOneOfPerm([
                SCOPES.organizationAdmin,
                SCOPES.globalAdmin,
                NEW_SCOPES.administration.report_template.edit,
              ])) ||
            (!(editItem && editItem.id) &&
              !keycloak.hasOneOfPerm([
                SCOPES.organizationAdmin,
                SCOPES.globalAdmin,
                NEW_SCOPES.administration.report_template.create,
              ])),
        }}
        onCancel={() => toggleAddTemplateModal(false)}
        destroyOnClose
      >
        <Form
          layout="vertical"
          hideRequiredMark
          form={form}
          name="dynamic_form_nest_item"
          onFinish={handleSave}
          className="form-filter-condition"
        >
          <Form.Item
            name="name"
            label={t('Template Name') + ' *'}
            rules={[{ required: true, message: t('Required value') }]}
          >
            <Input placeholder={t('Enter Template Name')} />
          </Form.Item>
          <Form.Item
            name="order"
            label={t('Order') + ' *'}
            rules={[{ required: true, message: t('Required value') }]}
          >
            <Input placeholder={t('Enter Order')} />
          </Form.Item>

          <Form.Item name="group" label={t('Group')}>
            <Select
              placeholder={t('Select Group')}
              dropdownClassName="vindr-dropdown dropdown-options-dark"
            >
              {MODALITIES.map((it, idx) => (
                <Option key={idx} value={it}>
                  {it}
                </Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item label={t('Content')}>
            <TextEditor
              content={templateContent}
              onChange={value => setNewTemplateContent(value)}
            />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default TemplateManagement;
