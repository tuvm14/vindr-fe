import api from '../../../services/api';
import { getTenant } from '../../../utils/helper';
const tenant = getTenant();

export const actionGetInstitutionList = async (params = {}) => {
  const data = await api({
    url: `/om/${tenant}/institutions`,
    method: 'GET',
    params,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve([data.data.data, data.data.count]);
};

export const actionCreateInstitution = async (payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/institutions`,
    method: 'POST',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};

export const actionUpdateInstitution = async (id, payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/institutions/${id}`,
    method: 'PUT',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};

export const actionDeleteInstitution = async (id, payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/institutions/${id}`,
    method: 'DELETE',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};
