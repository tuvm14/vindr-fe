import React, { useEffect, useState } from 'react';
import {
  Button,
  ConfigProvider,
  Empty,
  Table,
  Typography,
  Modal,
  Space,
  Form,
  Input,
  // Select,
  Upload,
} from 'antd';
import Icon, {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  CameraOutlined,
} from '@ant-design/icons';
import EmptyIcon from '../../../../public/assets/empty-img-simple.svg';
import {
  actionCreateInstitution,
  actionDeleteInstitution,
  actionGetInstitutionList,
  actionUpdateInstitution,
} from './actions';
import './Institution.styl';
import keycloak from '../../../services/auth';
import { NEW_SCOPES, ROWS_PER_PAGE, SCOPES } from '../../../utils/constants';
import { useTranslation } from 'react-i18next';

const { Title } = Typography;
// const { Option } = Select;
// const REPORT_LAYOUT = [{ value: 'Layout 1' }, { value: 'Layout 2' }];

function Institution() {
  const [institutions, setInstitution] = useState([]);
  // const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [imageUrl, setImageUrl] = useState('');
  const [openAddInstitutionModal, toggleAddInstitutionModal] = useState(false);
  const [editItem, setEditItem] = useState(null);
  const [pagination, setPagination] = useState({
    page: 1,
    size: ROWS_PER_PAGE[0],
    total: 0,
  });
  const { t } = useTranslation('Vindoc');

  const [form] = Form.useForm();

  const handleSave = () => {
    form
      .validateFields()
      .then(async values => {
        let result = null;
        if (editItem && editItem.id) {
          result = await actionUpdateInstitution(editItem.id, {
            ...values,
            logo: imageUrl,
          });
        } else {
          result = await actionCreateInstitution({
            ...values,
            logo: imageUrl,
          });
        }
        if (result) {
          toggleAddInstitutionModal(false);
          fetchInstitutionList();
        }
      })
      .catch(info => {
        console.log('Validate Failed:', info);
        Modal.error({
          title: 'Something went wrong',
          content: 'Please try again or contact admin for supports.',
        });
      });
  };

  const columns = [
    {
      title: '#',
      width: 40,
      align: 'center',
      dataIndex: 'id',
      key: 'id',
      render: (text, record, index) => index + 1,
    },
    {
      title: t('Institution Name'),
      dataIndex: 'name',
      key: 'name',
      width: '15%',
      sorter: true,
    },
    {
      title: t('Address'),
      dataIndex: 'address',
      key: 'address',
      // width: '25%',
      sorter: true,
    },
    {
      title: t('Email'),
      dataIndex: 'email',
      key: 'email',
      width: '15%',
      sorter: true,
    },
    {
      title: t('Phone'),
      dataIndex: 'phone_number',
      key: 'phone_number',
      width: '15%',
      sorter: true,
    },
    // {
    //   title: 'Layout',
    //   dataIndex: 'report_layout',
    //   key: 'layout',
    //   width: '25%',
    //   sorter: true,
    //   ellipsis: true,
    // },
    {
      title: '',
      dataIndex: 'operation',
      key: 'operation',
      align: 'center',
      width: 80,
      render: function ConnectorOperation(text, record) {
        return (
          <Space size="small" className="actions">
            <Button
              style={{ width: 20, height: 20 }}
              size="small"
              ghost
              icon={<EditOutlined />}
              onClick={() => {
                form.setFieldsValue({
                  name: record.name,
                  group: record.group,
                  address: record.address,
                  email: record.email,
                  phone_number: record.phone_number,
                  report_layout: record.report_layout,
                });
                setImageUrl(record.logo);
                setEditItem(record);
                toggleAddInstitutionModal(true);
              }}
            />
            <Button
              style={{ width: 20, height: 20 }}
              size="small"
              ghost
              icon={<DeleteOutlined />}
              onClick={() => {
                Modal.confirm({
                  title: t('Do you delete this item?'),
                  content: t('Data will be lost.'),
                  onOk: async () => {
                    await actionDeleteInstitution(record.id);
                    fetchInstitutionList();
                  },
                });
              }}
            />
          </Space>
        );
      },
    },
  ];

  // const rowSelection = {
  //   selectedRowKeys,
  //   onChange: (rowKeys, selectedRows) => {
  //     setSelectedRowKeys(rowKeys);
  //   },
  // };

  useEffect(() => {
    fetchInstitutionList();
  }, []);

  const fetchInstitutionList = async (page = pagination.page) => {
    const [data, count] = await actionGetInstitutionList({
      offset: (page - 1) * pagination.size,
      limit: pagination.size,
    });
    setInstitution(data);
    setPagination({ ...pagination, total: count, page: page });
  };

  return (
    <div className="institution">
      <div className="page-title">
        <Title level={4}>{t('Institution')}</Title>
        {keycloak.hasOneOfPerm([
          SCOPES.organizationAdmin,
          SCOPES.globalAdmin,
          NEW_SCOPES.administration.institute.edit,
        ]) && (
          <Button
            size="small"
            onClick={() => {
              form.setFieldsValue({
                name: null,
                group: null,
                address: null,
                email: null,
                phone_number: null,
                report_layout: null,
              });
              setImageUrl('');
              setEditItem(null);
              toggleAddInstitutionModal(true);
            }}
            icon={<PlusOutlined />}
            type="primary"
          >
            {t('Add Institution')}
          </Button>
        )}
      </div>
      <div className="content">
        <ConfigProvider
          renderEmpty={() => (
            <Empty
              image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
              description={t('No Data')}
            />
          )}
        >
          <Table
            size="small"
            columns={columns}
            dataSource={institutions}
            pagination={{
              showQuickJumper: true,
              showSizeChanger: true,
              size: 'small',
              current: pagination.page,
              pageSize: pagination.size,
              total: pagination.total,
              onChange: pageNumber => {
                fetchInstitutionList(pageNumber);
              },
              showTotal: total => `${t('Total')} ${total} ${t('items')}`,
              pageSizeOptions: ROWS_PER_PAGE,
            }}
            // rowSelection={rowSelection}
            rowKey="id"
          />
        </ConfigProvider>
      </div>
      <Modal
        className="vindr-modal add-institution"
        title={
          editItem && editItem.id ? t('Edit Institution') : t('Add Institution')
        }
        visible={openAddInstitutionModal}
        okText={t('Confirm')}
        cancelText={t('Cancel')}
        onOk={handleSave}
        onCancel={() => toggleAddInstitutionModal(false)}
      >
        <Form
          layout="vertical"
          hideRequiredMark
          form={form}
          name="dynamic_form_nest_item"
          onFinish={handleSave}
          className="form-filter-condition"
        >
          <div className="wrapper">
            <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={null}
              beforeUpload={file => {
                let reader = new FileReader();
                reader.onload = e => {
                  setImageUrl(e.target.result);
                };
                reader.readAsDataURL(file);
              }}
              onChange={data => console.log(data)}
            >
              {imageUrl ? (
                <>
                  <img
                    className="image"
                    src={imageUrl}
                    alt="avatar"
                    style={{ width: '100%' }}
                  />
                  <div
                    className="overlay"
                    style={{
                      width: '100%',
                      height: '100%',
                      position: 'absolute',
                      display: 'none',
                      background: 'rgba(0,0,0,0.5)',
                      justifyContent: 'center',
                      alignItems: 'center',
                      fontSize: 16,
                      transition: 'all 0.3s',
                    }}
                  >
                    <Space size="middle">
                      <EditOutlined className="image-action" />
                      <DeleteOutlined
                        className="image-action"
                        onClick={e => {
                          e.preventDefault();
                          e.stopPropagation();
                          setImageUrl('');
                        }}
                      />
                    </Space>
                  </div>
                </>
              ) : (
                <div
                  className="signature-text"
                  style={{ color: '#fff', opacity: 0.4 }}
                >
                  <div style={{ marginTop: 8 }}>{t('Upload Logo')}</div>
                  <CameraOutlined style={{ fontSize: 20 }} />
                </div>
              )}
            </Upload>
            <div className="right-wrapper">
              <Form.Item
                name="name"
                label={t('Institution Name') + ' *'}
                rules={[{ required: true, message: t('Required value') }]}
              >
                <Input placeholder={t('Enter Institution Name')} />
              </Form.Item>
              <Form.Item
                name="group"
                label={t('Institution Group') + ' *'}
                rules={[{ required: true, message: t('Required value') }]}
              >
                <Input placeholder={t('Enter Institution Group')} />
              </Form.Item>
            </div>
          </div>

          <Form.Item name="address" label={t('Address')}>
            <Input.TextArea placeholder={t('Enter address')} rows={4} />
          </Form.Item>
          <Form.Item name="email" label={t('Email')}>
            <Input placeholder={t('Enter email')} />
          </Form.Item>
          <Form.Item name="phone_number" label={t('Phone Number')}>
            <Input placeholder={t('Enter phone number')} />
          </Form.Item>

          {/* <Form.Item name="report_layout" label={t('Report Layout')}>
            <Select
              placeholder={t('Select report layout')}
              dropdownClassName="vindr-dropdown dropdown-options-dark"
            >
              {REPORT_LAYOUT.map((it, idx) => (
                <Option key={idx} value={it.value}>
                  {it.value}
                </Option>
              ))}
            </Select>
          </Form.Item> */}
        </Form>
      </Modal>
    </div>
  );
}

export default Institution;
