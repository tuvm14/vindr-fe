import React, { useEffect, useState } from 'react';
import { ConfigProvider, Empty, Badge, Table, Typography } from 'antd';
import Icon from '@ant-design/icons';
import EmptyIcon from '../../../../public/assets/empty-img-simple.svg';
import { actionGetDeviceList } from './actions';
import './ServerConnection.styl';
import { useTranslation } from 'react-i18next';

const { Title } = Typography;

const DeviceTable = data => {
  const columns = [
    { title: 'Device', dataIndex: 'device', key: 'device' },
    { title: 'LAN IP', dataIndex: 'lanIp', key: 'lanIp' },
    { title: 'Port', dataIndex: 'port', key: 'port' },
    { title: 'AE Title', dataIndex: 'aeTitle', key: 'aeTitle' },
    // {
    //   title: 'Action',
    //   dataIndex: 'operation',
    //   key: 'operation',
    //   render: function DeviceOperation() {
    //     return (
    //       <Space size="middle">
    //         <a>Edit</a>
    //         <a>Verify</a>
    //         <a>Delete</a>
    //       </Space>
    //     );
    //   },
    // },
  ];

  return (
    <Table
      size="small"
      columns={columns}
      dataSource={data.devices || []}
      pagination={false}
    />
  );
};

const ConnectorTable = data => {
  const columns = [
    { title: 'Connector', dataIndex: 'connector', key: 'connector' },
    {
      title: 'Status',
      key: 'status',
      render: function ConnectorStatus(d) {
        switch (d.status) {
          case 'running':
            return (
              <span>
                <Badge status="processing" />
                Running
              </span>
            );
          case 'stopped':
            return (
              <span>
                <Badge status="error" />
                Stopped
              </span>
            );
          case 'not_connected':
            return (
              <span>
                <Badge status="default" />
                Not connected
              </span>
            );
          default:
            return (
              <span>
                <Badge status="default" />
                Not connected
              </span>
            );
        }
      },
    },
    { title: 'N.o.Device', dataIndex: 'numberDevice', key: 'numberDevice' },
    // {
    //   title: 'Action',
    //   dataIndex: 'operation',
    //   key: 'operation',
    //   render: function ConnectorOperation() {
    //     return (
    //       <Space size="middle">
    //         <a>Edit</a>
    //         <a>Start</a>
    //         <a>Delete</a>
    //       </Space>
    //     );
    //   },
    // },
  ];

  return (
    <Table
      size="small"
      columns={columns}
      dataSource={data.connectors || []}
      pagination={false}
      expandedRowRender={DeviceTable}
    />
  );
};

function ServerConnection() {
  const [connections, setConnections] = useState([]);
  const { t } = useTranslation('Vindoc');

  const columns = [
    {
      title: 'Institution',
      dataIndex: 'institution',
      key: 'institution',
    },
    {
      title: 'N.o.Connector',
      dataIndex: 'numberConnector',
      key: 'numberConnector',
      width: '30%',
    },
  ];

  useEffect(() => {
    fetchConnectionList();
  }, []);

  const fetchConnectionList = async () => {
    const data = await actionGetDeviceList({});
    setConnections(data);
  };

  return (
    <div className="server-connection">
      <Title className="page-title" level={4}>
        Server Connection
      </Title>
      <div className="content">
        <ConfigProvider
          renderEmpty={() => (
            <Empty
              image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
              description={t('No Data')}
            />
          )}
        >
          <Table
            size="small"
            columns={columns}
            dataSource={connections}
            expandedRowRender={ConnectorTable}
            pagination={false}
          />
        </ConfigProvider>
      </div>
    </div>
  );
}

export default ServerConnection;
