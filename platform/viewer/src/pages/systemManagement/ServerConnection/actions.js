import api from '../../../services/api';

export const actionGetDeviceList = async (params = {}) => {
  const connectors = await api({
    url: '/connector/connectors',
    method: 'GET',
    params,
    timeout: 30000,
  });
  const devices = await api({
    // baseURL: 'http://10.124.68.187:8080',
    url: '/connector/devices',
    method: 'GET',
    params,
    timeout: 30000,
  });
  let list = [];
  connectors.data.forEach(it => {
    const {
      institution_id,
      institution_name,
      connector_name,
      _id_1, // connector_id
    } = it;
    let instidution_idx = list.findIndex(t => t.key == institution_id);
    if (instidution_idx >= 0) {
      list[instidution_idx].connectors.push({
        key: _id_1,
        connector: connector_name,
        numberDevice: 0,
        devices: [],
      });
      list[instidution_idx].numberConnector += 1;
    } else {
      list.push({
        key: institution_id,
        institution: institution_name,
        numberConnector: 1,
        connectors: [
          {
            key: _id_1,
            connector: connector_name,
            numberDevice: 0,
            devices: [],
          },
        ],
      });
    }
  });
  devices.data.forEach(it => {
    const {
      institution_id,
      connector_id,
      _id_2,
      device_name,
      lan_ip,
      port,
      ae_title,
    } = it;
    let instidution_idx = list.findIndex(t => t.key == institution_id);
    if (instidution_idx >= 0) {
      let connector_idx = list[instidution_idx].connectors.findIndex(
        t => t.key == connector_id
      );
      if (connector_idx >= 0) {
        list[instidution_idx].connectors[connector_idx].devices.push({
          key: _id_2,
          device: device_name,
          lanIp: lan_ip,
          port: port,
          aeTitle: ae_title,
        });
        list[instidution_idx].connectors[connector_idx].numberDevice += 1;
      }
    }
  });
  return Promise.resolve(list);
};
