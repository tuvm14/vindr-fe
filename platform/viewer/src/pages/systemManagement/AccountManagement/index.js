import React, { useEffect, useState } from 'react';
import {
  ConfigProvider,
  Empty,
  Table,
  Typography,
  Button,
  Modal,
  Form,
  Input,
  Select,
  DatePicker,
  Space,
  Upload,
} from 'antd';
import moment from 'moment';
import Icon, {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  CameraOutlined,
} from '@ant-design/icons';
import EmptyIcon from '../../../../public/assets/empty-img-simple.svg';
import {
  actionCreateAccount,
  actionDeleteAccount,
  actionGetAccountList,
  actionUpdateAccount,
} from './actions';
import './AccountManagement.styl';
import keycloak from '../../../services/auth';
import { NEW_SCOPES, ROWS_PER_PAGE, SCOPES } from '../../../utils/constants';
import { useTranslation } from 'react-i18next';
import { actionGetRoleList } from '../RoleManagement/actions';

const { Title } = Typography;
const { Option } = Select;
// const INSTITUTIONS = [{ value: 'Institution 1' }, { value: 'Institution 2' }];

function AccountManagement() {
  const [roleList, setRoleList] = useState([]);
  const [accounts, setAccounts] = useState([]);
  // const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [openAddAccountModal, toggleAddAccountModal] = useState(false);
  const [expiredDate, setExpiredDate] = useState(null);
  const [editItem, setEditItem] = useState(null);
  const [imageUrl, setImageUrl] = useState('');
  const { t } = useTranslation('Vindoc');

  const [pagination, setPagination] = useState({
    page: 1,
    size: ROWS_PER_PAGE[0],
    total: 0,
  });

  const [form] = Form.useForm();

  const handleSave = () => {
    form
      .validateFields()
      .then(async values => {
        let result = null;
        if (editItem && editItem.id) {
          result = await actionUpdateAccount(editItem.id, {
            ...values,
            expired_date: expiredDate,
            signature: imageUrl,
          });
        } else {
          result = await actionCreateAccount({
            ...values,
            expired_date: expiredDate,
            signature: imageUrl,
          });
        }
        if (result) {
          toggleAddAccountModal(false);
          fetchAccountList();
        }
      })
      .catch(info => {
        console.log('Validate Failed:', info);
        Modal.error({
          title: 'Something went wrong',
          content: 'Please try again or contact admin for supports.',
        });
      });
  };

  const columns = [
    {
      title: '#',
      width: 40,
      align: 'center',
      dataIndex: 'id',
      key: 'id',
      render: (text, record, index) => index + 1,
    },
    {
      title: t('Username'),
      dataIndex: 'username',
      key: 'username',
      width: '15%',
      sorter: true,
    },
    {
      title: t('Roles'),
      dataIndex: 'roles',
      key: 'roles',
      // width: '25%',
      sorter: true,
      ellipsis: true,
      render: roles => {
        if (roles && roles.length)
          return roles.map(it => it.display_name || it.name).join(', ');
        else return roles;
      },
    },
    {
      title: t('Status'),
      dataIndex: 'status',
      key: 'status',
      width: '10%',
      render: text => t(text),
      sorter: true,
    },
    {
      title: t('Full Name'),
      dataIndex: 'fullname',
      key: 'fullname',
      width: '15%',
      sorter: true,
    },
    {
      title: t('Title'),
      dataIndex: 'user_title',
      key: 'title',
      width: '15%',
      sorter: true,
    },
    // {
    //   title: 'Institution',
    //   dataIndex: 'institution',
    //   key: 'institution',
    //   sorter: true,
    // },
    {
      title: t('Expired Date'),
      dataIndex: 'expired_date',
      key: 'expired_date',
      width: '15%',
      sorter: true,
      render: text =>
        text ? moment(text).format('DD MMM YYYY HH:mm:ss') : t('Never'),
    },
    {
      title: '',
      dataIndex: 'operation',
      key: 'operation',
      align: 'center',
      width: 80,
      render: function ConnectorOperation(text, record) {
        return (
          <Space size="small" className="actions">
            <Button
              style={{ width: 20, height: 20 }}
              size="small"
              ghost
              icon={<EditOutlined />}
              onClick={() => {
                form.setFieldsValue({
                  username: record.username,
                  password: record.password,
                  roles: record.roles && record.roles.map(it => it.name),
                  user_title: record.user_title,
                  fullname: record.fullname,
                  institution: record.institution,
                });
                setImageUrl(record.signature);
                setExpiredDate(record.expired_date);
                setEditItem(record);
                toggleAddAccountModal(true);
              }}
            />
            <Button
              style={{ width: 20, height: 20 }}
              size="small"
              ghost
              icon={<DeleteOutlined />}
              onClick={() => {
                Modal.confirm({
                  title: t('Do you want to deactivate this account?'),
                  // content: t('Data will be lost.'),
                  onOk: async () => {
                    await actionDeleteAccount(record.id);
                    fetchAccountList();
                  },
                });
              }}
            />
          </Space>
        );
      },
    },
  ];

  // const rowSelection = {
  //   selectedRowKeys,
  //   onChange: (rowKeys, selectedRows) => {
  //     setSelectedRowKeys(rowKeys);
  //   },
  // };

  useEffect(() => {
    fetchAccountList();
    fetchRoleList();
  }, []);

  const fetchRoleList = async () => {
    const [data] = await actionGetRoleList({});
    setRoleList(data);
  };

  const fetchAccountList = async (page = pagination.page) => {
    const [data, count] = await actionGetAccountList({
      offset: (page - 1) * pagination.size,
      limit: pagination.size,
    });
    setAccounts(data);
    setPagination({ ...pagination, total: count, page: page });
  };

  return (
    <div className="account-management">
      <div className="page-title">
        <Title level={4}>{t('Account Management')}</Title>
        {keycloak.hasOneOfPerm([
          SCOPES.organizationAdmin,
          SCOPES.globalAdmin,
          NEW_SCOPES.administration.user.create,
        ]) && (
          <Button
            size="small"
            onClick={() => {
              form.setFieldsValue({
                username: null,
                password: null,
                roles: [],
                user_title: null,
                institution: null,
              });
              setImageUrl('');
              setExpiredDate(null);
              setEditItem(null);
              toggleAddAccountModal(true);
            }}
            icon={<PlusOutlined />}
            type="primary"
          >
            {t('Add Account')}
          </Button>
        )}
      </div>
      <div className="content">
        <ConfigProvider
          renderEmpty={() => (
            <Empty
              image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
              description={t('No Data')}
            />
          )}
        >
          <Table
            size="small"
            columns={columns}
            dataSource={accounts}
            pagination={{
              showQuickJumper: true,
              showSizeChanger: true,
              size: 'small',
              current: pagination.page,
              pageSize: pagination.size,
              total: pagination.total,
              onChange: pageNumber => {
                fetchAccountList(pageNumber);
              },
              showTotal: total => `${t('Total')} ${total} ${t('items')}`,
              pageSizeOptions: ROWS_PER_PAGE,
            }}
            // rowSelection={rowSelection}
            rowKey="id"
          />
        </ConfigProvider>
      </div>
      <Modal
        className="vindr-modal add-institution"
        title={editItem && editItem.id ? t('Edit Account') : t('Add Account')}
        visible={openAddAccountModal}
        okText={t('Confirm')}
        cancelText={t('Cancel')}
        onOk={handleSave}
        okButtonProps={{
          disabled:
            (editItem &&
              editItem.id &&
              !keycloak.hasOneOfPerm([
                SCOPES.organizationAdmin,
                SCOPES.globalAdmin,
                NEW_SCOPES.administration.user.edit,
              ])) ||
            (!(editItem && editItem.id) &&
              !keycloak.hasOneOfPerm([
                SCOPES.organizationAdmin,
                SCOPES.globalAdmin,
                NEW_SCOPES.administration.user.create,
              ])),
        }}
        onCancel={() => toggleAddAccountModal(false)}
      >
        <Form
          layout="vertical"
          hideRequiredMark
          form={form}
          name="dynamic_form_nest_item"
          onFinish={handleSave}
          className="form-filter-condition"
        >
          <Form.Item
            name="username"
            label={t('Username') + ' *'}
            rules={[{ required: true, message: t('Required value') }]}
          >
            <Input
              placeholder={t('Enter username')}
              disabled={editItem && editItem.id}
            />
          </Form.Item>
          {!(editItem && editItem.id) && (
            <>
              <Form.Item
                name="password"
                label={t('Password') + ' *'}
                rules={[
                  { required: true, message: t('Required value') },
                  {
                    validator: (_, value) => {
                      if (!value) return Promise.resolve();
                      var strongRegex = new RegExp(
                        '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})'
                      );
                      return strongRegex.test(value)
                        ? Promise.resolve()
                        : Promise.reject(
                            new Error(t('Password complexity required'))
                          );
                    },
                  },
                ]}
              >
                <Input placeholder={t('Enter password')} type="password" />
              </Form.Item>
              <Form.Item
                name="confirm"
                label={t('Confirm Password') + ' *'}
                dependencies={['password']}
                rules={[
                  { required: true, message: t('Required value') },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        new Error(t('Passwords do not match'))
                      );
                    },
                  }),
                ]}
              >
                <Input placeholder={t('Enter the password')} type="password" />
              </Form.Item>
            </>
          )}

          <Form.Item
            name="roles"
            label={t('Roles')}
            rules={[{ required: true, message: t('Required value') }]}
          >
            <Select
              mode="multiple"
              placeholder={t('Select role')}
              dropdownClassName="vindr-dropdown dropdown-options-dark"
            >
              {roleList.map((it, idx) => (
                <Option key={idx} value={it.name}>
                  {it.display_name || it.name}
                </Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item name="fullname" label={t('Full Name')}>
            <Input placeholder={t('Enter name')} />
          </Form.Item>

          <Form.Item name="user_title" label={t('Title')}>
            <Input placeholder={t('Enter title')} />
          </Form.Item>
          {/*
          <Form.Item name="institution" label={t('Institution')}>
            <Select
              placeholder={t('Select institution')}
              dropdownClassName="vindr-dropdown dropdown-options-dark"
            >
              {INSTITUTIONS.map((it, idx) => (
                <Option key={idx} value={it.value}>
                  {it.value}
                </Option>
              ))}
            </Select>
          </Form.Item> */}

          <div style={{ paddingBottom: 8, color: '#ffffffd9' }}>
            {t('Signature')}
          </div>
          <Upload
            name="signature"
            listType="picture-card"
            className="signature-uploader"
            style={{ marginRight: 30 }}
            showUploadList={false}
            action={null}
            beforeUpload={file => {
              let reader = new FileReader();
              reader.onload = e => {
                setImageUrl(e.target.result);
              };
              reader.readAsDataURL(file);
            }}
            onChange={data => console.log(data)}
          >
            {imageUrl ? (
              <>
                <img
                  className="image"
                  src={imageUrl}
                  alt="signature"
                  style={{ width: '100%' }}
                />
                <div
                  className="overlay"
                  style={{
                    width: '100%',
                    height: '100%',
                    position: 'absolute',
                    display: 'none',
                    background: 'rgba(0,0,0,0.5)',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: 16,
                    transition: 'all 0.3s',
                  }}
                >
                  <Space size="middle">
                    <EditOutlined className="image-action" />
                    <DeleteOutlined
                      className="image-action"
                      onClick={e => {
                        e.preventDefault();
                        e.stopPropagation();
                        setImageUrl('');
                      }}
                    />
                  </Space>
                </div>
              </>
            ) : (
              <div
                className="signature-text"
                style={{ color: '#fff', opacity: 0.4 }}
              >
                <div style={{ marginTop: 8 }}>{t('Upload Signature')}</div>
                <CameraOutlined style={{ fontSize: 20 }} />
              </div>
            )}
          </Upload>

          <Form.Item
            label={t('Expired Date')}
            rules={[{ required: true, message: t('Required value') }]}
          >
            <DatePicker
              value={expiredDate ? moment(expiredDate) : expiredDate}
              onChange={date => {
                setExpiredDate(date.valueOf());
              }}
            />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default AccountManagement;
