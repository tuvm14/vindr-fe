import api from '../../../services/api';
import { getTenant } from '../../../utils/helper';
const tenant = getTenant();

export const actionGetAccountList = async (params = {}) => {
  const data = await api({
    url: `/om/${tenant}/userinfo`,
    method: 'GET',
    params,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve([data.data.data, data.data.count]);
};

export const actionCreateAccount = async (payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/userinfo`,
    method: 'POST',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};

export const actionUpdateAccount = async (id, payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/userinfo/${id}`,
    method: 'PUT',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};

export const actionDeleteAccount = async (id, payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/userinfo/${id}`,
    method: 'DELETE',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};
