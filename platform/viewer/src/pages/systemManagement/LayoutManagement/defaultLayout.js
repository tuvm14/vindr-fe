const defaultLayout = `
  <div style="margin-bottom: 12px">
    <div style="display: flex; justify-content: space-between; padding-bottom: 16px">
      <div style="flex: 1; text-align: center; color: #000; display: flex; flex-direction: column; justify-content: center;">
        <div style="text-align: center; min-width: 150px; margin-bottom: 5px; height: 78px; display: flex; justify-content: center;">{{{InstitutionLogo}}}</div>
        <div style="font-size: 12px; font-weight: 700; line-height: normal;">
          {{{InstitutionName}}}
        </div>
      </div>
      <div style="flex: 1; text-align: center; color: #000; display: flex; flex-direction: column; justify-content: center;">
        <div style="font-size: 18px; font-weight: 700; line-height: normal;">
          {{{InstitutionDepartment}}}
        </div>
        <div style="line-height: normal; font-style: italic;">Địa chỉ: {{{InstitutionAddress}}}</div>
        <div style="line-height: normal; font-style: italic;">Điện thoại: {{{InstitutionPhone}}}</div>
        <div style="line-height: normal; font-style: italic;">Email: {{{InstitutionEmail}}}</div>
      </div>
    </div>
    <h2 style="text-align:center; color: #000; font-weight: 700; font-size: 18px; line-height: normal;">THÔNG TIN CA CHỤP</h2>
  </div>
  <div style="margin-bottom: 12px">
    <table style="width:100%; font-size: 14px;" class="tabel-patient-info">
      <tr style="height: 24px">
        <td width="100px" style="padding: 2px 4px ;line-height: normal;">Họ và tên:</td>
        <td style="min-width: 130px; padding: 2px 4px;line-height: normal">
          <input style="width: 100%" class="tb-edit-text" value="{{{PatientName}}}" />
        </td>
        <td width="100px" style="padding: 2px 4px ;line-height: normal">Tuổi:</td>
        <td style="min-width: 130px; padding: 2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="{{{PatientAge}}}" /></td>
      </tr>
      <tr style="height: 24px">
        <td style="padding: 2px 4px;line-height: normal">Mã BN:</td>
        <td style="padding: 2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="{{{PatientId}}}" /></td>
        <td style="padding: 2px 4px;line-height: normal">Giới tính:</td>
        <td style="padding:2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="{{{PatientSex}}}" /></td>
      </tr>
      <tr style="height: 24px">
        <td style="padding: 2px 4px;line-height: normal">Địa chỉ:</td>
        <td colspan="3" style="padding: 2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="{{{PatientAddress}}}" /></td>
      </tr>
      <tr style="height: 24px">
        <td style="padding: 2px 4px;line-height: normal">Chỉ định:</td>
        <td style="padding: 2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="" /></td>
        <td style="padding: 2px 4px;line-height: normal">BS chỉ định:</td>
        <td style="padding: 2px 4px;line-height: normal"><input style="width: 100%" class="tb-edit-text" value="{{{ReferringPhysicianName}}}" /></td>
      </tr>
      <tr style="height: 24px">
        <td style="padding: 2px 4px;line-height: normal">Ngày chụp:</td>
        <td colspan="3" style="padding: 2px 4px;line-height: normal">
        <input style="width: 100%" class="tb-edit-text" value="{{{StudyDate}}}" />

        </td>
      </tr>
    </table>
    <h2 style="text-align:center; color: #000; font-weight: 700; padding-top: 16px; font-size: 18px; line-height: normal;">KẾT QUẢ</h2>
  </div>
  {{{report_content}}}
  <div style="display: flex; justify-content: space-between; margin-top: 16px; font-size: 14px;">
    <div>
      <div style="width: 100px; height: 100px;">
        {{{qr}}}
      </div>
      <p style="text-align: center;"><a href="{{{LinkViewport}}}">Xem ảnh</a></p>
    </div>
    <div style="text-align: center">
      <input style="min-width: 250px; text-align: center;" class="tb-edit-text" value="{{{location}}}, Ngày {{{todayDay}}} tháng {{{todayMonth}}} năm {{{todayYear}}}" />
      <div>Bác sĩ chuyên khoa</div>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <p style="font-weight: 500; line-height: normal;">{{{doctorName}}}</p>
    </div>
  </div>
`;

export default defaultLayout;
