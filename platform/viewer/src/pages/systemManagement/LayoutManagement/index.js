import React, { useEffect, useState } from 'react';
import {
  Button,
  ConfigProvider,
  Empty,
  Table,
  Typography,
  Modal,
  Space,
  Form,
  Input,
} from 'antd';
import Icon, {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import EmptyIcon from '../../../../public/assets/empty-img-simple.svg';
import {
  actionCreateLayout,
  actionDeleteLayout,
  actionGetLayoutList,
  actionUpdateLayout,
} from './actions';
import './LayoutManagement.styl';
import keycloak from '../../../services/auth';
import { NEW_SCOPES, ROWS_PER_PAGE, SCOPES } from '../../../utils/constants';
import { useTranslation } from 'react-i18next';
import TextEditor from './TextEditor';
import defaultLayout from './defaultLayout';

const { Title } = Typography;

function LayoutManagement() {
  const [institutions, setInstitution] = useState([]);
  // const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [layoutContent, setLayoutContent] = useState('');
  const [newLayoutContent, setNewLayoutContent] = useState('');
  const [openAddLayoutModal, toggleAddLayoutModal] = useState(false);
  const [editItem, setEditItem] = useState(null);
  const [pagination, setPagination] = useState({
    page: 1,
    size: ROWS_PER_PAGE[0],
    total: 0,
  });
  const { t } = useTranslation('Vindoc');

  const [form] = Form.useForm();

  const handleSave = () => {
    form
      .validateFields()
      .then(async values => {
        let result = null;
        if (editItem && editItem.id) {
          result = await actionUpdateLayout(editItem.id, {
            name: values.name,
            template_type: 'report-layout',
            value: newLayoutContent,
          });
        } else {
          result = await actionCreateLayout({
            name: values.name,
            template_type: 'report-layout',
            value: newLayoutContent,
          });
        }
        if (result) {
          toggleAddLayoutModal(false);
          fetchLayoutList();
        }
      })
      .catch(info => {
        console.log('Validate Failed:', info);
        Modal.error({
          title: 'Something went wrong',
          content: 'Please try again or contact admin for supports.',
        });
      });
  };

  const columns = [
    {
      title: '#',
      width: 40,
      align: 'center',
      dataIndex: 'id',
      key: 'id',
      render: (text, record, index) => index + 1,
    },
    {
      title: t('Name'),
      dataIndex: 'name',
      key: 'name',
      // width: '25%',
      sorter: true,
    },
    {
      title: '',
      dataIndex: 'operation',
      key: 'operation',
      align: 'center',
      width: 80,
      render: function ConnectorOperation(text, record) {
        return (
          <Space size="small" className="actions">
            <Button
              style={{ width: 20, height: 20 }}
              size="small"
              ghost
              icon={<EditOutlined />}
              onClick={() => {
                form.setFieldsValue({
                  name: record.name,
                });
                setLayoutContent(record.value);
                setNewLayoutContent(record.value);
                toggleAddLayoutModal(true);
                setEditItem(record);
              }}
            />
            {keycloak.hasOneOfPerm([
              SCOPES.organizationAdmin,
              SCOPES.globalAdmin,
              NEW_SCOPES.administration.report_layout.delete,
            ]) && (
              <Button
                style={{ width: 20, height: 20 }}
                size="small"
                ghost
                icon={<DeleteOutlined />}
                onClick={() => {
                  Modal.confirm({
                    title: t('Do you delete this item?'),
                    content: t('Data will be lost.'),
                    onOk: async () => {
                      await actionDeleteLayout(record.id);
                      fetchLayoutList();
                    },
                  });
                }}
              />
            )}
          </Space>
        );
      },
    },
  ];

  // const rowSelection = {
  //   selectedRowKeys,
  //   onChange: (rowKeys, selectedRows) => {
  //     setSelectedRowKeys(rowKeys);
  //   },
  // };

  useEffect(() => {
    fetchLayoutList();
  }, []);

  const fetchLayoutList = async (page = pagination.page) => {
    const [data, count] = await actionGetLayoutList({
      offset: (page - 1) * pagination.size,
      limit: pagination.size,
    });
    setInstitution(data);
    setPagination({ ...pagination, total: count, page: page });
  };

  return (
    <div className="institution">
      <div className="page-title">
        <Title level={4}>{t('Layout Management')}</Title>
        {keycloak.hasOneOfPerm([
          SCOPES.organizationAdmin,
          SCOPES.globalAdmin,
          NEW_SCOPES.administration.report_layout.create,
        ]) && (
          <Button
            size="small"
            onClick={() => {
              form.setFieldsValue({
                name: null,
                order: null,
                group: null,
              });
              setLayoutContent(defaultLayout);
              setNewLayoutContent(defaultLayout);
              setEditItem(null);
              toggleAddLayoutModal(true);
            }}
            icon={<PlusOutlined />}
            type="primary"
          >
            {t('Add Layout')}
          </Button>
        )}
      </div>
      <div className="content">
        <ConfigProvider
          renderEmpty={() => (
            <Empty
              image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
              description={t('No Data')}
            />
          )}
        >
          <Table
            size="small"
            columns={columns}
            dataSource={institutions}
            pagination={{
              showQuickJumper: true,
              showSizeChanger: true,
              size: 'small',
              current: pagination.page,
              pageSize: pagination.size,
              total: pagination.total,
              onChange: pageNumber => {
                fetchLayoutList(pageNumber);
              },
              showTotal: total => `${t('Total')} ${total} ${t('items')}`,
              pageSizeOptions: [10, 25, 50, 100],
            }}
            // rowSelection={rowSelection}
            rowKey="id"
          />
        </ConfigProvider>
      </div>
      <Modal
        className="vindr-modal add-institution"
        title={editItem && editItem.id ? t('Edit Layout') : t('Add Layout')}
        height={window.innerHeight < 700 ? window.innerHeight : 700}
        width={window.innerWidth < 800 ? window.innerWidth : 800}
        visible={openAddLayoutModal}
        okText={t('Confirm')}
        cancelText={t('Cancel')}
        onOk={handleSave}
        okButtonProps={{
          disabled:
            (editItem &&
              editItem.id &&
              !keycloak.hasOneOfPerm([
                SCOPES.organizationAdmin,
                SCOPES.globalAdmin,
                NEW_SCOPES.administration.report_layout.edit,
              ])) ||
            (!(editItem && editItem.id) &&
              !keycloak.hasOneOfPerm([
                SCOPES.organizationAdmin,
                SCOPES.globalAdmin,
                NEW_SCOPES.administration.report_layout.create,
              ])),
        }}
        onCancel={() => toggleAddLayoutModal(false)}
        destroyOnClose
      >
        <Form
          layout="vertical"
          hideRequiredMark
          form={form}
          name="dynamic_form_nest_item"
          onFinish={handleSave}
          className="form-filter-condition"
        >
          <Form.Item
            name="name"
            label={t('Layout Name') + ' *'}
            rules={[{ required: true, message: t('Required value') }]}
          >
            <Input placeholder={t('Enter Layout Name')} />
          </Form.Item>

          <Form.Item label={t('Content')}>
            <TextEditor
              content={layoutContent}
              onChange={value => setNewLayoutContent(value)}
            />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default LayoutManagement;
