import api from '../../../services/api';
import { getTenant } from '../../../utils/helper';
const tenant = getTenant();

export const actionGetLayoutList = async (params = {}) => {
  const data = await api({
    url: `/om/${tenant}/templates?type=report-layout`,
    method: 'GET',
    params,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve([data.data.data, data.data.count]);
};

export const actionCreateLayout = async (payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/templates`,
    method: 'POST',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};

export const actionUpdateLayout = async (id, payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/templates/${id}`,
    method: 'PUT',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};

export const actionDeleteLayout = async (id, payload = {}) => {
  const data = await api({
    url: `/om/${tenant}/templates/${id}`,
    method: 'DELETE',
    data: payload,
    timeout: 30000,
    // headers: { 'X-TENANT-ID': 'local' },
  });
  return Promise.resolve(data.data.data);
};
