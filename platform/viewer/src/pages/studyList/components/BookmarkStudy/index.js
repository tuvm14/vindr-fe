import React, { useState, useEffect } from 'react';
import { Modal, message, Select, Button } from 'antd';
import { CloseOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

import {
  actionBookmarkStudies,
  actionGetBookmarkList,
  actionUnbookmarkStudies,
} from '../../actions';
import './BookmarkStudy.styl';

const { Option } = Select;

function tagRender(props) {
  const {
    label,
    value,
    onClose,
    selectedStudies,
    bookmarks = [],
    setBookmarks,
    t,
  } = props;

  const onPreventMouseDown = event => {
    event.preventDefault();
    event.stopPropagation();
  };

  const handleUnbookmarkStudies = async event => {
    try {
      await actionUnbookmarkStudies({
        bookmark_id: value,
        studies: selectedStudies.map(it => it.StudyInstanceUID),
      });
      const filterData = bookmarks.filter(it => it !== value);
      setBookmarks(filterData);
      if (onClose) {
        onClose(event);
      }
    } catch (error) {
      message.error(t('Error Message'));
    }
  };

  return (
    <span className="ant-tag bookmark-tag" onMouseDown={onPreventMouseDown}>
      {label}
      <CloseOutlined
        onClick={handleUnbookmarkStudies}
        className="ant-tag-close-icon"
      />
    </span>
  );
}

const BookmarkStudy = props => {
  const { t } = useTranslation(['Vindoc']);
  const { selectedStudies = [], onCancel } = props;

  const [bookmarkOptions, setBookmarkOptions] = useState([]);
  const [bookmarks, setBookmarks] = useState([]);

  useEffect(() => {
    handleGetBookmark();
  }, []);

  const handleGetBookmark = async () => {
    try {
      const { data } = await actionGetBookmarkList();
      setBookmarkOptions(data || []);
    } catch (error) {
      console.log(error);
    }
  };

  const handleCancel = () => {
    if (onCancel) {
      onCancel();
    }
  };

  const handleOnChangeBookmark = values => {
    if (values.length > 0) {
      handleBookmarkStudy(values[values.length - 1]);
    }

    if (values.length > bookmarks.length) {
      setBookmarks(values);
    }
  };

  const handleBookmarkStudy = async bookmark_id => {
    try {
      await actionBookmarkStudies({
        bookmark_id,
        studies: selectedStudies.map(it => it.StudyInstanceUID),
      });
    } catch (error) {
      message.error(t('Error Message'));
    }
  };

  const handleFilterOption = (input, option) => {
    return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
  };

  return (
    <Modal
      title={t('Bookmark')}
      className="bookmark-study-modal"
      visible={true}
      onCancel={handleCancel}
      maskClosable={false}
      footer={[
        <Button key="back" onClick={handleCancel}>
          {t('Close')}
        </Button>,
      ]}
    >
      <div className="bookmark-study-content">
        <Select
          mode="multiple"
          allowClear={false}
          showArrow
          style={{ width: '100%' }}
          placeholder={t('Select bookmark')}
          onChange={handleOnChangeBookmark}
          value={bookmarks}
          optionFilterProp="children"
          filterOption={handleFilterOption}
          tagRender={tagProps =>
            tagRender({
              ...tagProps,
              selectedStudies,
              bookmarks,
              setBookmarks,
              t,
            })
          }
        >
          {bookmarkOptions.map(it => (
            <Option key={it.id_} value={it.id_}>
              {it.name || ''}
            </Option>
          ))}
        </Select>
      </div>
    </Modal>
  );
};

export default React.memo(BookmarkStudy, (prevProps, nextProps) => {
  return true;
});
