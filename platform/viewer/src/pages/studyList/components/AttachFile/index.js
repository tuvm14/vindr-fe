import React, { useState, useEffect } from 'react';
import { Modal, message, Spin, Button, Collapse, Image } from 'antd';
import { useTranslation } from 'react-i18next';
import {
  actionDeleteAttachFile,
  actionDownloadAttachFile,
  actionGetAttachFileList,
  actionGetAttachFileSrc,
} from '../../actions';
import './AttachFile.styl';
import { get } from 'lodash';
import { fileToDataURL, uuidv4 } from '../../../../utils/helper';
import Video from './UploadFile/Video';
import Icon, { FileOutlined } from '@ant-design/icons';
import PdfIcon from './UploadFile/pdf.svg';
import UploadFile from './UploadFile';
import keycloak from '../../../../services/auth';
import { NEW_SCOPES } from '../../../../utils/constants';

const { Panel } = Collapse;

const AttachFile = props => {
  const { t } = useTranslation(['Vindoc']);
  const { onCancel, study } = props;
  const [isProcessing, setIsProcessing] = useState(false);
  const [fileList, setFileList] = useState([]);
  const [selectedFiles, setSelectedFiles] = useState([]);
  const [openUploadFile, setOpenUploadFile] = useState(false);

  useEffect(() => {
    handleGetFileList();
  }, []);

  const handleGetFileList = async () => {
    try {
      setIsProcessing(true);
      const res = await actionGetAttachFileList(study.StudyInstanceUID);
      const data = get(res, 'data.data');
      if (data && data.images) {
        for (let i = 0; i < data.images.length; i++) {
          const uid = uuidv4();
          const fileSrc = await actionGetAttachFileSrc(data.images[i].resource);
          const src = await fileToDataURL(fileSrc.data);
          data.images[i]['id'] = uid;
          data.images[i]['src'] = src;
        }
        for (let i = 0; i < data.videos.length; i++) {
          const uid = uuidv4();
          const fileSrc = await actionGetAttachFileSrc(data.videos[i].resource);
          const src = await fileToDataURL(fileSrc.data);
          data.videos[i]['id'] = uid;
          data.videos[i]['src'] = src;
        }
        for (let i = 0; i < data.others.length; i++) {
          const uid = uuidv4();
          const fileSrc = await actionGetAttachFileSrc(data.others[i].resource);
          const src = await fileToDataURL(fileSrc.data);
          data.others[i]['id'] = uid;
          data.others[i]['src'] = src;
        }
      }
      setFileList(data || {});
      setIsProcessing(false);
    } catch (error) {
      console.log(error);
      setIsProcessing(false);
    }
  };

  const handleDownloadFile = async () => {
    try {
      if (!(study.StudyInstanceUID && selectedFiles && selectedFiles.length)) {
        return;
      }
      const res = await actionDownloadAttachFile(
        study.StudyInstanceUID,
        selectedFiles.map(it => it.resource)
      );
      const fileName = `${study.StudyInstanceUID}_attach.zip`;

      let anchor = document.createElement('a');
      const blob = new Blob([res.data]);
      const URLObj = window.URL || window.webkitURL;
      let objectUrl = URLObj.createObjectURL(blob);
      anchor.href = objectUrl;
      anchor.download = fileName;
      anchor.click();
      URLObj.revokeObjectURL(objectUrl);
    } catch (error) {
      message.error(t('Sytem error!'));
    }
  };

  const handleDeleteFile = async () => {
    try {
      if (!(study.StudyInstanceUID && selectedFiles && selectedFiles.length)) {
        return;
      }
      await actionDeleteAttachFile(
        study.StudyInstanceUID,
        selectedFiles.map(it => it.resource)
      );
      message.success(t('Success'));
      handleGetFileList();
    } catch (error) {
      message.error(t('Sytem error!'));
    }
  };

  const handleCancel = () => {
    if (onCancel) {
      onCancel();
    }
  };

  const addSelectedFile = file => {
    const found = selectedFiles.find(it => it.id == file.id);
    if (found) {
      setSelectedFiles(selectedFiles.filter(it => it.id != file.id));
    } else {
      setSelectedFiles([...selectedFiles, file]);
    }
  };

  const selectFile = file => {
    setSelectedFiles([file]);
  };

  return (
    <Modal
      title={t('Attachment File')}
      className="attach-file-modal"
      width="100vw"
      style={{ top: 48 }}
      visible={true}
      onCancel={handleCancel}
      maskClosable={false}
      cancelText={t('close')}
      okButtonProps={{ style: { display: 'none' } }}
    >
      <div className="button-group">
        {keycloak.hasOneOfPerm([NEW_SCOPES.worklist.attachmentfile.upload]) && (
          <Button type="primary" onClick={() => setOpenUploadFile(true)}>
            Upload file
          </Button>
        )}
        {keycloak.hasOneOfPerm([NEW_SCOPES.worklist.attachmentfile.download]) && (
          <Button
            type="primary"
            onClick={handleDownloadFile}
            disabled={!(selectedFiles && selectedFiles.length)}
          >
            Download file
          </Button>
        )}
        {keycloak.hasOneOfPerm([NEW_SCOPES.worklist.attachmentfile.delete]) && (
          <Button
            type="primary"
            danger
            onClick={handleDeleteFile}
            disabled={!(selectedFiles && selectedFiles.length)}
          >
            Delete file
          </Button>
        )}
      </div>
      <Spin spinning={isProcessing}>
        <div
          className="attach-file-content"
          style={{
            height: window.innerHeight < 700 ? 450 : window.innerHeight - 270,
          }}
        >
          <div className="form-item">
            <Collapse defaultActiveKey={['1']} ghost>
              <Panel header="Image" key="1">
                <div className="file-list">
                  {fileList &&
                    fileList.images &&
                    fileList.images.map((it, idx) => (
                      <div
                        className={`file ${
                          selectedFiles.find(file => file.id == it.id)
                            ? 'selected'
                            : ''
                        }`}
                        key={`${idx}_${it.filename}`}
                        onClick={e => {
                          if (e.ctrlKey) {
                            addSelectedFile(it);
                          } else {
                            selectFile(it);
                          }
                        }}
                      >
                        <Image
                          width={'100%'}
                          src={it.src}
                          alt={it.filename}
                          preview={false}
                          style={{ cursor: 'pointer' }}
                        />
                        <div className="file-name">{it.filename}</div>
                      </div>
                    ))}
                </div>
              </Panel>
            </Collapse>
          </div>
          <div className="form-item">
            <Collapse defaultActiveKey={['1']} ghost>
              <Panel header="Video" key="1">
                <div className="file-list">
                  {fileList &&
                    fileList.videos &&
                    fileList.videos.map((it, idx) => (
                      <div
                        className={`file ${
                          selectedFiles.find(file => file.id == it.id)
                            ? 'selected'
                            : ''
                        }`}
                        key={`${idx}_${it.filename}`}
                        onClick={e => {
                          if (e.ctrlKey) {
                            addSelectedFile(it);
                          } else {
                            selectFile(it);
                          }
                        }}
                      >
                        <Video
                          id="video"
                          width="100%"
                          height="100%"
                          // controls
                          src={it.src}
                        />
                        <div className="file-name">{it.filename}</div>
                      </div>
                    ))}
                </div>
              </Panel>
            </Collapse>
          </div>
          <div className="form-item">
            <Collapse defaultActiveKey={['1']} ghost>
              <Panel header="Other" key="1">
                <div className="file-list">
                  {fileList &&
                    fileList.others &&
                    fileList.others.map((it, idx) => (
                      <div
                        className={`file ${
                          selectedFiles.find(file => file.id == it.id)
                            ? 'selected'
                            : ''
                        }`}
                        key={`${idx}_${it.filename}`}
                        onClick={e => {
                          if (e.ctrlKey) {
                            addSelectedFile(it);
                          } else {
                            selectFile(it);
                          }
                        }}
                      >
                        <div
                          style={{
                            width: '100%',
                            paddingTop: '56.25%',
                            position: 'relative',
                            border: '1px solid #647a87',
                            borderRadius: 2,
                          }}
                        >
                          <div
                            style={{
                              position: 'absolute',
                              top: 0,
                              left: 0,
                              bottom: 0,
                              right: 0,
                              display: 'flex',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                          >
                            {it.mimetype.indexOf('pdf') >= 0 ? (
                              <Icon component={PdfIcon} style={{ fontSize: 24 }} />
                            ) : (
                              <div>
                                <FileOutlined style={{ marginRight: 10 }} />
                                {it.mimetype.toUpperCase()}
                              </div>
                            )}
                          </div>
                        </div>
                        <div className="file-name">{it.filename}</div>
                      </div>
                    ))}
                </div>
              </Panel>
            </Collapse>
          </div>
        </div>
      </Spin>
      <Modal
        className="vindr-modal create-study"
        title={t('Dicom Adapter')}
        width="100vw"
        style={{ top: 48 }}
        visible={openUploadFile}
        maskClosable={false}
        footer={null}
        // okText={t('Confirm')}
        // cancelText={t('Cancel')}
        // onOk={() => createStudyRef.current.uploadImage()}
        onCancel={() => setOpenUploadFile(false)}
      >
        <div
          style={{
            height: window.innerHeight < 700 ? 535 : window.innerHeight - 180,
          }}
        >
          <UploadFile
            studyInfo={study}
            onFinish={() => {
              setOpenUploadFile(false);
              handleGetFileList();
            }}
          />
        </div>
      </Modal>
    </Modal>
  );
};

export default React.memo(AttachFile, (prevProps, nextProps) => {
  return true;
});
