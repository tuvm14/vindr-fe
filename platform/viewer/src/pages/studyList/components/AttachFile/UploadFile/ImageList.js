import Icon, { DeleteOutlined, FileOutlined } from '@ant-design/icons';
import { Button, Col, Image, Row, Select, Upload } from 'antd';
import React, { useState } from 'react';
import CreateStudyContext from './CreateStudyContext';
import PdfIcon from './pdf.svg';
import Video from './Video';
const COLUMN_SETS = [1, 2, 3, 4];

const ImageList = props => {
  const { imageList } = props;
  const { removeImage, addFile } = React.useContext(CreateStudyContext);

  const [colCount, setColCount] = useState(3);
  const [images, setImages] = useState([]);

  // const cols = [];
  // for (let i = 0; i < colCount; i++) {
  //   cols.push(
  //     <Col key={i.toString()} span={24 / colCount}>
  //       <div>Column</div>
  //     </Col>
  //   );
  // }

  const handleAddImage = React.useCallback(
    files => {
      addFile(imageList, files);
    },
    [imageList]
  );

  React.useEffect(() => {
    setImages(
      imageList.map(it =>
        it.type == 'video' ? { ...it, src: URL.createObjectURL(it.src) } : it
      )
    );
  }, [imageList]);

  return (
    <div className="image-list" style={{ height: 'calc(100% - 88px)' }}>
      <div
        className="study-info-header"
        style={{ display: 'flex', justifyContent: 'space-between' }}
      >
        <Select
          style={{ width: 100, marginBottom: 10 }}
          size="small"
          value={colCount}
          onChange={value => setColCount(value)}
        >
          {COLUMN_SETS.map(it => (
            <Select.Option key={it} value={it}>
              {it} columns
            </Select.Option>
          ))}
        </Select>
        <Upload
          name="avatar"
          className="avatar-uploader"
          showUploadList={false}
          action={null}
          multiple
          beforeUpload={(file, fileList) => {
            console.log(fileList);
            handleAddImage(fileList);
            return false;
          }}
          onChange={data => console.log(data)}
        >
          <Button size="small">Import</Button>
        </Upload>
      </div>
      <div
        style={{
          display: 'flex',
          flexFlow: 'row wrap',
          padding: 10,
          border: '1px solid #647a87',
          borderRadius: 2,
          height: '100%',
          overflowY: 'scroll',
        }}
      >
        <Row
          gutter={[10, 10]}
          style={{ width: 'calc(100% + 10px)', alignContent: 'flex-start' }}
        >
          {images.map(it => (
            <Col
              key={it.id}
              span={24 / colCount}
              style={{ height: 'max-content' }}
            >
              <div className="image-option">
                {/* <Checkbox /> */}
                <DeleteOutlined
                  // style={{ marginLeft: 10 }}
                  className="image-action-delete"
                  onClick={e => {
                    e.preventDefault();
                    e.stopPropagation();
                    removeImage(imageList, it.id);
                  }}
                />
              </div>
              {it.type == 'image' && <Image width="100%" src={it.src} />}
              {it.type.indexOf('video') >= 0 && (
                <Video
                  id="video"
                  width="100%"
                  height="100%"
                  // controls
                  src={it.src}
                />
              )}
              {it.type != 'image' && it.type.indexOf('video') < 0 && (
                <div
                  style={{
                    width: '100%',
                    paddingTop: '56.25%',
                    position: 'relative',
                    border: '1px solid #647a87',
                    borderRadius: 2,
                  }}
                >
                  <div
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      bottom: 0,
                      right: 0,
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    {it.type == 'pdf' ? (
                      <Icon component={PdfIcon} style={{ fontSize: 24 }} />
                    ) : (
                      <div>
                        <FileOutlined style={{ marginRight: 10 }} />
                        {it.type.toUpperCase()}
                      </div>
                    )}
                  </div>
                </div>
              )}
            </Col>
          ))}
        </Row>
      </div>
    </div>
  );
};

export default React.memo(ImageList);
