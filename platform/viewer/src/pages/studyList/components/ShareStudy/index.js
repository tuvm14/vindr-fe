import React, { useState, useEffect } from 'react';
import { Modal, message, Select, Spin, Checkbox, Input, Button } from 'antd';
import { useTranslation } from 'react-i18next';
import { actionGetLinkViewport, actionShareStudies } from '../../actions';
import { actionGetAccountList } from '../../../systemManagement/AccountManagement/actions';
import './ShareStudy.styl';
import { CopyOutlined } from '@ant-design/icons';
import CopyToClipboard from 'react-copy-to-clipboard';

const { Option } = Select;

const ShareStudy = props => {
  const { t } = useTranslation(['Vindoc']);
  const { selectedStudies = [], onCancel, study } = props;
  const [isProcessing, setIsProcessing] = useState(false);
  const [userOptions, setUserOptions] = useState([]);
  const [users, setUsers] = useState([]);
  const [isShareAll, setShareAll] = useState(false);
  const [linkViewport, setLinkViewport] = useState('');

  useEffect(() => {
    handleGetAccountList();
    getLinkViewport(study.StudyInstanceUID);
  }, []);

  const getLinkViewport = async StudyInstanceUID => {
    try {
      const data = await actionGetLinkViewport(StudyInstanceUID);
      if (data && data.data && data.data.url) {
        setLinkViewport(data.data.url);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleGetAccountList = async () => {
    try {
      const [data] = await actionGetAccountList({
        offset: 0,
        limit: 999,
      });
      setUserOptions(data || []);
    } catch (error) {
      console.log(error);
    }
  };

  const handleCancel = () => {
    if (onCancel) {
      onCancel();
    }
  };

  const handleShareStudy = async () => {
    try {
      setIsProcessing(true);
      const payload = {
        studies: selectedStudies.map(it => it.StudyInstanceUID),
      };
      if (isShareAll) {
        payload.share_user_ids = ['all'];
      } else {
        payload.share_user_ids = users;
      }
      await actionShareStudies(payload);
      setIsProcessing(false);
      handleCancel();
    } catch (error) {
      message.error(t('Error Message'));
      setIsProcessing(false);
    }
  };

  const handleOnChange = values => {
    setUsers(values);
  };

  const handleFilterOption = (input, option) => {
    return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
  };

  return (
    <Modal
      title={t('Share')}
      className="share-study-modal"
      visible={true}
      onCancel={handleCancel}
      onOk={handleShareStudy}
      maskClosable={false}
      cancelText={t('Cancel')}
      okText={t('Share')}
      okButtonProps={{ disabled: !users.length && !isShareAll }}
    >
      <Spin spinning={isProcessing}>
        <div className="share-study-content">
          <div className="form-item">
            <div className="form-label">{t('Select specific users')}</div>
            <Select
              mode="multiple"
              allowClear={false}
              showArrow
              style={{ width: '100%' }}
              placeholder="Select users"
              onChange={handleOnChange}
              value={users}
              optionFilterProp="children"
              filterOption={handleFilterOption}
              disabled={isShareAll}
            >
              {userOptions.map(it => (
                <Option key={it.id} value={it.id}>
                  {it.username || ''}
                </Option>
              ))}
            </Select>
          </div>
          <div className="form-item">
            <Checkbox onChange={event => setShareAll(event.target.checked)}>
              {t('Share with all users')}
            </Checkbox>
          </div>
        </div>
      </Spin>
      {linkViewport && (
        <div
          style={{
            position: 'absolute',
            top: 265,
            padding: 24,
            width: 520,
            background: 'var(--box-bg-600)',
          }}
          className="share-study-content"
        >
          <div className="form-item">
            <div className="form-label">{t('Share with link')}</div>
            <Input.Group
              style={{ display: 'flex', justifyContent: 'space-between' }}
            >
              <Input
                style={{ width: 'calc(100% - 148px)' }}
                defaultValue={linkViewport}
              />
              <CopyToClipboard
                text={linkViewport}
                onCopy={(_text, result) => {
                  if (result) {
                    message.success(t('Copied to Clipboard'));
                  }
                }}
              >
                <Button style={{ width: 140 }} icon={<CopyOutlined />}>
                  {t('Copy URL')}
                </Button>
              </CopyToClipboard>
            </Input.Group>
          </div>
        </div>
      )}
    </Modal>
  );
};

export default React.memo(ShareStudy, (prevProps, nextProps) => {
  return true;
});
