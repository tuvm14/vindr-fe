import React, { useEffect, useState } from 'react';

import { Checkbox, Dropdown, Button, Row, Col } from 'antd';
import './CheckboxMenu.styl';
import { CaretDownOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

const CheckboxMenu = props => {
  const [selectedItems, setSelectedItems] = useState([]);
  const [showDropdown, setShowDropdown] = useState(false);
  const { t } = useTranslation('Vindoc');

  useEffect(() => {
    if (props.value && props.value.length) {
      setSelectedItems([...props.value]);
    } else {
      setSelectedItems([]);
    }
  }, [props.value]);

  const onChange = selection => {
    setSelectedItems([...selection]);
  };

  const CheckboxRender = () => {
    const height = props.height || 3;

    const groups = props.options
      .map(function(e, i) {
        return i % height === 0 ? props.options.slice(i, i + height) : null;
      })
      .filter(function(e) {
        return e;
      });

    return (
      <Checkbox.Group
        onChange={onChange}
        value={selectedItems}
        className="checkbox-menu"
      >
        <Row>
          {groups.map((group, i) => {
            return (
              <Col
                key={'checkbox-group-' + i}
                span={Math.floor(24 / groups.length)}
              >
                {group.map((label, i) => {
                  return (
                    <Checkbox
                      key={label}
                      value={label}
                      style={{ display: 'flex', margin: '0' }}
                    >
                      {t(label)}
                    </Checkbox>
                  );
                })}
              </Col>
            );
          })}
        </Row>
        <Row>
          <Col sm={12}>
            <Button
              size="small"
              onClick={() => onChange([])}
              style={{ marginTop: 10, width: '100%' }}
            >
              {t('Clear')}
            </Button>
          </Col>
          <Col sm={12}>
            <Button
              size="small"
              type="primary"
              onClick={() => {
                props.onChange(selectedItems);
                setShowDropdown(false);
              }}
              style={{ marginTop: 10, width: '100%' }}
            >
              {t('Apply')}
            </Button>
          </Col>
        </Row>
      </Checkbox.Group>
    );
  };

  return (
    <Dropdown
      overlay={<CheckboxRender />}
      trigger={['click']}
      visible={showDropdown}
      onVisibleChange={visible => {
        if (!visible) {
          // set to previous value if current not apply
          setSelectedItems([...props.value]);
        }
        setShowDropdown(visible);
      }}
    >
      <Button
        size={props.size}
        style={{
          color: 'rgba(255, 255, 255, 0.3)',
          width: '100%',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'flex-end',
        }}
      >
        <span
          style={{
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            color:
              selectedItems.length &&
              !(selectedItems.length == 1 && selectedItems[0] == '*')
                ? '#fff'
                : 'rgba(255, 255, 255, 0.3)',
          }}
        >
          {selectedItems &&
          selectedItems.length &&
          !(selectedItems.length == 1 && selectedItems[0] == '*')
            ? selectedItems.map(it => t(it)).join(', ')
            : t('Select')}
        </span>
        {props.suffixIcon || (
          <CaretDownOutlined style={{ color: 'rgba(255, 255, 255, 0.3)' }} />
        )}
      </Button>
    </Dropdown>
  );
};

export default CheckboxMenu;
