import React, { useState } from 'react';
import keycloak from '../../../../services/auth';
import { NEW_SCOPES, SCOPES } from '../../../../utils/constants';
import { Button } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import ImportDataModal from '../Uploader/ImportDataModal';
import './Header.css';
import { useWindowSize } from '@tuvm/ui/src/hooks/useWindowSize';
import MobileFolder from '../MobileStudiesContainer/MobileFolder';
import { isMobile } from '../../../../utils/helper';

export default function Header(props) {
  const [isUploaderActive, setUploadActive] = useState(false);
  const { t } = useTranslation('Vindoc');
  const [width, height] = useWindowSize();

  return (
    <div className="vindr-studylist-header">
      {!isMobile(width, height) && (
        <h2 className="page-title">
          {t('Performed Studies')}
          <span className="total-item">
            ({props.totalItem || 0} {t('Study_items')})
          </span>
        </h2>
      )}
      {isMobile(width, height) && <MobileFolder />}
      {keycloak.hasOneOfPerm([
        SCOPES.studyUpload,
        SCOPES.apiAll,
        NEW_SCOPES.worklist.exam.upload,
      ]) && (
        <Button
          className="batch-button"
          size={isMobile(width, height) ? 'middle' : 'small'}
          onClick={() => setUploadActive(true)}
          icon={<UploadOutlined />}
          ghost
          style={{ width: 90 }}
        >
          {t('Upload')}
        </Button>
      )}
      {isUploaderActive && (
        <ImportDataModal onCancel={() => setUploadActive(false)} />
      )}
    </div>
  );
}
