import React, { useState, useMemo } from 'react';
import { Modal, TreeSelect, Spin, message } from 'antd';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { actionCopyStudyToFolder } from '../../actions';
import './CopyToFolderModal.styl';
const { TreeNode } = TreeSelect;

const CopyToFolderModal = props => {
  const { t } = useTranslation(['Vindoc']);
  const [selectedFolder, setSelectedFolder] = useState(null);
  const [isProcessing, setIsProcessing] = useState(false);

  const { visible, selectedStudies = [], studylist = {}, onCancel } = props;
  const { panel } = studylist;

  const treeData = useMemo(() => {
    const { directory_filter = [] } = panel;
    return [
      {
        title: t('Public Directories'),
        key: 'ALL',
        selectable: false,
        children: directory_filter.map(dir => ({
          title: dir.name,
          key: dir.id_,
          selectable: false,
        })),
      },
    ];
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [panel]);

  const handleOk = async () => {
    if (isProcessing || !selectedFolder || !selectedStudies.length) {
      return;
    }
    setIsProcessing(true);
    try {
      await actionCopyStudyToFolder({
        dest_dir_ids: [selectedFolder],
        study_uids: selectedStudies.map(it => it.StudyInstanceUID),
      });
      setIsProcessing(false);
      message.success(t('Copied'));
      handleCancel();
    } catch (error) {
      setIsProcessing(false);
      message.error(t('Error Message'));
    }
  };

  const handleCancel = () => {
    if (onCancel) {
      onCancel();
    }
  };

  return (
    <Modal
      title={t('Copy to my folder')}
      className="copy-to-folder-modal"
      visible={visible}
      onOk={handleOk}
      onCancel={handleCancel}
      okText={t('Confirm')}
      cancelText={t('Cancel')}
      maskClosable={false}
      okButtonProps={{
        disabled: !selectedFolder,
      }}
    >
      <Spin spinning={isProcessing}>
        <div className="copy-to-folder-content">
          <div style={{ marginBottom: 12, color: 'rgba(255, 255, 255, 0.7)' }}>
            Choose a folder to which selected files will be copied.
          </div>
          <div style={{ marginBottom: 5 }}>Target Folder *</div>
          <TreeSelect
            value={selectedFolder}
            style={{ width: '100%' }}
            onChange={value => setSelectedFolder(value)}
            placeholder="Select"
            treeDefaultExpandAll
          >
            {treeData.map(groupItem => (
              <TreeNode
                selectable={false}
                className="folder-submenu"
                key={groupItem.key}
                value={groupItem.key}
                title={t(groupItem.title)}
              >
                {groupItem.children &&
                  groupItem.children.map(item => (
                    <TreeNode
                      key={item.key}
                      className="vindr-sub-menu-item"
                      title={t(item.title)}
                      value={item.key}
                    />
                  ))}
              </TreeNode>
            ))}
          </TreeSelect>
        </div>
      </Spin>
    </Modal>
  );
};

export default connect(
  state => ({ studylist: state.extensions?.studylist }),
  {}
)(
  React.memo(CopyToFolderModal, (prevProps, nextProps) => {
    return true;
  })
);
