import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Modal, Select } from 'antd';
import { DIAGNOSE_MODEL } from '../../utils/constants';
import './DiagnoseModal.styl';

const { Option } = Select;

const DiagnoseModal = props => {
  const { t } = useTranslation(['Vindoc']);
  const [model, setModel] = useState(DIAGNOSE_MODEL[0]);

  const handleOk = () => {
    if (props.onOk) props.onOk(model?.action);
  };

  const handleCancel = () => {
    if (props.onCancel) props.onCancel();
  };

  const handleChangeModel = value => {
    const tempModel = DIAGNOSE_MODEL.find(it => it.action === value) || {};
    setModel({ ...tempModel });
  };

  return (
    <Modal
      title={t('Analyze')}
      visible={true}
      onOk={handleOk}
      onCancel={handleCancel}
      className="diagnose-modal"
      okText={t('Done')}
      cancelText={t('Cancel')}
      width="425px"
    >
      <div className="diagnose-content">
        <div style={{ marginBottom: 12, color: 'rgba(255, 255, 255, 0.7)' }}>
          {t('Choose an appropriate Computer-Aided Diagnosis tool')}
        </div>
        <div style={{ marginBottom: 5 }}>{t('CAD Tool')} *</div>
        <Select
          style={{ width: '100%' }}
          onChange={handleChangeModel}
          value={model?.action}
        >
          {(DIAGNOSE_MODEL || []).map((item, idx) => (
            <Option value={item?.action} key={idx} disabled={!item?.isActive}>
              {t(item?.name || '')}
            </Option>
          ))}
        </Select>
      </div>
    </Modal>
  );
};

export default DiagnoseModal;
