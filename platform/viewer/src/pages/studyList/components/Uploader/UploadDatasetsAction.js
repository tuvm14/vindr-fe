import api from '../../../../services/api';

export const actionUploadDICOM = (data = {}, cancelToken) => {
  return api({
    method: 'POST',
    url: '/backend/upload/v2',
    data,
    redirect: 'follow',
    cancelToken: cancelToken,
  });
};
