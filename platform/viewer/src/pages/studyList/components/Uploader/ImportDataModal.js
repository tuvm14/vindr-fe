import React, { useState } from 'react';
import { Modal } from 'antd';
import { CloseOutlined, MinusOutlined, UpOutlined } from '@ant-design/icons';
import UploadDatasets from './index';

const ImportDataModal = props => {
  const { visible = true, onCancel } = props;
  const [processing, setProcessing] = useState(false);
  const [minimize, setMinimize] = useState(false);

  const handleCancel = () => {
    if (processing) return;
    if (onCancel) onCancel();
  };

  const handleMinimize = (isMinimize = false) => {
    setMinimize(isMinimize);
  };

  return (
    <Modal
      wrapClassName={`fixed-upload-modal ${minimize ? 'minimize-popup' : ''}`}
      visible={visible}
      className="common-modal import-data-modal"
      width={720}
      maskClosable={false}
      mask={!minimize}
      footer={null}
      closeIcon={
        <span className="btn-header">
          {minimize && (
            <>
              <UpOutlined
                className="btn-ic btn-action"
                onClick={() => handleMinimize()}
              />
              <CloseOutlined
                className="btn-ic btn-close"
                onClick={handleCancel}
              />
            </>
          )}
          {!minimize && (
            <>
              <MinusOutlined
                className="btn-ic btn-action"
                onClick={() => handleMinimize(true)}
              />
              <CloseOutlined
                className="btn-ic btn-close"
                onClick={handleCancel}
              />
            </>
          )}
        </span>
      }
    >
      <UploadDatasets
        onUploading={isUploading => {
          setProcessing(isUploading);
        }}
        isMinimize={minimize}
        onMinimize={() => handleMinimize(true)}
        onCancel={handleCancel}
      />
    </Modal>
  );
};

export default ImportDataModal;
