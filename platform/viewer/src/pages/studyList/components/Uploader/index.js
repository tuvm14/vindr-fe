import React, { useState, useEffect } from 'react';
import { Button, Progress, List } from 'antd';
import {
  UploadOutlined,
  FileOutlined,
  LoadingOutlined,
  CloseCircleOutlined,
  ExclamationCircleOutlined,
  CheckCircleOutlined,
} from '@ant-design/icons';
import axios from 'axios';
import moment from 'moment';
import { actionUploadDICOM } from './UploadDatasetsAction';
import Checkbox from 'antd/lib/checkbox/Checkbox';
import { useTranslation } from 'react-i18next';
import './Uploader.styl';

const { default: PQueue } = require('p-queue');
const queue = new PQueue({ concurrency: 4 });

const acceptType = '.dcm, .dicom';
const UPLOAD_STATUS = {
  NONE: 'none',
  UPLOADING: 'uploading',
  DONE: 'done',
  ERROR: 'error',
};

let subscribersCancel = [];

const UploadDatasets = props => {
  const { isMinimize = false, onCancel } = props;
  const { t } = useTranslation('Vindoc');
  const [fileList, setFileList] = useState([]);
  const [uploading, setUploading] = useState(false);
  const [processItem, setProcessItem] = useState(0);
  const [uploadItem, setUploadItem] = useState(0);
  const [failItem, setFailItem] = useState(0);
  const [startUpload, setStartUpload] = useState(false);
  const [isFinish, setIsFinish] = useState(false);
  const [showErrorItemOnly, setShowErrorItemOnly] = useState(false);

  useEffect(() => {
    return () => {
      subscribersCancel = [];
    };
  }, []);

  useEffect(() => {
    if (processItem >= fileList?.length && fileList?.length > 0) {
      setIsFinish(true);
      setUploading(false);
    }
    // eslint-disable-next-line
  }, [processItem]);

  useEffect(() => {
    if (props.onUploading) {
      props.onUploading(uploading);
    }
    // eslint-disable-next-line
  }, [uploading]);

  useEffect(() => {
    const dragBox = document.getElementById('dragBox');
    if (!dragBox) return;
    [
      'drag',
      'dragstart',
      'dragend',
      'dragover',
      'dragenter',
      'dragleave',
      'drop',
    ].forEach(function(event) {
      dragBox.addEventListener(event, function(e) {
        e.preventDefault();
        e.stopPropagation();
      });
    });
    ['dragover', 'dragenter'].forEach(function(event) {
      dragBox.addEventListener(event, function() {
        dragBox.classList.add('is-dragover');
      });
    });
    ['dragleave', 'dragend', 'drop'].forEach(function(event) {
      dragBox.addEventListener(event, function() {
        dragBox.classList.remove('is-dragover');
      });
    });
    dragBox.addEventListener('drop', function(e) {
      addFiles(e.dataTransfer.files, true);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onCLickDeleteFile = (file, idx) => {
    if (uploading) return;
    let tempFileList = [...fileList];
    tempFileList.splice(idx, 1);
    setFileList([...tempFileList]);
  };

  const handleUploadFile = () => {
    if (startUpload || uploading || fileList.length === 0) return;
    const CancelToken = axios.CancelToken;

    setStartUpload(true);
    setUploading(true);

    let fileListUpload = [...fileList];

    fileListUpload.forEach(async (file, idx) => {
      fileListUpload[idx].status = UPLOAD_STATUS.UPLOADING;
      setFileList([...fileListUpload]);
      const formData = new FormData();
      formData.append('file', file?.originFileObj);

      try {
        await queue.add(() =>
          actionUploadDICOM(
            formData,
            new CancelToken(function executor(c) {
              subscribersCancel.push(c);
            })
          )
        );

        fileListUpload[idx].status = UPLOAD_STATUS.DONE;
        setFileList([...fileListUpload]);
        setProcessItem(processItem => processItem + 1);
        setUploadItem(uploadItem => uploadItem + 1);
      } catch (error) {
        fileListUpload[idx].status = UPLOAD_STATUS.ERROR;
        fileListUpload[idx].error_msg = error?.data?.message || '';
        setFileList([...fileListUpload]);
        setProcessItem(processItem => processItem + 1);
        setFailItem(failItem => failItem + 1);
      }
    });
  };

  const handleRetryUploadFile = () => {
    if (failItem === 0 || uploading || fileList.length === 0) return;
    const CancelToken = axios.CancelToken;

    setUploading(true);

    let fileListUpload = [...fileList];
    const processCnt = fileListUpload.filter(
      it => it.status === UPLOAD_STATUS.DONE
    ).length;
    setProcessItem(processItem => processCnt);
    setFailItem(failItem => 0);

    fileListUpload.forEach(async (file, idx) => {
      if (file.status === UPLOAD_STATUS.ERROR) {
        fileListUpload[idx].status = UPLOAD_STATUS.UPLOADING;
        setFileList([...fileListUpload]);
        const formData = new FormData();
        formData.append('file', file?.originFileObj);

        try {
          await queue.add(() =>
            actionUploadDICOM(
              formData,
              new CancelToken(function executor(c) {
                subscribersCancel.push(c);
              })
            )
          );

          fileListUpload[idx].status = UPLOAD_STATUS.DONE;
          setFileList([...fileListUpload]);
          setProcessItem(processItem => processItem + 1);
          setUploadItem(uploadItem => uploadItem + 1);
        } catch (error) {
          fileListUpload[idx].status = UPLOAD_STATUS.ERROR;
          fileListUpload[idx].error_msg = error?.data?.message || '';
          setFileList([...fileListUpload]);
          setProcessItem(processItem => processItem + 1);
          setFailItem(failItem => failItem + 1);
        }
      }
    });
  };

  const handleStop = () => {
    if (subscribersCancel.length === 0) return;
    subscribersCancel.forEach(cancelRequest => {
      cancelRequest && cancelRequest();
    });
    subscribersCancel = [];
    queue.clear();
    setUploading(false);
  };

  const convertArrToCSV = (data = []) => {
    if (!data || !data.length) return;
    let result = '';
    result += `File name, Type, Error message\n`;
    data.forEach(item => {
      result += `${item?.name || ''}, ${item?.status ||
        ''}, ${item?.error_msg || ''}\n`;
    });

    return result;
  };

  const handleExportLog = () => {
    let csv = convertArrToCSV(fileList);
    if (!csv) return;

    const filename = `${moment().format('YYYY_MM_DD')}_upload_log.csv`;
    if (!csv.match(/^data:text\/csv/i)) {
      csv = 'data:text/csv;charset=utf-8,' + csv;
    }

    const data = encodeURI(csv);
    const link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();
  };

  const addFiles = (files = [], isSelectFile) => {
    const filesTemp = [];
    const filesArray = Array.from(files);
    filesArray.forEach((file, idx) => {
      filesTemp.push({
        fileId: idx,
        name: file.name,
        size: file.size,
        originFileObj: file,
      });
    });
    if (isSelectFile) {
      setFileList([...fileList, ...filesTemp]);
    } else {
      setFileList(filesTemp);
    }
  };

  const onChangeFile = (event, isSelectFile) => {
    if (!event?.target?.files?.length) return;
    addFiles(event.target.files, isSelectFile);
  };

  const onSelectFile = isSelectFolder => {
    if (isSelectFolder) {
      document.getElementById('folderId').click();
    } else {
      document.getElementById('fileId').click();
    }
  };

  const rowRenderer = (file, index) => {
    return (
      <div
        key={`${file.name}_${index}`}
        style={{
          display:
            showErrorItemOnly && file.status === UPLOAD_STATUS.DONE
              ? 'none'
              : 'block',
        }}
      >
        <div className="file-item">
          <span className="file-icon">
            {file.status !== UPLOAD_STATUS.UPLOADING && <FileOutlined />}
            {file.status === UPLOAD_STATUS.UPLOADING && <LoadingOutlined />}
          </span>
          <div className="name-wrapper">
            <div className="file-name">{file.name}</div>
            <div className="file-size">{`${Math.floor(
              file.originFileObj.size / 1024
            )} KB`}</div>
          </div>
          <span className={`file-status ${file.status}`}>
            {(!file.status ||
              file.status === UPLOAD_STATUS.NONE ||
              file.status === UPLOAD_STATUS.UPLOADING) && (
              <CloseCircleOutlined
                onClick={() => onCLickDeleteFile(file, index)}
              />
            )}
            {file.status === UPLOAD_STATUS.DONE && <CheckCircleOutlined />}
            {file.status === UPLOAD_STATUS.ERROR && (
              <ExclamationCircleOutlined />
            )}
          </span>
        </div>
      </div>
    );
  };

  return (
    <div className="import-data-content">
      <div className="header-modal">
        <span className="header-title">{t('Upload Files')}</span>
      </div>
      {startUpload && (
        <div className="progress-bar">
          <span className="uploaded-info">{`(${t('Processed')} ${processItem}/${
            fileList.length
          } ${t('files')}, ${uploadItem} ${t('success')}, ${failItem} ${t(
            'failed'
          )})`}</span>
          <Progress
            percent={
              fileList.length === 0
                ? 0
                : Math.floor((processItem / fileList.length) * 100)
            }
            status="active"
          />
        </div>
      )}
      <div className={`upload-container ${isMinimize ? 'minimized' : ''}`}>
        <input
          className="input-file"
          type="file"
          id="folderId"
          webkitdirectory="true"
          mozdirectory="true"
          directory="true"
          onChange={evt => onChangeFile(evt)}
          onClick={event => (event.target.value = null)}
        />
        <input
          className="input-file"
          type="file"
          multiple
          onChange={evt => onChangeFile(evt, true)}
          id="fileId"
          accept={acceptType}
          onClick={event => (event.target.value = null)}
        />
        <div
          className={`upload-drag-box ${
            fileList.length > 0 ? 'hide-drag-box' : ''
          }`}
        >
          <div className="drag-box" id="dragBox" onClick={() => onSelectFile()}>
            <div className="drag-content">
              <p className="ant-upload-text">
                {t('Drag your DICOM files here')}
              </p>
              <p className="ant-upload-drag-icon">
                <UploadOutlined />
              </p>
            </div>
          </div>
          {fileList.length > 0 && (
            <div className="file-list">
              <List
                width={580}
                height={400}
                dataSource={fileList}
                renderItem={rowRenderer}
              />
            </div>
          )}
        </div>
        <span style={{ marginTop: 12 }}>
          <Checkbox onChange={() => setShowErrorItemOnly(state => !state)}>
            {t('Show Error Item Only')}
          </Checkbox>
        </span>
      </div>
      <div className={`btn-action ${isMinimize ? 'minimized' : ''}`}>
        {!startUpload && (
          <>
            <div className="btn-wrap">
              <Button
                size="small"
                ghost
                disabled={uploading}
                className="btn btn-select-file"
                onClick={() => onSelectFile()}
              >
                {t('Select Files')}
              </Button>
              <Button
                size="small"
                ghost
                disabled={uploading}
                className="btn btn-select-file"
                onClick={() => onSelectFile(true)}
              >
                {t('Select Folder')}
              </Button>
            </div>
            <Button
              size="small"
              type="primary"
              loading={uploading}
              onClick={handleUploadFile}
              className="btn btn-primary"
            >
              {t('Upload')}
            </Button>
          </>
        )}
        {startUpload && !isFinish && (
          <>
            <Button
              size="small"
              ghost
              className="btn btn-primary"
              onClick={() => {
                if (props.onMinimize) {
                  props.onMinimize();
                }
              }}
            >
              {t('Minimize')}
            </Button>
            <Button
              size="small"
              className="btn btn-stop"
              danger
              onClick={handleStop}
            >
              {t('Cancel')}
            </Button>
          </>
        )}
        {startUpload && isFinish && (
          <>
            <div className="btn-wrap">
              {failItem > 0 && (
                <Button
                  size="small"
                  ghost
                  className="btn btn-primary btn-export-log"
                  onClick={handleRetryUploadFile}
                >
                  {t('Retry')}
                </Button>
              )}
              <Button
                size="small"
                ghost
                className="btn btn-primary btn-export-log"
                onClick={handleExportLog}
              >
                {t('Export Log')}
              </Button>
            </div>
            <Button
              size="small"
              type="primary"
              className="btn"
              onClick={onCancel}
            >
              {t('Close')}
            </Button>
          </>
        )}
      </div>
    </div>
  );
};

export default UploadDatasets;
