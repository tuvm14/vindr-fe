import { Modal, Tag } from 'antd';
import React from 'react';
import { Icon } from '@tuvm/ui/src/elements/Icon';
import { useTranslation } from 'react-i18next';
import './StudyItem.styl';
import { isEmpty } from 'lodash';

const isAINotSuport = study => {
  return !(study.IsAbnormal || []).length;
};

const isAbnormal = study => {
  if (Array.isArray(study.IsAbnormal)) {
    if (study.IsAbnormal.length > 0) {
      return study.IsAbnormal[0];
    } else {
      return false;
    }
  } else {
    return study.IsAbnormal;
  }
};

const isMatched = matched => {
  return matched && matched[0];
};

const isUnarchiving = study => {
  return (
    (study.ArchiveStatus &&
      study.ArchiveStatus.length &&
      study.ArchiveStatus[0] === 'UN_ARCHIVING') ||
    (study.InUnarchivingProgress &&
      study.InUnarchivingProgress.length &&
      study.InUnarchivingProgress[0])
  );
};

const isArchived = study => {
  return (
    (study.ArchiveStatus &&
      study.ArchiveStatus.length &&
      study.ArchiveStatus[0]) ||
    (study.IsArchived && study.IsArchived.length && study.IsArchived[0])
  );
};

const colors = {
  APPROVED: '#5e9148',
  NEW: '#5d6979',
  READING: '#c4b715',
  PENDING: '#b23b00',
  SUBMITTED: '#b23b00',
};

const StudyItem = ({
  study,
  relatedStudy,
  onSetRelatedStudy,
  onUnarchive,
  onDoubleClickRow,
  onContextMenu,
}) => {
  const { t } = useTranslation('Vindoc');
  const { Modality, Matched, VindocStatus, StudyDate } = study;
  const PatientName = isEmpty(study?.OrderInfo?.PatientName)
    ? isEmpty(study?.PatientName)
      ? '-'
      : `${study?.PatientName}`.replace(/\^/gi, ' ')
    : study?.OrderInfo?.PatientName;
  const PatientID = isEmpty(study?.OrderInfo?.PatientID)
    ? isEmpty(study?.PatientID)
      ? '-'
      : study?.PatientID
    : study?.OrderInfo?.PatientID;
  const PatientSex = isEmpty(study?.OrderInfo?.PatientSex)
    ? isEmpty(study?.PatientSex)
      ? '-'
      : t(`PatientSex_${study?.PatientSex}`)
    : t(`PatientSex_${study?.OrderInfo?.PatientSex}`);
  const PatientAge = isEmpty(study?.OrderInfo?.PatientAge)
    ? isEmpty(study?.PatientAge)
      ? '-'
      : study?.PatientAge
    : study?.OrderInfo?.PatientAge;
  const CadSupport = !isAINotSuport(study);
  const Abnormal = isAbnormal(study);
  const Match = isMatched(Matched);

  return (
    <div
      className={`study-item mobile ${relatedStudy == study ? 'selected' : ''}`}
      onAuxClick={event => {
        console.log('Aux click');
      }}
      onClick={event => {
        event.stopPropagation();
        event.preventDefault();
        console.log('On click');
        onSetRelatedStudy && onSetRelatedStudy(study);

        // To do: need to be fix for mobile
        if (relatedStudy == study) {
          if (isUnarchiving(study)) {
            Modal.info({
              className: 'vindr-modal',
              title: 'Processing unarchive',
              content: 'Please wait until the study is unarchived.',
            });
            return;
          }
          if (isArchived(study)) {
            onUnarchive && onUnarchive(study.StudyInstanceUID);
            return;
          }
          onDoubleClickRow && onDoubleClickRow(study);
        }
      }}
      onDoubleClick={event => {
        event.stopPropagation();
        event.preventDefault();
        if (event?._targetInst?.elementType === 'input') {
          // handle action doubleClick to checkbox
          return;
        }
        console.log('Double click');
      }}
      onContextMenu={event => {
        event.stopPropagation();
        event.preventDefault();
        console.log('Context click');
        onContextMenu && onContextMenu(study);
      }}
    >
      <div className="item-title">
        <div className="title-left">
          <span className="item-name">{PatientName || '-'}</span>
          {Modality && Modality[0] && (
            <span className="round">{Modality.join('').replace('SR', '')}</span>
          )}
          <span className="round">
            {Match ? (
              <Icon
                style={{ color: 'rgb(0, 172, 90)' }}
                name="link2"
                width="16px"
                height="16px"
              />
            ) : (
              <Icon name="linkOff" width="16px" height="16px" />
            )}
          </span>
          {CadSupport && (
            <span
              className="round"
              style={{ color: Abnormal ? '#ff4a4a' : '#00ac5a' }}
            >
              CAD
            </span>
          )}
        </div>
        <div className="status">
          <Tag
            color={colors[VindocStatus]}
            style={{ width: '100%', textAlign: 'center', lineHeight: '16px' }}
          >
            {t(VindocStatus)}
          </Tag>
        </div>
      </div>
      <div className="item-info">
        <div className="more-info">
          <span className="patient-id">{PatientID || '-'}</span>
          <span>&nbsp;{`| ${PatientSex || '-'} | ${PatientAge || '-'}`}</span>
        </div>
        <div className="study-date">{StudyDate}</div>
      </div>
    </div>
  );
};

export default StudyItem;
