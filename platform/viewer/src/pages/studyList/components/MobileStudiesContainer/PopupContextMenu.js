import React, { useState } from 'react';
// import { forEach } from 'lodash';
import {
  BarChartOutlined,
  EyeOutlined,
  BookOutlined,
  ShareAltOutlined,
  RetweetOutlined,
  SnippetsOutlined,
  FileZipOutlined,
} from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import keycloak from '../../../../services/auth';
import './PopupContextMenu.styl';
import {
  FEATURES,
  NEW_SCOPES,
  SCOPES,
  STUDY_LIST_ACTION_NAMES,
} from '../../../../utils/constants';
import { StudyListContext } from '../../utils/contexts';
import AppContext from '../../../../context/AppContext';
import BookmarkStudy from '../BookmarkStudy';
import ShareStudy from '../ShareStudy';
import DoctorModal from '../../../../components/Report/DoctorModal';
import { openViewer } from '../../actions';
import AttachFile from '../AttachFile';

const STUDY_LIST_ACTIONS = [
  {
    name: 'View',
    action: STUDY_LIST_ACTION_NAMES.VIEW,
    permissions: '*',
    icon: <EyeOutlined />,
  },
  {
    name: 'Compare Studies',
    action: STUDY_LIST_ACTION_NAMES.COMPARE,
    permissions: '*',
    icon: <RetweetOutlined />,
    isCompareItem: true,
  },
  {
    name: 'Bookmark',
    action: STUDY_LIST_ACTION_NAMES.BOOKMARK,
    permissions: '*',
    icon: <BookOutlined />,
  },
  {
    name: 'Analyze',
    action: STUDY_LIST_ACTION_NAMES.BATCH_AI_DIAGNOSIS,
    permissions: [
      SCOPES.aiDiagnose,
      SCOPES.apiAll,
      NEW_SCOPES.worklist.cad.analyze,
    ],
    icon: <BarChartOutlined />,
  },
  {
    name: 'Share',
    action: STUDY_LIST_ACTION_NAMES.SHARE,
    permissions: [SCOPES.sysdirAdmin, NEW_SCOPES.worklist.exam.share],
    feature: FEATURES.publicShare,
    icon: <ShareAltOutlined />,
  },
  {
    name: 'Open Report',
    action: STUDY_LIST_ACTION_NAMES.OPEN_REPORT,
    permissions: [SCOPES.reportView, NEW_SCOPES.worklist.report.view],
    icon: <SnippetsOutlined />,
  },
  {
    name: 'Attachment File',
    action: STUDY_LIST_ACTION_NAMES.ATTACH_FILE,
    permissions: [NEW_SCOPES.worklist.attachmentfile.view],
    icon: <FileZipOutlined />,
  },
];

const PopupContextMenu = ({ currentRecord, records, visible, x, y }) => {
  const selectedStudies = records;
  const {
    // relatedStudy: [relatedStudy, setRelatedStudy],
    batch: [, handleBatchAnalysis],
  } = React.useContext(StudyListContext);
  const { appConfig = {} } = React.useContext(AppContext);
  const [visibleBookmarkModal, setVisibleBookmarkModal] = useState(false);
  const [visibleShareModal, setVisibleShareModal] = useState(false);
  const [visibleReportModal, setVisibleReportModal] = useState(false);
  const [visibleAttachFileModal, setVisibleAttachFileModal] = useState(false);

  const handleBatch = () => {
    handleBatchAnalysis(selectedStudies);
  };

  const handleBookmarkStudy = () => {
    setVisibleBookmarkModal(true);
  };

  const handleView = () => {
    openViewer(
      appConfig.routerBasename,
      currentRecord?.StudyInstanceUID,
      currentRecord?.PatientID
    );
  };

  const handleCompareStudies = () => {
    const studyUIDs = (selectedStudies || [])
      .slice(0, 5)
      .map(it => it.StudyInstanceUID);
    if (studyUIDs.length > 0) {
      openViewer(appConfig.routerBasename, studyUIDs.join(';'));
    }
  };

  const handleSelectMenu = option => {
    switch (option.action) {
      case STUDY_LIST_ACTION_NAMES.VIEW:
        handleView();
        break;
      case STUDY_LIST_ACTION_NAMES.COMPARE:
        handleCompareStudies();
        break;
      case STUDY_LIST_ACTION_NAMES.BATCH_AI_DIAGNOSIS:
        handleBatch();
        break;
      case STUDY_LIST_ACTION_NAMES.BOOKMARK:
        handleBookmarkStudy();
        break;
      case STUDY_LIST_ACTION_NAMES.SHARE:
        setVisibleShareModal(true);
        break;
      case STUDY_LIST_ACTION_NAMES.OPEN_REPORT:
        setVisibleReportModal(true);
        break;
      case STUDY_LIST_ACTION_NAMES.ATTACH_FILE:
        setVisibleAttachFileModal(true);
        break;
    }
  };

  const { t } = useTranslation('Vindoc');
  return (
    <>
      {visible && (
        <>
          <div
            className="overlay-bg"
            style={{
              width: '100vw',
              height: '100vh',
              position: 'absolute',
              left: 0,
              top: 0,
              background: 'black',
              opacity: 0.5,
              zIndex: 1,
            }}
          ></div>
          <ul
            className="popup"
            style={{ left: `${x}px`, top: `${y}px`, zIndex: 2 }}
          >
            {STUDY_LIST_ACTIONS.map((option, index) => {
              const isShowMenu =
                (option.permissions === '*' ||
                  keycloak.hasOneOfPerm(option.permissions)) &&
                keycloak.hasFeature(option?.feature);
              if (isShowMenu) {
                if (
                  option.isCompareItem &&
                  (selectedStudies || []).length < 2
                ) {
                  return null;
                }
                return (
                  <li key={index} onClick={() => handleSelectMenu(option)}>
                    <span style={{ marginRight: 10 }}>{option.icon}</span>
                    {t(option.name)}
                  </li>
                );
              }
              return null;
            })}
          </ul>
        </>
      )}

      {visibleBookmarkModal && (
        <BookmarkStudy
          onCancel={() => setVisibleBookmarkModal(false)}
          selectedStudies={selectedStudies}
        />
      )}
      {visibleShareModal && (
        <ShareStudy
          onCancel={() => setVisibleShareModal(false)}
          study={currentRecord}
          selectedStudies={selectedStudies}
        />
      )}
      {visibleReportModal && (
        <DoctorModal
          onCancel={() => setVisibleReportModal(false)}
          study={currentRecord}
          isModalVisible={visibleReportModal}
        />
      )}
      {visibleAttachFileModal && (
        <AttachFile
          onCancel={() => setVisibleAttachFileModal(false)}
          study={currentRecord}
          isModalVisible={visibleAttachFileModal}
        />
      )}
    </>
  );
};

export default React.memo(PopupContextMenu);
