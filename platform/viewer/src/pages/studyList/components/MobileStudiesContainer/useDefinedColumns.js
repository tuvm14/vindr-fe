import React from 'react';
import { isEmpty } from 'lodash';
import { Tag } from 'antd';
import { useTranslation } from 'react-i18next';
import { getDateFormatted } from '../../../../utils/helper';
import moment from 'moment';
import { Icon, Tooltip } from '@tuvm/ui';

const isAINotSuport = study => {
  return !(study.IsAbnormal || []).length;
};

const isAbnormal = study => {
  if (Array.isArray(study.IsAbnormal)) {
    if (study.IsAbnormal.length > 0) {
      return study.IsAbnormal[0];
    } else {
      return false;
    }
  } else {
    return study.IsAbnormal;
  }
};

const isMatched = matched => {
  return matched && matched[0];
};

const useDefinedColumns = (sorter = true) => {
  const { t } = useTranslation(['Vindoc']);
  return [
    {
      title: '#',
      width: 40,
      align: 'center',
      dataIndex: 'PatientID',
      key: 'PatientID',
      render: (text, record, index) => index + 1,
    },
    {
      title: t('Patient Name'),
      width: 173,
      dataIndex: 'PatientName',
      key: 'PatientName',
      ellipsis: true,
      sorter: sorter,
      render: (PatientName, record) => (
        <span>
          {isEmpty(record?.OrderInfo?.PatientName)
            ? isEmpty(PatientName)
              ? '-'
              : `${PatientName}`.replace(/\^/gi, ' ')
            : record?.OrderInfo?.PatientName}
        </span>
      ),
    },
    {
      title: t('Status'),
      width: 93,
      dataIndex: 'VindocStatus',
      key: 'VindocStatus',
      ellipsis: true,
      sorter: sorter,
      className: 'status-column',
      render: VindocStatus => {
        const colors = {
          APPROVED: '#5e9148',
          NEW: '#5d6979',
          READING: '#c4b715',
          PENDING: '#b23b00',
          SUBMITTED: '#b23b00',
        };
        return (
          <Tag
            color={colors[VindocStatus]}
            style={{ width: '100%', textAlign: 'center', lineHeight: '16px' }}
          >
            {t(VindocStatus)}
          </Tag>
        );
      },
    },
    {
      title: t('HIS'),
      width: 42,
      dataIndex: 'Matched',
      key: 'Matched',
      align: 'center',
      render: matched => (
        <Tooltip
          title={isMatched(matched) ? t('Matched') : t('Unmatched')}
          placement="bottom"
          overlayClassName="toolbar-tooltip"
        >
          <span
            className="match-status"
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            {isMatched(matched) ? (
              <Icon
                style={{ color: 'rgb(0, 172, 90)' }}
                name="link2"
                width="16px"
                height="16px"
              />
            ) : (
              <Icon name="linkOff" width="16px" height="16px" />
            )}
          </span>
        </Tooltip>
      ),
    },
    {
      title: 'CAD',
      width: 42,
      align: 'center',
      dataIndex: 'PatientName',
      key: 'PatientName',
      render: (text, record) => (
        <Tooltip
          title={
            isAINotSuport(record) ? (
              t('Unavailable CAD')
            ) : (
              <>
                <span>{isAbnormal(record) ? t('Abnormal') : t('Normal')}</span>
                <span style={{ color: '#5D6979' }}>
                  {' '}
                  [{t('CAD Suggestion')}]
                </span>
              </>
            )
          }
          placement="bottom"
          overlayClassName="toolbar-tooltip"
        >
          <div
            className="dot-ai-result"
            style={{
              backgroundColor: isAINotSuport(record)
                ? 'rgba(148,148,148,0.3)'
                : isAbnormal(record)
                ? '#ff4a4a'
                : '#00ac5a',
            }}
          ></div>
        </Tooltip>
      ),
    },
    {
      title: t('Patient ID'),
      width: 110,
      dataIndex: 'PatientID',
      key: 'PatientID',
      ellipsis: true,
      sorter: sorter,
      render: (PatientID, record) => (
        <span>
          {isEmpty(record?.OrderInfo?.PatientID)
            ? isEmpty(PatientID)
              ? '-'
              : PatientID
            : record?.OrderInfo?.PatientID}
        </span>
      ),
    },
    {
      title: t('Accession #'),
      dataIndex: 'AccessionNumber',
      key: 'AccessionNumber',
      width: 140,
      ellipsis: true,
      sorter: sorter,
      render: (AccessionNumber, record) => (
        <span>
          {isEmpty(record?.OrderInfo?.AccessionNumber)
            ? isEmpty(AccessionNumber)
              ? '-'
              : AccessionNumber
            : record?.OrderInfo?.AccessionNumber}
        </span>
      ),
    },
    {
      title: t('Sex'),
      dataIndex: 'PatientSex',
      key: 'PatientSex',
      width: 72,
      align: 'center',
      sorter: sorter,
      render: (PatientSex, record) => (
        <span>
          {isEmpty(record?.OrderInfo?.PatientSex)
            ? isEmpty(PatientSex)
              ? '-'
              : t(`PatientSex_${PatientSex}`)
            : t(`PatientSex_${record?.OrderInfo?.PatientSex}`)}
        </span>
      ),
    },
    {
      title: t('Age'),
      dataIndex: 'PatientAge',
      key: 'PatientAge',
      width: 52,
      align: 'center',
      sorter: sorter,
      render: (PatientAge, record) => (
        <span>
          {isEmpty(record?.OrderInfo?.PatientAge)
            ? isEmpty(PatientAge)
              ? '-'
              : PatientAge
            : record?.OrderInfo?.PatientAge}
        </span>
      ),
    },
    {
      title: '#S/#I',
      dataIndex: 'seriesInstances',
      key: 'seriesInstances',
      width: 80,
      render: (_, record) => (
        <span>{`${record?.NumberOfStudyRelatedSeries ||
          ''}/${record?.NumberOfStudyRelatedInstances || ''}`}</span>
      ),
    },
    {
      title: t('Body Part'),
      dataIndex: 'BodyPartExamined',
      key: 'BodyPartExamined',
      width: 90,
      sorter: sorter,
      ellipsis: true,
    },
    {
      title: t('Modality'),
      dataIndex: 'Modality',
      key: 'Modality',
      width: 84,
      sorter: sorter,
      render: (Modality, record) => (
        <span>
          {isEmpty(record?.OrderInfo?.Modality)
            ? isEmpty(Modality)
              ? '-'
              : Modality
            : record?.OrderInfo?.Modality}
        </span>
      ),
    },
    {
      title: t('Study Date'),
      dataIndex: 'StudyDate',
      key: 'StudyDate',
      width: 140,
      sorter: sorter,
      render: (StudyDate, { StudyTime = '' }) => {
        return (
          <span>
            {StudyDate &&
              moment(StudyDate + StudyTime, 'YYYYMMDDHHmmss').format(
                `${getDateFormatted()} HH:mm:ss`
              )}
          </span>
        );
      },
    },
    {
      title: t('Referring Physician'),
      dataIndex: 'ReferringPhysicianName',
      key: 'ReferringPhysicianName',
      width: 140,
      ellipsis: true,
      render: (ReferringPhysicianName, record) => (
        <span>
          {isEmpty(record?.OrderInfo?.ReferringPhysician)
            ? isEmpty(ReferringPhysicianName)
              ? '-'
              : ReferringPhysicianName
            : record?.OrderInfo?.ReferringPhysician}
        </span>
      ),
    },
    {
      title: t('Exam Description'),
      dataIndex: 'StudyDescription',
      key: 'StudyDescription',
      width: 150,
      ellipsis: true,
      render: (StudyDescription, record) => (
        <span>
          {isEmpty(record?.OrderInfo?.ExamDescription)
            ? isEmpty(StudyDescription)
              ? '-'
              : StudyDescription
            : record?.OrderInfo?.ExamDescription}
        </span>
      ),
    },
  ];
};

export default useDefinedColumns;
