export const FILTER_TYPES = [
  'MediaStorageSOPClassUID',
  'MediaStorageSOPInstanceUID',
  'TransferSyntaxUID',
  'ImplementationClassUID',
  'SpecificCharacterSet',
  'SOPClassUID',
  'StudyDate',
  'SeriesDate',
  'AcquisitionDate',
  'ContentDate',
  'StudyTime',
  'SeriesTime',
  'AcquisitionTime',
  'ContentTime',
  'AccessionNumber',
  'Modality',
  'Manufacturer',
  'InstitutionName',
  'InstitutionAddress',
  'ReferringPhysicianName',
  'StationName',
  'StudyDescription',
  'SeriesDescription',
  'InstitutionalDepartmentName',
  'PhysiciansOfRecord',
  'ManufacturerModelName',
  'CodeMeaning',
  'PatientName',
  'PatientID',
  'PatientBirthDate',
  'PatientSex',
  'PatientAge',
  'BodyPartExamined',
  'DeviceSerialNumber',
  'ProtocolName',
  'ExposureTime',
  'ViewPosition',
  'RequestingPhysician',
  'RequestedProcedureDescription',
  'RequestedProcedureCodeSequence',
  'PerformedProcedureStepDescription',
  'RequestAttributesSequence',
  'RequestedProcedureCodeSequence',
];

export const getFilterTypes = () => {
  const data = FILTER_TYPES.sort();

  return data.map(item => {
    const label = item
      .replace(/(_|-)/g, ' ')
      .trim()
      .replace(/\w\S*/g, function(str) {
        return str.charAt(0).toUpperCase() + str.substr(1);
      })
      .replace(/([a-z])([A-Z])/g, '$1 $2')
      .replace(/([A-Z])([A-Z][a-z])/g, '$1 $2');
    return { label: label, value: item };
  });
};
