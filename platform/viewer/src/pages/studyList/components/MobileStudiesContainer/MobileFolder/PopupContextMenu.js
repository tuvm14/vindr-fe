import React, { useState } from 'react';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
import { useTranslation } from 'react-i18next';
import keycloak from '../../../../../services/auth';
import './PopupContextMenu.styl';
import CreateFolder from './CreateFolder';
import { actionRemoveBookmark } from '../../../actions';

const FOLDER_ACTIONS_NAME = {
  EDIT: 'EDIT',
  DELETE: 'DELETE',
};

const FOLDER_ACTIONS = [
  {
    name: 'Edit',
    action: FOLDER_ACTIONS_NAME.EDIT,
    permissions: '*',
    icon: <EditOutlined />,
  },
  {
    name: 'Delete',
    action: FOLDER_ACTIONS_NAME.DELETE,
    permissions: '*',
    icon: <DeleteOutlined />,
  },
];

const PopupContextMenu = props => {
  const { folder, visible, x, y, onRefreshData } = props;
  const { t } = useTranslation('Vindoc');
  const [isEditBookmark, setEditBookmark] = useState(false);
  const [deleteConfirm, setDeleteConfirm] = useState(false);

  const handleSelectMenu = option => {
    switch (option.action) {
      case FOLDER_ACTIONS_NAME.EDIT:
        setEditBookmark(true);
        break;
      case FOLDER_ACTIONS_NAME.DELETE:
        setDeleteConfirm(true);
        break;
    }
  };

  const handleDeleteBookmark = async () => {
    try {
      await actionRemoveBookmark({ bookmark_id: folder.id_ });
      if (onRefreshData) {
        onRefreshData(folder);
      }
      setDeleteConfirm(false);
      // eslint-disable-next-line no-empty
    } catch (error) {}
  };

  return (
    <>
      {visible && (
        <>
          <div
            className="overlay-bg"
            style={{
              width: '100vw',
              height: '100vh',
              position: 'absolute',
              left: 0,
              top: -48,
              background: 'black',
              opacity: 0.5,
              zIndex: 1051,
            }}
          ></div>
          <ul
            className="popup"
            style={{ left: `${x}px`, top: `${y - 48}px`, zIndex: 1052 }}
          >
            {FOLDER_ACTIONS.map((option, index) => {
              const isShowMenu =
                option.permissions === '*' ||
                keycloak.hasOneOfPerm(option.permissions);
              if (isShowMenu) {
                return (
                  <li key={index} onClick={() => handleSelectMenu(option)}>
                    <span style={{ marginRight: 10 }}>{option.icon}</span>
                    {t(option.name)}
                  </li>
                );
              }
              return null;
            })}
          </ul>
        </>
      )}
      {isEditBookmark && (
        <CreateFolder
          handleClose={() => setEditBookmark(false)}
          item={folder}
        />
      )}
      {deleteConfirm && (
        <Modal
          className="vindr-modal"
          title={t('Delete bookmark')}
          visible={deleteConfirm}
          okText={t('Delete')}
          okType="danger"
          cancelText={t('Cancel')}
          onOk={() => handleDeleteBookmark()}
          onCancel={() => setDeleteConfirm(false)}
          maskClosable={false}
        >
          {`Do you want to delete bookmark "${folder?.name}"? Your action cannot be
        undone.`}
        </Modal>
      )}
    </>
  );
};

export default React.memo(PopupContextMenu);
