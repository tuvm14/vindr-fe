import React, { useState, useEffect, useRef } from 'react';
import { Modal, Empty, ConfigProvider } from 'antd';
import querystring from 'querystring';
import { split, pick, isEmpty } from 'lodash';
import { useTranslation } from 'react-i18next';
import { toLuceneQueryString } from '../../../../utils/helper';
import AppContext from '../../../../context/AppContext';
import {
  actionGetStudyList,
  actionUnarchiveStudy,
  openViewer,
} from '../../actions';
import FilterData from './FilterData';
import { StudyListContext } from '../../utils/contexts';
import Icon from '@ant-design/icons';
import PopupContextMenu from './PopupContextMenu';
import { useLocation } from 'react-router';
import {
  FILTER_FIELDS,
  INITIAL_FILTERS,
  STUDY_DATETIME,
} from '../../utils/constants';
import {
  isValidModality,
  isValidSearchField,
  isValidSortField,
  isValidVindocStatus,
  isValidPage,
} from '../../utils/validators';
import EmptyIcon from '../../../../../public/assets/empty-img-simple.svg';
import DoctorModal from '../../../../components/Report/DoctorModal';
import StudyItem from './StudyItem';

let intervalFetchList;
const DEFAULT_LIMIT = 15;

const MobilePerformTable = (props, ref) => {
  const { t } = useTranslation(['Vindoc']);
  const { appConfig = {} } = React.useContext(AppContext);

  const [performedData, setPerformedData] = useState({});
  const [isFetching, setIsFetching] = useState(false);
  const [unarchivingStudyUID, setUnarchivingStudyUID] = useState(null);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [contextMenu, setContextMenu] = useState({
    visible: false,
    x: 0,
    y: 0,
  });
  const {
    selectedStudies: [selectedStudies, setSelectedStudies],
    relatedStudy: [relatedStudy, setRelatedStudy],
    paramsInfo: { params, dispatchParams },
    totalStudy: [totalStudy, setTotalStudy],
  } = React.useContext(StudyListContext);
  const studyRef = useRef();
  const [isInitParams, setIsInitParams] = useState(false);
  const [unarchiveConfirm, setUnarchiveConfirm] = useState(false);
  const [visibleReportModal, setVisibleReportModal] = useState(false);
  const [reportStudy, setReportStudy] = useState({});

  const locationSearch = useLocation().search;

  const query = React.useMemo(
    () => querystring.parse(split(locationSearch, '?')[1]),
    [locationSearch]
  );

  const matchParams = React.useMemo(() => pick(query, FILTER_FIELDS), [query]);
  const matchSearch = React.useMemo(
    () => pick(query, ['search', 'Directory', 'sort', 'limit', 'page']),
    [query]
  );

  useEffect(() => {
    if (!performedData || !performedData.data) {
      setRelatedStudy({});
      return;
    }
    const find = performedData.data.find(
      it => it.StudyInstanceUID == relatedStudy.StudyInstanceUID
    );
    if (!find) {
      setRelatedStudy({});
    }
  }, [performedData]);

  useEffect(() => {
    initParams();
    setIsInitParams(true);
    return () => {
      clearInterval(intervalFetchList);
    };
  }, []);

  useEffect(() => {
    resetSelectedItems();
    if (isInitParams) {
      handleFetchData(params);
      // clearInterval(intervalFetchList);

      // intervalFetchList = setInterval(() => {
      //   handleFetchData(params, true);
      // }, 10000);
    }
  }, [params]);

  const initParams = () => {
    const newParams = {
      ...params,
      ...matchParams,
      ...matchSearch,
    };

    newParams.offset = 0;
    newParams.limit = DEFAULT_LIMIT;

    // if (!newParams.offset) {
    //   newParams.offset = INITIAL_FILTERS.offset;
    // }
    // if (!isValidLimit(newParams.limit)) {
    //   newParams.limit = INITIAL_FILTERS.limit;
    // }
    if (!isValidPage(newParams.page)) {
      newParams.page = 0;
    }
    if (!isValidSortField(newParams.sort)) {
      newParams.sort = INITIAL_FILTERS.sort;
    }
    if (!isValidSearchField(newParams.searchField)) {
      newParams.searchField = INITIAL_FILTERS.searchField;
    }
    if (!isValidModality(newParams.ModalitiesInStudy)) {
      newParams.ModalitiesInStudy = INITIAL_FILTERS.ModalitiesInStudy;
    }
    if (!isValidVindocStatus(newParams.VindocStatus)) {
      newParams.VindocStatus = INITIAL_FILTERS.VindocStatus;
    }
    // history.replace(`?${querystring.stringify(newParams)}`);
    dispatchParams(newParams);
  };

  const handleFetchData = async (params, isSilent = false) => {
    try {
      if (!isSilent) {
        setIsFetching(true);
      }
      let queryStr = toQueryString(params);
      params.sort = (params.sort || `-${STUDY_DATETIME.STUDY_DATE}`)
        .replace(`,-${STUDY_DATETIME.STUDY_TIME}`, '')
        .replace(`,${STUDY_DATETIME.STUDY_TIME}`, '');
      if (params.sort === STUDY_DATETIME.STUDY_DATE) {
        params.sort = params.sort + ',' + STUDY_DATETIME.STUDY_TIME;
      } else if (params.sort == `-${STUDY_DATETIME.STUDY_DATE}`) {
        params.sort = params.sort + ',-' + STUDY_DATETIME.STUDY_TIME;
      }
      const { data } = await actionGetStudyList({
        ...params,
        query_string: queryStr,
        offset: parseInt(params.offset) * parseInt(params.limit),
      });
      const currentData = (performedData.data || []).slice(
        0,
        parseInt(params.offset) * parseInt(params.limit)
      );
      console.log(currentData.length, data?.records.length);
      const tempPerformedData = {
        count: data?.count || 0,
        data: currentData.concat(data?.records || []),
      };
      setTotalStudy(tempPerformedData.count);
      setPerformedData(tempPerformedData);
      if (!isSilent) {
        setIsFetching(false);
      }
    } catch (error) {
      console.log(error);
      if (!isSilent) {
        setIsFetching(false);
      }
    }
  };

  const handleDoubleClickRow = (record = {}) => {
    openViewer(
      appConfig.routerBasename,
      record.StudyInstanceUID,
      record.PatientID
    );
  };

  const resetSelectedItems = () => {
    setSelectedRowKeys([]);
    setSelectedStudies([]);
  };

  const handleUnarchive = studyUID => {
    setUnarchivingStudyUID(studyUID);
    setUnarchiveConfirm(true);
  };

  const unarchiveStudy = async studyInstanceUID => {
    const input = { study_instance_uid: studyInstanceUID };
    setUnarchiveConfirm(false);
    const { data = '' } = await actionUnarchiveStudy(input);
    if (data === 'Processing unarchive') {
      handleFetchData(params);
      Modal.info({
        className: 'vindr-modal',
        title: 'Processing unarchive',
        content: 'Please wait until the study is unarchived.',
      });
    } else {
      Modal.error({
        className: 'vindr-modal',
        title: 'Something went wrong',
        content: 'Please try again or contact admin for supports.',
      });
    }
  };

  const toQueryString = params => {
    let transformQuery = {
      StudyDate: `[${params.startDate} TO ${params.endDate}]`,
    };
    let searchAll = null;
    if (params?.IsAbnormal && params?.IsAbnormal !== '*') {
      transformQuery.IsAbnormal = params?.IsAbnormal;
    }
    if (params?.ModalitiesInStudy && params?.ModalitiesInStudy !== '*') {
      transformQuery.ModalitiesInStudy = params?.ModalitiesInStudy;
    }
    if (isValidVindocStatus(params.VindocStatus)) {
      transformQuery.VindocStatus = params.VindocStatus;
    }
    if (params.search && params.search.length) {
      params.search.forEach(it => {
        if (it.key == 'All') {
          searchAll = it.value;
        } else {
          transformQuery[it.key] = it.value;
        }
      });
    }
    let queryStr = toLuceneQueryString(transformQuery, ' AND ', ':');
    if (searchAll) {
      queryStr = `*${searchAll} AND ${queryStr}`;
    }
    if (params.Directory && params.Directory !== '*') {
      queryStr += ` AND ${params.Directory}`;
    }
    return queryStr;
  };

  const onScroll = () => {
    if (studyRef && studyRef.current) {
      const { scrollTop, scrollHeight, clientHeight } = studyRef.current;
      if (scrollTop + clientHeight > scrollHeight - 100) {
        if (performedData?.data && performedData?.data.length < totalStudy) {
          handleFetchData(
            {
              ...params,
              offset: performedData?.data.length / DEFAULT_LIMIT,
              limit: DEFAULT_LIMIT,
            },
            true
          );
        }
      }
    }
  };

  return (
    <div className="perform-table-content">
      <div className="table-header">
        <FilterData onChange={data => dispatchParams({ ...data, offset: 0 })} />
      </div>

      <div className="table-content">
        <ConfigProvider
          renderEmpty={() => (
            <Empty
              image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
              description={t('No Data')}
            />
          )}
        >
          <div className="mobile-table" ref={studyRef} onScroll={onScroll}>
            {(performedData?.data || []).map(it => (
              <StudyItem
                key={it.StudyInstanceUID}
                study={it}
                relatedStudy={relatedStudy}
                onSetRelatedStudy={study => setRelatedStudy(study)}
                onUnarchive={studyUID => handleUnarchive(studyUID)}
                onDoubleClickRow={study => handleDoubleClickRow(study)}
                onContextMenu={record => {
                  let contextStudies = [record];
                  if (
                    selectedStudies.find(
                      it => it.StudyInstanceUID == record.StudyInstanceUID
                    )
                  ) {
                    contextStudies = selectedStudies;
                  }

                  if (!contextMenu.visible) {
                    document.addEventListener(
                      `click`,
                      function onClickOutside() {
                        setContextMenu({
                          visible: false,
                          currentRecord: record,
                          records: contextStudies,
                        });
                        document.removeEventListener(`click`, onClickOutside);
                      }
                    );
                  }
                  setContextMenu({
                    currentRecord: record,
                    records: contextStudies,
                    visible: true,
                    x: event.clientX,
                    y: event.clientY,
                  });
                }}
              />
            ))}
            {isEmpty(performedData?.data) && (
              <Empty
                image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
                description={t('No Data')}
              />
            )}
          </div>
        </ConfigProvider>
        <PopupContextMenu {...contextMenu} />
        {unarchiveConfirm && (
          <Modal
            className="vindr-modal"
            title={t('Study is archived')}
            visible={unarchiveConfirm}
            okText={t('Agree')}
            cancelText={t('Disagree')}
            onOk={() => unarchiveStudy(unarchivingStudyUID)}
            onCancel={() => setUnarchiveConfirm(false)}
          >
            {t('Do you want to unarchive the study?')}
          </Modal>
        )}
        {visibleReportModal && (
          <DoctorModal
            onCancel={() => setVisibleReportModal(false)}
            study={reportStudy}
            isModalVisible={visibleReportModal}
          />
        )}
      </div>
    </div>
  );
};

export default MobilePerformTable;
