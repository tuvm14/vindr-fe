import React, { useState, useEffect } from 'react';
import { Table, Modal, Empty, ConfigProvider, Checkbox } from 'antd';
import { Tooltip } from '@tuvm/ui';
import querystring from 'querystring';
import { split, pick } from 'lodash';
import { useTranslation } from 'react-i18next';
import { isMobile, toLuceneQueryString } from '../../../../utils/helper';
import AppContext from '../../../../context/AppContext';
import {
  actionGetStudyList,
  actionUnarchiveStudy,
  openViewer,
} from '../../actions';
import PaginationTable from './Pagination';
import FilterData from './FilterData';
import { StudyListContext } from '../../utils/contexts';
import useDefinedColumns from './useDefinedColumns';
import Icon, { LoadingOutlined } from '@ant-design/icons';
import PopupContextMenu from './PopupContextMenu';
import { useLocation } from 'react-router';
import {
  FILTER_FIELDS,
  INITIAL_FILTERS,
  STUDY_DATETIME,
} from '../../utils/constants';
import {
  isValidModality,
  isValidSearchField,
  isValidSortField,
  isValidVindocStatus,
  isValidLimit,
  isValidPage,
} from '../../utils/validators';
import EmptyIcon from '../../../../../public/assets/empty-img-simple.svg';
import DoctorModal from '../../../../components/Report/DoctorModal';

let intervalFetchList;

const PerformTable = (props, ref) => {
  const { t } = useTranslation(['Vindoc']);
  const { appConfig = {} } = React.useContext(AppContext);

  const [performedData, setPerformedData] = useState({});
  const [isFetching, setIsFetching] = useState(false);
  const [unarchivingStudyUID, setUnarchivingStudyUID] = useState(null);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [contextMenu, setContextMenu] = useState({
    visible: false,
    x: 0,
    y: 0,
  });
  const {
    selectedStudies: [selectedStudies, setSelectedStudies],
    relatedStudy: [relatedStudy, setRelatedStudy],
    paramsInfo: { params, dispatchParams },
    totalStudy: [, setTotalStudy],
  } = React.useContext(StudyListContext);
  const performedColumns = useDefinedColumns();
  const [isInitParams, setIsInitParams] = useState(false);
  const [unarchiveConfirm, setUnarchiveConfirm] = useState(false);
  const [visibleReportModal, setVisibleReportModal] = useState(false);
  const [reportStudy, setReportStudy] = useState({});

  const locationSearch = useLocation().search;

  const query = React.useMemo(
    () => querystring.parse(split(locationSearch, '?')[1]),
    [locationSearch]
  );

  const matchParams = React.useMemo(() => pick(query, FILTER_FIELDS), [query]);
  const matchSearch = React.useMemo(
    () => pick(query, ['search', 'Directory', 'sort', 'limit', 'page']),
    [query]
  );

  useEffect(() => {
    if (!performedData || !performedData.data) {
      setRelatedStudy({});
      return;
    }
    const find = performedData.data.find(
      it => it.StudyInstanceUID == relatedStudy.StudyInstanceUID
    );
    if (!find) {
      setRelatedStudy({});
    }
  }, [performedData]);

  useEffect(() => {
    initParams();
    setIsInitParams(true);
    return () => {
      clearInterval(intervalFetchList);
    };
  }, []);

  useEffect(() => {
    resetSelectedItems();
    if (isInitParams) {
      handleFetchData(params);
      clearInterval(intervalFetchList);

      intervalFetchList = setInterval(() => {
        handleFetchData(params, true);
      }, 10000);
    }
  }, [params]);

  const initParams = () => {
    const newParams = {
      ...params,
      ...matchParams,
      ...matchSearch,
    };

    if (!newParams.offset) {
      newParams.offset = INITIAL_FILTERS.offset;
    }
    if (!isValidLimit(newParams.limit)) {
      newParams.limit = INITIAL_FILTERS.limit;
    }
    if (!isValidPage(newParams.page)) {
      newParams.page = 0;
    }
    if (!isValidSortField(newParams.sort)) {
      newParams.sort = INITIAL_FILTERS.sort;
    }
    if (!isValidSearchField(newParams.searchField)) {
      newParams.searchField = INITIAL_FILTERS.searchField;
    }
    if (!isValidModality(newParams.ModalitiesInStudy)) {
      newParams.ModalitiesInStudy = INITIAL_FILTERS.ModalitiesInStudy;
    }
    if (!isValidVindocStatus(newParams.VindocStatus)) {
      newParams.VindocStatus = INITIAL_FILTERS.VindocStatus;
    }
    // history.replace(`?${querystring.stringify(newParams)}`);
    dispatchParams(newParams);
  };

  const handleFetchData = async (params, isSilent = false) => {
    try {
      if (!isSilent) {
        setIsFetching(true);
      }
      let queryStr = toQueryString(params);
      params.sort = (params.sort || `-${STUDY_DATETIME.STUDY_DATE}`)
        .replace(`,-${STUDY_DATETIME.STUDY_TIME}`, '')
        .replace(`,${STUDY_DATETIME.STUDY_TIME}`, '');
      if (params.sort === STUDY_DATETIME.STUDY_DATE) {
        params.sort = params.sort + ',' + STUDY_DATETIME.STUDY_TIME;
      } else if (params.sort == `-${STUDY_DATETIME.STUDY_DATE}`) {
        params.sort = params.sort + ',-' + STUDY_DATETIME.STUDY_TIME;
      }
      const { data } = await actionGetStudyList({
        ...params,
        query_string: queryStr,
        offset: parseInt(params.offset) * parseInt(params.limit),
      });
      const tempPerformedData = {
        count: data?.count || 0,
        data: data?.records || [],
      };
      setTotalStudy(tempPerformedData.count);
      setPerformedData(tempPerformedData);
      if (!isSilent) {
        setIsFetching(false);
      }
    } catch (error) {
      console.log(error);
      if (!isSilent) {
        setIsFetching(false);
      }
    }
  };

  const handleDoubleClickRow = (record = {}) => {
    openViewer(
      appConfig.routerBasename,
      record.StudyInstanceUID,
      record.PatientID
    );
  };

  const resetSelectedItems = () => {
    setSelectedRowKeys([]);
    setSelectedStudies([]);
  };

  const isUnarchiving = study => {
    return (
      (study.ArchiveStatus &&
        study.ArchiveStatus.length &&
        study.ArchiveStatus[0] === 'UN_ARCHIVING') ||
      (study.InUnarchivingProgress &&
        study.InUnarchivingProgress.length &&
        study.InUnarchivingProgress[0])
    );
  };

  const isArchived = study => {
    return (
      (study.ArchiveStatus &&
        study.ArchiveStatus.length &&
        study.ArchiveStatus[0]) ||
      (study.IsArchived && study.IsArchived.length && study.IsArchived[0])
    );
  };

  const handleUnarchive = studyUID => {
    setUnarchivingStudyUID(studyUID);
    setUnarchiveConfirm(true);
  };

  const unarchiveStudy = async studyInstanceUID => {
    const input = { study_instance_uid: studyInstanceUID };
    setUnarchiveConfirm(false);
    const { data = '' } = await actionUnarchiveStudy(input);
    if (data === 'Processing unarchive') {
      handleFetchData(params);
      Modal.info({
        className: 'vindr-modal',
        title: 'Processing unarchive',
        content: 'Please wait until the study is unarchived.',
      });
    } else {
      Modal.error({
        className: 'vindr-modal',
        title: 'Something went wrong',
        content: 'Please try again or contact admin for supports.',
      });
    }
  };

  const rowSelection = {
    selectedRowKeys,
    renderCell: (checked, record, index, originNode) =>
      isUnarchiving(record) ? (
        <Tooltip title={t('Unarchiving')}>
          <LoadingOutlined style={{ cursor: 'pointer', color: '#39C2D7' }} />
        </Tooltip>
      ) : isArchived(record) ? (
        <Checkbox
          checked={false}
          onClick={() => {
            handleUnarchive(record.StudyInstanceUID);
          }}
        />
      ) : (
        // <Tooltip title={t('Archived')}>
        //   <ExclamationCircleOutlined
        //     style={{ cursor: 'pointer', color: '#ff4a4a' }}
        //     onClick={() => {
        //       handleUnarchive(record.StudyInstanceUID);
        //     }}
        //   />
        // </Tooltip>
        originNode
      ),
    onChange: (rowKeys, selectedRows) => {
      setSelectedRowKeys(rowKeys);
      setSelectedStudies(selectedRows || []);
    },
  };

  const toQueryString = params => {
    let transformQuery = {
      StudyDate: `[${params.startDate} TO ${params.endDate}]`,
    };
    let searchAll = null;
    if (params?.IsAbnormal && params?.IsAbnormal !== '*') {
      transformQuery.IsAbnormal = params?.IsAbnormal;
    }
    if (params?.ModalitiesInStudy && params?.ModalitiesInStudy !== '*') {
      transformQuery.ModalitiesInStudy = params?.ModalitiesInStudy;
    }
    if (isValidVindocStatus(params.VindocStatus)) {
      transformQuery.VindocStatus = params.VindocStatus;
    }
    if (params.search && params.search.length) {
      params.search.forEach(it => {
        if (it.key == 'All') {
          searchAll = it.value;
        } else {
          transformQuery[it.key] = it.value;
        }
      });
    }
    let queryStr = toLuceneQueryString(transformQuery, ' AND ', ':');
    if (searchAll) {
      queryStr = `*${searchAll} AND ${queryStr}`;
    }
    if (params.Directory && params.Directory !== '*') {
      queryStr += ` AND ${params.Directory}`;
    }
    return queryStr;
  };

  const handleOnChangeTable = (_, __, sorter) => {
    let sort;
    if (sorter?.order === 'ascend') {
      sort = `${sorter.field}`;
    } else if (sorter?.order === 'descend') {
      sort = `-${sorter.field}`;
    }
    dispatchParams({ ...params, sort });
    resetSelectedItems();
  };

  return (
    <div className="perform-table-content">
      <div className="table-header">
        <FilterData onChange={data => dispatchParams({ ...data, offset: 0 })} />
      </div>

      <div className="table-content">
        <ConfigProvider
          renderEmpty={() => (
            <Empty
              image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
              description={t('No Data')}
            />
          )}
        >
          <Table
            size="small"
            rowSelection={{ ...rowSelection }}
            columns={performedColumns}
            dataSource={performedData?.data || []}
            scroll={{ y: 'calc(100% - 31px)' }}
            rowKey={record => record.StudyInstanceUID}
            rowClassName={record =>
              record.StudyInstanceUID == relatedStudy.StudyInstanceUID
                ? 'selected'
                : ''
            }
            pagination={false}
            loading={isFetching}
            onChange={handleOnChangeTable}
            onRow={record => {
              return {
                onAuxClick: event => {
                  event.stopPropagation();
                  event.preventDefault();
                  if (event.button == 1) {
                    setReportStudy(record);
                    setVisibleReportModal(true);
                  }
                },
                onClick: event => {
                  event.stopPropagation();
                  event.preventDefault();
                  setRelatedStudy(record);

                  // To do: need to be fix for mobile
                  if (
                    relatedStudy == record &&
                    isMobile(window.innerWidth, window.innerHeight)
                  ) {
                    if (isUnarchiving(record)) {
                      Modal.info({
                        className: 'vindr-modal',
                        title: 'Processing unarchive',
                        content: 'Please wait until the study is unarchived.',
                      });
                      return;
                    }
                    if (isArchived(record)) {
                      handleUnarchive(record.StudyInstanceUID);
                      return;
                    }
                    handleDoubleClickRow(record);
                  }
                },
                onDoubleClick: event => {
                  event.stopPropagation();
                  event.preventDefault();
                  if (event?._targetInst?.elementType === 'input') {
                    // handle action doubleClick to checkbox
                    return;
                  }
                  if (isUnarchiving(record)) {
                    Modal.info({
                      className: 'vindr-modal',
                      title: 'Processing unarchive',
                      content: 'Please wait until the study is unarchived.',
                    });
                    return;
                  }
                  if (isArchived(record)) {
                    handleUnarchive(record.StudyInstanceUID);
                    return;
                  }
                  handleDoubleClickRow(record);
                },
                onContextMenu: event => {
                  event.stopPropagation();
                  event.preventDefault();
                  let contextStudies = [record];
                  if (
                    selectedStudies.find(
                      it => it.StudyInstanceUID == record.StudyInstanceUID
                    )
                  ) {
                    contextStudies = selectedStudies;
                  }

                  if (!contextMenu.visible) {
                    document.addEventListener(
                      `click`,
                      function onClickOutside() {
                        setContextMenu({
                          visible: false,
                          currentRecord: record,
                          records: contextStudies,
                        });
                        document.removeEventListener(`click`, onClickOutside);
                      }
                    );
                  }
                  setContextMenu({
                    currentRecord: record,
                    records: contextStudies,
                    visible: true,
                    x: event.clientX,
                    y: event.clientY,
                  });
                },
              };
            }}
          />
        </ConfigProvider>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'flex-end',
          }}
        >
          <div>
            {selectedStudies.length} {t('item(s)')} {t('selected')}
          </div>
          <PaginationTable
            page={params.offset}
            size={params.limit}
            onChange={(page, pageSize) => {
              dispatchParams({ ...params, offset: page - 1, limit: pageSize });
            }}
            totalElements={performedData?.count || 0}
          />
        </div>
        <PopupContextMenu {...contextMenu} />
        {unarchiveConfirm && (
          <Modal
            className="vindr-modal"
            title={t('Study is archived')}
            visible={unarchiveConfirm}
            okText={t('Agree')}
            cancelText={t('Disagree')}
            onOk={() => unarchiveStudy(unarchivingStudyUID)}
            onCancel={() => setUnarchiveConfirm(false)}
          >
            {t('Do you want to unarchive the study?')}
          </Modal>
        )}
        {visibleReportModal && (
          <DoctorModal
            onCancel={() => setVisibleReportModal(false)}
            study={reportStudy}
            isModalVisible={visibleReportModal}
          />
        )}
      </div>
    </div>
  );
};

export default PerformTable;
