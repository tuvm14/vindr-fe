import React, { useState } from 'react';
import { Row, Select, Input, Button, Collapse } from 'antd';
import {
  CaretDownOutlined,
  CheckOutlined,
  DownOutlined,
  SearchOutlined,
  UpOutlined,
} from '@ant-design/icons';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { getDateFormatted, isThaiThuyHospital } from '../../../../utils/helper';
import CheckboxMenu from '../CheckboxMenu';
import { StudyListContext } from '../../utils/contexts';
import './FilterData.styl';
import CustomDateRange from '../CustomDateRange';
import { BASE_TIME } from '../../../../utils/constants';

const { Option } = Select;
const { Panel } = Collapse;

const MODALITIES = [
  '*',
  'CR',
  'DX',
  'DR',
  'CT',
  'MR',
  'MG',
  'US',
  'ES',
  'PT',
  'ST',
  'XA',
  'SR',
  'SM',
  'Other',
];

const FILTER_TYPE = {
  RANGE_DATE: 1,
  MODALITIES: 2,
  STATUS: 3,
  AI_RESULT: 4,
};

const AI_RESULTS = [
  { label: 'AI Results', value: '*' },
  { label: 'Normal', value: 'false' },
  { label: 'Abnormal', value: 'true' },
];

const today = moment().format('YYYYMMDD');

const filterStartDate = isThaiThuyHospital()
  ? BASE_TIME
  : process.env.FILTER_START_DATE || today;

const INITIAL_FILTERS = {
  VindocStatus: '*',
  startDate: filterStartDate,
  endDate: today,
  ModalitiesInStudy: MODALITIES[0],
  IsAbnormal: AI_RESULTS[0].value,
};

const SEARCH_OPTIONS = [
  { text: 'All', value: 'All' },
  { text: 'Patient Name', value: 'PatientName' },
  { text: 'Patient ID', value: 'PatientID' },
  { text: 'Accession #', value: 'AccessionNumber' },
  { text: 'Body Part', value: 'BodyPartExamined' },
  { text: 'Acquisition Date', value: 'AcquisitionDate' },
  // { text: 'Code Meaning', value: 'CodeMeaning' },
  { text: 'Source AETitle', value: 'AETitle' },
];

const INITIAL_SEARCH_FIELDS = [
  {
    fieldId: 1,
    key: 'All',
    label: 'All',
    value: '',
    options: SEARCH_OPTIONS,
  },
  {
    fieldId: 2,
    key: 'PatientName',
    label: 'Patient Name',
    value: '',
    options: SEARCH_OPTIONS,
  },
  {
    fieldId: 3,
    key: 'AccessionNumber',
    label: 'Accession #',
    value: '',
    options: SEARCH_OPTIONS,
  },
];

function FilterData(props) {
  const { formatDate = 'YYYYMMDD', onChange } = props;
  const { t } = useTranslation(['Vindoc']);
  const [searchFields, setSearchFields] = useState([...INITIAL_SEARCH_FIELDS]);
  const {
    paramsInfo: { params },
  } = React.useContext(StudyListContext);
  const [isFiltered, setIsFiltered] = useState(false);

  const handleOnSearch = () => {
    let searchData = [];
    searchFields.forEach(it => {
      if (it.key && it.value) searchData.push({ key: it.key, value: it.value });
    });
    setIsFiltered(true);
    onChange({ ...params, search: searchData });
  };

  const handleOnChangeFilter = (type, values) => {
    if (!type || !values) return;
    let tempFilterData = {};

    switch (type) {
      case FILTER_TYPE.RANGE_DATE:
        tempFilterData = {
          ...params,
          startDate: values[0],
          endDate: values[1],
        };
        break;
      case FILTER_TYPE.MODALITIES:
        tempFilterData = {
          ...params,
          ModalitiesInStudy: values.length ? values : '*',
        };
        break;
      case FILTER_TYPE.STATUS:
        tempFilterData = {
          ...params,
          VindocStatus: values,
        };
        break;
      case FILTER_TYPE.AI_RESULT:
        tempFilterData = {
          ...params,
          IsAbnormal: values,
        };
        break;
      default:
        break;
    }
    setIsFiltered(true);
    onChange(tempFilterData);
  };

  const handleChangeSearchKey = (fieldId, value) => {
    let tempSearchField = [...searchFields];
    tempSearchField.some(it => {
      if (it.fieldId === fieldId) {
        it.key = value;
        return true;
      }
    });
    setSearchFields(tempSearchField);
  };

  const handleChangeSearchValue = (fieldId, value) => {
    let tempSearchField = [...searchFields];
    tempSearchField.some(it => {
      if (it.fieldId === fieldId) {
        it.value = value;
        return true;
      }
    });
    setSearchFields(tempSearchField);
  };

  const handleClearAllFilters = () => {
    let init = INITIAL_SEARCH_FIELDS.map(it => ({ ...it, value: '' }));
    setSearchFields([...init]);
    onChange({ ...INITIAL_FILTERS, search: [] });
    setIsFiltered(false);
  };

  const disabledDate = current => {
    return current && current > moment().endOf('day');
  };

  const extendFilter = (
    <>
      <div className="filter-items">
        <Row>
          <div className="filter-item select-item">
            <Row className="filter-title">{t('Modality')}</Row>
            <CheckboxMenu
              suffixIcon={<CaretDownOutlined />}
              size="small"
              height={(MODALITIES.length - 1) / 2}
              style={{ width: '100%' }}
              options={MODALITIES.filter(modality => modality !== '*')}
              value={params?.ModalitiesInStudy || []}
              onChange={values => {
                handleOnChangeFilter(FILTER_TYPE.MODALITIES, values);
              }}
            />
          </div>
          <div className="filter-item select-item">
            <Row className="filter-title">{t('Status')}</Row>
            <Select
              size="small"
              style={{ width: '100%' }}
              suffixIcon={<CaretDownOutlined />}
              placeholder="Select"
              className="select-dropdown-light"
              dropdownClassName="dropdown-options-dark"
              onChange={value => {
                handleOnChangeFilter(FILTER_TYPE.STATUS, value);
              }}
              value={params?.VindocStatus || null}
            >
              <Option value="*">{t('All')}</Option>
              <Option value="NEW">{t('New')}</Option>
              <Option value="READING">{t('Reading')}</Option>
              <Option value="SUBMITTED">{t('Submitted')}</Option>
              <Option value="APPROVED">{t('Approved')}</Option>
              {/* <Option value="PENDING">Pending</Option> */}
            </Select>
          </div>
          <div className="filter-item select-item">
            <Row className="filter-title">CAD</Row>
            <Select
              size="small"
              style={{ width: '100%' }}
              suffixIcon={<CaretDownOutlined />}
              placeholder={t('Select')}
              className="select-dropdown-light"
              dropdownClassName="dropdown-options-dark"
              onChange={value => {
                handleOnChangeFilter(FILTER_TYPE.AI_RESULT, value);
              }}
              value={
                params?.IsAbnormal === true || params?.IsAbnormal === 'true'
                  ? t('Filter_Abnormal')
                  : params?.IsAbnormal === false ||
                    params?.IsAbnormal === 'false'
                  ? t('Filter_Normal')
                  : t('All')
              }
            >
              <Option value="*">{t('All')}</Option>
              <Option value="true">{t('Filter_Abnormal')}</Option>
              <Option value="false">{t('Filter_Normal')}</Option>
            </Select>
          </div>
        </Row>
      </div>
      <div className="filter-items">
        <Row>
          {searchFields.map((field, idx) => {
            const options = field.options.filter(
              it =>
                !searchFields.find(
                  f => f.fieldId != field.fieldId && f.key == it.value
                )
            );
            return (
              <div key={field?.fieldId} className="filter-item search-item">
                <Row>
                  <div>
                    <Select
                      size="small"
                      className="select-dropdown-light no-border"
                      dropdownClassName="dropdown-options-dark"
                      menuItemSelectedIcon={<CheckOutlined />}
                      suffixIcon={<CaretDownOutlined />}
                      value={field.key}
                      onChange={value =>
                        handleChangeSearchKey(field.fieldId, value)
                      }
                    >
                      {options.map((item, idx) => (
                        <Option key={idx} value={item.value}>
                          {t(item.text)}
                        </Option>
                      ))}
                    </Select>
                  </div>
                </Row>
                <Row>
                  <div>
                    <Input
                      size="small"
                      value={field.value}
                      placeholder={t('Keyword')}
                      onChange={event =>
                        handleChangeSearchValue(
                          field.fieldId,
                          event?.target?.value
                        )
                      }
                      onPressEnter={() => handleOnSearch()}
                    />
                  </div>
                </Row>
              </div>
            );
          })}
        </Row>
      </div>
      <div className="filter-items">
        <Row>
          <div className="filter-item">
            <Button
              className="filter-btn"
              size="small"
              type="primary"
              icon={<SearchOutlined />}
              onClick={() => handleOnSearch()}
            >
              {t('Search')}
            </Button>
          </div>
          <div className="filter-item">
            <Button
              className="filter-btn"
              size="small"
              ghost
              disabled={!isFiltered}
              // icon={<ReloadOutlined />}
              onClick={() => handleClearAllFilters()}
            >
              {t('Clear Filter')}
            </Button>
          </div>
        </Row>
      </div>
    </>
  );

  const dateFilter = (
    <div className="filter-items">
      <div className="filter-item">
        <Row className="filter-title">{t('Study Date')}</Row>
        <Row className="picker-date">
          <CustomDateRange
            className="studylist-datepicker"
            size="small"
            allowClear={false}
            value={[
              moment(params.startDate || new Date(), formatDate),
              moment(params.endDate || new Date(), formatDate),
            ]}
            style={{ width: '100%' }}
            format={getDateFormatted()}
            dropdownClassName="date-picker-light"
            onChange={values => {
              handleOnChangeFilter(FILTER_TYPE.RANGE_DATE, [
                values[0].startOf('days').format(formatDate),
                values[1].endOf('days').format(formatDate),
              ]);
            }}
            disabledDate={disabledDate}
          />
        </Row>
      </div>
    </div>
  );

  return (
    <Row className="filter-data study-filter" align="bottom">
      {window.innerWidth > 600 && dateFilter}
      {window.innerWidth > 600 && extendFilter}
      {window.innerWidth <= 600 && (
        <Collapse
          bordered={false}
          style={{
            marginBottom: 12,
            width: '100%',
            backgroundColor: 'transparent',
          }}
          expandIconPosition="right"
          ghost
          expandIcon={panelProps =>
            panelProps.isActive ? <UpOutlined /> : <DownOutlined />
          }
        >
          <Panel
            header={
              <div onClick={event => event.stopPropagation()}>{dateFilter}</div>
            }
          >
            {extendFilter}
          </Panel>
        </Collapse>
      )}
    </Row>
  );
}

export default React.memo(FilterData);
