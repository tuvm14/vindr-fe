import React, {
  useState,
  useEffect,
  useCallback,
  useImperativeHandle,
  forwardRef,
} from 'react';
import { connect } from 'react-redux';
import { Table, Empty, ConfigProvider, Modal, Checkbox } from 'antd';
import { Tooltip } from '@tuvm/ui';
import { useTranslation } from 'react-i18next';

import AppContext from '../../../../context/AppContext';
import {
  actionGetStudyList,
  actionUnarchiveStudy,
  openViewer,
} from '../../actions';
import { StudyListContext } from '../../utils/contexts';
import useDefinedColumns from './useDefinedColumns';
import Icon, {
  ExclamationCircleOutlined,
  LoadingOutlined,
} from '@ant-design/icons';
import EmptyIcon from '../../../../../public/assets/empty-img-simple.svg';
import PopupContextMenu from './PopupContextMenu';
import DoctorModal from '../../../../components/Report/DoctorModal';

const RelatedTable = (props, ref) => {
  const { t } = useTranslation(['Vindoc']);
  const { appConfig = {} } = React.useContext(AppContext);

  const [performedData, setPerformedData] = useState({});
  const [isFetching, setIsFetching] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [visibleReportModal, setVisibleReportModal] = useState(false);
  const [reportStudy, setReportStudy] = useState({});

  const [unarchivingStudyUID, setUnarchivingStudyUID] = useState(null);
  const [unarchiveConfirm, setUnarchiveConfirm] = useState(false);

  const {
    relatedStudy: [relatedStudy],
  } = React.useContext(StudyListContext);
  const performedColumns = useDefinedColumns(false);
  const [contextMenu, setContextMenu] = useState({
    visible: false,
    x: 0,
    y: 0,
  });

  let params = {
    offset: 0,
    limit: 100,
    sort: '-StudyDate',
    query_string: `PatientID.keyword:"${relatedStudy.PatientID}"`,
  };

  useEffect(() => {
    handleFetchData(params);
  }, [relatedStudy]);

  const handleFetchData = async (params, isSilent = false) => {
    try {
      if (!relatedStudy || !relatedStudy.PatientID) {
        setPerformedData(null);
        props.onChangeCount(0);
        return;
      }
      if (!isSilent) {
        setIsFetching(true);
      }
      const newParams = {
        ...params,
        search: [
          {
            key: 'PatientID',
            value: relatedStudy.PatientID,
          },
        ],
        query_string: `PatientID.keyword:"${relatedStudy.PatientID}"`,
      };
      const { data } = await actionGetStudyList({
        ...newParams,
        offset: newParams.offset * newParams.limit,
      });
      const tempPerformedData = {
        count: data?.count || 0,
        data: data?.records || [],
      };
      setPerformedData(tempPerformedData);
      props.onChangeCount(tempPerformedData.count);
      if (!isSilent) {
        setIsFetching(false);
      }
    } catch (error) {
      if (!isSilent) {
        setIsFetching(false);
      }
    }
  };

  useImperativeHandle(ref, () => ({
    refreshData() {
      handleFetchData(params);
    },
  }));

  const handleDoubleClickRow = (record = {}) => {
    openViewer(
      appConfig.routerBasename,
      record.StudyInstanceUID,
      record.PatientID
    );
  };

  const resetSelectedItems = () => {
    setSelectedRowKeys([]);
    setSelectedItems([]);
    // onSelectRow(null);
  };

  const isArchived = study => {
    return (
      (study.ArchiveStatus &&
        study.ArchiveStatus.length &&
        study.ArchiveStatus[0]) ||
      (study.IsArchived && study.IsArchived.length && study.IsArchived[0])
    );
  };

  const isUnarchiving = study => {
    return (
      (study.ArchiveStatus &&
        study.ArchiveStatus.length &&
        study.ArchiveStatus[0] === 'UN_ARCHIVING') ||
      (study.InUnarchivingProgress &&
        study.InUnarchivingProgress.length &&
        study.InUnarchivingProgress[0])
    );
  };

  const handleUnarchive = studyUID => {
    setUnarchivingStudyUID(studyUID);
    setUnarchiveConfirm(true);
  };

  const unarchiveStudy = async studyInstanceUID => {
    const input = { study_instance_uid: studyInstanceUID };
    setUnarchiveConfirm(false);
    const { data = '' } = await actionUnarchiveStudy(input);
    if (data === 'Processing unarchive') {
      handleFetchData(params);
      Modal.info({
        className: 'vindr-modal',
        title: 'Processing unarchive',
        content: 'Please wait until the study is unarchived.',
      });
    } else {
      Modal.error({
        className: 'vindr-modal',
        title: 'Something went wrong',
        content: 'Please try again or contact admin for supports.',
      });
    }
  };

  const rowSelection = {
    selectedRowKeys,
    renderCell: (checked, record, index, originNode) =>
      isUnarchiving(record) ? (
        <Tooltip title={t('Unarchiving')}>
          <LoadingOutlined style={{ cursor: 'pointer', color: '#39C2D7' }} />
        </Tooltip>
      ) : isArchived(record) ? (
        <Checkbox
          checked={false}
          onClick={() => {
            handleUnarchive(record.StudyInstanceUID);
          }}
        />
      ) : (
        // <Tooltip title={t('Archived')}>
        //   <ExclamationCircleOutlined
        //     style={{ cursor: 'pointer', color: '#ff4a4a' }}
        //     // onClick={() => {
        //     //   // setUnarchivingStudyUID(studyUID);
        //     //   // setUnarchivingConfirmOpen(true);
        //     //   console.log('unarchive');
        //     // }}
        //   />
        // </Tooltip>
        originNode
      ),
    onChange: (rowKeys, selectedRows) => {
      setSelectedRowKeys(rowKeys);
      setSelectedItems(selectedRows || []);
      if ((selectedRows || []).length === 1) {
        // onSelectRow(selectedRows[0]);
      } else {
        // onSelectRow(null);
      }
    },
  };

  const handleOnChangeTable = useCallback((_, __, sorter) => {
    let sort;
    if (sorter?.order === 'ascend') {
      sort = `${sorter.field}`;
    } else if (sorter?.order === 'descend') {
      sort = `-${sorter.field}`;
    }
    params = { ...params, sort };
    resetSelectedItems();
    handleFetchData(params);
  }, []);

  return (
    <div className="perform-table-content related-table">
      <div className="table-content">
        <ConfigProvider
          renderEmpty={() => (
            <Empty
              image={<Icon style={{ fontSize: 64 }} component={EmptyIcon} />}
              description={t('No Data')}
            />
          )}
        >
          <Table
            size="small"
            rowSelection={{ ...rowSelection }}
            columns={performedColumns}
            dataSource={performedData?.data || []}
            scroll={{ y: 'calc(100%)' }}
            rowKey={record => record.StudyInstanceUID}
            pagination={false}
            loading={isFetching}
            onChange={handleOnChangeTable}
            onRow={record => {
              return {
                onAuxClick: event => {
                  event.stopPropagation();
                  event.preventDefault();
                  if (event.button == 1) {
                    setReportStudy(record);
                    setVisibleReportModal(true);
                  }
                },
                // onClick: event => {
                //   event.stopPropagation();
                //   event.preventDefault();
                //   setRelatedStudy(record);
                // },
                onDoubleClick: event => {
                  event.stopPropagation();
                  event.preventDefault();
                  if (event?._targetInst?.elementType === 'input') {
                    // handle action doubleClick to checkbox
                    return;
                  }
                  if (isUnarchiving(record)) {
                    Modal.info({
                      className: 'vindr-modal',
                      title: 'Processing unarchive',
                      content: 'Please wait until the study is unarchived.',
                    });
                    return;
                  }
                  if (isArchived(record)) {
                    handleUnarchive(record.StudyInstanceUID);
                    return;
                  }
                  handleDoubleClickRow(record);
                },
                onContextMenu: event => {
                  event.stopPropagation();
                  event.preventDefault();
                  let contextStudies = [record];
                  if (
                    selectedItems.find(
                      it => it.StudyInstanceUID == record.StudyInstanceUID
                    )
                  ) {
                    contextStudies = selectedItems;
                  }

                  if (!contextMenu.visible) {
                    document.addEventListener(
                      `click`,
                      function onClickOutside() {
                        setContextMenu({
                          visible: false,
                          currentRecord: record,
                          records: contextStudies,
                        });
                        document.removeEventListener(`click`, onClickOutside);
                      }
                    );
                  }
                  setContextMenu({
                    currentRecord: record,
                    records: contextStudies,
                    visible: true,
                    x: event.clientX,
                    y: event.clientY,
                  });
                },
              };
            }}
          />
        </ConfigProvider>
        <PopupContextMenu {...contextMenu} />
        {unarchiveConfirm && (
          <Modal
            className="vindr-modal"
            title={t('Study is archived')}
            visible={unarchiveConfirm}
            okText={t('Agree')}
            cancelText={t('Disagree')}
            onOk={() => unarchiveStudy(unarchivingStudyUID)}
            onCancel={() => setUnarchiveConfirm(false)}
          >
            {t('Do you want to unarchive the study?')}
          </Modal>
        )}
        {visibleReportModal && (
          <DoctorModal
            onCancel={() => setVisibleReportModal(false)}
            study={reportStudy}
            isModalVisible={visibleReportModal}
          />
        )}
      </div>
    </div>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(forwardRef(RelatedTable));
