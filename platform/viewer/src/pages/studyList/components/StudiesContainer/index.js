import React, { createRef, useState } from 'react';
import { Container, Section, Bar } from 'react-simple-resizer';
import { DownOutlined, UpOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import PerformTable from './PerformTable';
import RelatedTable from './RelatedTable';
import './StudiesContainer.styl';

const StudiesContainer = () => {
  const [relatedSize, setRelatedSize] = useState(40);
  const [relatedCount, setRelatedCount] = useState(0);
  const containerRef = createRef();
  const { t } = useTranslation(['Vindoc']);

  const setSize = size => {
    const container = containerRef.current;
    if (container) {
      const resizer = container.getResizer();
      resizer.resizeSection(1, { toSize: size });
      container.applyResizer(resizer);
    }
    setRelatedSize(size);
  };

  return (
    <Container vertical className="studies-container" ref={containerRef}>
      <Section
        style={{
          height: '100%',
          marginBottom: 15,
          borderRadius: '0 0 2px 2px',
        }}
      >
        <PerformTable />
      </Section>
      <Bar>
        <div className="divider"></div>
      </Bar>
      <Section
        style={{ marginTop: 15, borderRadius: 2 }}
        onSizeChanged={e => setRelatedSize(e)}
        defaultSize={relatedSize}
        minSize={40}
      >
        <div
          style={{
            position: 'relative',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: 41,
            padding: '12px 15px',
            background: 'var(--black-bg-3)',
          }}
        >
          <div style={{ display: 'flex' }}>
            <h3 style={{ margin: '0 20px 0 0', fontSize: 16 }}>
              {t('Related Studies')}
            </h3>
            <div className={relatedCount >= 2 ? 'hight-light' : ''}>
              ({relatedCount} {t('Study_items')})
            </div>
          </div>

          {relatedSize > 40 && (
            <span className="arrow-btn" onClick={() => setSize(40)}>
              <DownOutlined className="arrow-icon" />
              {t('Collapse')}
            </span>
          )}
          {relatedSize <= 40 && (
            <span className="arrow-btn" onClick={() => setSize(250)}>
              <UpOutlined className="arrow-icon" />
              {t('Expand')}
            </span>
          )}
        </div>
        <RelatedTable
          onChangeCount={count => {
            // if (count > 0 && relatedSize <= 40) setSize(250);
            setRelatedCount(count);
          }}
        />
      </Section>
    </Container>
  );
};

export default StudiesContainer;
