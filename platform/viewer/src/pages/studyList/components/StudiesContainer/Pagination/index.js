import React from 'react';
import { Pagination } from 'antd';
import './PaginationTable.styl';
import { useTranslation } from 'react-i18next';

const PaginationTable = props => {
  const { t } = useTranslation('Vindoc');
  return (
    <div className="pagination-table">
      <Pagination
        size="small"
        current={props.page + 1}
        pageSize={props.size || 0}
        total={props.totalElements || 0}
        showSizeChanger
        showQuickJumper
        showTotal={total => `${t('Total')} ${total} ${t('items')}`}
        onChange={props.onChange}
        disabled={props.isDisabled}
        pageSizeOptions={[10, 25, 50, 100]}
      />
    </div>
  );
};

export default PaginationTable;
