import React, { useEffect, useState, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { Button, Layout, Menu } from 'antd';
import {
  CaretLeftOutlined,
  CaretRightOutlined,
  PlusOutlined,
  FolderOutlined,
  FolderOpenOutlined,
} from '@ant-design/icons';
import { Icon, Tooltip } from '@tuvm/ui';
import CreateFolder from './CreateFolder';
import { actionGetPanel } from '../../actions';
import { StudyListContext } from '../../utils/contexts';
import { FOLDER_TYPE, NEW_SCOPES, SCOPES } from '../../../../utils/constants';

import './Sidebar.styl';
import PopupContextMenu from './PopupContextMenu';
import keycloak from '../../../../services/auth';

const { SubMenu } = Menu;

const { Sider } = Layout;
// default folder
const ALL_FOLDER = {
  _id: 'ALL_0',
  name: 'All',
  query_string: '*',
};

export default function SideBar() {
  const { t } = useTranslation('Vindoc');
  const [collapsed, setCollapsed] = React.useState(window.innerWidth <= 600);
  const [activeFilterKey, setActiveFilterKey] = useState(null);
  const [openKeys, setOpenKeys] = useState(
    window.innerWidth > 600 ? ['sub0', 'sub1', 'sub2', 'sub3', 'sub4'] : []
  );
  const [openCreateFolderForm, setOpenCreateFolderForm] = useState(false);
  const [groups, setGroups] = useState([
    {
      key: FOLDER_TYPE.ALL_STUDIES,
      data: [ALL_FOLDER],
      name: 'All',
      hasSub: false,
    },
    {
      key: FOLDER_TYPE.DEVICES,
      data: [],
      name: 'Devices',
      hasSub: true,
    },
    {
      key: FOLDER_TYPE.USER_UPLOADS,
      data: [],
      name: 'Uploads',
      hasSub: true,
    },
    {
      key: FOLDER_TYPE.SHARED,
      data: [],
      name: 'Shared',
      hasSub: false,
    },
  ]);

  const [activeFolder, setActiveFolder] = useState('ALL_0');

  const extensions = useSelector(state => state.extensions);
  const { panel } = get(extensions, 'studylist') || {};

  const [contextMenu, setContextMenu] = useState({
    visible: false,
    x: 0,
    y: 0,
  });

  const {
    paramsInfo: { dispatchParams },
    selectedStudies: [, setSelectedStudies],
  } = React.useContext(StudyListContext);

  useEffect(() => {
    actionGetPanel();
  }, []);

  const handleSelectFolder = item => {
    if (item.filter_id || item.filterId) {
      dispatchParams({
        Directory: '',
        filter_id: item?.filter_id || item?.filterId,
      });
    } else {
      dispatchParams({ Directory: item?.query_string, filter_id: '' });
    }

    setActiveFolder(item?._id);
    setSelectedStudies([]);
  };

  const handleCreateFolder = () => {
    setOpenCreateFolderForm(!openCreateFolderForm);
  };

  useEffect(() => {
    if (!isEmpty(panel)) {
      const {
        folder_filter = [],
        bookmark_filter = [],
        user_upload_filter = [],
        share_study_filter = [],
      } = panel;

      let dataGroups = {
        ALL_STUDIES: [],
        DEVICES: [],
        USER_UPLOADS: [],
        SHARED: [],
        BOOKMARK: [],
      };
      let all_devices_filter_id = null;

      if (!isEmpty(folder_filter)) {
        folder_filter.forEach((it, idx) => {
          if (it.name === 'All') {
            dataGroups?.ALL_STUDIES.push({
              ...it,
              _id: `ALL_${idx}`,
            });
          } else if (it.name === 'Device') {
            all_devices_filter_id = it.filter_id;
            it.children?.forEach(item => {
              dataGroups?.DEVICES.push({
                ...item,
                _id: `FILTER_${item.filter_id}`,
              });
            });
          }
        });
      }
      if (!isEmpty(share_study_filter)) {
        share_study_filter.forEach((it, idx) => {
          dataGroups?.SHARED.push({
            ...it,
            _id: `SHARED_${idx}`,
          });
        });
      }
      if (!isEmpty(bookmark_filter)) {
        bookmark_filter.forEach((it, idx) => {
          dataGroups?.BOOKMARK.push({ ...it, _id: `DIRECTORY_${idx}` });
        });
      }

      if (!isEmpty(user_upload_filter)) {
        user_upload_filter.forEach((it, idx) => {
          dataGroups?.USER_UPLOADS.push({
            ...it,
            _id: `USER_${idx}`,
          });
        });
      }

      const finalData = [
        {
          key: FOLDER_TYPE.ALL_STUDIES,
          data: dataGroups.ALL_STUDIES,
          name: 'All',
          hasSub: false,
        },
        {
          key: FOLDER_TYPE.DEVICES,
          data: dataGroups.DEVICES,
          name: 'Devices',
          hasSub: true,
          filterId: all_devices_filter_id,
        },
        {
          key: FOLDER_TYPE.USER_UPLOADS,
          data: dataGroups.USER_UPLOADS,
          name: 'Uploads',
          hasSub: true,
        },
        {
          key: FOLDER_TYPE.SHARED,
          data: dataGroups.SHARED,
          name: 'Shared',
          hasSub: false,
        },
        {
          key: FOLDER_TYPE.BOOKMARK,
          data: dataGroups.BOOKMARK,
          name: 'Bookmarks',
          hasSub: true,
          hasContext: true,
        },
      ];

      setGroups(finalData);
    }
  }, [panel]);

  const clickExpandIcon = (x, y, iconRect) => {
    return (
      x >= iconRect.x - 5 &&
      x <= iconRect.x + iconRect.width + 5 &&
      y >= iconRect.y - 5 &&
      y <= iconRect.y + 11
    );
  };

  const renderSubMenu = (groupItem, menuItem, layer = 1) => {
    const id = menuItem._id || menuItem.filter_id;
    if (menuItem.children?.length) {
      return (
        <SubMenu
          className="submenu-list"
          // key={'sub' + index}
          style={{
            background: activeFilterKey === id ? '#2C5166' : 'transparent',
            color: activeFilterKey === id ? '#fff' : '#dbdbdb',
            borderRight: activeFilterKey === id ? '#39C2D7 3px solid' : 'none',
          }}
          onTitleClick={event => clickSubMenuCallback(menuItem, event)}
          key={menuItem._id || `FILTER_${menuItem.filter_id}`}
          icon={
            openKeys.includes(
              menuItem._id || `FILTER_${menuItem.filter_id}`
            ) ? (
              <FolderOpenOutlined style={{ fontSize: 15 }} />
            ) : (
              <FolderOutlined style={{ fontSize: 15 }} />
            )
          }
          title={t(menuItem.name)}
        >
          {(menuItem.children || []).map(item =>
            renderSubMenu(menuItem, item, layer + 1)
          )}
        </SubMenu>
      );
    }
    return (
      <Menu.Item
        key={menuItem._id || `FILTER_${menuItem.filter_id}`}
        className="folder-submenu-item"
        onClick={() => {
          const id = menuItem._id || menuItem.filter_id;
          if (activeFilterKey !== id) {
            setActiveFilterKey(id);
            handleSelectFolder(menuItem);
          }
        }}
        style={{
          background: activeFilterKey === id ? '#2C5166' : 'transparent',
          color: activeFilterKey === id ? '#fff' : '#dbdbdb',
          borderRight:
            activeFilterKey === id && layer !== 1
              ? '#39C2D7 3px solid'
              : 'none',
        }}
        onContextMenu={event => {
          event.stopPropagation();
          event.preventDefault();

          if (!groupItem.hasContext) return;

          if (!contextMenu.visible) {
            document.addEventListener(`click`, function onClickOutside() {
              setContextMenu({ visible: false, folder: menuItem });
              document.removeEventListener(`click`, onClickOutside);
            });
          }
          setContextMenu({
            folder: menuItem,
            visible: true,
            x: event.clientX,
            y: event.clientY,
          });
        }}
      >
        <div className="submenu-text">{menuItem.name}</div>
      </Menu.Item>
    );
  };

  const handleRefreshData = useCallback(
    folder => {
      if (folder && folder._id === activeFolder) {
        handleSelectFolder(groups[0].data[0]);
      }
      actionGetPanel();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [groups]
  );

  const clickSubMenuCallback = (menuItem, event, index = 0) => {
    const node = event.domEvent.target;
    const iconExpand = node.classList.contains('ant-menu-submenu-arrow')
      ? node
      : node.classList.length === 0
      ? node.nextElementSibling
      : node.childNodes[node.childNodes.length - 1];
    const key = menuItem.filterId
      ? 'sub' + index
      : menuItem._id || menuItem.filter_id;
    if (
      (!iconExpand ||
        !clickExpandIcon(
          event.domEvent.pageX,
          event.domEvent.pageY,
          iconExpand.getBoundingClientRect()
        )) &&
      activeFilterKey !== key
    ) {
      setActiveFilterKey(key);
      handleSelectFolder(menuItem);
      if (!openKeys.includes(event.key)) {
        const openingKeys = [...openKeys];
        openingKeys.push(event.key);
        setOpenKeys(openingKeys);
      }
    } else {
      let openingKeys = [...openKeys];
      openingKeys.includes(event.key)
        ? openingKeys.splice(openKeys.indexOf(event.key), 1)
        : openingKeys.push(event.key);
      setOpenKeys(openingKeys);
    }
  };

  return (
    <Sider
      collapsible
      collapsed={collapsed}
      trigger={collapsed ? <CaretRightOutlined /> : <CaretLeftOutlined />}
      onCollapse={() => setCollapsed(!collapsed)}
      collapsedWidth={60}
      width={200}
      className="vindr-custom-sidebar"
    >
      {!collapsed ? (
        <Menu
          selectedKeys={[activeFolder]}
          triggerSubMenuAction="hover"
          openKeys={openKeys}
          mode="inline"
          className={`folder-menu-list ${collapsed ? 'is-collapsed' : ''}`}
          inlineIndent={12}
          style={{ minWidth: '40px' }}
        >
          {groups.map((groupItem, index) => {
            return groupItem?.hasSub ? (
              <SubMenu
                className={
                  activeFilterKey === 'sub' + index && groupItem.filterId
                    ? 'submenu-list submenu-active'
                    : 'submenu-list'
                }
                key={'sub' + index}
                icon={
                  <span role="img" className="anticon anticon-folder menu-icon">
                    <Icon
                      name={folderIcons[groupItem.key]}
                      className="vindr-menu-icon"
                    />
                  </span>
                }
                title={t(groupItem.name)}
                onTitleClick={event =>
                  clickSubMenuCallback(groupItem, event, index)
                }
              >
                {(groupItem.data || []).map(item =>
                  renderSubMenu(groupItem, item, 1)
                )}
              </SubMenu>
            ) : (
              groupItem.data.map(item => (
                <Menu.Item
                  key={item._id}
                  onClick={event => clickSubMenuCallback(item, event)}
                  className="folder-menu-item"
                  icon={
                    <span
                      role="img"
                      className="anticon anticon-folder menu-icon"
                    >
                      <Icon
                        name={folderIcons[groupItem.key]}
                        className="vindr-menu-icon"
                      />
                    </span>
                  }
                >
                  {t(item.name)}
                </Menu.Item>
              ))
            );
          })}
        </Menu>
      ) : (
        <Menu
          selectedKeys={[activeFolder]}
          triggerSubMenuAction="hover"
          mode="inline"
          className={`folder-menu-list ${collapsed ? 'is-collapsed' : ''}`}
          inlineIndent={12}
          style={{ minWidth: '40px' }}
        >
          {groups.map((groupItem, index) => {
            return groupItem?.hasSub ? (
              <SubMenu
                className={
                  activeFilterKey === 'sub' + index && groupItem.filterId
                    ? 'submenu-list submenu-active'
                    : 'submenu-list'
                }
                key={'sub' + index}
                icon={
                  <span role="img" className="anticon anticon-folder menu-icon">
                    <Icon
                      name={folderIcons[groupItem.key]}
                      className="vindr-menu-icon"
                    />
                  </span>
                }
                title={t(groupItem.name)}
                onTitleClick={event =>
                  clickSubMenuCallback(groupItem, event, index)
                }
              >
                {(groupItem.data || []).map(item =>
                  renderSubMenu(groupItem, item, 1)
                )}
              </SubMenu>
            ) : (
              groupItem.data.map(item => (
                <Menu.Item
                  key={item._id}
                  onClick={event => clickSubMenuCallback(item, event)}
                  className="folder-menu-item"
                  icon={
                    <span
                      role="img"
                      className="anticon anticon-folder menu-icon"
                    >
                      <Icon
                        name={folderIcons[groupItem.key]}
                        className="vindr-menu-icon"
                      />
                    </span>
                  }
                >
                  {t(item.name)}
                </Menu.Item>
              ))
            );
          })}
        </Menu>
      )}

      {keycloak.hasOneOfPerm([
        SCOPES.organizationAdmin,
        SCOPES.globalAdmin,
        NEW_SCOPES.worklist.bookmark.create,
      ]) &&
        (!collapsed ? (
          <Button
            className="create-folder-button"
            type="primary"
            icon={<PlusOutlined />}
            onClick={handleCreateFolder}
          >
            {t('Create Bookmark')}
          </Button>
        ) : (
          <Tooltip title={t('Create Bookmark')}>
            <Button
              className="create-folder-circle-button"
              type="primary"
              shape="circle"
              icon={<PlusOutlined />}
              onClick={handleCreateFolder}
            />
          </Tooltip>
        ))}

      {openCreateFolderForm && (
        <CreateFolder handleClose={handleCreateFolder} />
      )}
      <PopupContextMenu {...contextMenu} onRefreshData={handleRefreshData} />
    </Sider>
  );
}

const folderIcons = {
  [FOLDER_TYPE.ALL_STUDIES]: 'home',
  [FOLDER_TYPE.DEVICES]: 'allDeviceFolder',
  [FOLDER_TYPE.SHARED]: 'shared-folder',
  [FOLDER_TYPE.BOOKMARK]: 'bookmark-folder',
  [FOLDER_TYPE.USER_UPLOADS]: 'user-folder',
};
