import React, { useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Form, Button, Input, message, Modal } from 'antd';
import {
  actionCreateBookmark,
  actionGetPanel,
  actionUpdateBookmark,
  actionGetBookmarkDetail,
} from '../../actions';
import isEmpty from 'lodash/isEmpty';

import './CreateFolder.styl';
import { NEW_SCOPES, SCOPES } from '../../../../utils/constants';
import keycloak from '../../../../services/auth';

const CreateFolder = props => {
  const { item } = props;
  const { t } = useTranslation('Vindoc');
  const [form] = Form.useForm();
  const isAdd = useMemo(() => isEmpty(item), [item]);

  useEffect(() => {
    if (!isAdd && item.id_) {
      handleGetBookmarkDetail(item.id_);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAdd]);

  const handleGetBookmarkDetail = async id => {
    try {
      const { data } = await actionGetBookmarkDetail(id);
      if (data) {
        form.setFieldsValue({ description: data.description || '' });
      }
      // eslint-disable-next-line no-empty
    } catch (error) {}
  };

  const handleSave = () => {
    form
      .validateFields()
      .then(async values => {
        try {
          const payload = {
            name: values.name,
            description: values.description,
          };
          let res;
          if (isAdd) {
            res = await actionCreateBookmark(payload);
          } else {
            payload.id_ = item.id_;
            res = await actionUpdateBookmark(payload);
          }

          if (res && res.status === 200) {
            message.success(
              isAdd ? t('Add successfully') : t('Update successfully')
            );
            actionGetPanel();
            props.handleClose();
          } else {
            message.error(t('Error Message'));
          }
        } catch (error) {
          message.error(t('Error Message'));
        }
      })
      .catch(info => {
        console.log('Validate Failed:', info);
      });
  };

  return (
    <>
      <Modal
        title={isAdd ? t('Create New Bookmark') : t('Edit Bookmark')}
        width={620}
        onCancel={props.handleClose}
        visible={true}
        maskClosable={false}
        className="create-folder-modal"
        footer={
          <div
            style={{
              textAlign: 'right',
            }}
          >
            <Button onClick={props.handleClose} style={{ marginRight: 8 }}>
              {t('Cancel')}
            </Button>
            <Button
              onClick={handleSave}
              type="primary"
              disabled={
                (!isAdd &&
                  !keycloak.hasOneOfPerm([
                    SCOPES.organizationAdmin,
                    SCOPES.globalAdmin,
                    NEW_SCOPES.worklist.bookmark.edit,
                  ])) ||
                (isAdd &&
                  !keycloak.hasOneOfPerm([
                    SCOPES.organizationAdmin,
                    SCOPES.globalAdmin,
                    NEW_SCOPES.worklist.bookmark.create,
                  ]))
              }
            >
              {isAdd ? t('Add') : t('Save')}
            </Button>
          </div>
        }
      >
        <Form
          layout="vertical"
          hideRequiredMark
          form={form}
          name="dynamic_form_nest_item"
          onFinish={handleSave}
          className="form-filter-condition"
          initialValues={isAdd ? {} : { ...item }}
        >
          <Form.Item
            name="name"
            label={t('Bookmark name') + ' *'}
            rules={[{ required: true, message: t('Required value') }]}
          >
            <Input placeholder={t('Bookmark name')} />
          </Form.Item>
          <Form.Item name="description" label={t('Description')}>
            <Input.TextArea rows={4} />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default CreateFolder;

CreateFolder.propTypes = {
  handleClose: PropTypes.func,
};
