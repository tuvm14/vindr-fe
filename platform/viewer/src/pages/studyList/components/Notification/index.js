import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Alert, Button } from 'antd';
import { w3cwebsocket } from 'websocket';
import { CONFIG_SERVER } from '../../../../utils/constants';
import get from 'lodash/get';
import './Notification.css';

const { WEBSOCKET_URL } = CONFIG_SERVER;

function Notification(props) {
  const userId = get(props.user, 'id');
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    if (userId) {
      try {
        const wsProtocol = window.location.protocol === 'https:' ? 'wss' : 'ws';
        let uri = WEBSOCKET_URL || `${wsProtocol}:${window.location.host}`;
        const client = new w3cwebsocket(`${uri}/api/notification/ws`);
        if (client) {
          client.onopen = () => {
            console.log('WebSocket Client Connected');
          };
          client.onmessage = m => {
            setMessages(messages => [...messages, JSON.parse(m.data)]);
          };
        }
      } catch (error) {
        console.log('Init Websocket fail');
      }
    }
  }, [userId]);

  const onCloseAlert = (event, it) => {
    event.preventDefault();
    event.stopPropagation();
    console.log('Close message');
  };

  const viewStudy = it => {
    if (!it.action_link) return;
    window.open(it.action_link, 'vindrViewerWindow');
  };

  return (
    <div className="notification">
      {messages &&
        messages.length > 0 &&
        messages.map(
          (it, idx) =>
            it &&
            it.title &&
            it.body && (
              <Alert
                className="noti-item"
                key={idx}
                message={it.title}
                description={<Button type="link">{it.body}</Button>}
                type="info"
                showIcon
                closable
                onClose={event => onCloseAlert(event, it)}
                onClick={() => viewStudy(it)}
              />
            )
        )}
    </div>
  );
}

export default connect(
  state => ({ user: state.extensions?.user }),
  {}
)(
  React.memo(Notification, (prevProps, nextProps) => {
    return prevProps?.user?.id !== nextProps?.user?.id;
  })
);
