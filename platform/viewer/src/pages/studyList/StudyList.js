import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import querystring from 'querystring';
import { Layout, message } from 'antd';

import ConnectedHeader from '../../components/Header/ConnectedHeader';
import {
  StudiesContainer,
  MobileStudiesContainer,
  Header,
  DiagnoseModal,
  SideBar,
  Notification,
} from './components';
import { useAssignment } from './hooks';
import { StudyListContext } from './utils/contexts';
import { actionCallBatchAnalysis } from './actions';

import './style.styl';
import { INITIAL_FILTERS } from './utils/constants';
import { NAV_LIST } from '../../utils/constants';
import useNavigationMenu from '../../components/Header/useNavigationMenu';
import { useWindowSize } from '@tuvm/ui/src/hooks/useWindowSize';
import { isMobile } from '../../utils/helper';

export default function StudyList(props) {
  const { user } = props;

  const {
    callAssignDoctor,
    isAssigned,
    assignmentError,
    isAssignmentLoading,
  } = useAssignment();
  const { t } = useTranslation(['Vindoc']);
  window.t = t;
  const history = useHistory();
  const navigationOptions = useNavigationMenu();

  React.useEffect(() => {
    document.title = 'VinDr - Study List';
  }, []);

  const [selectedStudies, setSelectedStudies] = React.useState([]);
  const [isBatchAnalyzing, setBatchAnalyzing] = React.useState(false);
  const [width, height] = useWindowSize();

  const [params, setParams] = useState(INITIAL_FILTERS || {});
  const [visibleDiagnose, setVisibleDiagnose] = useState(false);
  const [relatedStudy, setRelatedStudy] = useState({});
  const [totalStudy, setTotalStudy] = useState(0);

  const handleChangeParams = (value = {}) => {
    const newParams = { ...params, ...value };
    if (!value.search) delete newParams.search;
    if (!newParams.sort) delete newParams.sort;

    setParams(newParams);

    let qrString = { ...newParams };
    delete qrString.Directory;
    history.replace(`?${querystring.stringify(qrString)}`);
  };

  const handleBatchAnalysis = (selectedIds = []) => {
    setVisibleDiagnose(true);
  };

  const actionBatchAnalysis = async (selectedStudies, model = '') => {
    try {
      const selectedIds = selectedStudies.map(it => it.StudyInstanceUID);
      setVisibleDiagnose(false);
      setBatchAnalyzing(true);
      await actionCallBatchAnalysis({
        study_sop_ids: selectedIds,
        model: model,
      });

      if (selectedIds.length > 1) {
        message.info(
          t('DIAGNOSING_STUDIES', { selectedItems: selectedIds.length || 0 })
        );
      } else {
        message.info(
          t('DIAGNOSING_STUDY', { selectedItems: selectedIds.length || 0 })
        );
      }

      setBatchAnalyzing(false);
    } catch (error) {
      setBatchAnalyzing(false);
    }
  };

  const store = {
    relatedStudy: [relatedStudy, setRelatedStudy],
    selectedStudies: [selectedStudies, setSelectedStudies],
    batch: [isBatchAnalyzing, handleBatchAnalysis],
    paramsInfo: { params, dispatchParams: handleChangeParams },
    assignment: {
      callAssignDoctor,
      isAssigned,
      assignmentError,
      isAssignmentLoading,
    },
    totalStudy: [totalStudy, setTotalStudy],
  };

  return (
    <>
      <ConnectedHeader
        useLargeLogo={true}
        user={user}
        navigationOptions={navigationOptions}
        activeMenu={NAV_LIST.STUDYLIST}
      />
      <StudyListContext.Provider value={store}>
        <Layout className="wrapper-layout">
          <SideBar />
          <Layout className="site-layout">
            <div className="container">
              <Header totalItem={totalStudy} />
              {!isMobile(width, height) && <StudiesContainer />}
              {isMobile(width, height) && <MobileStudiesContainer />}
              <Notification />
            </div>
            {visibleDiagnose && (
              <DiagnoseModal
                onCancel={() => setVisibleDiagnose(false)}
                onOk={model => {
                  actionBatchAnalysis(selectedStudies, model);
                }}
              />
            )}
          </Layout>
        </Layout>
      </StudyListContext.Provider>
    </>
  );
}

StudyList.propTypes = {
  server: PropTypes.object,
  user: PropTypes.object,
};
