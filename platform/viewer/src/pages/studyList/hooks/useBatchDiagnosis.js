import React from 'react';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { useActiveServer } from './useActiveServer.js';
import api from '../../../services/api';

export const useAnalysisResult = () => {
  const activeServer = useActiveServer();
  const [result, setResult] = React.useState(null);

  const callAnalysisResult = React.useCallback(
    sopId => {
      const requestOptions = {
        method: 'GET',
        redirect: 'follow',
        url: `${activeServer.aiRoot}/backend/diagnosis?study_sop_id=${sopId}`,
      };

      api(requestOptions)
        .then(result => {
          const fullResult = get(result, 'data.data.[0]');
          const state = get(fullResult, 'state');
          if (state === 'finished') {
            setResult(fullResult);
          }
        })
        .catch(error => console.log('error', error));
    },
    [activeServer.aiRoot]
  );

  return { callAnalysisResult, result };
};

export const useStudyResult = () => {
  const activeServer = useActiveServer();
  const [result, setResult] = React.useState(null);

  const callStudyResult = React.useCallback(
    studySopId => {
      const requestOptions = {
        method: 'GET',
        redirect: 'follow',
        url: `${activeServer.aiRoot}'/backend/doctor/study'}?study_sop_id=${studySopId}`,
      };

      api(requestOptions)
        .then(results => {
          if (!isEmpty(results)) {
            setResult(results.data);
          }
        })
        .catch(error => console.log('error', error));
    },
    [activeServer.aiRoot]
  );

  return { callStudyResult, result };
};
