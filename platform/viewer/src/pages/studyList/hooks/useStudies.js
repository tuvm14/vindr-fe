import React from 'react';
import assign from 'lodash/assign';
import get from 'lodash/get';
import values from 'lodash/values';
import { toLuceneQueryString } from '../utils/transform';
import {
  isValidParams,
  isValidSearch,
  isValidSortField,
} from '../utils/validators';
import api from '../../../services/api';
import { INITIAL_FILTERS, STUDY_DATETIME } from '../utils/constants';
import { actionSetExtensionData } from '../../../system/systemActions';

const initialPagination = {
  limit: 25,
  offset: 0,
};

const initialQueryString = {};

const initialSearch = {};

const reducerPagination = (state, { type, pagination }) => {
  switch (type) {
    case 'pagination':
      return assign({}, state, {
        limit: pagination.limit,
        offset: pagination.offset,
      });
    default:
      throw new Error('Unexpected action.');
  }
};

const reducerDirectory = (state, { type, directory }) => {
  switch (type) {
    case 'directory':
      return directory;
    default:
      throw new Error('Unexpected action.');
  }
};

const reducerSort = (state, { type, field }) => {
  switch (type) {
    case 'sort':
      return field;
    default:
      throw new Error('Unexpected action.');
  }
};

const reducerParams = (state, { type, params }) => {
  switch (type) {
    case 'params':
      return assign({}, state, params);
    case 'reset':
      return initialQueryString;
    default:
      throw new Error('Unexpected action.');
  }
};

const reducerSearch = (state, { type, field, value }) => {
  switch (type) {
    case 'search':
      return assign({}, { [field]: value });
    case 'reset':
      return initialSearch;
    default:
      throw new Error('Unexpected action.');
  }
};

export default function useStudies() {
  const [error, setError] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(false);
  const [studies, setStudies] = React.useState([]);
  const [count, setCount] = React.useState(0);
  const [pagination, dispatchPagination] = React.useReducer(
    reducerPagination,
    initialPagination
  );
  const [directory, dispatchDirectory] = React.useReducer(reducerDirectory, -1);
  const [sort, dispatchSort] = React.useReducer(
    reducerSort,
    INITIAL_FILTERS.sort
  );
  const [params, dispatchParams] = React.useReducer(
    reducerParams,
    initialQueryString
  );
  const [search, dispatchSearch] = React.useReducer(
    reducerSearch,
    initialSearch
  );

  const fetchStudies = React.useCallback(() => {
    if (isValidParams(params) && isValidSearch(search) && directory >= 0) {
      let queryString = '';

      if (search.All) {
        queryString =
          `*${search.All} AND ` + toLuceneQueryString({ ...params });
      } else {
        queryString =
          values(search)[0] === ''
            ? toLuceneQueryString({ ...params })
            : toLuceneQueryString({ ...params, ...search });
      }

      let sortFiled = {};
      if (isValidSortField(sort)) {
        if (sort === STUDY_DATETIME.STUDY_DATE) {
          sortFiled.sort = sort + ',' + STUDY_DATETIME.STUDY_TIME;
        } else {
          sortFiled.sort = sort + ',-' + STUDY_DATETIME.STUDY_TIME;
        }
      }

      const url = '/studylist/studies';

      queryString += ` AND Directory.${directory}:true`;

      api({
        method: 'get',
        url,
        params: assign({}, pagination, sortFiled, {
          query_string: queryString,
        }),
        timeout: 30000,
      })
        .then(response => {
          setStudies(get(response, 'data.data', []));
          setCount(get(response, 'data.count', 0));
        })
        .catch(error => {
          setError(error);
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  }, [params, search, pagination, sort, directory]);

  // Trigger fetchStudies right after criteria changed
  React.useEffect(() => {
    setIsLoading(true);
    fetchStudies();
  }, [fetchStudies]);

  // Trigger fetchStudies after each 15 seconds
  React.useEffect(() => {
    setIsLoading(true);

    const interval = setInterval(() => {
      fetchStudies();
    }, 15000);

    return () => {
      clearInterval(interval);
    };
  }, [fetchStudies]);

  return {
    count,
    error,
    isLoading,
    studies,
    pagination,
    dispatchPagination,
    sort,
    dispatchSort,
    params,
    dispatchParams,
    search,
    dispatchSearch,
    fetchStudies,
    directory,
    dispatchDirectory,
  };
}
