import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { workgroupEndpoint } from '../../../services/endpoint';
import api from '../../../services/api';
import { message } from 'antd';

export function useAssignment() {
  const [assignmentError, setAssignmentError] = React.useState(null);
  const [isAssignmentLoading, setIsAssignmentLoading] = React.useState(false);
  const [isAssigned, setIsAssigned] = React.useState([]);
  const { t } = useTranslation('Vindoc');

  const callAssignDoctor = useCallback(
    data => {
      const requestOptions = {
        method: 'POST',
        data: {
          study_instance_uids: data.studyInstancesUIDs,
        },
        url: `${workgroupEndpoint}/${data.workgroupId}/add-studies`,
      };

      setIsAssignmentLoading(true);

      api(requestOptions)
        .then(result => {
          setIsAssigned(true);
          setIsAssignmentLoading(false);
          message.success(t('Assign studies success message'));
        })
        .catch(error => {
          setAssignmentError(error);
          setIsAssignmentLoading(false);
        });
    },
    [t]
  );

  return {
    callAssignDoctor,
    isAssigned,
    assignmentError,
    isAssignmentLoading,
  };
}
