import api from '../../services/api';
import { actionSetExtensionData } from '../../system/systemActions';
import { CONFIG_SERVER } from '../../utils/constants';
import { getTenant, isMobile } from '../../utils/helper';
import { getStudyList } from '../../utils/studiesHelper';

const tenant = getTenant();

export const actionGetStudyList = (params = {}) => {
  if (CONFIG_SERVER.DISABLE_AUTH) {
    return actionGetLocalStudyList(params);
  }
  return api({
    url: '/studylist/v2/search',
    method: 'GET',
    params,
    timeout: 30000,
  });
};

export const actionGetLocalStudyList = (params = {}) => {
  const searchPatientName =
    (params.search && params.search.find(it => it.key == 'PatientName')) || {};
  const searchPatientID =
    (params.search && params.search.find(it => it.key == 'PatientID')) || {};
  const searchAccessionNumber =
    (params.search && params.search.find(it => it.key == 'AccessionNumber')) ||
    {};
  const sortString = (params.sort && params.sort.split(',')[0]) || '';
  const filterValues = {
    studyDateTo: params.endDate,
    studyDateFrom: params.startDate,
    PatientName: searchPatientName.value || '',
    PatientID: searchPatientID.value || '',
    AccessionNumber: searchAccessionNumber.value || '',
    StudyDate: '',
    modalities: params.ModalitiesInStudy || '',
    StudyDescription: '',
    //
    patientNameOrId: '',
    accessionOrModalityOrDescription: '',
    //
    allFields: '',
  };
  const sort = {
    fieldName: sortString ? sortString.replace('-', '') : 'StudyDate',
    direction: sortString.startsWith('-') ? 'asc' : 'desc',
  };
  // const debouncedSort = useDebounce(sort, 200);
  // const debouncedFilters = useDebounce(filterValues, 250);
  const rowsPerPage = parseInt(params.limit);
  const pageNumber = parseInt(params.offset) / rowsPerPage;
  const displaySize = 'small';
  const server = {
    name: 'DCM4CHEE',
    wadoUriRoot: 'http://localhost:5000/wado',
    qidoRoot: 'http://localhost:5000/dicom-web',
    wadoRoot: 'http://localhost:5000/dicom-web',
    qidoSupportsIncludeField: true,
    imageRendering: 'wadors',
    thumbnailRendering: 'wadors',
    requestOptions: {
      requestFromBrowser: true,
    },
  };
  return getStudyList(
    server,
    filterValues,
    sort,
    rowsPerPage,
    pageNumber,
    displaySize
  );
};

export const actionGetStudiesFromParams = search => {
  return api({
    url: `/studylist/v2/search_param${search}`,
    method: 'GET',
    timeout: 30000,
  });
};

export const actionGetLinkViewport = StudyInstanceUID => {
  return api({
    url: `/backend/link_viewport/${StudyInstanceUID}`,
    method: 'GET',
    timeout: 30000,
  });
};

export const actionUnarchiveStudy = (data = {}) => {
  return api({
    url: '/backend/unarchive',
    method: 'POST',
    data,
  });
};

export const actionCallBatchAnalysis = (data = {}) => {
  return api({
    url: '/backend/diagnose_batch',
    method: 'POST',
    redirect: 'follow',
    data,
  });
};

export const actionGetDirectories = async (params = {}) => {
  const url = '/backend/directory/get';
  try {
    const { data } = await api({ url, method: 'GET', params });
    if (data) {
      actionSetExtensionData('studylist', {
        folder: Array.isArray(data) ? { directories: data } : data,
      });
    }
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const actionGetPanel = async (params = {}) => {
  const url = '/ui/panel/';
  try {
    const { data } = await api({ url, method: 'GET', params });
    if (data) {
      actionSetExtensionData('studylist', {
        panel: data,
      });
    }
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const actionGetBookmarkList = () => {
  return api({ url: '/backend/bookmark', method: 'GET' });
};

export const actionBookmarkStudies = (data = {}) => {
  return api({
    url: '/backend/bookmark/actions/add-studies',
    method: 'POST',
    data,
  });
};

export const actionUnbookmarkStudies = (data = {}) => {
  return api({
    url: '/backend/bookmark/actions/remove-studies',
    method: 'POST',
    data,
  });
};

export const actionShareStudies = (data = {}) => {
  return api({
    url: '/backend/share_study',
    method: 'POST',
    data,
  });
};

export const actionGetBookmarkDetail = bookmarkId => {
  return api({ url: '/backend/bookmark/' + bookmarkId, method: 'GET' });
};

export const actionCreateBookmark = (data = {}) => {
  return api({ url: '/backend/bookmark', method: 'POST', data });
};

export const actionUpdateBookmark = (data = {}) => {
  return api({ url: '/backend/bookmark/update', method: 'POST', data });
};

export const actionRemoveBookmark = (data = {}) => {
  return api({ url: '/backend/bookmark/remove', method: 'POST', data });
};

export const actionCopyStudyToFolder = (data = {}) => {
  return api({
    url: '/backend/directory/move_files',
    method: 'post',
    data,
  });
};

export const actionGetAttachFileList = StudyInstanceUID => {
  return api({
    baseURL: CONFIG_SERVER.BACKEND_URL.replace('/api', ''),
    url: `/nondicom/attachment/gallery/${StudyInstanceUID}`,
    method: 'GET',
  });
};

export const actionGetAttachFileSrc = path => {
  return api({
    baseURL: `${CONFIG_SERVER.BACKEND_URL.replace('/api', '')}/s3/nondicom`,
    url: '/' + path,
    method: 'GET',
    responseType: 'blob',
  });
};

export const actionDownloadAttachFile = (StudyInstanceUID, keys) => {
  return api({
    baseURL: `${CONFIG_SERVER.BACKEND_URL.replace('/api', '')}/s3/nondicom`,
    url: `/${tenant}/data/${StudyInstanceUID}/_download`,
    responseType: 'arraybuffer',
    method: 'POST',
    data: {
      downloadkeys: keys,
    },
  });
};

export const actionDeleteAttachFile = (StudyInstanceUID, keys) => {
  return api({
    baseURL: CONFIG_SERVER.BACKEND_URL.replace('/api', ''),
    url: `/nondicom/attachment/unattach/${StudyInstanceUID}`,
    method: 'POST',
    data: {
      deletekeys: keys,
    },
    redirect: 'follow',
  });
};

export const actionUploadAttachFile = (StudyInstanceUID, data) => {
  return api({
    baseURL: CONFIG_SERVER.BACKEND_URL.replace('/api', ''),
    url: `/nondicom/attachment/attach/${StudyInstanceUID}`,
    method: 'POST',
    data,
    redirect: 'follow',
  });
};

const getRelatedStudyUID = async (patientId, currentStudyUID) => {
  const relatedUID = [];
  const isArchived = study => {
    return (
      (study.ArchiveStatus &&
        study.ArchiveStatus.length &&
        study.ArchiveStatus[0]) ||
      (study.IsArchived && study.IsArchived.length && study.IsArchived[0])
    );
  };
  const isUnarchiving = study => {
    return (
      (study.ArchiveStatus &&
        study.ArchiveStatus.length &&
        study.ArchiveStatus[0] === 'UN_ARCHIVING') ||
      (study.InUnarchivingProgress &&
        study.InUnarchivingProgress.length &&
        study.InUnarchivingProgress[0])
    );
  };
  try {
    let relatedParams = {
      offset: 0,
      limit: 30,
      sort: '-StudyDate',
      query_string: `PatientID.keyword:"${patientId}" AND NOT StudyInstanceUID:"${currentStudyUID}"`,
    };
    const { data = {} } = await actionGetStudyList(relatedParams);

    if ((data.records || []).length > 0) {
      data.records.map(it => {
        if (!isArchived(it) && !isUnarchiving(it)) {
          relatedUID.push(it.StudyInstanceUID);
        }
      });
    }
  } catch (error) {}
  return relatedUID;
};

export const openViewer = async (
  routerBasename = '',
  StudyInstanceUID,
  PatientID
) => {
  if (!StudyInstanceUID) return;

  let relatedUID = [];
  let urlViewer = routerBasename;

  if (PatientID) {
    relatedUID = await getRelatedStudyUID(PatientID, StudyInstanceUID);
  }

  if (relatedUID.length > 0) {
    urlViewer += `viewer/${StudyInstanceUID};${relatedUID.join(';')}`;
  } else {
    urlViewer += `viewer/${StudyInstanceUID}`;
  }
  if (window.name === 'vindrViewerWindow') {
    window.name = '';
  }
  window.open(
    urlViewer,
    isMobile(window.innerWidth, window.innerHeight)
      ? '_self'
      : 'vindrViewerWindow'
  );
};
