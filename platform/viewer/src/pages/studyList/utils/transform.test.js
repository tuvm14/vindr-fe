import { toLuceneQueryString } from './transform';

describe('vindocStudyList transform', () => {
  describe('toLuceneQueryString', () => {
    test('returns correct lucene query string', () => {
      expect(
        toLuceneQueryString({
          StudyDate: 'abc',
          Modality: 'xyz',
        })
      ).toBe('StudyDate:abc AND Modality:xyz');
      expect(
        toLuceneQueryString({
          StudyDate: '[21312462 TO 42352346]',
          Modality: 'xyz',
        })
      ).toBe('StudyDate:[21312462 TO 42352346] AND Modality:xyz');
    });
  });
});
