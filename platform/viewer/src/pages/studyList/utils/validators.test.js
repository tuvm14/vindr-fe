import {
  isValidDateRange,
  isValidModality,
  isValidSearch,
  isValidVindocStatus,
  isValidSortField,
  isValidLimit,
  isValidOffset,
  isValidPage,
  isValidPagination,
  isValidParams,
  isValidSearchField,
  isValidSearchValue,
} from './validators';

describe('vindocStudyList validators', () => {
  describe('isValidModality', () => {
    test('returns FALSE if modality is not correct', () => {
      expect(isValidModality('')).toBe(false);
      expect(isValidModality('19750430')).toBe(false);
    });

    test('returns TRUE if correct modality', () => {
      expect(isValidModality('CR')).toBe(true);
      expect(isValidModality('MG')).toBe(true);
    });
  });

  describe('isValidDateRange', () => {
    test('returns FALSE if range is not correct', () => {
      expect(isValidDateRange('')).toBe(false);
      expect(isValidDateRange('-')).toBe(false);
      expect(isValidDateRange('19750430')).toBe(false);
      expect(isValidDateRange('19750430-')).toBe(false);
      expect(isValidDateRange('[wetwet-w3t34t3]')).toBe(false);
      expect(isValidDateRange('[19750430 TO w3t34t3]')).toBe(false);
    });

    test('returns TRUE if correct range', () => {
      expect(isValidDateRange('[19750430-20200514]')).toBe(true);
      // expect(isValidDateRange('[19750430-*]')).toBe(true);
    });
  });

  describe('isValidSearch', () => {
    test('returns FALSE if not correct search', () => {
      expect(isValidSearch({ PatientName1: '' })).toBe(false);
      expect(isValidSearch({ PatientID2: '' })).toBe(false);
    });
    test('returns TRUE if correct search', () => {
      expect(isValidSearch({ PatientName: '' })).toBe(true);
      expect(isValidSearch({ AccessionNumber: '' })).toBe(true);
    });
  });

  describe('isValidVindocStatus', () => {
    test('returns FALSE if not correct status', () => {
      expect(isValidVindocStatus('FAKE-STATUS')).toBe(false);
    });
    test('returns TRUE if correct status', () => {
      expect(isValidVindocStatus('*')).toBe(true);
      expect(isValidVindocStatus('NEW')).toBe(true);
      expect(isValidVindocStatus('APPROVED')).toBe(true);
    });
  });
});
