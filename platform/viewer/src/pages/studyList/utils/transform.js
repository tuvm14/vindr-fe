import map from 'lodash/map';
import join from 'lodash/join';

export const toLuceneQueryString = queryObject => {
  return join(
    map(queryObject, (value, field) => `${field}:${value || '*'}`),
    ' AND '
  );
};
