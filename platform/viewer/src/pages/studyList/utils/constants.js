import moment from 'moment';
import { BASE_TIME } from '../../../utils/constants';
import { isThaiThuyHospital } from '../../../utils/helper';

export const ROWS_PER_PAGE = [25, 50, 100, 150, 200, 250, 500, 750, 1000];
export const FILTER_FIELDS = [
  'ModalitiesInStudy',
  'VindocStatus',
  'AccessionNumber',
  'IsAbnormal',
  'startDate',
  'endDate',
];
export const SORT_FIELDS = [
  'PatientName',
  'PatientID',
  'AccessionNumber',
  'PatientSex',
  'PatientAge',
  'BodyPartExamined',
  'ModalitiesInStudy',
  'StudyDate',
  'VindocAIResult',
  'TimeInserted',
];
export const SEARCH_FIELDS = [
  'All',
  'PatientName',
  'PatientID',
  'BodyPartExamined',
];
export const VINDOC_STATUSES = [
  '*',
  'NEW',
  'APPROVED',
  'PENDING',
  'READING',
  'SUBMITTED',
];
export const MODALITIES = [
  '*',
  'CR',
  'DX',
  'DR',
  'CT',
  'MR',
  'MG',
  'US',
  'ES',
  'PT',
  'ST',
  'XA',
  'SR',
  'SM',
  'Other',
];
export const AI_RESULTS = [
  { label: 'AI Results', value: '*' },
  { label: 'Normal', value: 'false' },
  { label: 'Abnormal', value: 'true' },
];

const today = moment().format('YYYYMMDD');

const filterStartDate = isThaiThuyHospital()
  ? BASE_TIME
  : process.env.FILTER_START_DATE || today;

export const INITIAL_FILTERS = {
  VindocStatus: '*',
  startDate: filterStartDate,
  endDate: today,
  ModalitiesInStudy: MODALITIES[0],
  limit: ROWS_PER_PAGE[0],
  offset: 0,
  page: 0,
  sort: '-StudyDate',
  AccessionNumber: '',
  IsAbnormal: AI_RESULTS[0].value,
};

export const STUDY_DATETIME = {
  STUDY_DATE: 'StudyDate',
  STUDY_TIME: 'StudyTime',
};

export const ENTER_KEY = 13;

export const DIAGNOSE_MODEL = [
  { name: 'Chest X-ray', action: 'chestxray', isActive: true },
  { name: 'Mammography', action: 'mammogram', isActive: true },
  { name: 'Lung CT', action: 'lungct', isActive: true },
  { name: 'Liver CT', action: 'liverct', isActive: true },
  { name: 'Brain CT', action: 'brainct', isActive: true },
  { name: 'Spine XR', action: 'spinexr', isActive: true },
  { name: 'Brain MRI', action: 'brainmri', isActive: true },
];
