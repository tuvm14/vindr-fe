import isInteger from 'lodash/isInteger';
import forEach from 'lodash/forEach';
import toNumber from 'lodash/toNumber';
import words from 'lodash/words';
import replace from 'lodash/replace';
import moment from 'moment';
import {
  MODALITIES,
  ROWS_PER_PAGE,
  SEARCH_FIELDS,
  SORT_FIELDS,
  VINDOC_STATUSES,
} from './constants';

export function isValidModality(modality) {
  if (!Array.isArray(modality)) return false;
  for (let i; i < modality.length; i++) {
    if (MODALITIES.indexOf(modality[i]) === -1) return false;
  }
  return true;
}

export function isValidDateRange(dateRange) {
  const [startDate, endDate] = words(dateRange, /[0-9]{8}/gm);

  let result = false;

  if (
    moment(startDate, 'YYYYMMDD').isValid() &&
    moment(endDate, 'YYYYMMDD').isValid()
  ) {
    result = true;
  }
  return result;
}

export function isValidSearchField(field) {
  return SEARCH_FIELDS.indexOf(field) !== -1;
}

export function isValidSortField(field) {
  return SORT_FIELDS.indexOf(replace(field, '-', '')) !== -1;
}

export function isValidSearchValue(value) {
  return typeof value === 'string';
}

export function isValidSearch(search) {
  let result = true;
  forEach(search, (value, field) => {
    if (!isValidSearchField(field) || !isValidSearchValue(value)) {
      result = false;
    }
  });
  return result;
}

export function isValidPage(page) {
  return isInteger(toNumber(page));
}

export function isValidOffset(offset) {
  return toNumber(offset) % ROWS_PER_PAGE[0] === 0;
}

export function isValidLimit(limit) {
  return ROWS_PER_PAGE.indexOf(toNumber(limit)) !== -1;
}

export function isValidVindocStatus(status) {
  return VINDOC_STATUSES.indexOf(status) !== -1;
}

export function isValidPagination(pagination) {
  return isValidOffset(pagination.offset) && isValidLimit(pagination.limit);
}

export function isValidParams(params) {
  return (
    isValidModality(params.ModalitiesInStudy) &&
    isValidDateRange(params.StudyDate)
  );
}
