import React from 'react';
import { Link } from 'react-router-dom';
import { Typography } from 'antd';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import Header from '../components/Header';
import { FullScreenLoading } from '../components/Customize/FullScreenLoading';
import './NotFound/NotFound.css';
import { isEmpty } from 'lodash-es';
import { LockOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

export default function NoPermission({
  message = "Sorry, you don't have permission to view this page.",
  showGoBackButton = true,
}) {
  const { t } = useTranslation('Vindoc');
  const user = useSelector(state => state.extensions.user);
  if (isEmpty(user)) {
    return <FullScreenLoading />;
  }

  return (
    <>
      <Header />
      <div className="not-found text-center">
        <div>
          <LockOutlined style={{ fontSize: 35 }} />
          <Typography style={{ fontSize: 24 }} className="error-message">
            {message}
          </Typography>
          {showGoBackButton && (
            <Typography style={{ fontSize: 14 }}>
              <Link to={'/'}>{t('Go back Home')}</Link>
            </Typography>
          )}
        </div>
      </div>
    </>
  );
}

NoPermission.propTypes = {
  message: PropTypes.string,
  showGoBackButton: PropTypes.bool,
};
