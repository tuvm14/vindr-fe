import React, { useEffect } from 'react';

import './antd-dark.less';
import './theme-tide.css';

const DarkApp = ({ children }) => {
  useEffect(() => {
    document.body.classList.remove('light-theme');
    document.body.classList.add('dark-theme');
  }, []);
  return <>{children}</>;
};

export default DarkApp;
