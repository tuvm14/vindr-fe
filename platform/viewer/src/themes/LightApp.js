import React, { useEffect } from 'react';

import './antd-light.less';
import './theme-light.css';

const LightApp = ({ children }) => {
  useEffect(() => {
    document.body.classList.remove('dark-theme');
    document.body.classList.add('light-theme');
  }, []);
  return <>{children}</>;
};

export default LightApp;
