import React, { useState } from 'react';
import DarkApp from './DarkApp';
import LightApp from './LightApp';

const ThemeProvider = ({ children }) => {
  const [isDark] = useState(true);

  return (
    <>
      {isDark ? <DarkApp>{children}</DarkApp> : <LightApp>{children}</LightApp>}
    </>
  );
};

export default ThemeProvider;
