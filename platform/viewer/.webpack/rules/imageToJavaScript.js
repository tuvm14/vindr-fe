/**
 * For CommonJS, we want to bundle whatever font we've landed on. This allows
 * us to reduce the number of script-tags we need to specify for simple use.
 *
 * PWA will grab these externally to reduce bundle size (think code split),
 * and cache the grab using service-worker.
 */
const imageToJavaScript = {
  test: /\.(png|jpe?g|gif)$/i,
  use: [
    {
      loader: 'file-loader?name=/public/assets/images/[name].[ext]',
    },
  ],
};

module.exports = imageToJavaScript;
