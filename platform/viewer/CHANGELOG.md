# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 5.0.0 (2022-07-14)


### Bug Fixes

* 🐛 check series has abnormal ([bf601c0](https://github.com/OHIF/Viewers/commit/bf601c06d7aa8533dc30b66368e6beda2fd860bf))
* 🐛 load assset tiny mce ([b411ad6](https://github.com/OHIF/Viewers/commit/b411ad61f1b1c2da7e65272b4fea55f2cf330b78))
* 🐛 resolve import constants ([834f41e](https://github.com/OHIF/Viewers/commit/834f41e58f22e7650f71b642b9bb8ea5fda00c9c))
* 🐛 rm polling timeout ([1238100](https://github.com/OHIF/Viewers/commit/123810030e7f8b0aaa18a8ecb54b0e50d0012a8e))
* 🐛 styling text color ([36508ef](https://github.com/OHIF/Viewers/commit/36508efedb368024e93c505aa52e56dcc267e883))


### Features

* 🎸 add action pin and unpin ([34255b9](https://github.com/OHIF/Viewers/commit/34255b913ec1587ee3c2e9d22c27ee690906f6d8))
* 🎸 add active template ([4e44b1b](https://github.com/OHIF/Viewers/commit/4e44b1b0b48902b909559fc3017833ce446904d2))
* 🎸 add ant design ([eaaa19e](https://github.com/OHIF/Viewers/commit/eaaa19ebd25f783c3e079230bab9a38481f1054d))
* 🎸 add checkbox create folder ([9dd5430](https://github.com/OHIF/Viewers/commit/9dd5430bea142d8f4a998be728f58dd08ef3b688))
* 🎸 add contrast theme ([bf6399e](https://github.com/OHIF/Viewers/commit/bf6399e23a63ca4c6820df3ddf2df2b2b00f971b))
* 🎸 add default content ([518ff20](https://github.com/OHIF/Viewers/commit/518ff20abe899b60ce20276949c356227bfde7e0))
* 🎸 add delete icon ([f44871e](https://github.com/OHIF/Viewers/commit/f44871ec990ae81024f3eed17690409e37f1be79))
* 🎸 add doctor actions ([252133f](https://github.com/OHIF/Viewers/commit/252133f2ae0ce5768335fd07a846e73fb5c3c921))
* 🎸 add dynamic form ([fb9c836](https://github.com/OHIF/Viewers/commit/fb9c836b967b6856949fffbd21734058089209d2))
* 🎸 add folder and folder type ([275b7c5](https://github.com/OHIF/Viewers/commit/275b7c5ad721f4ef72645e8fb0175ac90dc1adf7))
* 🎸 add function cancel text editor ([0924ec4](https://github.com/OHIF/Viewers/commit/0924ec4559e6841071f004438c5a7ef8b76b22f8))
* 🎸 add menu list and collapse action ([b7debfc](https://github.com/OHIF/Viewers/commit/b7debfc980c5b958082a7e7b84956096a34f8fc5))
* 🎸 add new button icons ([e950b6e](https://github.com/OHIF/Viewers/commit/e950b6e22cae8701b7ebff84c10ec445ca45a719))
* 🎸 add new doctor tab ([93c6545](https://github.com/OHIF/Viewers/commit/93c6545cbfa1fa959bcbf986558dc9df4a8659b7))
* 🎸 add new tab panels ([1d765bc](https://github.com/OHIF/Viewers/commit/1d765bc62bf7e8a3454c4cf34a7b5988ac832e83))
* 🎸 add notification message ([a008ffd](https://github.com/OHIF/Viewers/commit/a008ffd5652981cdf94f90ec2ec49e9086652b0d))
* 🎸 add notify message for doctor action ([1328457](https://github.com/OHIF/Viewers/commit/1328457ec722774f9cd5f5f727145f4824ca8273))
* 🎸 add order information ([6cf9ac3](https://github.com/OHIF/Viewers/commit/6cf9ac3109a7e8d494c36f56da4ba4e2294ab1f1))
* 🎸 add patient content ([3e06bcf](https://github.com/OHIF/Viewers/commit/3e06bcfbf8c46b12d2ecab55b54469f5fd8d046e))
* 🎸 add plugin text editor ([1a10697](https://github.com/OHIF/Viewers/commit/1a1069756bd2b7c83b53e30208b6b41e0a0f12d8))
* 🎸 add preview html ([d677081](https://github.com/OHIF/Viewers/commit/d677081b7dbfdd99210235a044d1f5da8fb9a756))
* 🎸 add printing screen ([7ae263a](https://github.com/OHIF/Viewers/commit/7ae263af1f3623feafde00c531cbccb3879247c6))
* 🎸 add report content ([860eae6](https://github.com/OHIF/Viewers/commit/860eae66e413e6cbd23020a3c72e7b0e9cfb94c9))
* 🎸 add report printing screen ([f84bacf](https://github.com/OHIF/Viewers/commit/f84bacfc147df91677434f645bc501654b1c7334))
* 🎸 add right form panel ([63cfd60](https://github.com/OHIF/Viewers/commit/63cfd60ba71826b38e9651dfda17199ef5b3f999))
* 🎸 add right panel ([c961fa1](https://github.com/OHIF/Viewers/commit/c961fa17ad5ee1ff5fdb8e81a446a24244883290))
* 🎸 add sidebar ([fdf9151](https://github.com/OHIF/Viewers/commit/fdf915178d2bd866ef65b836a4e8f2640e9dbcd1))
* 🎸 add study approved ([4bf8350](https://github.com/OHIF/Viewers/commit/4bf8350d5f368d4077bf11bc4ff40853cb6e621e))
* 🎸 add styling sidebar ([3fe2ae2](https://github.com/OHIF/Viewers/commit/3fe2ae2a3edcea80cb85d9d40f72fe110ac82a6d))
* 🎸 add text editor ([aaa64c8](https://github.com/OHIF/Viewers/commit/aaa64c83bb3fe9e9714f43e198270971bec4dea5))
* 🎸 add there is no data ([a6327f3](https://github.com/OHIF/Viewers/commit/a6327f384ac8753c26c0a13ebc3c9eba632d49e0))
* 🎸 approve and revoke approval ([eb09f84](https://github.com/OHIF/Viewers/commit/eb09f841e6f7615a68639558b037c25f1de6b426))
* 🎸 change buttons color theme ([1140b74](https://github.com/OHIF/Viewers/commit/1140b7410a26d1508b1a9518c488bc6a9a63ffea))
* 🎸 change margin between sections ([d1ea0a2](https://github.com/OHIF/Viewers/commit/d1ea0a217541d0cc1b99706b179844f06cb6211d))
* 🎸 clearmeasurement in when change to doctor tab ([b74a312](https://github.com/OHIF/Viewers/commit/b74a312ccd7368e698335e61a130fde4e7a42586))
* 🎸 close popup create folder ([fda956e](https://github.com/OHIF/Viewers/commit/fda956eb0a1e14314ab3679472df3ba331f796dd))
* 🎸 disable approve button with last action ([8fc4a8b](https://github.com/OHIF/Viewers/commit/8fc4a8b5049a7184bc69bea5295b0ea2232c768c))
* 🎸 disable editor ([60bc6ae](https://github.com/OHIF/Viewers/commit/60bc6ae73cb9d90bf211c9d2bc67c995de3a6215))
* 🎸 draw bouding box ([ccde400](https://github.com/OHIF/Viewers/commit/ccde4008476a7b6b2fe8427e1ace05bfc6152048))
* 🎸 draw measurements from doctor tab ([68bac6d](https://github.com/OHIF/Viewers/commit/68bac6dc044122e285f44c3859537d5a0b44857b))
* 🎸 dynamic form ([3dbf405](https://github.com/OHIF/Viewers/commit/3dbf405cc368e68c4cc92906e31bc22ad4a7398b))
* 🎸 fix approve status ([c67b88b](https://github.com/OHIF/Viewers/commit/c67b88bca1d9e7746e1984d89316d7db51162a67))
* 🎸 format filter name and order by alphbet ([85f1dd7](https://github.com/OHIF/Viewers/commit/85f1dd7132037bf2e578941766052f1dbef264e3))
* 🎸 get templates ([48d793d](https://github.com/OHIF/Viewers/commit/48d793dd68b1f1027b0e3c66dcfdfa5b53191be4))
* 🎸 get tenant info ([28b46b7](https://github.com/OHIF/Viewers/commit/28b46b7cef18c95a1ab3b5cf60406c01d3f12f14))
* 🎸 git commit -m "save text in description" ([906a365](https://github.com/OHIF/Viewers/commit/906a3650e960887b099f94d770d36288a279391f))
* 🎸 handle action CANCEL ([d8646cc](https://github.com/OHIF/Viewers/commit/d8646cc4397eddc0e04bd318a05f514d19236cef))
* 🎸 handle draw measurements ([3ef51e8](https://github.com/OHIF/Viewers/commit/3ef51e854aaeab028ff5cd890fa22cc2737ff8c2))
* 🎸 hide sidebar ([ef9ce4a](https://github.com/OHIF/Viewers/commit/ef9ce4a692be5365afc7d8c1de65d3206f025130))
* 🎸 hide studylist layout ([29ec10b](https://github.com/OHIF/Viewers/commit/29ec10b5fa60996c8e89990e4aff0777a36a60a3))
* 🎸 hot fix cancel text editor ([0dfcf6b](https://github.com/OHIF/Viewers/commit/0dfcf6bac0a2a07634299f9db3ef5e7778fd4c89))
* 🎸 integrate api save create folder ([c613e73](https://github.com/OHIF/Viewers/commit/c613e73fe18de7dfde03d2b4be72f80289ddb0aa))
* 🎸 integrate folder api query ([eb27b85](https://github.com/OHIF/Viewers/commit/eb27b8566fd5998fea70ab032d49f341b7c8097b))
* 🎸 load measurement from new panel tab ([bcce7f6](https://github.com/OHIF/Viewers/commit/bcce7f6e6379c44b1cbf019437bf55f0b208c881))
* 🎸 load text html from storage ([5deb182](https://github.com/OHIF/Viewers/commit/5deb182bdfdb74391a887f07a7630ec5f2d27641))
* 🎸 prevent re render panel tab ([aff98de](https://github.com/OHIF/Viewers/commit/aff98de24eaea09fa61e8a75213a8bcb192163f0))
* 🎸 reset button ([64bea09](https://github.com/OHIF/Viewers/commit/64bea09385d985f2c9a406d821b0cecee9851b54))
* 🎸 reset table style ([3e4561d](https://github.com/OHIF/Viewers/commit/3e4561d705286039a9df1282962c30f28bdb84d4))
* 🎸 reset template on select ([a28885c](https://github.com/OHIF/Viewers/commit/a28885c3ce1be22bbb4af3bee8f868fb7f4b4ca7))
* 🎸 responsive doctor tab ([d230dcc](https://github.com/OHIF/Viewers/commit/d230dcc2571bc354e2e38624894365c9fc8a24ea))
* 🎸 rm chest & ammo screen ([a0b4132](https://github.com/OHIF/Viewers/commit/a0b4132aa98cf784624feeaca02a7b6dc047ea83))
* 🎸 rm disable text editor ([1f951b6](https://github.com/OHIF/Viewers/commit/1f951b646c81158ca8b0c5a1c280079558c69e7a))
* 🎸 rm import 'react-draft-wysiwyg/dist/react-draft-wysiwyg ([609070f](https://github.com/OHIF/Viewers/commit/609070fbce8d252d5938afca268b7ed5228d6551))
* 🎸 rm loading in editor container ([ad79487](https://github.com/OHIF/Viewers/commit/ad794871a6359572eb8a7256c8d8c790f9e7d6e5))
* 🎸 save text editor into redux ([5bfa30b](https://github.com/OHIF/Viewers/commit/5bfa30b7be0a97ac4cd912bd11d7992c476b50ee))
* 🎸 set active template ([ab8f905](https://github.com/OHIF/Viewers/commit/ab8f905f15ac95abdf1f69a778a71447186422d4))
* 🎸 set Default template to first of list ([b4747a0](https://github.com/OHIF/Viewers/commit/b4747a0f0005053891ff98bbd8a5dbe59f888a65))
* 🎸 set isNearTool false ([8cb81a9](https://github.com/OHIF/Viewers/commit/8cb81a9f5f1b37b618e0231c6fea2fc024f90bd8))
* 🎸 set loading when load text editor ([81d6162](https://github.com/OHIF/Viewers/commit/81d6162e321f829a35dee5fa5cbf067f55a948ba))
* 🎸 set margin button form row ([7b9fd88](https://github.com/OHIF/Viewers/commit/7b9fd8850053e800790a3ca51f22fe2abd28e129))
* 🎸 set max height content description ([f65364d](https://github.com/OHIF/Viewers/commit/f65364db58d3f774860c97228defe68e59bc8f7e))
* 🎸 set max with preview content ([d8b1472](https://github.com/OHIF/Viewers/commit/d8b1472a19226a90bdb2c5a1ccf8c4284e13ae4b))
* 🎸 set min width 500px ([d1b0145](https://github.com/OHIF/Viewers/commit/d1b0145662a6940bdb63032097d05c0d58ad0c6c))
* 🎸 set pin folder default false ([f9c4186](https://github.com/OHIF/Viewers/commit/f9c41863ec7e9a738a68084451f743e293437dcc))
* 🎸 set position fixed create folder button ([88b96cf](https://github.com/OHIF/Viewers/commit/88b96cfe0e99ee21ace127bc7796b4fe8380c4d0))
* 🎸 show limit toolbar ([36c20ee](https://github.com/OHIF/Viewers/commit/36c20eeaf6790b0dae9ab6977d0561584be67a20))
* 🎸 show studies for all time ([8dfc300](https://github.com/OHIF/Viewers/commit/8dfc3008ca38a3979169546b48142c81a9811c7e))
* 🎸 showing auto rule by checkbox ([0a6b5ee](https://github.com/OHIF/Viewers/commit/0a6b5ee2e05c679c7adb5ed204fc0acd53060e99))
* 🎸 split file action to styles file ([208996e](https://github.com/OHIF/Viewers/commit/208996e06de62d88cc9c7494622da69b3f6ab63e))
* 🎸 styling create button ([833ae33](https://github.com/OHIF/Viewers/commit/833ae33499ad57dc92f45153ac1682bc6b861a1c))
* 🎸 styling dynamic form filter ([2d39605](https://github.com/OHIF/Viewers/commit/2d396054c0d9f623c74e17587156f9093869db91))
* 🎸 styling padding 8px patient ([2790d73](https://github.com/OHIF/Viewers/commit/2790d73c93190befb53c68ff791a955e8c55663c))
* 🎸 styling panel studylist ([91cc576](https://github.com/OHIF/Viewers/commit/91cc5763634c62999ab2b1dc50597f73aec19161))
* 🎸 styling preview screen table ([5702d7f](https://github.com/OHIF/Viewers/commit/5702d7fd3f6ec7e40b516a34729b7c6b2ee93cbb))
* 🎸 styling sidebar collapse button ([c75f6bf](https://github.com/OHIF/Viewers/commit/c75f6bf3f9f47ef8f9bba581b2ae0df1ec87a806))
* 🎸 styling studylist with ant design ([2eab77f](https://github.com/OHIF/Viewers/commit/2eab77fc7cef4fd6b6113b41fab828d5a8cbd906))
* 🎸 styling table header ([4d74c97](https://github.com/OHIF/Viewers/commit/4d74c97e345d3eda45f207ad983306c7bc40c780))
* 🎸 styling template selections ([14b4b8b](https://github.com/OHIF/Viewers/commit/14b4b8b8830d6688a44be89fe9409e7c1c3ef00b))
* 🎸 styling vindoc component ([7600e2c](https://github.com/OHIF/Viewers/commit/7600e2c6a20f10f6f93100e7d571a0a7640bc2aa))
* 🎸 tabname ([87d3acc](https://github.com/OHIF/Viewers/commit/87d3accce31c89aacdb6fef237610363e6b36ac8))
* 🎸 trim value data ([6f291e3](https://github.com/OHIF/Viewers/commit/6f291e3ecfd10b8299af04a9da13d3cf58446c78))
* 🎸 update active child tempalte ([09ed07e](https://github.com/OHIF/Viewers/commit/09ed07e5c7bf8b1be5f2ae50a0faea802f3d8271))
* 🎸 update order folders ([28846dd](https://github.com/OHIF/Viewers/commit/28846dd96b88fb2ec0519fadb155d64ab282aab6))
* 🎸 update select template ([fe9afa5](https://github.com/OHIF/Viewers/commit/fe9afa5a5c48b936b1d4ea870c446f40ce9a5852))
* 🎸 update stuylist view ([0f92caa](https://github.com/OHIF/Viewers/commit/0f92caae22dde737a07417ca3fe65f7567d133a9))
* 🎸 update tinymce ([d0f6566](https://github.com/OHIF/Viewers/commit/d0f656641ec039ff5a900c94080193d6b0c18abb))
* 🎸 update verion vindr tools 1.0.8 ([9ac66d8](https://github.com/OHIF/Viewers/commit/9ac66d85a78bef041db28e8857cfeb52d218086e))
* 🎸 using ant design left menu ([1f5079f](https://github.com/OHIF/Viewers/commit/1f5079f2496e9609ff105ee812707a0aa6bab8a2))
* 🎸 using default form text editor ([885f5ca](https://github.com/OHIF/Viewers/commit/885f5ca7e9bca9ffe64c50edb7e64b983569c27c))
* 🎸 using default template from api ([1662d88](https://github.com/OHIF/Viewers/commit/1662d88f69d86dc543b1e7b4d772edd9ab831698))
* 🎸 validate measurements ([01440b7](https://github.com/OHIF/Viewers/commit/01440b796cd7d420d4c21b9803dfd6eba6b659a5))
* 🎸 validate select public folder ([0b07e94](https://github.com/OHIF/Viewers/commit/0b07e94dc004dcff6f6f3552809ec8d39853f22d))



## 1.1.1 (2021-01-23)


### Bug Fixes

* 🐛 redirect url callback ([e1b1671](https://github.com/OHIF/Viewers/commit/e1b1671b4391bf77793a0e101f75456fcf31b4c0))


### Features

* 🎸 refresh token redirect url ([3b065a5](https://github.com/OHIF/Viewers/commit/3b065a51fd0159fba081002453e4d6a23695fd23))



# 1.1.0 (2021-01-20)


### Features

* 🎸 order by study date and study time ([b950a0c](https://github.com/OHIF/Viewers/commit/b950a0ccd0bb5c14db8ebe5aa6a44d303db4b46e))
* 🎸 rm sort after searching ([5582cfc](https://github.com/OHIF/Viewers/commit/5582cfcc8cab077f8caebd8bf63c37ab28d58559))
* 🎸 show label title ([f53f93b](https://github.com/OHIF/Viewers/commit/f53f93bf6f9a74b4682bf0292ca0fadd18f38db9))
* 🎸 show sort ([aa8a332](https://github.com/OHIF/Viewers/commit/aa8a332d12916f0e3b92e369f858b2d4c07dbf6d))
* 🎸 show study date & study time ([0c6e37f](https://github.com/OHIF/Viewers/commit/0c6e37fc42677acc57a5860b1fe89740ceebeabf))



## 1.0.7 (2021-01-13)


### Bug Fixes

* 🐛 fix patientname undefined ([8ad3499](https://github.com/OHIF/Viewers/commit/8ad34997dd51fed262f9b5bbd3b5ba9c053d00ce))
* 🐛 hot fix endpoint  permission token url ([890f161](https://github.com/OHIF/Viewers/commit/890f161a54de09938f4f2224159ae48858ee58bb))



## 1.0.6 (2021-01-08)


### Bug Fixes

* 🐛 fix localization setting ([87c7009](https://github.com/OHIF/Viewers/commit/87c70099a3429db972dddda243f205b74368bb8d))


### Features

* 🎸 localization liver disease name ([123df2d](https://github.com/OHIF/Viewers/commit/123df2d9a8815645f86ae3a16762ed519e5feb1f))
* 🎸 re-order menu buttons ([8ef3b58](https://github.com/OHIF/Viewers/commit/8ef3b582052cb7d81588dac6fd7504fb497f6fa3))
* 🎸 replace ^ by space ([a266218](https://github.com/OHIF/Viewers/commit/a26621830fc20c821e99fee0a9fc812be37e2dbb))
* 🎸 replace ^ patient name ([fd4c554](https://github.com/OHIF/Viewers/commit/fd4c554c36915609c40e429a8ccdb41b6ee04957))
* 🎸 rm box number in label box ([01b0248](https://github.com/OHIF/Viewers/commit/01b0248abc4777db616ce12bfc42332d7ce5b8a6))
* 🎸 show density conclusion ([357fbdf](https://github.com/OHIF/Viewers/commit/357fbdf6ec3dcecc5b893e2a337aad6fde78ddb3))
* 🎸 yarn cm ([a8a4204](https://github.com/OHIF/Viewers/commit/a8a42044082ddbf1a2177165c7f5394e4668bd8f))



## 1.0.4 (2020-12-31)


### Bug Fixes

* [#1075](https://github.com/OHIF/Viewers/issues/1075) Returning to the Study List before all series have finishe… ([#1090](https://github.com/OHIF/Viewers/issues/1090)) ([ecaf578](https://github.com/OHIF/Viewers/commit/ecaf578f92dc40294cec7ff9b272fb432dec4125))
* [#1312](https://github.com/OHIF/Viewers/issues/1312) Cine dialog remains on screen ([#1540](https://github.com/OHIF/Viewers/issues/1540)) ([7d22bb7](https://github.com/OHIF/Viewers/commit/7d22bb7d5a8590cffc169725c93942f758fe13a0))
* @ohif/viewer package build ([4aa7cbd](https://github.com/OHIF/Viewers/commit/4aa7cbd6416a0cd70a03aa1f071c083e3a7a5fa2))
* `showStudyList` config ([#1647](https://github.com/OHIF/Viewers/issues/1647)) ([d9fc7bb](https://github.com/OHIF/Viewers/commit/d9fc7bbb0e6d868f507c515f031aaf88a2353e2f))
* 🎸 switch ohif logo from text + font to SVG ([#1021](https://github.com/OHIF/Viewers/issues/1021)) ([e7de8be](https://github.com/OHIF/Viewers/commit/e7de8be2d8164ca5b9ea6299d7d39bcd9790164e))
* 🐛 - Put guards in all places that a cornerstone re-render ([#1899](https://github.com/OHIF/Viewers/issues/1899)) ([451f7ea](https://github.com/OHIF/Viewers/commit/451f7eab9258e7a193eb362e0926b13aedc4b3c9))
* 🐛 1241: Make Plugin switch part of ToolbarModule ([#1322](https://github.com/OHIF/Viewers/issues/1322)) ([6540e36](https://github.com/OHIF/Viewers/commit/6540e36818944ac2eccc696186366ae495b33a04)), closes [#1241](https://github.com/OHIF/Viewers/issues/1241)
* 🐛 able to sort by -TimeInserted as default ([61cbb41](https://github.com/OHIF/Viewers/commit/61cbb415f66d82a4dba013d32fe1b818f634cbe3))
* 🐛 Activating Pan and Zoom on right and middle click by def ([#841](https://github.com/OHIF/Viewers/issues/841)) ([7a9b477](https://github.com/OHIF/Viewers/commit/7a9b477609943efd13e46c912ea1f8f59816124a))
* 🐛 Add DicomLoaderService & FileLoaderService to fix SR, PDF, and SEG support in local file and WADO-RS-only use cases ([#862](https://github.com/OHIF/Viewers/issues/862)) ([e7e1a8a](https://github.com/OHIF/Viewers/commit/e7e1a8a6cdfcc333c7d2723e156a2760f8fa722e)), closes [#838](https://github.com/OHIF/Viewers/issues/838)
* 🐛 add missing column - IimeInserted ([0d25f10](https://github.com/OHIF/Viewers/commit/0d25f10479ea7065e09c8ed6af1fdb578570a94b))
* 🐛 add new logo vinrad ([5515b47](https://github.com/OHIF/Viewers/commit/5515b47887746ef4150190a608edd4edcd1496a9))
* 🐛 add new tab REJECTED and translations ([6cef715](https://github.com/OHIF/Viewers/commit/6cef715c6596454b65366a26f5cc19fd71ee25f1))
* 🐛 add spacing top & bottom between Chip ([6721dbe](https://github.com/OHIF/Viewers/commit/6721dbea11f6a2378435d8612c1995c7ea0c5ac8))
* 🐛 add tab active background color ([e26c7ba](https://github.com/OHIF/Viewers/commit/e26c7ba7f972824012ef8db7431575923914d039))
* 🐛 add timepoint manager ([dd7ec71](https://github.com/OHIF/Viewers/commit/dd7ec711d7d06122729cf2dde25dc72264a007e2))
* 🐛 apply /doctor/study result for modality:CR ([7dc69de](https://github.com/OHIF/Viewers/commit/7dc69dea2aab7ff948b4d54677145a1e79495a16))
* 🐛 change default message measurement box ([fe10a62](https://github.com/OHIF/Viewers/commit/fe10a629ede8d7ab899de44fdd6018a2bf096861))
* 🐛 change done icon ([e298470](https://github.com/OHIF/Viewers/commit/e298470d0db618177117af368023d0b3f3f64351))
* 🐛 change empty message ([0873fe5](https://github.com/OHIF/Viewers/commit/0873fe5e2148231435d148ec280040eb6761396d))
* 🐛 change menu link to button ([a31ff6f](https://github.com/OHIF/Viewers/commit/a31ff6f729df0bc7c7a056b11d8a7348deaecfbf))
* 🐛 change table row selected and hover color ([43b0635](https://github.com/OHIF/Viewers/commit/43b063519c120fdf01c10ca6062e39c4ec180c77))
* 🐛 change text of empty description section ([7437db6](https://github.com/OHIF/Viewers/commit/7437db6d52949f587e309cbfec8748dd77c140de))
* 🐛 check init mammo success ([70209d8](https://github.com/OHIF/Viewers/commit/70209d8d1511727114a9d0bae7237929ccd45840))
* 🐛 check load metadata in Mammo Processing ([dd8ab59](https://github.com/OHIF/Viewers/commit/dd8ab596b454c6e937fe7d4d39abf1885195da63))
* 🐛 check mammo position ([39dfdff](https://github.com/OHIF/Viewers/commit/39dfdff15dd2fa54b85a6265faaef4b4c10e7a24))
* 🐛 clear all annotation in chest ([82a03a2](https://github.com/OHIF/Viewers/commit/82a03a2aa099d69569beb4fb186efd6ada45abca))
* 🐛 clear selected items on batch actions ([f33f5c5](https://github.com/OHIF/Viewers/commit/f33f5c52a6f825daa718177bd8bab2c8405313e6))
* 🐛 clicking on tab name ([7c6d299](https://github.com/OHIF/Viewers/commit/7c6d299de430ed2ffef0f48ab1b817e79bbf6493))
* 🐛 correct Assign doctor translation ([f060fa2](https://github.com/OHIF/Viewers/commit/f060fa212d9adcfe5a5cceba34b2fb2492688010))
* 🐛 correct condition to not send search ([e1bf5be](https://github.com/OHIF/Viewers/commit/e1bf5bed0d471cf42ca824b46ffd491a26f201ea))
* 🐛 correct Ctrl or Shift is stuck ([12d1dd9](https://github.com/OHIF/Viewers/commit/12d1dd943ddf1c6b3f7622bc0e3eacaef215952a))
* 🐛 correct displaying diseases ([8b101e9](https://github.com/OHIF/Viewers/commit/8b101e9e887cd1f72057b20b71c71f3556f97fb7))
* 🐛 correct last day in range ([79c5603](https://github.com/OHIF/Viewers/commit/79c56036f7cabf82b741f8c86548728c204e1e92))
* 🐛 correct list of filter fields as default ([7fda2f3](https://github.com/OHIF/Viewers/commit/7fda2f3b49752df33aa2dff918f64c495fc71d7f))
* 🐛 Correct lodash/isEmpty again ([91c8bba](https://github.com/OHIF/Viewers/commit/91c8bba7c87ee50bc51663030312ef5805c57e2e))
* 🐛 Correct sort icon and direction ([c955381](https://github.com/OHIF/Viewers/commit/c955381bcc7efc0646281ccf2eb6d3d771e63c52))
* 🐛 correct unit tests ([cb7f3e3](https://github.com/OHIF/Viewers/commit/cb7f3e3d8d81b0292ac0d541f1f683a4b29fb7ec))
* 🐛 correct using Shift right after refresh page ([1e3b123](https://github.com/OHIF/Viewers/commit/1e3b1235f47277fce802569aa5ca3c6e23082f2a))
* 🐛 correctly display image for upload buttons ([3127b03](https://github.com/OHIF/Viewers/commit/3127b03e110001d4290e2e55bb2e5798ae52817f))
* 🐛 Desc of meas.table not being updated on properly ([#1094](https://github.com/OHIF/Viewers/issues/1094)) ([85f836c](https://github.com/OHIF/Viewers/commit/85f836cd918614be722fce1bff2373460ec4900b)), closes [#1013](https://github.com/OHIF/Viewers/issues/1013)
* 🐛 Disable seg panel when data for seg unavailable ([#1732](https://github.com/OHIF/Viewers/issues/1732)) ([698e900](https://github.com/OHIF/Viewers/commit/698e900b85121d3c2a46747c443ef69fb7a8c95b)), closes [#1728](https://github.com/OHIF/Viewers/issues/1728)
* 🐛 disable text select while Ctrl+Shift to select studies ([00ed9cf](https://github.com/OHIF/Viewers/commit/00ed9cf4e3d80b32ec0505054a85e4efdf2c82c8))
* 🐛 Dismiss all dialogs if leaving viewer route [#1242](https://github.com/OHIF/Viewers/issues/1242) ([#1301](https://github.com/OHIF/Viewers/issues/1301)) ([5c3d8b3](https://github.com/OHIF/Viewers/commit/5c3d8b37b6f723fbd8edcc447c37984e7eee8d40))
* 🐛 display correct VindocResult ([96a2984](https://github.com/OHIF/Viewers/commit/96a298493da8c494353d3d68ed0721689fc40d65))
* 🐛 display diseases result ([845d2d5](https://github.com/OHIF/Viewers/commit/845d2d5c4698a0f5d042f7ccb08772af66c40528))
* 🐛 display max badge number by 99999 ([5268011](https://github.com/OHIF/Viewers/commit/5268011f187fcfaec5f255036cfe332fc6c8ab52))
* 🐛 display only BIRADS ([1b2ee7f](https://github.com/OHIF/Viewers/commit/1b2ee7fc3ac9da2550db1b3e82b3aa1954d5f7b3))
* 🐛 fix bug update panel after delete ([866ed20](https://github.com/OHIF/Viewers/commit/866ed20c0100f71107f0824bf81216d32664fa71))
* 🐛 fix checked status in vindoc select tree ([fc9f66d](https://github.com/OHIF/Viewers/commit/fc9f66d624363cebbbd0d19620ae5041e38b8230))
* 🐛 fix double drawing box ([f7adc5c](https://github.com/OHIF/Viewers/commit/f7adc5cb000a6e6184cf028eaf2e7ab2d3c6c1bc))
* 🐛 Fix drag-n-drop of local files into OHIF ([#1319](https://github.com/OHIF/Viewers/issues/1319)) ([23305ce](https://github.com/OHIF/Viewers/commit/23305cec9c0f514e73a8dd17f984ffc87ad8d131)), closes [#1307](https://github.com/OHIF/Viewers/issues/1307)
* 🐛 fix drawing box without patient ID ([4751715](https://github.com/OHIF/Viewers/commit/47517155a1ae2aa2844472134157cef0f4e71dbf))
* 🐛 fix empty blank measurement ([c4ddffc](https://github.com/OHIF/Viewers/commit/c4ddffc8b4bbd03b9302be6e0640cec628926a43))
* 🐛 fix error load box on mammo ([110e725](https://github.com/OHIF/Viewers/commit/110e725bad35415ae9e94f1fa26e80c2c661a498))
* 🐛 fix flag icon size ([1f27188](https://github.com/OHIF/Viewers/commit/1f27188c513d8a98a7ca121bebc5752776048d60))
* 🐛 Fix for JS breaking on header ([#1164](https://github.com/OHIF/Viewers/issues/1164)) ([0fbaf95](https://github.com/OHIF/Viewers/commit/0fbaf95971dc0b3a671e1f586a876d9019e860ed))
* 🐛 Fix ghost shadow on thumb ([#1113](https://github.com/OHIF/Viewers/issues/1113)) ([caaa032](https://github.com/OHIF/Viewers/commit/caaa032c4bc24fd69fdb01a15a8feb2721c321db))
* 🐛 Fix issue on not loading gcloud ([#919](https://github.com/OHIF/Viewers/issues/919)) ([f723546](https://github.com/OHIF/Viewers/commit/f723546e6b9af2322b83c14c048f33b786cf9625))
* 🐛 fix load chest data result ([2114b03](https://github.com/OHIF/Viewers/commit/2114b03b8eb089da2f7fea9cdb5b9656e79c22ac))
* 🐛 fix mammo showing position ([4666d9f](https://github.com/OHIF/Viewers/commit/4666d9f5935540d4ea5a47cec963fa4fb97a5615))
* 🐛 fix open popup edit conslusions ([448435f](https://github.com/OHIF/Viewers/commit/448435f4d595d01ca8a5fb4e90f6e97580ff5d2d))
* 🐛 Fix race condition when loading derived display sets ([#1718](https://github.com/OHIF/Viewers/issues/1718)) ([b1678ce](https://github.com/OHIF/Viewers/commit/b1678ce6399dde37a9878f45ccc7c63286d93fab)), closes [#1715](https://github.com/OHIF/Viewers/issues/1715)
* 🐛 Fix RT Panel hide/show and Fix looping load errors ([#1877](https://github.com/OHIF/Viewers/issues/1877)) ([e7cc735](https://github.com/OHIF/Viewers/commit/e7cc735c03d02eeb0d3af4ba02c15ed4f81bbec2))
* 🐛 fix search filter ([f70ee9d](https://github.com/OHIF/Viewers/commit/f70ee9dd3bf161ec224419943411e6811c316fac))
* 🐛 fix search text in studylist ([b76ffbc](https://github.com/OHIF/Viewers/commit/b76ffbc18103ef2d3325ba9fc4fdfe341e964075))
* 🐛 Fix seg color load ([#1724](https://github.com/OHIF/Viewers/issues/1724)) ([c4f84b1](https://github.com/OHIF/Viewers/commit/c4f84b1174d04ba84d37ed89b6d7ab541be28181))
* 🐛 fix store label name ([9e20030](https://github.com/OHIF/Viewers/commit/9e20030e136067aba84cc8aa411cfd6d9cac9784))
* 🐛 handle label name in chest ([09e50cb](https://github.com/OHIF/Viewers/commit/09e50cbd85b133f6c5e846159f2a61b5e93466dd))
* 🐛 hide panel with new modality ([cb91000](https://github.com/OHIF/Viewers/commit/cb9100083f4ce036fdea1ccad24a01406a6f6784))
* 🐛 hide PatientID and change selected row color ([a108cde](https://github.com/OHIF/Viewers/commit/a108cde52a419571e86305715f857cf091986d16))
* 🐛 hide pollyfill js ([3887502](https://github.com/OHIF/Viewers/commit/38875025161d4680ef005acdff8ad402a51409c1))
* 🐛 hide prediction panel name ([52d6b2e](https://github.com/OHIF/Viewers/commit/52d6b2e52c1e915e154372c14b5dee76a7bbd010))
* 🐛 hot fix delete measurement box ([8df8cde](https://github.com/OHIF/Viewers/commit/8df8cde1d7c7140d0b03f9d05db384aa65612b1f))
* 🐛 hot fix label showing characters ([a28a896](https://github.com/OHIF/Viewers/commit/a28a896bafed4c2e81aab957f9b7011d969ee274))
* 🐛 hotfix load user data when change tab ([697f59e](https://github.com/OHIF/Viewers/commit/697f59ef15bf18914cfd602c0cd2ec73efac6e96))
* 🐛 ignore tabs as filter change ([d82fb29](https://github.com/OHIF/Viewers/commit/d82fb29f6de622eab4ff881462428877c92f85dd))
* 🐛 Import correct lodash/isEmpty ([93e3b79](https://github.com/OHIF/Viewers/commit/93e3b7952dae642def17e47105ca0422e7648126))
* 🐛 Include StudyDescription as default ([e0969d6](https://github.com/OHIF/Viewers/commit/e0969d67a72d32b6e2bd29d0567e2d9136291d04))
* 🐛 increase diseases' Chip border width ([fc743c7](https://github.com/OHIF/Viewers/commit/fc743c7f25231e6170131037678d3659583764c7))
* 🐛 JSON launch not working properly ([#1089](https://github.com/OHIF/Viewers/issues/1089)) ([#1093](https://github.com/OHIF/Viewers/issues/1093)) ([2677170](https://github.com/OHIF/Viewers/commit/2677170d67659ee178cf77307414d54cfe9cb563))
* 🐛 Limit image download size to avoid browser issues ([#1112](https://github.com/OHIF/Viewers/issues/1112)) ([5716b71](https://github.com/OHIF/Viewers/commit/5716b71d409ee1c6f13393c8cb7f50222415e198)), closes [#1099](https://github.com/OHIF/Viewers/issues/1099)
* 🐛 Load default display set when no time metadata ([#1684](https://github.com/OHIF/Viewers/issues/1684)) ([f7b8b6a](https://github.com/OHIF/Viewers/commit/f7b8b6a41c4626084ef56b0fdf7363e914b143c4)), closes [#1683](https://github.com/OHIF/Viewers/issues/1683)
* 🐛 localize prediction panel title ([89700fc](https://github.com/OHIF/Viewers/commit/89700fc380e560cf8509324f8d96995057d518e3))
* 🐛 loop right side conclusion ([94fc376](https://github.com/OHIF/Viewers/commit/94fc37653aabea61e1646fbb9b1b40c9dd6411b7))
* 🐛 make select all box same size  with others ([dca1a24](https://github.com/OHIF/Viewers/commit/dca1a2455ad846d628b164bf2ff60e7f896c2cfc))
* 🐛 merge conflict ([fcbb5dc](https://github.com/OHIF/Viewers/commit/fcbb5dcf547c803ecee196f2334f7f9dac4bfeb9))
* 🐛 Metadata is being mistakenly purged ([#1360](https://github.com/OHIF/Viewers/issues/1360)) ([b9a66d4](https://github.com/OHIF/Viewers/commit/b9a66d44241f2896ef184511287fb4984671e16d)), closes [#1326](https://github.com/OHIF/Viewers/issues/1326)
* 🐛 Minor issues measurement panel related to description ([#1142](https://github.com/OHIF/Viewers/issues/1142)) ([681384b](https://github.com/OHIF/Viewers/commit/681384b7425c83b02a0ed83371ca92d78ca7838c))
* 🐛 new upload API and wait for the result ([6867102](https://github.com/OHIF/Viewers/commit/68671025db6aee5eae46a28a9d863e6363551a02))
* 🐛 only dispatch when criterias are correct ([1f1cb71](https://github.com/OHIF/Viewers/commit/1f1cb712aa724b779eb7b5beb5ca343d9fa5a5f0))
* 🐛 only search on submit ([3e63117](https://github.com/OHIF/Viewers/commit/3e631176128ec5badcf4f1237ac623f21015b1c7))
* 🐛 Orthographic MPR fix ([#1092](https://github.com/OHIF/Viewers/issues/1092)) ([460e375](https://github.com/OHIF/Viewers/commit/460e375f0aa75d35f7a46b4d48e6cc706019956d))
* 🐛 prevent error with dom-helper/addClass ([8fd2ef0](https://github.com/OHIF/Viewers/commit/8fd2ef010bb5a5c0583d5693f99ad6ee5348ff95))
* 🐛 Prevent next pagination while end of list ([b29182f](https://github.com/OHIF/Viewers/commit/b29182fcd2be281e6b3fd160abe8b8c37582f689))
* 🐛 Proper error handling for derived display sets ([#1708](https://github.com/OHIF/Viewers/issues/1708)) ([5b20d8f](https://github.com/OHIF/Viewers/commit/5b20d8f323e4b3ef9988f2f2ab672d697b6da409))
* 🐛 proptypes conclusion ([0f76ab0](https://github.com/OHIF/Viewers/commit/0f76ab079354293eee24e7e502b0199c1f526f33))
* 🐛 put a span to wrap Button inside a Tooltip ([6e6540c](https://github.com/OHIF/Viewers/commit/6e6540c54610f359d82212ead6d7a238abc90c40))
* 🐛 redirect to correct routerbasename ([818be94](https://github.com/OHIF/Viewers/commit/818be9482ebfebc5a6da9cb109ce93da6acd126b))
* 🐛 refactor ui lablling ([c29a4c3](https://github.com/OHIF/Viewers/commit/c29a4c3e72247648cdbb5e0e1d5f2506b8cabf0c))
* 🐛 refactor warning the same key ([d64b4d1](https://github.com/OHIF/Viewers/commit/d64b4d13b575fb725268b90b8c4fcbf30c6a782f))
* 🐛 reload page after change language ([e572d6b](https://github.com/OHIF/Viewers/commit/e572d6b5943293f3e470e8cb1694f3675f7036d9))
* 🐛 Remove debugger statement left in from last PR ([#1052](https://github.com/OHIF/Viewers/issues/1052)) ([d091cd6](https://github.com/OHIF/Viewers/commit/d091cd604c91da4531a6c5417ff3d2b6c66ce6fa))
* 🐛 remove platform/viewer/.env from git ([63e9c14](https://github.com/OHIF/Viewers/commit/63e9c14cfe5910c41fa27158b654390b63ed5857))
* 🐛 remove polling for study result ([397335f](https://github.com/OHIF/Viewers/commit/397335faf87665238ccac667571480aa375c8c4f))
* 🐛 rm lung lesion disease namne ([ba719a3](https://github.com/OHIF/Viewers/commit/ba719a3319447537ebcd76598a0e285dd7e77a86))
* 🐛 rm material ui tab panel ([6f47146](https://github.com/OHIF/Viewers/commit/6f47146bcbca756cd9ef3204f577fccc1e3d0f2f))
* 🐛 rm readonly proptypes ([4c9ec9b](https://github.com/OHIF/Viewers/commit/4c9ec9b88355ed2e7ff880cf93dba1d784b8f649))
* 🐛 rm some proptypes ([f308647](https://github.com/OHIF/Viewers/commit/f308647c893a127eda3b8bd29ae5981549c49a43))
* 🐛 separate triggering fetchStudies ([549bd5f](https://github.com/OHIF/Viewers/commit/549bd5f14c9f88a5a5b715847983024eaf1fef54))
* 🐛 set current viewport as active when switching layouts ([#1018](https://github.com/OHIF/Viewers/issues/1018)) ([2a74355](https://github.com/OHIF/Viewers/commit/2a743554b65960be6f36bd2e60d0624e11375e42))
* 🐛 set default window level ([42f9985](https://github.com/OHIF/Viewers/commit/42f998501e6b9e798bea0e470a677f834c470203))
* 🐛 set initialSort as empty for OrderDashboard ([ac4b142](https://github.com/OHIF/Viewers/commit/ac4b14222f38e86fa03e4d5205429834d22ca8f8))
* 🐛 Set series into active viewport by clicking on thumbnail ([#945](https://github.com/OHIF/Viewers/issues/945)) ([5551f81](https://github.com/OHIF/Viewers/commit/5551f817e346a8db0795039fe3574ab0d44a3a23)), closes [#895](https://github.com/OHIF/Viewers/issues/895) [#895](https://github.com/OHIF/Viewers/issues/895)
* 🐛 show AI result when change tab ([a858e50](https://github.com/OHIF/Viewers/commit/a858e5030dfe04955c49bfafb7f220f808440623))
* 🐛 show analysis data for MG modality ([eca274a](https://github.com/OHIF/Viewers/commit/eca274acb25ddc240390759bcd5f307396c34849))
* 🐛 show default current option tab ([73ee84d](https://github.com/OHIF/Viewers/commit/73ee84d1d289ba21f774c75c4a23524d098fc7a8))
* 🐛 stop put InputLabelProps into InputBase ([9c8eef6](https://github.com/OHIF/Viewers/commit/9c8eef69278c87c3f1fcfe1f666a9b121a34b635))
* 🐛 stop sending Search if empty ([248c91a](https://github.com/OHIF/Viewers/commit/248c91a317591e10449d1ca83f24a2aa37b1d8d2))
* 🐛 StudyList minor UI bugs ([d5d15f3](https://github.com/OHIF/Viewers/commit/d5d15f3471364d26505d08747373cb4026f24b36))
* 🐛 stylling alt-thumbnail ([3b5e632](https://github.com/OHIF/Viewers/commit/3b5e6325006516e58ccce7a8b88f115924971c5c))
* 🐛 Switch to orhtographic view for 2D MPR ([#1074](https://github.com/OHIF/Viewers/issues/1074)) ([13d337a](https://github.com/OHIF/Viewers/commit/13d337aaabb8dadf6366c6262c5e47e7781edd08))
* 🐛 tabs and button use lowercase text ([856bb48](https://github.com/OHIF/Viewers/commit/856bb480dc754ce441d58d02870a5f8487edc215))
* 🐛 temporarily turn off service-worker ([356383f](https://github.com/OHIF/Viewers/commit/356383f2d1b42a1fc486fe6b98f8904b157998d5))
* 🐛 turn off autocomplete to prevent incorrect style ([ee1d13e](https://github.com/OHIF/Viewers/commit/ee1d13e1c36a35adc33c288abe5853e1a0fe947c))
* 🐛 Update for changes in ExpandableToolMenu props ([e09670a](https://github.com/OHIF/Viewers/commit/e09670ada291a62a72c6a73becff8ad69d12744c))
* 🐛 update StudyTable column width ([649edc5](https://github.com/OHIF/Viewers/commit/649edc543f4f7d6acb483b201d62d6ae0251aaf6))
* 🐛 use horizonal filter bar ([d709f89](https://github.com/OHIF/Viewers/commit/d709f89c6433ac53c8a67dfe57956d48b92a56a7))
* 🐛 use PatientName as default search ([834e280](https://github.com/OHIF/Viewers/commit/834e28058fc5caa422e6d80c1f9bd3a02ef5992e))
* 🐛 using wrong env ([e831a85](https://github.com/OHIF/Viewers/commit/e831a85224943f346a21904f425bd5a61592d56d))
* add DX modality ([51946ac](https://github.com/OHIF/Viewers/commit/51946acc0534d37af07570a7f2f0a70b59fb81d1))
* Add IHEInvokeImageDisplay routes back into viewer ([#1695](https://github.com/OHIF/Viewers/issues/1695)) ([f7162ce](https://github.com/OHIF/Viewers/commit/f7162ce61708776a6c192732b0904a022bcc6b3a))
* Add some code splitting for PWA build ([#937](https://github.com/OHIF/Viewers/issues/937)) ([8938035](https://github.com/OHIF/Viewers/commit/89380353d270cbb89d0c60290a1210f7ac8baa7a))
* application crash if patientName is an object ([#1138](https://github.com/OHIF/Viewers/issues/1138)) ([64cf3b3](https://github.com/OHIF/Viewers/commit/64cf3b324da2383a927af1df2d46db2fca5318aa))
* asset resolution when at non-root route ([#828](https://github.com/OHIF/Viewers/issues/828)) ([d48b617](https://github.com/OHIF/Viewers/commit/d48b617e1ce2dc4285665eb23ec347f3515d6785))
* avoid-wasteful-renders ([#1544](https://github.com/OHIF/Viewers/issues/1544)) ([e41d339](https://github.com/OHIF/Viewers/commit/e41d339f5faef6b93700bc860f37f29f32ad5ed6))
* bump cornerstone-tools to latest version ([f519f86](https://github.com/OHIF/Viewers/commit/f519f8609cdb3372ab6626f3eeeacb66876bfb8e))
* Combined Hotkeys for special characters ([#1233](https://github.com/OHIF/Viewers/issues/1233)) ([2f30e7a](https://github.com/OHIF/Viewers/commit/2f30e7a821a238144c49c56f37d8e5565540b4bd))
* Creating 2 commands to activate zoom tool and also to move between displaySets ([#1446](https://github.com/OHIF/Viewers/issues/1446)) ([06a4af0](https://github.com/OHIF/Viewers/commit/06a4af06faaecf6fa06ccd90cdfa879ee8d53053))
* display diseases from search API ([a6947ca](https://github.com/OHIF/Viewers/commit/a6947ca7797d3aa38e598b27cac115dbfca9cbe1))
* download tool fixes & improvements ([#1235](https://github.com/OHIF/Viewers/issues/1235)) ([b9574b6](https://github.com/OHIF/Viewers/commit/b9574b6efcfeb85cde35b5cae63282f8e1b35be6))
* Exit MPR mode if Layout is changed ([#984](https://github.com/OHIF/Viewers/issues/984)) ([674ca9f](https://github.com/OHIF/Viewers/commit/674ca9f96f103925219204a037243bdc01c00c2e))
* Fix incorrect command name in Percy test ([#1999](https://github.com/OHIF/Viewers/issues/1999)) ([ebdcde1](https://github.com/OHIF/Viewers/commit/ebdcde1c4d9c95393cf79cc9994f5d60f6d66fdd))
* GCloud dataset picker dialog broken ([#1453](https://github.com/OHIF/Viewers/issues/1453)) ([64dfbea](https://github.com/OHIF/Viewers/commit/64dfbeab7af98277efefadd334df14db79e32a4f))
* get adapter store picker to show ([#1134](https://github.com/OHIF/Viewers/issues/1134)) ([50ca2bd](https://github.com/OHIF/Viewers/commit/50ca2bde971e1e67b73ece96369052dd1a35ac68))
* google cloud support w/ docker (via env var) ([#958](https://github.com/OHIF/Viewers/issues/958)) ([e375a4a](https://github.com/OHIF/Viewers/commit/e375a4ab1129f80e823a004d40c155864928995e))
* hide AI status from StudyList ([fde2760](https://github.com/OHIF/Viewers/commit/fde2760a1e0699863ee81c554a3c4662f143867b))
* import regenerator-runtime for umd build ([bad987a](https://github.com/OHIF/Viewers/commit/bad987a06baf45b0ebfef03a43fea078f6396a22))
* Issue branch from danny experimental changes pr 1128 ([#1150](https://github.com/OHIF/Viewers/issues/1150)) ([a870b3c](https://github.com/OHIF/Viewers/commit/a870b3cc6056cf824af422e46f1ad674910b534e)), closes [#1161](https://github.com/OHIF/Viewers/issues/1161) [#1164](https://github.com/OHIF/Viewers/issues/1164) [#1177](https://github.com/OHIF/Viewers/issues/1177) [#1179](https://github.com/OHIF/Viewers/issues/1179) [#1180](https://github.com/OHIF/Viewers/issues/1180) [#1181](https://github.com/OHIF/Viewers/issues/1181) [#1182](https://github.com/OHIF/Viewers/issues/1182) [#1183](https://github.com/OHIF/Viewers/issues/1183) [#1184](https://github.com/OHIF/Viewers/issues/1184) [#1185](https://github.com/OHIF/Viewers/issues/1185)
* Load measurement in active viewport. ([#1558](https://github.com/OHIF/Viewers/issues/1558)) ([99022f2](https://github.com/OHIF/Viewers/commit/99022f2bac752f3cd1cedb61e222b8d411e158c8))
* measurementsAPI issue caused by production build ([#842](https://github.com/OHIF/Viewers/issues/842)) ([49d3439](https://github.com/OHIF/Viewers/commit/49d343941e236e442114e59861a373c7edb0b1fb))
* minor date picker UX improvements ([813ee5e](https://github.com/OHIF/Viewers/commit/813ee5ed4d78b7bda234922d5f3389efe346451c))
* MPR initialization ([#1062](https://github.com/OHIF/Viewers/issues/1062)) ([b037394](https://github.com/OHIF/Viewers/commit/b03739428f72bb50bdabdd6f83b7af885057da69))
* of undefined name of project ([#1231](https://github.com/OHIF/Viewers/issues/1231)) ([e34a057](https://github.com/OHIF/Viewers/commit/e34a05726319e3e70279c43d5bf976d33cdf71f7))
* OIDC Redirect erases query parameters ([#1773](https://github.com/OHIF/Viewers/issues/1773)) ([6123741](https://github.com/OHIF/Viewers/commit/6123741765e81d0bea8fbd5dbb0f310aaca0fb33))
* on-brand library global name ([ababe63](https://github.com/OHIF/Viewers/commit/ababe63a55cb6e5993f5d23bf51b3a164634636a))
* Only permit web workers to be initialized once. ([#1535](https://github.com/OHIF/Viewers/issues/1535)) ([9feadd3](https://github.com/OHIF/Viewers/commit/9feadd3c6d71c1c48f7825d024ccf95d5d82606d))
* prevent the native context menu from appearing when right-clicking on a measurement or angle (https://github.com/OHIF/Viewers/issues/1406) ([#1469](https://github.com/OHIF/Viewers/issues/1469)) ([9b3be9b](https://github.com/OHIF/Viewers/commit/9b3be9b0c082c9a5b62f2a40f42e59381860fe73))
* remove requestOptions when key is not needed ([32bc47d](https://github.com/OHIF/Viewers/commit/32bc47d31593e55de4db5e92047c38f3223ca9a2))
* resolves [#1483](https://github.com/OHIF/Viewers/issues/1483) ([#1527](https://github.com/OHIF/Viewers/issues/1527)) ([2747eff](https://github.com/OHIF/Viewers/commit/2747effd9e893bd78b80ee7d0444f44676e9d632))
* Revert "refactor: Reduce bundle size ([#1575](https://github.com/OHIF/Viewers/issues/1575))" ([#1622](https://github.com/OHIF/Viewers/issues/1622)) ([d21af3f](https://github.com/OHIF/Viewers/commit/d21af3f133492fa31492413b8782936c9ff18b44))
* Revert "Revert "fix: MPR initialization"" ([#1065](https://github.com/OHIF/Viewers/issues/1065)) ([c680720](https://github.com/OHIF/Viewers/commit/c680720ce5ead58fdb399e3a356edac18093f5c0)), closes [#1062](https://github.com/OHIF/Viewers/issues/1062) [#1064](https://github.com/OHIF/Viewers/issues/1064)
* rollbar template needs PUBLIC_URL defined ([#1127](https://github.com/OHIF/Viewers/issues/1127)) ([352407c](https://github.com/OHIF/Viewers/commit/352407c71ae93946e9ebad41446d6086cfbc237b))
* segmentation not loading ([#1566](https://github.com/OHIF/Viewers/issues/1566)) ([4a7ce1c](https://github.com/OHIF/Viewers/commit/4a7ce1c09324d74c61048393e3a2427757e4001a))
* set SR in ActiveViewport by clicking thumb ([#1091](https://github.com/OHIF/Viewers/issues/1091)) ([986b7ae](https://github.com/OHIF/Viewers/commit/986b7ae2bf4f7d27f326e62f93285ce20eaf0a79))
* Set SR viewport as active by interaction ([#1118](https://github.com/OHIF/Viewers/issues/1118)) ([5b33417](https://github.com/OHIF/Viewers/commit/5b334175c370afb930b4b6dbd307ddece8f850e3))
* Switch DICOMFileUploader to use the UIModalService ([#1904](https://github.com/OHIF/Viewers/issues/1904)) ([7772fee](https://github.com/OHIF/Viewers/commit/7772fee21ae6a65994e1251e2f1d2554b47781be))
* Switch token storage back to localStorage because in-memory was annoying for end users ([#1030](https://github.com/OHIF/Viewers/issues/1030)) ([412fe4e](https://github.com/OHIF/Viewers/commit/412fe4e23fa05bf448f7fe6fcb39cd69eaaf04ad))
* undefined `errorHandler` in cornerstoneWadoImageLoader configuration ([#1664](https://github.com/OHIF/Viewers/issues/1664)) ([709f147](https://github.com/OHIF/Viewers/commit/709f14708e2b0f912b5ea509114acd87af3149cb))
* update script-tag output to include config from default.js ([c522ff3](https://github.com/OHIF/Viewers/commit/c522ff3ddab7ed8e3a128dd6edd2cd6902226e99))
* User Preferences Issues ([#1207](https://github.com/OHIF/Viewers/issues/1207)) ([1df21a9](https://github.com/OHIF/Viewers/commit/1df21a9e075b5e6dfc10a429ae825826f46c71b8)), closes [#1161](https://github.com/OHIF/Viewers/issues/1161) [#1164](https://github.com/OHIF/Viewers/issues/1164) [#1177](https://github.com/OHIF/Viewers/issues/1177) [#1179](https://github.com/OHIF/Viewers/issues/1179) [#1180](https://github.com/OHIF/Viewers/issues/1180) [#1181](https://github.com/OHIF/Viewers/issues/1181) [#1182](https://github.com/OHIF/Viewers/issues/1182) [#1183](https://github.com/OHIF/Viewers/issues/1183) [#1184](https://github.com/OHIF/Viewers/issues/1184) [#1185](https://github.com/OHIF/Viewers/issues/1185)
* version bump issue ([#962](https://github.com/OHIF/Viewers/issues/962)) ([c80ea17](https://github.com/OHIF/Viewers/commit/c80ea17cc2dd931de5f4585a7e6afc4095e03f2e))
* version bump issue ([#963](https://github.com/OHIF/Viewers/issues/963)) ([e607ed2](https://github.com/OHIF/Viewers/commit/e607ed2aa07fb9a08d0dfcfb44b8d10bdfb67a66)), closes [#962](https://github.com/OHIF/Viewers/issues/962)
* viewer project should build output before publish ([94b625d](https://github.com/OHIF/Viewers/commit/94b625d4e665594ea2988cef32e796c83c206bb7))
* whiteLabeling should support component creation by passing React to defined fn ([#1659](https://github.com/OHIF/Viewers/issues/1659)) ([2093a00](https://github.com/OHIF/Viewers/commit/2093a0036584b2cc698c8f06fe62b334523b1029))
* **StandaloneRouting:** Promise rejection - added `return` ([#791](https://github.com/OHIF/Viewers/issues/791)) ([d09fb4e](https://github.com/OHIF/Viewers/commit/d09fb4e71c49e799e2fa49c2a1aa38679ed21c14))


### Features

* [#1342](https://github.com/OHIF/Viewers/issues/1342) - Window level tab ([#1429](https://github.com/OHIF/Viewers/issues/1429)) ([ebc01a8](https://github.com/OHIF/Viewers/commit/ebc01a8ca238d5a3437b44d81f75aa8a5e8d0574))
* 🎸 1729 - error boundary wrapper ([#1764](https://github.com/OHIF/Viewers/issues/1764)) ([c02b232](https://github.com/OHIF/Viewers/commit/c02b232b0cc24f38af5d5e3831d987d048e60ada))
* 🎸 able to do batch and view from context menu ([71b9eb5](https://github.com/OHIF/Viewers/commit/71b9eb582253e62bacfaf1cb82873b5b58a0bca3))
* 🎸 able to open multi tabs for viewer ([28158f7](https://github.com/OHIF/Viewers/commit/28158f74d6f0edfcc96379503674d52843ff62bd))
* 🎸 able to redirect to signout ([9c74834](https://github.com/OHIF/Viewers/commit/9c74834f087335481c56f51eecd5f316d0245538))
* 🎸 active detault Magnify tool ([b224a1f](https://github.com/OHIF/Viewers/commit/b224a1fe65e1c78abbe6ca4d69da8c82b52032ba))
* 🎸 active header link ([05bbe51](https://github.com/OHIF/Viewers/commit/05bbe5176e5582d2f6a632d9585b8fbe34afbc6a))
* 🎸 add .env file ([d0ff7c2](https://github.com/OHIF/Viewers/commit/d0ff7c2a5191192f265700a3ca8873ff404669c1))
* 🎸 add "Add Order List" button ([d812872](https://github.com/OHIF/Viewers/commit/d81287221c62467b7bf67e10d470c43ae741035c))
* 🎸 Add [@material-ui](https://github.com/material-ui) and StudyList boilerplate ([d4c79a6](https://github.com/OHIF/Viewers/commit/d4c79a6da93c0185978d06e57407af9b1cbe35d6))
* 🎸 add /series for CT ([2373340](https://github.com/OHIF/Viewers/commit/2373340f1157a613071e834485296c39ea809429))
* 🎸 add 302 redirect ([c9521e0](https://github.com/OHIF/Viewers/commit/c9521e0ca2e6f9536feac251504ecbe17ea272e0))
* 🎸 add AccessionNumber and BodyPartExamined to search ([4935ba6](https://github.com/OHIF/Viewers/commit/4935ba6d67f8fbc20677f5903cb341265b693c31))
* 🎸 add action comment ([b7e2225](https://github.com/OHIF/Viewers/commit/b7e2225135f431ca63191c02d43f642fa0db4a05))
* 🎸 add action Rating ([8811ca5](https://github.com/OHIF/Viewers/commit/8811ca5cef7e08897916b742e4d5b7939abdff1c))
* 🎸 add advanced Batch button ([a6eab6a](https://github.com/OHIF/Viewers/commit/a6eab6a5f9353e6172bb9c5de500b91855e7561e))
* 🎸 add ai icon ([17b747c](https://github.com/OHIF/Viewers/commit/17b747c834d0d37c4e9989caf8b6b1d16098c608))
* 🎸 add ai result for chest ([972a5a2](https://github.com/OHIF/Viewers/commit/972a5a2bfa352d2424baef0c08bf6063542a9a00))
* 🎸 add annotation on viewer ([2b8d70b](https://github.com/OHIF/Viewers/commit/2b8d70b1edfa609c936363ab1c7073bd31dea293))
* 🎸 add api when get setting ([d8543a4](https://github.com/OHIF/Viewers/commit/d8543a47021d2c50fd8fbaf1ccce2c6eca87b819))
* 🎸 add approve feature for mammo ([848f858](https://github.com/OHIF/Viewers/commit/848f85852843d597bf4465092a1b6271256c6c72))
* 🎸 add assign popup and useAssignment hooks ([d0eded2](https://github.com/OHIF/Viewers/commit/d0eded2b1427d8a577af7c03c8aca1812dd4dbe9))
* 🎸 add body assign studies to workgroup ([b6afc5a](https://github.com/OHIF/Viewers/commit/b6afc5a97f4bdd05882f085e99f5d42306b0b9b8))
* 🎸 Add bootstrap and react-bootstrap UI kit ([92007bd](https://github.com/OHIF/Viewers/commit/92007bd42a51b0712940a383d1a3b5c454743dc3))
* 🎸 add check lastupdate ai time ([8422358](https://github.com/OHIF/Viewers/commit/8422358c034ab4ce8cd5167d62c418b40a809c7e))
* 🎸 add checkbox to share screen ([3271070](https://github.com/OHIF/Viewers/commit/3271070149a83b2efd8e23c1e6e1e42595a72ab7))
* 🎸 add chest disease name ([f12db7e](https://github.com/OHIF/Viewers/commit/f12db7e04e6c7df71fd8537aed052f44d6e3dfae))
* 🎸 add close button ([93724fc](https://github.com/OHIF/Viewers/commit/93724fccd973ab41a2df746e7d885ce32508d706))
* 🎸 add collapse button ([c7037a6](https://github.com/OHIF/Viewers/commit/c7037a697a5572e46f241ff29ed913082b661594))
* 🎸 add collapse button for left side ([4ea37e1](https://github.com/OHIF/Viewers/commit/4ea37e1c46a52be9dd12c64d422185d800bfacb2))
* 🎸 add Comment ([36c6b70](https://github.com/OHIF/Viewers/commit/36c6b7018fdbc8a4e9c5a9fa30037e2d718f97e3))
* 🎸 add common2 ([18f1186](https://github.com/OHIF/Viewers/commit/18f118615004283fc269fe6285d8cfbee8c37bb9))
* 🎸 add CommonSearch component ([abeb99e](https://github.com/OHIF/Viewers/commit/abeb99e0bbd06805eb633e7dc4c5e9f169ccff7f))
* 🎸 add condition show batch diagnosis ([88a5f20](https://github.com/OHIF/Viewers/commit/88a5f2081fcd5c5ad8f60fa0c836d6ea95cb4de4))
* 🎸 add condition show mammo prefix ([72e4645](https://github.com/OHIF/Viewers/commit/72e464518d465c8f1c9b617625a785a4de39b20c))
* 🎸 add console mammo panel ([785f9b6](https://github.com/OHIF/Viewers/commit/785f9b682cf9b325744740f3affb5cd197682ea7))
* 🎸 add cursor pointer close btn ([94adccf](https://github.com/OHIF/Viewers/commit/94adccf83134f04fdc8c3be22f4d9ec9d61380c0))
* 🎸 Add Date Presets to DateRangeFilter ([1ad58c9](https://github.com/OHIF/Viewers/commit/1ad58c9790e1fdfac2beb4ded65036abe79891de))
* 🎸 add default language to 'vi' ([63ec80a](https://github.com/OHIF/Viewers/commit/63ec80a228dda420df835d83cd97e87d5ea41859))
* 🎸 add dialog popup for labelling ([e93c3f5](https://github.com/OHIF/Viewers/commit/e93c3f54f7e16e7ad7cf9eee611a25f287d34324))
* 🎸 add edit button in conclusion ([735c7fd](https://github.com/OHIF/Viewers/commit/735c7fd59503859dc7d43874a3883fac7ecc21fb))
* 🎸 add env ([9e11abd](https://github.com/OHIF/Viewers/commit/9e11abdd9289f8aa34afa3c708d1fc624c3900b6))
* 🎸 add event delete for doctor ai tab ([8fdbe22](https://github.com/OHIF/Viewers/commit/8fdbe22a741f090fb1c1c9386b10aab29e0fd9c8))
* 🎸 add FEEDBACK_ACTIONS ([1432380](https://github.com/OHIF/Viewers/commit/14323802c6ef91318d8d007a0b71b3f38ca9db18))
* 🎸 Add filter for Description and PatientID ([93d2d68](https://github.com/OHIF/Viewers/commit/93d2d68b0e8354de20e98f3d941ea2306c819dff))
* 🎸 add fullscreen loading ([f52369f](https://github.com/OHIF/Viewers/commit/f52369fdea133735149b097faca31dc7d38a3aa3))
* 🎸 add function approve data ([6ce9a36](https://github.com/OHIF/Viewers/commit/6ce9a361f4d56430d6b8b2f59f505a363854bfea))
* 🎸 add function assign studies ([db87a00](https://github.com/OHIF/Viewers/commit/db87a00adf1752fd52edf47894eb48a9c6f0f65d))
* 🎸 add function back to 4 viewport ([ff54159](https://github.com/OHIF/Viewers/commit/ff54159cd1dfa383d3f205435cefc6f763e90629))
* 🎸 add function check modality ([1a67067](https://github.com/OHIF/Viewers/commit/1a6706724b073514d646c20dcc0b6d56fd80c8a1))
* 🎸 add function check tab only ([2c4700e](https://github.com/OHIF/Viewers/commit/2c4700e5b4e93b23186b421f148253a19c63fde4))
* 🎸 add function delete all box ([8ea8beb](https://github.com/OHIF/Viewers/commit/8ea8beb1f7d6b66e239941a588dbc2af5095426e))
* 🎸 add function draw segment ([1b81dc8](https://github.com/OHIF/Viewers/commit/1b81dc8356caf4cf1fa81f55dbeaa1506b934deb))
* 🎸 add function draw segmentation ([0d709aa](https://github.com/OHIF/Viewers/commit/0d709aa36fe05c810288f5477ea4aee842bee328))
* 🎸 add function draw segmentation ([9e91940](https://github.com/OHIF/Viewers/commit/9e919401746449b9bd934de7213225eefe9d6f17))
* 🎸 add function get userinfor ([284c725](https://github.com/OHIF/Viewers/commit/284c725212f7239a4bea08ed19bc73a79309f52e))
* 🎸 add function merge chest conclusion ([c3700a8](https://github.com/OHIF/Viewers/commit/c3700a83c9c6873c616cf4f99ae6782ed5bb6c62))
* 🎸 add function refresh diagnose darta ([2e2e8d9](https://github.com/OHIF/Viewers/commit/2e2e8d9c0b27226393cc10d6781dc4c2e2016b5f))
* 🎸 add function remove all box ([41d3c40](https://github.com/OHIF/Viewers/commit/41d3c4024fd17e3c6d433d91c6c66cffc893b823))
* 🎸 add function store measurement info ([92619a8](https://github.com/OHIF/Viewers/commit/92619a8cc234d34f8c3f48a42a3c1834782b9c48))
* 🎸 add function visible all tag ([15251c0](https://github.com/OHIF/Viewers/commit/15251c07450dd29638d2d98a164e7eb6d1eaa429))
* 🎸 add function visible box when hover ([6086b71](https://github.com/OHIF/Viewers/commit/6086b71010759996f129f9e92e4607655220a8db))
* 🎸 add group list selection mockup and hide batch actins ([66fb694](https://github.com/OHIF/Viewers/commit/66fb6944e7d93ef29a346c4daae93af1f11bdf0f))
* 🎸 add Home routing ([30f9400](https://github.com/OHIF/Viewers/commit/30f94007e0e42d686c5c7372c2739feda08ed4fa))
* 🎸 add hot key zero to reset viewport ([4e8a712](https://github.com/OHIF/Viewers/commit/4e8a71281bed8c79ae422c06cdf3a08de682fd2d))
* 🎸 add hot keys ([1ca6248](https://github.com/OHIF/Viewers/commit/1ca62481016cbdfa566efcd268f9bd5581e6f8d6))
* 🎸 add hotkey on toolbar button ([e7268e7](https://github.com/OHIF/Viewers/commit/e7268e73ce954c68051d79349ce80355e265fb13))
* 🎸 add i18n for prediction panel & styling progressbar ([0d93c1a](https://github.com/OHIF/Viewers/commit/0d93c1a9d47d2e1ac296fc5e7081da2b9808d3f9))
* 🎸 add interceptor request ([7edec06](https://github.com/OHIF/Viewers/commit/7edec06549b8aaf45868fb860f1627c2fd8411a7))
* 🎸 add labelling data list ([1ca4409](https://github.com/OHIF/Viewers/commit/1ca4409a7171d504877b755deafaa143e4728c9d))
* 🎸 add labelling data template ([3ecfc27](https://github.com/OHIF/Viewers/commit/3ecfc27a01dbd0ad6facb325a733213f22321c02))
* 🎸 add like and dislike button ([5d6f6f3](https://github.com/OHIF/Viewers/commit/5d6f6f3dd94f19b421df89604e2d19ea813d150d))
* 🎸 add link to s3 ([f2ffc56](https://github.com/OHIF/Viewers/commit/f2ffc56708925e42c518c1e604473a8659ceddfa))
* 🎸 add load more button ([21adfe6](https://github.com/OHIF/Viewers/commit/21adfe63efdb16dfbaf1a82c8cb1d580167399c7))
* 🎸 add loading full screen ([d0e088b](https://github.com/OHIF/Viewers/commit/d0e088bf44de49bc2e5ae444a6d4285baec6ffb8))
* 🎸 add localization for checkbox list ([b729dfe](https://github.com/OHIF/Viewers/commit/b729dfecb8ab38c78ddbd4a243346007f0e8311f))
* 🎸 add logic show workgroup ([d35ea5c](https://github.com/OHIF/Viewers/commit/d35ea5cd99bce410e5ad48c1fecd9c6ba35cc5d9))
* 🎸 add mammo conclusion "Not available" option ([87c8941](https://github.com/OHIF/Viewers/commit/87c89419b0d337928dddc061a21d7bf3a1b2d2ee))
* 🎸 add mammo global ([7d29a18](https://github.com/OHIF/Viewers/commit/7d29a18ed1a06bfad4dbd7e27ea53aa9ff37430b))
* 🎸 add mammo label data ([727a4e7](https://github.com/OHIF/Viewers/commit/727a4e78c57da1a18d9bbb4a1b350344c1cfa357))
* 🎸 add mammo labelling data ([78aeeee](https://github.com/OHIF/Viewers/commit/78aeeee651335545b8fc3ce16e79ade8fe1bfecb))
* 🎸 add mammo processing class ([c7a586f](https://github.com/OHIF/Viewers/commit/c7a586fb6554b11445bf7b4fae9ad32491a8b8e4))
* 🎸 add memu lis ([6c6b559](https://github.com/OHIF/Viewers/commit/6c6b559fd393c53e7b5106f64db1c5950e93086d))
* 🎸 add message notify like sussess ([afd8823](https://github.com/OHIF/Viewers/commit/afd88233538ee421bbe1c3b340de4b9dee850512))
* 🎸 Add Modality filter ([a1a72f3](https://github.com/OHIF/Viewers/commit/a1a72f3620958e46803c70a9db731782a52a05e5))
* 🎸 add n/a localization ([958a7c5](https://github.com/OHIF/Viewers/commit/958a7c5a525d384389537ae8f068db3beff6a60e))
* 🎸 add new authorization flow ([a59f0ee](https://github.com/OHIF/Viewers/commit/a59f0ee732e30454daea42a6ba5f481fb79005cf))
* 🎸 add new label ([c414df2](https://github.com/OHIF/Viewers/commit/c414df24ccf1b67717813488dd7ac5db91f2291c))
* 🎸 add new logo ([2e23a7e](https://github.com/OHIF/Viewers/commit/2e23a7e38a578b40affd4b45fb1450e95239c6c4))
* 🎸 add NFI localization ([f2e0eaf](https://github.com/OHIF/Viewers/commit/f2e0eafde4c5431241124c70d53032a4d75bcafd))
* 🎸 Add normal DicomUploader ([8cacc96](https://github.com/OHIF/Viewers/commit/8cacc9694ca5762b0644d502e40ee83a0b4031dc))
* 🎸 add not supported screen ([bc93764](https://github.com/OHIF/Viewers/commit/bc937647d151f1cf620b3144d853eb44c4a2dc41))
* 🎸 add note ([7375835](https://github.com/OHIF/Viewers/commit/7375835f01d1562dbaf26fa3c378a8e4690eb4f5))
* 🎸 add option to query for all modaltities ([aa96087](https://github.com/OHIF/Viewers/commit/aa96087e51a9efd1d1684deb3ec5bbfe5a5185ec))
* 🎸 add order group name ([0d707f0](https://github.com/OHIF/Viewers/commit/0d707f0b5da036deea20bc8ebee397c70a07fb27))
* 🎸 add order group to table header ([20234fc](https://github.com/OHIF/Viewers/commit/20234fc852715d54c313c98d829f7bb6266ecbc6))
* 🎸 add order list button on header ([bdbea93](https://github.com/OHIF/Viewers/commit/bdbea93b4f1bf5d41fcb65a199430c75bb078e99))
* 🎸 add order list page ([0f24299](https://github.com/OHIF/Viewers/commit/0f242997b0b93e148e4efb1641c8509395158187))
* 🎸 add order list page ([4c4805d](https://github.com/OHIF/Viewers/commit/4c4805d7942afd1a97a35fd978053089979d7566))
* 🎸 add order list table ([10a0c9f](https://github.com/OHIF/Viewers/commit/10a0c9fb2ca1303c8579e4e91aada72a78dff345))
* 🎸 add page title ([8aa02f2](https://github.com/OHIF/Viewers/commit/8aa02f2b5c295c2277f986b7eb7fcc6c2839b0c3))
* 🎸 Add PatientName filter ([f82dd37](https://github.com/OHIF/Viewers/commit/f82dd37fcb6f5e62080531263efb91940de5e4e3))
* 🎸 add percent to findings ([6516fdd](https://github.com/OHIF/Viewers/commit/6516fdd7111239287abb20dc058a5c83d6f39c69))
* 🎸 add permissions list ([0038275](https://github.com/OHIF/Viewers/commit/00382754c3a21ae69d2ce443ea1377fb738e56b8))
* 🎸 add popup edit order item ([68aca30](https://github.com/OHIF/Viewers/commit/68aca305c92a73faaf04872864fdd41d1938a102))
* 🎸 add prefix api workspace ([1553c2e](https://github.com/OHIF/Viewers/commit/1553c2ea0c84fbe1535ae398e43b03e0df07dba1))
* 🎸 add prefix backend in api url ([dd57ab2](https://github.com/OHIF/Viewers/commit/dd57ab22e3800e63e9890a02430f05666de7dc57))
* 🎸 add print button ([fbdeea0](https://github.com/OHIF/Viewers/commit/fbdeea0dd21f93911f1db9291bfbf46a4eb8d04c))
* 🎸 add query params for orderlist ([ee07469](https://github.com/OHIF/Viewers/commit/ee07469b6d0403747c9e13c18ce8fed2b35b86b7))
* 🎸 add queue for upload file service ([b8ed81c](https://github.com/OHIF/Viewers/commit/b8ed81c314cfb35b38340f690f96cdbc02feedf9))
* 🎸 add react-tabs package json ([afe781c](https://github.com/OHIF/Viewers/commit/afe781c06f9ab18564a126a35dc46805fc23eae8))
* 🎸 add react-to-print package ([0595498](https://github.com/OHIF/Viewers/commit/0595498338707a39c4fdbf5ff5eb22435068556d))
* 🎸 add redirect login ([f6049fd](https://github.com/OHIF/Viewers/commit/f6049fdfb9fb57c473924a4298fde28003c48b07))
* 🎸 add refresh function ([061286b](https://github.com/OHIF/Viewers/commit/061286bd018c75da9e084c64402c0065e86add35))
* 🎸 add reject action in chest ([1e926a7](https://github.com/OHIF/Viewers/commit/1e926a771db89c43382c898c3c947927f808689b))
* 🎸 add reject button ([4fdc60e](https://github.com/OHIF/Viewers/commit/4fdc60e853816177bcfa6505a7b781faa5c10207))
* 🎸 add reject confirm popup ([51fa153](https://github.com/OHIF/Viewers/commit/51fa1536ea71b146c8920faef93be6d416016103))
* 🎸 add relative path roboto font ([3d2ebcd](https://github.com/OHIF/Viewers/commit/3d2ebcd8767e47ace08be6834fa863dac25c3974))
* 🎸 add relm id ([2579f10](https://github.com/OHIF/Viewers/commit/2579f108ad2a73decb80633c25e9933cdae52300))
* 🎸 add roboto font ([2089593](https://github.com/OHIF/Viewers/commit/20895930ee597caa34cd3ef5d9703bb301165aec))
* 🎸 add role ([50ce02a](https://github.com/OHIF/Viewers/commit/50ce02a469d0b15b86557e6eef9c21b6b4ee47c6))
* 🎸 add role for order list ([549192b](https://github.com/OHIF/Viewers/commit/549192bfd2efede674851fc9491612936c30ac33))
* 🎸 add role for workgroup ([71ba1be](https://github.com/OHIF/Viewers/commit/71ba1be8d20ee0b517b7088184f347d6f6c435b0))
* 🎸 add role hide ai result ([6cf8452](https://github.com/OHIF/Viewers/commit/6cf84520043d4d318c3ab7d79456b278524dc7de))
* 🎸 add role show ai tab only ([a64b96f](https://github.com/OHIF/Viewers/commit/a64b96fe2fa34f5263206eb5b350730a01f7997d))
* 🎸 add role view ai result ([00fbf12](https://github.com/OHIF/Viewers/commit/00fbf126d21bbd26d5e66e1e44f1b1e67d544643))
* 🎸 add rotate right and left ([4b9d15c](https://github.com/OHIF/Viewers/commit/4b9d15c1aad8237bfe8f7a3e9965c0a63df97c9b))
* 🎸 add save and cancel button ([efe9f5f](https://github.com/OHIF/Viewers/commit/efe9f5f5bef2de423d63e13b0c4bb11bef771f30))
* 🎸 add save function for mammo ([ac312af](https://github.com/OHIF/Viewers/commit/ac312afbc2f6dcc98fa65dc57fd26b1b9c846e2a))
* 🎸 add search all ([c884568](https://github.com/OHIF/Viewers/commit/c884568879fe745bf73b3bf853795d3cb694099b))
* 🎸 add select AI dropdown button ([e53b11f](https://github.com/OHIF/Viewers/commit/e53b11fdc055fda06dea4f9402d0b8c9d2ecc0be))
* 🎸 add select dropdown user ([560ba40](https://github.com/OHIF/Viewers/commit/560ba4011d242a0f06dbca3b9e74a313f4278abe))
* 🎸 add selection storage ([4c02278](https://github.com/OHIF/Viewers/commit/4c0227827840497ae1f947c733e06b2e8704726b))
* 🎸 add setting showing tag list ([51b7b46](https://github.com/OHIF/Viewers/commit/51b7b469400d0889dd01fdc3f09720bee5d52296))
* 🎸 add settings chest pannel ([486921c](https://github.com/OHIF/Viewers/commit/486921c263e8c8a3240901357c6f6dda73a623d3))
* 🎸 add settings window level ([e81fc6c](https://github.com/OHIF/Viewers/commit/e81fc6c1db80addb8d7416bfabdef01e64779b59))
* 🎸 add share order ([119f864](https://github.com/OHIF/Viewers/commit/119f8647dd27a57b061890634f03a64d51df89a2))
* 🎸 add sort in ai result ([3965948](https://github.com/OHIF/Viewers/commit/396594886e4eb36ed8a2d8af721e3188a3376cc7))
* 🎸 add sort in study list ([b661d94](https://github.com/OHIF/Viewers/commit/b661d94ae63067ad15b656ea83e60cc655acdb96))
* 🎸 add split code to mammo and chest extension ([305d501](https://github.com/OHIF/Viewers/commit/305d50132430e294148dc4d840d91e78ff126a23))
* 🎸 Add study status as badge ([ca8866c](https://github.com/OHIF/Viewers/commit/ca8866cb4b1ea64384a888c62f273299bafdf6ae))
* 🎸 add StudyDate filter ([1a1cb4d](https://github.com/OHIF/Viewers/commit/1a1cb4d2b270cb2bdc443e4495da30eb35269218))
* 🎸 add table index ([6082b97](https://github.com/OHIF/Viewers/commit/6082b97b2f4d5248288ec783d0152f55cabd5f2b))
* 🎸 add tabs to studylist ([7632570](https://github.com/OHIF/Viewers/commit/76325706b1b2c9916e196ce2c5db3b462d500556))
* 🎸 add text list disease ([a784d61](https://github.com/OHIF/Viewers/commit/a784d61488e0420e10bcb2f23bde06560f88b4dd))
* 🎸 add text tobe filed in in conclusion tab ([a29ef40](https://github.com/OHIF/Viewers/commit/a29ef409b1acc95f3bdc2877b9a55cfc0fa219ac))
* 🎸 add title for help page ([a90eddb](https://github.com/OHIF/Viewers/commit/a90eddba6b351b12f420ffa8e4b97469a366fd0a))
* 🎸 add tooltip ai model ([5ee5911](https://github.com/OHIF/Viewers/commit/5ee591119c1585ac5afc47e304315e92460a94c5))
* 🎸 add tooltip message for Chest ([cdf2fd9](https://github.com/OHIF/Viewers/commit/cdf2fd9f3a1d62f0de8e6f1f7ef5c429e6993c9c))
* 🎸 add tooltip message for mammo button ([df59d46](https://github.com/OHIF/Viewers/commit/df59d465e8b7d7b08cdbd4443a922947e4dbfa66))
* 🎸 add transition label list ([21ccd35](https://github.com/OHIF/Viewers/commit/21ccd35eb4544625a0a393604cfa17e6465a91f8))
* 🎸 add try catch drawing measurements when change tab ([6366b3e](https://github.com/OHIF/Viewers/commit/6366b3e6e09a1fed9ce8bb4dd36b8362c9657dab))
* 🎸 add upload progress bar ([1b179b3](https://github.com/OHIF/Viewers/commit/1b179b32830e456c6d2dc574214e2994266ba6e1))
* 🎸 add use common ([8455d65](https://github.com/OHIF/Viewers/commit/8455d6503627600225506052bacfa573eea68009))
* 🎸 add username in header ([8944e09](https://github.com/OHIF/Viewers/commit/8944e09c02a897d210de0a81fa3877a52c535092))
* 🎸 add users to workgroup ([d386a99](https://github.com/OHIF/Viewers/commit/d386a990658a902a4c2e8611384ace298f6fa3d4))
* 🎸 add variable for staging add production ([f1a69c2](https://github.com/OHIF/Viewers/commit/f1a69c2c14172b6767d04431fd47c07b3b3a4534))
* 🎸 add variable to switch AI first or doctor first ([b006be4](https://github.com/OHIF/Viewers/commit/b006be47ed587bba710feaaadc60238cb927a399))
* 🎸 add viewer workgroup list ([e49441a](https://github.com/OHIF/Viewers/commit/e49441a193d239a4bfae1a81fb1248d1461a94dc))
* 🎸 add vindoc select tree ([e65d03e](https://github.com/OHIF/Viewers/commit/e65d03efd3ab9e41e8ff082224ee9b2385c2f202))
* 🎸 add vindoc-tools ([ab450d0](https://github.com/OHIF/Viewers/commit/ab450d077a20ad1d326d0cb67369292ed217def1))
* 🎸 add vindrAdmin ([10f8c79](https://github.com/OHIF/Viewers/commit/10f8c79eba454058331cb59fa6427cb9bd22c8a4))
* 🎸 add work group ([127f707](https://github.com/OHIF/Viewers/commit/127f7072025f33d1181fb395c09f9f9938ae042b))
* 🎸 adding new label to checkbox list ([b081aaf](https://github.com/OHIF/Viewers/commit/b081aaf2e49528e750552c3e90e9943ed74522f2))
* 🎸 AI diagnogis by series id ([35fd327](https://github.com/OHIF/Viewers/commit/35fd3276aa816cce1ce9db1fde483058baf15c81))
* 🎸 Allow routes to load Google Cloud DICOM Stores in the Study List ([#1069](https://github.com/OHIF/Viewers/issues/1069)) ([21b586b](https://github.com/OHIF/Viewers/commit/21b586b08f3dde6613859712a9e0577dece564db))
* 🎸 Allow Shift for multi select on OrderDashboard ([3818582](https://github.com/OHIF/Viewers/commit/3818582159776ac8afbaadeb890584b2d618fca9))
* 🎸 Allow Shift on StudyList ([aaa984d](https://github.com/OHIF/Viewers/commit/aaa984ddac11a7a3e9c312c979eaa7efa22c8a19))
* 🎸 allow sort in study list ([c449961](https://github.com/OHIF/Viewers/commit/c449961be57430de0271616bafe5a53482672ef5))
* 🎸 alway show second approval after first approval clicked ([9ed9afc](https://github.com/OHIF/Viewers/commit/9ed9afca97853ec1d9061aed3ab7eea3dae96140))
* 🎸 always show AI tab when admin login ([a2ed907](https://github.com/OHIF/Viewers/commit/a2ed9075b09d4cbb6db1b2f609709b4aa8d74efb))
* 🎸 apply new API for StudyList ([1eb2c95](https://github.com/OHIF/Viewers/commit/1eb2c95789633d8cb79e8e58100a89fa86ddd988))
* 🎸 apply set window level for mpr view ([79571d3](https://github.com/OHIF/Viewers/commit/79571d36082c9583d6d26c339e9ccd50e68c7a21))
* 🎸 apply sort to URL for OrderDashboard ([097d49c](https://github.com/OHIF/Viewers/commit/097d49cd0902c9acb61bff23c24cbc122c0168fc))
* 🎸 apply sort to URL for StudyList ([7ed6c44](https://github.com/OHIF/Viewers/commit/7ed6c448377f18f3bb4398bff81f4c7a73bb7000))
* 🎸 apply styling from design ([0637cc5](https://github.com/OHIF/Viewers/commit/0637cc52fe2e87f3c7d4144eb6801afc63fe35d8))
* 🎸 assign user to work group ([00b523f](https://github.com/OHIF/Viewers/commit/00b523f10826800c3ae8c05d208576acf308a1eb))
* 🎸 assign user to workgroup ([cf1c322](https://github.com/OHIF/Viewers/commit/cf1c32202f0095fdf02b4dd967b9d4cf6c7d0c5f))
* 🎸 attach token when we has refresh token ([c463f31](https://github.com/OHIF/Viewers/commit/c463f3135b405d49bc4bbbabeb6122170f07af69))
* 🎸 authorization step 2 ([d376072](https://github.com/OHIF/Viewers/commit/d376072ea5429b3d8f32aefdc5f8252d6ed7464f))
* 🎸 auto close window level setting on mouse leave ([5301cbd](https://github.com/OHIF/Viewers/commit/5301cbd28f3923aad82f28e38718cf8dca618505))
* 🎸 auto prevent autosuggesstion ([4d44e05](https://github.com/OHIF/Viewers/commit/4d44e05b44829f046b9fec5ad18027c92d4abe4f))
* 🎸 axios redirect follow location ([e4e8c51](https://github.com/OHIF/Viewers/commit/e4e8c51a76dc316f4b7f9b02d778bb8e4e217e71))
* 🎸 Bring back batch analysis ([659e6b8](https://github.com/OHIF/Viewers/commit/659e6b809cad95f17069a916a38182699e53ca24))
* 🎸 cache doctor result ([1b9d9b8](https://github.com/OHIF/Viewers/commit/1b9d9b82875e87b28f57f8178315003cf76c5c0d))
* 🎸 calculate position of label list ([0d505a5](https://github.com/OHIF/Viewers/commit/0d505a56a4bd112f1574546c67030390fb9c88a1))
* 🎸 call api ai analysis ([bfb616a](https://github.com/OHIF/Viewers/commit/bfb616a5a436902c4d12cfb858da64d9a89d7f69))
* 🎸 call api diagnosis ([917c8de](https://github.com/OHIF/Viewers/commit/917c8dedaa8f7a8446f2804fbee3def82c046a7e))
* 🎸 call feedback function with timeout ([8c3b7f5](https://github.com/OHIF/Viewers/commit/8c3b7f5b82e0bc06042813cefc43f2743be18db7))
* 🎸 cancel mpr view when change series ([0157ba5](https://github.com/OHIF/Viewers/commit/0157ba5d3728fd70c5baf7847aa38e9ac935e0b4))
* 🎸 cancel upload queue ([51c752b](https://github.com/OHIF/Viewers/commit/51c752b7229c8fc2abd9b6f411b21688dc37649d))
* 🎸 change "suspected diseases" text ([3f32663](https://github.com/OHIF/Viewers/commit/3f32663af27c6aa20f0eeade1138b0c759f7f657))
* 🎸 change api diagnose ([ccec77e](https://github.com/OHIF/Viewers/commit/ccec77e751d25e623e7735687469db73e7b23010))
* 🎸 change api diagnose in viewer ([16548ab](https://github.com/OHIF/Viewers/commit/16548ab92c91cb66e756aab83e62d4faf1a52ccc))
* 🎸 change api url ([97b0b17](https://github.com/OHIF/Viewers/commit/97b0b174414dacaf515fc099d1c1ee312c6c7873))
* 🎸 change dicom web url ([1958ffd](https://github.com/OHIF/Viewers/commit/1958ffda54cfc4ab00fc2f68a6541efb4763723d))
* 🎸 change footer infor ([9a7acc3](https://github.com/OHIF/Viewers/commit/9a7acc3a251cdba1ff9a664d97f2a468023effbb))
* 🎸 change icon radio select ([1948f80](https://github.com/OHIF/Viewers/commit/1948f8053cfa8910916e6cce75fdcbc0c05c7a93))
* 🎸 change id name of predict extensions ([9393e86](https://github.com/OHIF/Viewers/commit/9393e86b8e5ec26a2a6cc0de67a76fe28a09b3e2))
* 🎸 change localization approve button ([fb1ffdf](https://github.com/OHIF/Viewers/commit/fb1ffdf8945082bc57a125c1c3744bf7eafb3fc2))
* 🎸 change page title name ([f7c135e](https://github.com/OHIF/Viewers/commit/f7c135ef75502b0bb7d4d8fcd4253aa74b936d57))
* 🎸 change panel work group title ([62d5271](https://github.com/OHIF/Viewers/commit/62d527114b7c20313f8444a95c1bb92e96c22574))
* 🎸 change report form ([e766e4b](https://github.com/OHIF/Viewers/commit/e766e4b6dee3ef8dd0e2816868e8f0fef251142b))
* 🎸 change role vindr-admin to vindr-reviewer ([c41b9ba](https://github.com/OHIF/Viewers/commit/c41b9ba45d3f3b9fedac02e76e9eeabf832e32d6))
* 🎸 change roles naem ([c894c37](https://github.com/OHIF/Viewers/commit/c894c372631ee7c93eddb1768bed3ff4d497115d))
* 🎸 change route workgroups ([a1a6bc6](https://github.com/OHIF/Viewers/commit/a1a6bc658a76e0264f4243ef8dacd798e4d02994))
* 🎸 change tab order list ([7cb5ed3](https://github.com/OHIF/Viewers/commit/7cb5ed343ee5c800ee20701c89aa61b69d23c989))
* 🎸 check 302 redirect login ([6fed059](https://github.com/OHIF/Viewers/commit/6fed0599bf2831baa454b0a35570cd0dcf42f839))
* 🎸 check 302 redirect with reponse success ([dd12539](https://github.com/OHIF/Viewers/commit/dd12539c4f9276c194b03e713dce8890bc069723))
* 🎸 check 302 to redirect ([fce9478](https://github.com/OHIF/Viewers/commit/fce947806723bba4abea3e2d234e426adfd72b4f))
* 🎸 check condition showing pagination on top ([12b59dd](https://github.com/OHIF/Viewers/commit/12b59dd44a87f34cb92f91336670fa760e73afcb))
* 🎸 check deseae with 100% percentage ([f40f96a](https://github.com/OHIF/Viewers/commit/f40f96a1cb547c1d2a788bd41cbbf0fefe1e7247))
* 🎸 check image loaded ids ([31cd68c](https://github.com/OHIF/Viewers/commit/31cd68c233a7bf7b6ef443aae44f69f9249c1e95))
* 🎸 check local has data ([2a01b81](https://github.com/OHIF/Viewers/commit/2a01b819c6c2bd462309fba147783cd8a96e20e4))
* 🎸 check localData loaded to draw box ([d54c2de](https://github.com/OHIF/Viewers/commit/d54c2dee4205c7dd2143a741754c03633772964d))
* 🎸 check metadata loaded when view mpr ([bae39fe](https://github.com/OHIF/Viewers/commit/bae39fe59ecc194e2d9bff4a269b14237242780d))
* 🎸 check model type for mammo and chest ([f9d6423](https://github.com/OHIF/Viewers/commit/f9d6423ebb0b313ee80c414fa984eace0d69c815))
* 🎸 check n/a for chest ([92fb688](https://github.com/OHIF/Viewers/commit/92fb68843c86189eae2c581fd691a2dfde553f88))
* 🎸 check N/A result ([5917bc6](https://github.com/OHIF/Viewers/commit/5917bc61775ea5873303d73219626eb1fe179233))
* 🎸 check redirect with status 302 ([ae5f6bd](https://github.com/OHIF/Viewers/commit/ae5f6bd131a0542ee01b5a3867a7973276e22b6a))
* 🎸 check workgroups of study loaded ([714e087](https://github.com/OHIF/Viewers/commit/714e087f808111385bfee8ff9da9ce5efe44c481))
* 🎸 chnage predictionPanel.json to Vindoc.json ([3183f96](https://github.com/OHIF/Viewers/commit/3183f96ca9a94bb70737b362ede238d13db369da))
* 🎸 clear measurement box after fill data ([e4c3709](https://github.com/OHIF/Viewers/commit/e4c3709a3a306cc86865ecf28f93782cbe35a5aa))
* 🎸 clear upload in background ([2d6d1cc](https://github.com/OHIF/Viewers/commit/2d6d1cc9dcb60994577e156d4781f97e2a998c27))
* 🎸 clearbox before get new AI result ([e4d36de](https://github.com/OHIF/Viewers/commit/e4d36dea822779a325ca338706b1377afc0b94b5))
* 🎸 close upload form when click on cancel button ([fd7e862](https://github.com/OHIF/Viewers/commit/fd7e862ea8f7cb70910c304ffc7af09fb46ca1bd))
* 🎸 collapse popup login ([a340b45](https://github.com/OHIF/Viewers/commit/a340b45d6d107814671b3fb2ab1fd6f21619b032))
* 🎸 comment pdf component render ([18ae44d](https://github.com/OHIF/Viewers/commit/18ae44d02824f13890b08ab213726a52c81b60ef))
* 🎸 compare global string ([e4c9df5](https://github.com/OHIF/Viewers/commit/e4c9df57d3cbdc45def5ee5230e8b93371c79a67))
* 🎸 Configuration so viewer tools can nix handles ([#1304](https://github.com/OHIF/Viewers/issues/1304)) ([63594d3](https://github.com/OHIF/Viewers/commit/63594d36b0bdba59f0901095aed70b75fb05172d)), closes [#1223](https://github.com/OHIF/Viewers/issues/1223)
* 🎸 control filters by URL ([eea3d59](https://github.com/OHIF/Viewers/commit/eea3d598efe13eff479f3a1da959a4e11c171525))
* 🎸 control pagination by URL ([aee15ef](https://github.com/OHIF/Viewers/commit/aee15ef2f894044cfb8b8c04751f6224310487a4))
* 🎸 create AdminAccess ([35c98dd](https://github.com/OHIF/Viewers/commit/35c98dd1a31500721d115b64844d286e326416c5))
* 🎸 create collapse button in conclusion ([5b9769b](https://github.com/OHIF/Viewers/commit/5b9769b413ecd90215475e50429ce32cc367fb1f))
* 🎸 create doctor tab and ai tab ([ee45408](https://github.com/OHIF/Viewers/commit/ee45408b46d3b60def4d08d9600f4eee126b51c3))
* 🎸 create grouplabel when draw new label ([d42b4de](https://github.com/OHIF/Viewers/commit/d42b4deea6fe15ce72385c7245cfd174ad1b0a8f))
* 🎸 create mammo extension space ([5a2f419](https://github.com/OHIF/Viewers/commit/5a2f4190d98f211efa61ac98b14271cf0fc4caa9))
* 🎸 create new mammo conclusion before apply ai resutl ([70a06cf](https://github.com/OHIF/Viewers/commit/70a06cf7e5d64985065dfdf9cc7a915c257f58a8))
* 🎸 create radio tree ([cb4780d](https://github.com/OHIF/Viewers/commit/cb4780dc0e2eb1f253d00b6ce47f9cc3d17a2daf))
* 🎸 custom generic commands module ([d1818a9](https://github.com/OHIF/Viewers/commit/d1818a9843fda29ea24bc329f7b3075aa9c702fd))
* 🎸 deactive all box when update measurement ([2c90961](https://github.com/OHIF/Viewers/commit/2c90961bcd1515a04573fe1a5ca674717aefde13))
* 🎸 defaut routing is OrderDashboard ([8640301](https://github.com/OHIF/Viewers/commit/8640301973e9d906269ef933e3c2becad3ba4149))
* 🎸 delete .env file ([4b2eeea](https://github.com/OHIF/Viewers/commit/4b2eeeaff0a70b304398294f3779d403a8bafbf5))
* 🎸 delete hdsd pdf ([bcdd7e3](https://github.com/OHIF/Viewers/commit/bcdd7e339ce064fa94d7e72110bf52da19d3c722))
* 🎸 DICOM SR STOW on MeasurementAPI ([#954](https://github.com/OHIF/Viewers/issues/954)) ([ebe1af8](https://github.com/OHIF/Viewers/commit/ebe1af8d4f75d2483eba869655906d7829bd9666)), closes [#758](https://github.com/OHIF/Viewers/issues/758)
* 🎸 disable doctor tab ([08d3c92](https://github.com/OHIF/Viewers/commit/08d3c92a764018e48fc5cd4b065693477b62ccf6))
* 🎸 disable right click on AI tab ([139ca80](https://github.com/OHIF/Viewers/commit/139ca801f27f7d4cbd576e12c9d27741e7ee58d9))
* 🎸 dispatch ai result to store ([f5ad879](https://github.com/OHIF/Viewers/commit/f5ad879062b2792be6d155e2a2fa49b1b480d945))
* 🎸 Display correct series/instances ([66ab530](https://github.com/OHIF/Viewers/commit/66ab5303406a1012ee67111a5a914424b1a71c25))
* 🎸 Display diseases on study list ([79b0c5a](https://github.com/OHIF/Viewers/commit/79b0c5a275973551dd40667f20ff155b719b431f))
* 🎸 Display diseases over 0.5 ([50b6e88](https://github.com/OHIF/Viewers/commit/50b6e881a0a96b5973080c1dbcd56d165ecb611e))
* 🎸 display DoctorResult column ([10bb402](https://github.com/OHIF/Viewers/commit/10bb402fa41447409922b76df98c5a775486e07e))
* 🎸 display index and selected studies ([e13f2a6](https://github.com/OHIF/Viewers/commit/e13f2a63584b33b27346efd6d041a4fc5fa2cbff))
* 🎸 Display more info from Patient Study ([1617ae7](https://github.com/OHIF/Viewers/commit/1617ae7d26d2cd7097690a4d09be00a0e2f89b64))
* 🎸 display number of NEW and APPROVED studies by filters ([edc3f8d](https://github.com/OHIF/Viewers/commit/edc3f8de382f28172b819c7c9e5c6578ee6633f4))
* 🎸 double click to view full grid ([dfc6c57](https://github.com/OHIF/Viewers/commit/dfc6c577366acf4d23b7ecc0e118417cd7dedf62))
* 🎸 down concurrency to one ([9cbe207](https://github.com/OHIF/Viewers/commit/9cbe207cec704e5e92809251a73cf952210cf228))
* 🎸 draw fullbox when click on Load more button ([2e490b8](https://github.com/OHIF/Viewers/commit/2e490b84d32e65c557890c297c94985956ab27d6))
* 🎸 draw measurements from AI tab ([ce62408](https://github.com/OHIF/Viewers/commit/ce62408883b3297ea3b61f7ef9c6d992ff62b244))
* 🎸 echo auth url ([9574ef1](https://github.com/OHIF/Viewers/commit/9574ef1eeb0485a644345db0ad95c8355e325865))
* 🎸 edit AI tab result ([e5c4155](https://github.com/OHIF/Viewers/commit/e5c4155a550b0e62cbf1fef0fed86bfb44d8fae3))
* 🎸 edit image path ([99d9a6a](https://github.com/OHIF/Viewers/commit/99d9a6a4c1effd569e41a6049a3d51b203b12e38))
* 🎸 enable livect ([275a3a3](https://github.com/OHIF/Viewers/commit/275a3a3ce19913aad951e661dfd55b2da5e6e808))
* 🎸 export default usestudies ([ea66ed0](https://github.com/OHIF/Viewers/commit/ea66ed01bafa1989b31d7cb5035ed893e36dd799))
* 🎸 Expose extension config to modules ([#1279](https://github.com/OHIF/Viewers/issues/1279)) ([4ea239a](https://github.com/OHIF/Viewers/commit/4ea239a9535ef297e23387c186e537ab273744ea)), closes [#1268](https://github.com/OHIF/Viewers/issues/1268)
* 🎸 fill result to the doctor conclusion ([02300eb](https://github.com/OHIF/Viewers/commit/02300eb5a4d89e7543675b3c51669bf90c7b1643))
* 🎸 Filter by url query param for seriesInstnaceUID ([#1117](https://github.com/OHIF/Viewers/issues/1117)) ([e208f2e](https://github.com/OHIF/Viewers/commit/e208f2e6a9c49b16dadead0a917f657cf023929a)), closes [#1118](https://github.com/OHIF/Viewers/issues/1118)
* 🎸 filter feedback by action ([af51aad](https://github.com/OHIF/Viewers/commit/af51aad2f46ea63254df750248e33b52159c628d))
* 🎸 Filter/promote multiple series instances ([#1533](https://github.com/OHIF/Viewers/issues/1533)) ([5fdace1](https://github.com/OHIF/Viewers/commit/5fdace1432b4b1bd31a60c5db6bd681610ec2b8e))
* 🎸 finish clone doctor tab from ai tab ([1360534](https://github.com/OHIF/Viewers/commit/13605348c5685adf41252124e697f002d4054a2e))
* 🎸 finish group label left and right ([69b4c26](https://github.com/OHIF/Viewers/commit/69b4c265f4547c0bc9d5cdc0996272e29116701f))
* 🎸 fix double rad name ([be21f26](https://github.com/OHIF/Viewers/commit/be21f26ff21689961da1ad779fc796f5d3104aa7))
* 🎸 fix draw CT segmentation ([5bfd9e9](https://github.com/OHIF/Viewers/commit/5bfd9e999f63c62188894caa298e3abab38cf36a))
* 🎸 fix error load multipe box ([2ff3b6d](https://github.com/OHIF/Viewers/commit/2ff3b6dbc981eeddb9ed4acfec2945d2f6d21d91))
* 🎸 fix error no finding value ([48edb1f](https://github.com/OHIF/Viewers/commit/48edb1f56228beced8bd1016533838ff7a1907b6))
* 🎸 fix error upload dicoms ([c3578e4](https://github.com/OHIF/Viewers/commit/c3578e45a29b96a6e74a31a084a64374bd07f4d0))
* 🎸 fix height labelling popup ([7f3ec97](https://github.com/OHIF/Viewers/commit/7f3ec971c208c7ebedf4a88980dcadbe59ffb095))
* 🎸 fix load more button when select model AI ([3a32d60](https://github.com/OHIF/Viewers/commit/3a32d6018e96b44ab359f0fad195a8518a38c522))
* 🎸 fix position for mammo ([1b0d269](https://github.com/OHIF/Viewers/commit/1b0d26984fc5bc061537eb79ab8ffedb6a686f94))
* 🎸 fix select AI analyze button ([e0d67c5](https://github.com/OHIF/Viewers/commit/e0d67c5d310f6ff7d66a5779ac775921a32c5335))
* 🎸 fix showing no finding and no data ([a3d240b](https://github.com/OHIF/Viewers/commit/a3d240b916195e9ea30275a557c7b9c37e933da2))
* 🎸 fix store mammo conclusion ([5e9d114](https://github.com/OHIF/Viewers/commit/5e9d1143adf10ea897cb4cc9e3faf3091f805ff0))
* 🎸 fix variable extension name ([473a246](https://github.com/OHIF/Viewers/commit/473a246f0507ae9d0ad6af9d1333c8ae100b23eb))
* 🎸 fix z-index collapse button ([01d2226](https://github.com/OHIF/Viewers/commit/01d222650753c0ca2977e841284d47f517714eb9))
* 🎸 flex column mammo table ([aecb04b](https://github.com/OHIF/Viewers/commit/aecb04b36b1fcb72f8309c95473b669710cfda21))
* 🎸 get all work group ([b9c5461](https://github.com/OHIF/Viewers/commit/b9c5461ea270632f8e0771a6ee25b492201feb91))
* 🎸 get max of conlution ([4f13193](https://github.com/OHIF/Viewers/commit/4f1319373acfb3481f9a52a07e9665bef5b02554))
* 🎸 get prefix name ([5b74d47](https://github.com/OHIF/Viewers/commit/5b74d47d03b1c6c51f9379688455bcd8f940a521))
* 🎸 get rating action result ([088db59](https://github.com/OHIF/Viewers/commit/088db59765576fc7a7c603079d75b5881928a9b5))
* 🎸 get user actions data ([48a97c5](https://github.com/OHIF/Viewers/commit/48a97c5060d5e99436b56e5c09e405ddddc8d063))
* 🎸 get user info in VinDrStandaloneViewer ([83e608f](https://github.com/OHIF/Viewers/commit/83e608f5df84e553e190eb5471387477b680fcaa))
* 🎸 get user list ([8027833](https://github.com/OHIF/Viewers/commit/80278333a5c6c95f79d378c5b8e7eb1b61158820))
* 🎸 get workgroups of study ([26daf82](https://github.com/OHIF/Viewers/commit/26daf82ee5615a92f6ca6676d67a137e5ba68cef))
* 🎸 group diagnose result ([97429f6](https://github.com/OHIF/Viewers/commit/97429f6bdf9d06d4824c2cdb17b4a4389fda928d))
* 🎸 group label in left and right ([60b4639](https://github.com/OHIF/Viewers/commit/60b4639d9531a4f4866cdf47e0c8bb8d933c389f))
* 🎸 group label list ([47a34a7](https://github.com/OHIF/Viewers/commit/47a34a70b2eb938ede8525869c3ff6120ad30d04))
* 🎸 group label right and left ([013ea24](https://github.com/OHIF/Viewers/commit/013ea24a02774ed3b4f050012beb6c68120be39c))
* 🎸 group measurement by label name ([adbb0de](https://github.com/OHIF/Viewers/commit/adbb0ded432150b61fff08e086993f9edba1faa3))
* 🎸 group measurent box by label ([081a3d5](https://github.com/OHIF/Viewers/commit/081a3d565036ca4edfdaf0618e6e8d73599e46df))
* 🎸 grouping mammo by position ([5aeff94](https://github.com/OHIF/Viewers/commit/5aeff9400d2736a59356da49c81347f0af56e3d4))
* 🎸 handle toggle all box ([5da51af](https://github.com/OHIF/Viewers/commit/5da51afd888789d217c0f030b2a70f0626767503))
* 🎸 hidden load more button when loaded ([471dfe7](https://github.com/OHIF/Viewers/commit/471dfe708fcf75688ad4c41abc372fa82efc3720))
* 🎸 hide AI model that not supported ([e028d47](https://github.com/OHIF/Viewers/commit/e028d47249a34c1d09905b36f39f1da043a7c081))
* 🎸 hide ai result column ([e9ada9f](https://github.com/OHIF/Viewers/commit/e9ada9fbb9345a1f37b1d77feb4815974bdc4361))
* 🎸 hide annotation setting in Menu ([ed86b67](https://github.com/OHIF/Viewers/commit/ed86b6715f66a35fafd85066d76a702bff4f5ef9))
* 🎸 hide box number ([9f65cf0](https://github.com/OHIF/Viewers/commit/9f65cf0d34a698d72226b03e10c34ac9888bc8a4))
* 🎸 hide doctor result in study list ([9a1d72d](https://github.com/OHIF/Viewers/commit/9a1d72d792cd3542bdc0f1d54f902f7ccb5b7261))
* 🎸 hide function hover on box ([c26f99c](https://github.com/OHIF/Viewers/commit/c26f99c8ab02cb0c23c1c40aed62938b1edfbd0f))
* 🎸 hide function remove all annotations ([d497c8a](https://github.com/OHIF/Viewers/commit/d497c8ae4a6a03a94ba2780a51f0d2d9fa4c0b94))
* 🎸 hide n/a in studylist ([d730488](https://github.com/OHIF/Viewers/commit/d7304882cbb2f63936e6e427a1e166bcbb589692))
* 🎸 hide native measurement table ([9218922](https://github.com/OHIF/Viewers/commit/921892238f3c12962f0380fc04065df451ca476b))
* 🎸 hide tag list on viewer ([0f39b03](https://github.com/OHIF/Viewers/commit/0f39b0334ccdce69cdb01c8268666f5ddc16850f))
* 🎸 hide tag list on viewport ([15d2b1b](https://github.com/OHIF/Viewers/commit/15d2b1b66dc61c3a98312bb411c925f433e2df0a))
* 🎸 highlight conclusion ([7275057](https://github.com/OHIF/Viewers/commit/727505796e7dae0e20bfbb11b2b14e36d3cec2ec))
* 🎸 highlight disease name ([2d07819](https://github.com/OHIF/Viewers/commit/2d07819bd6ea28f68ca830b62e35d89d8de28134))
* 🎸 hold mammo label group ([f93f065](https://github.com/OHIF/Viewers/commit/f93f0651a09cb45a3d1ef07f00aa8a110dac1689))
* 🎸 hotfix clear filter order list ([c047532](https://github.com/OHIF/Viewers/commit/c04753264af8ffc6fa4be164872ec9c22b62bd08))
* 🎸 hover mouse and toggle box ([2195c19](https://github.com/OHIF/Viewers/commit/2195c19a43acd0c487720b13df73e03470eccc22))
* 🎸 ignore .env in viewer file ([67d73e2](https://github.com/OHIF/Viewers/commit/67d73e295bf2f4385d03d11b3685450808bf3b3c))
* 🎸 import useAccessControl mammo ([8a2ff59](https://github.com/OHIF/Viewers/commit/8a2ff595b716c1815f845429414f38687729c051))
* 🎸 Improve usability of Google Cloud adapter, including direct routes to studies ([#989](https://github.com/OHIF/Viewers/issues/989)) ([2bc361c](https://github.com/OHIF/Viewers/commit/2bc361cdca2b18bcb03c0eb0a7cd7ef43f83e3c8))
* 🎸 increase box index ([c93fa94](https://github.com/OHIF/Viewers/commit/c93fa949b7136da848bedb2679839f53608fe7b2))
* 🎸 init vindr web workers ([bf36cb8](https://github.com/OHIF/Viewers/commit/bf36cb8cb9b023de36f8f0dcc7307a2a6cdc907a))
* 🎸 initialize extensions state on chest measurement table ([2a34f17](https://github.com/OHIF/Viewers/commit/2a34f177924a46c73e76bd5a76df17e8fb8a761f))
* 🎸 keep check modality ([f4526c6](https://github.com/OHIF/Viewers/commit/f4526c6cc0e852b770fa807f68fb40f45f529485))
* 🎸 load data into two tabs ([14d3874](https://github.com/OHIF/Viewers/commit/14d3874dc39b868bd3fdb7ccb36f0855bdf19b1e))
* 🎸 load first workgroup result in viewer ([5e649f7](https://github.com/OHIF/Viewers/commit/5e649f731cf0caf131b148ebe422046a0d68adf9))
* 🎸 Load local file or folder using native dialog ([#870](https://github.com/OHIF/Viewers/issues/870)) ([c221dd8](https://github.com/OHIF/Viewers/commit/c221dd86d2d34d32e2fccb436efc85b5039514e7))
* 🎸 Load spinner when selecting gcloud store. Add key on td ([#1034](https://github.com/OHIF/Viewers/issues/1034)) ([e62f403](https://github.com/OHIF/Viewers/commit/e62f403fe9e3df56713128e3d59045824b086d8d)), closes [#1057](https://github.com/OHIF/Viewers/issues/1057)
* 🎸 load workgroup after add new ([87a890e](https://github.com/OHIF/Viewers/commit/87a890eddd3c2bcbe181c380c02dbc213ad629ea))
* 🎸 localization all tag text ([a2728eb](https://github.com/OHIF/Viewers/commit/a2728eb43d283e80c241c2ee95586e07d1024f15))
* 🎸 localization for bouding box ([0805844](https://github.com/OHIF/Viewers/commit/0805844f57a27d29ae22caa7a80cd56c05633f07))
* 🎸 localization in CreateOrder popup ([da879d1](https://github.com/OHIF/Viewers/commit/da879d106caff82c92c89885b8d1a20c7adf083c))
* 🎸 localization popup mammo ([041051f](https://github.com/OHIF/Viewers/commit/041051fb75d89af198b5a8bc177589900d9ade4d))
* 🎸 localization prediction panel ([330eab7](https://github.com/OHIF/Viewers/commit/330eab7f4fb940a52708aae11b5c1586fcd07efa))
* 🎸 localization toolbar selected ([e4c6804](https://github.com/OHIF/Viewers/commit/e4c68049886470bd9977955f7377c2f40e458312))
* 🎸 localize bouding box vietnamese ([e56e95f](https://github.com/OHIF/Viewers/commit/e56e95f03523368d4aba331133d9d001f73e5837))
* 🎸 logic show ai and doctor tab first ([311905d](https://github.com/OHIF/Viewers/commit/311905dfd54b477cfd5cf3e43802a6a4f503f5ab))
* 🎸 making doctor result call api ([c4a961f](https://github.com/OHIF/Viewers/commit/c4a961ff0e87e3d3d7131a4cc1752187a4a091fc))
* 🎸 mammo group ([177682c](https://github.com/OHIF/Viewers/commit/177682cec08f02f7f5fa6e550a48d19632304efd))
* 🎸 mapping all instances mammo ([f7e92e8](https://github.com/OHIF/Viewers/commit/f7e92e80d49813511bf0c3645b090c48f82112b3))
* 🎸 mapping multiple box in one item ([8188778](https://github.com/OHIF/Viewers/commit/81887781b217d12382b7021660ac2f89781643f0))
* 🎸 MeasurementService ([#1314](https://github.com/OHIF/Viewers/issues/1314)) ([0c37a40](https://github.com/OHIF/Viewers/commit/0c37a406d963569af8c3be24c697dafd42712dfc))
* 🎸 merge chest conclusion ([0808eb0](https://github.com/OHIF/Viewers/commit/0808eb07005ab76164700f673a4e961b41fbcee4))
* 🎸 merge chest conclusion and showing no finding ([af6774d](https://github.com/OHIF/Viewers/commit/af6774defdc8a91fe91c7501c66febdb1e1a0ce7))
* 🎸 move AI model to right of toolbar ([b18069c](https://github.com/OHIF/Viewers/commit/b18069c46857e833844f37092d7ec94d27a63040))
* 🎸 move cc and mammo position ([56ddd09](https://github.com/OHIF/Viewers/commit/56ddd098a272325a985d6284f7d17cf09edcb93b))
* 🎸 move clear all viewport to app extensions ([67a9a1e](https://github.com/OHIF/Viewers/commit/67a9a1e8a57436bfa3c67d5071b283d626539c4a))
* 🎸 move docs to assets folder ([7389c44](https://github.com/OHIF/Viewers/commit/7389c445ad9c2b2629f168daeb5267d6676846a3))
* 🎸 move draw measurement to router ([b169444](https://github.com/OHIF/Viewers/commit/b1694440d70fa97c70aabfd8b69adec2b5b15f22))
* 🎸 move logic load doctor result to context ([043ebea](https://github.com/OHIF/Viewers/commit/043ebea32356aefd497e6c7e39c4c543e6d9381f))
* 🎸 MPR UI improvements. Added MinIP, AvgIP, slab thickness slider and mode toggle ([#947](https://github.com/OHIF/Viewers/issues/947)) ([c79c0c3](https://github.com/OHIF/Viewers/commit/c79c0c301d003d21b095dd708e33c55033f29794))
* 🎸 mv orderlist to work group ([2244b37](https://github.com/OHIF/Viewers/commit/2244b37ff6a553e4cdcd8312316c5b98b6bd2a8c))
* 🎸 New modal provider ([#1110](https://github.com/OHIF/Viewers/issues/1110)) ([5ee832b](https://github.com/OHIF/Viewers/commit/5ee832b19505a4e8e5756660ce6ed03a7f18dec3)), closes [#1086](https://github.com/OHIF/Viewers/issues/1086) [#1116](https://github.com/OHIF/Viewers/issues/1116)
* 🎸 not disable second approval ([5095c89](https://github.com/OHIF/Viewers/commit/5095c891a7c8848f1575bb8d545a454b223a3655))
* 🎸 Only allow reconstruction of datasets that make sense ([#1010](https://github.com/OHIF/Viewers/issues/1010)) ([2d75e01](https://github.com/OHIF/Viewers/commit/2d75e01ea07f4322ccfaccd108089299be43d0df)), closes [#561](https://github.com/OHIF/Viewers/issues/561)
* 🎸 only show menu that have role ([7a4360b](https://github.com/OHIF/Viewers/commit/7a4360b57ba66c3aeb7cc0b3c087e8c86213e93e))
* 🎸 open new tab for help link ([c441612](https://github.com/OHIF/Viewers/commit/c4416125017fc59fb605255859596bb052f3dac7))
* 🎸 Optional disable measurements panel in app config ([#1912](https://github.com/OHIF/Viewers/issues/1912)) ([3d86b5f](https://github.com/OHIF/Viewers/commit/3d86b5f876d6481bcc57de88a0ba1f974f4ee263)), closes [#1864](https://github.com/OHIF/Viewers/issues/1864)
* 🎸 order group list ([a9eb2b9](https://github.com/OHIF/Viewers/commit/a9eb2b9c77f750c78e0762413f20ba36f059b7a4))
* 🎸 order mammo conclusion result ([e23bb8f](https://github.com/OHIF/Viewers/commit/e23bb8f5a514da191c134747cb94740fe77b851c))
* 🎸 pass panelTabName for context in chest panel ([1850ab6](https://github.com/OHIF/Viewers/commit/1850ab69de638dd04f259fc635b111a27782dce4))
* 🎸 place tooltip position ([63afcb0](https://github.com/OHIF/Viewers/commit/63afcb08bed24a0ad1d7db6c228d933a55e09e1e))
* 🎸 polling call ai result ([732cd70](https://github.com/OHIF/Viewers/commit/732cd70e7644e818b3aa2d37b47925978c0fac39))
* 🎸 polling reqest get new AI result ([3d58e75](https://github.com/OHIF/Viewers/commit/3d58e75b59e00023f8f872d3ffccb5f17dac0351))
* 🎸 polling request studylist after 15s ([8b462a0](https://github.com/OHIF/Viewers/commit/8b462a0c0d0b14e59ff9291342465a558c20d47d))
* 🎸 polling time request study ([a4cb1c6](https://github.com/OHIF/Viewers/commit/a4cb1c6c175711d0204d4fb5caffb23078acfb77))
* 🎸 pool request ai result ([cabbeab](https://github.com/OHIF/Viewers/commit/cabbeabbee961346613419b2b5f72a77dd635d02))
* 🎸 pre format doctor result ([f65a4f8](https://github.com/OHIF/Viewers/commit/f65a4f86a1c7bd7cb9144c45f69ab511d0afa71f))
* 🎸 prevent close dialog outsite ([f3eaab8](https://github.com/OHIF/Viewers/commit/f3eaab83cc166052b1999189d7021e788587c07a))
* 🎸 prevent event click on box in tab panel ([f0fd0b7](https://github.com/OHIF/Viewers/commit/f0fd0b7da9e133b89432b3a306053bb4c0068579))
* 🎸 prevent load studylist in background ([0db97a8](https://github.com/OHIF/Viewers/commit/0db97a8872678d79ec8e959a7eafa6f18cb205d5))
* 🎸 prevent re-render when change user owner on viewer ([e701348](https://github.com/OHIF/Viewers/commit/e701348c5584590e83ee99816b401da036c34e4f))
* 🎸 re order header item ([d3e488b](https://github.com/OHIF/Viewers/commit/d3e488b83455535194cdfdec3da1752c2697f2fe))
* 🎸 React custom component on toolbar button ([#935](https://github.com/OHIF/Viewers/issues/935)) ([a90605c](https://github.com/OHIF/Viewers/commit/a90605c82d0c831b955db011698247b8ee104bde))
* 🎸 rebase ohif git ([84ff157](https://github.com/OHIF/Viewers/commit/84ff1575ff7be053b5f452771ba3062995d49040))
* 🎸 redirect follow response url ([980fa0c](https://github.com/OHIF/Viewers/commit/980fa0cc1b12abd0fa2e4765f5b3ccdcce425ad5))
* 🎸 refactor active mammo series ([89a5a97](https://github.com/OHIF/Viewers/commit/89a5a97e54e40b4d4c4e5671ef211037493bb5dd))
* 🎸 refactor aI tab ([1cb87a5](https://github.com/OHIF/Viewers/commit/1cb87a5a6c7cb54a17f1407fcc57e2ef4e948a01))
* 🎸 refactor AppRoot ([50a6091](https://github.com/OHIF/Viewers/commit/50a609181870dc4a7c946a06e9760727c0bf46b4))
* 🎸 refactor approve and export button ([8f09347](https://github.com/OHIF/Viewers/commit/8f09347db0ee914545e347774c5ae626925bb248))
* 🎸 refactor callback hover on box function ([5425e97](https://github.com/OHIF/Viewers/commit/5425e9794f6f3aab8fe37b8fb4024d8a22e354bf))
* 🎸 refactor change chest tab ([1fcf316](https://github.com/OHIF/Viewers/commit/1fcf3162226ab90a851f1fdcea4d80705739f2e1))
* 🎸 refactor chest conclusion list ([03bbc80](https://github.com/OHIF/Viewers/commit/03bbc80dd8074147658dcbb9545a556cca87157f))
* 🎸 refactor chest tabs ([c40751c](https://github.com/OHIF/Viewers/commit/c40751c33195da133177b0c24749d7d69f4edb9c))
* 🎸 refactor chest using hooks ([fb5fa24](https://github.com/OHIF/Viewers/commit/fb5fa249db69b3613842972388e46024050efaca))
* 🎸 refactor chest utils ([87bd965](https://github.com/OHIF/Viewers/commit/87bd96538f7606360cbda768830151a101d14386))
* 🎸 refactor code to awoid warning ([e38c3c7](https://github.com/OHIF/Viewers/commit/e38c3c7b40c316762a6f118a7d62dcd7b85cb7a5))
* 🎸 refactor delete box ([acaaf70](https://github.com/OHIF/Viewers/commit/acaaf707f0c3a8a98c2e23ade8ee310160a2b283))
* 🎸 refactor mammo ([575eede](https://github.com/OHIF/Viewers/commit/575eede2455fd22890fbd0dd0e4f991d09afaf72))
* 🎸 refactor mammo processing ([354a41a](https://github.com/OHIF/Viewers/commit/354a41af7b6ee437d4ea3e46d470ce533a40c36c))
* 🎸 refactor remain files ([44daca2](https://github.com/OHIF/Viewers/commit/44daca2e453e353d1566a81666a412acb0ab75a7))
* 🎸 refactor tabs panel for mammo ([2a62fa4](https://github.com/OHIF/Viewers/commit/2a62fa4115080874a1eb39c75001ec6a7182492a))
* 🎸 refactor use storeage result ([171fdc4](https://github.com/OHIF/Viewers/commit/171fdc46bc1c32c618b3b69122392836763bc294))
* 🎸 refactor user actions ([63a81af](https://github.com/OHIF/Viewers/commit/63a81afd5edd8b651c0b1ae96fdca8608b391e5c))
* 🎸 refator mammo mapping position ([b48e374](https://github.com/OHIF/Viewers/commit/b48e374b0ed042f36c535edc3b50302beb02cee9))
* 🎸 register prediction panel in App.js ([488dadf](https://github.com/OHIF/Viewers/commit/488dadf4fa7847ee3bce331d2eb2ff2f1d0c1da2))
* 🎸 reload page after get ai result ([9257aa0](https://github.com/OHIF/Viewers/commit/9257aa0a64f75e7eff662f60251a9827989436c2))
* 🎸 remove interval after 100s ([ec8fc71](https://github.com/OHIF/Viewers/commit/ec8fc71710913d8dd911d9b1bf231df9e81c5558))
* 🎸 remove single box ([a0981c6](https://github.com/OHIF/Viewers/commit/a0981c64ce27d189806895adca71443f08e006b3))
* 🎸 remove value findings ([7c379a2](https://github.com/OHIF/Viewers/commit/7c379a2601bbbaf38c913b91550824435eb9a534))
* 🎸 remove X USER INFO ([4e195f1](https://github.com/OHIF/Viewers/commit/4e195f152711dec54f9c7a9955d990387fce4d01))
* 🎸 rename ([5a9e0c9](https://github.com/OHIF/Viewers/commit/5a9e0c9a6d649c84c23d7193b9e6285a93e3903d))
* 🎸 rename panelrouter ([87bbe3a](https://github.com/OHIF/Viewers/commit/87bbe3aab47e70a50842061e7d5917c8c0d06889))
* 🎸 rename prediction tab names ([303b422](https://github.com/OHIF/Viewers/commit/303b422acc8baddf1b0ee1da5f14f2903ce2032b))
* 🎸 render load saved data ([5cc4cbc](https://github.com/OHIF/Viewers/commit/5cc4cbcbfc5fb3098e19c558c9bee811bdae7e94))
* 🎸 reset chest when scroll image ([c4c7e80](https://github.com/OHIF/Viewers/commit/c4c7e80eb7ce2ad35b18489b7645d933434a081c))
* 🎸 reset viewport with chest only ([e7ce06f](https://github.com/OHIF/Viewers/commit/e7ce06fafa09f6fe456a8c2cbddc2a50a4701d93))
* 🎸 reset window level onNewImage ([0bfa1cd](https://github.com/OHIF/Viewers/commit/0bfa1cd122a54c48352a08dc460b1bba04fe42c5))
* 🎸 responsive popup labeling height ([b9e0a3e](https://github.com/OHIF/Viewers/commit/b9e0a3e5e874d9a663b2870d43ef8dc27dfe2c41))
* 🎸 responsive popup max-height ([98eeffe](https://github.com/OHIF/Viewers/commit/98eeffe022cf5013614e7cc69ceb7e6159cc04a3))
* 🎸 responsive with screen hd ([4736013](https://github.com/OHIF/Viewers/commit/47360135dd270e6ef97b911ef250953daa78783c))
* 🎸 reuse function instances mapping ([1beb8af](https://github.com/OHIF/Viewers/commit/1beb8af18483430d3235427ddf107acc03761232))
* 🎸 revert highlight conclusin ([5e3b3aa](https://github.com/OHIF/Viewers/commit/5e3b3aa646baaff9322290502e74747e0b13741d))
* 🎸 revert segmentation without try catch ([6b28f14](https://github.com/OHIF/Viewers/commit/6b28f1496bf1a889cf9f63ee56fcd32bede13589))
* 🎸 rm /api studylist ([fbf8778](https://github.com/OHIF/Viewers/commit/fbf8778eec5f08f77a792757ecb034377aba9e31))
* 🎸 rm backend url in studylist ([55096a8](https://github.com/OHIF/Viewers/commit/55096a86863178d762382247fa3b462152655225))
* 🎸 rm console mammo ([e809e73](https://github.com/OHIF/Viewers/commit/e809e737dc0a60bb3f8d646486ac4c43d5b75be0))
* 🎸 rm console.log ([be85aa7](https://github.com/OHIF/Viewers/commit/be85aa7b82623e78933b4cfc31e4d0c0b86888c6))
* 🎸 rm console.log ([43963f1](https://github.com/OHIF/Viewers/commit/43963f1f7e843f79300d224119fa8e01d5f64947))
* 🎸 rm dialog actions on upload popup ([a7eacac](https://github.com/OHIF/Viewers/commit/a7eacac004ced933b1f532b698698b8da00d3759))
* 🎸 rm dicom uploader in order dashboard ([85613e3](https://github.com/OHIF/Viewers/commit/85613e3f418b76ca84d4bb895daaf53bc0a90bac))
* 🎸 rm function order Mammo position and fix load CT ([9f5fb76](https://github.com/OHIF/Viewers/commit/9f5fb76a91383588a7bd94b94b7f75d5d7ae4ae1))
* 🎸 rm highlight disease name ([fbb69f2](https://github.com/OHIF/Viewers/commit/fbb69f2d50c02471306cfa4502cfb5183037d320))
* 🎸 rm import AppSevice ([0b601b7](https://github.com/OHIF/Viewers/commit/0b601b71afad6f4d9c143f8e43cc4cee595b324c))
* 🎸 rm measurement after close labelling ([813ea45](https://github.com/OHIF/Viewers/commit/813ea4551b59fd254e6104c013e7e21964de2aaf))
* 🎸 rm pdf render content ([ec04582](https://github.com/OHIF/Viewers/commit/ec04582f86732f7a48caa5798fd7715f9a37dfa6))
* 🎸 rm react-pdf ([0050694](https://github.com/OHIF/Viewers/commit/0050694804db2a01bb0b129d7ce9f618b242511e))
* 🎸 rm request studylist ([1b3e665](https://github.com/OHIF/Viewers/commit/1b3e6656b4f1cb25d6a19f9e24054f3487d2fa14))
* 🎸 rm set findings notification message ([13df999](https://github.com/OHIF/Viewers/commit/13df999dc95cc5f970337c0ea2691e0423b065ce))
* 🎸 rm showaitabfirst on mammopanel ([ffe6357](https://github.com/OHIF/Viewers/commit/ffe635735896b4743040863b7fea36482954f475))
* 🎸 rm Token when token expire ([72da3e7](https://github.com/OHIF/Viewers/commit/72da3e785dd8ed2e3cc23de5f69e4c2f6e117618))
* 🎸 rm vindr-admin role ([3f4965a](https://github.com/OHIF/Viewers/commit/3f4965a2bf099b7bafc4f34ca69db6e61239021a))
* 🎸 roll back show studylist tab ([851c109](https://github.com/OHIF/Viewers/commit/851c109b6deec397723007f1cc74ff489e9be990))
* 🎸 rollback filter order list ([7cba339](https://github.com/OHIF/Viewers/commit/7cba3395b8aa2e63563fcdad878208f9d84c3f27))
* 🎸 rollback jpeg2000 to raw ([7f00a94](https://github.com/OHIF/Viewers/commit/7f00a9450d801738ecac4068bb6dd3feb54de0ac))
* 🎸 rollback orderdashboard ([8e14bdf](https://github.com/OHIF/Viewers/commit/8e14bdf38f092faef8dc18cde0b163abb510cd80))
* 🎸 rollback show studylist ([3298687](https://github.com/OHIF/Viewers/commit/3298687423593f081e73722ad1da966db70a5d0d))
* 🎸 roudup percent ([746d267](https://github.com/OHIF/Viewers/commit/746d2678be303e0b6cca5b247340704a3684273f))
* 🎸 round percent value ([fede312](https://github.com/OHIF/Viewers/commit/fede312df9e0f76e928fcfe5c2a45aefb62999ad))
* 🎸 routing mammo and chest in prediction panel ([45c769b](https://github.com/OHIF/Viewers/commit/45c769b8eee1c232a90dc837a66a785f7c223813))
* 🎸 Seg jump to slice + show/hide ([835f64d](https://github.com/OHIF/Viewers/commit/835f64d47a9994f6a25aaf3941a4974e215e7e7f))
* 🎸 select model Lung CT ([6ca668a](https://github.com/OHIF/Viewers/commit/6ca668a55e256455cd265c4dbdd20a8c57dc07e3))
* 🎸 send request feedback ([7637a9a](https://github.com/OHIF/Viewers/commit/7637a9a8da1179e4a27412b5290b14409baf6d70))
* 🎸 set 1000 rows in studylist ([9121c40](https://github.com/OHIF/Viewers/commit/9121c403551f00d38b977476b03fce5de8049553))
* 🎸 set active color ([0a82987](https://github.com/OHIF/Viewers/commit/0a829872e34fe2611d3ed1182aa5b4f4d282ea05))
* 🎸 set Chest panel form as default ([da68d98](https://github.com/OHIF/Viewers/commit/da68d98776007fa88dfb8d2f339ae27020da3339))
* 🎸 set date formatted on studylist & set Today as default ([dc20f5d](https://github.com/OHIF/Viewers/commit/dc20f5dad02c9a06e950a3807cff89af43494872))
* 🎸 set default homepage is OrderDashboard ([fad670c](https://github.com/OHIF/Viewers/commit/fad670ceede66cae7af736bc6bcc20e8db972043))
* 🎸 set default numbers rows perpage is 150 ([e3fc77b](https://github.com/OHIF/Viewers/commit/e3fc77b154c60c3ef9a7c5f5e75f0dd1425d7847))
* 🎸 set default routing for user is order list ([f3730e4](https://github.com/OHIF/Viewers/commit/f3730e4fd2a73594ff5426f9777b75734824c41e))
* 🎸 set doctor first ([5b4606f](https://github.com/OHIF/Viewers/commit/5b4606ffa9dbdd5f85bd3b24f95b24c4e6296142))
* 🎸 set inherit height for mammo items ([2c27fc4](https://github.com/OHIF/Viewers/commit/2c27fc4e32cab39b5a99a1772d8d41403cd497f5))
* 🎸 set loadmore button ([2a56010](https://github.com/OHIF/Viewers/commit/2a560105c742e234e85f18a0d612ef02a76bb5de))
* 🎸 set mammo image to 4 viewport ([2f5d13e](https://github.com/OHIF/Viewers/commit/2f5d13eaeeca0bbb5c38e3efd8ee46d7945bb6e3))
* 🎸 set mapping instances mammo ([3786578](https://github.com/OHIF/Viewers/commit/3786578bba21b8a46f20c07cd63012aab35ad4b2))
* 🎸 set margin labelling list with box ([a795bdf](https://github.com/OHIF/Viewers/commit/a795bdfca82dba18ecf82f1bfa76ab7184ef788e))
* 🎸 set max height measurement list ([2c67704](https://github.com/OHIF/Viewers/commit/2c67704c7559b2542b4c6595671131fbcbb4cf6c))
* 🎸 set max height unset popup ([b4fffba](https://github.com/OHIF/Viewers/commit/b4fffba430a7767b27cfdb72ed036599bcc7c34d))
* 🎸 set max popup item height 75vh ([0448193](https://github.com/OHIF/Viewers/commit/0448193d34bb6797ca45cbc6812c0ce11e2252ac))
* 🎸 set maxRedirects 0 - no redirect ([ed26e02](https://github.com/OHIF/Viewers/commit/ed26e02af4fdd60b48252bf6562afd57ab1a3901))
* 🎸 set min height mammo column ([dc21578](https://github.com/OHIF/Viewers/commit/dc215786ddacf2b437357de2c923361e791170da))
* 🎸 set min width upload form ([f87e2f9](https://github.com/OHIF/Viewers/commit/f87e2f98aac5b0956018dc52a11adfd80a9db594))
* 🎸 set open prediction panel ([72b17df](https://github.com/OHIF/Viewers/commit/72b17df4a13093d8add9a43e632a9dea431b40eb))
* 🎸 set permissions stuydlist ([f376220](https://github.com/OHIF/Viewers/commit/f376220851f02dda617cd9ea7acaa1e1555814f9))
* 🎸 set prams * as all order list ([eefa9da](https://github.com/OHIF/Viewers/commit/eefa9dad16756eac452bfd003705a1712960f253))
* 🎸 set reactangle tool as default ([fe26ef7](https://github.com/OHIF/Viewers/commit/fe26ef79f43622a66ac2d321f04cb54256c398d5))
* 🎸 set scrollbar width ([03e3bec](https://github.com/OHIF/Viewers/commit/03e3bec4b2fd864eb1d5bc932f6aa4f3a9c4df63))
* 🎸 set studylist as default ([1806cfd](https://github.com/OHIF/Viewers/commit/1806cfd062e70dbc4d3fe832ae8ab8c06c101cdc))
* 🎸 set timeout refresh token ([da0d376](https://github.com/OHIF/Viewers/commit/da0d3768c5be72d7f39afd3971cffb2335b46a6f))
* 🎸 set vi as default language ([7fcc9d3](https://github.com/OHIF/Viewers/commit/7fcc9d3899bc729c11cf2163ba5ef526ac7d2520))
* 🎸 show active series ([c0027b3](https://github.com/OHIF/Viewers/commit/c0027b3487aa7d5e7e35869a753ae7601814cef2))
* 🎸 show active series ([6954754](https://github.com/OHIF/Viewers/commit/6954754c0e7f95ffe3b2a16036aba1a1f51d6e48))
* 🎸 show ai first and doctor first ([075344e](https://github.com/OHIF/Viewers/commit/075344efd1e4d51c018529e267e1d953100a4ad0))
* 🎸 show ai result after first approval ([4142f8c](https://github.com/OHIF/Viewers/commit/4142f8c845eb2ab926690397ec9871140dc8d5b6))
* 🎸 show ai result base on vindr viewer ai result ([4a2e8a2](https://github.com/OHIF/Viewers/commit/4a2e8a255f04e8cd395c63eac71b154ee697492d))
* 🎸 show ai result on ct ([cb36458](https://github.com/OHIF/Viewers/commit/cb36458467c647a655c9e5fc520686dbd9495a4c))
* 🎸 show ai result when change tab ([d34c14e](https://github.com/OHIF/Viewers/commit/d34c14e833af28d51e1dcd5fddd943bcc1a829b9))
* 🎸 show ai tab first as default ([25754cd](https://github.com/OHIF/Viewers/commit/25754cd6bca66da088e5cefd512e2b0a46365f39))
* 🎸 show ai-tab first ([7b157e2](https://github.com/OHIF/Viewers/commit/7b157e26fea0b826fbb0d903d42dd8d0e1fb3141))
* 🎸 show annotation setting on bottom ([ed8ddac](https://github.com/OHIF/Viewers/commit/ed8ddac739717740a4bd4f87e8db82987451bf0b))
* 🎸 show assign button on study list ([3047e29](https://github.com/OHIF/Viewers/commit/3047e29ad853482153f7b2f53c7b599a0ac29285))
* 🎸 show backgroud when has atleast 2 items row ([b5e52a5](https://github.com/OHIF/Viewers/commit/b5e52a5a6d03b94c691b08852bac7c5dfb73dca0))
* 🎸 show boxnumber when does not loaded ([bc1fbef](https://github.com/OHIF/Viewers/commit/bc1fbefa940fc3c0118e96700a785b2615bccab9))
* 🎸 show close button when upload completed ([08a580d](https://github.com/OHIF/Viewers/commit/08a580d695238efebbcb0488a09138f312fcf95f))
* 🎸 show coment ([d71d71a](https://github.com/OHIF/Viewers/commit/d71d71a43cb8f8425fb242e3ce3c6dbec2ba54e0))
* 🎸 show conclusion ([a8a9690](https://github.com/OHIF/Viewers/commit/a8a96909a235d3a3f0ad4b3f99217430d3854408))
* 🎸 show counter on tab name ([50efd39](https://github.com/OHIF/Viewers/commit/50efd396dc9a0ebe85f0eb9005deab4879cf17ef))
* 🎸 show current checbox ([69740df](https://github.com/OHIF/Viewers/commit/69740dffbc1493e41119b5fdc53cf3b0147f4d48))
* 🎸 show current order content ([b8c7706](https://github.com/OHIF/Viewers/commit/b8c7706cfbc23b258ba6fc10f9d9faba0f242759))
* 🎸 show default avatar icon ([c2797fb](https://github.com/OHIF/Viewers/commit/c2797fbfb8df81b82de9efaec45015057b5f46f4))
* 🎸 show default message in conclusion tab ([a2311e2](https://github.com/OHIF/Viewers/commit/a2311e2601de83981bcc2052b8f135ef94a32fbd))
* 🎸 show doctor and ai result ([c7c8340](https://github.com/OHIF/Viewers/commit/c7c8340e18f45a20ac4e3eeb3373d3353f6a7f33))
* 🎸 show doctor result on viewer by owner ([62cb3d4](https://github.com/OHIF/Viewers/commit/62cb3d43363a428fae3063184b0dac5dfe497290))
* 🎸 show empty text for chest as default ([7337ca4](https://github.com/OHIF/Viewers/commit/7337ca43f34e20d67282572334081c29c44cf4e2))
* 🎸 show first measurement Data when do not load more ([3e027a5](https://github.com/OHIF/Viewers/commit/3e027a56f4cf7a661e1f8519e1ae61049e8cedf0))
* 🎸 show fullscreen loading when has no studies ([5ae6dd6](https://github.com/OHIF/Viewers/commit/5ae6dd61b5f035fe6dd11f170e339e1f59a3079f))
* 🎸 show global for chest and mammo ([17e6cf6](https://github.com/OHIF/Viewers/commit/17e6cf654543e209f59ad010417429564edc20b9))
* 🎸 show labelling default position by mouse down ([cd751c1](https://github.com/OHIF/Viewers/commit/cd751c13bdfd101ff599c717695a207c69e65387))
* 🎸 show labelling popup only with rectangle roi ([7ec148b](https://github.com/OHIF/Viewers/commit/7ec148bcf57f82937228775cf229e5f47a41b0f8))
* 🎸 show loading or no data on table ([0d6fd95](https://github.com/OHIF/Viewers/commit/0d6fd955262794ab7f60e0d9e0f62573eda741c3))
* 🎸 show logic text with no finding ([01910b9](https://github.com/OHIF/Viewers/commit/01910b9470df1fa52dfd12d3a3a9e5bc5ebf5c07))
* 🎸 show mammo conclusion from db ([9356a9c](https://github.com/OHIF/Viewers/commit/9356a9c411702880491bcc02a9b3678221079970))
* 🎸 show mammo doctor tab first ([89fb21a](https://github.com/OHIF/Viewers/commit/89fb21a0aeb239504fc051af76916c812baa9e4e))
* 🎸 show mammo origin conclusion in AI tab ([d9de412](https://github.com/OHIF/Viewers/commit/d9de4129aebcceff5fcb9b0ab17f3d364f06906f))
* 🎸 show mammo series active ([c7c23ea](https://github.com/OHIF/Viewers/commit/c7c23ea9443064d714ee35f8eb38ec199f6bc052))
* 🎸 show messsage when panel has no data ([e9197f5](https://github.com/OHIF/Viewers/commit/e9197f5c7f5c4a0414466b72b6363ddca9aa9ef6))
* 🎸 show more Modality and ViewPosition ([33ecc27](https://github.com/OHIF/Viewers/commit/33ecc27309096c58fab0a52407546cbf9b1c0a39))
* 🎸 show n/a status ([8d2cf3a](https://github.com/OHIF/Viewers/commit/8d2cf3ac72bac8170f3d805e85cc599c64be6810))
* 🎸 show new approve with labelling role ([0fda1ac](https://github.com/OHIF/Viewers/commit/0fda1aca076bf9043253fb8a97fa56873ba39d19))
* 🎸 show no data as default in ai mammo ([41a291f](https://github.com/OHIF/Viewers/commit/41a291fea6109c355138504252ff1a0bc1a58e58))
* 🎸 show no finding text ([77f2d8c](https://github.com/OHIF/Viewers/commit/77f2d8cd2637234eae8f65e5c754f3795adefe11))
* 🎸 show order list status ([3e0b7f8](https://github.com/OHIF/Viewers/commit/3e0b7f8ee1e89cda11ff5e2b7c0ade35938b8262))
* 🎸 show popup add order list ([88ce0b6](https://github.com/OHIF/Viewers/commit/88ce0b6350fabe620f0493b277a7227171084085))
* 🎸 show popup delete order ([3eeadaf](https://github.com/OHIF/Viewers/commit/3eeadaf7aa2518672e6c13744f661d4155f38ed7))
* 🎸 show popup labeling by mouseup event ([512202d](https://github.com/OHIF/Viewers/commit/512202df4836f32eb19c05025fca893ec9927116))
* 🎸 show previous state of doctor on panel ([e8c64d9](https://github.com/OHIF/Viewers/commit/e8c64d990498950176c751239aced67877888cd9))
* 🎸 show select ai model for admin role ([3d0b1dd](https://github.com/OHIF/Viewers/commit/3d0b1ddf19fdc2b76dbd8b0fb38c1a0bef4d6f4c))
* 🎸 show selected item in disease list ([d69b1d7](https://github.com/OHIF/Viewers/commit/d69b1d74a6eae902816da54efba7d4c384cd511a))
* 🎸 show series description ([f24925d](https://github.com/OHIF/Viewers/commit/f24925d4a87ffc72038be3875284935c38a75df3))
* 🎸 show status row status on order list ([6430cc7](https://github.com/OHIF/Viewers/commit/6430cc79150cb6423ef2d4b36a10fd3ce31293ba))
* 🎸 show study list status ([522eaf8](https://github.com/OHIF/Viewers/commit/522eaf81a034a3fddef96f2365c01d8edb24c851))
* 🎸 show studylist as default ([3b86e18](https://github.com/OHIF/Viewers/commit/3b86e18d8e4ed52898d68639f84d1139a7b8262a))
* 🎸 show suffix ([7d0dcdc](https://github.com/OHIF/Viewers/commit/7d0dcdcd6c0e3308a7a9bb4dd3f323484fb6f72c))
* 🎸 show tab name by roles ([3c52e4e](https://github.com/OHIF/Viewers/commit/3c52e4eaeba85ce75d0aa9a7b798b80a9742ca4b))
* 🎸 show temp doctor result when change tab ([86d7a6a](https://github.com/OHIF/Viewers/commit/86d7a6afb93da383beaab15fb03a86d5d222097b))
* 🎸 show upload button with vindr study list admin only ([b9d6c67](https://github.com/OHIF/Viewers/commit/b9d6c67ca7bc5a995387c0cb7d5bb23e1ed3cd7f))
* 🎸 show upload progress status on header ([b55f406](https://github.com/OHIF/Viewers/commit/b55f406e3b568938e43829a2e91746a6bce70a94))
* 🎸 show user list on order popup ([3d6a2c3](https://github.com/OHIF/Viewers/commit/3d6a2c3ffd086e834bc37cfd49c5c1144fd0024d))
* 🎸 show vi as default on header ([912ea66](https://github.com/OHIF/Viewers/commit/912ea66afb27c903b8da502eadfa4a01dbd62535))
* 🎸 show view setting ([15ced1d](https://github.com/OHIF/Viewers/commit/15ced1df6635b4af3167f55539762e8cc5ae16eb))
* 🎸 show window settings for all modalities ([bbad50d](https://github.com/OHIF/Viewers/commit/bbad50d08a78d49172191fad8d5e062f41dbf645))
* 🎸 show workgroup list on order list ([e47a315](https://github.com/OHIF/Viewers/commit/e47a3150852303aa062bab0f4d1a9da63c613831))
* 🎸 showboxLabel for mammo ([4340a25](https://github.com/OHIF/Viewers/commit/4340a259c118d82e1dfe43035d27e1b63b01f534))
* 🎸 showing box selection ([ea56f27](https://github.com/OHIF/Viewers/commit/ea56f27a761e02dd2b18976c9b956d645b1edff2))
* 🎸 showing box without ViewPosition tag ([12e06e8](https://github.com/OHIF/Viewers/commit/12e06e8b06f6b950bbb5b47d2c1b9179919a4dee))
* 🎸 showing conclusion tab ([d7b9b0b](https://github.com/OHIF/Viewers/commit/d7b9b0bdaed46aa681cf93fc7ac169fdb9f05b85))
* 🎸 showing empty message in AI measurent ([bdc6f3f](https://github.com/OHIF/Viewers/commit/bdc6f3fafe9a2450907113ab15c122865b21309d))
* 🎸 showing empty text default in mammo and chest ([e7c7a4d](https://github.com/OHIF/Viewers/commit/e7c7a4dbb592ff8c1fbca06e24bfd08776802d01))
* 🎸 showing labelling in findings tab ([ada1cad](https://github.com/OHIF/Viewers/commit/ada1caddf233b21742d122ee195031875ab0c611))
* 🎸 showing loading when images are not loaded yet ([b02fa87](https://github.com/OHIF/Viewers/commit/b02fa8714442e732bd549401d06dace944d39c81))
* 🎸 showing mammo position ([5414744](https://github.com/OHIF/Viewers/commit/54147448ecb6c19fc338a7147cf71f8634de69cf))
* 🎸 showing measurement table when have new box ([56172eb](https://github.com/OHIF/Viewers/commit/56172eb14386419e3dac48a9060f261d55e9589b))
* 🎸 showing pagination on studies table ([8a96d27](https://github.com/OHIF/Viewers/commit/8a96d27bdc1fba873578cf457048b4755a044c5e))
* 🎸 showing saved data in chest ([bf3794a](https://github.com/OHIF/Viewers/commit/bf3794a4669419c58aed3836d2ab2f4a1219a795))
* 🎸 showing unique box name ([44ba17f](https://github.com/OHIF/Viewers/commit/44ba17f363c542a2d502eaebfcd2560922cdea27))
* 🎸 showing workgroup list on prediction panel ([dc7237f](https://github.com/OHIF/Viewers/commit/dc7237f870fe2d22d9596b0e78a37ed71854b678))
* 🎸 showing worklist on mammo ([ef8a950](https://github.com/OHIF/Viewers/commit/ef8a9507496ba66f735f9130bf1d6ab5301ce6d7))
* 🎸 sort ai result by prob ([5356f51](https://github.com/OHIF/Viewers/commit/5356f5184376016213097ac16581a64b69ce41e0))
* 🎸 split mammo to 4 viewport ([6ce2754](https://github.com/OHIF/Viewers/commit/6ce2754ee52f542e56f0e867fbf974d1e79b2049))
* 🎸 stlying no data message ([0c37267](https://github.com/OHIF/Viewers/commit/0c3726720dcd343034882c496e1c68f1c4b82559))
* 🎸 stlying thumbnail box ([9b15fa5](https://github.com/OHIF/Viewers/commit/9b15fa564aefd92fa8187cc087a4d544987acf51))
* 🎸 store measurement data ([4d1806e](https://github.com/OHIF/Viewers/commit/4d1806e3dc3581008fc39ff2e65b1c29d84ef232))
* 🎸 store token in cookie ([30afe31](https://github.com/OHIF/Viewers/commit/30afe31d94f579b4455ee38a35263c3408514c88))
* 🎸 store viewer open counter ([b8f7cec](https://github.com/OHIF/Viewers/commit/b8f7cec991b0c284843dd19cc299e3d66216c501))
* 🎸 Study List able to call analysis ([f6cc7e0](https://github.com/OHIF/Viewers/commit/f6cc7e09a4e32049028995b87b58752ad95ab33d))
* 🎸 StudyList able to list all studyies ([864f22e](https://github.com/OHIF/Viewers/commit/864f22e71eb010a691f1489f693963dab81607bf))
* 🎸 stying upload form ([63066e9](https://github.com/OHIF/Viewers/commit/63066e9cf5cb70e58dbf007f32e40350e3407677))
* 🎸 style tab button ([ce3befb](https://github.com/OHIF/Viewers/commit/ce3befb6e05e37bd8afb66595f1e6741aa59fe5d))
* 🎸 styling mammo ([bf27e65](https://github.com/OHIF/Viewers/commit/bf27e650bc27e2dcb3287b58c38554a2f693d643))
* 🎸 styling tab button ([d796c01](https://github.com/OHIF/Viewers/commit/d796c0166ebf7af48e04ca02819af1b6e37f498f))
* 🎸 styling tab panel ([e880b52](https://github.com/OHIF/Viewers/commit/e880b5253e1c62ce6e75e6355c7389a9939ecbc1))
* 🎸 styling webkit scrollbar ([784a921](https://github.com/OHIF/Viewers/commit/784a9214352f666bf1f371b3fc84bbda9a6f4092))
* 🎸 styling webkit scrollbar height ([ad15514](https://github.com/OHIF/Viewers/commit/ad15514fc8a7edf1bf4bc5f5646966a708967dd7))
* 🎸 styling window level list ([3484f9e](https://github.com/OHIF/Viewers/commit/3484f9e326802453e93e89191e5ee47f5eb5593b))
* 🎸 swap approve button ([3f97e9a](https://github.com/OHIF/Viewers/commit/3f97e9a2be05ea515a064020af864ebe31a97298))
* 🎸 switch to new upload API ([cec4a37](https://github.com/OHIF/Viewers/commit/cec4a3751823bdee15fc30a987df631a2c2aed5f))
* 🎸 temp refactor ([07b0fe2](https://github.com/OHIF/Viewers/commit/07b0fe25c848e24e82ab92d23d9302c898dbdcc4))
* 🎸 try catch segmentation data ([f0a11ac](https://github.com/OHIF/Viewers/commit/f0a11ac3782432fbf7be5900dd2360099983bfd6))
* 🎸 try order mammo and reset viewport ([2c75156](https://github.com/OHIF/Viewers/commit/2c75156490f74e4e4038f6f92ed64b8d38042f4e))
* 🎸 un comment batch analysis button ([275f4d9](https://github.com/OHIF/Viewers/commit/275f4d92c992a812056d59002702b5d8a98191af))
* 🎸 update add lungct model name ([52453a8](https://github.com/OHIF/Viewers/commit/52453a846424a93705d5f35b2e7198c1407b1d36))
* 🎸 update AI result after refresh ([339ba83](https://github.com/OHIF/Viewers/commit/339ba830bee507f2407c5eb66995bf8f4f8f9f85))
* 🎸 update api ([a7e2a80](https://github.com/OHIF/Viewers/commit/a7e2a80ed14d9ef12353aa48fe9ec63f4beb3d49))
* 🎸 update api diagnose v2 ([08db85f](https://github.com/OHIF/Viewers/commit/08db85f59489a77a4f95ab097ce75f0f2c4fc3c5))
* 🎸 update api with workgroup ([427b242](https://github.com/OHIF/Viewers/commit/427b242b7d5cd1f887d88974d0496cfa3e0e6493))
* 🎸 update authorization flow ([45a58f0](https://github.com/OHIF/Viewers/commit/45a58f024d7e419b81e13f3b8269d8e75ec876a1))
* 🎸 update bouding box number ([c04915f](https://github.com/OHIF/Viewers/commit/c04915f8dd66bebda08321eb925d329a640e43df))
* 🎸 update box number ([fb052ef](https://github.com/OHIF/Viewers/commit/fb052efc5435c55e6a798f79c660074a29380c89))
* 🎸 update call workgroups mammo ([0ecb841](https://github.com/OHIF/Viewers/commit/0ecb8417ef7d5853bb3b284e7d342795f50684b6))
* 🎸 update check authorization when has no session ([4162c23](https://github.com/OHIF/Viewers/commit/4162c23a15946c43b8b8543e9a9afed5e61f972c))
* 🎸 update chest panel showing workgroups ([d93bf3c](https://github.com/OHIF/Viewers/commit/d93bf3cac689955c5e027d7af315cc1c266f1f5d))
* 🎸 update concurrency upload queue to 2 ([99aecc6](https://github.com/OHIF/Viewers/commit/99aecc685374fb3cd2a7c75b284bde35832afd3b))
* 🎸 update current tabname for mammo ([bc93afd](https://github.com/OHIF/Viewers/commit/bc93afde16e5263c39e077d42926031cb65b6150))
* 🎸 update dicom tag table ([e1213dc](https://github.com/OHIF/Viewers/commit/e1213dc522b66b0f0d7ed7f9a7121a39da29fa62))
* 🎸 update disease name ([be183b2](https://github.com/OHIF/Viewers/commit/be183b2b78c85c2cc75fab6ef9550c859bc2528d))
* 🎸 update draw segment multiple series ([50bfc1a](https://github.com/OHIF/Viewers/commit/50bfc1a19481e70bc4418d927d2afc02d223d932))
* 🎸 update drawing mpr segmentations ([0c72d37](https://github.com/OHIF/Viewers/commit/0c72d37d75cab6d3c9861d1ae0ebd4c5072314df))
* 🎸 update endpoint with workspace prefix ([f3bf21b](https://github.com/OHIF/Viewers/commit/f3bf21b2986d9c7cf4c160ffb5a3efd4c6286036))
* 🎸 update filter search ([93c36d4](https://github.com/OHIF/Viewers/commit/93c36d4d6b58765977188fb5feb1f6b468e3400f))
* 🎸 Update hotkeys and user preferences modal ([#1135](https://github.com/OHIF/Viewers/issues/1135)) ([e62f5f8](https://github.com/OHIF/Viewers/commit/e62f5f8dd28ab363f23671cd21cee115abb870ff)), closes [#923](https://github.com/OHIF/Viewers/issues/923)
* 🎸 update load mammo series ([de62355](https://github.com/OHIF/Viewers/commit/de62355337a61cf9241edc1967a803874ba83e1d))
* 🎸 update localization ai button ([2d35ba0](https://github.com/OHIF/Viewers/commit/2d35ba04aa7e87c0c73ac0a0c2ea5be670d4bd2e))
* 🎸 update mammo ([d1c69d4](https://github.com/OHIF/Viewers/commit/d1c69d464e26d8efa53f361bad5fa133de9911ec))
* 🎸 update mammo AI tab ([7973c09](https://github.com/OHIF/Viewers/commit/7973c09fde7fb8c8887ec7555ca85e02e2aeff91))
* 🎸 update mammo measurement item ([82029de](https://github.com/OHIF/Viewers/commit/82029ded63ceae940028c1895a5fd42ed1e4b129))
* 🎸 update mammo measurements ([28e0e83](https://github.com/OHIF/Viewers/commit/28e0e837f45c0fbf1e37640041b635b675e7fde7))
* 🎸 update mammo workflow ([4645fee](https://github.com/OHIF/Viewers/commit/4645fee7a60e16c29f51ec367cab3626fe056755))
* 🎸 update measurement number in bouding boax ([b3182db](https://github.com/OHIF/Viewers/commit/b3182dbfdda295f1219c45c0b05b850ad98119b7))
* 🎸 update no permission header ([53e7478](https://github.com/OHIF/Viewers/commit/53e7478d81c08461daff498ddc7fa5d5cab31f5c))
* 🎸 update open panel button ([dfb8cf4](https://github.com/OHIF/Viewers/commit/dfb8cf4a0c6bb08629e8fca5dd92817cb19ca8d4))
* 🎸 update order list search ([6ae5b55](https://github.com/OHIF/Viewers/commit/6ae5b55bcd7b82760a12df13590415aba7901754))
* 🎸 update panel icon ([b3ff8db](https://github.com/OHIF/Viewers/commit/b3ff8db2f39a1de0ea185fe5c200d6106ca77444))
* 🎸 update queue to 4 ([2c24203](https://github.com/OHIF/Viewers/commit/2c242032debe12d8865483b87784f6af01ea2c94))
* 🎸 Update react-vtkjs-viewport usage to use requestPool ([#1984](https://github.com/OHIF/Viewers/issues/1984)) ([bb5f30c](https://github.com/OHIF/Viewers/commit/bb5f30ce2a0192d2e021beaaadfff22fd38e17b9))
* 🎸 update roles studylist ([2f63e1a](https://github.com/OHIF/Viewers/commit/2f63e1a7428b199619b41bd0e351ebe10e4c72d6))
* 🎸 update routing with roles ([f7c597e](https://github.com/OHIF/Viewers/commit/f7c597e443643805641d201d0116f7151e01f2e5))
* 🎸 update search star fulltext ([0aea9a8](https://github.com/OHIF/Viewers/commit/0aea9a8caf86d46d40317ed19901bdc39d7e03f4))
* 🎸 update server endpoit url ([43a0bac](https://github.com/OHIF/Viewers/commit/43a0bacc9458482376725f3708a88078c23f90e3))
* 🎸 update show finding percent ([4519bbf](https://github.com/OHIF/Viewers/commit/4519bbf6845227588f683fb52ee3ec6823652c42))
* 🎸 update show no finding and 100% value ([8279964](https://github.com/OHIF/Viewers/commit/82799643abb3fe4594e581e4b937f8857b820a03))
* 🎸 update showing ai result ([ee50344](https://github.com/OHIF/Viewers/commit/ee50344cd08092ec61a429d1818f984ed4c06ea9))
* 🎸 update showing comment box ([f8f28da](https://github.com/OHIF/Viewers/commit/f8f28da373b8261561f161399fbe15c082c9f208))
* 🎸 update tag list popup ([db2734b](https://github.com/OHIF/Viewers/commit/db2734b4fe75b58e4935b79022a5cf12e1fe7a7d))
* 🎸 update tag style ([5433449](https://github.com/OHIF/Viewers/commit/54334490c41048b632b2347d6482485a6f0d8f80))
* 🎸 update variable ([c8d80e3](https://github.com/OHIF/Viewers/commit/c8d80e3303b693b4ca0dc9a1c633dcb20b9522b5))
* 🎸 update version of react-viewport ([21c3034](https://github.com/OHIF/Viewers/commit/21c3034153bb1c954946c2a78a911971ded8dea6))
* 🎸 update version vindr-viewport ([a599550](https://github.com/OHIF/Viewers/commit/a5995507fd0162f70cd46a0e24986eba13757677))
* 🎸 update vindoc-tools version ([1975eb2](https://github.com/OHIF/Viewers/commit/1975eb2a09ee20b53cf70799aa6fd4c4e0625337))
* 🎸 update vindocExtensions reducer ([47e407e](https://github.com/OHIF/Viewers/commit/47e407ea58807f58563ce93e86b07bbbd6959b62))
* 🎸 update vindr-tools version ([c057e02](https://github.com/OHIF/Viewers/commit/c057e02110febc8b3b94624153aadc1b122de64d))
* 🎸 update workgroup with interceptor ([d112cea](https://github.com/OHIF/Viewers/commit/d112cea55cea2cd5701c17b5a1cfa3118ce37c06))
* 🎸 update zindex labelling ([222ba97](https://github.com/OHIF/Viewers/commit/222ba974a1ae0aebf1dd9138188db07b4974cbe2))
* 🎸 use measurementNumber instead of lession number ([06af090](https://github.com/OHIF/Viewers/commit/06af0906f480f6b4f079c407d7693ecbe464df53))
* 🎸 use memmo for ai tab ([2f1960a](https://github.com/OHIF/Viewers/commit/2f1960abe2345cb06410da8b179e23daf7b55261))
* 🎸 use role for showing study status ([af8b17c](https://github.com/OHIF/Viewers/commit/af8b17c719cf316064b571d3191429a49dca73ac))
* 🎸 useCallback for chest pdf reporting ([85146de](https://github.com/OHIF/Viewers/commit/85146de1cc8adf1b9f90c86227ee089ffbceda1f))
* 🎸 using jpeg2000 transfer-syntax ([89ffce2](https://github.com/OHIF/Viewers/commit/89ffce2d1521f17927990b7d9ca62ecefa2707af))
* 🎸 using new logic to routing mammo and chest ([cf88965](https://github.com/OHIF/Viewers/commit/cf889652782c3fb8c8d8e24c3f4df9367ea3a549))
* 🎸 using radio instead of checklist ([07a37a0](https://github.com/OHIF/Viewers/commit/07a37a01be656f29fc1b1f9d05ebac65edcd374f))
* 🎸 validate local data whenn drawing box ([073adb3](https://github.com/OHIF/Viewers/commit/073adb3f7220bc5af068cc0fb87d003eff893e69))
* 🎸 watch show detail in ai conclusion ([d41452e](https://github.com/OHIF/Viewers/commit/d41452e3efd74644281f660ed747b803f762b620))
* Add browser info and app version ([#1046](https://github.com/OHIF/Viewers/issues/1046)) ([c217b8b](https://github.com/OHIF/Viewers/commit/c217b8bbdefb7f40c24d460856ec7f4523d70b8a))
* add endpoint orthanc ([26fc1be](https://github.com/OHIF/Viewers/commit/26fc1beb1a443bec1600100f0cb39aa7bd4cdd9f))
* add header menu item ([fbfe665](https://github.com/OHIF/Viewers/commit/fbfe665faab3b59dcaefc9cfc5649975a397e725))
* Add new annotate tool using new dialog service ([#1211](https://github.com/OHIF/Viewers/issues/1211)) ([8fd3af1](https://github.com/OHIF/Viewers/commit/8fd3af1e137e793f1b482760a22591c64a072047))
* add Vindoc Header ([75cb67a](https://github.com/OHIF/Viewers/commit/75cb67ad454d1f3224e30f28812eb320ca963761))
* add vindoc icons ([cdcbf48](https://github.com/OHIF/Viewers/commit/cdcbf4846cbf28e55b07f4a2e5a6dd89bce6e2ff))
* add vindoc measurement panel ([d428ff0](https://github.com/OHIF/Viewers/commit/d428ff08cc0627350940473b84fc51efc2457103))
* apply polling to studylist ([63b5245](https://github.com/OHIF/Viewers/commit/63b52450524ce7403fa2bb53b78151b01893cf94))
* configuration to hook into XHR Error handling ([e96205d](https://github.com/OHIF/Viewers/commit/e96205de35e5bec14dc8a9a8509db3dd4e6ecdb6))
* Custom Healthcare API endpoint ([#1367](https://github.com/OHIF/Viewers/issues/1367)) ([a5d6bc6](https://github.com/OHIF/Viewers/commit/a5d6bc6a51784ed3a8a40d4ae773de9099f116b9))
* expose some app internals as window.app ([#1735](https://github.com/OHIF/Viewers/issues/1735)) ([63fd656](https://github.com/OHIF/Viewers/commit/63fd65690cba450721870a6222e0fb3ad71bb291))
* expose UiNotifications service ([#1172](https://github.com/OHIF/Viewers/issues/1172)) ([5c04e34](https://github.com/OHIF/Viewers/commit/5c04e34c8fb2394ab7acd9eb4f2ab12afeb2f255))
* filter field for google api windows ([#1170](https://github.com/OHIF/Viewers/issues/1170)) ([c59c5b3](https://github.com/OHIF/Viewers/commit/c59c5b3f14d44f1c06aa396125a1f4caaa431c25))
* Implement a 'Exit 2D MPR' button in the toolbar ([c99e0d8](https://github.com/OHIF/Viewers/commit/c99e0d8a23e3957aca7ebf566b529753cea0fac5))
* Issue 879 viewer route query param not filtering but promoting ([#1141](https://github.com/OHIF/Viewers/issues/1141)) ([b17f753](https://github.com/OHIF/Viewers/commit/b17f753e6222045252ef885e40233681541a32e1)), closes [#1118](https://github.com/OHIF/Viewers/issues/1118)
* lesion-tracker extension ([#1420](https://github.com/OHIF/Viewers/issues/1420)) ([73e4409](https://github.com/OHIF/Viewers/commit/73e440968ce4699d081a9c9f2d21dd68095b3056))
* modal provider ([#1151](https://github.com/OHIF/Viewers/issues/1151)) ([75d88bc](https://github.com/OHIF/Viewers/commit/75d88bc454710d2dcdbc7d68c4d9df041159c840)), closes [#1086](https://github.com/OHIF/Viewers/issues/1086) [#1116](https://github.com/OHIF/Viewers/issues/1116) [#1116](https://github.com/OHIF/Viewers/issues/1116) [#1146](https://github.com/OHIF/Viewers/issues/1146) [#1142](https://github.com/OHIF/Viewers/issues/1142) [#1143](https://github.com/OHIF/Viewers/issues/1143) [#1110](https://github.com/OHIF/Viewers/issues/1110) [#1086](https://github.com/OHIF/Viewers/issues/1086) [#1116](https://github.com/OHIF/Viewers/issues/1116) [#1119](https://github.com/OHIF/Viewers/issues/1119)
* Multiple fixes and implementation changes to react-cornerstone-viewport ([1cc94f3](https://github.com/OHIF/Viewers/commit/1cc94f36a77cccb34cab68dcd7f991241801290f))
* New dialog service ([#1202](https://github.com/OHIF/Viewers/issues/1202)) ([f65639c](https://github.com/OHIF/Viewers/commit/f65639c2b0dab01decd20cab2cef4263cb4fab37))
* Notification Service ([#1011](https://github.com/OHIF/Viewers/issues/1011)) ([92c8996](https://github.com/OHIF/Viewers/commit/92c8996ed49107fc555203bd673544cf2c87b6f8))
* responsive study list ([#1068](https://github.com/OHIF/Viewers/issues/1068)) ([2cdef4b](https://github.com/OHIF/Viewers/commit/2cdef4b9844cc2ce61e9ce76b5a942ba7051fe16))
* rm border left of study menu list ([1f3d0e7](https://github.com/OHIF/Viewers/commit/1f3d0e7a7d3ab1e6fed82af10e4131fa66b0e901))
* Segmentations Settings UI - Phase 1 [#1391](https://github.com/OHIF/Viewers/issues/1391) ([#1392](https://github.com/OHIF/Viewers/issues/1392)) ([e8842cf](https://github.com/OHIF/Viewers/commit/e8842cf8aebde98db7fc123e4867c8288552331f)), closes [#1423](https://github.com/OHIF/Viewers/issues/1423)
* service worker ([#1045](https://github.com/OHIF/Viewers/issues/1045)) ([cf51368](https://github.com/OHIF/Viewers/commit/cf5136899eac08300ec4f15474a6440129ef7a9a))
* set logo max width ([1732427](https://github.com/OHIF/Viewers/commit/1732427aadceef89957dc9c4271abdf68b4dfdf4))
* set the authorization header for DICOMWeb requests if provided in query string ([#1646](https://github.com/OHIF/Viewers/issues/1646)) ([450c80b](https://github.com/OHIF/Viewers/commit/450c80b9d5f172be8b5713b422370360325a0afc))
* Snapshot Download Tool ([#840](https://github.com/OHIF/Viewers/issues/840)) ([450e098](https://github.com/OHIF/Viewers/commit/450e0981a5ba054fcfcb85eeaeb18371af9088f8))
* style background color ([28beec5](https://github.com/OHIF/Viewers/commit/28beec58806e1e1c3c84c047677f44ab02f907da))
* Use QIDO + WADO to load series metadata individually rather than the entire study metadata at once ([#953](https://github.com/OHIF/Viewers/issues/953)) ([9e10c2b](https://github.com/OHIF/Viewers/commit/9e10c2b2ded5dcf5c05779ca9ef9a7fc453750ad))


* feat!: Ability to configure cornerstone tools via extension configuration (#1229) ([55a5806](https://github.com/OHIF/Viewers/commit/55a580659ecb74ca6433461d8f9a05c2a2b69533)), closes [#1229](https://github.com/OHIF/Viewers/issues/1229)


### Code Refactoring

* 💡 React components to consume appConfig using Context ([#852](https://github.com/OHIF/Viewers/issues/852)) ([7c4ee73](https://github.com/OHIF/Viewers/commit/7c4ee734fab23940d74e96dbe79c08dec25e8798)), closes [#725](https://github.com/OHIF/Viewers/issues/725) [#725](https://github.com/OHIF/Viewers/issues/725)


### Reverts

* Revert "fix: MPR initialization (#1062)" (#1064) ([a6c1e6c](https://github.com/OHIF/Viewers/commit/a6c1e6c71702d2635ed992cc05f8f0b0e3e444c8)), closes [#1062](https://github.com/OHIF/Viewers/issues/1062) [#1064](https://github.com/OHIF/Viewers/issues/1064)
* Revert "* fix: 🐛 Enabling workbox to cache webfonts and fonts-stylesheets, add local fonts for UMD build (#906)" (#961) ([ae26904](https://github.com/OHIF/Viewers/commit/ae2690488593bd5d5b22350552a64312baf36e51)), closes [#906](https://github.com/OHIF/Viewers/issues/906) [#961](https://github.com/OHIF/Viewers/issues/961)
* Revert "chore: full --watch tag to reduce confusion" ([ee88672](https://github.com/OHIF/Viewers/commit/ee88672d375749a8deed2e64a1a27834c7924a43))


### BREAKING CHANGES

* 🧨 However we start to load once the first set of metadata arrives. We need
to wait until all series metadata is fetched.
* modifies the exposed react <App /> components props. The contract for providing configuration for the app has changed. Please reference updated documentation for guidance.
* 1013
* DICOM Seg
* #725





## [4.5.22](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.21...@ohif/viewer@4.5.22) (2020-10-20)

**Note:** Version bump only for package @ohif/viewer





## [4.5.21](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.20...@ohif/viewer@4.5.21) (2020-10-15)

**Note:** Version bump only for package @ohif/viewer





## [4.5.20](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.19...@ohif/viewer@4.5.20) (2020-10-13)

**Note:** Version bump only for package @ohif/viewer





## [4.5.19](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.18...@ohif/viewer@4.5.19) (2020-10-13)

**Note:** Version bump only for package @ohif/viewer





## [4.5.18](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.17...@ohif/viewer@4.5.18) (2020-10-12)

**Note:** Version bump only for package @ohif/viewer





## [4.5.17](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.16...@ohif/viewer@4.5.17) (2020-10-07)

**Note:** Version bump only for package @ohif/viewer





## [4.5.16](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.15...@ohif/viewer@4.5.16) (2020-10-06)

**Note:** Version bump only for package @ohif/viewer





## [4.5.15](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.14...@ohif/viewer@4.5.15) (2020-09-30)

**Note:** Version bump only for package @ohif/viewer





## [4.5.14](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.13...@ohif/viewer@4.5.14) (2020-09-30)

**Note:** Version bump only for package @ohif/viewer





## [4.5.13](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.12...@ohif/viewer@4.5.13) (2020-09-29)

**Note:** Version bump only for package @ohif/viewer





## [4.5.12](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.11...@ohif/viewer@4.5.12) (2020-09-24)

**Note:** Version bump only for package @ohif/viewer





## [4.5.11](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.10...@ohif/viewer@4.5.11) (2020-09-17)

**Note:** Version bump only for package @ohif/viewer





## [4.5.10](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.9...@ohif/viewer@4.5.10) (2020-09-17)

**Note:** Version bump only for package @ohif/viewer





## [4.5.9](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.8...@ohif/viewer@4.5.9) (2020-09-10)

**Note:** Version bump only for package @ohif/viewer





## [4.5.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.7...@ohif/viewer@4.5.8) (2020-09-10)

**Note:** Version bump only for package @ohif/viewer





## [4.5.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.6...@ohif/viewer@4.5.7) (2020-09-09)

**Note:** Version bump only for package @ohif/viewer





## [4.5.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.5...@ohif/viewer@4.5.6) (2020-09-03)

**Note:** Version bump only for package @ohif/viewer





## [4.5.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.4...@ohif/viewer@4.5.5) (2020-09-03)

**Note:** Version bump only for package @ohif/viewer





## [4.5.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.3...@ohif/viewer@4.5.4) (2020-09-02)

**Note:** Version bump only for package @ohif/viewer





## [4.5.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.2...@ohif/viewer@4.5.3) (2020-08-28)

**Note:** Version bump only for package @ohif/viewer





## [4.5.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.1...@ohif/viewer@4.5.2) (2020-08-24)

**Note:** Version bump only for package @ohif/viewer





## [4.5.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.5.0...@ohif/viewer@4.5.1) (2020-08-20)

**Note:** Version bump only for package @ohif/viewer





# [4.5.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.4.1...@ohif/viewer@4.5.0) (2020-08-20)


### Features

* 🎸 Filter/promote multiple series instances ([#1533](https://github.com/OHIF/Viewers/issues/1533)) ([5fdace1](https://github.com/OHIF/Viewers/commit/5fdace1432b4b1bd31a60c5db6bd681610ec2b8e))





## [4.4.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.4.0...@ohif/viewer@4.4.1) (2020-08-20)


### Bug Fixes

* Fix incorrect command name in Percy test ([#1999](https://github.com/OHIF/Viewers/issues/1999)) ([ebdcde1](https://github.com/OHIF/Viewers/commit/ebdcde1c4d9c95393cf79cc9994f5d60f6d66fdd))





# [4.4.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.3.1...@ohif/viewer@4.4.0) (2020-08-18)


### Features

* 🎸 Update react-vtkjs-viewport usage to use requestPool ([#1984](https://github.com/OHIF/Viewers/issues/1984)) ([bb5f30c](https://github.com/OHIF/Viewers/commit/bb5f30ce2a0192d2e021beaaadfff22fd38e17b9))





## [4.3.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.3.0...@ohif/viewer@4.3.1) (2020-08-10)

**Note:** Version bump only for package @ohif/viewer





# [4.3.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.16...@ohif/viewer@4.3.0) (2020-08-10)


### Features

* 🎸 Optional disable measurements panel in app config ([#1912](https://github.com/OHIF/Viewers/issues/1912)) ([3d86b5f](https://github.com/OHIF/Viewers/commit/3d86b5f876d6481bcc57de88a0ba1f974f4ee263)), closes [#1864](https://github.com/OHIF/Viewers/issues/1864)





## [4.2.16](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.15...@ohif/viewer@4.2.16) (2020-08-10)

**Note:** Version bump only for package @ohif/viewer





## [4.2.15](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.14...@ohif/viewer@4.2.15) (2020-08-05)

**Note:** Version bump only for package @ohif/viewer





## [4.2.14](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.13...@ohif/viewer@4.2.14) (2020-08-05)

**Note:** Version bump only for package @ohif/viewer





## [4.2.13](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.12...@ohif/viewer@4.2.13) (2020-07-23)

**Note:** Version bump only for package @ohif/viewer





## [4.2.12](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.11...@ohif/viewer@4.2.12) (2020-07-22)


### Bug Fixes

* Switch DICOMFileUploader to use the UIModalService ([#1904](https://github.com/OHIF/Viewers/issues/1904)) ([7772fee](https://github.com/OHIF/Viewers/commit/7772fee21ae6a65994e1251e2f1d2554b47781be))





## [4.2.11](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.10...@ohif/viewer@4.2.11) (2020-07-13)


### Bug Fixes

* 🐛 - Put guards in all places that a cornerstone re-render ([#1899](https://github.com/OHIF/Viewers/issues/1899)) ([451f7ea](https://github.com/OHIF/Viewers/commit/451f7eab9258e7a193eb362e0926b13aedc4b3c9))





## [4.2.10](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.9...@ohif/viewer@4.2.10) (2020-07-13)

**Note:** Version bump only for package @ohif/viewer





## [4.2.9](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.8...@ohif/viewer@4.2.9) (2020-07-13)

**Note:** Version bump only for package @ohif/viewer





## [4.2.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.7...@ohif/viewer@4.2.8) (2020-07-13)


### Bug Fixes

* 🐛 Fix RT Panel hide/show and Fix looping load errors ([#1877](https://github.com/OHIF/Viewers/issues/1877)) ([e7cc735](https://github.com/OHIF/Viewers/commit/e7cc735c03d02eeb0d3af4ba02c15ed4f81bbec2))





## [4.2.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.6...@ohif/viewer@4.2.7) (2020-06-18)

**Note:** Version bump only for package @ohif/viewer





## [4.2.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.5...@ohif/viewer@4.2.6) (2020-06-18)

**Note:** Version bump only for package @ohif/viewer





## [4.2.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.4...@ohif/viewer@4.2.5) (2020-06-15)


### Bug Fixes

* 🐛 Disable seg panel when data for seg unavailable ([#1732](https://github.com/OHIF/Viewers/issues/1732)) ([698e900](https://github.com/OHIF/Viewers/commit/698e900b85121d3c2a46747c443ef69fb7a8c95b)), closes [#1728](https://github.com/OHIF/Viewers/issues/1728)





## [4.2.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.3...@ohif/viewer@4.2.4) (2020-06-15)


### Bug Fixes

* OIDC Redirect erases query parameters ([#1773](https://github.com/OHIF/Viewers/issues/1773)) ([6123741](https://github.com/OHIF/Viewers/commit/6123741765e81d0bea8fbd5dbb0f310aaca0fb33))





## [4.2.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.2...@ohif/viewer@4.2.3) (2020-06-05)

**Note:** Version bump only for package @ohif/viewer





## [4.2.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.1...@ohif/viewer@4.2.2) (2020-06-04)

**Note:** Version bump only for package @ohif/viewer





## [4.2.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.2.0...@ohif/viewer@4.2.1) (2020-06-04)

**Note:** Version bump only for package @ohif/viewer





# [4.2.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.1.0...@ohif/viewer@4.2.0) (2020-06-04)


### Features

* 🎸 1729 - error boundary wrapper ([#1764](https://github.com/OHIF/Viewers/issues/1764)) ([c02b232](https://github.com/OHIF/Viewers/commit/c02b232b0cc24f38af5d5e3831d987d048e60ada))





# [4.1.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@4.0.0...@ohif/viewer@4.1.0) (2020-05-15)


### Features

* expose some app internals as window.app ([#1735](https://github.com/OHIF/Viewers/issues/1735)) ([63fd656](https://github.com/OHIF/Viewers/commit/63fd65690cba450721870a6222e0fb3ad71bb291))





# [4.0.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.11...@ohif/viewer@4.0.0) (2020-05-14)


### Bug Fixes

* 🐛 Fix race condition when loading derived display sets ([#1718](https://github.com/OHIF/Viewers/issues/1718)) ([b1678ce](https://github.com/OHIF/Viewers/commit/b1678ce6399dde37a9878f45ccc7c63286d93fab)), closes [#1715](https://github.com/OHIF/Viewers/issues/1715)


### BREAKING CHANGES

* 🧨 However we start to load once the first set of metadata arrives. We need
to wait until all series metadata is fetched.





## [3.11.11](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.10...@ohif/viewer@3.11.11) (2020-05-14)


### Bug Fixes

* 🐛 Load default display set when no time metadata ([#1684](https://github.com/OHIF/Viewers/issues/1684)) ([f7b8b6a](https://github.com/OHIF/Viewers/commit/f7b8b6a41c4626084ef56b0fdf7363e914b143c4)), closes [#1683](https://github.com/OHIF/Viewers/issues/1683)





## [3.11.10](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.9...@ohif/viewer@3.11.10) (2020-05-13)

**Note:** Version bump only for package @ohif/viewer





## [3.11.9](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.8...@ohif/viewer@3.11.9) (2020-05-12)


### Bug Fixes

* 🐛 Fix seg color load ([#1724](https://github.com/OHIF/Viewers/issues/1724)) ([c4f84b1](https://github.com/OHIF/Viewers/commit/c4f84b1174d04ba84d37ed89b6d7ab541be28181))





## [3.11.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.7...@ohif/viewer@3.11.8) (2020-05-06)

**Note:** Version bump only for package @ohif/viewer





## [3.11.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.6...@ohif/viewer@3.11.7) (2020-05-04)

**Note:** Version bump only for package @ohif/viewer





## [3.11.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.5...@ohif/viewer@3.11.6) (2020-05-04)


### Bug Fixes

* 🐛 Proper error handling for derived display sets ([#1708](https://github.com/OHIF/Viewers/issues/1708)) ([5b20d8f](https://github.com/OHIF/Viewers/commit/5b20d8f323e4b3ef9988f2f2ab672d697b6da409))





## [3.11.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.4...@ohif/viewer@3.11.5) (2020-05-04)

**Note:** Version bump only for package @ohif/viewer





## [3.11.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.3...@ohif/viewer@3.11.4) (2020-05-04)

**Note:** Version bump only for package @ohif/viewer





## [3.11.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.2...@ohif/viewer@3.11.3) (2020-04-29)


### Bug Fixes

* Add IHEInvokeImageDisplay routes back into viewer ([#1695](https://github.com/OHIF/Viewers/issues/1695)) ([f7162ce](https://github.com/OHIF/Viewers/commit/f7162ce61708776a6c192732b0904a022bcc6b3a))





## [3.11.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.1...@ohif/viewer@3.11.2) (2020-04-28)

**Note:** Version bump only for package @ohif/viewer





## [3.11.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.11.0...@ohif/viewer@3.11.1) (2020-04-27)

**Note:** Version bump only for package @ohif/viewer





# [3.11.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.10.2...@ohif/viewer@3.11.0) (2020-04-24)


### Features

* 🎸 Seg jump to slice + show/hide ([835f64d](https://github.com/OHIF/Viewers/commit/835f64d47a9994f6a25aaf3941a4974e215e7e7f))





## [3.10.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.10.1...@ohif/viewer@3.10.2) (2020-04-23)


### Bug Fixes

* undefined `errorHandler` in cornerstoneWadoImageLoader configuration ([#1664](https://github.com/OHIF/Viewers/issues/1664)) ([709f147](https://github.com/OHIF/Viewers/commit/709f14708e2b0f912b5ea509114acd87af3149cb))





## [3.10.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.10.0...@ohif/viewer@3.10.1) (2020-04-23)

**Note:** Version bump only for package @ohif/viewer





# [3.10.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.9.2...@ohif/viewer@3.10.0) (2020-04-23)


### Features

* configuration to hook into XHR Error handling ([e96205d](https://github.com/OHIF/Viewers/commit/e96205de35e5bec14dc8a9a8509db3dd4e6ecdb6))





## [3.9.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.9.1...@ohif/viewer@3.9.2) (2020-04-22)


### Bug Fixes

* whiteLabeling should support component creation by passing React to defined fn ([#1659](https://github.com/OHIF/Viewers/issues/1659)) ([2093a00](https://github.com/OHIF/Viewers/commit/2093a0036584b2cc698c8f06fe62b334523b1029))





## [3.9.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.9.0...@ohif/viewer@3.9.1) (2020-04-17)


### Bug Fixes

* `showStudyList` config ([#1647](https://github.com/OHIF/Viewers/issues/1647)) ([d9fc7bb](https://github.com/OHIF/Viewers/commit/d9fc7bbb0e6d868f507c515f031aaf88a2353e2f))





# [3.9.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.21...@ohif/viewer@3.9.0) (2020-04-17)


### Features

* set the authorization header for DICOMWeb requests if provided in query string ([#1646](https://github.com/OHIF/Viewers/issues/1646)) ([450c80b](https://github.com/OHIF/Viewers/commit/450c80b9d5f172be8b5713b422370360325a0afc))





## [3.8.21](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.20...@ohif/viewer@3.8.21) (2020-04-15)

**Note:** Version bump only for package @ohif/viewer





## [3.8.20](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.19...@ohif/viewer@3.8.20) (2020-04-09)


### Bug Fixes

* Revert "refactor: Reduce bundle size ([#1575](https://github.com/OHIF/Viewers/issues/1575))" ([#1622](https://github.com/OHIF/Viewers/issues/1622)) ([d21af3f](https://github.com/OHIF/Viewers/commit/d21af3f133492fa31492413b8782936c9ff18b44))





## [3.8.19](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.18...@ohif/viewer@3.8.19) (2020-04-09)

**Note:** Version bump only for package @ohif/viewer





# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.8.18](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.17...@ohif/viewer@3.8.18) (2020-04-07)

**Note:** Version bump only for package @ohif/viewer





## [3.8.17](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.16...@ohif/viewer@3.8.17) (2020-04-06)

**Note:** Version bump only for package @ohif/viewer





## [3.8.16](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.15...@ohif/viewer@3.8.16) (2020-04-02)

**Note:** Version bump only for package @ohif/viewer





## [3.8.15](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.14...@ohif/viewer@3.8.15) (2020-04-02)

**Note:** Version bump only for package @ohif/viewer





## [3.8.14](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.13...@ohif/viewer@3.8.14) (2020-04-02)

**Note:** Version bump only for package @ohif/viewer





## [3.8.13](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.12...@ohif/viewer@3.8.13) (2020-04-01)


### Bug Fixes

* segmentation not loading ([#1566](https://github.com/OHIF/Viewers/issues/1566)) ([4a7ce1c](https://github.com/OHIF/Viewers/commit/4a7ce1c09324d74c61048393e3a2427757e4001a))





## [3.8.12](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.11...@ohif/viewer@3.8.12) (2020-03-31)

**Note:** Version bump only for package @ohif/viewer





## [3.8.11](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.10...@ohif/viewer@3.8.11) (2020-03-26)


### Bug Fixes

* [#1312](https://github.com/OHIF/Viewers/issues/1312) Cine dialog remains on screen ([#1540](https://github.com/OHIF/Viewers/issues/1540)) ([7d22bb7](https://github.com/OHIF/Viewers/commit/7d22bb7d5a8590cffc169725c93942f758fe13a0))





## [3.8.10](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.9...@ohif/viewer@3.8.10) (2020-03-26)

**Note:** Version bump only for package @ohif/viewer





## [3.8.9](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.8...@ohif/viewer@3.8.9) (2020-03-25)


### Bug Fixes

* Load measurement in active viewport. ([#1558](https://github.com/OHIF/Viewers/issues/1558)) ([99022f2](https://github.com/OHIF/Viewers/commit/99022f2bac752f3cd1cedb61e222b8d411e158c8))





## [3.8.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.7...@ohif/viewer@3.8.8) (2020-03-25)

**Note:** Version bump only for package @ohif/viewer





## [3.8.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.6...@ohif/viewer@3.8.7) (2020-03-24)

**Note:** Version bump only for package @ohif/viewer





## [3.8.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.5...@ohif/viewer@3.8.6) (2020-03-24)

**Note:** Version bump only for package @ohif/viewer





## [3.8.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.4...@ohif/viewer@3.8.5) (2020-03-23)


### Bug Fixes

* avoid-wasteful-renders ([#1544](https://github.com/OHIF/Viewers/issues/1544)) ([e41d339](https://github.com/OHIF/Viewers/commit/e41d339f5faef6b93700bc860f37f29f32ad5ed6))





## [3.8.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.3...@ohif/viewer@3.8.4) (2020-03-19)


### Bug Fixes

* Only permit web workers to be initialized once. ([#1535](https://github.com/OHIF/Viewers/issues/1535)) ([9feadd3](https://github.com/OHIF/Viewers/commit/9feadd3c6d71c1c48f7825d024ccf95d5d82606d))





## [3.8.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.2...@ohif/viewer@3.8.3) (2020-03-17)

**Note:** Version bump only for package @ohif/viewer





## [3.8.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.1...@ohif/viewer@3.8.2) (2020-03-17)

**Note:** Version bump only for package @ohif/viewer





## [3.8.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.8.0...@ohif/viewer@3.8.1) (2020-03-17)


### Bug Fixes

* resolves [#1483](https://github.com/OHIF/Viewers/issues/1483) ([#1527](https://github.com/OHIF/Viewers/issues/1527)) ([2747eff](https://github.com/OHIF/Viewers/commit/2747effd9e893bd78b80ee7d0444f44676e9d632))





# [3.8.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.7.8...@ohif/viewer@3.8.0) (2020-03-13)


### Features

* Segmentations Settings UI - Phase 1 [#1391](https://github.com/OHIF/Viewers/issues/1391) ([#1392](https://github.com/OHIF/Viewers/issues/1392)) ([e8842cf](https://github.com/OHIF/Viewers/commit/e8842cf8aebde98db7fc123e4867c8288552331f)), closes [#1423](https://github.com/OHIF/Viewers/issues/1423)





# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.7.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.7.7...@ohif/viewer@3.7.8) (2020-03-09)

**Note:** Version bump only for package @ohif/viewer





## [3.7.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.7.6...@ohif/viewer@3.7.7) (2020-03-09)

**Note:** Version bump only for package @ohif/viewer





## [3.7.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.7.5...@ohif/viewer@3.7.6) (2020-03-06)

**Note:** Version bump only for package @ohif/viewer





## [3.7.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.7.4...@ohif/viewer@3.7.5) (2020-03-05)

**Note:** Version bump only for package @ohif/viewer





## [3.7.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.7.3...@ohif/viewer@3.7.4) (2020-03-03)

**Note:** Version bump only for package @ohif/viewer





## [3.7.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.7.2...@ohif/viewer@3.7.3) (2020-03-02)


### Bug Fixes

* GCloud dataset picker dialog broken ([#1453](https://github.com/OHIF/Viewers/issues/1453)) ([64dfbea](https://github.com/OHIF/Viewers/commit/64dfbeab7af98277efefadd334df14db79e32a4f))





## [3.7.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.7.1...@ohif/viewer@3.7.2) (2020-02-29)


### Bug Fixes

* prevent the native context menu from appearing when right-clicking on a measurement or angle (https://github.com/OHIF/Viewers/issues/1406) ([#1469](https://github.com/OHIF/Viewers/issues/1469)) ([9b3be9b](https://github.com/OHIF/Viewers/commit/9b3be9b0c082c9a5b62f2a40f42e59381860fe73))





## [3.7.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.7.0...@ohif/viewer@3.7.1) (2020-02-21)

**Note:** Version bump only for package @ohif/viewer





# [3.7.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.6.3...@ohif/viewer@3.7.0) (2020-02-20)


### Features

* [#1342](https://github.com/OHIF/Viewers/issues/1342) - Window level tab ([#1429](https://github.com/OHIF/Viewers/issues/1429)) ([ebc01a8](https://github.com/OHIF/Viewers/commit/ebc01a8ca238d5a3437b44d81f75aa8a5e8d0574))





## [3.6.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.6.2...@ohif/viewer@3.6.3) (2020-02-14)


### Bug Fixes

* Creating 2 commands to activate zoom tool and also to move between displaySets ([#1446](https://github.com/OHIF/Viewers/issues/1446)) ([06a4af0](https://github.com/OHIF/Viewers/commit/06a4af06faaecf6fa06ccd90cdfa879ee8d53053))





## [3.6.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.6.1...@ohif/viewer@3.6.2) (2020-02-12)


### Bug Fixes

* Combined Hotkeys for special characters ([#1233](https://github.com/OHIF/Viewers/issues/1233)) ([2f30e7a](https://github.com/OHIF/Viewers/commit/2f30e7a821a238144c49c56f37d8e5565540b4bd))





## [3.6.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.6.0...@ohif/viewer@3.6.1) (2020-02-10)

**Note:** Version bump only for package @ohif/viewer





# [3.6.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.5.1...@ohif/viewer@3.6.0) (2020-02-10)


### Features

* 🎸 MeasurementService ([#1314](https://github.com/OHIF/Viewers/issues/1314)) ([0c37a40](https://github.com/OHIF/Viewers/commit/0c37a406d963569af8c3be24c697dafd42712dfc))





## [3.5.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.5.0...@ohif/viewer@3.5.1) (2020-02-07)

**Note:** Version bump only for package @ohif/viewer





# [3.5.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.14...@ohif/viewer@3.5.0) (2020-02-06)


### Features

* lesion-tracker extension ([#1420](https://github.com/OHIF/Viewers/issues/1420)) ([73e4409](https://github.com/OHIF/Viewers/commit/73e440968ce4699d081a9c9f2d21dd68095b3056))





## [3.4.14](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.13...@ohif/viewer@3.4.14) (2020-02-06)

**Note:** Version bump only for package @ohif/viewer





## [3.4.13](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.12...@ohif/viewer@3.4.13) (2020-01-30)

**Note:** Version bump only for package @ohif/viewer





## [3.4.12](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.11...@ohif/viewer@3.4.12) (2020-01-30)

**Note:** Version bump only for package @ohif/viewer





## [3.4.11](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.10...@ohif/viewer@3.4.11) (2020-01-30)


### Bug Fixes

* download tool fixes & improvements ([#1235](https://github.com/OHIF/Viewers/issues/1235)) ([b9574b6](https://github.com/OHIF/Viewers/commit/b9574b6efcfeb85cde35b5cae63282f8e1b35be6))





## [3.4.10](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.9...@ohif/viewer@3.4.10) (2020-01-28)

**Note:** Version bump only for package @ohif/viewer





## [3.4.9](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.8...@ohif/viewer@3.4.9) (2020-01-28)

**Note:** Version bump only for package @ohif/viewer





## [3.4.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.7...@ohif/viewer@3.4.8) (2020-01-28)

**Note:** Version bump only for package @ohif/viewer





## [3.4.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.6...@ohif/viewer@3.4.7) (2020-01-28)

**Note:** Version bump only for package @ohif/viewer





## [3.4.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.5...@ohif/viewer@3.4.6) (2020-01-28)

**Note:** Version bump only for package @ohif/viewer





## [3.4.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.4...@ohif/viewer@3.4.5) (2020-01-27)

**Note:** Version bump only for package @ohif/viewer





## [3.4.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.3...@ohif/viewer@3.4.4) (2020-01-27)

**Note:** Version bump only for package @ohif/viewer





## [3.4.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.2...@ohif/viewer@3.4.3) (2020-01-24)

**Note:** Version bump only for package @ohif/viewer





## [3.4.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.1...@ohif/viewer@3.4.2) (2020-01-17)

**Note:** Version bump only for package @ohif/viewer





## [3.4.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.4.0...@ohif/viewer@3.4.1) (2020-01-15)


### Bug Fixes

* 🐛 Metadata is being mistakenly purged ([#1360](https://github.com/OHIF/Viewers/issues/1360)) ([b9a66d4](https://github.com/OHIF/Viewers/commit/b9a66d44241f2896ef184511287fb4984671e16d)), closes [#1326](https://github.com/OHIF/Viewers/issues/1326)





# [3.4.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.3.8...@ohif/viewer@3.4.0) (2020-01-14)


### Features

* Custom Healthcare API endpoint ([#1367](https://github.com/OHIF/Viewers/issues/1367)) ([a5d6bc6](https://github.com/OHIF/Viewers/commit/a5d6bc6a51784ed3a8a40d4ae773de9099f116b9))





## [3.3.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.3.7...@ohif/viewer@3.3.8) (2020-01-10)

**Note:** Version bump only for package @ohif/viewer





## [3.3.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.3.6...@ohif/viewer@3.3.7) (2020-01-08)

**Note:** Version bump only for package @ohif/viewer





## [3.3.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.3.5...@ohif/viewer@3.3.6) (2020-01-07)

**Note:** Version bump only for package @ohif/viewer





## [3.3.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.3.4...@ohif/viewer@3.3.5) (2020-01-06)

**Note:** Version bump only for package @ohif/viewer





## [3.3.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.3.3...@ohif/viewer@3.3.4) (2019-12-30)

**Note:** Version bump only for package @ohif/viewer





## [3.3.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.3.2...@ohif/viewer@3.3.3) (2019-12-20)


### Bug Fixes

* 🐛 1241: Make Plugin switch part of ToolbarModule ([#1322](https://github.com/OHIF/Viewers/issues/1322)) ([6540e36](https://github.com/OHIF/Viewers/commit/6540e36818944ac2eccc696186366ae495b33a04)), closes [#1241](https://github.com/OHIF/Viewers/issues/1241)





## [3.3.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.3.1...@ohif/viewer@3.3.2) (2019-12-20)

**Note:** Version bump only for package @ohif/viewer





## [3.3.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.3.0...@ohif/viewer@3.3.1) (2019-12-20)

**Note:** Version bump only for package @ohif/viewer





# [3.3.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.2.2...@ohif/viewer@3.3.0) (2019-12-20)


### Features

* 🎸 Configuration so viewer tools can nix handles ([#1304](https://github.com/OHIF/Viewers/issues/1304)) ([63594d3](https://github.com/OHIF/Viewers/commit/63594d36b0bdba59f0901095aed70b75fb05172d)), closes [#1223](https://github.com/OHIF/Viewers/issues/1223)





## [3.2.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.2.1...@ohif/viewer@3.2.2) (2019-12-19)


### Bug Fixes

* 🐛 Fix drag-n-drop of local files into OHIF ([#1319](https://github.com/OHIF/Viewers/issues/1319)) ([23305ce](https://github.com/OHIF/Viewers/commit/23305cec9c0f514e73a8dd17f984ffc87ad8d131)), closes [#1307](https://github.com/OHIF/Viewers/issues/1307)





## [3.2.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.2.0...@ohif/viewer@3.2.1) (2019-12-18)

**Note:** Version bump only for package @ohif/viewer





# [3.2.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.12...@ohif/viewer@3.2.0) (2019-12-16)


### Features

* 🎸 Expose extension config to modules ([#1279](https://github.com/OHIF/Viewers/issues/1279)) ([4ea239a](https://github.com/OHIF/Viewers/commit/4ea239a9535ef297e23387c186e537ab273744ea)), closes [#1268](https://github.com/OHIF/Viewers/issues/1268)





## [3.1.12](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.11...@ohif/viewer@3.1.12) (2019-12-16)


### Bug Fixes

* 🐛 Dismiss all dialogs if leaving viewer route [#1242](https://github.com/OHIF/Viewers/issues/1242) ([#1301](https://github.com/OHIF/Viewers/issues/1301)) ([5c3d8b3](https://github.com/OHIF/Viewers/commit/5c3d8b37b6f723fbd8edcc447c37984e7eee8d40))





## [3.1.11](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.10...@ohif/viewer@3.1.11) (2019-12-16)

**Note:** Version bump only for package @ohif/viewer





## [3.1.10](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.9...@ohif/viewer@3.1.10) (2019-12-16)

**Note:** Version bump only for package @ohif/viewer





## [3.1.9](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.8...@ohif/viewer@3.1.9) (2019-12-16)

**Note:** Version bump only for package @ohif/viewer





## [3.1.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.7...@ohif/viewer@3.1.8) (2019-12-16)

**Note:** Version bump only for package @ohif/viewer





## [3.1.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.6...@ohif/viewer@3.1.7) (2019-12-13)

**Note:** Version bump only for package @ohif/viewer





## [3.1.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.5...@ohif/viewer@3.1.6) (2019-12-13)

**Note:** Version bump only for package @ohif/viewer





## [3.1.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.4...@ohif/viewer@3.1.5) (2019-12-13)

**Note:** Version bump only for package @ohif/viewer





## [3.1.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.3...@ohif/viewer@3.1.4) (2019-12-13)

**Note:** Version bump only for package @ohif/viewer





## [3.1.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.2...@ohif/viewer@3.1.3) (2019-12-12)

**Note:** Version bump only for package @ohif/viewer





## [3.1.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.1...@ohif/viewer@3.1.2) (2019-12-12)

**Note:** Version bump only for package @ohif/viewer





## [3.1.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.1.0...@ohif/viewer@3.1.1) (2019-12-11)

**Note:** Version bump only for package @ohif/viewer





# [3.1.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.0.3...@ohif/viewer@3.1.0) (2019-12-11)


### Features

* 🎸 DICOM SR STOW on MeasurementAPI ([#954](https://github.com/OHIF/Viewers/issues/954)) ([ebe1af8](https://github.com/OHIF/Viewers/commit/ebe1af8d4f75d2483eba869655906d7829bd9666)), closes [#758](https://github.com/OHIF/Viewers/issues/758)





## [3.0.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.0.2...@ohif/viewer@3.0.3) (2019-12-11)

**Note:** Version bump only for package @ohif/viewer





## [3.0.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.0.1...@ohif/viewer@3.0.2) (2019-12-11)

**Note:** Version bump only for package @ohif/viewer





## [3.0.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@3.0.0...@ohif/viewer@3.0.1) (2019-12-09)

**Note:** Version bump only for package @ohif/viewer





# [3.0.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.11.8...@ohif/viewer@3.0.0) (2019-12-09)


* feat!: Ability to configure cornerstone tools via extension configuration (#1229) ([55a5806](https://github.com/OHIF/Viewers/commit/55a580659ecb74ca6433461d8f9a05c2a2b69533)), closes [#1229](https://github.com/OHIF/Viewers/issues/1229)


### BREAKING CHANGES

* modifies the exposed react <App /> components props. The contract for providing configuration for the app has changed. Please reference updated documentation for guidance.





## [2.11.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.11.7...@ohif/viewer@2.11.8) (2019-12-07)

**Note:** Version bump only for package @ohif/viewer





## [2.11.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.11.6...@ohif/viewer@2.11.7) (2019-12-07)

**Note:** Version bump only for package @ohif/viewer





## [2.11.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.11.5...@ohif/viewer@2.11.6) (2019-12-07)

**Note:** Version bump only for package @ohif/viewer





## [2.11.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.11.4...@ohif/viewer@2.11.5) (2019-12-06)

**Note:** Version bump only for package @ohif/viewer





## [2.11.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.11.3...@ohif/viewer@2.11.4) (2019-12-02)

**Note:** Version bump only for package @ohif/viewer





## [2.11.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.11.2...@ohif/viewer@2.11.3) (2019-12-02)

**Note:** Version bump only for package @ohif/viewer





## [2.11.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.11.1...@ohif/viewer@2.11.2) (2019-11-28)


### Bug Fixes

* User Preferences Issues ([#1207](https://github.com/OHIF/Viewers/issues/1207)) ([1df21a9](https://github.com/OHIF/Viewers/commit/1df21a9e075b5e6dfc10a429ae825826f46c71b8)), closes [#1161](https://github.com/OHIF/Viewers/issues/1161) [#1164](https://github.com/OHIF/Viewers/issues/1164) [#1177](https://github.com/OHIF/Viewers/issues/1177) [#1179](https://github.com/OHIF/Viewers/issues/1179) [#1180](https://github.com/OHIF/Viewers/issues/1180) [#1181](https://github.com/OHIF/Viewers/issues/1181) [#1182](https://github.com/OHIF/Viewers/issues/1182) [#1183](https://github.com/OHIF/Viewers/issues/1183) [#1184](https://github.com/OHIF/Viewers/issues/1184) [#1185](https://github.com/OHIF/Viewers/issues/1185)





## [2.11.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.11.0...@ohif/viewer@2.11.1) (2019-11-27)


### Bug Fixes

* of undefined name of project ([#1231](https://github.com/OHIF/Viewers/issues/1231)) ([e34a057](https://github.com/OHIF/Viewers/commit/e34a05726319e3e70279c43d5bf976d33cdf71f7))





# [2.11.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.10.2...@ohif/viewer@2.11.0) (2019-11-25)


### Features

* Add new annotate tool using new dialog service ([#1211](https://github.com/OHIF/Viewers/issues/1211)) ([8fd3af1](https://github.com/OHIF/Viewers/commit/8fd3af1e137e793f1b482760a22591c64a072047))





## [2.10.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.10.1...@ohif/viewer@2.10.2) (2019-11-25)


### Bug Fixes

* Issue branch from danny experimental changes pr 1128 ([#1150](https://github.com/OHIF/Viewers/issues/1150)) ([a870b3c](https://github.com/OHIF/Viewers/commit/a870b3cc6056cf824af422e46f1ad674910b534e)), closes [#1161](https://github.com/OHIF/Viewers/issues/1161) [#1164](https://github.com/OHIF/Viewers/issues/1164) [#1177](https://github.com/OHIF/Viewers/issues/1177) [#1179](https://github.com/OHIF/Viewers/issues/1179) [#1180](https://github.com/OHIF/Viewers/issues/1180) [#1181](https://github.com/OHIF/Viewers/issues/1181) [#1182](https://github.com/OHIF/Viewers/issues/1182) [#1183](https://github.com/OHIF/Viewers/issues/1183) [#1184](https://github.com/OHIF/Viewers/issues/1184) [#1185](https://github.com/OHIF/Viewers/issues/1185)





## [2.10.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.10.0...@ohif/viewer@2.10.1) (2019-11-20)

**Note:** Version bump only for package @ohif/viewer





# [2.10.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.9.0...@ohif/viewer@2.10.0) (2019-11-19)


### Features

* New dialog service ([#1202](https://github.com/OHIF/Viewers/issues/1202)) ([f65639c](https://github.com/OHIF/Viewers/commit/f65639c2b0dab01decd20cab2cef4263cb4fab37))





# [2.9.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.8.5...@ohif/viewer@2.9.0) (2019-11-19)


### Features

* Issue 879 viewer route query param not filtering but promoting ([#1141](https://github.com/OHIF/Viewers/issues/1141)) ([b17f753](https://github.com/OHIF/Viewers/commit/b17f753e6222045252ef885e40233681541a32e1)), closes [#1118](https://github.com/OHIF/Viewers/issues/1118)





## [2.8.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.8.4...@ohif/viewer@2.8.5) (2019-11-18)


### Bug Fixes

* minor date picker UX improvements ([813ee5e](https://github.com/OHIF/Viewers/commit/813ee5ed4d78b7bda234922d5f3389efe346451c))





## [2.8.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.8.3...@ohif/viewer@2.8.4) (2019-11-15)

**Note:** Version bump only for package @ohif/viewer





## [2.8.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.8.2...@ohif/viewer@2.8.3) (2019-11-15)

**Note:** Version bump only for package @ohif/viewer





## [2.8.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.8.1...@ohif/viewer@2.8.2) (2019-11-14)

**Note:** Version bump only for package @ohif/viewer

## [2.8.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.8.0...@ohif/viewer@2.8.1) (2019-11-14)

**Note:** Version bump only for package @ohif/viewer

# [2.8.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.7.1...@ohif/viewer@2.8.0) (2019-11-13)

### Features

- expose UiNotifications service
  ([#1172](https://github.com/OHIF/Viewers/issues/1172))
  ([5c04e34](https://github.com/OHIF/Viewers/commit/5c04e34c8fb2394ab7acd9eb4f2ab12afeb2f255))
- filter field for google api windows
  ([#1170](https://github.com/OHIF/Viewers/issues/1170))
  ([c59c5b3](https://github.com/OHIF/Viewers/commit/c59c5b3f14d44f1c06aa396125a1f4caaa431c25))

## [2.7.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.7.0...@ohif/viewer@2.7.1) (2019-11-12)

### Bug Fixes

- 🐛 Fix for JS breaking on header
  ([#1164](https://github.com/OHIF/Viewers/issues/1164))
  ([0fbaf95](https://github.com/OHIF/Viewers/commit/0fbaf95971dc0b3a671e1f586a876d9019e860ed))

# [2.7.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.6.4...@ohif/viewer@2.7.0) (2019-11-12)

### Features

- 🎸 Update hotkeys and user preferences modal
  ([#1135](https://github.com/OHIF/Viewers/issues/1135))
  ([e62f5f8](https://github.com/OHIF/Viewers/commit/e62f5f8dd28ab363f23671cd21cee115abb870ff)),
  closes [#923](https://github.com/OHIF/Viewers/issues/923)

## [2.6.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.6.3...@ohif/viewer@2.6.4) (2019-11-11)

**Note:** Version bump only for package @ohif/viewer

## [2.6.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.6.2...@ohif/viewer@2.6.3) (2019-11-08)

**Note:** Version bump only for package @ohif/viewer

## [2.6.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.6.1...@ohif/viewer@2.6.2) (2019-11-08)

**Note:** Version bump only for package @ohif/viewer

## [2.6.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.6.0...@ohif/viewer@2.6.1) (2019-11-06)

**Note:** Version bump only for package @ohif/viewer

# [2.6.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.5.0...@ohif/viewer@2.6.0) (2019-11-06)

### Features

- modal provider ([#1151](https://github.com/OHIF/Viewers/issues/1151))
  ([75d88bc](https://github.com/OHIF/Viewers/commit/75d88bc454710d2dcdbc7d68c4d9df041159c840)),
  closes [#1086](https://github.com/OHIF/Viewers/issues/1086)
  [#1116](https://github.com/OHIF/Viewers/issues/1116)
  [#1116](https://github.com/OHIF/Viewers/issues/1116)
  [#1146](https://github.com/OHIF/Viewers/issues/1146)
  [#1142](https://github.com/OHIF/Viewers/issues/1142)
  [#1143](https://github.com/OHIF/Viewers/issues/1143)
  [#1110](https://github.com/OHIF/Viewers/issues/1110)
  [#1086](https://github.com/OHIF/Viewers/issues/1086)
  [#1116](https://github.com/OHIF/Viewers/issues/1116)
  [#1119](https://github.com/OHIF/Viewers/issues/1119)

# [2.5.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.4.1...@ohif/viewer@2.5.0) (2019-11-05)

### Features

- 🎸 Filter by url query param for seriesInstnaceUID
  ([#1117](https://github.com/OHIF/Viewers/issues/1117))
  ([e208f2e](https://github.com/OHIF/Viewers/commit/e208f2e6a9c49b16dadead0a917f657cf023929a)),
  closes [#1118](https://github.com/OHIF/Viewers/issues/1118)

## [2.4.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.4.0...@ohif/viewer@2.4.1) (2019-11-05)

### Bug Fixes

- [#1075](https://github.com/OHIF/Viewers/issues/1075) Returning to the Study
  List before all series have finishe…
  ([#1090](https://github.com/OHIF/Viewers/issues/1090))
  ([ecaf578](https://github.com/OHIF/Viewers/commit/ecaf578f92dc40294cec7ff9b272fb432dec4125))

# [2.4.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.3.8...@ohif/viewer@2.4.0) (2019-11-04)

### Features

- 🎸 New modal provider ([#1110](https://github.com/OHIF/Viewers/issues/1110))
  ([5ee832b](https://github.com/OHIF/Viewers/commit/5ee832b19505a4e8e5756660ce6ed03a7f18dec3)),
  closes [#1086](https://github.com/OHIF/Viewers/issues/1086)
  [#1116](https://github.com/OHIF/Viewers/issues/1116)

## [2.3.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.3.7...@ohif/viewer@2.3.8) (2019-11-04)

**Note:** Version bump only for package @ohif/viewer

## [2.3.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.3.6...@ohif/viewer@2.3.7) (2019-11-04)

### Bug Fixes

- 🐛 Minor issues measurement panel related to description
  ([#1142](https://github.com/OHIF/Viewers/issues/1142))
  ([681384b](https://github.com/OHIF/Viewers/commit/681384b7425c83b02a0ed83371ca92d78ca7838c))

## [2.3.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.3.5...@ohif/viewer@2.3.6) (2019-11-02)

**Note:** Version bump only for package @ohif/viewer

## [2.3.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.3.4...@ohif/viewer@2.3.5) (2019-10-31)

### Bug Fixes

- application crash if patientName is an object
  ([#1138](https://github.com/OHIF/Viewers/issues/1138))
  ([64cf3b3](https://github.com/OHIF/Viewers/commit/64cf3b324da2383a927af1df2d46db2fca5318aa))

## [2.3.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.3.3...@ohif/viewer@2.3.4) (2019-10-30)

### Bug Fixes

- 🐛 Fix ghost shadow on thumb
  ([#1113](https://github.com/OHIF/Viewers/issues/1113))
  ([caaa032](https://github.com/OHIF/Viewers/commit/caaa032c4bc24fd69fdb01a15a8feb2721c321db))

## [2.3.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.3.2...@ohif/viewer@2.3.3) (2019-10-30)

### Bug Fixes

- get adapter store picker to show
  ([#1134](https://github.com/OHIF/Viewers/issues/1134))
  ([50ca2bd](https://github.com/OHIF/Viewers/commit/50ca2bde971e1e67b73ece96369052dd1a35ac68))

## [2.3.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.3.1...@ohif/viewer@2.3.2) (2019-10-29)

### Bug Fixes

- 🐛 Limit image download size to avoid browser issues
  ([#1112](https://github.com/OHIF/Viewers/issues/1112))
  ([5716b71](https://github.com/OHIF/Viewers/commit/5716b71d409ee1c6f13393c8cb7f50222415e198)),
  closes [#1099](https://github.com/OHIF/Viewers/issues/1099)

## [2.3.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.3.0...@ohif/viewer@2.3.1) (2019-10-29)

### Bug Fixes

- rollbar template needs PUBLIC_URL defined
  ([#1127](https://github.com/OHIF/Viewers/issues/1127))
  ([352407c](https://github.com/OHIF/Viewers/commit/352407c71ae93946e9ebad41446d6086cfbc237b))

# [2.3.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.2.2...@ohif/viewer@2.3.0) (2019-10-29)

### Features

- service worker ([#1045](https://github.com/OHIF/Viewers/issues/1045))
  ([cf51368](https://github.com/OHIF/Viewers/commit/cf5136899eac08300ec4f15474a6440129ef7a9a))

## [2.2.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.2.1...@ohif/viewer@2.2.2) (2019-10-29)

### Bug Fixes

- Set SR viewport as active by interaction
  ([#1118](https://github.com/OHIF/Viewers/issues/1118))
  ([5b33417](https://github.com/OHIF/Viewers/commit/5b334175c370afb930b4b6dbd307ddece8f850e3))

## [2.2.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.2.0...@ohif/viewer@2.2.1) (2019-10-29)

**Note:** Version bump only for package @ohif/viewer

# [2.2.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.1.4...@ohif/viewer@2.2.0) (2019-10-28)

### Features

- responsive study list ([#1068](https://github.com/OHIF/Viewers/issues/1068))
  ([2cdef4b](https://github.com/OHIF/Viewers/commit/2cdef4b9844cc2ce61e9ce76b5a942ba7051fe16))

## [2.1.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.1.3...@ohif/viewer@2.1.4) (2019-10-28)

**Note:** Version bump only for package @ohif/viewer

## [2.1.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.1.2...@ohif/viewer@2.1.3) (2019-10-26)

**Note:** Version bump only for package @ohif/viewer

## [2.1.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.1.1...@ohif/viewer@2.1.2) (2019-10-26)

### Bug Fixes

- update script-tag output to include config from default.js
  ([c522ff3](https://github.com/OHIF/Viewers/commit/c522ff3ddab7ed8e3a128dd6edd2cd6902226e99))

## [2.1.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.1.0...@ohif/viewer@2.1.1) (2019-10-26)

### Bug Fixes

- 🐛 JSON launch not working properly
  ([#1089](https://github.com/OHIF/Viewers/issues/1089))
  ([#1093](https://github.com/OHIF/Viewers/issues/1093))
  ([2677170](https://github.com/OHIF/Viewers/commit/2677170d67659ee178cf77307414d54cfe9cb563))

# [2.1.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@2.0.0...@ohif/viewer@2.1.0) (2019-10-26)

### Features

- Snapshot Download Tool ([#840](https://github.com/OHIF/Viewers/issues/840))
  ([450e098](https://github.com/OHIF/Viewers/commit/450e0981a5ba054fcfcb85eeaeb18371af9088f8))

# [2.0.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.12.2...@ohif/viewer@2.0.0) (2019-10-26)

### Bug Fixes

- 🐛 Desc of meas.table not being updated on properly
  ([#1094](https://github.com/OHIF/Viewers/issues/1094))
  ([85f836c](https://github.com/OHIF/Viewers/commit/85f836cd918614be722fce1bff2373460ec4900b)),
  closes [#1013](https://github.com/OHIF/Viewers/issues/1013)

### BREAKING CHANGES

- 1013

## [1.12.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.12.1...@ohif/viewer@1.12.2) (2019-10-25)

### Bug Fixes

- set SR in ActiveViewport by clicking thumb
  ([#1091](https://github.com/OHIF/Viewers/issues/1091))
  ([986b7ae](https://github.com/OHIF/Viewers/commit/986b7ae2bf4f7d27f326e62f93285ce20eaf0a79))

## [1.12.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.12.0...@ohif/viewer@1.12.1) (2019-10-25)

### Bug Fixes

- 🐛 Orthographic MPR fix ([#1092](https://github.com/OHIF/Viewers/issues/1092))
  ([460e375](https://github.com/OHIF/Viewers/commit/460e375f0aa75d35f7a46b4d48e6cc706019956d))

# [1.12.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.11.5...@ohif/viewer@1.12.0) (2019-10-25)

### Features

- 🎸 Allow routes to load Google Cloud DICOM Stores in the Study List
  ([#1069](https://github.com/OHIF/Viewers/issues/1069))
  ([21b586b](https://github.com/OHIF/Viewers/commit/21b586b08f3dde6613859712a9e0577dece564db))

## [1.11.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.11.4...@ohif/viewer@1.11.5) (2019-10-24)

**Note:** Version bump only for package @ohif/viewer

## [1.11.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.11.3...@ohif/viewer@1.11.4) (2019-10-23)

### Bug Fixes

- Revert "Revert "fix: MPR initialization""
  ([#1065](https://github.com/OHIF/Viewers/issues/1065))
  ([c680720](https://github.com/OHIF/Viewers/commit/c680720ce5ead58fdb399e3a356edac18093f5c0)),
  closes [#1062](https://github.com/OHIF/Viewers/issues/1062)
  [#1064](https://github.com/OHIF/Viewers/issues/1064)

## [1.11.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.11.2...@ohif/viewer@1.11.3) (2019-10-23)

### Bug Fixes

- 🐛 Switch to orhtographic view for 2D MPR
  ([#1074](https://github.com/OHIF/Viewers/issues/1074))
  ([13d337a](https://github.com/OHIF/Viewers/commit/13d337aaabb8dadf6366c6262c5e47e7781edd08))

## [1.11.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.11.1...@ohif/viewer@1.11.2) (2019-10-23)

**Note:** Version bump only for package @ohif/viewer

## [1.11.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.11.0...@ohif/viewer@1.11.1) (2019-10-23)

**Note:** Version bump only for package @ohif/viewer

# [1.11.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.10.3...@ohif/viewer@1.11.0) (2019-10-22)

### Bug Fixes

- MPR initialization ([#1062](https://github.com/OHIF/Viewers/issues/1062))
  ([b037394](https://github.com/OHIF/Viewers/commit/b03739428f72bb50bdabdd6f83b7af885057da69))

### Features

- 🎸 Load spinner when selecting gcloud store. Add key on td
  ([#1034](https://github.com/OHIF/Viewers/issues/1034))
  ([e62f403](https://github.com/OHIF/Viewers/commit/e62f403fe9e3df56713128e3d59045824b086d8d)),
  closes [#1057](https://github.com/OHIF/Viewers/issues/1057)

## [1.10.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.10.2...@ohif/viewer@1.10.3) (2019-10-18)

**Note:** Version bump only for package @ohif/viewer

## [1.10.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.10.1...@ohif/viewer@1.10.2) (2019-10-18)

**Note:** Version bump only for package @ohif/viewer

## [1.10.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.10.0...@ohif/viewer@1.10.1) (2019-10-16)

**Note:** Version bump only for package @ohif/viewer

# [1.10.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.9.1...@ohif/viewer@1.10.0) (2019-10-15)

### Features

- Add browser info and app version
  ([#1046](https://github.com/OHIF/Viewers/issues/1046))
  ([c217b8b](https://github.com/OHIF/Viewers/commit/c217b8b))

## [1.9.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.9.0...@ohif/viewer@1.9.1) (2019-10-15)

### Bug Fixes

- 🐛 Remove debugger statement left in from last PR
  ([#1052](https://github.com/OHIF/Viewers/issues/1052))
  ([d091cd6](https://github.com/OHIF/Viewers/commit/d091cd6))

# [1.9.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.8.0...@ohif/viewer@1.9.0) (2019-10-15)

### Features

- 🎸 Only allow reconstruction of datasets that make sense
  ([#1010](https://github.com/OHIF/Viewers/issues/1010))
  ([2d75e01](https://github.com/OHIF/Viewers/commit/2d75e01)), closes
  [#561](https://github.com/OHIF/Viewers/issues/561)

# [1.8.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.7.0...@ohif/viewer@1.8.0) (2019-10-14)

### Features

- Notification Service ([#1011](https://github.com/OHIF/Viewers/issues/1011))
  ([92c8996](https://github.com/OHIF/Viewers/commit/92c8996))

# [1.7.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.6.3...@ohif/viewer@1.7.0) (2019-10-14)

### Features

- Implement a 'Exit 2D MPR' button in the toolbar
  ([c99e0d8](https://github.com/OHIF/Viewers/commit/c99e0d8))

## [1.6.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.6.2...@ohif/viewer@1.6.3) (2019-10-14)

**Note:** Version bump only for package @ohif/viewer

## [1.6.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.6.1...@ohif/viewer@1.6.2) (2019-10-11)

**Note:** Version bump only for package @ohif/viewer

## [1.6.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.6.0...@ohif/viewer@1.6.1) (2019-10-11)

### Bug Fixes

- Switch token storage back to localStorage because in-memory was annoying for
  end users ([#1030](https://github.com/OHIF/Viewers/issues/1030))
  ([412fe4e](https://github.com/OHIF/Viewers/commit/412fe4e))

# [1.6.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.5.4...@ohif/viewer@1.6.0) (2019-10-11)

### Features

- 🎸 Improve usability of Google Cloud adapter, including direct routes to
  studies ([#989](https://github.com/OHIF/Viewers/issues/989))
  ([2bc361c](https://github.com/OHIF/Viewers/commit/2bc361c))

## [1.5.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.5.3...@ohif/viewer@1.5.4) (2019-10-10)

**Note:** Version bump only for package @ohif/viewer

## [1.5.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.5.2...@ohif/viewer@1.5.3) (2019-10-10)

**Note:** Version bump only for package @ohif/viewer

## [1.5.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.5.1...@ohif/viewer@1.5.2) (2019-10-10)

### Bug Fixes

- 🎸 switch ohif logo from text + font to SVG
  ([#1021](https://github.com/OHIF/Viewers/issues/1021))
  ([e7de8be](https://github.com/OHIF/Viewers/commit/e7de8be))

## [1.5.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.5.0...@ohif/viewer@1.5.1) (2019-10-09)

### Bug Fixes

- 🐛 set current viewport as active when switching layouts
  ([#1018](https://github.com/OHIF/Viewers/issues/1018))
  ([2a74355](https://github.com/OHIF/Viewers/commit/2a74355))

# [1.5.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.4.5...@ohif/viewer@1.5.0) (2019-10-09)

### Features

- Multiple fixes and implementation changes to react-cornerstone-viewport
  ([1cc94f3](https://github.com/OHIF/Viewers/commit/1cc94f3))

## [1.4.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.4.4...@ohif/viewer@1.4.5) (2019-10-09)

**Note:** Version bump only for package @ohif/viewer

## [1.4.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.4.3...@ohif/viewer@1.4.4) (2019-10-07)

**Note:** Version bump only for package @ohif/viewer

## [1.4.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.4.2...@ohif/viewer@1.4.3) (2019-10-04)

**Note:** Version bump only for package @ohif/viewer

## [1.4.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.4.1...@ohif/viewer@1.4.2) (2019-10-04)

**Note:** Version bump only for package @ohif/viewer

## [1.4.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.4.0...@ohif/viewer@1.4.1) (2019-10-03)

**Note:** Version bump only for package @ohif/viewer

# [1.4.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.3.3...@ohif/viewer@1.4.0) (2019-10-03)

### Features

- Use QIDO + WADO to load series metadata individually rather than the entire
  study metadata at once ([#953](https://github.com/OHIF/Viewers/issues/953))
  ([9e10c2b](https://github.com/OHIF/Viewers/commit/9e10c2b))

## [1.3.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.3.2...@ohif/viewer@1.3.3) (2019-10-02)

**Note:** Version bump only for package @ohif/viewer

## [1.3.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.3.1...@ohif/viewer@1.3.2) (2019-10-01)

**Note:** Version bump only for package @ohif/viewer

## [1.3.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.3.0...@ohif/viewer@1.3.1) (2019-10-01)

### Bug Fixes

- Exit MPR mode if Layout is changed
  ([#984](https://github.com/OHIF/Viewers/issues/984))
  ([674ca9f](https://github.com/OHIF/Viewers/commit/674ca9f))

# [1.3.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.2.8...@ohif/viewer@1.3.0) (2019-10-01)

### Features

- 🎸 MPR UI improvements. Added MinIP, AvgIP, slab thickness slider and mode
  toggle ([#947](https://github.com/OHIF/Viewers/issues/947))
  ([c79c0c3](https://github.com/OHIF/Viewers/commit/c79c0c3))

## [1.2.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.2.7...@ohif/viewer@1.2.8) (2019-10-01)

**Note:** Version bump only for package @ohif/viewer

## [1.2.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.2.6...@ohif/viewer@1.2.7) (2019-10-01)

**Note:** Version bump only for package @ohif/viewer

## [1.2.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.2.5...@ohif/viewer@1.2.6) (2019-09-27)

**Note:** Version bump only for package @ohif/viewer

## [1.2.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.2.4...@ohif/viewer@1.2.5) (2019-09-27)

### Bug Fixes

- version bump issue ([#963](https://github.com/OHIF/Viewers/issues/963))
  ([e607ed2](https://github.com/OHIF/Viewers/commit/e607ed2)), closes
  [#962](https://github.com/OHIF/Viewers/issues/962)

# [2.0.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.2.3...@ohif/viewer@2.0.0) (2019-09-27)

### Bug Fixes

- 🐛 Add DicomLoaderService & FileLoaderService to fix SR, PDF, and SEG support
  in local file and WADO-RS-only use cases
  ([#862](https://github.com/OHIF/Viewers/issues/862))
  ([e7e1a8a](https://github.com/OHIF/Viewers/commit/e7e1a8a)), closes
  [#838](https://github.com/OHIF/Viewers/issues/838)
- version bump issue ([#962](https://github.com/OHIF/Viewers/issues/962))
  ([c80ea17](https://github.com/OHIF/Viewers/commit/c80ea17))

### BREAKING CHANGES

- DICOM Seg

# [2.0.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.2.3...@ohif/viewer@2.0.0) (2019-09-27)

### Bug Fixes

- 🐛 Add DicomLoaderService & FileLoaderService to fix SR, PDF, and SEG support
  in local file and WADO-RS-only use cases
  ([#862](https://github.com/OHIF/Viewers/issues/862))
  ([e7e1a8a](https://github.com/OHIF/Viewers/commit/e7e1a8a)), closes
  [#838](https://github.com/OHIF/Viewers/issues/838)

### BREAKING CHANGES

- DICOM Seg

## [1.2.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.2.2...@ohif/viewer@1.2.3) (2019-09-27)

**Note:** Version bump only for package @ohif/viewer

## [1.2.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.2.1...@ohif/viewer@1.2.2) (2019-09-27)

**Note:** Version bump only for package @ohif/viewer

## [1.2.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.2.0...@ohif/viewer@1.2.1) (2019-09-26)

### Bug Fixes

- google cloud support w/ docker (via env var)
  ([#958](https://github.com/OHIF/Viewers/issues/958))
  ([e375a4a](https://github.com/OHIF/Viewers/commit/e375a4a))

# [1.2.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.14...@ohif/viewer@1.2.0) (2019-09-26)

### Features

- 🎸 React custom component on toolbar button
  ([#935](https://github.com/OHIF/Viewers/issues/935))
  ([a90605c](https://github.com/OHIF/Viewers/commit/a90605c))

## [1.1.14](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.13...@ohif/viewer@1.1.14) (2019-09-26)

### Bug Fixes

- 🐛 Set series into active viewport by clicking on thumbnail
  ([#945](https://github.com/OHIF/Viewers/issues/945))
  ([5551f81](https://github.com/OHIF/Viewers/commit/5551f81)), closes
  [#895](https://github.com/OHIF/Viewers/issues/895)
  [#895](https://github.com/OHIF/Viewers/issues/895)

## [1.1.13](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.12...@ohif/viewer@1.1.13) (2019-09-26)

### Bug Fixes

- Add some code splitting for PWA build
  ([#937](https://github.com/OHIF/Viewers/issues/937))
  ([8938035](https://github.com/OHIF/Viewers/commit/8938035))

## [1.1.12](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.11...@ohif/viewer@1.1.12) (2019-09-26)

**Note:** Version bump only for package @ohif/viewer

## [1.1.11](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.10...@ohif/viewer@1.1.11) (2019-09-23)

**Note:** Version bump only for package @ohif/viewer

## [1.1.10](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.9...@ohif/viewer@1.1.10) (2019-09-19)

**Note:** Version bump only for package @ohif/viewer

## [1.1.9](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.8...@ohif/viewer@1.1.9) (2019-09-19)

**Note:** Version bump only for package @ohif/viewer

## [1.1.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.7...@ohif/viewer@1.1.8) (2019-09-19)

**Note:** Version bump only for package @ohif/viewer

## [1.1.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.6...@ohif/viewer@1.1.7) (2019-09-19)

**Note:** Version bump only for package @ohif/viewer

## [1.1.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.5...@ohif/viewer@1.1.6) (2019-09-19)

**Note:** Version bump only for package @ohif/viewer

## [1.1.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.4...@ohif/viewer@1.1.5) (2019-09-17)

### Bug Fixes

- bump cornerstone-tools to latest version
  ([f519f86](https://github.com/OHIF/Viewers/commit/f519f86))

## [1.1.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.3...@ohif/viewer@1.1.4) (2019-09-17)

**Note:** Version bump only for package @ohif/viewer

## [1.1.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.2...@ohif/viewer@1.1.3) (2019-09-16)

### Bug Fixes

- 🐛 Fix issue on not loading gcloud
  ([#919](https://github.com/OHIF/Viewers/issues/919))
  ([f723546](https://github.com/OHIF/Viewers/commit/f723546))

## [1.1.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.1...@ohif/viewer@1.1.2) (2019-09-12)

**Note:** Version bump only for package @ohif/viewer

## [1.1.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.1.0...@ohif/viewer@1.1.1) (2019-09-12)

**Note:** Version bump only for package @ohif/viewer

# [1.1.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.0.5...@ohif/viewer@1.1.0) (2019-09-12)

### Features

- 🎸 Load local file or folder using native dialog
  ([#870](https://github.com/OHIF/Viewers/issues/870))
  ([c221dd8](https://github.com/OHIF/Viewers/commit/c221dd8))

## [1.0.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.0.4...@ohif/viewer@1.0.5) (2019-09-10)

**Note:** Version bump only for package @ohif/viewer

## [1.0.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.0.3...@ohif/viewer@1.0.4) (2019-09-10)

**Note:** Version bump only for package @ohif/viewer

## [1.0.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.0.2...@ohif/viewer@1.0.3) (2019-09-10)

### Bug Fixes

- on-brand library global name
  ([ababe63](https://github.com/OHIF/Viewers/commit/ababe63))
- remove requestOptions when key is not needed
  ([32bc47d](https://github.com/OHIF/Viewers/commit/32bc47d))

## [1.0.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.0.1...@ohif/viewer@1.0.2) (2019-09-09)

### Bug Fixes

- import regenerator-runtime for umd build
  ([bad987a](https://github.com/OHIF/Viewers/commit/bad987a))

## [1.0.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@1.0.0...@ohif/viewer@1.0.1) (2019-09-09)

**Note:** Version bump only for package @ohif/viewer

# [1.0.0](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.21...@ohif/viewer@1.0.0) (2019-09-06)

### Code Refactoring

- 💡 React components to consume appConfig using Context
  ([#852](https://github.com/OHIF/Viewers/issues/852))
  ([7c4ee73](https://github.com/OHIF/Viewers/commit/7c4ee73)), closes
  [#725](https://github.com/OHIF/Viewers/issues/725)
  [#725](https://github.com/OHIF/Viewers/issues/725)

### BREAKING CHANGES

- #725

## [0.50.21](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.20...@ohif/viewer@0.50.21) (2019-09-06)

### Bug Fixes

- viewer project should build output before publish
  ([94b625d](https://github.com/OHIF/Viewers/commit/94b625d))

## [0.50.20](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.19...@ohif/viewer@0.50.20) (2019-09-06)

**Note:** Version bump only for package @ohif/viewer

## [0.50.19](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.18...@ohif/viewer@0.50.19) (2019-09-06)

### Bug Fixes

- @ohif/viewer package build
  ([4aa7cbd](https://github.com/OHIF/Viewers/commit/4aa7cbd))

## [0.50.18](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.17...@ohif/viewer@0.50.18) (2019-09-05)

**Note:** Version bump only for package @ohif/viewer

## [0.50.17](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.16...@ohif/viewer@0.50.17) (2019-09-04)

**Note:** Version bump only for package @ohif/viewer

## [0.50.16](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.15...@ohif/viewer@0.50.16) (2019-09-04)

**Note:** Version bump only for package @ohif/viewer

## [0.50.15](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.14...@ohif/viewer@0.50.15) (2019-09-04)

### Bug Fixes

- measurementsAPI issue caused by production build
  ([#842](https://github.com/OHIF/Viewers/issues/842))
  ([49d3439](https://github.com/OHIF/Viewers/commit/49d3439))

## [0.50.14](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.13...@ohif/viewer@0.50.14) (2019-09-03)

**Note:** Version bump only for package @ohif/viewer

## [0.50.13](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.12...@ohif/viewer@0.50.13) (2019-09-03)

### Bug Fixes

- 🐛 Activating Pan and Zoom on right and middle click by def
  ([#841](https://github.com/OHIF/Viewers/issues/841))
  ([7a9b477](https://github.com/OHIF/Viewers/commit/7a9b477))

## [0.50.12](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.11...@ohif/viewer@0.50.12) (2019-08-29)

### Bug Fixes

- asset resolution when at non-root route
  ([#828](https://github.com/OHIF/Viewers/issues/828))
  ([d48b617](https://github.com/OHIF/Viewers/commit/d48b617))

## [0.50.11](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.10...@ohif/viewer@0.50.11) (2019-08-29)

**Note:** Version bump only for package @ohif/viewer

## [0.50.10](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.9...@ohif/viewer@0.50.10) (2019-08-27)

**Note:** Version bump only for package @ohif/viewer

## [0.50.9](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.8...@ohif/viewer@0.50.9) (2019-08-27)

**Note:** Version bump only for package @ohif/viewer

## [0.50.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.7...@ohif/viewer@0.50.8) (2019-08-26)

**Note:** Version bump only for package @ohif/viewer

## [0.50.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.6...@ohif/viewer@0.50.7) (2019-08-22)

### Bug Fixes

- 🐛 Update for changes in ExpandableToolMenu props
  ([e09670a](https://github.com/OHIF/Viewers/commit/e09670a))

## [0.50.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.5...@ohif/viewer@0.50.6) (2019-08-22)

**Note:** Version bump only for package @ohif/viewer

## [0.50.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.4...@ohif/viewer@0.50.5) (2019-08-21)

### Bug Fixes

- **StandaloneRouting:** Promise rejection - added `return`
  ([#791](https://github.com/OHIF/Viewers/issues/791))
  ([d09fb4e](https://github.com/OHIF/Viewers/commit/d09fb4e))

## [0.50.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.3...@ohif/viewer@0.50.4) (2019-08-20)

**Note:** Version bump only for package @ohif/viewer

## [0.50.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.2...@ohif/viewer@0.50.3) (2019-08-15)

**Note:** Version bump only for package @ohif/viewer

## [0.50.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.1...@ohif/viewer@0.50.2) (2019-08-15)

**Note:** Version bump only for package @ohif/viewer

## [0.50.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.0-alpha.13...@ohif/viewer@0.50.1) (2019-08-14)

**Note:** Version bump only for package @ohif/viewer

# [0.50.0-alpha.13](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.0-alpha.12...@ohif/viewer@0.50.0-alpha.13) (2019-08-14)

**Note:** Version bump only for package @ohif/viewer

# [0.50.0-alpha.12](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.50.0-alpha.11...@ohif/viewer@0.50.0-alpha.12) (2019-08-14)

**Note:** Version bump only for package @ohif/viewer

# [0.50.0-alpha.11](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.0.22-alpha.10...@ohif/viewer@0.50.0-alpha.11) (2019-08-14)

**Note:** Version bump only for package @ohif/viewer

## [0.0.22-alpha.10](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.0.22-alpha.9...@ohif/viewer@0.0.22-alpha.10) (2019-08-14)

**Note:** Version bump only for package @ohif/viewer

## 0.0.22-alpha.9 (2019-08-14)

**Note:** Version bump only for package @ohif/viewer

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.22-alpha.8](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.0.22-alpha.7...@ohif/viewer@0.0.22-alpha.8) (2019-08-08)

**Note:** Version bump only for package @ohif/viewer

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.22-alpha.7](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.0.22-alpha.6...@ohif/viewer@0.0.22-alpha.7) (2019-08-08)

**Note:** Version bump only for package @ohif/viewer

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.22-alpha.6](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.0.22-alpha.5...@ohif/viewer@0.0.22-alpha.6) (2019-08-08)

**Note:** Version bump only for package @ohif/viewer

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.22-alpha.5](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.0.22-alpha.4...@ohif/viewer@0.0.22-alpha.5) (2019-08-08)

**Note:** Version bump only for package @ohif/viewer

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.22-alpha.4](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.0.22-alpha.3...@ohif/viewer@0.0.22-alpha.4) (2019-08-08)

**Note:** Version bump only for package @ohif/viewer

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.22-alpha.3](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.0.22-alpha.2...@ohif/viewer@0.0.22-alpha.3) (2019-08-07)

**Note:** Version bump only for package @ohif/viewer

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.22-alpha.2](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.0.22-alpha.1...@ohif/viewer@0.0.22-alpha.2) (2019-08-07)

**Note:** Version bump only for package @ohif/viewer

## [0.0.22-alpha.1](https://github.com/OHIF/Viewers/compare/@ohif/viewer@0.0.22-alpha.0...@ohif/viewer@0.0.22-alpha.1) (2019-08-07)

**Note:** Version bump only for package @ohif/viewer

## 0.0.22-alpha.0 (2019-08-05)

**Note:** Version bump only for package @ohif/viewer
