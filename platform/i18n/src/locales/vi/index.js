import CineDialog from './CineDialog.json';
import Common from './Common.json';
import Header from './Header.json';
import MeasurementTable from './MeasurementTable.json';
import StudyList from './StudyList.json';
import UserPreferencesModal from './UserPreferencesModal.json';
import Vindoc from './Vindoc.json';
import PredictionPanel from './PredictionPanel.json';

export default {
  vi: {
    CineDialog,
    Common,
    Header,
    MeasurementTable,
    StudyList,
    UserPreferencesModal,
    Vindoc,
    PredictionPanel,
  },
};
