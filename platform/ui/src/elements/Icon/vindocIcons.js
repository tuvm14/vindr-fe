import vi from './icons/vietnam.svg';
import usa from './icons/usa.svg';
import account from './icons/account.svg';
import batchAnalysis from './icons/batch-analysis.svg';
import bin from './icons/bin.svg';
import graph from './icons/graph.svg';
import trashfiles from './icons/trash-files.svg';
import modality from './icons/modality.svg';
import download from './icons/download.svg';
import settings from './icons/settings.svg';
import ai from './icons/ai.svg';
import dashboard from './icons/dashboard.svg';
import chart from './icons/chart.svg';
import next from './icons/next.svg';
import previous from './icons/previous.svg';
import rightArrow from './icons/right-arrow.svg';
import leftArrow from './icons/left-arrow.svg';
import eraser from './icons/eraser.svg';
import pin from './icons/push_pin.svg';
import unpin from './icons/push_unpin.svg';
import caretDown from './icons/caret-down.svg';
import caretUp from './icons/caret-up.svg';
import userFolder from './icons/user-folder.svg';
import shareFolder from './icons/share-folder.svg';
import folder from './icons/folder.svg';
import windowLevel from './icons/window-level.svg';
import infor from './icons/infor.svg';
import scroll from './icons/scroll.svg';
import icon3D from './icons/3d-model.svg';
import doctorReport from './icons/doctor-report.svg';
import multiViewer from './icons/multi_viewer.svg';
import panTool from './icons/pan_tool.svg';
import zoomIn from './icons/zoom_in.svg';
import imageSearch from './icons/image_search.svg';
import rotateRight from './icons/rotate_right.svg';
import flipVertical from './icons/flip_vertical.svg';
import flipHorizontal from './icons/flip_horizontal.svg';
import invertColors from './icons/invert_colors.svg';
import brightness from './icons/brightness.svg';
import tune from './icons/tune.svg';
import settingsBrightness from './icons/settings_brightness.svg';
import picker from './icons/picker.svg';
import settingsOverscan from './icons/settings_overscan.svg';
import refresh from './icons/refresh.svg';
import smartDisplay from './icons/smart_display.svg';
import crossHair from './icons/cross-hair.svg';
import stack from './icons/stack.svg';
import cube3D1 from './icons/3D_Сube_1.svg';
import cube3D2 from './icons/3D_Сube_2.svg';
import straighten from './icons/straighten.svg';
import distance from './icons/distance.svg';
import deleteIcon from './icons/delete.svg';
import elipticalRoi from './icons/Eliptical ROI.svg';
import textRotation from './icons/text_rotation.svg';
import pen from './icons/pen.svg';
import brush2 from './icons/brush_2.svg';
import info2 from './icons/info_2.svg';
import download2 from './icons/download_2.svg';
import description from './icons/description.svg';
import cad from './icons/cad.svg';
import rectangleRoi from './icons/Rectangle ROI.svg';
import angle from './icons/Angle.svg';
import polyline from './icons/polyline.svg';
import eraser2 from './icons/eraser_2.svg';
import splineCut from './icons/spline_cut.svg';
import crop from './icons/crop.svg';
import coupon from './icons/coupon.svg';
import insertLink from './icons/insert_link.svg';
import clear from './icons/clear.svg';
import link2 from './icons/link_2.svg';
import linkOff from './icons/link_off.svg';
import home from './icons/home.svg';
import bookmarkFolder from './icons/folder-bookmark.svg';
import sharedFolder from './icons/shared-folder.svg';
import allDeviceFolder from './icons/all-devices-folder.svg';
import removeBed from './icons/remove_bed.svg';
import rectScalpel from './icons/rect-scalpel.svg';
import freehandScalpel from './icons/freehand-scalpel.svg';
import turnedIn from './icons/turned_in.svg';

const icons = {
  vi,
  'en-US': usa,
  account,
  'batch-analysis': batchAnalysis,
  bin,
  graph,
  'trash-files': trashfiles,
  modality,
  download,
  settings,
  ai,
  dashboard,
  chart,
  next,
  previous,
  'right-arrow': rightArrow,
  'left-arrow': leftArrow,
  eraser,
  pin,
  unpin,
  'caret-down': caretDown,
  'caret-up': caretUp,
  'share-folder': shareFolder,
  'bookmark-folder': bookmarkFolder,
  'shared-folder': sharedFolder,
  allDeviceFolder,
  folder: folder,
  'user-folder': userFolder,
  scroll,
  'window-level': windowLevel,
  'icon-3d': icon3D,
  'doctor-report': doctorReport,
  infor,
  multiViewer,
  panTool,
  zoomIn,
  imageSearch,
  rotateRight,
  flipVertical,
  flipHorizontal,
  invertColors,
  brightness,
  tune,
  settingsBrightness,
  picker,
  settingsOverscan,
  refresh,
  smartDisplay,
  crossHair,
  stack,
  cube3D1,
  cube3D2,
  straighten,
  distance,
  deleteIcon,
  elipticalRoi,
  textRotation,
  pen,
  brush2,
  info2,
  download2,
  description,
  cad,
  rectangleRoi,
  angle,
  polyline,
  eraser2,
  splineCut,
  crop,
  coupon,
  insertLink,
  clear,
  link2,
  linkOff,
  home,
  'remove-bed': removeBed,
  rectScalpel,
  freehandScalpel,
  turnedIn,
};

export default icons;
