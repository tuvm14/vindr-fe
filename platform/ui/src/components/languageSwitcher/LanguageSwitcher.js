import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import { CaretDownOutlined } from '@ant-design/icons';

import './LanguageSwitcher.styl';

const { Option } = Select;

const LanguageSwitcher = ({ language, onLanguageChange, languages }) => {
  const onChange = value => {
    onLanguageChange(value);
  };

  return (
    <Select
      name="language-select"
      id="language-select"
      className="language-select"
      dropdownClassName="dropdown-options-dark"
      suffixIcon={<CaretDownOutlined />}
      value={language}
      onChange={onChange}
    >
      {languages.map(lng => (
        <Option value={lng.value} key={lng.value}>
          {lng.label}
        </Option>
      ))}
    </Select>
  );
};

LanguageSwitcher.propTypes = {
  language: PropTypes.string.isRequired,
  languages: PropTypes.array.isRequired,
  onLanguageChange: PropTypes.func.isRequired,
};

export { LanguageSwitcher };
