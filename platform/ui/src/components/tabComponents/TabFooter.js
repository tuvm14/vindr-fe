import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';

import './TabFooter.styl';

// In case translate is not passed
const translate = word => word;

function TabFooter({
  onResetPreferences,
  onSave,
  onCancel,
  hasErrors,
  t = translate,
}) {
  return (
    <div className="footer">
      <Button
        data-cy="reset-default-btn"
        className="btn"
        ghost
        onClick={onResetPreferences}
      >
        {t('Reset to Defaults')}
      </Button>
      <div>
        <Button data-cy="cancel-btn" className="btn" ghost onClick={onCancel}>
          {t('Cancel')}
        </Button>
        <Button
          data-cy="save-btn"
          className="btn"
          type="primary"
          onClick={onSave}
          disabled={hasErrors}
        >
          {t('Save')}
        </Button>
      </div>
    </div>
  );
}

TabFooter.propTypes = {
  onResetPreferences: PropTypes.func,
  onSave: PropTypes.func,
  onCancel: PropTypes.func,
  hasErrors: PropTypes.bool,
  t: PropTypes.func,
};

export { TabFooter };
