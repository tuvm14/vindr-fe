import React, {
  useRef,
  useCallback,
  useEffect,
  useState,
  createRef,
  useMemo,
} from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import './ViewportDownloadForm.styl';
import { Icon } from '@tuvm/ui';
import { Spin, Button, Input, Select, Checkbox, message } from 'antd';
import { CaretDownOutlined, CheckOutlined } from '@ant-design/icons';
import api from '@tuvm/viewer/src/services/api';
import cornerstone from 'cornerstone-core';
const { Option } = Select;

const FILE_TYPE_OPTIONS = [
  {
    key: 'jpg',
    value: 'jpg',
  },
  {
    key: 'png',
    value: 'png',
  },
];

const EXPORT_TYPE = {
  STUDY: 'study',
  SERIES: 'series',
  INSTANCE: 'instance',
  IMAGE: 'image',
};

const DEFAULT_FILENAME = 'image';
const REFRESH_VIEWPORT_TIMEOUT = 1000;

const ViewportDownloadForm = ({
  activeViewport,
  onClose,
  updateViewportPreview,
  enableViewport,
  disableViewport,
  toggleAnnotations,
  loadImage,
  downloadBlob,
  defaultSize,
  minimumSize,
  maximumSize,
  canvasClass,
  viewportSpecificData,
}) => {
  const [t] = useTranslation('Vindoc');

  const [filename, setFilename] = useState(DEFAULT_FILENAME);
  const [fileType, setFileType] = useState('jpg');
  const [processing, setProcessing] = useState(false);
  const [exportType, setExportType] = useState('image');

  const EXPORT_TYPE_OPTIONS = useMemo(
    () => [
      {
        key: t('Current study (dicom)'),
        value: EXPORT_TYPE.STUDY,
      },
      {
        key: t('Current series (dicom)'),
        value: EXPORT_TYPE.SERIES,
      },
      {
        key: t('Current instance (dicom)'),
        value: EXPORT_TYPE.INSTANCE,
      },
      {
        key: t('Current image'),
        value: EXPORT_TYPE.IMAGE,
      },
    ],
    [t]
  );

  const [dimensions, setDimensions] = useState({
    width: defaultSize,
    height: defaultSize,
  });

  const [showAnnotations, setShowAnnotations] = useState(true);

  const [keepAspect, setKeepAspect] = useState(true);
  const [aspectMultiplier, setAspectMultiplier] = useState({
    width: 1,
    height: 1,
  });

  const [viewportElement, setViewportElement] = useState();
  const [viewportElementDimensions, setViewportElementDimensions] = useState({
    width: defaultSize,
    height: defaultSize,
  });

  const [downloadCanvas, setDownloadCanvas] = useState({
    ref: createRef(),
    width: defaultSize,
    height: defaultSize,
  });

  const [viewportPreview, setViewportPreview] = useState({
    src: null,
    width: defaultSize,
    height: defaultSize,
  });

  const [error, setError] = useState({
    width: false,
    height: false,
    filename: false,
  });

  const hasError = Object.values(error).includes(true);

  const refreshViewport = useRef(null);

  const downloadDicomFile = async () => {
    try {
      setProcessing(true);
      const { StudyInstanceUID, SeriesInstanceUID } =
        viewportSpecificData || {};
      let url = '';
      let fileName = '';
      if (exportType === EXPORT_TYPE.STUDY) {
        url = `/backend/dicom/studies/${StudyInstanceUID}/download`;
        fileName = `study_${StudyInstanceUID}_download.zip`;
      } else if (exportType === EXPORT_TYPE.SERIES) {
        url = `/backend/dicom/series/${SeriesInstanceUID}/download`;
        fileName = `series_${SeriesInstanceUID}_download.zip`;
      } else {
        const img = cornerstone.getImage(activeViewport);
        const { sopInstanceUid } =
          cornerstone.metaData.get('generalImageModule', img.imageId) || {};
        if (!sopInstanceUid) return;
        url = `/backend/dicom/instances/${sopInstanceUid}/download`;
        fileName = `instance_${sopInstanceUid}_download.zip`;
      }
      const res = await api({
        url,
        responseType: 'arraybuffer',
        method: 'GET',
      });

      let anchor = document.createElement('a');
      const blob = new Blob([res.data]);
      const URLObj = window.URL || window.webkitURL;
      let objectUrl = URLObj.createObjectURL(blob);
      anchor.href = objectUrl;
      anchor.download = fileName;
      anchor.click();
      URLObj.revokeObjectURL(objectUrl);

      setProcessing(false);
    } catch (error) {
      message.error(t('Sytem error!'));
      setProcessing(false);
    }
  };

  const downloadImage = () => {
    if (exportType === EXPORT_TYPE.IMAGE) {
      downloadBlob(
        filename || DEFAULT_FILENAME,
        fileType,
        viewportElement,
        downloadCanvas.ref.current
      );
    } else {
      downloadDicomFile();
    }
  };

  /**
   * @param {object} event - Input change event
   * @param {string} dimension - "height" | "width"
   */
  const onDimensionsChange = (event, dimension) => {
    const oppositeDimension = dimension === 'height' ? 'width' : 'height';
    const sanitizedTargetValue = event.target.value.replace(/\D/, '');
    const isEmpty = sanitizedTargetValue === '';
    const newDimensions = { ...dimensions };
    const updatedDimension = isEmpty
      ? ''
      : Math.min(sanitizedTargetValue, maximumSize);

    if (updatedDimension === dimensions[dimension]) {
      return;
    }

    newDimensions[dimension] = updatedDimension;

    if (keepAspect && newDimensions[oppositeDimension] !== '') {
      newDimensions[oppositeDimension] = Math.round(
        newDimensions[dimension] * aspectMultiplier[oppositeDimension]
      );
    }

    // In current code, keepAspect is always `true`
    // And we always start w/ a square width/height
    setDimensions(newDimensions);

    // Only update if value is non-empty
    if (!isEmpty) {
      setViewportElementDimensions(newDimensions);
      setDownloadCanvas(state => ({
        ...state,
        ...newDimensions,
      }));
    }
  };

  const error_messages = {
    width: t('minWidthError'),
    height: t('minHeightError'),
    filename: t('emptyFilenameError'),
  };

  const renderErrorHandler = errorType => {
    if (!error[errorType]) {
      return null;
    }

    return <div className="input-error">{error_messages[errorType]}</div>;
  };

  const onKeepAspectToggle = () => {
    const { width, height } = dimensions;
    const aspectMultiplier = { ...aspectMultiplier };
    if (!keepAspect) {
      const base = Math.min(width, height);
      aspectMultiplier.width = width / base;
      aspectMultiplier.height = height / base;
      setAspectMultiplier(aspectMultiplier);
    }

    setKeepAspect(!keepAspect);
  };

  const validSize = value => (value >= minimumSize ? value : minimumSize);
  const loadAndUpdateViewports = useCallback(async () => {
    const { width: scaledWidth, height: scaledHeight } = await loadImage(
      activeViewport,
      viewportElement,
      dimensions.width,
      dimensions.height
    );

    toggleAnnotations(showAnnotations, viewportElement);

    const scaledDimensions = {
      height: validSize(scaledHeight),
      width: validSize(scaledWidth),
    };

    setViewportElementDimensions(scaledDimensions);
    setDownloadCanvas(state => ({
      ...state,
      ...scaledDimensions,
    }));

    const {
      dataUrl,
      width: viewportElementWidth,
      height: viewportElementHeight,
    } = await updateViewportPreview(
      viewportElement,
      downloadCanvas.ref.current,
      fileType
    );

    setViewportPreview(state => ({
      ...state,
      src: dataUrl,
      width: validSize(viewportElementWidth),
      height: validSize(viewportElementHeight),
    }));
  }, [
    activeViewport,
    viewportElement,
    showAnnotations,
    loadImage,
    toggleAnnotations,
    updateViewportPreview,
    fileType,
    downloadCanvas.ref,
    minimumSize,
    maximumSize,
    viewportElementDimensions,
  ]);

  useEffect(() => {
    enableViewport(viewportElement);

    return () => {
      disableViewport(viewportElement);
    };
  }, [disableViewport, enableViewport, viewportElement]);

  useEffect(() => {
    if (refreshViewport.current !== null) {
      clearTimeout(refreshViewport.current);
    }

    refreshViewport.current = setTimeout(() => {
      refreshViewport.current = null;
      loadAndUpdateViewports();
    }, REFRESH_VIEWPORT_TIMEOUT);
  }, [
    activeViewport,
    viewportElement,
    showAnnotations,
    dimensions,
    loadImage,
    toggleAnnotations,
    updateViewportPreview,
    fileType,
    downloadCanvas.ref,
    minimumSize,
    maximumSize,
  ]);

  useEffect(() => {
    const { width, height } = dimensions;
    const hasError = {
      width: width < minimumSize,
      height: height < minimumSize,
      filename: !filename,
    };

    setError({ ...hasError });
  }, [dimensions, filename, minimumSize]);

  return (
    <div className="ViewportDownloadForm">
      <Spin spinning={processing}>
        <div
          style={{ display: 'flex', justifyContent: 'space-between' }}
          className="download-container"
        >
          {viewportPreview.src ? (
            <div className="preview" data-cy="image-preview">
              <div className="preview-header"> {t('Preview')}</div>
              <img
                className="viewport-preview"
                src={viewportPreview.src}
                alt={t('Preview')}
                data-cy="viewport-preview-img"
              />
            </div>
          ) : (
            <div className="loading-image">
              <Icon name="circle-notch" className="icon-spin" />
              {t('loadingPreview')}
            </div>
          )}
          <div className="section-select">
            <div className="export-type input-group">
              <div className="input-label">{t('Select Source')}</div>
              <Select
                data-cy="export-type"
                className="select-dropdown-light no-border"
                dropdownClassName="dropdown-options-dark"
                menuItemSelectedIcon={<CheckOutlined />}
                suffixIcon={<CaretDownOutlined />}
                value={exportType}
                onChange={value => setExportType(value)}
                style={{ width: '100%' }}
              >
                {EXPORT_TYPE_OPTIONS.map((item, idx) => (
                  <Option key={idx} value={item.value}>
                    {item.key}
                  </Option>
                ))}
              </Select>
            </div>
            <div
              style={exportType == EXPORT_TYPE.IMAGE ? {} : { display: 'none' }}
            >
              <div
                className="file-info-container"
                data-cy="file-info-container"
              >
                <div className="dimension-wrapper">
                  <div className="dimensions">
                    <div className="width input-group">
                      <div className="input-label">{t('imageWidth')}</div>
                      <Input
                        type="number"
                        min={minimumSize}
                        max={maximumSize}
                        value={dimensions.width}
                        label={t('imageWidth')}
                        onChange={evt => onDimensionsChange(evt, 'width')}
                        data-cy="image-width"
                      />
                      {renderErrorHandler('width')}
                    </div>
                    <div className="height input-group">
                      <div className="input-label">{t('imageHeight')}</div>
                      <Input
                        type="number"
                        min={minimumSize}
                        max={maximumSize}
                        value={dimensions.height}
                        label={t('imageHeight')}
                        onChange={evt => onDimensionsChange(evt, 'height')}
                        data-cy="image-height"
                      />
                      {renderErrorHandler('height')}
                    </div>
                  </div>
                </div>

                <div className="file-wrapper">
                  <div className="file-name input-group">
                    <div className="input-label">{t('filename')}</div>
                    <Input
                      type="text"
                      data-cy="file-name"
                      value={filename}
                      onChange={event => setFilename(event.target.value)}
                      label={t('filename')}
                      id="file-name"
                    />
                    {renderErrorHandler('filename')}
                  </div>
                  <div className="file-type input-group">
                    <div className="input-label">{t('fileType')}</div>
                    <Select
                      className="select-dropdown-light no-border"
                      dropdownClassName="dropdown-options-dark"
                      menuItemSelectedIcon={<CheckOutlined />}
                      suffixIcon={<CaretDownOutlined />}
                      value={fileType}
                      data-cy="file-type"
                      onChange={value => setFileType(value)}
                      style={{ width: '100%' }}
                    >
                      {FILE_TYPE_OPTIONS.map((item, idx) => (
                        <Option key={idx} value={item.value}>
                          {item.key}
                        </Option>
                      ))}
                    </Select>
                  </div>
                </div>

                <div>
                  <Checkbox
                    id="keep-aspect"
                    data-cy="keep-aspect"
                    type="checkbox"
                    className="form-check-input"
                    checked={keepAspect}
                    onChange={onKeepAspectToggle}
                  >
                    {t('Keep Aspect Ratio')}
                  </Checkbox>
                  <Checkbox
                    id="show-annotations"
                    data-cy="show-annotations"
                    type="checkbox"
                    className="form-check-input"
                    checked={showAnnotations}
                    onChange={event => setShowAnnotations(event.target.checked)}
                  >
                    {t('showAnnotations')}
                  </Checkbox>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="actions">
          <div className="action-cancel">
            <Button
              data-cy="cancel-btn"
              className="btn"
              ghost
              onClick={onClose}
            >
              {t('Cancel')}
            </Button>
          </div>
          <div className="action-save">
            <Button
              disabled={exportType === EXPORT_TYPE.IMAGE && hasError}
              onClick={downloadImage}
              className="btn"
              type="primary"
              data-cy="download-btn"
            >
              {t('Download')}
            </Button>
          </div>
        </div>
      </Spin>
      <div
        style={{
          height: viewportElementDimensions.height,
          width: viewportElementDimensions.width,
          position: 'absolute',
          left: '9999px',
        }}
        ref={ref => setViewportElement(ref)}
      >
        <canvas
          className={canvasClass}
          style={{
            height: downloadCanvas.height,
            width: downloadCanvas.width,
            display: 'block',
          }}
          width={downloadCanvas.width}
          height={downloadCanvas.height}
          ref={downloadCanvas.ref}
        ></canvas>
      </div>
    </div>
  );
};

ViewportDownloadForm.propTypes = {
  onClose: PropTypes.func.isRequired,
  activeViewport: PropTypes.object,
  updateViewportPreview: PropTypes.func.isRequired,
  enableViewport: PropTypes.func.isRequired,
  disableViewport: PropTypes.func.isRequired,
  toggleAnnotations: PropTypes.func.isRequired,
  loadImage: PropTypes.func.isRequired,
  downloadBlob: PropTypes.func.isRequired,
  /** A default width & height, between the minimum and maximum size */
  defaultSize: PropTypes.number.isRequired,
  minimumSize: PropTypes.number.isRequired,
  maximumSize: PropTypes.number.isRequired,
  canvasClass: PropTypes.string.isRequired,
};

export default ViewportDownloadForm;
