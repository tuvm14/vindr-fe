import React from 'react';
import { Tooltip as AntTooltip } from 'antd';
import { useWindowSize } from '../../hooks/useWindowSize';
import { MOBILE_SIZE_LIMIT } from '@tuvm/viewer/src/utils/constants';

function isMobile(width, height) {
  return width < MOBILE_SIZE_LIMIT;
}

const Tooltip = ({ ...rest }) => {
  const [width, height] = useWindowSize();
  return (
    <AntTooltip
      trigger={isMobile(width, height) ? 'contextMenu' : 'hover'}
      {...rest}
    ></AntTooltip>
  );
};

export default Tooltip;
