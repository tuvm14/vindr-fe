import PropTypes from 'prop-types';
import React from 'react';
import './ContextMenu.css';

const ContextMenu = ({ items, onClick }) => {
  if (!items || !items.length) return null;
  return (
    <ul className="popup" onContextMenu={e => e.preventDefault()}>
      {items.map((item, index) => (
        <li key={index} onClick={() => onClick(item)}>
          <span style={{ marginRight: 10 }}>{item.icon}</span>
          <span>{item.label}</span>
        </li>
      ))}
    </ul>
  );
};

ContextMenu.propTypes = {
  items: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ContextMenu;
