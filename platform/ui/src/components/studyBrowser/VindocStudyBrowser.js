import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import { Collapse, Menu, Dropdown } from 'antd';
import { HistoryOutlined } from '@ant-design/icons';
import moment from 'moment';
import { Thumbnail } from './VindrThumbnail';
import './VinDrStudyBrowser.styl';
import { useWindowSize } from '../../hooks/useWindowSize';
import { isMobile } from '@tuvm/viewer/src/utils/helper';

const { Panel } = Collapse;

function getAIResult(aiResult, StudyInstanceUID) {
  if (aiResult && aiResult[StudyInstanceUID]) {
    return aiResult[StudyInstanceUID];
  }
  // const aiSeriesData = get(aiResult, `${StudyInstanceUID}.series`, {});
  // return aiSeriesData;
  return {};
}

function StudyBrowser(props) {
  const {
    studies,
    onThumbnailClick,
    onThumbnailDoubleClick,
    supportsDrag,
    aiResult,
    onRetrySeriesMetadata,
    displaySetInstanceUIDSelected,
  } = props;

  const [width, height] = useWindowSize();
  const [selectedStudy, setSelectedStudy] = useState(studies[0] || {});
  useEffect(() => {
    if (isMobile(width, height)) {
      studies.forEach(it => {
        if (
          it.thumbnails &&
          it.thumbnails.length &&
          it.thumbnails[0]?.displaySetInstanceUID ==
            displaySetInstanceUIDSelected
        ) {
          setSelectedStudy(it);
        }
      });
    }
  }, [studies]);

  const handleChangeSelectedStudy = selectedStudy => {
    if (
      selectedStudy &&
      selectedStudy.thumbnails &&
      selectedStudy.thumbnails.length
    ) {
      const newSelected = selectedStudy.thumbnails[0];
      const { displaySetInstanceUID, isFetching, success } = newSelected;
      if (isFetching || success === false) return;
      onThumbnailClick(displaySetInstanceUID);
    }
  };

  const windowSettingMenu = (
    <Menu className="window-setting-options" se>
      {studies.map(item => (
        <Menu.Item
          key={item.StudyInstanceUID}
          onClick={() => {
            setSelectedStudy(item);
            handleChangeSelectedStudy(item);
          }}
          className="window-setting-item"
        >
          {`${item.PatientName || item.PatientID || 'No name'}
          ${moment(selectedStudy.StudyDate, 'YYYYMMDD').format('YYYY-MM-DD')}`}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <div className="study-browser">
      <div
        className={`scrollable-study-thumbnails ${
          studies.length > 1 ? '' : 'list-thumbnails'
        }`}
      >
        {isMobile(width, height) && (
          <>
            <Collapse
              defaultActiveKey={['1']}
              expandIconPosition="right"
              className="study-item"
            >
              <Panel
                header={
                  <span className="patient-info">
                    <div
                      onClick={event => event.stopPropagation()}
                      className="more-study"
                    >
                      <Dropdown
                        overlay={windowSettingMenu}
                        destroyPopupOnHide={true}
                        placement="topCenter"
                        trigger={['click']}
                        overlayClassName="dropdown-window-setting"
                      >
                        <HistoryOutlined />
                      </Dropdown>
                    </div>

                    <div className="patient-name">
                      {selectedStudy.PatientName || selectedStudy.PatientID || 'No name'}
                    </div>
                    {selectedStudy.StudyDate && (
                      <div className="patient-date">
                        {moment(selectedStudy.StudyDate, 'YYYYMMDD').format('YYYY-MM-DD')}
                      </div>
                    )}
                  </span>
                }
                className="list-thumbnails"
              >
                {selectedStudy.thumbnails &&
                  selectedStudy.thumbnails.map((thumb, thumbIndex) => {
                    const {
                      altImageText,
                      displaySetInstanceUID,
                      imageId,
                      InstanceNumber,
                      numImageFrames,
                      SeriesDescription,
                      SeriesNumber,
                      stackPercentComplete,
                      SeriesInstanceUID,
                      isFetching,
                      success,
                    } = thumb;

                    let isShowHighlight = false;
                    const { series: aiSeriesData } =
                      getAIResult(aiResult, selectedStudy.StudyInstanceUID) ||
                      {};
                    if (aiSeriesData && aiSeriesData[SeriesInstanceUID]) {
                      const seriesAIResult =
                        (aiSeriesData[SeriesInstanceUID] || {}).ai_result || {};

                      isShowHighlight = seriesAIResult.result_status;
                    }

                    return (
                      <div
                        key={thumb.displaySetInstanceUID}
                        className="thumbnail-container"
                        data-cy="thumbnail-list"
                      >
                        <Thumbnail
                          supportsDrag={supportsDrag}
                          key={`${thumbIndex}`}
                          id={`${thumbIndex}`} // Unused?
                          // Study
                          StudyInstanceUID={selectedStudy.StudyInstanceUID} // used by drop
                          // Thumb
                          altImageText={altImageText}
                          imageId={imageId}
                          InstanceNumber={InstanceNumber}
                          displaySetInstanceUID={displaySetInstanceUID} // used by drop
                          numImageFrames={numImageFrames}
                          SeriesDescription={SeriesDescription}
                          SeriesNumber={SeriesNumber}
                          stackPercentComplete={stackPercentComplete}
                          // Events
                          onClick={() => {
                            if (isFetching || success === false) return;
                            onThumbnailClick(displaySetInstanceUID);
                          }}
                          thumbIndex={thumbIndex}
                          active={
                            displaySetInstanceUIDSelected === displaySetInstanceUID
                          }
                          onDoubleClick={onThumbnailDoubleClick}
                          isShowHighlight={isShowHighlight}
                          onRetrySeriesMetadata={() =>
                            onRetrySeriesMetadata(SeriesInstanceUID)
                          }
                          isFetching={isFetching}
                          isLoadSucess={success}
                        />
                      </div>
                    );
                  })}
              </Panel>
            </Collapse>
          </>
        )}
        {!isMobile(width, height) &&
          studies
            .map((study, studyIndex) => {
              const {
                StudyInstanceUID,
                PatientName,
                StudyDate,
                PatientID,
              } = study;

              if (studies.length > 1) {
                return (
                  <Collapse
                    key={studyIndex}
                    defaultActiveKey={['1']}
                    expandIconPosition="right"
                    className="study-item"
                  >
                    <Panel
                      key={`${studyIndex + 1}`}
                      header={
                        <span className="patient-info">
                          <div className="patient-name">
                            {PatientName || PatientID || 'No name'}
                          </div>
                          {StudyDate && (
                            <div className="patient-date">
                              {moment(StudyDate, 'YYYYMMDD').format('YYYY-MM-DD')}
                            </div>
                          )}
                        </span>
                      }
                      className="list-thumbnails"
                    >
                      {study.thumbnails.map((thumb, thumbIndex) => {
                        const {
                          altImageText,
                          displaySetInstanceUID,
                          imageId,
                          InstanceNumber,
                          numImageFrames,
                          SeriesDescription,
                          SeriesNumber,
                          stackPercentComplete,
                          SeriesInstanceUID,
                          isFetching,
                          success,
                        } = thumb;

                        let isShowHighlight = false;
                        const { series: aiSeriesData } =
                          getAIResult(aiResult, StudyInstanceUID) || {};
                        if (aiSeriesData && aiSeriesData[SeriesInstanceUID]) {
                          const seriesAIResult =
                            (aiSeriesData[SeriesInstanceUID] || {}).ai_result ||
                            {};

                          isShowHighlight = seriesAIResult.result_status;
                        }

                        return (
                          <div
                            key={thumb.displaySetInstanceUID}
                            className="thumbnail-container"
                            data-cy="thumbnail-list"
                          >
                            <Thumbnail
                              supportsDrag={supportsDrag}
                              key={`${studyIndex}_${thumbIndex}`}
                              id={`${studyIndex}_${thumbIndex}`} // Unused?
                              // Study
                              StudyInstanceUID={StudyInstanceUID} // used by drop
                              // Thumb
                              altImageText={altImageText}
                              imageId={imageId}
                              InstanceNumber={InstanceNumber}
                              displaySetInstanceUID={displaySetInstanceUID} // used by drop
                              numImageFrames={numImageFrames}
                              SeriesDescription={SeriesDescription}
                              SeriesNumber={SeriesNumber}
                              stackPercentComplete={stackPercentComplete}
                              // Events
                              onClick={() => {
                                if (isFetching || success === false) return;
                                onThumbnailClick(displaySetInstanceUID);
                              }}
                              thumbIndex={thumbIndex}
                              active={
                                displaySetInstanceUIDSelected ===
                                displaySetInstanceUID
                              }
                              onDoubleClick={onThumbnailDoubleClick}
                              isShowHighlight={isShowHighlight}
                              onRetrySeriesMetadata={() =>
                                onRetrySeriesMetadata(SeriesInstanceUID)
                              }
                              isFetching={isFetching}
                              isLoadSucess={success}
                            />
                          </div>
                        );
                      })}
                    </Panel>
                  </Collapse>
                );
              } else {
                return study.thumbnails.map((thumb, thumbIndex) => {
                  const {
                    altImageText,
                    displaySetInstanceUID,
                    imageId,
                    InstanceNumber,
                    numImageFrames,
                    SeriesDescription,
                    SeriesNumber,
                    stackPercentComplete,
                    SeriesInstanceUID,
                    isFetching,
                    success,
                  } = thumb;

                  let isShowHighlight = false;

                  const { series: aiSeriesData } =
                    getAIResult(aiResult, StudyInstanceUID) || {};
                  if (aiSeriesData && aiSeriesData[SeriesInstanceUID]) {
                    const seriesAIResult =
                      (aiSeriesData[SeriesInstanceUID] || {}).ai_result || {};

                    isShowHighlight = seriesAIResult.result_status;
                  }

                  return (
                    <div
                      key={thumb.displaySetInstanceUID}
                      className="thumbnail-container"
                      data-cy="thumbnail-list"
                    >
                      <Thumbnail
                        supportsDrag={supportsDrag}
                        key={`${studyIndex}_${thumbIndex}`}
                        id={`${studyIndex}_${thumbIndex}`} // Unused?
                        // Study
                        StudyInstanceUID={StudyInstanceUID} // used by drop
                        // Thumb
                        altImageText={altImageText}
                        imageId={imageId}
                        InstanceNumber={InstanceNumber}
                        displaySetInstanceUID={displaySetInstanceUID} // used by drop
                        numImageFrames={numImageFrames}
                        SeriesDescription={SeriesDescription}
                        SeriesNumber={SeriesNumber}
                        stackPercentComplete={stackPercentComplete}
                        // Events
                        onClick={() => {
                          if (isFetching || success === false) return;
                          onThumbnailClick(displaySetInstanceUID);
                        }}
                        thumbIndex={thumbIndex}
                        active={
                          displaySetInstanceUIDSelected === displaySetInstanceUID
                        }
                        onDoubleClick={onThumbnailDoubleClick}
                        isShowHighlight={isShowHighlight}
                        onRetrySeriesMetadata={() =>
                          onRetrySeriesMetadata(SeriesInstanceUID)
                        }
                        isFetching={isFetching}
                        isLoadSucess={success}
                      />
                    </div>
                  );
                });
              }
            })
            .flat()}
      </div>
    </div>
  );
}

const noop = () => {};

StudyBrowser.propTypes = {
  studies: PropTypes.arrayOf(
    PropTypes.shape({
      StudyInstanceUID: PropTypes.string.isRequired,
      thumbnails: PropTypes.arrayOf(
        PropTypes.shape({
          altImageText: PropTypes.string,
          displaySetInstanceUID: PropTypes.string.isRequired,
          imageId: PropTypes.string,
          InstanceNumber: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
          ]),
          numImageFrames: PropTypes.number,
          SeriesDescription: PropTypes.string,
          SeriesNumber: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
          ]),
          stackPercentComplete: PropTypes.number,
        })
      ),
    })
  ).isRequired,
  supportsDrag: PropTypes.bool,
  onThumbnailClick: PropTypes.func,
  onThumbnailDoubleClick: PropTypes.func,
};

StudyBrowser.defaultProps = {
  studies: [],
  supportsDrag: !isMobile(window.innerWidth, window.innerHeight),
  onThumbnailClick: noop,
  onThumbnailDoubleClick: noop,
};

export default React.memo(StudyBrowser);
