import React from 'react';
import PropTypes from 'prop-types';
import { useDrag } from 'react-dnd';
import { Button } from 'antd';
import { Tooltip } from '@tuvm/ui';
import ImageThumbnail from './VindrImageThumbnail';
import classNames from 'classnames';

import './Thumbnail.styl';

function ThumbnailFooter({
  SeriesDescription,
  SeriesNumber,
  InstanceNumber,
  numImageFrames,
  thumbIndex,
}) {
  const infoOnly = !SeriesDescription;

  const getInfo = (value, icon, className = '') => {
    return (
      <div className={classNames('item item-series', className)}>
        <div className="icon">{icon}</div>
        <div className="value">{value}</div>
      </div>
    );
  };
  const getSeriesInformation = (thumbIndex, numImageFrames) => {
    if (!numImageFrames) {
      return;
    }

    return (
      <div className="series-information">
        <span
          style={{ fontSize: '11px', maxWidth: '78%' }}
          className="series-number"
        >
          Series {thumbIndex + 1}
        </span>
        {/*{getInfo(InstanceNumber, 'I:')}*/}
        {getInfo(numImageFrames, '', 'image-frames')}
      </div>
    );
  };

  return (
    <div className={classNames('series-details', { 'info-only': infoOnly })}>
      <div className="series-description">{SeriesDescription}</div>
      {getSeriesInformation(thumbIndex, numImageFrames)}
    </div>
  );
}

function ReloadIcon() {
  return (
    <span role="img" aria-label="reload" className="anticon anticon-reload">
      <svg
        viewBox="64 64 896 896"
        focusable="false"
        data-icon="reload"
        width="1em"
        height="1em"
        fill="currentColor"
        aria-hidden="true"
      >
        <path d="M909.1 209.3l-56.4 44.1C775.8 155.1 656.2 92 521.9 92 290 92 102.3 279.5 102 511.5 101.7 743.7 289.8 932 521.9 932c181.3 0 335.8-115 394.6-276.1 1.5-4.2-.7-8.9-4.9-10.3l-56.7-19.5a8 8 0 00-10.1 4.8c-1.8 5-3.8 10-5.9 14.9-17.3 41-42.1 77.8-73.7 109.4A344.77 344.77 0 01655.9 829c-42.3 17.9-87.4 27-133.8 27-46.5 0-91.5-9.1-133.8-27A341.5 341.5 0 01279 755.2a342.16 342.16 0 01-73.7-109.4c-17.9-42.4-27-87.4-27-133.9s9.1-91.5 27-133.9c17.3-41 42.1-77.8 73.7-109.4 31.6-31.6 68.4-56.4 109.3-73.8 42.3-17.9 87.4-27 133.8-27 46.5 0 91.5 9.1 133.8 27a341.5 341.5 0 01109.3 73.8c9.9 9.9 19.2 20.4 27.8 31.4l-60.2 47a8 8 0 003 14.1l175.6 43c5 1.2 9.9-2.6 9.9-7.7l.8-180.9c-.1-6.6-7.8-10.3-13-6.2z"></path>
      </svg>
    </span>
  );
}
function Thumbnail(props) {
  const {
    active,
    altImageText,
    error,
    displaySetInstanceUID,
    imageId,
    imageSrc,
    InstanceNumber,
    numImageFrames,
    SeriesDescription,
    SeriesNumber,
    stackPercentComplete,
    StudyInstanceUID,
    onClick,
    onDoubleClick,
    onMouseDown,
    supportsDrag,
    isShowHighlight,
    isFetching,
    isLoadSucess,
    onRetrySeriesMetadata,
  } = props;

  const [collectedProps, drag, dragPreview] = useDrag({
    // `droppedItem` in `dropTarget`
    // The only data it will have access to
    item: {
      StudyInstanceUID,
      displaySetInstanceUID,
      type: 'thumbnail', // Has to match `dropTarget`'s type
    },
    canDrag: function (monitor) {
      return !isFetching && isLoadSucess && supportsDrag;
    },
  });

  const hasImage = imageSrc || imageId;
  const hasAltText = altImageText !== undefined;

  return (
    <div
      ref={drag}
      className={classNames('thumbnail', { active: active })}
      onClick={onClick}
      onDoubleClick={onDoubleClick}
      onMouseDown={onMouseDown}
    >
      {/* SHOW IMAGE */}
      {hasImage && (
        <>
          {isShowHighlight && (
            <div className="image-thumbnail-highlight">
              <span>CAD</span>
            </div>
          )}
          <ImageThumbnail
            imageSrc={imageSrc}
            imageId={imageId}
            error={error}
            stackPercentComplete={stackPercentComplete}
          />
        </>
      )}
      {/* SHOW TEXT ALTERNATIVE */}
      {!hasImage && hasAltText && (
        <div className={'alt-image-text p-x-1'}>
          {isLoadSucess !== false && <h1>{altImageText}</h1>}
          {!isFetching && isLoadSucess === false && (
            <Tooltip title="Retry">
              <Button
                type="link"
                // ghost
                shape="circle"
                className="btn-retry"
                onClick={event => {
                  event.stopPropagation();
                  onRetrySeriesMetadata();
                }}
              >
                {ReloadIcon()}
              </Button>
            </Tooltip>
          )}
          {isFetching && (
            <div className="series-thumbnail-loading-indicator"></div>
          )}
        </div>
      )}
      {ThumbnailFooter(props)}
    </div>
  );
}

const noop = () => { };

Thumbnail.propTypes = {
  supportsDrag: PropTypes.bool,
  id: PropTypes.string.isRequired,
  displaySetInstanceUID: PropTypes.string.isRequired,
  StudyInstanceUID: PropTypes.string.isRequired,
  imageSrc: PropTypes.string,
  imageId: PropTypes.string,
  error: PropTypes.bool,
  active: PropTypes.bool,
  stackPercentComplete: PropTypes.number,
  /**
     altImageText will be used when no imageId or imageSrc is provided.
     It will be displayed inside the <div>. This is useful when it is difficult
     to make a preview for a type of DICOM series (e.g. DICOM-SR)
     */
  altImageText: PropTypes.string,
  SeriesDescription: PropTypes.string,
  SeriesNumber: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  InstanceNumber: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  numImageFrames: PropTypes.number,
  onDoubleClick: PropTypes.func,
  onClick: PropTypes.func,
  onMouseDown: PropTypes.func,
};

Thumbnail.defaultProps = {
  supportsDrag: false,
  active: false,
  error: false,
  stackPercentComplete: 0,
  onDoubleClick: noop,
  onClick: noop,
  onMouseDown: noop,
};

export { Thumbnail };
