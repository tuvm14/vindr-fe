import React, { Component, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Modal } from 'antd';
import './SimpleDialog.styl';
import { isMobile } from '@tuvm/viewer/src/utils/helper';

class SimpleDialog extends Component {
  static propTypes = {
    children: PropTypes.node,
    componentRef: PropTypes.any,
    componentStyle: PropTypes.object,
    rootClass: PropTypes.string,
    isOpen: PropTypes.bool,
    headerTitle: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isOpen: true,
    componentStyle: {},
    rootClass: '',
  };

  static InputDialog = ({ onSubmit, defaultValue, title, label, onClose }) => {
    const [value, setValue] = useState(defaultValue);

    const onSubmitHandler = () => {
      onSubmit(value);
    };

    return (
      <div className="InputDialog">
        <SimpleDialog
          headerTitle={title}
          onClose={onClose}
          onConfirm={onSubmitHandler}
        >
          <Form.Item name="label" label={label}>
            <Input
              placeholder={'Enter'}
              onChange={event => setValue(event.target.value)}
            />
          </Form.Item>
        </SimpleDialog>
      </div>
    );
  };

  render() {
    return (
      <React.Fragment>
        {this.props.isOpen && (
          <Modal
            className="vindr-modal"
            visible={this.props.isOpen}
            title={this.props.headerTitle}
            onCancel={this.onClose}
            onOk={this.onConfirm}
            width={
              isMobile(window.innerWidth, window.innerHeight)
                ? window.innerWidth - 20
                : 520
            }
          >
            {this.props.children}
          </Modal>
        )}
      </React.Fragment>
    );
  }

  onClose = event => {
    event.preventDefault();
    event.stopPropagation();
    this.props.onClose();
  };

  onConfirm = event => {
    event.preventDefault();
    event.stopPropagation();
    this.props.onConfirm();
  };
}

export { SimpleDialog };
