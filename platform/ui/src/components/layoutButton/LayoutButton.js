import React, { PureComponent } from 'react';
import { Menu, Dropdown } from 'antd';
import { LayoutChooser } from './LayoutChooser.js';
import PropTypes from 'prop-types';
import ToolbarButton from '../../viewer/VindocToolbarButton';
import { Icon } from '@tuvm/ui';
import './LayoutButton.styl';
import { Translation } from 'react-i18next';

const { SubMenu } = Menu;

const MENU_LIST = [
  { text: '1x1', icon: 'view1x1' },
  { text: '1x2', icon: 'view1x2' },
  { text: '2x1', icon: 'view2x1' },
  { text: '2x2', icon: 'view2x2' },
];

export class LayoutButton extends PureComponent {
  static defaultProps = {
    dropdownVisible: false,
  };

  static propTypes = {
    dropdownVisible: PropTypes.bool.isRequired,
    /** Called with the selectedCell number when grid sell is selected */
    onChange: PropTypes.func,
    /** The cell to show as selected */
    selectedCell: PropTypes.object,
  };

  state = {
    dropdownVisible: this.props.dropdownVisible,
  };

  hideActive() {
    this.setState({
      dropdownVisible: false,
    });
  }

  handleSelect = item => {
    let selectedCell = {};

    if (item == 'CC') {
      selectedCell.position = 'CC';
      selectedCell.row = 1;
      selectedCell.col = 2;
    } else if (item == 'MLO') {
      selectedCell.position = 'MLO';
      selectedCell.row = 1;
      selectedCell.col = 2;
    } else if (item == 'CC/MLO') {
      selectedCell.position = 'CC/MLO';
      selectedCell.row = 2;
      selectedCell.col = 2;
    } else {
      const split_row_col = item.split('x');
      selectedCell.row = parseInt(split_row_col[0]) - 1;
      selectedCell.col = parseInt(split_row_col[1]) - 1;
    }

    selectedCell.className = 'hover';

    this.onChange(selectedCell);
    this.hideActive();
  };

  onChange = selectedCell => {
    if (this.props.onChange) {
      this.props.onChange(selectedCell);
    }
  };

  render() {
    const menu = (
      <Menu style={{ minWidth: 130 }}>
        {MENU_LIST.map(item => (
          <Menu.Item
            key={item.text}
            onClick={() => this.handleSelect(item.text)}
          >
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <span>{item.text}</span>
              <Icon style={{ opacity: 0.5 }} name={item.icon} />
            </div>
          </Menu.Item>
        ))}
        <SubMenu
          key="sub1"
          title={
            <Translation ns="Vindoc">
              {t => <>{t('Custom Layout')}</>}
            </Translation>
          }
          popupClassName="custom-layout-submenu"
        >
          <Menu.Item key="custom-layout" onClick={() => this.hideActive()}>
            <LayoutChooser
              visible={true}
              onChange={this.onChange}
              selectedCell={this.props.selectedCell}
            />
          </Menu.Item>
        </SubMenu>
      </Menu>
    );

    return (
      <Dropdown
        overlay={menu}
        trigger={['click']}
        onVisibleChange={visible =>
          this.setState({
            dropdownVisible: visible,
          })
        }
        overlayClassName="dropdown-select-layout"
      >
        <div className="btn-group toolbar-button">
          <ToolbarButton
            isActive={this.state.dropdownVisible}
            label={'Layout'}
            icon="multiViewer"
          />
        </div>
      </Dropdown>
    );
  }
}

export default LayoutButton;
