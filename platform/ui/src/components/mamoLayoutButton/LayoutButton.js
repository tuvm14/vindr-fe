import React, { PureComponent } from 'react';
import { Menu, Dropdown } from 'antd';
import PropTypes from 'prop-types';
import ToolbarButton from '../../viewer/VindocToolbarButton';
import OHIF from '@tuvm/core';
import get from 'lodash/get';
import './LayoutButton.styl';

const MENU_LIST = ['CC', 'MLO', 'CC/MLO'];

export class LayoutButton extends PureComponent {
  static defaultProps = {
    dropdownVisible: false,
  };

  static propTypes = {
    dropdownVisible: PropTypes.bool.isRequired,
    /** Called with the selectedCell number when grid sell is selected */
    onChange: PropTypes.func,
    /** The cell to show as selected */
    selectedCell: PropTypes.object,
  };

  state = {
    dropdownVisible: this.props.dropdownVisible,
  };

  hideActive() {
    this.setState({
      dropdownVisible: false,
    });
  }

  handleSelect = item => {
    let selectedCell = {};

    if (item == 'CC') {
      selectedCell.position = 'CC';
      selectedCell.row = 1;
      selectedCell.col = 2;
    } else if (item == 'MLO') {
      selectedCell.position = 'MLO';
      selectedCell.row = 1;
      selectedCell.col = 2;
    } else if (item == 'CC/MLO') {
      selectedCell.position = 'CC/MLO';
      selectedCell.row = 2;
      selectedCell.col = 2;
    } else {
      const split_row_col = item.split('x');
      selectedCell.row = parseInt(split_row_col[0]) - 1;
      selectedCell.col = parseInt(split_row_col[1]) - 1;
    }

    selectedCell.className = 'hover';

    this.onChange(selectedCell);
    this.hideActive();
  };

  onChange = selectedCell => {
    if (this.props.onChange) {
      this.props.onChange(selectedCell);
    }
  };

  render() {
    const menu = (
      <Menu style={{ minWidth: 130 }}>
        {MENU_LIST.map(item => (
          <Menu.Item key={item} onClick={() => this.handleSelect(item)}>
            {item}
          </Menu.Item>
        ))}
      </Menu>
    );

    const store = window.store.getState();
    const currentViewportSpecificData = get(
      store,
      `viewports.viewportSpecificData[0]`
    );

    const currentSeriesInstanceUID = get(
      currentViewportSpecificData,
      'SeriesInstanceUID'
    );

    const StudyInstanceUID = get(
      currentViewportSpecificData,
      'StudyInstanceUID'
    );

    const { studyMetadataManager } = OHIF.utils;
    const study = studyMetadataManager.get(StudyInstanceUID);

    const _all_series = get(study, '_series');
    if (!_all_series) return null;
    const series_filter = _all_series.find(
      s => s.seriesInstanceUID == currentSeriesInstanceUID
    );

    const instances = get(series_filter, '_instances', []);
    let modalities = [];

    let instances_tags = instances.map(instance => {
      const metadata = get(instance, '_data.metadata');
      modalities.push(get(metadata, 'Modality'));
    });

    modalities = Array.from(new Set(modalities));
    if (!modalities.includes('MG')) {
      return null;
    }

    return (
      <Dropdown
        overlay={menu}
        trigger={['click']}
        onVisibleChange={visible =>
          this.setState({
            dropdownVisible: visible,
          })
        }
        overlayClassName="dropdown-select-layout"
      >
        <div className="btn-group">
          <ToolbarButton
            isActive={this.state.dropdownVisible}
            label="Hanging Protocol"
            icon="hangingProtocol"
          />
        </div>
      </Dropdown>
    );
  }
}

export default LayoutButton;
