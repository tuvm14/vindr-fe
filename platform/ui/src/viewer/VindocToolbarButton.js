import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';
import { Tooltip } from '@tuvm/ui';
import { useTranslation } from 'react-i18next';

import { Icon } from './../elements/Icon';

import './VinDrToolbarButton.styl';

function ToolbarButton(props) {
  const {
    isActive,
    icon,
    labelWhenActive,
    onClick,
    hotkeys = [],
    mobileOrder,
  } = props;
  const { t } = useTranslation('Vindoc');
  const className = classnames(props.className, { active: isActive });
  const iconProps = typeof icon === 'string' ? { name: icon } : icon;
  const label = isActive && labelWhenActive ? labelWhenActive : props.label;

  const arrowIconName = props.isExpanded ? 'caret-up' : 'caret-down';
  const arrowIcon = props.isExpandable && (
    <Icon name={arrowIconName} className="expand-caret" />
  );

  const handleClick = event => {
    if (onClick) {
      onClick(event, props);
    }
  };

  const cypressSelectorId = props.label.toLowerCase();
  const hotkeyString = hotkeys.length
    ? ` (${hotkeys.map(key => key.toUpperCase()).join('+')})`
    : '';
  return (
    <>
      <Tooltip
        title={t(label) + hotkeyString}
        placement="bottom"
        overlayClassName="toolbar-tooltip"
      >
        <div
          className={`${className} mobile-order-${mobileOrder || 100}`}
          onClick={handleClick}
          data-cy={cypressSelectorId}
        >
          {iconProps && <Icon {...iconProps} />}
          {arrowIcon}
          <div className="toolbar-label">{t(label)}</div>
        </div>
      </Tooltip>
      {props.divider && <div className="toolbar-divider"></div>}
    </>
  );
}

ToolbarButton.propTypes = {
  id: PropTypes.string,
  isActive: PropTypes.bool,
  label: PropTypes.string.isRequired,
  labelWhenActive: PropTypes.string,
  className: PropTypes.string.isRequired,
  icon: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      name: PropTypes.string.isRequired,
    }),
  ]),
  onClick: PropTypes.func,
  isExpandable: PropTypes.bool,
  isExpanded: PropTypes.bool,
  t: PropTypes.func,
  hotkeys: PropTypes.any,
  divider: PropTypes.bool,
};

ToolbarButton.defaultProps = {
  isActive: false,
  className: 'toolbar-button',
};

export default React.memo(ToolbarButton);
