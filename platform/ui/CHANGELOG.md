# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 2.0.0 (2022-07-14)


### Bug Fixes

* [#1075](https://github.com/OHIF/Viewers/issues/1075) Returning to the Study List before all series have finishe… ([#1090](https://github.com/OHIF/Viewers/issues/1090)) ([ecaf578](https://github.com/OHIF/Viewers/commit/ecaf578f92dc40294cec7ff9b272fb432dec4125))
* 🎸 switch ohif logo from text + font to SVG ([#1021](https://github.com/OHIF/Viewers/issues/1021)) ([e7de8be](https://github.com/OHIF/Viewers/commit/e7de8be2d8164ca5b9ea6299d7d39bcd9790164e))
* 🐛 Fix drag-n-drop of local files into OHIF ([#1319](https://github.com/OHIF/Viewers/issues/1319)) ([23305ce](https://github.com/OHIF/Viewers/commit/23305cec9c0f514e73a8dd17f984ffc87ad8d131)), closes [#1307](https://github.com/OHIF/Viewers/issues/1307)
* 🐛 Fix ghost shadow on thumb ([#1113](https://github.com/OHIF/Viewers/issues/1113)) ([caaa032](https://github.com/OHIF/Viewers/commit/caaa032c4bc24fd69fdb01a15a8feb2721c321db))
* 🐛 Limit image download size to avoid browser issues ([#1112](https://github.com/OHIF/Viewers/issues/1112)) ([5716b71](https://github.com/OHIF/Viewers/commit/5716b71d409ee1c6f13393c8cb7f50222415e198)), closes [#1099](https://github.com/OHIF/Viewers/issues/1099)
* 🐛 merge conflict ([fcbb5dc](https://github.com/OHIF/Viewers/commit/fcbb5dcf547c803ecee196f2334f7f9dac4bfeb9))
* 🐛 Minor issues measurement panel related to description ([#1142](https://github.com/OHIF/Viewers/issues/1142)) ([681384b](https://github.com/OHIF/Viewers/commit/681384b7425c83b02a0ed83371ca92d78ca7838c))
* 🐛 Prevent next pagination while end of list ([b29182f](https://github.com/OHIF/Viewers/commit/b29182fcd2be281e6b3fd160abe8b8c37582f689))
* 🐛 Proper error handling for derived display sets ([#1708](https://github.com/OHIF/Viewers/issues/1708)) ([5b20d8f](https://github.com/OHIF/Viewers/commit/5b20d8f323e4b3ef9988f2f2ab672d697b6da409))
* 🐛 Set series into active viewport by clicking on thumbnail ([#945](https://github.com/OHIF/Viewers/issues/945)) ([5551f81](https://github.com/OHIF/Viewers/commit/5551f817e346a8db0795039fe3574ab0d44a3a23)), closes [#895](https://github.com/OHIF/Viewers/issues/895) [#895](https://github.com/OHIF/Viewers/issues/895)
* Address issues with touch devices and drag/drop causing crashes ([#982](https://github.com/OHIF/Viewers/issues/982)) ([cf40a83](https://github.com/OHIF/Viewers/commit/cf40a8388cc4dbf72da41f9249a1fa79feb57933))
* allow empty values for dimensions ([#1295](https://github.com/OHIF/Viewers/issues/1295)) ([cd2da34](https://github.com/OHIF/Viewers/commit/cd2da349e5212cccdd8e65ffa3f7fdc6bad1057c))
* application crash if patientName is an object ([#1138](https://github.com/OHIF/Viewers/issues/1138)) ([64cf3b3](https://github.com/OHIF/Viewers/commit/64cf3b324da2383a927af1df2d46db2fca5318aa))
* Combined Hotkeys for special characters ([#1233](https://github.com/OHIF/Viewers/issues/1233)) ([2f30e7a](https://github.com/OHIF/Viewers/commit/2f30e7a821a238144c49c56f37d8e5565540b4bd))
* download tool fixes & improvements ([#1235](https://github.com/OHIF/Viewers/issues/1235)) ([b9574b6](https://github.com/OHIF/Viewers/commit/b9574b6efcfeb85cde35b5cae63282f8e1b35be6))
* Fix display issues with incorrect thumbnails. Change ImageThumb to functional component. ([#1148](https://github.com/OHIF/Viewers/issues/1148)) ([d70eae3](https://github.com/OHIF/Viewers/commit/d70eae3eb04fe854464f3e62316df8869bba6f11))
* Issue branch from danny experimental changes pr 1128 ([#1150](https://github.com/OHIF/Viewers/issues/1150)) ([a870b3c](https://github.com/OHIF/Viewers/commit/a870b3cc6056cf824af422e46f1ad674910b534e)), closes [#1161](https://github.com/OHIF/Viewers/issues/1161) [#1164](https://github.com/OHIF/Viewers/issues/1164) [#1177](https://github.com/OHIF/Viewers/issues/1177) [#1179](https://github.com/OHIF/Viewers/issues/1179) [#1180](https://github.com/OHIF/Viewers/issues/1180) [#1181](https://github.com/OHIF/Viewers/issues/1181) [#1182](https://github.com/OHIF/Viewers/issues/1182) [#1183](https://github.com/OHIF/Viewers/issues/1183) [#1184](https://github.com/OHIF/Viewers/issues/1184) [#1185](https://github.com/OHIF/Viewers/issues/1185)
* measurements panel css and delete button visibility ([#1352](https://github.com/OHIF/Viewers/issues/1352)) ([7ab0bbb](https://github.com/OHIF/Viewers/commit/7ab0bbb32581dcba16ee16b49b92406e2856ac76))
* measurementsAPI issue caused by production build ([#842](https://github.com/OHIF/Viewers/issues/842)) ([49d3439](https://github.com/OHIF/Viewers/commit/49d343941e236e442114e59861a373c7edb0b1fb))
* minor date picker UX improvements ([813ee5e](https://github.com/OHIF/Viewers/commit/813ee5ed4d78b7bda234922d5f3389efe346451c))
* MIP styling ([#1109](https://github.com/OHIF/Viewers/issues/1109)) ([0d21cc6](https://github.com/OHIF/Viewers/commit/0d21cc6ad0c47706b9e62e05fe2a0f1d86339760))
* Move Series Information to Separate Row ([#990](https://github.com/OHIF/Viewers/issues/990)) ([458d310](https://github.com/OHIF/Viewers/commit/458d310c78d6d8d1f4b211f55c4f5ea2c0ceb33b))
* prevent the native context menu from appearing when right-clicking on a measurement or angle (https://github.com/OHIF/Viewers/issues/1406) ([#1469](https://github.com/OHIF/Viewers/issues/1469)) ([9b3be9b](https://github.com/OHIF/Viewers/commit/9b3be9b0c082c9a5b62f2a40f42e59381860fe73))
* Remove Eraser and ROI Window ([6c950a9](https://github.com/OHIF/Viewers/commit/6c950a9669f7fbf3c46e48679fa26ee514824156))
* rendering delete button when text is too wide for parent div ([#1526](https://github.com/OHIF/Viewers/issues/1526)) ([b269415](https://github.com/OHIF/Viewers/commit/b269415048dfec50e2abb3dd4f4355a23d6ad75a))
* Switch DICOMFileUploader to use the UIModalService ([#1904](https://github.com/OHIF/Viewers/issues/1904)) ([7772fee](https://github.com/OHIF/Viewers/commit/7772fee21ae6a65994e1251e2f1d2554b47781be))
* translations ([#1234](https://github.com/OHIF/Viewers/issues/1234)) ([30b9e44](https://github.com/OHIF/Viewers/commit/30b9e4422073557287ef26a80b38eeb3f3fcff4c))
* User Preferences Issues ([#1207](https://github.com/OHIF/Viewers/issues/1207)) ([1df21a9](https://github.com/OHIF/Viewers/commit/1df21a9e075b5e6dfc10a429ae825826f46c71b8)), closes [#1161](https://github.com/OHIF/Viewers/issues/1161) [#1164](https://github.com/OHIF/Viewers/issues/1164) [#1177](https://github.com/OHIF/Viewers/issues/1177) [#1179](https://github.com/OHIF/Viewers/issues/1179) [#1180](https://github.com/OHIF/Viewers/issues/1180) [#1181](https://github.com/OHIF/Viewers/issues/1181) [#1182](https://github.com/OHIF/Viewers/issues/1182) [#1183](https://github.com/OHIF/Viewers/issues/1183) [#1184](https://github.com/OHIF/Viewers/issues/1184) [#1185](https://github.com/OHIF/Viewers/issues/1185)
* **StudyList:** camel case colSpan ([#1123](https://github.com/OHIF/Viewers/issues/1123)) ([0d498ba](https://github.com/OHIF/Viewers/commit/0d498ba17ddde8d8f0c51d742770e1574041eec0))
* Use HTML5Backend for drag-drop if the device does not support touch ([#927](https://github.com/OHIF/Viewers/issues/927)) ([6fdac4d](https://github.com/OHIF/Viewers/commit/6fdac4d6056b95b0b2346aac108d28c93256e43a))


### Features

* [#1342](https://github.com/OHIF/Viewers/issues/1342) - Window level tab ([#1429](https://github.com/OHIF/Viewers/issues/1429)) ([ebc01a8](https://github.com/OHIF/Viewers/commit/ebc01a8ca238d5a3437b44d81f75aa8a5e8d0574))
* 🎸 1729 - error boundary wrapper ([#1764](https://github.com/OHIF/Viewers/issues/1764)) ([c02b232](https://github.com/OHIF/Viewers/commit/c02b232b0cc24f38af5d5e3831d987d048e60ada))
* 🎸 add ai icon ([17b747c](https://github.com/OHIF/Viewers/commit/17b747c834d0d37c4e9989caf8b6b1d16098c608))
* 🎸 add ant design ([235ff05](https://github.com/OHIF/Viewers/commit/235ff05eb7045b1f9e35fa3f2afcc4304715aa98))
* 🎸 add collapse button ([c7037a6](https://github.com/OHIF/Viewers/commit/c7037a697a5572e46f241ff29ed913082b661594))
* 🎸 add CommonSearch component ([abeb99e](https://github.com/OHIF/Viewers/commit/abeb99e0bbd06805eb633e7dc4c5e9f169ccff7f))
* 🎸 add function delete all box ([8ea8beb](https://github.com/OHIF/Viewers/commit/8ea8beb1f7d6b66e239941a588dbc2af5095426e))
* 🎸 add function draw segment ([1b81dc8](https://github.com/OHIF/Viewers/commit/1b81dc8356caf4cf1fa81f55dbeaa1506b934deb))
* 🎸 add hotkey on toolbar button ([e7268e7](https://github.com/OHIF/Viewers/commit/e7268e73ce954c68051d79349ce80355e265fb13))
* 🎸 add menu list and collapse action ([2046823](https://github.com/OHIF/Viewers/commit/2046823379579be5e0f9cff18ab5b3741979ca95))
* 🎸 add new button icons ([04cb5d9](https://github.com/OHIF/Viewers/commit/04cb5d951551f1bc53f583335426d48b9cbfe170))
* 🎸 add option to query for all modaltities ([aa96087](https://github.com/OHIF/Viewers/commit/aa96087e51a9efd1d1684deb3ec5bbfe5a5185ec))
* 🎸 add scroll download image popup ([cb7cc65](https://github.com/OHIF/Viewers/commit/cb7cc657d69137f935c6b8dbf2680a9cb424b94a))
* 🎸 add settings window level ([e81fc6c](https://github.com/OHIF/Viewers/commit/e81fc6c1db80addb8d7416bfabdef01e64779b59))
* 🎸 add toolbar button ([6388df5](https://github.com/OHIF/Viewers/commit/6388df51840e7da9ee3755d817e89ca29095c969))
* 🎸 add tooltip panel ([7035a9f](https://github.com/OHIF/Viewers/commit/7035a9f9d73aa8e9364d26ff0045a73ad3829a1f))
* 🎸 apply styling from design ([0637cc5](https://github.com/OHIF/Viewers/commit/0637cc52fe2e87f3c7d4144eb6801afc63fe35d8))
* 🎸 auto close window level setting on mouse leave ([5301cbd](https://github.com/OHIF/Viewers/commit/5301cbd28f3923aad82f28e38718cf8dca618505))
* 🎸 cancel mpr view when change series ([0157ba5](https://github.com/OHIF/Viewers/commit/0157ba5d3728fd70c5baf7847aa38e9ac935e0b4))
* 🎸 chnage predictionPanel.json to Vindoc.json ([3183f96](https://github.com/OHIF/Viewers/commit/3183f96ca9a94bb70737b362ede238d13db369da))
* 🎸 DICOM SR STOW on MeasurementAPI ([#954](https://github.com/OHIF/Viewers/issues/954)) ([ebe1af8](https://github.com/OHIF/Viewers/commit/ebe1af8d4f75d2483eba869655906d7829bd9666)), closes [#758](https://github.com/OHIF/Viewers/issues/758)
* 🎸 Load spinner when selecting gcloud store. Add key on td ([#1034](https://github.com/OHIF/Viewers/issues/1034)) ([e62f403](https://github.com/OHIF/Viewers/commit/e62f403fe9e3df56713128e3d59045824b086d8d)), closes [#1057](https://github.com/OHIF/Viewers/issues/1057)
* 🎸 move AI model to right of toolbar ([b18069c](https://github.com/OHIF/Viewers/commit/b18069c46857e833844f37092d7ec94d27a63040))
* 🎸 MPR UI improvements. Added MinIP, AvgIP, slab thickness slider and mode toggle ([#947](https://github.com/OHIF/Viewers/issues/947)) ([c79c0c3](https://github.com/OHIF/Viewers/commit/c79c0c301d003d21b095dd708e33c55033f29794))
* 🎸 New modal provider ([#1110](https://github.com/OHIF/Viewers/issues/1110)) ([5ee832b](https://github.com/OHIF/Viewers/commit/5ee832b19505a4e8e5756660ce6ed03a7f18dec3)), closes [#1086](https://github.com/OHIF/Viewers/issues/1086) [#1116](https://github.com/OHIF/Viewers/issues/1116)
* 🎸 reload page after get ai result ([9257aa0](https://github.com/OHIF/Viewers/commit/9257aa0a64f75e7eff662f60251a9827989436c2))
* 🎸 reset series info width ([22a4a65](https://github.com/OHIF/Viewers/commit/22a4a6509409a184faa455ecc9e5fb1a698fcf5c))
* 🎸 set active thumbnail ([f42d433](https://github.com/OHIF/Viewers/commit/f42d43310ed86c6a697f3d34f0ca1ad6a799e11d))
* 🎸 set scrollbar width ([03e3bec](https://github.com/OHIF/Viewers/commit/03e3bec4b2fd864eb1d5bc932f6aa4f3a9c4df63))
* 🎸 set series description ([0e5e009](https://github.com/OHIF/Viewers/commit/0e5e0090e01386f3c719910103e75b061531288b))
* 🎸 show active series ([c0027b3](https://github.com/OHIF/Viewers/commit/c0027b3487aa7d5e7e35869a753ae7601814cef2))
* 🎸 show active series ([6954754](https://github.com/OHIF/Viewers/commit/6954754c0e7f95ffe3b2a16036aba1a1f51d6e48))
* 🎸 show default avatar icon ([c2797fb](https://github.com/OHIF/Viewers/commit/c2797fbfb8df81b82de9efaec45015057b5f46f4))
* 🎸 show series description ([f24925d](https://github.com/OHIF/Viewers/commit/f24925d4a87ffc72038be3875284935c38a75df3))
* 🎸 show suffix ([7d0dcdc](https://github.com/OHIF/Viewers/commit/7d0dcdcd6c0e3308a7a9bb4dd3f323484fb6f72c))
* 🎸 StudyList able to list all studyies ([864f22e](https://github.com/OHIF/Viewers/commit/864f22e71eb010a691f1489f693963dab81607bf))
* 🎸 style background studybrowser ([e49ccb3](https://github.com/OHIF/Viewers/commit/e49ccb353bc3e51ad1da80467d3c6568eea16232))
* 🎸 styling vtk select ([1cbb673](https://github.com/OHIF/Viewers/commit/1cbb673e9fec8e45a7f7e77f019b21033c03aee2))
* 🎸 trim value data ([4d73eda](https://github.com/OHIF/Viewers/commit/4d73edaba379f2a03b2e53a4c04f5a675572d7d4))
* 🎸 update api with workgroup ([427b242](https://github.com/OHIF/Viewers/commit/427b242b7d5cd1f887d88974d0496cfa3e0e6493))
* 🎸 Update hotkeys and user preferences modal ([#1135](https://github.com/OHIF/Viewers/issues/1135)) ([e62f5f8](https://github.com/OHIF/Viewers/commit/e62f5f8dd28ab363f23671cd21cee115abb870ff)), closes [#923](https://github.com/OHIF/Viewers/issues/923)
* 🎸 update load mammo series ([de62355](https://github.com/OHIF/Viewers/commit/de62355337a61cf9241edc1967a803874ba83e1d))
* 🎸 update localization ai button ([2d35ba0](https://github.com/OHIF/Viewers/commit/2d35ba04aa7e87c0c73ac0a0c2ea5be670d4bd2e))
* 🎸 update open panel button ([dfb8cf4](https://github.com/OHIF/Viewers/commit/dfb8cf4a0c6bb08629e8fca5dd92817cb19ca8d4))
* 🎸 update panel icon ([b3ff8db](https://github.com/OHIF/Viewers/commit/b3ff8db2f39a1de0ea185fe5c200d6106ca77444))
* 🎸 update series number ([6a8ee4e](https://github.com/OHIF/Viewers/commit/6a8ee4edd18f2e59229cb69c19f809779d095648))
* 🎸 update tag list popup ([db2734b](https://github.com/OHIF/Viewers/commit/db2734b4fe75b58e4935b79022a5cf12e1fe7a7d))
* 🎸 yarn cm ([a8a4204](https://github.com/OHIF/Viewers/commit/a8a42044082ddbf1a2177165c7f5394e4668bd8f))
* Add browser info and app version ([#1046](https://github.com/OHIF/Viewers/issues/1046)) ([c217b8b](https://github.com/OHIF/Viewers/commit/c217b8bbdefb7f40c24d460856ec7f4523d70b8a))
* Add new annotate tool using new dialog service ([#1211](https://github.com/OHIF/Viewers/issues/1211)) ([8fd3af1](https://github.com/OHIF/Viewers/commit/8fd3af1e137e793f1b482760a22591c64a072047))
* add vindoc icons ([cdcbf48](https://github.com/OHIF/Viewers/commit/cdcbf4846cbf28e55b07f4a2e5a6dd89bce6e2ff))
* expose UiNotifications service ([#1172](https://github.com/OHIF/Viewers/issues/1172)) ([5c04e34](https://github.com/OHIF/Viewers/commit/5c04e34c8fb2394ab7acd9eb4f2ab12afeb2f255))
* Issue 879 viewer route query param not filtering but promoting ([#1141](https://github.com/OHIF/Viewers/issues/1141)) ([b17f753](https://github.com/OHIF/Viewers/commit/b17f753e6222045252ef885e40233681541a32e1)), closes [#1118](https://github.com/OHIF/Viewers/issues/1118)
* Lesion tracker right panel ([#1428](https://github.com/OHIF/Viewers/issues/1428)) ([98a649b](https://github.com/OHIF/Viewers/commit/98a649b455ffc712938fc5035cdef40695e58440))
* modal provider ([#1151](https://github.com/OHIF/Viewers/issues/1151)) ([75d88bc](https://github.com/OHIF/Viewers/commit/75d88bc454710d2dcdbc7d68c4d9df041159c840)), closes [#1086](https://github.com/OHIF/Viewers/issues/1086) [#1116](https://github.com/OHIF/Viewers/issues/1116) [#1116](https://github.com/OHIF/Viewers/issues/1116) [#1146](https://github.com/OHIF/Viewers/issues/1146) [#1142](https://github.com/OHIF/Viewers/issues/1142) [#1143](https://github.com/OHIF/Viewers/issues/1143) [#1110](https://github.com/OHIF/Viewers/issues/1110) [#1086](https://github.com/OHIF/Viewers/issues/1086) [#1116](https://github.com/OHIF/Viewers/issues/1116) [#1119](https://github.com/OHIF/Viewers/issues/1119)
* New dialog service ([#1202](https://github.com/OHIF/Viewers/issues/1202)) ([f65639c](https://github.com/OHIF/Viewers/commit/f65639c2b0dab01decd20cab2cef4263cb4fab37))
* Notification Service ([#1011](https://github.com/OHIF/Viewers/issues/1011)) ([92c8996](https://github.com/OHIF/Viewers/commit/92c8996ed49107fc555203bd673544cf2c87b6f8))
* responsive study list ([#1068](https://github.com/OHIF/Viewers/issues/1068)) ([2cdef4b](https://github.com/OHIF/Viewers/commit/2cdef4b9844cc2ce61e9ce76b5a942ba7051fe16))
* Segmentations Settings UI - Phase 1 [#1391](https://github.com/OHIF/Viewers/issues/1391) ([#1392](https://github.com/OHIF/Viewers/issues/1392)) ([e8842cf](https://github.com/OHIF/Viewers/commit/e8842cf8aebde98db7fc123e4867c8288552331f)), closes [#1423](https://github.com/OHIF/Viewers/issues/1423)
* Snapshot Download Tool ([#840](https://github.com/OHIF/Viewers/issues/840)) ([450e098](https://github.com/OHIF/Viewers/commit/450e0981a5ba054fcfcb85eeaeb18371af9088f8))
* Use QIDO + WADO to load series metadata individually rather than the entire study metadata at once ([#953](https://github.com/OHIF/Viewers/issues/953)) ([9e10c2b](https://github.com/OHIF/Viewers/commit/9e10c2b2ded5dcf5c05779ca9ef9a7fc453750ad))
* **EraserTool:** add eraserTool to @ohif/extension-cornerstone ([#912](https://github.com/OHIF/Viewers/issues/912)) ([698d274](https://github.com/OHIF/Viewers/commit/698d274c64e9cf92ccd35af7fc78c90c54a62fc9))


* feat!: Ability to configure cornerstone tools via extension configuration (#1229) ([55a5806](https://github.com/OHIF/Viewers/commit/55a580659ecb74ca6433461d8f9a05c2a2b69533)), closes [#1229](https://github.com/OHIF/Viewers/issues/1229)


### BREAKING CHANGES

* modifies the exposed react <App /> components props. The contract for providing configuration for the app has changed. Please reference updated documentation for guidance.





## [1.5.6](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.5.5...@ohif/ui@1.5.6) (2020-10-06)

**Note:** Version bump only for package @ohif/ui





## [1.5.5](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.5.4...@ohif/ui@1.5.5) (2020-09-17)

**Note:** Version bump only for package @ohif/ui





## [1.5.4](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.5.3...@ohif/ui@1.5.4) (2020-09-10)

**Note:** Version bump only for package @ohif/ui





## [1.5.3](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.5.2...@ohif/ui@1.5.3) (2020-07-23)

**Note:** Version bump only for package @ohif/ui





## [1.5.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.5.1...@ohif/ui@1.5.2) (2020-07-22)


### Bug Fixes

* Switch DICOMFileUploader to use the UIModalService ([#1904](https://github.com/OHIF/Viewers/issues/1904)) ([7772fee](https://github.com/OHIF/Viewers/commit/7772fee21ae6a65994e1251e2f1d2554b47781be))





## [1.5.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.5.0...@ohif/ui@1.5.1) (2020-06-05)

**Note:** Version bump only for package @ohif/ui





# [1.5.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.4.4...@ohif/ui@1.5.0) (2020-06-04)


### Features

* 🎸 1729 - error boundary wrapper ([#1764](https://github.com/OHIF/Viewers/issues/1764)) ([c02b232](https://github.com/OHIF/Viewers/commit/c02b232b0cc24f38af5d5e3831d987d048e60ada))





## [1.4.4](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.4.3...@ohif/ui@1.4.4) (2020-05-04)


### Bug Fixes

* 🐛 Proper error handling for derived display sets ([#1708](https://github.com/OHIF/Viewers/issues/1708)) ([5b20d8f](https://github.com/OHIF/Viewers/commit/5b20d8f323e4b3ef9988f2f2ab672d697b6da409))





## [1.4.3](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.4.2...@ohif/ui@1.4.3) (2020-04-09)

**Note:** Version bump only for package @ohif/ui





# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.4.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.4.1...@ohif/ui@1.4.2) (2020-04-06)

**Note:** Version bump only for package @ohif/ui





## [1.4.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.4.0...@ohif/ui@1.4.1) (2020-03-17)


### Bug Fixes

* rendering delete button when text is too wide for parent div ([#1526](https://github.com/OHIF/Viewers/issues/1526)) ([b269415](https://github.com/OHIF/Viewers/commit/b269415048dfec50e2abb3dd4f4355a23d6ad75a))





# [1.4.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.3.3...@ohif/ui@1.4.0) (2020-03-13)


### Features

* Segmentations Settings UI - Phase 1 [#1391](https://github.com/OHIF/Viewers/issues/1391) ([#1392](https://github.com/OHIF/Viewers/issues/1392)) ([e8842cf](https://github.com/OHIF/Viewers/commit/e8842cf8aebde98db7fc123e4867c8288552331f)), closes [#1423](https://github.com/OHIF/Viewers/issues/1423)





# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.3.3](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.3.2...@ohif/ui@1.3.3) (2020-03-09)

**Note:** Version bump only for package @ohif/ui





## [1.3.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.3.1...@ohif/ui@1.3.2) (2020-03-09)


### Bug Fixes

* Remove Eraser and ROI Window ([6c950a9](https://github.com/OHIF/Viewers/commit/6c950a9669f7fbf3c46e48679fa26ee514824156))





## [1.3.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.3.0...@ohif/ui@1.3.1) (2020-02-29)


### Bug Fixes

* prevent the native context menu from appearing when right-clicking on a measurement or angle (https://github.com/OHIF/Viewers/issues/1406) ([#1469](https://github.com/OHIF/Viewers/issues/1469)) ([9b3be9b](https://github.com/OHIF/Viewers/commit/9b3be9b0c082c9a5b62f2a40f42e59381860fe73))





# [1.3.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.2.1...@ohif/ui@1.3.0) (2020-02-20)


### Features

* [#1342](https://github.com/OHIF/Viewers/issues/1342) - Window level tab ([#1429](https://github.com/OHIF/Viewers/issues/1429)) ([ebc01a8](https://github.com/OHIF/Viewers/commit/ebc01a8ca238d5a3437b44d81f75aa8a5e8d0574))





## [1.2.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.2.0...@ohif/ui@1.2.1) (2020-02-12)


### Bug Fixes

* Combined Hotkeys for special characters ([#1233](https://github.com/OHIF/Viewers/issues/1233)) ([2f30e7a](https://github.com/OHIF/Viewers/commit/2f30e7a821a238144c49c56f37d8e5565540b4bd))





# [1.2.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.1.9...@ohif/ui@1.2.0) (2020-02-10)


### Features

* Lesion tracker right panel ([#1428](https://github.com/OHIF/Viewers/issues/1428)) ([98a649b](https://github.com/OHIF/Viewers/commit/98a649b455ffc712938fc5035cdef40695e58440))





## [1.1.9](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.1.8...@ohif/ui@1.1.9) (2020-01-30)


### Bug Fixes

* download tool fixes & improvements ([#1235](https://github.com/OHIF/Viewers/issues/1235)) ([b9574b6](https://github.com/OHIF/Viewers/commit/b9574b6efcfeb85cde35b5cae63282f8e1b35be6))





## [1.1.8](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.1.7...@ohif/ui@1.1.8) (2020-01-08)


### Bug Fixes

* measurements panel css and delete button visibility ([#1352](https://github.com/OHIF/Viewers/issues/1352)) ([7ab0bbb](https://github.com/OHIF/Viewers/commit/7ab0bbb32581dcba16ee16b49b92406e2856ac76))





## [1.1.7](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.1.6...@ohif/ui@1.1.7) (2019-12-20)

**Note:** Version bump only for package @ohif/ui





## [1.1.6](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.1.5...@ohif/ui@1.1.6) (2019-12-20)

**Note:** Version bump only for package @ohif/ui





## [1.1.5](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.1.4...@ohif/ui@1.1.5) (2019-12-19)


### Bug Fixes

* 🐛 Fix drag-n-drop of local files into OHIF ([#1319](https://github.com/OHIF/Viewers/issues/1319)) ([23305ce](https://github.com/OHIF/Viewers/commit/23305cec9c0f514e73a8dd17f984ffc87ad8d131)), closes [#1307](https://github.com/OHIF/Viewers/issues/1307)





## [1.1.4](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.1.3...@ohif/ui@1.1.4) (2019-12-16)

**Note:** Version bump only for package @ohif/ui





## [1.1.3](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.1.2...@ohif/ui@1.1.3) (2019-12-13)


### Bug Fixes

* allow empty values for dimensions ([#1295](https://github.com/OHIF/Viewers/issues/1295)) ([cd2da34](https://github.com/OHIF/Viewers/commit/cd2da349e5212cccdd8e65ffa3f7fdc6bad1057c))





## [1.1.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.1.1...@ohif/ui@1.1.2) (2019-12-12)


### Bug Fixes

* translations ([#1234](https://github.com/OHIF/Viewers/issues/1234)) ([30b9e44](https://github.com/OHIF/Viewers/commit/30b9e4422073557287ef26a80b38eeb3f3fcff4c))





## [1.1.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.1.0...@ohif/ui@1.1.1) (2019-12-11)

**Note:** Version bump only for package @ohif/ui





# [1.1.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.0.1...@ohif/ui@1.1.0) (2019-12-11)


### Features

* 🎸 DICOM SR STOW on MeasurementAPI ([#954](https://github.com/OHIF/Viewers/issues/954)) ([ebe1af8](https://github.com/OHIF/Viewers/commit/ebe1af8d4f75d2483eba869655906d7829bd9666)), closes [#758](https://github.com/OHIF/Viewers/issues/758)





## [1.0.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@1.0.0...@ohif/ui@1.0.1) (2019-12-09)

**Note:** Version bump only for package @ohif/ui





# [1.0.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.65.4...@ohif/ui@1.0.0) (2019-12-09)


* feat!: Ability to configure cornerstone tools via extension configuration (#1229) ([55a5806](https://github.com/OHIF/Viewers/commit/55a580659ecb74ca6433461d8f9a05c2a2b69533)), closes [#1229](https://github.com/OHIF/Viewers/issues/1229)


### BREAKING CHANGES

* modifies the exposed react <App /> components props. The contract for providing configuration for the app has changed. Please reference updated documentation for guidance.





## [0.65.4](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.65.3...@ohif/ui@0.65.4) (2019-12-07)

**Note:** Version bump only for package @ohif/ui





## [0.65.3](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.65.2...@ohif/ui@0.65.3) (2019-12-07)

**Note:** Version bump only for package @ohif/ui





## [0.65.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.65.1...@ohif/ui@0.65.2) (2019-12-07)

**Note:** Version bump only for package @ohif/ui





## [0.65.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.65.0...@ohif/ui@0.65.1) (2019-11-28)


### Bug Fixes

* User Preferences Issues ([#1207](https://github.com/OHIF/Viewers/issues/1207)) ([1df21a9](https://github.com/OHIF/Viewers/commit/1df21a9e075b5e6dfc10a429ae825826f46c71b8)), closes [#1161](https://github.com/OHIF/Viewers/issues/1161) [#1164](https://github.com/OHIF/Viewers/issues/1164) [#1177](https://github.com/OHIF/Viewers/issues/1177) [#1179](https://github.com/OHIF/Viewers/issues/1179) [#1180](https://github.com/OHIF/Viewers/issues/1180) [#1181](https://github.com/OHIF/Viewers/issues/1181) [#1182](https://github.com/OHIF/Viewers/issues/1182) [#1183](https://github.com/OHIF/Viewers/issues/1183) [#1184](https://github.com/OHIF/Viewers/issues/1184) [#1185](https://github.com/OHIF/Viewers/issues/1185)





# [0.65.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.64.2...@ohif/ui@0.65.0) (2019-11-25)


### Features

* Add new annotate tool using new dialog service ([#1211](https://github.com/OHIF/Viewers/issues/1211)) ([8fd3af1](https://github.com/OHIF/Viewers/commit/8fd3af1e137e793f1b482760a22591c64a072047))





## [0.64.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.64.1...@ohif/ui@0.64.2) (2019-11-25)


### Bug Fixes

* Issue branch from danny experimental changes pr 1128 ([#1150](https://github.com/OHIF/Viewers/issues/1150)) ([a870b3c](https://github.com/OHIF/Viewers/commit/a870b3cc6056cf824af422e46f1ad674910b534e)), closes [#1161](https://github.com/OHIF/Viewers/issues/1161) [#1164](https://github.com/OHIF/Viewers/issues/1164) [#1177](https://github.com/OHIF/Viewers/issues/1177) [#1179](https://github.com/OHIF/Viewers/issues/1179) [#1180](https://github.com/OHIF/Viewers/issues/1180) [#1181](https://github.com/OHIF/Viewers/issues/1181) [#1182](https://github.com/OHIF/Viewers/issues/1182) [#1183](https://github.com/OHIF/Viewers/issues/1183) [#1184](https://github.com/OHIF/Viewers/issues/1184) [#1185](https://github.com/OHIF/Viewers/issues/1185)





## [0.64.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.64.0...@ohif/ui@0.64.1) (2019-11-20)

**Note:** Version bump only for package @ohif/ui





# [0.64.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.63.0...@ohif/ui@0.64.0) (2019-11-19)


### Features

* New dialog service ([#1202](https://github.com/OHIF/Viewers/issues/1202)) ([f65639c](https://github.com/OHIF/Viewers/commit/f65639c2b0dab01decd20cab2cef4263cb4fab37))





# [0.63.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.62.4...@ohif/ui@0.63.0) (2019-11-19)


### Features

* Issue 879 viewer route query param not filtering but promoting ([#1141](https://github.com/OHIF/Viewers/issues/1141)) ([b17f753](https://github.com/OHIF/Viewers/commit/b17f753e6222045252ef885e40233681541a32e1)), closes [#1118](https://github.com/OHIF/Viewers/issues/1118)





## [0.62.4](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.62.3...@ohif/ui@0.62.4) (2019-11-18)


### Bug Fixes

* minor date picker UX improvements ([813ee5e](https://github.com/OHIF/Viewers/commit/813ee5ed4d78b7bda234922d5f3389efe346451c))





## [0.62.3](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.62.2...@ohif/ui@0.62.3) (2019-11-15)

**Note:** Version bump only for package @ohif/ui





## [0.62.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.62.1...@ohif/ui@0.62.2) (2019-11-15)

**Note:** Version bump only for package @ohif/ui





## [0.62.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.62.0...@ohif/ui@0.62.1) (2019-11-14)

**Note:** Version bump only for package @ohif/ui

# [0.62.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.61.0...@ohif/ui@0.62.0) (2019-11-13)

### Features

- expose UiNotifications service
  ([#1172](https://github.com/OHIF/Viewers/issues/1172))
  ([5c04e34](https://github.com/OHIF/Viewers/commit/5c04e34c8fb2394ab7acd9eb4f2ab12afeb2f255))

# [0.61.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.60.1...@ohif/ui@0.61.0) (2019-11-12)

### Features

- 🎸 Update hotkeys and user preferences modal
  ([#1135](https://github.com/OHIF/Viewers/issues/1135))
  ([e62f5f8](https://github.com/OHIF/Viewers/commit/e62f5f8dd28ab363f23671cd21cee115abb870ff)),
  closes [#923](https://github.com/OHIF/Viewers/issues/923)

## [0.60.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.60.0...@ohif/ui@0.60.1) (2019-11-08)

### Bug Fixes

- Fix display issues with incorrect thumbnails. Change ImageThumb to functional
  component. ([#1148](https://github.com/OHIF/Viewers/issues/1148))
  ([d70eae3](https://github.com/OHIF/Viewers/commit/d70eae3eb04fe854464f3e62316df8869bba6f11))

# [0.60.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.59.1...@ohif/ui@0.60.0) (2019-11-06)

### Features

- modal provider ([#1151](https://github.com/OHIF/Viewers/issues/1151))
  ([75d88bc](https://github.com/OHIF/Viewers/commit/75d88bc454710d2dcdbc7d68c4d9df041159c840)),
  closes [#1086](https://github.com/OHIF/Viewers/issues/1086)
  [#1116](https://github.com/OHIF/Viewers/issues/1116)
  [#1116](https://github.com/OHIF/Viewers/issues/1116)
  [#1146](https://github.com/OHIF/Viewers/issues/1146)
  [#1142](https://github.com/OHIF/Viewers/issues/1142)
  [#1143](https://github.com/OHIF/Viewers/issues/1143)
  [#1110](https://github.com/OHIF/Viewers/issues/1110)
  [#1086](https://github.com/OHIF/Viewers/issues/1086)
  [#1116](https://github.com/OHIF/Viewers/issues/1116)
  [#1119](https://github.com/OHIF/Viewers/issues/1119)

## [0.59.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.59.0...@ohif/ui@0.59.1) (2019-11-05)

### Bug Fixes

- [#1075](https://github.com/OHIF/Viewers/issues/1075) Returning to the Study
  List before all series have finishe…
  ([#1090](https://github.com/OHIF/Viewers/issues/1090))
  ([ecaf578](https://github.com/OHIF/Viewers/commit/ecaf578f92dc40294cec7ff9b272fb432dec4125))

# [0.59.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.58.5...@ohif/ui@0.59.0) (2019-11-04)

### Features

- 🎸 New modal provider ([#1110](https://github.com/OHIF/Viewers/issues/1110))
  ([5ee832b](https://github.com/OHIF/Viewers/commit/5ee832b19505a4e8e5756660ce6ed03a7f18dec3)),
  closes [#1086](https://github.com/OHIF/Viewers/issues/1086)
  [#1116](https://github.com/OHIF/Viewers/issues/1116)

## [0.58.5](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.58.4...@ohif/ui@0.58.5) (2019-11-04)

### Bug Fixes

- 🐛 Minor issues measurement panel related to description
  ([#1142](https://github.com/OHIF/Viewers/issues/1142))
  ([681384b](https://github.com/OHIF/Viewers/commit/681384b7425c83b02a0ed83371ca92d78ca7838c))

## [0.58.4](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.58.3...@ohif/ui@0.58.4) (2019-10-31)

### Bug Fixes

- application crash if patientName is an object
  ([#1138](https://github.com/OHIF/Viewers/issues/1138))
  ([64cf3b3](https://github.com/OHIF/Viewers/commit/64cf3b324da2383a927af1df2d46db2fca5318aa))

## [0.58.3](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.58.2...@ohif/ui@0.58.3) (2019-10-30)

### Bug Fixes

- 🐛 Fix ghost shadow on thumb
  ([#1113](https://github.com/OHIF/Viewers/issues/1113))
  ([caaa032](https://github.com/OHIF/Viewers/commit/caaa032c4bc24fd69fdb01a15a8feb2721c321db))

## [0.58.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.58.1...@ohif/ui@0.58.2) (2019-10-29)

### Bug Fixes

- 🐛 Limit image download size to avoid browser issues
  ([#1112](https://github.com/OHIF/Viewers/issues/1112))
  ([5716b71](https://github.com/OHIF/Viewers/commit/5716b71d409ee1c6f13393c8cb7f50222415e198)),
  closes [#1099](https://github.com/OHIF/Viewers/issues/1099)

## [0.58.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.58.0...@ohif/ui@0.58.1) (2019-10-29)

### Bug Fixes

- **StudyList:** camel case colSpan
  ([#1123](https://github.com/OHIF/Viewers/issues/1123))
  ([0d498ba](https://github.com/OHIF/Viewers/commit/0d498ba17ddde8d8f0c51d742770e1574041eec0))

# [0.58.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.57.1...@ohif/ui@0.58.0) (2019-10-28)

### Features

- responsive study list ([#1068](https://github.com/OHIF/Viewers/issues/1068))
  ([2cdef4b](https://github.com/OHIF/Viewers/commit/2cdef4b9844cc2ce61e9ce76b5a942ba7051fe16))

## [0.57.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.57.0...@ohif/ui@0.57.1) (2019-10-28)

### Bug Fixes

- MIP styling ([#1109](https://github.com/OHIF/Viewers/issues/1109))
  ([0d21cc6](https://github.com/OHIF/Viewers/commit/0d21cc6ad0c47706b9e62e05fe2a0f1d86339760))

# [0.57.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.56.1...@ohif/ui@0.57.0) (2019-10-26)

### Features

- Snapshot Download Tool ([#840](https://github.com/OHIF/Viewers/issues/840))
  ([450e098](https://github.com/OHIF/Viewers/commit/450e0981a5ba054fcfcb85eeaeb18371af9088f8))

## [0.56.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.56.0...@ohif/ui@0.56.1) (2019-10-26)

**Note:** Version bump only for package @ohif/ui

# [0.56.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.55.0...@ohif/ui@0.56.0) (2019-10-22)

### Features

- 🎸 Load spinner when selecting gcloud store. Add key on td
  ([#1034](https://github.com/OHIF/Viewers/issues/1034))
  ([e62f403](https://github.com/OHIF/Viewers/commit/e62f403fe9e3df56713128e3d59045824b086d8d)),
  closes [#1057](https://github.com/OHIF/Viewers/issues/1057)

# [0.55.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.54.0...@ohif/ui@0.55.0) (2019-10-15)

### Features

- Add browser info and app version
  ([#1046](https://github.com/OHIF/Viewers/issues/1046))
  ([c217b8b](https://github.com/OHIF/Viewers/commit/c217b8b))

# [0.54.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.53.4...@ohif/ui@0.54.0) (2019-10-14)

### Features

- Notification Service ([#1011](https://github.com/OHIF/Viewers/issues/1011))
  ([92c8996](https://github.com/OHIF/Viewers/commit/92c8996))

## [0.53.4](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.53.3...@ohif/ui@0.53.4) (2019-10-11)

**Note:** Version bump only for package @ohif/ui

## [0.53.3](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.53.2...@ohif/ui@0.53.3) (2019-10-10)

### Bug Fixes

- 🎸 switch ohif logo from text + font to SVG
  ([#1021](https://github.com/OHIF/Viewers/issues/1021))
  ([e7de8be](https://github.com/OHIF/Viewers/commit/e7de8be))

## [0.53.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.53.1...@ohif/ui@0.53.2) (2019-10-09)

**Note:** Version bump only for package @ohif/ui

## [0.53.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.53.0...@ohif/ui@0.53.1) (2019-10-04)

### Bug Fixes

- Move Series Information to Separate Row
  ([#990](https://github.com/OHIF/Viewers/issues/990))
  ([458d310](https://github.com/OHIF/Viewers/commit/458d310))

# [0.53.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.52.0...@ohif/ui@0.53.0) (2019-10-03)

### Features

- Use QIDO + WADO to load series metadata individually rather than the entire
  study metadata at once ([#953](https://github.com/OHIF/Viewers/issues/953))
  ([9e10c2b](https://github.com/OHIF/Viewers/commit/9e10c2b))

# [0.52.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.51.3...@ohif/ui@0.52.0) (2019-10-01)

### Features

- 🎸 MPR UI improvements. Added MinIP, AvgIP, slab thickness slider and mode
  toggle ([#947](https://github.com/OHIF/Viewers/issues/947))
  ([c79c0c3](https://github.com/OHIF/Viewers/commit/c79c0c3))

## [0.51.3](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.51.2...@ohif/ui@0.51.3) (2019-10-01)

### Bug Fixes

- Address issues with touch devices and drag/drop causing crashes
  ([#982](https://github.com/OHIF/Viewers/issues/982))
  ([cf40a83](https://github.com/OHIF/Viewers/commit/cf40a83))

## [0.51.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.51.1...@ohif/ui@0.51.2) (2019-09-26)

### Bug Fixes

- 🐛 Set series into active viewport by clicking on thumbnail
  ([#945](https://github.com/OHIF/Viewers/issues/945))
  ([5551f81](https://github.com/OHIF/Viewers/commit/5551f81)), closes
  [#895](https://github.com/OHIF/Viewers/issues/895)
  [#895](https://github.com/OHIF/Viewers/issues/895)

## [0.51.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.51.0...@ohif/ui@0.51.1) (2019-09-19)

### Bug Fixes

- Use HTML5Backend for drag-drop if the device does not support touch
  ([#927](https://github.com/OHIF/Viewers/issues/927))
  ([6fdac4d](https://github.com/OHIF/Viewers/commit/6fdac4d))

# [0.51.0](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.50.4...@ohif/ui@0.51.0) (2019-09-12)

### Features

- **EraserTool:** add eraserTool to @ohif/extension-cornerstone
  ([#912](https://github.com/OHIF/Viewers/issues/912))
  ([698d274](https://github.com/OHIF/Viewers/commit/698d274))

## [0.50.4](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.50.3...@ohif/ui@0.50.4) (2019-09-06)

**Note:** Version bump only for package @ohif/ui

## [0.50.3](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.50.2...@ohif/ui@0.50.3) (2019-09-04)

**Note:** Version bump only for package @ohif/ui

## [0.50.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.50.1...@ohif/ui@0.50.2) (2019-09-04)

### Bug Fixes

- measurementsAPI issue caused by production build
  ([#842](https://github.com/OHIF/Viewers/issues/842))
  ([49d3439](https://github.com/OHIF/Viewers/commit/49d3439))

## [0.50.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.50.0-alpha.10...@ohif/ui@0.50.1) (2019-08-14)

**Note:** Version bump only for package @ohif/ui

# [0.50.0-alpha.10](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.2.18-alpha.9...@ohif/ui@0.50.0-alpha.10) (2019-08-14)

**Note:** Version bump only for package @ohif/ui

## [0.2.18-alpha.9](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.2.18-alpha.8...@ohif/ui@0.2.18-alpha.9) (2019-08-14)

**Note:** Version bump only for package @ohif/ui

## 0.2.18-alpha.8 (2019-08-14)

**Note:** Version bump only for package @ohif/ui

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.18-alpha.7](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.2.18-alpha.6...@ohif/ui@0.2.18-alpha.7) (2019-08-08)

**Note:** Version bump only for package @ohif/ui

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.18-alpha.6](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.2.18-alpha.5...@ohif/ui@0.2.18-alpha.6) (2019-08-08)

**Note:** Version bump only for package @ohif/ui

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.18-alpha.5](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.2.18-alpha.4...@ohif/ui@0.2.18-alpha.5) (2019-08-08)

**Note:** Version bump only for package @ohif/ui

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.18-alpha.4](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.2.18-alpha.3...@ohif/ui@0.2.18-alpha.4) (2019-08-08)

**Note:** Version bump only for package @ohif/ui

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.18-alpha.3](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.2.18-alpha.2...@ohif/ui@0.2.18-alpha.3) (2019-08-08)

**Note:** Version bump only for package @ohif/ui

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.18-alpha.2](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.2.18-alpha.1...@ohif/ui@0.2.18-alpha.2) (2019-08-07)

**Note:** Version bump only for package @ohif/ui

## [0.2.18-alpha.1](https://github.com/OHIF/Viewers/compare/@ohif/ui@0.2.18-alpha.0...@ohif/ui@0.2.18-alpha.1) (2019-08-07)

**Note:** Version bump only for package @ohif/ui

## 0.2.18-alpha.0 (2019-08-05)

**Note:** Version bump only for package @ohif/ui
