import macro from 'vtk.js/Sources/macro';
import * as vtkMath from 'vtk.js/Sources/Common/Core/Math';
import { Vector3, Line3 } from 'cornerstone-math';
import { vtkInteractorStyleMPRSlice } from '@vindr/vindr-vtkjs-viewport';
import {
  clearCanvasVtk,
  drawHandlesVtk,
  drawLengthVtk,
  computedLocalDisplayValue,
  computedWorlPosValue,
} from './drawing';
import { isEmpty } from 'lodash';

function computedSubVec(u, v) {
  let subVec = new Vector3();
  const uVec = new Vector3(...u);
  const vVec = new Vector3(...v);
  subVec.subVectors(uVec, vVec);
  return subVec;
}

function computedClosestLine(line, point) {
  let p0 = new Vector3(...point);
  let p1 = new Vector3(...line.worldPosStart);
  let p2 = new Vector3(...line.worldPosEnd);
  const line3 = new Line3(p1, p2);

  const distance = line3.closestPointToPoint(p0, true).distanceTo(p0);
  return distance || 999;
}

function computedClosestPoint(p1, p2) {
  const distance = vtkMath.distance2BetweenPoints(p1, p2);
  return distance || 999;
}

function vtkInteractorStyleLength(publicAPI, model) {
  // Set our className
  model.classHierarchy.push('vtkInteractorStyleLength');

  let isDrawing = false;
  let drawingData = {};
  let nearLineData = {};
  let isDown = false;
  let mouseStart = [];

  function renderToolData(api, context, renderer) {
    clearCanvasVtk(context);
    const toolData = api.toolsState && api.toolsState['vtkLength'];

    if (!toolData || !(toolData.data || []).length) {
      return;
    }

    for (let i = 0; i < toolData.data.length; i++) {
      const data = toolData.data[i];
      const distance = vtkMath.distance2BetweenPoints(
        data.worldPosStart,
        data.worldPosEnd
      );
      const distanceText = `${Math.sqrt(distance).toFixed(2)} mm`;

      const start = computedLocalDisplayValue(data.worldPosStart, renderer);
      const end = computedLocalDisplayValue(data.worldPosEnd, renderer);
      const handles = { ...data, start, end };

      drawLengthVtk(context, handles, distanceText);
    }
  }

  const superHandleMouseMove = publicAPI.handleMouseMove;
  publicAPI.handleMouseMove = callData => {
    const { apis = [], apiIndex } = model;

    if (!apis.length || apiIndex === null || apiIndex === undefined) return;

    const thisApi = apis[apiIndex];
    const renderer = thisApi.genericRenderWindow.getRenderer();
    const container = thisApi.genericRenderWindow.getContainer();
    const measurementElm = container.querySelector(`.measurement-canvas`);
    let context = measurementElm.getContext('2d');

    if (isDown && !isDrawing && !isEmpty(nearLineData)) {
      let currentMouseWorlPos = computedWorlPosValue(
        callData.position,
        renderer
      );

      const distancePStart = computedClosestPoint(
        mouseStart,
        nearLineData.worldPosStart
      );
      const distancePEnd = computedClosestPoint(
        mouseStart,
        nearLineData.worldPosEnd
      );

      let subVec = computedSubVec(currentMouseWorlPos, mouseStart);

      if (distancePStart < 1) {
        nearLineData.worldPosStart[0] += subVec.x;
        nearLineData.worldPosStart[1] += subVec.y;
        nearLineData.worldPosStart[2] += subVec.z;
      } else if (distancePEnd < 1) {
        nearLineData.worldPosEnd[0] += subVec.x;
        nearLineData.worldPosEnd[1] += subVec.y;
        nearLineData.worldPosEnd[2] += subVec.z;
      } else {
        nearLineData.worldPosStart[0] += subVec.x;
        nearLineData.worldPosStart[1] += subVec.y;
        nearLineData.worldPosStart[2] += subVec.z;

        nearLineData.worldPosEnd[0] += subVec.x;
        nearLineData.worldPosEnd[1] += subVec.y;
        nearLineData.worldPosEnd[2] += subVec.z;
      }

      // update mouse start
      mouseStart = currentMouseWorlPos;

      renderToolData(thisApi, context, renderer);

      return;
    }

    if (!isDrawing) {
      const toolData = thisApi.toolsState && thisApi.toolsState['vtkLength'];
      if (toolData && (toolData.data || []).length > 0) {
        nearLineData = {};
        let isUpdate = false;

        for (let i = 0; i < toolData.data.length; i++) {
          const data = toolData.data[i];
          const currentWorldPos = computedWorlPosValue(
            callData.position,
            renderer
          );

          const distance = computedClosestLine(data, currentWorldPos);

          if (distance < 1 && isEmpty(nearLineData)) {
            data.active = true;
            nearLineData = data;
            isUpdate = true;
          } else {
            if (data.active) {
              data.active = false;
              isUpdate = true;
            }
          }
        }
        if (isUpdate) {
          renderToolData(thisApi, context, renderer);
        }
      }
    }

    if (isDrawing) {
      // redraw data
      renderToolData(thisApi, context, renderer);

      drawingData.worldPosEnd = computedWorlPosValue(
        callData.position,
        renderer
      );

      const distance = vtkMath.distance2BetweenPoints(
        drawingData.worldPosStart,
        drawingData.worldPosEnd
      );
      const distanceText = `${Math.sqrt(distance).toFixed(2)} mm`;

      const start = computedLocalDisplayValue(
        drawingData.worldPosStart,
        renderer
      );
      const end = computedLocalDisplayValue(drawingData.worldPosEnd, renderer);
      const handles = { ...drawingData, start, end };

      drawLengthVtk(context, handles, distanceText);
    }

    if (superHandleMouseMove) {
      superHandleMouseMove(callData);
    }
  };

  const superHandleLeftButtonPress = publicAPI.handleLeftButtonPress;
  publicAPI.handleLeftButtonPress = callData => {
    const { apis = [], apiIndex } = model;
    if (!apis.length || apiIndex === null || apiIndex === undefined) return;
    const thisApi = apis[apiIndex];

    const renderer = thisApi.genericRenderWindow.getRenderer();
    const container = thisApi.genericRenderWindow.getContainer();
    const measurementElm = container.querySelector(`.measurement-canvas`);
    let context = measurementElm.getContext('2d');

    const worldPos = computedWorlPosValue(callData.position, renderer);
    isDown = true;

    if (!isEmpty(nearLineData)) {
      mouseStart = worldPos;
      return;
    }

    if (!isDrawing) {
      drawingData = {};
      drawingData.worldPosStart = worldPos;
      drawingData.uuid = uuidv4();
      isDrawing = true;
      const handles = computedLocalDisplayValue(
        drawingData.worldPosStart,
        renderer
      );
      drawHandlesVtk(context, handles);
    } else {
      drawingData.worldPosEnd = worldPos;
      isDrawing = false;

      if (thisApi.hasOwnProperty('toolsState') === false) {
        thisApi.toolsState = {};
      }
      if (thisApi.toolsState.hasOwnProperty('vtkLength') === false) {
        thisApi.toolsState['vtkLength'] = {
          data: [],
        };
      }
      const toolData = thisApi.toolsState['vtkLength'];
      toolData.data.push({ ...drawingData });
      drawingData = {};
    }

    if (superHandleLeftButtonPress) {
      // superHandleLeftButtonPress(callData);
    }
  };

  publicAPI.superHandleLeftButtonRelease = publicAPI.handleLeftButtonRelease;
  publicAPI.handleLeftButtonRelease = callData => {
    isDown = false;
    nearLineData = {};
    mouseStart = [];

    if (publicAPI.superHandleLeftButtonRelease) {
      // publicAPI.superHandleLeftButtonRelease(callData);
    }
  };
}

// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {
  wlStartPos: [0, 0],
  levelScale: 1,
};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  vtkInteractorStyleMPRSlice.extend(publicAPI, model, initialValues);

  macro.setGet(publicAPI, model, ['onMeasurementChanged']);

  // Object specific methods
  vtkInteractorStyleLength(publicAPI, model);
}

export const newInstance = macro.newInstance(
  extend,
  'vtkInteractorStyleLength'
);

export default Object.assign({ newInstance, extend });

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    let r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;

    return v.toString(16);
  });
}
