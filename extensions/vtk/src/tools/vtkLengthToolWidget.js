import macro from 'vtk.js/Sources/macro';
import vtkHandleWidget from 'vtk.js/Sources/Interaction/Widgets/HandleWidget';
import vtkLengthToolRepresentation from './LengthToolRepresentation';

const { VOID } = macro;

// ----------------------------------------------------------------------------
// vtkLengthToolWidget methods
// ----------------------------------------------------------------------------

function vtkLengthToolWidget(publicAPI, model) {
  // // Set our className
  model.classHierarchy.push('vtkLengthToolWidget');

  const superClass = Object.assign({}, publicAPI);

  publicAPI.createDefaultRepresentation = () => {
    if (!model.widgetRep) {
      model.widgetRep = vtkLengthToolRepresentation.newInstance();
    }
  };

  publicAPI.setEnabled = (enabling, apiInfo = {}) => {
    if (!enabling && model.widgetRep) {
      // Remove label
      model.widgetRep.setContainer(null);
    }

    superClass.setEnabled(enabling);

    if (enabling) {
      const container = model.interactor
        ? model.interactor.getContainer()
        : null;
      model.widgetRep.setContainer(container, apiInfo);
      model.apiInfo = apiInfo;
    }
  };

  publicAPI.scaleAction = callData => VOID;
}

// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  vtkHandleWidget.extend(publicAPI, model, initialValues);

  // Object methods
  vtkLengthToolWidget(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(extend, 'vtkLengthToolWidget');

// ----------------------------------------------------------------------------

export default { newInstance, extend };
