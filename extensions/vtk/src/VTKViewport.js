import React, { useEffect, useCallback } from 'react';
import { View2D } from '@vindr/vindr-vtkjs-viewport';
import PropTypes from 'prop-types';

import './VTKViewport.css';
import { removeVtkAnnotations } from './tools/drawing';
import View3D from './View3D';

const VTKViewport = props => {
  const style = { width: '100%', height: '100%', position: 'relative' };

  const setViewportActiveHandler = useCallback(() => {
    const { setViewportActive, viewportIndex, activeViewportIndex } = props;

    if (viewportIndex !== activeViewportIndex) {
      // set in Connected
      setViewportActive();
    }
  });

  useEffect(() => {
    const handleScrollEvent = evt => {
      const vtkViewportApiReference = props.onScroll(props.viewportIndex) || {};
      const viewportUID = vtkViewportApiReference.uid;
      const viewportWasScrolled = viewportUID === evt.detail.uid;

      if (viewportWasScrolled) {
        setViewportActiveHandler();
        removeVtkAnnotations([vtkViewportApiReference]);
      }
    };

    window.addEventListener('vtkscrollevent', handleScrollEvent);
    return () =>
      window.removeEventListener('vtkscrollevent', handleScrollEvent);
  }, [props, props.onScroll, props.viewportIndex, setViewportActiveHandler]);

  return (
    <div
      className="vtk-viewport-handler"
      style={style}
      onClick={setViewportActiveHandler}
      onDoubleClick={event => {
        if (props.onResizeViewport) {
          props.onResizeViewport();
        }
      }}
    >
      {props.mode == '3d' ? <View3D {...props} /> : <View2D {...props} />}
    </div>
  );
};

VTKViewport.propTypes = {
  setViewportActive: PropTypes.func.isRequired,
  viewportIndex: PropTypes.number.isRequired,
  activeViewportIndex: PropTypes.number.isRequired,
  /* Receives viewportIndex */
  onScroll: PropTypes.func,
};

VTKViewport.defaultProps = {
  onScroll: () => {},
};

export default VTKViewport;
