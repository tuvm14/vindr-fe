import SlabThicknessToolbarComponent from './toolbarComponents/SlabThicknessToolbarComponent';
import { WindowLevelSettings } from '@tuvm/viewer/src/components/WindowLevelSettings';

const TOOLBAR_BUTTON_TYPES = {
  COMMAND: 'command',
  SET_TOOL_ACTIVE: 'setToolActive',
};

const definitions = [
  {
    id: 'Crosshairs',
    label: 'Crosshairs',
    icon: 'crossHair',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'enableCrosshairsTool',
    commandOptions: {},
  },
  {
    id: 'WWWC',
    label: 'Window',
    icon: 'brightness',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'enableLevelTool',
    commandOptions: {},
  },
  {
    id: 'setWindowLevel',
    CustomComponent: WindowLevelSettings,
  },
  {
    id: 'Length',
    label: 'Length',
    icon: 'straighten',
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'enableVtkLengthTool',
    commandOptions: {},
  },
  {
    id: 'Clear',
    label: 'Clear',
    icon: 'deleteIcon',
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'clearVtkAnnotations',
  },
  {
    id: 'Reset',
    label: 'Reset',
    icon: 'refresh',
    divider: true,
    //
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'resetMPRView',
    commandOptions: {},
  },
  // {
  //   id: 'Rotate',
  //   label: 'Rotate',
  //   icon: 'rotateRight',
  //   //
  //   type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
  //   commandName: 'enableRotateTool',
  //   commandOptions: {},
  // },
  // {
  //   id: 'setBlendModeToComposite',
  //   label: 'Disable MIP',
  //   icon: 'times',
  //   //
  //   type: TOOLBAR_BUTTON_TYPES.COMMAND,
  //   commandName: 'setBlendModeToComposite',
  //   commandOptions: {},
  // },
  // {
  //   id: 'setBlendModeToMaximumIntensity',
  //   label: 'Enable MIP',
  //   icon: 'soft-tissue',
  //   //
  //   type: TOOLBAR_BUTTON_TYPES.COMMAND,
  //   commandName: 'setBlendModeToMaximumIntensity',
  //   commandOptions: {},
  // },
  //
  // {
  //   id: 'increaseSlabThickness',
  //   label: 'Increase Slab Thickness',
  //   icon: 'caret-up',
  //   //
  //   type: TOOLBAR_BUTTON_TYPES.COMMAND,
  //   commandName: 'increaseSlabThickness',
  //   commandOptions: {},
  // },
  // {
  //   id: 'decreaseSlabThickness',
  //   label: 'Decrease Slab Thickness',
  //   icon: 'caret-down',
  //   //
  //   type: TOOLBAR_BUTTON_TYPES.COMMAND,
  //   commandName: 'decreaseSlabThickness',
  //   commandOptions: {},
  // },
  {
    id: 'changeSlabThickness',
    label: 'Slab Thickness',
    icon: 'soft-tissue',
    CustomComponent: SlabThicknessToolbarComponent,
    divider: true,
    commandName: 'setSlabThickness',
    actionButton: {
      id: 'setSlabThickness',
      label: 'slider',
      type: TOOLBAR_BUTTON_TYPES.COMMAND,
      commandName: 'setSlabThickness',
      commandOptions: {},
    },
    deactivateButton: {
      id: 'setBlendModeToComposite',
      type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
      commandName: 'setBlendModeToComposite',
      commandOptions: {},
    },
    operationButtons: [
      {
        id: 'setBlendModeToMaximumIntensity',
        label: 'MIP',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setBlendModeToMaximumIntensity',
        commandOptions: {},
      },
      {
        id: 'setBlendModeToMinimumIntensity',
        label: 'MinIP',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setBlendModeToMinimumIntensity',
        commandOptions: {},
      },
      {
        id: 'setBlendModeToAverageIntensity',
        label: 'AvgIP',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setBlendModeToAverageIntensity',
        commandOptions: {},
      },
    ],
  },
];

export default {
  definitions,
  defaultContext: 'ACTIVE_VIEWPORT::VTK',
};
