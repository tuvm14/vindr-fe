import vtkPicker from 'vtk.js/Sources/Rendering/Core/Picker';
import vtkPlane from 'vtk.js/Sources/Common/DataModel/Plane';
import { clearCanvasVtk } from './drawing';

const vec3 = {
  sum: (a, b) => [a[0] + b[0], a[1] + b[1], a[2] + b[2]],
  minus: (a, b) => [a[0] - b[0], a[1] - b[1], a[2] - b[2]],
  divide: (a, b) => [
    b[0] == 0 ? b[0] : a[0] / b[0],
    b[1] == 0 ? b[1] : a[1] / b[1],
    b[2] == 0 ? b[2] : a[2] / b[2],
  ],
  mul: (a, b) => [a[0] * b[0], a[1] * b[1], a[2] * b[2]],
  round: a => [Math.round(a[0]), Math.round(a[1]), Math.round(a[2])],
  distance: (a, b) =>
    Math.sqrt(
      (a[0] - b[0]) * (a[0] - b[0]) +
        (a[1] - b[1]) * (a[1] - b[1]) +
        (a[2] - b[2]) * (a[2] - b[2])
    ),
  parseInt: a => [parseInt(a[0]), parseInt(a[1]), parseInt(a[2])],
};

export function worldCoordsPick(
  volumeActor,
  volumeMapper,
  renderer,
  imageDataObject,
  position
) {
  const scalarsData = volumeMapper
    .getInputData()
    .getPointData()
    .getScalars()
    .getData();

  const bounds = volumeActor.getMapper().getBounds();
  let center = [
    (bounds[0] + bounds[1]) / 2.0,
    (bounds[2] + bounds[3]) / 2.0,
    (bounds[4] + bounds[5]) / 2.0,
  ];

  const picker = vtkPicker.newInstance();

  const camera = renderer.getActiveCamera();
  const cameraPos = camera.getPosition();

  const pos = position;
  const p = [pos.x, pos.y, 0.0];
  picker.pick(p, renderer);

  const pickedPoint = picker.getPickPosition();

  let windowLevel = 0;
  const oNodes = volumeActor
    .getProperty()
    .getScalarOpacity(0)
    .get().nodes;
  for (let i = 0; i < oNodes.length; i++) {
    if (oNodes[i] && oNodes[i].y > 0) {
      windowLevel = oNodes[i].x;
      break;
    }
  }

  const spacing = imageDataObject.spacing;
  const dimensions = imageDataObject.dimensions;
  const camToFP = vec3.minus(cameraPos, center);
  const distance = vec3.distance(cameraPos, center);
  const normal = vec3.divide(camToFP, [distance, distance, distance]); // vector phap tuyen
  let index,
    minPoint,
    minDistance = 1000000;

  const step = Math.round(distance);

  for (let i = 0; i < step * 4; i++) {
    const origin = [
      center[0] + (normal[0] / 2) * (i - step * 2),
      center[1] + (normal[1] / 2) * (i - step * 2),
      center[2] + (normal[2] / 2) * (i - step * 2),
    ];
    const intersect = vtkPlane.intersectWithLine(
      pickedPoint,
      cameraPos,
      origin,
      normal
    );
    const root = vec3.minus(
      center,
      vec3.mul(spacing, vec3.divide(dimensions, [2, 2, 2]))
    );
    const coords = vec3.round(
      vec3.divide(vec3.minus(intersect.x, root), spacing)
    );
    if (
      coords[0] < 0 ||
      coords[0] >= dimensions[0] ||
      coords[1] < 0 ||
      coords[1] > dimensions[1] ||
      coords[2] < 0 ||
      coords[2] > dimensions[2]
    ) {
      continue;
    }
    const pixelIndex =
      coords[2] * (dimensions[0] * dimensions[1]) +
      coords[1] * dimensions[0] +
      coords[0];
    if (scalarsData[pixelIndex] > windowLevel) {
      // need improve expresstion with torlence
      const distance = vec3.distance(intersect.x, cameraPos);
      if (distance < minDistance) {
        minDistance = distance;
        index = pixelIndex;
        minPoint = intersect.x;
      }
    }
  }

  if (index && minPoint) {
    // const point = volumeMapper.getInputData().getPoint(index);
    // console.log("MIN", minPoint, point);

    return minPoint;
  }

  // console.log("PICKED", pickedPoint);

  return pickedPoint;
}

export function clearCanvas(genericRenderWindow) {
  const container = genericRenderWindow.getContainer();
  const measurementElm = container.querySelector(`.measurement-canvas`);
  let context = measurementElm.getContext('2d');
  clearCanvasVtk(context);
}

function uniq(a) {
  var newA = [a[0]],
    index = 0;
  for (let i = 1; i < a.length; i++) {
    if (a[i].y !== newA[index].y) {
      // need to be changed
      newA.push(a[i]);
    } else {
      newA.push({ ...a[i], y: a[i].y + 1 });
    }
    ++index;
  }
  return newA;
}

export function polygonBeamCast(
  volumeActor,
  renderer,
  points,
  spacing,
  dimensions,
  pointData,
  invert = false
) {
  const camera = renderer.getActiveCamera();
  const cameraPos = camera.getPosition();

  const bounds = volumeActor.getMapper().getBounds();
  let center = [
    (bounds[0] + bounds[1]) / 2.0,
    (bounds[2] + bounds[3]) / 2.0,
    (bounds[4] + bounds[5]) / 2.0,
  ];
  const camToFP = vec3.minus(cameraPos, center);
  const viewUp = getViewUp(camToFP);
  const picker = vtkPicker.newInstance();

  // const distance = vec3.distance(cameraPos, center);
  // const orientation = vec3.divide(camToFP, [distance, distance, distance]);

  const polygons = points.map(it => {
    const pp = [it.x, it.y, it.z];
    picker.pick(pp, renderer);
    const pickedPoint = picker.getPickPosition();

    const camToPoint = vec3.minus(cameraPos, pickedPoint);
    const distance = vec3.distance(cameraPos, pickedPoint);
    const orientation = vec3.divide(camToPoint, [distance, distance, distance]);

    return getListMatPoint(
      pickedPoint,
      spacing,
      center,
      dimensions,
      viewUp,
      orientation
    );
  });

  const copyScalar = {};
  if (invert) {
    for (let i = 0; i < pointData.length; i++) {
      copyScalar[i] = pointData[i];
      pointData[i] = -10000;
    }
  }
  // console.log(polygons);
  if (viewUp == 0) {
    for (let i = 0; i < dimensions[0]; i++) {
      const ps = polygons.map(it => ({ x: it[i][1], y: it[i][2] }));
      const uniqPoints = uniq(ps);
      pointData = fill(
        uniqPoints,
        pointData,
        i,
        dimensions,
        viewUp,
        invert,
        copyScalar
      );
    }
  } else if (viewUp == 1) {
    for (let i = 0; i < dimensions[1]; i++) {
      const ps = polygons.map(it => ({ x: it[i][0], y: it[i][2] }));
      const uniqPoints = uniq(ps);
      pointData = fill(
        uniqPoints,
        pointData,
        i,
        dimensions,
        viewUp,
        invert,
        copyScalar
      );
    }
  } else {
    for (let i = 0; i < dimensions[2]; i++) {
      const ps = polygons.map(it => ({ x: it[i][0], y: it[i][1] }));
      const uniqPoints = uniq(ps);
      pointData = fill(
        uniqPoints,
        pointData,
        i,
        dimensions,
        viewUp,
        invert,
        copyScalar
      );
    }
  }

  return pointData;
}

function getListMatPoint(
  point,
  spacing,
  center,
  dimentions,
  viewUp,
  orientation
) {
  const pixelCoords = getPixelCoords(point, spacing, center, dimentions);
  let points = [];
  let m = [
    orientation[0] / orientation[viewUp],
    orientation[1] / orientation[viewUp],
    orientation[2] / orientation[viewUp],
  ];

  for (let i = 0; i < dimentions[viewUp]; i++) {
    const shift = i - pixelCoords[viewUp];
    let p = [
      Math.round(
        pixelCoords[0] + (m[0] * shift) / (spacing[0] / spacing[viewUp])
      ),
      Math.round(
        pixelCoords[1] + (m[1] * shift) / (spacing[1] / spacing[viewUp])
      ),
      Math.round(
        pixelCoords[2] + (m[2] * shift) / (spacing[2] / spacing[viewUp])
      ),
    ];
    points.push(p);
  }
  return points;
}

function getScalarIndex(coords, dimensions) {
  if (
    coords[0] < 0 ||
    coords[0] >= dimensions[0] ||
    coords[1] < 0 ||
    coords[1] >= dimensions[1] ||
    coords[2] < 0 ||
    coords[2] >= dimensions[2]
  ) {
    return 0;
  }
  const pixelIndex =
    coords[2] * (dimensions[0] * dimensions[1]) +
    coords[1] * dimensions[0] +
    coords[0];
  return pixelIndex;
}

function fill(
  points,
  scalarArray,
  slice,
  dimensions,
  viewUp = 1,
  invert = false,
  copyScalar = []
) {
  var edges = points.map((it, idx) => {
    let p = idx;
    let c = (idx + 1) % points.length;
    let n = (idx + 2) % points.length;
    let mark = false;
    if (
      (points[p].y < points[c].y && points[c].y < points[n].y) ||
      (points[p].y > points[c].y && points[c].y > points[n].y)
    ) {
      mark = true;
    }
    return { s: it, t: points[(idx + 1) % points.length], mark };
  });

  var pre = edges.map(it => {
    let reverseM = (it.t.x - it.s.x) / (it.t.y - it.s.y);
    if (it.t.y > it.s.y) {
      return {
        m: reverseM,
        ymin: it.s.y,
        x: it.s.x,
        ymax: it.mark ? it.t.y - 1 : it.t.y,
      };
    } else {
      return {
        m: reverseM,
        ymin: it.mark ? it.t.y + 1 : it.t.y,
        x: it.mark ? it.t.x + reverseM : it.t.x,
        ymax: it.s.y,
      };
    }
  });

  let table = {};

  pre.forEach(it => {
    let x = it.x;

    for (let i = it.ymin; i <= it.ymax; i++) {
      if (!table[i]) {
        table[i] = [Math.round(x)];
      } else {
        table[i].push(Math.round(x));
      }
      x = x + it.m;
    }
  });

  for (let key of Object.keys(table)) {
    table[key] = table[key].sort((a, b) => a - b);
  }

  for (let key of Object.keys(table)) {
    for (let i = 0; i < table[key].length; i += 2) {
      for (let j = table[key][i]; j <= table[key][i + 1]; j++) {
        let index;
        if (viewUp === 0) {
          index = getScalarIndex([slice, j, parseInt(key)], dimensions);
        } else if (viewUp == 1) {
          index = getScalarIndex([j, slice, parseInt(key)], dimensions);
        } else {
          index = getScalarIndex([j, parseInt(key), slice], dimensions);
        }
        if (invert) {
          scalarArray[index] = copyScalar[index];
        } else {
          scalarArray[index] = -10000;
        }
      }
    }
    for (let i = table[key].length - 1; i > 0; i -= 2) {
      for (let j = table[key][i]; j >= table[key][i-1]; j--) {
        let index;
        if (viewUp === 0) {
          index = getScalarIndex([slice, j, parseInt(key)], dimensions);
        } else if (viewUp == 1) {
          index = getScalarIndex([j, slice, parseInt(key)], dimensions);
        } else {
          index = getScalarIndex([j, parseInt(key), slice], dimensions);
        }
        if (invert) {
          scalarArray[index] = copyScalar[index];
        } else {
          scalarArray[index] = -10000;
        }
      }
    }
  }

  return scalarArray;
}

function getViewUp(vec) {
  const a = vec.map(it => (it >= 0 ? it : -it));
  const m = Math.max(...a);
  const viewUp = a.indexOf(m);
  if (viewUp < 0) return 1; // default is yaxis
  return viewUp;
}

function getPixelCoords(worldCoords, spacing, center, dimensions) {
  const root = vec3.minus(
    center,
    vec3.mul(spacing, vec3.divide(dimensions, [2, 2, 2]))
  );
  const coords = vec3.round(
    vec3.divide(vec3.minus(worldCoords, root), spacing)
  );
  return coords;
}

export default {};
