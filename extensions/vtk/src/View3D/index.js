import React, { useEffect, useState, useRef } from 'react';
import { connect } from 'react-redux';
import { Select, Spin, Checkbox, Popover, InputNumber } from 'antd';
import {
  CaretDownOutlined,
  CheckOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import vtkVolume from 'vtk.js/Sources/Rendering/Core/Volume';
import vtkVolumeMapper from 'vtk.js/Sources/Rendering/Core/VolumeMapper';
import vtkColorTransferFunction from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction';
import GenericRenderWindow from 'vtk.js/Sources/Rendering/Misc/GenericRenderWindow';
import vtkPiecewiseFunction from 'vtk.js/Sources/Common/DataModel/PiecewiseFunction';
import vtkImageData from 'vtk.js/Sources/Common/DataModel/ImageData';
import vtkOrientationMarkerWidget from 'vtk.js/Sources/Interaction/Widgets/OrientationMarkerWidget';
import vtkAnnotatedCubeActor from 'vtk.js/Sources/Rendering/Core/AnnotatedCubeActor';
import vtkInteractorStyle3DWindowLevel from './vtkInteractorStyle3DWindowLevel';
import vtkInteractorStyleScalpelRect from './vtkInteractorStyleScalpelRect';
import vtkInteractorStyleScalpelFreehand from './vtkInteractorStyleScalpelFreehand';

import presets from './presets.js';
import { useTranslation } from 'react-i18next';
import { Icon, Tooltip } from '@tuvm/ui';
import './View3D.styl';
import { bed_remove } from './bedRemove';
import vtkInteractorStyle3DCrosshairs from './vtkInteractorStyle3DCrosshairs';
import vtkInteractorStyle3DLength from './vtkInteractorStyle3DLength';
import vtkInteractorStyle3DRotate from './vtkInteractorStyle3DRotate';
import { clearCanvas } from './helper';

const PROPERTY_TYPE = {
  QUALITY: 1,
  SHADE: 2,
  SAMPLE_DISTANCE: 3,
  RANGE: 4,
  AMBIENT: 5,
  DIFFUSE: 6,
  SPECULAR: 7,
  SPECULAR_POWER: 8,
  OPACITY_DISTANCE: 9,
};

const KEY = {
  DEL: 'Del',
  ENTER: 'Enter',
  ESC: 'Esc',
};

const cursorMapper = activeTool => {
  switch (activeTool) {
    case TOOLS_3D.ROTATE:
      return 'rotate-cursor';
    case TOOLS_3D.CROSSHAIR:
      return 'crosshair-cursor';
    default:
      return 'default-cursor';
  }
};

let oglrw;
let volumeActor;
let volumeMapper;
let ofun;
let cfun;
let genericRender;
let orientationWidget;
let toolsState = {};

let timeoutRender;
const defaultPresetId = 'vtkMRMLVolumePropertyNode11'; // CT-Coronary-Arteries-2
const TOOLS_3D = {
  ROTATE: 1,
  ADJUST_WINDOW: 2,
  RESET: 3,
  CROSSHAIR: 4,
  DISTANCE: 5,
  RECT_SCALPEL: 6,
  FREEHAND_SCALPEL: 7,
};

let initCameraInfo = {};

function getScalarsData(imageData) {
  const dataType = imageData
    .getPointData()
    .getScalars()
    .getDataType();
  const data = imageData
    .getPointData()
    .getScalars()
    .getData();

  let pointsData = [];

  switch (dataType) {
    case 'Uint8Array':
      pointsData = new Uint8Array(data);
      break;
    case 'Uint8ClampedArray':
      pointsData = new Uint8ClampedArray(data);
      break;
    case 'Uint16Array':
      pointsData = new Uint16Array(data);
      break;
    case 'Uint32Array':
      pointsData = new Uint32Array(data);
      break;
    case 'Int8Array':
      pointsData = new Int8Array(data);
      break;
    case 'Int16Array':
      pointsData = new Int16Array(data);
      break;
    case 'Int32Array':
      pointsData = new Int32Array(data);
      break;
    case 'Float32Array':
      pointsData = new Float32Array(data);
      break;
    case 'Float64Array':
      pointsData = new Float64Array(data);
      break;
    default:
      break;
  }
  return pointsData;
}

const View3D = props => {
  const volumeContainer = useRef();
  const { t } = useTranslation('Vindoc');

  const [currentPreset, setCurrentPreset] = useState(defaultPresetId);
  const [isRemovedBed, setRemovedBed] = useState(false);
  const [isCutting, setIsCutting] = useState(false);
  const [isProcessing, setProcessing] = useState(false);
  const [activeTool, setActiveTool] = useState(TOOLS_3D.ROTATE);
  // const [isLoaded, setIsLoaded] = useState(false);
  // const [percentComplete, setPercentComplete] = useState(0);
  // const [sampleDistance, setSampleDistance] = useState(1);
  const [opacityDistance, setOpacityDistance] = useState(1.5); // default 1
  // const [newLower, setLower] = useState(0);
  // const [newUpper, setUpper] = useState(1);
  const [shade, setShade] = useState(true);
  const [ambient, setAmbient] = useState(0);
  const [diffuse, setDiffuse] = useState(0);
  const [specular, setSpecular] = useState(0);
  const [specularPower, setSpecularPower] = useState(0);

  useEffect(() => {
    setStateFromProps();
  }, []);

  useEffect(() => {
    return () => {
      oglrw = null;
      volumeActor = null;
      volumeMapper = null;
      ofun = null;
      cfun = null;
      genericRender = null;
      initCameraInfo = {};
      orientationWidget = null;
    };
  }, []);

  const getOrCreateVolume = imageDataObject => {
    // create data for 3D
    const imageData = vtkImageData.newInstance();
    const { vtkImageData: imgData, imageMetaData0 } = imageDataObject;
    imageData.shallowCopy(imgData);
    const pointsData = getScalarsData(imgData);
    imageData
      .getPointData()
      .getScalars()
      .setData(pointsData, 1);

    // const {
    //   windowWidth: WindowWidth,
    //   windowCenter: WindowCenter,
    //   modality: Modality,
    // } = imageMetaData0;

    // const { lower, upper } = _getRangeFromWindowLevels(
    //   WindowWidth,
    //   WindowCenter,
    //   Modality
    // );
    volumeActor = vtkVolume.newInstance();
    volumeMapper = vtkVolumeMapper.newInstance();

    volumeActor.setMapper(volumeMapper);
    volumeMapper.setInputData(imageData);

    // volumeActor
    //   .getProperty()
    //   .getRGBTransferFunction(0)
    //   .setRange(lower, upper);
    // volumeActor
    //   .getProperty()
    //   .getScalarOpacity(0)
    //   .setRange([lower, upper]);

    // const ext = vtkImageData.getExtent();
    const spacing = imageData.getSpacing();
    // const vsize = vec3.create();
    // vec3.set(
    //   vsize,
    //   (ext[1] - ext[0] + 1) * spacing[0],
    //   (ext[3] - ext[2] + 1) * spacing[1],
    //   (ext[5] - ext[4] + 1) * spacing[2]
    // );

    // const maximumSamples = volumeMapper.getMaximumSamplesPerRay();
    let sampleDistance = (spacing[0] + spacing[1] + spacing[2]) / 6;
    // const maxDistanceValue = vec3.length(vsize) / maximumSamples + 0.1;

    // if (maxDistanceValue > sampleDistance) {
    //   sampleDistance = parseFloat(maxDistanceValue.toFixed(1));
    // }
    if (sampleDistance > 0) {
      // setSampleDistance(sampleDistance);
      volumeMapper.setSampleDistance(sampleDistance);
    }

    // Be generous to surpress warnings, as the logging really hurts performance.
    // TODO: maybe we should auto adjust samples to 1000.
    volumeMapper.setMaximumSamplesPerRay(2000);

    return volumeActor;
  };

  const setStateFromProps = () => {
    const volumeActor = getOrCreateVolume(props.imageDataObject);

    setTimeout(() => {
      render3D(props.imageDataObject, volumeActor);
    }, 0);
  };

  const render3D = (imageDataObject, volumeActor) => {
    const preset = presets.find(preset => preset.id === currentPreset);
    applyPreset(
      volumeActor,
      preset,
      setShade,
      setAmbient,
      setDiffuse,
      setSpecular,
      setSpecularPower,
      setOpacityDistance
      // setLower,
      // setUpper
    );

    genericRender = GenericRenderWindow.newInstance({
      background: [0, 0, 0],
      listenWindowResize: true,
    });
    genericRender.setContainer(volumeContainer.current);
    const renderer = genericRender.getRenderer();
    const renderWindow = genericRender.getRenderWindow();
    // renderWindow.getInteractor().setDesiredUpdateRate(15.0);

    volumeActor.getProperty().setScalarOpacityUnitDistance(0, opacityDistance);
    // oglrw = genericRender.getOpenGLRenderWindow();
    // oglrw.setSize((window.innerWidth - 156) / 2, window.innerHeight - 104);

    renderer.addVolume(volumeActor);
    renderer.resetCamera();
    const camera = renderer.getActiveCamera();
    camera.elevation(-70);
    renderer.updateLightsGeometryToFollowCamera();

    let center = camera.getFocalPoint();
    const bounds = volumeActor.getMapper().getBounds();
    center = [
      (bounds[0] + bounds[1]) / 2.0,
      (bounds[2] + bounds[3]) / 2.0,
      (bounds[4] + bounds[5]) / 2.0,
    ];
    const { viewUp = [], position = [] } = camera.get() || {};
    initCameraInfo.viewUp = [...viewUp];
    initCameraInfo.position = [...position];

    const interactorStyle = vtkInteractorStyle3DRotate.newInstance();
    interactorStyle.setCenterOfRotation(center);
    renderWindow.getInteractor().setInteractorStyle(interactorStyle);

    addOrientationMarker(renderWindow);

    // TODO: Not sure why this is necessary to force the initial draw
    genericRender.resize();
    if (props.onModifyRendering) {
      props.onModifyRendering(genericRender);
    }

    renderWindow.render();

    // Debug
    // window.cfun = cfun;
    // window.ofun = ofun;
    // window.widget = widget;
    // window.oglrw = oglrw;
    // window.volumeActor = volumeActor;
    // window.volumeMapper = volumeMapper;
    // window.genericRender = genericRender;
    // window.renderer = renderer;
    // window.renderWindow = renderWindow;
    // window.imageDataObject = imageDataObject;
  };

  function handleChangePreset(value) {
    setCurrentPreset(value);
    const preset = presets.find(preset => preset.id === value);

    applyPreset(
      volumeActor,
      preset,
      setShade,
      setAmbient,
      setDiffuse,
      setSpecular,
      setSpecularPower,
      setOpacityDistance
      // setLower,
      // setUpper
    );
    if (genericRender) {
      const renderWindow = genericRender.getRenderWindow();
      renderWindow.render();
    }
  }

  const handleChangeProperty = (type, values) => {
    try {
      switch (type) {
        case PROPERTY_TYPE.QUALITY:
          // const currentSize = oglrw.getSize();
          // const [currentWidth, currentHeight] = currentSize;

          if (genericRender) {
            const renderer = genericRender.getRenderer();
            const volumes = renderer.getVolumes();
            const currentVolumes = volumes && volumes[0];
            const mapper = currentVolumes.getMapper();
            const { spacing = [] } = props.imageDataObject;
            const defaultSamplesDistance =
              (spacing[0] + spacing[1] + spacing[2]) / 6;

            if (values == 1) {
              // oglrw.setSize((300 * currentWidth) / currentHeight, 300);
              mapper.setSampleDistance(defaultSamplesDistance * 1.5);
            } else if (values == 2) {
              mapper.setSampleDistance(defaultSamplesDistance);
              // oglrw.setSize((1000 * currentWidth) / currentHeight, 1000);
            }
          }
          break;
        case PROPERTY_TYPE.SHADE:
          setShade(values);
          volumeActor.getProperty().setShade(values);
          break;
        case PROPERTY_TYPE.OPACITY_DISTANCE:
          setOpacityDistance(values);
          volumeActor.getProperty().setScalarOpacityUnitDistance(0, values);
          break;
        // case PROPERTY_TYPE.SAMPLE_DISTANCE:
        //   setSampleDistance(values);
        //   // mapper
        //   volumeMapper.setSampleDistance(values);
        //   break;
        // case PROPERTY_TYPE.RANGE:
        //   setLower(values[0]);
        //   setUpper(values[1]);
        //   volumeActor
        //     .getProperty()
        //     .getRGBTransferFunction(0)
        //     .setRange(values[0], values[1]);
        //   break;
        case PROPERTY_TYPE.AMBIENT:
          setAmbient(values);
          volumeActor.getProperty().setAmbient(values);
          break;
        case PROPERTY_TYPE.DIFFUSE:
          setDiffuse(values);
          volumeActor.getProperty().setDiffuse(values);
          break;
        case PROPERTY_TYPE.SPECULAR:
          setSpecular(values);
          volumeActor.getProperty().setSpecular(values);
          break;
        case PROPERTY_TYPE.SPECULAR_POWER:
          setSpecularPower(values);
          volumeActor.getProperty().setSpecularPower(values);
          break;
        default:
          break;
      }

      clearTimeout(timeoutRender);
      timeoutRender = setTimeout(() => {
        if (genericRender) {
          const renderWindow = genericRender.getRenderWindow();
          renderWindow.render();
        }
      }, 500);
    } catch (error) {
      console.log(error);
    }
  };

  const handleAdjustWindow = () => {
    if (genericRender) {
      setActiveTool(TOOLS_3D.ADJUST_WINDOW);
      const renderWindow = genericRender.getRenderWindow();
      const istyle = vtkInteractorStyle3DWindowLevel.newInstance();
      istyle.setVolumeActor(volumeActor);
      const interactor = renderWindow.getInteractor();
      if (interactor) {
        interactor.setInteractorStyle(istyle);
        istyle.setInteractor(interactor);
        renderWindow.render();
      }
    }
  };

  const handleActiveCrosshairs = () => {
    if (genericRender) {
      setActiveTool(TOOLS_3D.CROSSHAIR);
      const renderWindow = genericRender.getRenderWindow();
      const istyle = vtkInteractorStyle3DCrosshairs.newInstance();
      istyle.setVolumeActor(volumeActor);
      istyle.setRenderer(genericRender.getRenderer());
      istyle.setVolumeMapper(volumeMapper);
      istyle.setImageDataObject(props.imageDataObject);

      // const zoomManipulatorScroll = vtkMouseCameraTrackballZoomManipulator.newInstance();
      // zoomManipulatorScroll.setDragEnabled(false);
      // zoomManipulatorScroll.setScrollEnabled(true);
      // istyle.addMouseManipulator(zoomManipulatorScroll);

      const interactor = renderWindow.getInteractor();
      if (interactor) {
        interactor.setInteractorStyle(istyle);
        istyle.setInteractor(interactor);
        renderWindow.render();
      }
    }
  };

  const handleActiveDistance = () => {
    if (genericRender) {
      setActiveTool(TOOLS_3D.DISTANCE);
      const renderWindow = genericRender.getRenderWindow();
      const istyle = vtkInteractorStyle3DLength.newInstance();
      istyle.setGenericRenderWindow(genericRender);
      istyle.setVolumeMapper(volumeMapper);
      istyle.setImageDataObject(props.imageDataObject);
      istyle.setToolsState(toolsState);

      const interactor = renderWindow.getInteractor();
      if (interactor) {
        interactor.setInteractorStyle(istyle);
        istyle.setInteractor(interactor);
        renderWindow.render();
      }
    }
  };

  const handleActiveRotate3D = () => {
    if (genericRender) {
      setActiveTool(TOOLS_3D.ROTATE);
      const renderWindow = genericRender.getRenderWindow();
      const interactorStyle = vtkInteractorStyle3DRotate.newInstance();
      const bounds = volumeActor.getMapper().getBounds();
      let center = [
        (bounds[0] + bounds[1]) / 2.0,
        (bounds[2] + bounds[3]) / 2.0,
        (bounds[4] + bounds[5]) / 2.0,
      ];

      interactorStyle.setCenterOfRotation(center);
      interactorStyle.setGenericRenderWindow(genericRender);
      interactorStyle.setToolsState(toolsState);

      renderWindow.getInteractor().setInteractorStyle(interactorStyle);
      renderWindow.render();
    }
  };

  const handleReset3D = () => {
    toolsState = {};
    if (genericRender && volumeActor) {
      clearCanvas(genericRender);
      // if (isRemovedBed) {
      setRemovedBed(false);
      const { vtkImageData: imgData } = props.imageDataObject;
      const pointsData = getScalarsData(imgData);

      volumeMapper
        .getInputData()
        .getPointData()
        .getScalars()
        .setData(pointsData, 1);

      volumeMapper.getInputData().modified();
      handleActiveRotate3D();
      // }

      const preset = presets.find(preset => preset.id === currentPreset);
      applyPreset(
        volumeActor,
        preset,
        setShade,
        setAmbient,
        setDiffuse,
        setSpecular,
        setSpecularPower,
        setOpacityDistance
      );

      const renderWindow = genericRender.getRenderWindow();
      const renderer = genericRender.getRenderer();
      const camera = renderer.getActiveCamera();
      camera.setPosition(
        initCameraInfo.position[0],
        initCameraInfo.position[1],
        initCameraInfo.position[2]
      );
      camera.setViewUp(initCameraInfo.viewUp);
      renderer.resetCamera();
      renderer.updateLightsGeometryToFollowCamera();
      if (orientationWidget) {
        orientationWidget.updateMarkerOrientation();
      }
      renderWindow.render();
    }
  };

  const handleRemoveBed = () => {
    try {
      if (genericRender && !isProcessing && !isRemovedBed) {
        setProcessing(true);
        setTimeout(() => {
          const renderWindow = genericRender.getRenderWindow();
          const { metaData0 = {} } = props.imageDataObject || {};
          const w = metaData0.columns;
          const h = metaData0.rows;
          if (!w || !h) return;

          const scalarsData = volumeMapper
            .getInputData()
            .getPointData()
            .getScalars()
            .getData();

          let sliceIdx = 0;

          for (let i = 0; i < scalarsData.length; i += w * h) {
            const slice = scalarsData.slice(i, w * h * (sliceIdx + 1));
            let newData = bed_remove(slice, h, w);
            let n = 0;
            for (let k = i; k < w * h * (sliceIdx + 1); k++) {
              scalarsData[k] = newData[n++];
            }
            sliceIdx++;
            newData = null;
          }

          volumeMapper.getInputData().modified();
          renderWindow.render();
          setRemovedBed(true);
          setProcessing(false);
        }, 300);
      }
    } catch (error) {}
  };

  const handleActiveRecTool = () => {
    if (genericRender) {
      setActiveTool(TOOLS_3D.RECT_SCALPEL);
      const renderWindow = genericRender.getRenderWindow();
      const istyle = vtkInteractorStyleScalpelRect.newInstance();
      istyle.setGenericRenderWindow(genericRender);
      istyle.setImageDataObject(props.imageDataObject);
      istyle.setOnCutting(() => {
        setIsCutting(true);
      });
      istyle.setOnCuttingDone(() => {
        setIsCutting(false);
      });
      const interactor = renderWindow.getInteractor();
      if (interactor) {
        interactor.setInteractorStyle(istyle);
        istyle.setInteractor(interactor);
        renderWindow.render();
      }
    }
  };

  const handleActiveFreehandTool = () => {
    if (genericRender) {
      setActiveTool(TOOLS_3D.FREEHAND_SCALPEL);
      const renderWindow = genericRender.getRenderWindow();
      const istyle = vtkInteractorStyleScalpelFreehand.newInstance();
      istyle.setGenericRenderWindow(genericRender);
      istyle.setImageDataObject(props.imageDataObject);
      istyle.setOnCutting(() => {
        setIsCutting(true);
      });
      istyle.setOnCuttingDone(() => {
        setIsCutting(false);
      });
      const interactor = renderWindow.getInteractor();
      if (interactor) {
        interactor.setInteractorStyle(istyle);
        istyle.setInteractor(interactor);
        renderWindow.render();
      }
    }
  };

  const handleActionScalpel = key => {
    if (genericRender) {
      const renderWindow = genericRender.getRenderWindow();
      const interactor = renderWindow.getInteractor();
      const istyle = interactor.getInteractorStyle();

      if (istyle) {
        if (key === KEY.DEL && istyle.cutInside) {
          istyle.cutInside();
        } else if (key === KEY.ENTER && istyle.cutOutside) {
          istyle.cutOutside();
        } else if (key === KEY.ESC && istyle.clearArea) {
          istyle.clearArea();
        }
        setIsCutting(false);
      }
    }
  };

  const advanceContent = (
    <div className="advance-content" onDoubleClick={e => e.stopPropagation()}>
      <div className="advance-list">
        <div className="adv-option">
          <div className="property-name">Shade:</div>
          <div className="property-data">
            <Checkbox
              checked={shade}
              onChange={e =>
                handleChangeProperty(PROPERTY_TYPE.SHADE, e.target.checked)
              }
            />
          </div>
        </div>
        <div className="adv-option">
          <div className="property-name">Ambient:</div>
          <div className="property-data">
            <InputNumber
              className="input-value"
              min={0}
              max={1}
              step={0.01}
              value={ambient}
              size="small"
              onChange={value =>
                handleChangeProperty(PROPERTY_TYPE.AMBIENT, value)
              }
            />
          </div>
        </div>
        <div className="adv-option">
          <div className="property-name">Diffuse:</div>
          <div className="property-data">
            <InputNumber
              className="input-value"
              min={0}
              max={1}
              step={0.01}
              value={diffuse}
              size="small"
              onChange={value =>
                handleChangeProperty(PROPERTY_TYPE.DIFFUSE, value)
              }
            />
          </div>
        </div>
        <div className="adv-option">
          <div className="property-name">Specular:</div>
          <div className="property-data">
            <InputNumber
              className="input-value"
              min={0}
              max={1}
              step={0.01}
              value={specular}
              size="small"
              onChange={value =>
                handleChangeProperty(PROPERTY_TYPE.SPECULAR, value)
              }
            />
          </div>
        </div>
        <div className="adv-option">
          <div className="property-name">Specular power:</div>
          <div className="property-data">
            <InputNumber
              className="input-value"
              min={0}
              max={100}
              step={0.1}
              value={specularPower}
              size="small"
              onChange={value =>
                handleChangeProperty(PROPERTY_TYPE.SPECULAR_POWER, value)
              }
            />
          </div>
        </div>
        <div className="adv-option">
          <div className="property-name">Opacity distance:</div>
          <div className="property-data">
            <InputNumber
              className="input-value"
              min={0}
              max={10}
              step={0.1}
              value={opacityDistance}
              size="small"
              onChange={value =>
                handleChangeProperty(PROPERTY_TYPE.OPACITY_DISTANCE, value)
              }
            />
          </div>
        </div>
        <div className="adv-option">
          <div className="property-name">{t('Quality')}:</div>
          <div className="property-data">
            <Select
              size="small"
              dropdownClassName="dropdown-options-dark vindr-dropdown"
              menuItemSelectedIcon={<CheckOutlined />}
              suffixIcon={<CaretDownOutlined />}
              defaultValue="2"
              style={{ minWidth: 90 }}
              onChange={value =>
                handleChangeProperty(PROPERTY_TYPE.QUALITY, value)
              }
            >
              <Select.Option value="1">{t('Normal Quality')}</Select.Option>
              <Select.Option value="2">{t('High Quality')}</Select.Option>
              {/* <Select.Option value="3">Highest</Select.Option> */}
            </Select>
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <div className="view-3d-content">
      <Spin spinning={isProcessing} tip="Processing...">
        <div className="toolbar-3d">
          <div className="toolbar-3d-list">
            <Tooltip title={t('3D Rotate')}>
              <div
                onClick={handleActiveRotate3D}
                className={`toolbar-3d-item ${
                  activeTool === TOOLS_3D.ROTATE ? 'is-active' : ''
                }`}
              >
                <Icon name="3d-rotate" />
              </div>
            </Tooltip>
            <Tooltip title={t('Adjust window')}>
              <div
                onClick={handleAdjustWindow}
                className={`toolbar-3d-item ${
                  activeTool === TOOLS_3D.ADJUST_WINDOW ? 'is-active' : ''
                }`}
              >
                <Icon name="brightness" />
              </div>
            </Tooltip>
            <Tooltip title={t('Crosshairs')}>
              <div
                onClick={handleActiveCrosshairs}
                className={`toolbar-3d-item ${
                  activeTool === TOOLS_3D.CROSSHAIR ? 'is-active' : ''
                }`}
              >
                <Icon name="crossHair" />
              </div>
            </Tooltip>
            <Tooltip title={t('Length')}>
              <div
                onClick={handleActiveDistance}
                className={`toolbar-3d-item ${
                  activeTool === TOOLS_3D.DISTANCE ? 'is-active' : ''
                }`}
              >
                <Icon name="straighten" />
              </div>
            </Tooltip>
            <Tooltip title={t('Remove bed')}>
              <div onClick={handleRemoveBed} className="toolbar-3d-item">
                <Icon name="remove-bed" />
              </div>
            </Tooltip>
            <Tooltip title={t('Rectangle Scalpel')}>
              <div
                onClick={handleActiveRecTool}
                className={`toolbar-3d-item ${
                  activeTool === TOOLS_3D.RECT_SCALPEL ? 'is-active' : ''
                }`}
              >
                <Icon name="rectScalpel" />
              </div>
            </Tooltip>
            <Tooltip title={t('Freehand Scalpel')}>
              <div
                onClick={handleActiveFreehandTool}
                className={`toolbar-3d-item ${
                  activeTool === TOOLS_3D.FREEHAND_SCALPEL ? 'is-active' : ''
                }`}
              >
                <Icon name="freehandScalpel" />
              </div>
            </Tooltip>
            <Popover
              placement="bottomLeft"
              content={advanceContent}
              trigger="click"
              overlayClassName="popover-advance-option"
            >
              <div className="toolbar-3d-item">
                <SettingOutlined />
              </div>
            </Popover>
            <Tooltip title={t('Reset')}>
              <div onClick={handleReset3D} className="toolbar-3d-item">
                <Icon name="refresh" />
              </div>
            </Tooltip>
          </div>
        </div>
        <div className="view-3d">
          <div
            className={`volume-rendering ${cursorMapper(activeTool)}`}
            ref={volumeContainer}
            style={{
              height: '100%',
              width: '100%',
              margin: '0 auto',
            }}
          ></div>
          {isCutting && (
            <div className="cutting-toolbar">
              <div
                onClick={() => handleActionScalpel(KEY.DEL)}
                className="cutting-toolbar-item"
              >
                <span>Cut inside</span>
                <span>Del</span>
              </div>
              <div
                onClick={() => handleActionScalpel(KEY.ENTER)}
                className="cutting-toolbar-item"
              >
                <span>Cut outside</span>
                <span>Enter</span>
              </div>
              <div
                onClick={() => handleActionScalpel(KEY.ESC)}
                className="cutting-toolbar-item"
              >
                <span>Clear area</span>
                <span>Esc</span>
              </div>
            </div>
          )}
          <Select
            size="small"
            dropdownClassName="dropdown-options-dark vindr-dropdown"
            menuItemSelectedIcon={<CheckOutlined />}
            suffixIcon={<CaretDownOutlined />}
            style={{ width: 190, position: 'absolute', bottom: 20, right: 20 }}
            defaultValue={currentPreset}
            onChange={handleChangePreset}
          >
            {presets.map(preset => (
              <Select.Option key={preset.id} value={preset.id}>
                {preset.name}
              </Select.Option>
            ))}
          </Select>
        </div>
      </Spin>
    </div>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(View3D);

function _getRangeFromWindowLevels(width, center, Modality = undefined) {
  // For PET just set the range to 0-5 SUV
  if (Modality === 'PT') {
    return { lower: 0, upper: 5 };
  }

  const levelsAreNotNumbers = isNaN(center) || isNaN(width);

  if (levelsAreNotNumbers) {
    return { lower: 0, upper: 512 };
  }

  return {
    lower: center - width / 2.0,
    upper: center + width / 2.0,
  };
}

function getShiftRange(colorTransferArray) {
  let lower = Infinity;
  let upper = -Infinity;
  for (let i = 0; i < colorTransferArray.length; i += 4) {
    lower = Math.min(lower, colorTransferArray[i]);
    upper = Math.max(upper, colorTransferArray[i]);
  }

  const center = (upper - lower) / 2;

  return {
    shiftRange: [-center, center],
    lower,
    upper,
  };
}

function applyPointsToPiecewiseFunction(points, range, pwf) {
  // const width = range[1] - range[0];
  // const rescaled = points.map(([x, y]) => [x * width + range[0], y]);
  pwf.removeAllPoints();
  points.forEach(([x, y]) => pwf.addPoint(x, y));
  // return rescaled;
}

function applyPointsToRGBFunction(points, range, cfun) {
  // const width = range[1] - range[0];
  // const rescaled = points.map(([x, r, g, b]) => [
  //   x * width + range[0],
  //   r,
  //   g,
  //   b,
  // ]);
  cfun.removeAllPoints();
  points.forEach(([x, r, g, b]) => cfun.addRGBPoint(x, r, g, b));
  // return rescaled;
}

function applyPreset(
  actor,
  preset,
  setShade,
  setAmbient,
  setDiffuse,
  setSpecular,
  setSpecularPower,
  setOpacityDistance
  // setLower,
  // setUpper
) {
  // Create color transfer function
  const colorTransferArray = preset.colorTransfer
    .split(' ')
    .splice(1)
    .map(parseFloat);

  const { shiftRange, lower, upper } = getShiftRange(colorTransferArray);
  // let min = shiftRange[0];
  // const width = shiftRange[1] - shiftRange[0];

  cfun = vtkColorTransferFunction.newInstance();
  const normColorTransferValuePoints = [];
  for (let i = 0; i < colorTransferArray.length; i += 4) {
    let value = colorTransferArray[i];
    const r = colorTransferArray[i + 1];
    const g = colorTransferArray[i + 2];
    const b = colorTransferArray[i + 3];

    // value = (value - min) / width;
    normColorTransferValuePoints.push([value, r, g, b]);
  }

  applyPointsToRGBFunction(normColorTransferValuePoints, shiftRange, cfun);
  actor.getProperty().setRGBTransferFunction(0, cfun);

  // Create scalar opacity function
  const scalarOpacityArray = preset.scalarOpacity
    .split(' ')
    .splice(1)
    .map(parseFloat);

  ofun = vtkPiecewiseFunction.newInstance();
  const normPoints = [];
  for (let i = 0; i < scalarOpacityArray.length; i += 2) {
    let value = scalarOpacityArray[i];
    const opacity = scalarOpacityArray[i + 1];

    // value = (value - min) / width;
    normPoints.push([value, opacity]);
  }

  applyPointsToPiecewiseFunction(normPoints, shiftRange, ofun);
  actor.getProperty().setScalarOpacity(0, ofun);

  const [
    gradientMinValue,
    gradientMinOpacity,
    gradientMaxValue,
    gradientMaxOpacity,
  ] = preset.gradientOpacity
    .split(' ')
    .splice(1)
    .map(parseFloat);

  actor.getProperty().setUseGradientOpacity(0, true);
  actor.getProperty().setGradientOpacityMinimumValue(0, gradientMinValue);
  actor.getProperty().setGradientOpacityMinimumOpacity(0, gradientMinOpacity);

  actor.getProperty().setGradientOpacityMaximumValue(0, gradientMaxValue);
  actor.getProperty().setGradientOpacityMaximumOpacity(0, gradientMaxOpacity);

  if (preset.interpolation === '1') {
    actor.getProperty().setInterpolationTypeToFastLinear();
  }

  const ambient = parseFloat(preset.ambient);
  const shade = preset.shade === '1';
  const diffuse = parseFloat(preset.diffuse);
  const specular = parseFloat(preset.specular);
  const specularPower = parseFloat(preset.specularPower);
  const opacityDistance = 1.5;

  actor.getProperty().setShade(shade);
  actor.getProperty().setAmbient(ambient);
  actor.getProperty().setDiffuse(diffuse);
  actor.getProperty().setSpecular(specular);
  actor.getProperty().setSpecularPower(specularPower);
  actor.getProperty().setScalarOpacityUnitDistance(0, opacityDistance);

  // // set state
  // setLower(lower);
  // setUpper(upper);
  setShade(shade);
  setAmbient(ambient);
  setDiffuse(diffuse);
  setSpecular(specular);
  setSpecularPower(specularPower);
  setOpacityDistance(opacityDistance);
}

function addOrientationMarker(renderWindow) {
  // create axes
  const axes = vtkAnnotatedCubeActor.newInstance();
  axes.setDefaultStyle({
    text: 'L',
    fontStyle: 'bold',
    fontFamily: 'Arial',
    fontColor: '#ffffff',
    fontSizeScale: res => res / 1.2,
    faceColor: '#0000ff',
    faceRotation: 0,
    edgeThickness: 0,
    edgeColor: 'black',
    resolution: 200,
  });

  axes.setXPlusFaceProperty({
    text: 'L',
    faceRotation: 90,
    faceColor: '#1976d2',
  });
  axes.setXMinusFaceProperty({
    text: 'R',
    faceColor: '#1976d2',
    faceRotation: -90,
  });
  axes.setYPlusFaceProperty({
    text: 'P',
    faceColor: '#d32f2f',
    faceRotation: 180,
  });
  axes.setYMinusFaceProperty({
    text: 'A',
    faceColor: '#d32f2f',
  });
  axes.setZPlusFaceProperty({
    text: 'H',
    faceColor: ' #2e7d32',
  });
  axes.setZMinusFaceProperty({
    text: 'F',
    faceRotation: 180,
    faceColor: ' #2e7d32',
  });

  // create orientation widget
  orientationWidget = vtkOrientationMarkerWidget.newInstance({
    actor: axes,
    interactor: renderWindow.getInteractor(),
  });
  orientationWidget.setEnabled(true);
  orientationWidget.setViewportCorner(
    vtkOrientationMarkerWidget.Corners.TOP_RIGHT
  );
  orientationWidget.setViewportSize(0.05);
  orientationWidget.setMinPixelSize(50);
  orientationWidget.setMaxPixelSize(200);
}
