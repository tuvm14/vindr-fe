import vtkCoordinate from 'vtk.js/Sources/Rendering/Core/Coordinate';

export function computedLocalDisplayValue(worldPos, renderer) {
  const vtkCoor = vtkCoordinate.newInstance();
  vtkCoor.setCoordinateSystemToWorld();
  vtkCoor.setValue(...worldPos);

  const point = vtkCoor.getComputedLocalDisplayValue(renderer);
  return { x: point[0], y: point[1] };
}

export function drawHandlesVtk(context, handles = {}) {
  context.save();
  context.lineWidth = 2;
  context.strokeStyle = 'rgb(255, 255, 0)';
  if (handles.active) {
    context.strokeStyle = 'rgb(255, 0, 0)';
  }
  context.beginPath();
  context.moveTo(handles.x - 6, handles.y);
  context.lineTo(handles.x + 6, handles.y);
  context.moveTo(handles.x, handles.y - 6);
  context.lineTo(handles.x, handles.y + 6);
  context.closePath();
  context.stroke();
  context.restore();
}

export function drawLengthVtk(context, handles = {}, distanceText) {
  const { start, end } = handles;

  context.save();
  context.lineWidth = 2;
  context.strokeStyle = 'rgb(255, 255, 0)';
  if (handles.active) {
    context.strokeStyle = 'rgb(255, 0, 0)';
  }
  context.beginPath();
  context.moveTo(start.x, start.y);
  context.lineTo(end.x, end.y);

  context.textBaseline = 'top';
  context.font = '16px Arial';
  const measurementWidth = context.measureText(distanceText).width;
  context.fillStyle = 'rgba(5, 32, 48, 0.7)';
  context.fillRect(end.x + 5, end.y - 5, measurementWidth + 10, 24);
  context.fillStyle = 'rgb(255, 255, 0)';
  if (handles.active) {
    context.fillStyle = 'rgb(255, 0, 0)';
  }

  context.fillText(distanceText, end.x + 10, end.y);
  context.closePath();
  context.stroke();
  context.restore();

  if (handles.active) {
    drawHandlesVtk(context, { ...start, active: true });
    drawHandlesVtk(context, { ...end, active: true });
  }
}

export function clearCanvasVtk(context) {
  context.clearRect(0, 0, context.canvas.width, context.canvas.height);
}

export function removeVtkAnnotations(apis) {
  try {
    apis.forEach(api => {
      api.toolsState = {};
      const measurementElm = api.container.querySelector(`.measurement-canvas`);
      const context = measurementElm.getContext('2d');
      clearCanvasVtk(context);
    });
  } catch (error) {
    console.log('removeVtkAnnotations fail');
  }
}
