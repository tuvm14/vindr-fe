import macro from 'vtk.js/Sources/macro';
import vtkInteractorStyleManipulator from 'vtk.js/Sources/Interaction/Style/InteractorStyleManipulator';
import { clearCanvasVtk } from '../tools/drawing';
import vtkCoordinate from 'vtk.js/Sources/Rendering/Core/Coordinate';
import { polygonBeamCast } from './helper';

function vtkInteractorStyleScalpelRect(publicAPI, model) {
  // Set our className
  model.classHierarchy.push('vtkInteractorStyleRect');
  let genericRender = {};
  let isDrawing = false;
  let mouseStart = null;
  let mouseEnd = null;
  const vtkCoor = vtkCoordinate.newInstance();
  vtkCoor.setCoordinateSystemToWorld();

  publicAPI.setGenericRenderWindow = genericRenderWindow => {
    genericRender = genericRenderWindow;
  };
  publicAPI.setImageDataObject = imageDataObject => {
    model.imageDataObject = imageDataObject;
  };

  const superHandleMouseMove = publicAPI.handleMouseMove;
  publicAPI.handleMouseMove = callData => {
    if (isDrawing) {
      mouseEnd = callData.position;
      const container = genericRender.getContainer();
      const measurementElm = container.querySelector(`.measurement-canvas`);
      let context = measurementElm.getContext('2d');
      clearCanvasVtk(context);

      context.save();

      context.lineWidth = 2;
      context.strokeStyle = 'rgb(0, 255, 0)';
      context.fillStyle = 'rgb(0, 255, 0, 0.3)';

      context.beginPath();
      let start = { x: mouseStart.x, y: context.canvas.height - mouseStart.y };
      let end = {
        x: callData.position.x,
        y: context.canvas.height - callData.position.y,
      };

      const w = Math.abs(start.x - end.x);
      const h = Math.abs(start.y - end.y);

      let corner1 = {
        x: Math.min(start.x, end.x),
        y: Math.min(start.y, end.y),
      };

      context.rect(corner1.x, corner1.y, w, h);
      context.closePath();
      context.stroke();
      context.fill();
      context.restore();
    }

    if (superHandleMouseMove) {
      superHandleMouseMove(callData);
    }
  };

  const superHandleLeftButtonPress = publicAPI.handleLeftButtonPress;
  publicAPI.handleLeftButtonPress = callData => {
    isDrawing = true;
    mouseStart = callData.position;

    if (superHandleLeftButtonPress) {
      superHandleLeftButtonPress(callData);
    }
  };

  publicAPI.handleLeftButtonRelease = callData => {
    isDrawing = false;
    const onCutting = publicAPI.getOnCutting();
    if (onCutting) {
      onCutting();
    }
  };

  publicAPI.cutInside = () => {
    if (mouseStart && mouseEnd) {
      const renderer = genericRender.getRenderer();
      const renderWindow = genericRender.getRenderWindow();
      const container = genericRender.getContainer();
      const measurementElm = container.querySelector(`.measurement-canvas`);
      let context = measurementElm.getContext('2d');
      const points = [
        { x: mouseStart.x, y: mouseStart.y, z: 0.0 },
        { x: mouseStart.x, y: mouseEnd.y, z: 0.0 },
        { x: mouseEnd.x, y: mouseEnd.y, z: 0.0 },
        { x: mouseEnd.x, y: mouseStart.y, z: 0.0 },
      ];

      const spacing = model.imageDataObject.spacing;
      const dimensions = model.imageDataObject.dimensions;

      const volumes = renderer.getVolumes();
      const currentVolumes = volumes && volumes[0];
      const mapper = currentVolumes.getMapper();
      const inputData = mapper.getInputData();
      const scalarsData = inputData
        .getPointData()
        .getScalars()
        .getData();

      polygonBeamCast(
        currentVolumes,
        renderer,
        points,
        spacing,
        dimensions,
        scalarsData,
        false
      );

      inputData.modified();
      renderWindow.render();
      clearCanvasVtk(context);
      mouseStart = null;
      mouseEnd = null;
    }
  };

  publicAPI.cutOutside = () => {
    if (mouseStart && mouseEnd) {
      const renderer = genericRender.getRenderer();
      const renderWindow = genericRender.getRenderWindow();
      const container = genericRender.getContainer();
      const measurementElm = container.querySelector(`.measurement-canvas`);
      let context = measurementElm.getContext('2d');
      const points = [
        { x: mouseStart.x, y: mouseStart.y, z: 0.0 },
        { x: mouseStart.x, y: mouseEnd.y, z: 0.0 },
        { x: mouseEnd.x, y: mouseEnd.y, z: 0.0 },
        { x: mouseEnd.x, y: mouseStart.y, z: 0.0 },
      ];

      const spacing = model.imageDataObject.spacing;
      const dimensions = model.imageDataObject.dimensions;

      const volumes = renderer.getVolumes();
      const currentVolumes = volumes && volumes[0];
      const mapper = currentVolumes.getMapper();
      const inputData = mapper.getInputData();
      const scalarsData = inputData
        .getPointData()
        .getScalars()
        .getData();

      polygonBeamCast(
        currentVolumes,
        renderer,
        points,
        spacing,
        dimensions,
        scalarsData,
        true
      );

      inputData.modified();
      renderWindow.render();
      clearCanvasVtk(context);
      mouseStart = null;
      mouseEnd = null;
    }
  };

  publicAPI.clearArea = () => {
    mouseStart = null;
    mouseEnd = null;
    const container = genericRender.getContainer();
    const measurementElm = container.querySelector(`.measurement-canvas`);
    let context = measurementElm.getContext('2d');
    clearCanvasVtk(context);
  };

  publicAPI.handleKeyUp = callData => {
    if (callData && callData.key && mouseStart && mouseEnd) {
      if (callData.key === 'Enter') {
        publicAPI.cutOutside();
        const onCuttingDone = publicAPI.getOnCuttingDone();
        if (onCuttingDone) {
          onCuttingDone();
        }
      } else if (callData.key === 'Delete') {
        publicAPI.cutInside();
        const onCuttingDone = publicAPI.getOnCuttingDone();
        if (onCuttingDone) {
          onCuttingDone();
        }
      } else if (callData.key === 'Escape') {
        publicAPI.clearArea();
        const onCuttingDone = publicAPI.getOnCuttingDone();
        if (onCuttingDone) {
          onCuttingDone();
        }
      }
    }
  };
}

// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  vtkInteractorStyleManipulator.extend(publicAPI, model, initialValues);
  macro.setGet(publicAPI, model, ['onCutting', 'onCuttingDone']);
  // Object specific methods
  vtkInteractorStyleScalpelRect(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(
  extend,
  'vtkInteractorStyleScalpelRect'
);

// ----------------------------------------------------------------------------

export default Object.assign({ newInstance, extend });
