import macro from 'vtk.js/Sources/macro';
import vtkInteractorStyleManipulator from 'vtk.js/Sources/Interaction/Style/InteractorStyleManipulator';
import { clearCanvasVtk } from './drawing';
import { polygonBeamCast } from './helper';

function vtkInteractorStyleScalpelFreehand(publicAPI, model) {
  // Set our className
  model.classHierarchy.push('vtkInteractorStyleRect');
  let genericRender = {};
  let isDrawing = false;
  let pointsData = [];

  publicAPI.setGenericRenderWindow = genericRenderWindow => {
    genericRender = genericRenderWindow;
  };
  publicAPI.setImageDataObject = imageDataObject => {
    model.imageDataObject = imageDataObject;
  };

  const superHandleMouseMove = publicAPI.handleMouseMove;
  publicAPI.handleMouseMove = callData => {
    if (isDrawing) {
      pointsData.push(callData.position);

      const container = genericRender.getContainer();
      const measurementElm = container.querySelector(`.measurement-canvas`);
      let context = measurementElm.getContext('2d');
      clearCanvasVtk(context);

      context.save();

      context.lineWidth = 2;
      context.strokeStyle = 'rgb(0, 255, 0)';
      context.fillStyle = 'rgb(0, 255, 0, 0.3)';

      context.beginPath();

      context.moveTo(pointsData[0].x, context.canvas.height - pointsData[0].y);
      for (let i = 1; i < pointsData.length; i++) {
        context.lineTo(
          pointsData[i].x,
          context.canvas.height - pointsData[i].y
        );
      }

      context.closePath();
      context.stroke();
      context.fill();
      context.restore();
    }

    if (superHandleMouseMove) {
      superHandleMouseMove(callData);
    }
  };

  const superHandleLeftButtonPress = publicAPI.handleLeftButtonPress;
  publicAPI.handleLeftButtonPress = callData => {
    isDrawing = true;
    pointsData = [callData.position];

    if (superHandleLeftButtonPress) {
      superHandleLeftButtonPress(callData);
    }
  };

  publicAPI.handleLeftButtonRelease = callData => {
    isDrawing = false;
    const onCutting = publicAPI.getOnCutting();
    if (onCutting) {
      onCutting();
    }
  };

  publicAPI.cutInside = () => {
    if (pointsData) {
      const renderer = genericRender.getRenderer();
      const renderWindow = genericRender.getRenderWindow();

      const spacing = model.imageDataObject.spacing;
      const dimensions = model.imageDataObject.dimensions;

      const volumes = renderer.getVolumes();
      const currentVolumes = volumes && volumes[0];
      const mapper = currentVolumes.getMapper();
      const inputData = mapper.getInputData();
      const scalarsData = inputData
        .getPointData()
        .getScalars()
        .getData();

      polygonBeamCast(
        currentVolumes,
        renderer,
        pointsData,
        spacing,
        dimensions,
        scalarsData,
        false
      );

      inputData.modified();
      renderWindow.render();
      pointsData = [];
      const container = genericRender.getContainer();
      const measurementElm = container.querySelector(`.measurement-canvas`);
      let context = measurementElm.getContext('2d');
      clearCanvasVtk(context);
    }
  };

  publicAPI.cutOutside = () => {
    if (pointsData) {
      const renderer = genericRender.getRenderer();
      const renderWindow = genericRender.getRenderWindow();

      const spacing = model.imageDataObject.spacing;
      const dimensions = model.imageDataObject.dimensions;

      const volumes = renderer.getVolumes();
      const currentVolumes = volumes && volumes[0];
      const mapper = currentVolumes.getMapper();
      const inputData = mapper.getInputData();
      const scalarsData = inputData
        .getPointData()
        .getScalars()
        .getData();

      polygonBeamCast(
        currentVolumes,
        renderer,
        pointsData,
        spacing,
        dimensions,
        scalarsData,
        true
      );

      inputData.modified();
      renderWindow.render();
      pointsData = [];
      const container = genericRender.getContainer();
      const measurementElm = container.querySelector(`.measurement-canvas`);
      let context = measurementElm.getContext('2d');
      clearCanvasVtk(context);
    }
  };

  publicAPI.clearArea = () => {
    pointsData = [];
    const container = genericRender.getContainer();
    const measurementElm = container.querySelector(`.measurement-canvas`);
    let context = measurementElm.getContext('2d');
    clearCanvasVtk(context);
  };

  publicAPI.handleKeyUp = callData => {
    if (callData && callData.key && pointsData) {
      if (callData.key === 'Enter') {
        publicAPI.cutOutside();
        const onCuttingDone = publicAPI.getOnCuttingDone();
        if (onCuttingDone) {
          onCuttingDone();
        }
      } else if (callData.key === 'Delete') {
        publicAPI.cutInside();
        const onCuttingDone = publicAPI.getOnCuttingDone();
        if (onCuttingDone) {
          onCuttingDone();
        }
      } else if (callData.key === 'Escape') {
        publicAPI.clearArea();
        const onCuttingDone = publicAPI.getOnCuttingDone();
        if (onCuttingDone) {
          onCuttingDone();
        }
      }
    }
  };
}

// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  vtkInteractorStyleManipulator.extend(publicAPI, model, initialValues);
  macro.setGet(publicAPI, model, ['onCutting', 'onCuttingDone']);
  // Object specific methods
  vtkInteractorStyleScalpelFreehand(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(
  extend,
  'vtkInteractorStyleScalpelFreehand'
);

// ----------------------------------------------------------------------------

export default Object.assign({ newInstance, extend });
