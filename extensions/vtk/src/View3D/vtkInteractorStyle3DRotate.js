import macro from 'vtk.js/Sources/macro';
import * as vtkMath from 'vtk.js/Sources/Common/Core/Math';
import vtkInteractorStyleManipulator from 'vtk.js/Sources/Interaction/Style/InteractorStyleManipulator';
import vtkMouseCameraTrackballRotateManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballRotateManipulator';
import vtkMouseCameraTrackballPanManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballPanManipulator';
import vtkMouseCameraTrackballZoomManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballZoomManipulator';
import {
  clearCanvasVtk,
  computedLocalDisplayValue,
  drawLengthVtk,
} from './drawing';

function vtkInteractorStyle3DRotate(publicAPI, model) {
  // Set our className
  model.classHierarchy.push('vtkInteractorStyle3DRotate');

  const rotateManipulator = vtkMouseCameraTrackballRotateManipulator.newInstance();
  rotateManipulator.setButton(1); // Left Button
  publicAPI.addMouseManipulator(rotateManipulator);

  const zoomManipulatorScroll = vtkMouseCameraTrackballZoomManipulator.newInstance();
  zoomManipulatorScroll.setDragEnabled(false);
  zoomManipulatorScroll.setScrollEnabled(true);
  publicAPI.addMouseManipulator(zoomManipulatorScroll);

  const zoomManipulatorRightMouse = vtkMouseCameraTrackballZoomManipulator.newInstance();
  zoomManipulatorRightMouse.setButton(3); // Right button
  publicAPI.addMouseManipulator(zoomManipulatorRightMouse);

  const panManipulatorMiddleButton = vtkMouseCameraTrackballPanManipulator.newInstance();
  panManipulatorMiddleButton.setButton(2); // Middle Button
  publicAPI.addMouseManipulator(panManipulatorMiddleButton);

  const panManipulatorLeftMouse = vtkMouseCameraTrackballPanManipulator.newInstance();
  panManipulatorLeftMouse.setButton(1); // Left button
  panManipulatorLeftMouse.setShift(true);
  publicAPI.addMouseManipulator(panManipulatorLeftMouse);

  publicAPI.setGenericRenderWindow = genericRenderWindow => {
    model.genericRenderWindow = genericRenderWindow;
  };
  publicAPI.setToolsState = toolsState => {
    model.toolsState = toolsState;
  };

  function renderToolData(api, context, renderer) {
    clearCanvasVtk(context);
    const toolData = api.toolsState && api.toolsState['vtkLength'];

    if (!toolData || !(toolData.data || []).length) {
      return;
    }

    for (let i = 0; i < toolData.data.length; i++) {
      const data = toolData.data[i];
      const distance = vtkMath.distance2BetweenPoints(
        data.worldPosStart,
        data.worldPosEnd
      );
      const distanceText = `${Math.sqrt(distance).toFixed(2)} mm`;

      const start = computedLocalDisplayValue(data.worldPosStart, renderer);
      const end = computedLocalDisplayValue(data.worldPosEnd, renderer);
      const handles = { ...data, start, end };

      drawLengthVtk(context, handles, distanceText);
    }
  }

  const superHandleMouseMove = publicAPI.handleMouseMove;
  publicAPI.handleMouseMove = callData => {
    const { genericRenderWindow } = model;

    if (genericRenderWindow) {
      const renderer = genericRenderWindow.getRenderer();
      const container = genericRenderWindow.getContainer();
      const measurementElm = container.querySelector(`.measurement-canvas`);
      let context = measurementElm.getContext('2d');

      renderToolData(model, context, renderer);
    }

    if (superHandleMouseMove) {
      superHandleMouseMove(callData);
    }
  };

  const superHandleLeftButtonRelease = publicAPI.handleLeftButtonRelease;
  publicAPI.handleLeftButtonRelease = callData => {
    const { genericRenderWindow } = model;

    if (genericRenderWindow) {
      const renderer = genericRenderWindow.getRenderer();
      const container = genericRenderWindow.getContainer();
      const measurementElm = container.querySelector(`.measurement-canvas`);
      let context = measurementElm.getContext('2d');

      renderToolData(model, context, renderer);
    }

    if (superHandleMouseMove) {
      superHandleLeftButtonRelease(callData);
    }
  };
}

const DEFAULT_VALUES = {};

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  vtkInteractorStyleManipulator.extend(publicAPI, model, initialValues);

  // Object specific methods
  vtkInteractorStyle3DRotate(publicAPI, model);
}

export const newInstance = macro.newInstance(
  extend,
  'vtkInteractorStyle3DRotate'
);

export default Object.assign({ newInstance, extend });
