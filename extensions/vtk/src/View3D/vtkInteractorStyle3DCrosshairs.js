import macro from 'vtk.js/Sources/macro';
import vtkInteractorStyleManipulator from 'vtk.js/Sources/Interaction/Style/InteractorStyleManipulator';
import Constants from 'vtk.js/Sources/Rendering/Core/InteractorStyle/Constants';
import { commandsManager } from '@tuvm/viewer/src/App';
import { worldCoordsPick } from './helper';

const { States } = Constants;

// ----------------------------------------------------------------------------
// Global methods
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// vtkInteractorStyle3DCrosshairs methods
// ----------------------------------------------------------------------------

function vtkInteractorStyle3DCrosshairs(publicAPI, model) {
  // Set our className
  model.classHierarchy.push('vtkInteractorStyle3DCrosshairs');

  const superHandleMouseMove = publicAPI.handleMouseMove;
  publicAPI.handleMouseMove = callData => {
    const pos = [callData.position.x, callData.position.y];

    if (model.state === States.IS_WINDOW_LEVEL) {
      publicAPI.windowLevelFromMouse(pos);
      publicAPI.invokeInteractionEvent({ type: 'InteractionEvent' });
    }

    if (superHandleMouseMove) {
      superHandleMouseMove(callData);
    }
  };

  publicAPI.setVolumeActor = actor => {
    model.volumeActor = actor;
  };
  publicAPI.setRenderer = renderer => {
    model.renderer = renderer;
  };
  publicAPI.setVolumeMapper = volumeMapper => {
    model.volumeMapper = volumeMapper;
  };
  publicAPI.setImageDataObject = imageDataObject => {
    model.imageDataObject = imageDataObject;
  };

  const superHandleLeftButtonPress = publicAPI.handleLeftButtonPress;
  publicAPI.handleLeftButtonPress = callData => {
    if (!callData.shiftKey && !callData.controlKey) {
      if (model.renderer !== callData.pokedRenderer) {
        return;
      }

      const point = worldCoordsPick(
        model.volumeActor,
        model.volumeMapper,
        model.renderer,
        model.imageDataObject,
        callData.position
      );

      commandsManager.runCommand('moveCrosshairs', {
        position: point,
      });
    } else if (superHandleLeftButtonPress) {
      superHandleLeftButtonPress(callData);
    }
  };

  publicAPI.superHandleLeftButtonRelease = publicAPI.handleLeftButtonRelease;
  publicAPI.handleLeftButtonRelease = () => {
    switch (model.state) {
      case States.IS_WINDOW_LEVEL:
        publicAPI.endWindowLevel();
        break;

      default:
        publicAPI.superHandleLeftButtonRelease();
        break;
    }
  };
}

// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  vtkInteractorStyleManipulator.extend(publicAPI, model, initialValues);

  // Object specific methods
  vtkInteractorStyle3DCrosshairs(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(
  extend,
  'vtkInteractorStyle3DCrosshairs'
);

// ----------------------------------------------------------------------------

export default Object.assign({ newInstance, extend });
