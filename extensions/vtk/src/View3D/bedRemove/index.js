const cv = require('./opencv.js');

export function bed_remove(buffer_dicom_array, dicom_h, dicom_w) {
  /*
    buffer_dicom_array: 1d array, contains dicom pixels value (int 16), length = dicom_h * dicom_w
    dicom_h, dicom_w: shape
  */

  var window_min = -550;
  var window_max = -549;
  var pixel_value = 0;
  let gray_img = new Uint8Array(buffer_dicom_array.length);
  let pmin = 1000000;

  buffer_dicom_array.forEach((ele, index) => {
    pmin = Math.min(pmin, buffer_dicom_array[index]);
    pixel_value = buffer_dicom_array[index];

    pixel_value = pixel_value >= window_max ? 255 : 0;
    gray_img[index] = pixel_value;
  });

  // const canvas = createCanvas(600, 600);
  let gray_mat = cv.matFromArray(dicom_h, dicom_w, cv.CV_8UC1, gray_img);
  gray_img = null;
  let dsize = new cv.Size(256, 256);
  cv.resize(gray_mat, gray_mat, dsize, 0, 0, cv.INTER_NEAREST);

  let morpho_output = new cv.Mat();
  let M = cv.Mat.ones(3, 5, cv.CV_8U);
  let anchor = new cv.Point(-1, -1);
  cv.morphologyEx(
    gray_mat,
    morpho_output,
    cv.MORPH_OPEN,
    M,
    anchor,
    1,
    cv.BORDER_CONSTANT,
    cv.morphologyDefaultBorderValue()
  );
  M.delete();
  let morpho_raw = morpho_output.clone();
  gray_mat.delete();

  let mask = new cv.Mat(
    morpho_output.rows + 2,
    morpho_output.cols + 2,
    cv.CV_8UC1,
    [0, 0, 0, 0]
  );
  let white_mask = new cv.Mat(
    morpho_output.rows,
    morpho_output.cols,
    cv.CV_8UC1,
    [255, 255, 255, 255]
  );
  let dst = new cv.Mat();

  cv.floodFill(morpho_output, mask, { x: 0, y: 0 }, [255, 255, 255, 255]);
  for (let i = 0; i < morpho_output.rows; i += 30) {
    if (morpho_output.ucharAt(i, 0) == 0) {
      cv.floodFill(
        morpho_output, mask, { x: 0, y: i }, [255, 255, 255, 255]
      )
    }
    if (morpho_output.ucharAt(i, morpho_output.cols - 1) == 0) {
      cv.floodFill(
        morpho_output, mask, { x: morpho_output.cols - 1, y: i }, [
        255,
        255,
        255,
        255]
      )
    }
  }
  cv.subtract(white_mask, morpho_output, dst);
  cv.add(morpho_raw, dst, dst);
  mask.delete();
  white_mask.delete();

  let markers = new cv.Mat();
  let stat = new cv.Mat();
  let cen = new cv.Mat();

  cv.connectedComponentsWithStats(dst, markers, stat, cen);

  var id_with_area = [];
  for (let i = 0; i < stat.rows; i++) {
    id_with_area.push([stat.intAt(i, 4), i]);
  }
  id_with_area.sort(function (left, right) {
    return left[0] < right[0] ? -1 : 1;
  });
  var flag = new Int8Array(id_with_area.length).fill(0);
  flag[0] = 1
  for (let i = 0; i < id_with_area.length - 2; i++) {
    let index = id_with_area[i][1];
    let area = id_with_area[i][0];
    // console.log(index)
    let xmin = stat.intAt(index, 1);
    let xmax = stat.intAt(index, 3);
    let ymax = stat.intAt(index, 2)
    xmax = xmin + xmax;
    if (xmin > 256 * 0.7) {
      flag[index] = 1;
    } else if (xmax < 256 * 0.7) {
      if ((area < 300) || ((ymax / 256) < 0.04)) {
        flag[index] = 1;
      }
    } else {
      if (area < 1800) {
        flag[index] = 1;
      }
    }
  }
  gray_img = new Uint8Array(256*256);
  let cur_y = -1
  let cur_x = 0
  gray_img.forEach((ele, index) => {
    cur_y += 1
    if (cur_y == 256) {
      cur_y = 0;
      cur_x += 1;
    }
    var lab = markers.intAt(cur_x, cur_y);
    if (flag[lab] == 1) {
      gray_img[index] = 1;
    } else {
      gray_img[index] = 0;
    }

  });
  gray_mat = cv.matFromArray(256, 256, cv.CV_8UC1, gray_img);
  dsize = new cv.Size(dicom_w, dicom_h);
  cv.resize(gray_mat, gray_mat, dsize, 0, 0);
  var dicom_mat = cv.matFromArray(dicom_w, dicom_h, cv.CV_16SC1, buffer_dicom_array)
  dicom_mat.setTo([pmin,0,0,0], gray_mat)
  var temp = Buffer.from(dicom_mat.data)
  var ret16 = new Int16Array(temp.buffer, temp.byteOffset, temp.byteLength / Int16Array.BYTES_PER_ELEMENT);


  flag = null;
  dst.delete();
  markers.delete();
  gray_mat.delete();
  stat.delete();
  cen.delete();
  morpho_raw.delete();
  morpho_output.delete();
  dicom_mat.delete()

  // gray_mat = cv.matFromArray(dicom_h, dicom_w, cv.CV_8UC1, gray_img);
  // cv.imshow(canvas, gray_mat);
  // writeFileSync('output_5.png', canvas.toBuffer('image/png'));
  // gray_mat.delete();
  // delete gray_img;
  return ret16;
}
