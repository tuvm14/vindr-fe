import macro from 'vtk.js/Sources/macro';
import * as vtkMath from 'vtk.js/Sources/Common/Core/Math';
import { Vector3, Line3 } from 'cornerstone-math';
import { vtkInteractorStyleMPRSlice } from '@vindr/vindr-vtkjs-viewport';
import {
  clearCanvasVtk,
  drawHandlesVtk,
  drawLengthVtk,
  computedLocalDisplayValue,
} from './drawing';
import { isEmpty } from 'lodash';
import { worldCoordsPick } from './helper';

function computedSubVec(u, v) {
  let subVec = new Vector3();
  const uVec = new Vector3(...u);
  const vVec = new Vector3(...v);
  subVec.subVectors(uVec, vVec);
  return subVec;
}

function computedClosestLine(line, point) {
  let p0 = new Vector3(...point);
  let p1 = new Vector3(...line.worldPosStart);
  let p2 = new Vector3(...line.worldPosEnd);
  const line3 = new Line3(p1, p2);

  const distance = line3.closestPointToPoint(p0, true).distanceTo(p0);
  return distance || 999;
}

function computedClosestPoint(p1, p2) {
  const distance = vtkMath.distance2BetweenPoints(p1, p2);
  return distance || 999;
}

function vtkInteractorStyle3DLength(publicAPI, model) {
  // Set our className
  model.classHierarchy.push('vtkInteractorStyle3DLength');

  let isDrawing = false;
  let drawingData = {};
  let nearLineData = {};
  let isDown = false;
  let mouseStart = [];

  function renderToolData(api, context, renderer) {
    clearCanvasVtk(context);
    const toolData = api.toolsState && api.toolsState['vtkLength'];

    if (!toolData || !(toolData.data || []).length) {
      return;
    }

    for (let i = 0; i < toolData.data.length; i++) {
      const data = toolData.data[i];
      const distance = vtkMath.distance2BetweenPoints(
        data.worldPosStart,
        data.worldPosEnd
      );
      const distanceText = `${Math.sqrt(distance).toFixed(2)} mm`;

      const start = computedLocalDisplayValue(data.worldPosStart, renderer);
      const end = computedLocalDisplayValue(data.worldPosEnd, renderer);
      const handles = { ...data, start, end };

      drawLengthVtk(context, handles, distanceText);
    }
  }

  publicAPI.setGenericRenderWindow = genericRenderWindow => {
    model.genericRenderWindow = genericRenderWindow;
  };
  publicAPI.setVolumeMapper = volumeMapper => {
    model.volumeMapper = volumeMapper;
  };
  publicAPI.setImageDataObject = imageDataObject => {
    model.imageDataObject = imageDataObject;
  };
  publicAPI.setToolsState = toolsState => {
    model.toolsState = toolsState;
  };

  const superHandleMouseMove = publicAPI.handleMouseMove;
  publicAPI.handleMouseMove = callData => {
    const { genericRenderWindow } = model;

    const renderer = genericRenderWindow.getRenderer();
    const volumeActor = renderer.getVolumes()[0];
    const container = genericRenderWindow.getContainer();
    const measurementElm = container.querySelector(`.measurement-canvas`);
    let context = measurementElm.getContext('2d');

    if (isDown && !isDrawing && !isEmpty(nearLineData)) {
      let currentMouseWorlPos = worldCoordsPick(
        volumeActor,
        model.volumeMapper,
        renderer,
        model.imageDataObject,
        callData.position
      );

      const distancePStart = computedClosestPoint(
        mouseStart,
        nearLineData.worldPosStart
      );
      const distancePEnd = computedClosestPoint(
        mouseStart,
        nearLineData.worldPosEnd
      );

      let subVec = computedSubVec(currentMouseWorlPos, mouseStart);

      if (distancePStart < 1) {
        nearLineData.worldPosStart[0] += subVec.x;
        nearLineData.worldPosStart[1] += subVec.y;
        nearLineData.worldPosStart[2] += subVec.z;
      } else if (distancePEnd < 1) {
        nearLineData.worldPosEnd[0] += subVec.x;
        nearLineData.worldPosEnd[1] += subVec.y;
        nearLineData.worldPosEnd[2] += subVec.z;
      } else {
        nearLineData.worldPosStart[0] += subVec.x;
        nearLineData.worldPosStart[1] += subVec.y;
        nearLineData.worldPosStart[2] += subVec.z;

        nearLineData.worldPosEnd[0] += subVec.x;
        nearLineData.worldPosEnd[1] += subVec.y;
        nearLineData.worldPosEnd[2] += subVec.z;
      }

      // update mouse start
      mouseStart = currentMouseWorlPos;

      renderToolData(model, context, renderer);

      return;
    }

    if (!isDrawing) {
      const toolData = model.toolsState && model.toolsState['vtkLength'];
      if (toolData && (toolData.data || []).length > 0) {
        nearLineData = {};
        let isUpdate = false;

        for (let i = 0; i < toolData.data.length; i++) {
          const data = toolData.data[i];
          const currentWorldPos = worldCoordsPick(
            volumeActor,
            model.volumeMapper,
            renderer,
            model.imageDataObject,
            callData.position
          );

          const distance = computedClosestLine(data, currentWorldPos);

          if (distance < 1 && isEmpty(nearLineData)) {
            data.active = true;
            nearLineData = data;
            isUpdate = true;
          } else {
            if (data.active) {
              data.active = false;
              isUpdate = true;
            }
          }
        }
        if (isUpdate) {
          renderToolData(model, context, renderer);
        }
      }
    }

    if (isDrawing) {
      // redraw data
      renderToolData(model, context, renderer);

      drawingData.worldPosEnd = worldCoordsPick(
        volumeActor,
        model.volumeMapper,
        renderer,
        model.imageDataObject,
        callData.position
      );

      const distance = vtkMath.distance2BetweenPoints(
        drawingData.worldPosStart,
        drawingData.worldPosEnd
      );
      const distanceText = `${Math.sqrt(distance).toFixed(2)} mm`;

      const start = computedLocalDisplayValue(
        drawingData.worldPosStart,
        renderer
      );
      const end = computedLocalDisplayValue(drawingData.worldPosEnd, renderer);
      const handles = { ...drawingData, start, end };

      drawLengthVtk(context, handles, distanceText);
    }

    if (superHandleMouseMove) {
      superHandleMouseMove(callData);
    }
  };

  const superHandleLeftButtonPress = publicAPI.handleLeftButtonPress;
  publicAPI.handleLeftButtonPress = callData => {
    const { genericRenderWindow } = model;
    // if (!apis.length || apiIndex === null || apiIndex === undefined) return;
    // const thisApi = apis[apiIndex];

    const renderer = genericRenderWindow.getRenderer();
    const volumeActor = renderer.getVolumes()[0];
    const container = genericRenderWindow.getContainer();
    const measurementElm = container.querySelector(`.measurement-canvas`);
    let context = measurementElm.getContext('2d');

    const worldPos = worldCoordsPick(
      volumeActor,
      model.volumeMapper,
      renderer,
      model.imageDataObject,
      callData.position
    );
    isDown = true;

    if (!isEmpty(nearLineData)) {
      mouseStart = worldPos;
      return;
    }

    if (!isDrawing) {
      drawingData = {};
      drawingData.worldPosStart = worldPos;
      drawingData.uuid = uuidv4();
      isDrawing = true;
      const handles = computedLocalDisplayValue(
        drawingData.worldPosStart,
        renderer
      );
      drawHandlesVtk(context, handles);
    } else {
      drawingData.worldPosEnd = worldPos;
      isDrawing = false;

      if (model.hasOwnProperty('toolsState') === false) {
        model.toolsState = {};
      }
      if (model.toolsState.hasOwnProperty('vtkLength') === false) {
        model.toolsState['vtkLength'] = {
          data: [],
        };
      }
      const toolData = model.toolsState['vtkLength'];
      toolData.data.push({ ...drawingData });
      drawingData = {};
    }

    if (superHandleLeftButtonPress) {
      // superHandleLeftButtonPress(callData);
    }
  };

  publicAPI.superHandleLeftButtonRelease = publicAPI.handleLeftButtonRelease;
  publicAPI.handleLeftButtonRelease = callData => {
    isDown = false;
    nearLineData = {};
    mouseStart = [];

    if (publicAPI.superHandleLeftButtonRelease) {
      // publicAPI.superHandleLeftButtonRelease(callData);
    }
  };

  publicAPI.handleStartMouseWheel = () => {
    return;
  };

  publicAPI.handleEndMouseWheel = () => {
    return;
  };
}

// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  vtkInteractorStyleMPRSlice.extend(publicAPI, model, initialValues);

  macro.setGet(publicAPI, model, ['onMeasurementChanged']);

  // Object specific methods
  vtkInteractorStyle3DLength(publicAPI, model);
}

export const newInstance = macro.newInstance(
  extend,
  'vtkInteractorStyle3DLength'
);

export default Object.assign({ newInstance, extend });

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    let r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;

    return v.toString(16);
  });
}
