import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { message } from 'antd';
import { useTranslation } from 'react-i18next';
import { ToolbarButton } from '@tuvm/ui';
import { utils } from '@tuvm/core';
import { toggleAllSegments } from '@tuvm/viewer/src/utils/helper';
import { SEGMENT_DEFAULT_INDEX } from '@tuvm/viewer/src/utils/constants';

const { studyMetadataManager } = utils;

let isVisible = true;

const _isDisplaySetReconstructable = (
  viewportSpecificData = {},
  activeViewportIndex
) => {
  if (!viewportSpecificData[activeViewportIndex]) {
    return false;
  }

  const { displaySetInstanceUID, StudyInstanceUID } = viewportSpecificData[
    activeViewportIndex
  ];

  const studies = studyMetadataManager.all();

  const study = studies.find(
    study => study.studyInstanceUID === StudyInstanceUID
  );

  if (!study) {
    return false;
  }

  const displaySet = study._displaySets.find(
    set => set.displaySetInstanceUID === displaySetInstanceUID
  );

  if (!displaySet) {
    return false;
  }

  return displaySet.isReconstructable;
};

function VTKMPRToolbarButton({
  parentContext,
  toolbarClickCallback,
  button,
  activeButtons,
  isActive,
  className,
}) {
  const { t } = useTranslation('Vindoc');

  const { id, label, icon } = button;
  const { viewportSpecificData, activeViewportIndex } = useSelector(state => {
    const { viewports = {} } = state;
    const { viewportSpecificData, activeViewportIndex } = viewports;

    return {
      viewportSpecificData,
      activeViewportIndex,
    };
  });

  isVisible = _isDisplaySetReconstructable(
    viewportSpecificData,
    activeViewportIndex
  );

  const handleActionOpenMpr = evt => {
    try {
      const activeVp =
        (viewportSpecificData && viewportSpecificData[activeViewportIndex]) ||
        {};
      if (activeVp.isInvalidRegularSpacing) {
        message.warning({
          content: t('The slice spacing is irregular'),
          style: {
            margin: '20px auto 0',
            maxWidth: '463px',
          },
          duration: 6,
        });
      }

      toggleAllSegments(SEGMENT_DEFAULT_INDEX);
      toolbarClickCallback(button, evt);
    } catch (error) {
      console.warn(error);
    }
  };

  return (
    <React.Fragment>
      {isVisible && (
        <ToolbarButton
          key={id}
          label={label}
          icon={icon}
          onClick={handleActionOpenMpr}
          isActive={isActive}
        />
      )}
    </React.Fragment>
  );
}

VTKMPRToolbarButton.propTypes = {
  parentContext: PropTypes.object.isRequired,
  toolbarClickCallback: PropTypes.func.isRequired,
  button: PropTypes.object.isRequired,
  activeButtons: PropTypes.array.isRequired,
  isActive: PropTypes.bool,
  className: PropTypes.string,
};

export default VTKMPRToolbarButton;
