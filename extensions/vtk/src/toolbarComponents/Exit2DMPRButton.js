import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import { useTranslation } from 'react-i18next';

let isVisible = true;

function Exit2DMPRButton({ toolbarClickCallback, button }) {
  const { t } = useTranslation('Vindoc');

  const { id, label, icon } = button;

  const handleActionExitMpr = evt => {
    toolbarClickCallback(button, evt);
  };

  return (
    <React.Fragment>
      {isVisible && (
        <Button
          size="small"
          type="primary"
          style={{ width: 80, order: 2, marginLeft: 15 }}
          key={id}
          label={label}
          icon={icon}
          onClick={handleActionExitMpr}
        >
          {t('Exit MPR')}
        </Button>
      )}
    </React.Fragment>
  );
}

Exit2DMPRButton.propTypes = {
  toolbarClickCallback: PropTypes.func.isRequired,
  button: PropTypes.object.isRequired,
};

export default Exit2DMPRButton;
