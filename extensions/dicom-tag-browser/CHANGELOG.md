# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 0.1.0 (2022-07-14)


### Features

* 🎸 add ant design ([235ff05](https://github.com/OHIF/Viewers/commit/235ff05eb7045b1f9e35fa3f2afcc4304715aa98))
* 🎸 add new button icons ([04cb5d9](https://github.com/OHIF/Viewers/commit/04cb5d951551f1bc53f583335426d48b9cbfe170))
* 🎸 styling dicom tag table ([c429c20](https://github.com/OHIF/Viewers/commit/c429c2071e933f7e3092ed86d3b8e136ff7eb3f7))
* 🎸 update dicom tag table ([e1213dc](https://github.com/OHIF/Viewers/commit/e1213dc522b66b0f0d7ed7f9a7121a39da29fa62))
* 🎸 update tag list popup ([db2734b](https://github.com/OHIF/Viewers/commit/db2734b4fe75b58e4935b79022a5cf12e1fe7a7d))





## [0.0.4](https://github.com/OHIF/Viewers/compare/@ohif/extension-dicom-tag-browser@0.0.3...@ohif/extension-dicom-tag-browser@0.0.4) (2020-10-07)

**Note:** Version bump only for package @ohif/extension-dicom-tag-browser





## [0.0.3](https://github.com/OHIF/Viewers/compare/@ohif/extension-dicom-tag-browser@0.0.2...@ohif/extension-dicom-tag-browser@0.0.3) (2020-09-24)

**Note:** Version bump only for package @ohif/extension-dicom-tag-browser





## 0.0.2 (2020-09-10)

**Note:** Version bump only for package @ohif/extension-dicom-tag-browser
