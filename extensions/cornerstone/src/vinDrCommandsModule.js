import cornerstone from 'cornerstone-core';
import cornerstoneTools from '@vindr/vindr-tools';
import OHIF from '@tuvm/core';

import setCornerstoneLayout from './utils/setCornerstoneLayout.js';
import { getEnabledElement } from './state';
import CornerstoneViewportDownloadForm from './CornerstoneViewportDownloadForm';
import {
  resetExpandLayout,
  toggleAllSegments,
} from '@tuvm/viewer/src/utils/helper';
import { SEGMENT_DEFAULT_INDEX } from '@tuvm/viewer/src/utils/constants';
import { actionSetExtensionData } from '@tuvm/viewer/src/system/systemActions';
// import { actionSetExtensionData } from '../../../platform/viewer/src/system/systemActions.js';

const scroll = cornerstoneTools.import('util/scroll');
const MouseCursor = cornerstoneTools.importInternal(
  'tools/cursors/MouseCursor'
);

const stackScrollSvgStr = `<g transform="translate(-4, -8)"><path fill="ACTIVE_COLOR" d="M16.2188 5.34375C15.9775 5.34375 15.7365 5.25127 15.5531 5.06644L12.6632 2.15259C12.4855 1.97358 12.2501 1.875 12 1.875C11.7499 1.875 11.5145 1.97358 11.3369 2.15259L8.44689 5.06644C8.0823 5.43403 7.48877 5.43652 7.12108 5.07187C6.75344 4.70728 6.751 4.11366 7.11564 3.74606L10.0056 0.832219C10.5379 0.295547 11.2462 0 12 0C12.7538 0 13.4621 0.295547 13.9944 0.832219L16.8844 3.74606C17.249 4.11366 17.2466 4.70728 16.8789 5.07187C16.6962 5.25319 16.4574 5.34375 16.2188 5.34375ZM13.9944 23.1678L16.8844 20.2539C17.249 19.8863 17.2466 19.2927 16.8789 18.9281C16.5113 18.5635 15.9177 18.566 15.5531 18.9336L12.6632 21.8474C12.4855 22.0264 12.2501 22.125 12 22.125C11.7499 22.125 11.5145 22.0264 11.3369 21.8474L8.44689 18.9336C8.0823 18.566 7.48877 18.5635 7.12108 18.9281C6.75344 19.2927 6.751 19.8863 7.11564 20.2539L10.0056 23.1678C10.5379 23.7045 11.2462 24 12 24C12.7538 24 13.4621 23.7045 13.9944 23.1678ZM15.2344 11.9531C15.2344 10.1438 13.7624 8.67188 11.9531 8.67188C10.1439 8.67188 8.67189 10.1438 8.67189 11.9531C8.67189 13.7624 10.1439 15.2344 11.9531 15.2344C13.7624 15.2344 15.2344 13.7624 15.2344 11.9531ZM13.3594 11.9531C13.3594 12.7285 12.7285 13.3594 11.9531 13.3594C11.1777 13.3594 10.5469 12.7285 10.5469 11.9531C10.5469 11.1777 11.1777 10.5469 11.9531 10.5469C12.7285 10.5469 13.3594 11.1777 13.3594 11.9531Z" /></g>`;
const stackScrollOption = {
  iconSize: 24,
  viewBox: {
    x: 24,
    y: 24,
  },
};

const stackScrollSvgCursor = new MouseCursor(
  stackScrollSvgStr,
  stackScrollOption
);

const { studyMetadataManager } = OHIF.utils;
const { setViewportSpecificData } = OHIF.redux.actions;

const refreshCornerstoneViewports = () => {
  cornerstone.getEnabledElements().forEach(enabledElement => {
    if (enabledElement.image) {
      cornerstone.updateImage(enabledElement.element);
    }
  });
};

const commandsModule = ({ servicesManager }) => {
  const actions = {
    rotateViewport: ({ viewports, rotation }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        let viewport = cornerstone.getViewport(enabledElement);
        viewport.rotation += rotation;
        cornerstone.setViewport(enabledElement, viewport);
      }
    },
    flipViewportHorizontal: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        let viewport = cornerstone.getViewport(enabledElement);
        viewport.hflip = !viewport.hflip;
        cornerstone.setViewport(enabledElement, viewport);
      }
    },
    flipViewportVertical: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        let viewport = cornerstone.getViewport(enabledElement);
        viewport.vflip = !viewport.vflip;
        cornerstone.setViewport(enabledElement, viewport);
      }
    },
    scaleViewport: ({ direction, viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);
      const step = direction * 0.15;

      if (enabledElement) {
        if (step) {
          let viewport = cornerstone.getViewport(enabledElement);
          viewport.scale += step;
          cornerstone.setViewport(enabledElement, viewport);
        } else {
          cornerstone.fitToWindow(enabledElement);
        }
      }
    },
    resetViewport: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        cornerstone.reset(enabledElement);
      }
    },
    invertViewport: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        let viewport = cornerstone.getViewport(enabledElement);
        viewport.invert = !viewport.invert;
        cornerstone.setViewport(enabledElement, viewport);
      }
    },
    // TODO: this is receiving `evt` from `ToolbarRow`. We could use it to have
    //       better mouseButtonMask sets.
    setToolActive: ({ toolName, viewports }) => {
      if (!toolName) {
        console.warn('No toolname provided to setToolActive command');
      }
      if (toolName === 'StackScroll') {
        // Change Cursor
        (cornerstoneTools.store.state.tools || []).some(tool => {
          if (tool.name === toolName) {
            tool.svgCursor = stackScrollSvgCursor;
            return true;
          }
        });
      }
      const { viewportSpecificData = {} } = viewports || {};
      Object.keys(viewportSpecificData).forEach(i => {
        const viewport = viewportSpecificData[i];
        const hasDisplaySet = viewport.displaySetInstanceUID !== undefined;
        if (hasDisplaySet) {
          const viewportElm = getEnabledElement(i);
          cornerstoneTools.setToolActiveForElement(viewportElm, toolName, {
            mouseButtonMask: 1,
          });
        }
      });
    },
    clearAnnotations: ({ viewports }) => {
      const element = getEnabledElement(viewports.activeViewportIndex);
      if (!element) {
        return;
      }

      const enabledElement = cornerstone.getEnabledElement(element);
      if (!enabledElement || !enabledElement.image) {
        return;
      }

      const {
        toolState,
      } = cornerstoneTools.globalImageIdSpecificToolStateManager;
      if (
        !toolState ||
        toolState.hasOwnProperty(enabledElement.image.imageId) === false
      ) {
        return;
      }

      const imageIdToolState = toolState[enabledElement.image.imageId];

      const measurementsToRemove = [];

      Object.keys(imageIdToolState).forEach(toolType => {
        const { data } = imageIdToolState[toolType];

        data.forEach(measurementData => {
          const {
            _id,
            lesionNamingNumber,
            measurementNumber,
          } = measurementData;
          if (!_id) {
            return;
          }

          measurementsToRemove.push({
            toolType,
            _id,
            lesionNamingNumber,
            measurementNumber,
          });
        });
      });

      measurementsToRemove.forEach(measurementData => {
        OHIF.measurements.MeasurementHandlers.onRemoved({
          detail: {
            toolType: measurementData.toolType,
            measurementData,
          },
        });
      });
    },
    clearSegmentations: ({ viewports }) => {
      const element = getEnabledElement(viewports.activeViewportIndex);
      if (!element) {
        return;
      }

      const enabledElement = cornerstone.getEnabledElement(element);
      if (!enabledElement || !enabledElement.image) {
        return;
      }

      const segmentationModule = cornerstoneTools.getModule('segmentation');
      const activeSegmentIndex = segmentationModule.getters.labelmap3D(
        enabledElement.element
      ).activeSegmentIndex;
      const activeLabelmapIndex = segmentationModule.getters.labelmaps3D(
        enabledElement.element
      ).activeLabelmapIndex;

      for (let i = 1; i <= activeSegmentIndex; i++) {
        for (let j = 0; j <= activeLabelmapIndex; j++) {
          segmentationModule.setters.deleteSegment(
            enabledElement.element,
            i,
            j
          );
        }
      }
    },
    clearMeasurements: ({ viewports }) => {
      const element = getEnabledElement(viewports.activeViewportIndex);
      if (!element) {
        return;
      }

      const enabledElement = cornerstone.getEnabledElement(element);
      if (!enabledElement || !enabledElement.image) {
        return;
      }

      try {
        // delete segmentations
        const segmentationModule = cornerstoneTools.getModule('segmentation');
        const activeSegmentIndex = segmentationModule.getters.labelmap3D(
          enabledElement.element
        ).activeSegmentIndex;
        const activeLabelmapIndex = segmentationModule.getters.labelmaps3D(
          enabledElement.element
        ).activeLabelmapIndex;

        for (let i = 1; i <= activeSegmentIndex; i++) {
          for (let j = 0; j <= activeLabelmapIndex; j++) {
            segmentationModule.setters.deleteSegment(
              enabledElement.element,
              i,
              j
            );
          }
        }
      } catch (err) {
        console.log(err);
      }

      // delete annotations
      const {
        toolState,
      } = cornerstoneTools.globalImageIdSpecificToolStateManager;
      if (
        !toolState ||
        toolState.hasOwnProperty(enabledElement.image.imageId) === false
      ) {
        return;
      }

      const imageIdToolState = toolState[enabledElement.image.imageId];

      const measurementsToRemove = [];

      Object.keys(imageIdToolState).forEach(toolType => {
        const { data } = imageIdToolState[toolType];

        data.forEach(measurementData => {
          const {
            _id,
            lesionNamingNumber,
            measurementNumber,
          } = measurementData;
          if (!_id) {
            return;
          }

          measurementsToRemove.push({
            toolType,
            _id,
            lesionNamingNumber,
            measurementNumber,
          });
        });
      });

      measurementsToRemove.forEach(measurementData => {
        OHIF.measurements.MeasurementHandlers.onRemoved({
          detail: {
            toolType: measurementData.toolType,
            measurementData,
          },
        });
      });
    },
    nextImage: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);
      scroll(enabledElement, 1);
    },
    previousImage: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);
      scroll(enabledElement, -1);
    },
    getActiveViewportEnabledElement: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);
      return enabledElement;
    },
    showDownloadViewportModal: ({ title, viewports }) => {
      const activeViewportIndex = viewports.activeViewportIndex;
      const viewportSpecificData =
        viewports.viewportSpecificData[activeViewportIndex];
      const { UIModalService } = servicesManager.services;
      if (UIModalService) {
        UIModalService.show({
          content: CornerstoneViewportDownloadForm,
          title,
          contentProps: {
            activeViewportIndex,
            onClose: UIModalService.hide,
            viewportSpecificData,
          },
        });
      }
    },
    updateTableWithNewMeasurementData({
      toolType,
      measurementNumber,
      location,
      description,
    }) {
      // Update all measurements by measurement number
      const measurementApi = OHIF.measurements.MeasurementApi.Instance;
      const measurements = measurementApi.tools[toolType].filter(
        m => m.measurementNumber === measurementNumber
      );

      measurements.forEach(measurement => {
        measurement.location = location;
        measurement.description = description;

        measurementApi.updateMeasurement(measurement.toolType, measurement);
      });

      measurementApi.syncMeasurementsAndToolData();

      refreshCornerstoneViewports();
    },
    getNearbyToolData({ element, canvasCoordinates, availableToolTypes }) {
      const nearbyTool = {};
      let pointNearTool = false;

      try {
        availableToolTypes.forEach(toolType => {
          const elementToolData = cornerstoneTools.getToolState(
            element,
            toolType
          );

          if (!elementToolData) {
            return;
          }

          elementToolData.data.forEach((toolData, index) => {
            let elementToolInstance = cornerstoneTools.getToolForElement(
              element,
              toolType
            );

            if (!elementToolInstance) {
              elementToolInstance = cornerstoneTools.getToolForElement(
                element,
                `${toolType}Tool`
              );
            }

            if (!elementToolInstance) {
              console.warn('Tool not found.');
              return undefined;
            }

            if (
              elementToolInstance.pointNearTool(
                element,
                toolData,
                canvasCoordinates
              )
            ) {
              pointNearTool = true;
              nearbyTool.tool = toolData;
              nearbyTool.index = index;
              nearbyTool.toolType = toolType;
            }
          });

          if (pointNearTool) {
            return false;
          }
        });
      } catch (error) {
        console.log(error);
      }

      return pointNearTool ? nearbyTool : undefined;
    },
    removeToolState: ({ element, toolType, tool }) => {
      cornerstoneTools.removeToolState(element, toolType, tool);
      cornerstone.updateImage(element);
    },
    setCornerstoneLayout: () => {
      setCornerstoneLayout();
      toggleAllSegments(SEGMENT_DEFAULT_INDEX);
    },
    exit2DMPR: () => {
      resetExpandLayout();
      actions.setCornerstoneLayout();
    },
    setWindowLevel: ({ viewports, window, level }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        let viewport = cornerstone.getViewport(enabledElement);

        viewport.voi = {
          windowWidth: Number(window),
          windowCenter: Number(level),
        };
        cornerstone.setViewport(enabledElement, viewport);
      }
    },
    jumpToImage: ({
      StudyInstanceUID,
      SOPInstanceUID,
      frameIndex,
      activeViewportIndex,
    }) => {
      try {
        const study = studyMetadataManager.get(StudyInstanceUID);

        const displaySet = study.findDisplaySet(ds => {
          return (
            ds.images &&
            ds.images.find(i => i.getSOPInstanceUID() === SOPInstanceUID)
          );
        });

        displaySet.SOPInstanceUID = SOPInstanceUID;
        displaySet.frameIndex = frameIndex;

        window.store.dispatch(
          setViewportSpecificData(activeViewportIndex, displaySet)
        );

        refreshCornerstoneViewports();
      } catch (error) {
        console.log(error);
      }
    },
    openReport: () => {
      actionSetExtensionData('predictionPanel', {
        rightPanel: true,
        selectedTab: 'doctor-tab',
      });
    },
    openCad: () => {
      actionSetExtensionData('predictionPanel', {
        rightPanel: true,
        selectedTab: 'ai-tab',
      });
    },
  };

  const definitions = {
    jumpToImage: {
      commandFn: actions.jumpToImage,
      storeContexts: [],
      options: {},
    },
    getNearbyToolData: {
      commandFn: actions.getNearbyToolData,
      storeContexts: [],
      options: {},
    },
    removeToolState: {
      commandFn: actions.removeToolState,
      storeContexts: [],
      options: {},
    },
    updateTableWithNewMeasurementData: {
      commandFn: actions.updateTableWithNewMeasurementData,
      storeContexts: [],
      options: {},
    },
    showDownloadViewportModal: {
      commandFn: actions.showDownloadViewportModal,
      storeContexts: ['viewports'],
      options: {},
    },
    getActiveViewportEnabledElement: {
      commandFn: actions.getActiveViewportEnabledElement,
      storeContexts: ['viewports'],
      options: {},
    },
    rotateViewportCW: {
      commandFn: actions.rotateViewport,
      storeContexts: ['viewports'],
      options: { rotation: 90 },
    },
    rotateViewportCCW: {
      commandFn: actions.rotateViewport,
      storeContexts: ['viewports'],
      options: { rotation: -90 },
    },
    invertViewport: {
      commandFn: actions.invertViewport,
      storeContexts: ['viewports'],
      options: {},
    },
    flipViewportVertical: {
      commandFn: actions.flipViewportVertical,
      storeContexts: ['viewports'],
      options: {},
    },
    flipViewportHorizontal: {
      commandFn: actions.flipViewportHorizontal,
      storeContexts: ['viewports'],
      options: {},
    },
    scaleUpViewport: {
      commandFn: actions.scaleViewport,
      storeContexts: ['viewports'],
      options: { direction: 1 },
    },
    scaleDownViewport: {
      commandFn: actions.scaleViewport,
      storeContexts: ['viewports'],
      options: { direction: -1 },
    },
    fitViewportToWindow: {
      commandFn: actions.scaleViewport,
      storeContexts: ['viewports'],
      options: { direction: 0 },
    },
    resetViewport: {
      commandFn: actions.resetViewport,
      storeContexts: ['viewports'],
      options: {},
    },
    clearAnnotations: {
      commandFn: actions.clearAnnotations,
      storeContexts: ['viewports'],
      options: {},
    },
    clearSegmentations: {
      commandFn: actions.clearSegmentations,
      storeContexts: ['viewports'],
      options: {},
    },
    clearMeasurements: {
      commandFn: actions.clearMeasurements,
      storeContexts: ['viewports'],
      options: {},
    },
    nextImage: {
      commandFn: actions.nextImage,
      storeContexts: ['viewports'],
      options: {},
    },
    previousImage: {
      commandFn: actions.previousImage,
      storeContexts: ['viewports'],
      options: {},
    },
    // TOOLS
    setToolActive: {
      commandFn: actions.setToolActive,
      storeContexts: ['viewports'],
      options: {},
    },
    setZoomTool: {
      commandFn: actions.setToolActive,
      storeContexts: ['viewports'],
      options: { toolName: 'Zoom' },
    },
    setCornerstoneLayout: {
      commandFn: actions.setCornerstoneLayout,
      storeContexts: [],
      options: {},
      context: 'VIEWER',
    },
    exit2DMPR: {
      commandFn: actions.exit2DMPR,
      storeContexts: [],
      options: {},
      context: 'VIEWER',
    },
    setWindowLevel: {
      commandFn: actions.setWindowLevel,
      storeContexts: ['viewports'],
      options: {},
    },
    openReport: {
      commandFn: actions.openReport,
      storeContexts: [],
      options: {},
      context: 'VIEWER',
    },
    openCad: {
      commandFn: actions.openCad,
      storeContexts: [],
      options: {},
      context: 'VIEWER',
    },
  };

  return {
    actions,
    definitions,
    defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE',
  };
};

export default commandsModule;
