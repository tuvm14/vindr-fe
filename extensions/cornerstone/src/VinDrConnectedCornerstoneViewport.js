import CornerstoneViewport from '@vindr/react-vindr-viewport';
import OHIF from '@tuvm/core';
import { connect } from 'react-redux';
import throttle from 'lodash.throttle';
import cornerstone from 'cornerstone-core';
import {
  commandsManager,
  servicesManager,
} from '../../../platform/viewer/src/App';
// import { getTextByLanguage } from '../../../platform/viewer/src/utils/common';
// import VindocLabellingFlow from '.../../../platform/viewer/src/components/VindocLabelling';
import { setEnabledElement } from './state';

const {
  setViewportActive,
  setViewportSpecificData,
  setExtensionData,
} = OHIF.redux.actions;
const {
  onAdded,
  onRemoved,
  onModified,
} = OHIF.measurements.MeasurementHandlers;

// TODO: Transition to enums for the action names so that we can ensure they stay up to date
// everywhere they're used.
const MEASUREMENT_ACTION_MAP = {
  added: onAdded,
  removed: onRemoved,
  modified: throttle(event => {
    return onModified(event);
  }, 300),
};

const mapStateToProps = (state, ownProps) => {
  let dataFromStore;

  // TODO: This may not be updated anymore :thinking:
  if (state.extensions && state.extensions.cornerstone) {
    dataFromStore = state.extensions.cornerstone;
  }

  // If this is the active viewport, enable prefetching.
  const { viewportIndex } = ownProps; //.viewportData;
  const isActive = viewportIndex === state.viewports.activeViewportIndex;
  const viewportSpecificData =
    state.viewports.viewportSpecificData[viewportIndex] || {};

  // CINE
  let isPlaying = false;
  let frameRate = 24;

  if (viewportSpecificData && viewportSpecificData.cine) {
    const cine = viewportSpecificData.cine;

    isPlaying = cine.isPlaying === true;
    frameRate = cine.cineFrameRate || frameRate;
  }

  return {
    // layout: state.viewports.layout,
    isActive,
    // TODO: Need a cleaner and more versatile way.
    // Currently justing using escape hatch + commands
    // activeTool: activeButton && activeButton.command,
    ...dataFromStore,
    isStackPrefetchEnabled: isActive,
    isPlaying,
    frameRate,
    //stack: viewportSpecificData.stack,
    // viewport: viewportSpecificData.viewport,
    isOverlayVisible: state.extensions.settings.isVisibleAnnotation,
    // measurements: state.timepointManager.measurements,
    isVisibleLocalTag: state.extensions.settings.isVisibleLocalTag,
    isVisibleAllTag: state.extensions.settings.isVisibleAllTag,
    tagAllText: '',
    tagBorderColor: 'var(--active-color)',
    tagTextColor: '#fbfbfb',
    tagBackgroundColor: '#11425d',
    showTagColorByBox: true,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { viewportIndex } = ownProps;

  const handleActiveBox = tag => {
    // dispatch action
    const measurementApi = OHIF.measurements.MeasurementApi.Instance;

    Object.keys(measurementApi.tools).forEach(toolType => {
      const measurements = measurementApi.tools[toolType];

      measurements.forEach(measurement => {
        measurement.active = false;
      });
    });

    const measurementsToActive = measurementApi.tools[tag.toolType].filter(
      measurement => {
        return measurement.measurementNumber === tag.measurementNumber;
      }
    );

    measurementsToActive.forEach(measurementToToggled => {
      measurementToToggled.active = true;
    });

    measurementApi.syncMeasurementsAndToolData();

    cornerstone.getEnabledElements().forEach(enabledElement => {
      cornerstone.updateImage(enabledElement.element);
    });
  };

  // const showLabellingDialog = measurementData => {
  //   const { UIDialogService } = servicesManager.services;

  //   const _getLabelPopupPosition = () => {
  //     const screenWidth = window.innerWidth;
  //     let x = (screenWidth - 320) / 2;
  //     let y = 200;

  //     return { x, y };
  //   };
  //   if (!UIDialogService) {
  //     return;
  //   }

  //   UIDialogService.dismiss({ id: 'labelling' });
  //   UIDialogService.create({
  //     id: 'labelling',
  //     isDraggable: false,
  //     showOverlay: false,
  //     centralize: false,
  //     defaultPosition: _getLabelPopupPosition(),
  //     // content: VindocLabellingFlow,
  //     contentProps: {
  //       measurementData,
  //       labellingDoneCallback: () =>
  //         UIDialogService.dismiss({ id: 'labelling' }),
  //       labellingClose: () => {
  //         UIDialogService.dismiss({ id: 'labelling' });
  //       },
  //       updateLabelling: ({
  //         location,
  //         boxNumber,
  //         boxDescription,
  //         response,
  //       }) => {
  //         measurementData.location = location || measurementData.location;
  //         measurementData.description = boxDescription || [];
  //         measurementData.response = response || measurementData.response;
  //         measurementData.boxNumber = boxNumber || measurementData.boxNumber;

  //         const { MeasurementHandlers } = OHIF.measurements;

  //         MeasurementHandlers.onModified({
  //           detail: {
  //             toolType: measurementData.toolType,
  //             measurementData: {
  //               _id: measurementData._id,
  //               lesionNamingNumber: measurementData.lesionNamingNumber,
  //               measurementNumber: measurementData.measurementNumber,
  //               boxNumber: measurementData.boxNumber,
  //             },
  //             element: document.querySelector('.viewport-element'),
  //           },
  //         });

  //         commandsManager.runCommand(
  //           'updateTableWithNewMeasurementData',
  //           measurementData
  //         );
  //       },
  //     },
  //   });
  // };

  const measurementApi = OHIF.measurements.MeasurementApi.Instance;

  const handleToggleAllBox = toggle => {
    Object.keys(measurementApi.tools).forEach(toolType => {
      const measurements = measurementApi.tools[toolType];

      measurements.forEach(measurement => {
        measurement.visible = toggle;
      });
    });

    measurementApi.syncMeasurementsAndToolData();

    cornerstone.getEnabledElements().forEach(enabledElement => {
      cornerstone.updateImage(enabledElement.element);
    });

    dispatch(setExtensionData('settings', { isVisibleAllTag: toggle }));
  };

  return {
    setViewportActive: () => {
      dispatch(setViewportActive(viewportIndex));
    },

    setViewportSpecificData: data => {
      dispatch(setViewportSpecificData(viewportIndex, data));
    },

    /**
     * Our component "enables" the underlying dom element on "componentDidMount"
     * It listens for that event, and then emits the enabledElement. We can grab
     * a reference to it here, to make playing with cornerstone's native methods
     * easier.
     */
    onElementEnabled: event => {
      const enabledElement = event.detail.element;
      setEnabledElement(viewportIndex, enabledElement);
      dispatch(
        setViewportSpecificData(viewportIndex, {
          // TODO: Hack to make sure our plugin info is available from the outset
          plugin: 'cornerstone',
        })
      );
    },

    onMeasurementsChanged: (event, action) => {
      return MEASUREMENT_ACTION_MAP[action](event);
    },
    handleToggleBox: tag => {
      // dispatch action
      if (tag) {
        const nextBoxActiveStatus = !tag.visible;
        const measurementsToActive = measurementApi.tools[tag.toolType].filter(
          measurement => {
            return measurement.measurementNumber === tag.measurementNumber;
          }
        );

        measurementsToActive.forEach(measurementToToggled => {
          measurementToToggled.visible = nextBoxActiveStatus;
        });

        measurementApi.syncMeasurementsAndToolData();

        cornerstone.getEnabledElements().forEach(enabledElement => {
          cornerstone.updateImage(enabledElement.element);
        });

        let countVisibleItems = 0,
          countInvisibleItem = 0,
          countAllItem = 0;

        Object.keys(measurementApi.tools).forEach(toolType => {
          const measurements = measurementApi.tools[toolType];

          measurements.forEach(measurement => {
            countAllItem += 1;
            if (measurement.visible) countVisibleItems += 1;
            else countInvisibleItem += 1;
          });
        });

        if (nextBoxActiveStatus && countVisibleItems === countAllItem) {
          handleToggleAllBox(true);
        } else if (
          !nextBoxActiveStatus &&
          countInvisibleItem === countAllItem
        ) {
          handleToggleAllBox(false);
        }
      }
    },

    handleToggleAllBox: toggle => {
      handleToggleAllBox(toggle);
    },

    handleActiveBox: tag => {
      handleActiveBox(tag);
    },

    handleDeleteBox: tag => {
      const { MeasurementHandlers } = OHIF.measurements;

      MeasurementHandlers.onRemoved({
        detail: {
          toolType: tag.toolType,
          measurementData: {
            _id: tag._id,
            lesionNamingNumber: tag.lesionNamingNumber,
            measurementNumber: tag.measurementNumber,
          },
        },
      });

      Object.keys(measurementApi.tools).forEach(toolType => {
        const measurements = measurementApi.tools[toolType];

        measurements.forEach(measurement => {
          const newBoxNumber = measurement.boxNumber.split('-')[1];
          const boxNumber = measurement.measurementNumber + '-' + newBoxNumber;
          measurement.boxNumber = boxNumber;
          commandsManager.runCommand(
            'updateTableWithNewMeasurementData',
            measurement
          );
        });
      });
    },

    // handleEditBox: tag => {
    //   handleActiveBox(tag);

    //   showLabellingDialog(tag);
    // },
  };
};

const ConnectedCornerstoneViewport = connect(
  mapStateToProps,
  mapDispatchToProps
)(CornerstoneViewport);

export default ConnectedCornerstoneViewport;
