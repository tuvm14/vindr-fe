import { WindowLevelSettings } from '@tuvm/viewer/src/components/WindowLevelSettings';
import SeriesSynchronization from '@tuvm/viewer/src/components/SeriesSynchronization';
import VTKMPRToolbarButton from '../../vtk/src/toolbarComponents/VTKMPRToolbarButton';
import Exit2DMPRButton from '../../vtk/src/toolbarComponents/Exit2DMPRButton';
// TODO: A way to add Icons that don't already exist?
// - Register them and add
// - Include SVG Source/Inline?
// - By URL, or own component?

// What KINDS of toolbar buttons do we have...
// - One's that dispatch commands
// - One's that set tool's active
// - More custom, like CINE
//    - Built in for one's like this, or custom components?

// Visible?
// Disabled?
// Based on contexts or misc. criteria?
//  -- ACTIVE_ROUTE::VIEWER
//  -- ACTIVE_VIEWPORT::CORNERSTONE
// setToolActive commands should receive the button event that triggered
// so we can do the "bind to this button" magic

const TOOLBAR_BUTTON_TYPES = {
  COMMAND: 'command',
  SET_TOOL_ACTIVE: 'setToolActive',
  BUILT_IN: 'builtIn',
};

const TOOLBAR_BUTTON_BEHAVIORS = {
  CINE: 'CINE',
  DOWNLOAD_SCREEN_SHOT: 'DOWNLOAD_SCREEN_SHOT',
};

/* TODO: Export enums through a extension manager. */
const enums = {
  TOOLBAR_BUTTON_TYPES,
  TOOLBAR_BUTTON_BEHAVIORS,
};

const definitions = [
  {
    id: 'syncSeries',
    divider: true,
    CustomComponent: SeriesSynchronization,
  },
  {
    id: 'Pan',
    label: 'Pan',
    icon: 'panTool',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'Pan' },
    mobileOrder: 1,
  },
  {
    id: 'Zoom',
    label: 'Zoom',
    icon: 'zoomIn',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'Zoom' },
    mobileOrder: 2,
  },
  {
    id: 'Magnify',
    label: 'Magnify',
    icon: 'imageSearch',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'Magnify' },
  },
  {
    id: 'RotateRight',
    label: 'Rotate Right',
    icon: 'rotateRight',
    //
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'rotateViewportCW',
  },
  {
    id: 'FlipV',
    label: 'Flip V',
    icon: 'flipVertical',
    //
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'flipViewportVertical',
  },
  {
    id: 'FlipH',
    label: 'Flip H',
    icon: 'flipHorizontal',
    //
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'flipViewportHorizontal',
  },
  {
    id: 'Invert',
    label: 'Invert',
    icon: 'invertColors',
    divider: true,
    //
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'invertViewport',
  },
  {
    id: 'Wwwc',
    label: 'Window',
    icon: 'brightness',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'Wwwc' },
    mobileOrder: 3,
  },
  {
    id: 'setWindowLevel',
    CustomComponent: WindowLevelSettings,
  },
  {
    id: 'DragProbe',
    label: 'Probe',
    icon: 'picker',
    divider: true,
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'DragProbe' },
  },
  {
    id: 'Reset',
    label: 'Reset',
    icon: 'refresh',
    divider: true,
    //
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'resetViewport',
  },
  {
    id: 'Cine',
    label: 'CINE',
    icon: 'smartDisplay',
    //
    type: TOOLBAR_BUTTON_TYPES.BUILT_IN,
    options: {
      behavior: TOOLBAR_BUTTON_BEHAVIORS.CINE,
    },
  },
  {
    id: 'StackScroll',
    label: 'Stack Scroll',
    icon: 'stack',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'StackScroll' },
  },
  // {
  //   id: 'reconstruct3D',
  //   CustomComponent: Reconstruct3D,
  // },
  {
    id: '2DMPR',
    label: '2D MPR',
    icon: 'cube3D2',
    //
    CustomComponent: VTKMPRToolbarButton,
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'mpr2d',
    context: 'ACTIVE_VIEWPORT::CORNERSTONE',
  },
  {
    id: '3DMPR',
    label: '3D Reconstruction',
    icon: 'cube3D1',
    divider: true,
    //
    CustomComponent: VTKMPRToolbarButton,
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'mpr3d',
    context: 'ACTIVE_VIEWPORT::CORNERSTONE',
  },
  {
    id: 'Exit2DMPR',
    // label: 'Exit 2D MPR',
    // icon: 'clear',
    //
    // type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'exit2DMPR',
    CustomComponent: Exit2DMPRButton,
    context: 'ACTIVE_VIEWPORT::VTK',
  },
  {
    id: 'Length',
    label: 'Length',
    icon: 'straighten',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'Length' },
    mobileOrder: 5,
  },
  {
    id: 'Bidirectional',
    label: 'Bidirectional',
    icon: 'distance',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'Bidirectional' },
  },
  {
    id: 'ArrowAnnotate',
    label: 'Annotate',
    icon: 'textRotation',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'ArrowAnnotate' },
  },
  {
    id: 'Draw',
    label: 'Ellipse',
    icon: 'elipticalRoi',
    buttons: [
      {
        id: 'EllipticalRoi',
        label: 'Ellipse',
        icon: 'elipticalRoi',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'EllipticalRoi' },
      },
      {
        id: 'RectangleRoi',
        label: 'Rectangle',
        icon: 'rectangleRoi',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'RectangleRoi' },
      },
      {
        id: 'Angle',
        label: 'Angle',
        icon: 'angle',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Angle' },
      },
    ],
  },
  {
    id: 'Scrisor',
    label: 'Brush',
    icon: 'brush2',
    buttons: [
      {
        id: 'Brush',
        label: 'Brush',
        icon: 'brush2',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Brush' },
      },
      {
        id: 'CorrectionScissors',
        label: 'Correction Scissors',
        icon: 'splineCut',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'CorrectionScissors' },
      },
      {
        id: 'BrushEraser',
        label: 'Eraser',
        icon: 'eraser2',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'BrushEraser' },
      },
    ],
  },
  {
    id: 'Clear',
    label: 'Clear',
    icon: 'deleteIcon',
    divider: true,
    //
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'clearMeasurements',
  },
  // {
  //   id: 'SphericalBrush',
  //   label: 'Spherical',
  //   icon: 'sphere',
  //   //
  //   type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
  //   commandName: 'setToolActive',
  //   commandOptions: { toolName: 'SphericalBrush' },
  // },
  {
    id: 'Download',
    label: 'Download',
    icon: 'download2',
    //
    type: TOOLBAR_BUTTON_TYPES.BUILT_IN,
    options: {
      behavior: TOOLBAR_BUTTON_BEHAVIORS.DOWNLOAD_SCREEN_SHOT,
      togglable: true,
    },
  },
  // {
  //   id: 'Report',
  //   label: 'Report',
  //   icon: 'description',
  //   //
  //   type: TOOLBAR_BUTTON_TYPES.COMMAND,
  //   commandName: 'report',
  // },
  // {
  //   id: 'CAD',
  //   label: 'CAD',
  //   icon: 'cad',
  //   //
  //   type: TOOLBAR_BUTTON_TYPES.COMMAND,
  //   commandName: 'cad',
  // },
];

export default {
  enums,
  definitions,
  defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE',
};
