import React, { Component } from 'react';
import PropTypes from 'prop-types';
import OHIF from '@tuvm/core';
import ConnectedDicomHtmlViewport from './ConnectedDicomHtmlViewport';

const { DicomLoaderService } = OHIF.utils;
let currentDisplaySetInstanceUID = null;
let SRCache = {};

class OHIFDicomHtmlViewport extends Component {
  static propTypes = {
    studies: PropTypes.object,
    displaySet: PropTypes.object,
    viewportIndex: PropTypes.number,
    viewportData: PropTypes.object,
  };

  state = {
    byteArray: null,
    error: null,
    forceUpdate: 0,
  };

  componentDidMount() {
    const { displaySet, studies } = this.props.viewportData;
    currentDisplaySetInstanceUID = displaySet.displaySetInstanceUID;
    if (SRCache[currentDisplaySetInstanceUID]) {
      this.setState({
        byteArray: SRCache[currentDisplaySetInstanceUID],
      });
      return;
    }
    DicomLoaderService.findDicomDataPromise(displaySet, studies).then(
      data => {
        const byteArray = new Uint8Array(data);
        this.setState({
          byteArray: byteArray,
        });
        SRCache[currentDisplaySetInstanceUID] = byteArray;
      },
      error => {
        this.setState({
          error,
        });

        throw new Error(error);
      }
    );
  }

  componentDidUpdate() {
    const { displaySet, studies } = this.props.viewportData;
    const { displaySetInstanceUID } = displaySet;
    if (displaySetInstanceUID != currentDisplaySetInstanceUID) {
      currentDisplaySetInstanceUID = displaySetInstanceUID;

      if (SRCache[displaySetInstanceUID]) {
        this.setState({
          byteArray: SRCache[displaySetInstanceUID],
          forceUpdate: 1 + this.state.forceUpdate,
        });
        return;
      }

      DicomLoaderService.findDicomDataPromise(displaySet, studies).then(
        data => {
          const byteArray = new Uint8Array(data);
          this.setState({
            byteArray: byteArray,
            forceUpdate: 1 + this.state.forceUpdate,
          });
          SRCache[displaySetInstanceUID] = byteArray;
        },
        error => {
          this.setState({
            error,
          });

          throw new Error(error);
        }
      );
    }
  }

  render() {
    return (
      <>
        {this.state.byteArray && (
          <ConnectedDicomHtmlViewport
            byteArray={this.state.byteArray}
            viewportIndex={this.props.viewportIndex}
            forceUpdate={this.state.forceUpdate}
          />
        )}
        {this.state.error && <h2>{JSON.stringify(this.state.error)}</h2>}
      </>
    );
  }
}

export default OHIFDicomHtmlViewport;
