var gulp = require('gulp');
var del = require('del');
// var uglify = require('gulp-uglify');
// var concat = require('gulp-concat');

gulp.task('clean', function() {
  // You can use multiple globbing patterns as you would with `gulp.src`
  return del(['dist']);
});

gulp.task('copy-build', function() {
  return (
    gulp
      .src('platform/viewer/dist/**/*.*')
      // .pipe(concat('scripts.js'))
      .pipe(gulp.dest('dist/resources'))
  );
  // .pipe(uglify())
  // .pipe(gulp.dest('.'))
});

gulp.task('copy-proxy', function() {
  return gulp
    .src([
      'proxy/**/*.*',
      'proxy/**/*',
      '!proxy/data/**/*.*',
      '!proxy/data/**/*',
      '!proxy/logs/**/*.*',
      '!proxy/logs/**/*',
      '!proxy/node_modules/**/*.*',
      '!proxy/node_modules/**/*',
    ])
    .pipe(gulp.dest('dist'));
  // .pipe());
  // .pipe(uglify())
  // .pipe(gulp.dest('.'))
});

gulp.task('clean-proxy', function() {
  return del(['dist/data', 'dist/logs', 'dist/node_modules']);
});

gulp.task(
  'default',
  gulp.series('clean', 'copy-proxy', 'copy-build', 'clean-proxy')
);
