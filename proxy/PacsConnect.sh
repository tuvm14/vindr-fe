#!/usr/bin/env bash

set -e

realpath() { [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"; }
ROOT=$(dirname "$(realpath "$0")")

function code() {
	cd "$ROOT"

	# Get electron, compile, built-in extensions
	if [[ -z "${VSCODE_SKIP_PRELAUNCH}" ]]; then
		node preLaunchConnect.js
	fi

  node server.js
}

code "$@"

exit $?
