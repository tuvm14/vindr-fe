@echo off
setlocal

title Starting Vindr

pushd %~dp0

:: Get node_modules
node preLaunchVindr.js

@REM for /f "tokens=2 delims=:," %%a in ('findstr /R /C:"\"nameShort\":.*" product.json') do set NAMESHORT=%%~a
@REM set NAMESHORT=%NAMESHORT: "=%
@REM set NAMESHORT=%NAMESHORT:"=%.exe
@REM set CODE=".build\electron\%NAMESHORT%"

:: Manage built-in extensions
@REM if "%~1"=="--builtin" goto builtin

:: Configuration
@REM set NODE_ENV=development
@REM set VSCODE_DEV=1
@REM set VSCODE_CLI=1
@REM set ELECTRON_ENABLE_LOGGING=1
@REM set ELECTRON_ENABLE_STACK_DUMPING=1
:: Launch Connect

@REM %CODE% . %*
@REM node npm run start
goto end

@REM :builtin
@REM %CODE% build/builtin

@REM pause

:end

popd

endlocal
